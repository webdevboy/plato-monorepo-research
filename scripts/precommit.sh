#!/bin/bash
set -e

branch=$(git branch | sed -n -e 's/^\* \(.*\)/\1/p')
if [ $branch == master ] || [ $branch == develop ];
then
    echo "Forbidden action on $branch";
    exit 1;
fi

if [[ $branch != feature/* ]] && [[ $branch != bugfix/* ]];
then
  echo "Invalid branch name - should start with feature/ or bugfix/";
  exit 1;
fi

yarn lint;
yarn test;
yarn doc:coverage;