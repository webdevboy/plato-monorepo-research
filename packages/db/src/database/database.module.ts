import { Global, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import * as entities from '../entities';

@Global()
@Module({
  imports: [TypeOrmModule.forRoot(), TypeOrmModule.forFeature(Object.values(entities))],
})
export class DatabaseModule {}
