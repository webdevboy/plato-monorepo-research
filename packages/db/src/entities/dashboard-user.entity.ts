import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

import { Company } from './company.entity';
import { User } from './user.entity';

@Entity('dashboard_users', { schema: 'public' })
@Index('index_dashboard_users_on_company_id', ['company'])
export class DashboardUser {
  @PrimaryGeneratedColumn({
    type: 'integer',
    name: 'id',
  })
  id: number;

  @Column('integer', { name: 'company_id' })
  companyId: number;

  @ManyToOne(type => Company, company => company.dashboardUsers, {
    nullable: false,
  })
  @JoinColumn({ name: 'company_id' })
  company: Company;

  @Column('boolean', {
    nullable: false,
    default: () => 'true',
    name: 'is_active',
  })
  isActive: boolean;

  @Column('timestamp', {
    nullable: false,
    name: 'created_at',
    default: () => 'CURRENT_TIMESTAMP',
  })
  createdAt: Date;

  @Column('timestamp', {
    nullable: false,
    name: 'updated_at',
    default: () => 'CURRENT_TIMESTAMP',
  })
  updatedAt: Date;

  @OneToMany(type => User, users => users.dashboardUser)
  users: User[];
}
