import { Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

import { Request } from './request.entity';
import { Tag } from './tag.entity';

@Entity('request_tags', { schema: 'public' })
export class RequestTag {
  @PrimaryGeneratedColumn({
    type: 'integer',
    name: 'id',
  })
  id: number;

  @ManyToOne(type => Request, request => request.requestTags, {})
  @JoinColumn({ name: 'request_id' })
  request: Request | null;

  @ManyToOne(type => Tag, tag => tag.requestTags, {})
  @JoinColumn({ name: 'tag_id' })
  tag: Tag | null;
}
