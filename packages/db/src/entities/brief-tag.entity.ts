import { Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

import { Brief } from './brief.entity';
import { Tag } from './tag.entity';

@Entity('brief_tags', { schema: 'public' })
export class BriefTag {
  @PrimaryGeneratedColumn({
    type: 'integer',
    name: 'id',
  })
  id: number;

  @ManyToOne(type => Brief, brief => brief.briefTags, {})
  @JoinColumn({ name: 'brief_id' })
  brief: Brief | null;

  @ManyToOne(type => Tag, tag => tag.briefTags, {})
  @JoinColumn({ name: 'tag_id' })
  tag: Tag | null;
}
