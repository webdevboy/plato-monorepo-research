import { Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

import { DashboardUser } from './dashboard-user.entity';
import { ManagerUser } from './manager-user.entity';
import { Operator } from './operator.entity';
import { Subscription } from './subscription.entity';

@Entity('companies', { schema: 'public' })
export class Company {
  @PrimaryGeneratedColumn({
    type: 'integer',
    name: 'id',
  })
  id: number;

  @Column('varchar', {
    nullable: false,
    name: 'name',
  })
  name: string;

  @Column('varchar', {
    nullable: true,
    name: 'location',
  })
  location: string | null;

  @Column('varchar', {
    nullable: true,
    name: 'description',
  })
  description: string | null;

  @Column('integer', {
    nullable: true,
    name: 'nb_employees',
  })
  nbEmployees: number | null;

  @Column('integer', {
    nullable: true,
    name: 'nb_engineers',
  })
  nbEngineers: number | null;

  @Column('integer', {
    nullable: true,
    name: 'nb_tech_managers',
  })
  nbTechManagers: number | null;

  @Column('timestamp', {
    nullable: false,
    name: 'created_at',
    default: () => 'CURRENT_TIMESTAMP',
  })
  createdAt: Date;

  @Column('timestamp', {
    nullable: false,
    name: 'updated_at',
    default: () => 'CURRENT_TIMESTAMP',
  })
  updatedAt: Date;

  @Column('varchar', {
    nullable: true,
    name: 'notes',
  })
  notes: string | null;

  @Column('varchar', {
    nullable: true,
    name: 'account_importance',
  })
  accountImportance: string | null;

  @Column('boolean', {
    nullable: false,
    default: () => 'false',
    name: 'enterprise_only',
  })
  isEnterpriseOnly: boolean;

  @Column('varchar', {
    nullable: true,
    name: 'logo_file_name',
  })
  logoFileName: string | null;

  @Column('varchar', {
    nullable: true,
    name: 'logo_content_type',
  })
  logoContentType: string | null;

  @Column('integer', {
    nullable: true,
    name: 'logo_file_size',
  })
  logoFileSize: number | null;

  @Column('timestamp', {
    nullable: true,
    name: 'logo_updated_at',
  })
  logoUpdatedAt: Date | null;

  @Column('varchar', {
    nullable: true,
    name: 'logo_attribute_path',
  })
  logoAttributePath: string | null;

  @Column('boolean', {
    nullable: false,
    default: () => 'false',
    name: 'key_account',
  })
  isKeyAccount: boolean;

  @ManyToOne(type => Operator, operators => operators.companies)
  @JoinColumn({ name: 'operator_id' })
  operator: Operator | null;

  @OneToMany(type => Subscription, subscription => subscription.company)
  subscriptions: Subscription[];

  @OneToMany(type => DashboardUser, dashboardUser => dashboardUser.company)
  dashboardUsers: DashboardUser[];

  @OneToMany(type => ManagerUser, managerUser => managerUser.company)
  managerUsers: ManagerUser[];
}
