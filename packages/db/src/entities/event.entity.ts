import { Column, Entity, Index, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

import { Ama } from './ama.entity';
import { Request } from './request.entity';

@Entity('events', { schema: 'public' })
@Index('uk_4weld9vloo6u4ujehhqw2cm6a', ['googleId'], { unique: true })
@Index('index_events_on_mentor_id', ['mentorId'])
export class Event {
  @PrimaryGeneratedColumn({
    type: 'integer',
    name: 'id',
  })
  id: number;

  @Column('varchar', {
    nullable: true,
    name: 'google_id',
  })
  googleId: string | null;

  @Column('varchar', {
    nullable: true,
    name: 'status',
  })
  status: string | null;

  @Column('varchar', {
    nullable: true,
    name: 'summary',
  })
  summary: string | null;

  @Column('text', {
    nullable: true,
    name: 'description',
  })
  description: string | null;

  @Column('varchar', {
    nullable: true,
    name: 'location',
  })
  location: string | null;

  @Column('timestamp', {
    nullable: true,
    name: 'start_time',
  })
  startTime: Date | null;

  @Column('timestamp', {
    nullable: true,
    name: 'end_time',
  })
  endTime: Date | null;

  @Column('jsonb', {
    nullable: true,
    name: 'attendees',
  })
  attendees: object | null;

  @Column('timestamp', {
    nullable: true,
    name: 'google_created_at',
  })
  googleCreatedAt: Date | null;

  @Column('timestamp', {
    nullable: true,
    name: 'google_updated_at',
  })
  googleUpdatedAt: Date | null;

  @Column('timestamp', {
    nullable: false,
    name: 'created_at',
    default: () => 'CURRENT_TIMESTAMP',
  })
  createdAt: Date;

  @Column('timestamp', {
    nullable: false,
    name: 'updated_at',
    default: () => 'CURRENT_TIMESTAMP',
  })
  updatedAt: Date;

  @Column('integer', {
    nullable: true,
    name: 'mentor_id',
  })
  mentorId: number | null;

  @Column('boolean', {
    nullable: false,
    default: () => 'false',
    name: 'is_declined_by_mentor',
  })
  isDeclinedByMentor: boolean;

  @OneToMany(type => Ama, ama => ama.amaCalendarEvent)
  amas: Ama[];

  @OneToMany(type => Request, request => request.event)
  requests: Request[];
}
