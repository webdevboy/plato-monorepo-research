import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

import { Mentor } from './mentor.entity';
import { Request } from './request.entity';
import { StoryParagraph } from './story-paragraph.entity';
import { Teaser } from './teaser.entity';

@Entity('stories', { schema: 'public' })
@Index('index_stories_on_mentor_id', ['mentor'])
export class Story {
  @PrimaryGeneratedColumn({
    type: 'integer',
    name: 'id',
  })
  id: number;

  @Column('integer', {
    nullable: true,
    name: 'number',
  })
  number: number | null;

  @Column('varchar', {
    nullable: true,
    name: 'name',
  })
  name: string | null;

  @Column('varchar', {
    nullable: false,
    name: 'link',
  })
  link: string;

  @Column('varchar', {
    nullable: true,
    name: 'obviousness',
  })
  obviousness: string | null;

  @Column('varchar', {
    nullable: true,
    name: 'authenticity',
  })
  authenticity: string | null;

  @Column('varchar', {
    nullable: true,
    name: 'actionability',
  })
  actionability: string | null;

  @Column('varchar', {
    nullable: true,
    name: 'company_size',
  })
  companySize: string | null;

  @Column('varchar', {
    nullable: true,
    name: 'teaser',
  })
  description: string | null;

  @Column('varchar', {
    nullable: true,
    name: 'abstract',
  })
  abstract: string | null;

  @Column('timestamp', {
    nullable: true,
    name: 'content_updated_at',
  })
  contentUpdatedAt: Date | null;

  @Column('boolean', {
    nullable: false,
    default: () => 'false',
    name: 'validated',
  })
  isValidated: boolean;

  @Column('boolean', {
    nullable: false,
    default: () => 'false',
    name: 'usage_prevented',
  })
  isUsagePrevented: boolean;

  @Column('boolean', {
    nullable: false,
    default: () => 'false',
    name: 'is_fake',
  })
  isFake: boolean;

  @Column('boolean', {
    nullable: false,
    default: () => 'false',
    name: 'hide_on_website',
  })
  isHiddenOnWebsite: boolean;

  @Column('timestamp', {
    nullable: false,
    name: 'created_at',
    default: () => 'CURRENT_TIMESTAMP',
  })
  createdAt: Date;

  @Column('timestamp', {
    nullable: false,
    name: 'updated_at',
    default: () => 'CURRENT_TIMESTAMP',
  })
  updatedAt: Date;

  @ManyToOne(type => Mentor, mentor => mentor.stories, {})
  @JoinColumn({ name: 'mentor_id' })
  mentor: Mentor | null;

  @Column('varchar', {
    nullable: true,
    name: 'story_writer_email',
  })
  storyWriterEmail: string | null;

  @Column('varchar', {
    nullable: true,
    name: 'video_link',
  })
  videoLink: string | null;

  @Column('varchar', {
    nullable: true,
    name: 'status',
  })
  status: string | null;

  @Column('timestamp', {
    nullable: true,
    name: 'next_validation_date',
  })
  nextValidationDate: Date | null;

  @Column('timestamp', {
    nullable: true,
    name: 'written_at',
  })
  writtenAt: Date | null;

  @Column('timestamp', {
    nullable: true,
    name: 'validated_at',
  })
  validatedAt: Date | null;

  @Column('timestamp', {
    nullable: true,
    name: 'published_at',
  })
  publishedAt: Date | null;

  @Column('timestamp', {
    nullable: true,
    name: 'validating_at',
  })
  validatingAt: Date | null;

  @Column('timestamp', {
    nullable: true,
    name: 'ready_to_validate_at',
  })
  readyToValidateAt: Date | null;

  @Column('boolean', {
    nullable: false,
    default: () => 'false',
    name: 'migrated',
  })
  isMigrated: boolean;

  @Column('boolean', {
    nullable: false,
    default: () => 'false',
    name: 'sent_in_stories_channel',
  })
  isSentInStoriesChannel: boolean;

  @ManyToOne(type => Teaser, teaser => teaser.stories, {})
  @JoinColumn({ name: 'teaser_id' })
  teaser: Teaser | null;

  @OneToMany(type => Request, requests => requests.story)
  requests: Request[];

  @OneToMany(type => StoryParagraph, storyParagraph => storyParagraph.story)
  storyParagraphs: StoryParagraph[];
}
