import { Column, Entity, JoinTable, ManyToMany, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

import { BriefTag } from './brief-tag.entity';
import { Challenge } from './challenge.entity';
import { Mentor } from './mentor.entity';
import { RequestTag } from './request-tag.entity';
import { Teaser } from './teaser.entity';

@Entity('tags', { schema: 'public' })
export class Tag {
  @PrimaryGeneratedColumn({
    type: 'integer',
    name: 'id',
  })
  id: number;

  @Column('varchar', {
    nullable: false,
    name: 'name',
  })
  name: string;

  @Column('timestamp', {
    nullable: false,
    name: 'created_at',
    default: () => 'CURRENT_TIMESTAMP',
  })
  createdAt: Date;

  @Column('timestamp', {
    nullable: false,
    name: 'updated_at',
    default: () => 'CURRENT_TIMESTAMP',
  })
  updatedAt: Date;

  @Column('varchar', {
    nullable: true,
    name: 'details',
  })
  details: string | null;

  @Column('varchar', {
    nullable: true,
    name: 'slack_name',
  })
  slackName: string | null;

  @Column('varchar', {
    nullable: true,
    name: 'theme',
  })
  theme: string | null;

  @Column('varchar', {
    nullable: true,
    name: 'type',
  })
  type: string | null;

  @OneToMany(type => BriefTag, briefTag => briefTag.tag)
  briefTags: BriefTag[];

  @OneToMany(type => RequestTag, requestTag => requestTag.tag)
  requestTags: RequestTag[];

  @ManyToMany(type => Mentor, mentor => mentor.tags)
  mentors: Mentor[];

  @ManyToMany(type => Teaser, teaser => teaser.tags)
  teasers: Teaser[];

  @ManyToMany(type => Challenge, challenge => challenge.tags, {
    nullable: false,
  })
  @JoinTable({ name: 'challenge_tags' })
  challenges: Challenge[];
}
