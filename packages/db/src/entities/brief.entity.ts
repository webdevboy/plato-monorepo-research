import { Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

import { BriefChallenge } from './brief-challenge.entity';
import { BriefTag } from './brief-tag.entity';
import { Mentee } from './mentee.entity';

@Entity('briefs', { schema: 'public' })
export class Brief {
  @PrimaryGeneratedColumn({
    type: 'integer',
    name: 'id',
  })
  id: number;

  @Column('varchar', {
    nullable: false,
    name: 'share_type',
  })
  shareType: string;

  @Column('varchar', {
    nullable: false,
    name: 'briefing_one_liner',
  })
  briefingOneLiner: string;

  @Column('varchar', {
    nullable: false,
    name: 'briefing_description',
  })
  briefingDescription: string;

  @ManyToOne(type => Mentee, mentee => mentee.briefs, { nullable: false })
  @JoinColumn({ name: 'mentee_id' })
  mentee: Mentee;

  @Column('timestamp', {
    nullable: false,
    name: 'created_at',
    default: () => 'CURRENT_TIMESTAMP',
  })
  createdAt: Date;

  @Column('timestamp', {
    nullable: false,
    name: 'updated_at',
    default: () => 'CURRENT_TIMESTAMP',
  })
  updatedAt: Date;

  @Column('varchar', {
    nullable: true,
    name: 'problem_referer',
  })
  problemReferer: string | null;

  @Column('boolean', {
    nullable: false,
    default: () => 'false',
    name: 'closed',
  })
  isClosed: boolean;

  @Column('boolean', {
    nullable: false,
    default: () => 'false',
    name: 'to_match',
  })
  toMatch: boolean;

  @Column('timestamp', {
    nullable: true,
    name: 'close_date',
  })
  closeDate: Date | null;

  @OneToMany(type => BriefChallenge, briefChallenge => briefChallenge.brief)
  briefChallenges: BriefChallenge[];

  @OneToMany(type => BriefTag, briefTag => briefTag.brief)
  briefTags: BriefTag[];
}
