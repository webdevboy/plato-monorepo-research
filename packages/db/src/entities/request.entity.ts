import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

import { Event } from './event.entity';
import { Mentee } from './mentee.entity';
import { Mentor } from './mentor.entity';
import { RequestChallenge } from './request-challenge.entity';
import { RequestTag } from './request-tag.entity';
import { Story } from './story.entity';

@Entity('requests', { schema: 'public' })
@Index('index_requests_on_brief_id', ['briefId'])
@Index('index_requests_on_mentee_id', ['mentee'])
@Index('index_requests_on_mentor_id', ['mentor'])
@Index('index_requests_on_story_id', ['story'])
export class Request {
  @PrimaryGeneratedColumn({
    type: 'integer',
    name: 'id',
  })
  id: number;

  @Column('integer', {
    nullable: true,
    name: 'number',
  })
  number: number | null;

  @Column('varchar', {
    nullable: true,
    name: 'status',
  })
  status: string | null;

  @Column('varchar', {
    nullable: true,
    name: 'calendar_status',
  })
  calendarStatus: string | null;

  @Column('varchar', {
    nullable: true,
    name: 'call_type',
  })
  callType: string | null;

  @Column('varchar', {
    nullable: true,
    name: 'share_type',
  })
  shareType: string | null;

  @Column('varchar', {
    nullable: true,
    name: 'proposal_type',
  })
  proposalType: string | null;

  @Column('varchar', {
    nullable: true,
    name: 'mentee_invitation_status',
  })
  menteeInvitationStatus: string | null;

  @Column('varchar', {
    nullable: true,
    name: 'mentor_invitation_status',
  })
  mentorInvitationStatus: string | null;

  @Column('varchar', {
    nullable: true,
    name: 'briefing_one_liner',
  })
  briefingOneLiner: string | null;

  @Column('text', {
    nullable: true,
    name: 'briefing_description',
  })
  briefingDescription: string | null;

  @Column('varchar', {
    nullable: true,
    name: 'bridge_channel_name',
  })
  bridgeChannelName: string | null;

  @ManyToOne(type => Event, event => event.requests, {})
  @JoinColumn({ name: 'event_id' })
  event: Event | null;

  @Column('integer', {
    nullable: true,
    name: 'mentee_useful_grade',
  })
  menteeUsefulGrade: number | null;

  @Column('integer', {
    nullable: true,
    name: 'mentee_feeling_grade',
  })
  menteeFeelingGrade: number | null;

  @Column('integer', {
    nullable: true,
    name: 'mentor_feeling_grade',
  })
  mentorFeelingGrade: number | null;

  @Column('timestamp', {
    nullable: true,
    name: 'call_scheduled_at',
  })
  callScheduledAt: Date | null;

  @Column('timestamp', {
    nullable: true,
    name: 'next_proposal_date',
  })
  nextProposalDate: Date | null;

  @Column('timestamp', {
    nullable: true,
    name: 'event_updated_at',
  })
  eventUpdatedAt: Date | null;

  @Column('timestamp', {
    nullable: true,
    name: 'previous_connect_reminder_date',
  })
  previousConnectReminderDate: Date | null;

  @Column('timestamp', {
    nullable: true,
    name: 'next_connect_reminder_date',
  })
  nextConnectReminderDate: Date | null;

  @Column('timestamp', {
    nullable: true,
    name: 'rescheduling_proposal_expiration_date',
  })
  reschedulingProposalExpirationDate: Date | null;

  @Column('timestamp', {
    nullable: true,
    name: 'last_action_date',
  })
  lastActionDate: Date | null;

  @Column('timestamp', {
    nullable: true,
    name: 'follow_up_date',
  })
  followUpDate: Date | null;

  @Column('timestamp', {
    nullable: true,
    name: 'date_of_status_change_to_rescheduling',
  })
  dateOfStatusChangeToRescheduling: Date | null;

  @Column('boolean', {
    nullable: false,
    default: () => 'false',
    name: 'mentee_reminded',
  })
  isMenteeReminded: boolean;

  @Column('boolean', {
    nullable: false,
    default: () => 'false',
    name: 'mentor_reminded',
  })
  isMentorReminded: boolean;

  @Column('boolean', {
    nullable: false,
    default: () => 'false',
    name: 'follow_up_sent',
  })
  isFollowUpSent: boolean;

  @Column('boolean', {
    nullable: false,
    default: () => 'false',
    name: 'send_connect_reminder_2',
  })
  sendConnectReminder2: boolean;

  @Column('boolean', {
    nullable: false,
    default: () => 'false',
    name: 'send_connect_reminder_3',
  })
  sendConnectReminder3: boolean;

  @Column('boolean', {
    nullable: false,
    default: () => 'false',
    name: 'after_call_prompt_sent',
  })
  isAfterCallPromptSent: boolean;

  @Column('timestamp', {
    nullable: false,
    name: 'created_at',
    default: () => 'CURRENT_TIMESTAMP',
  })
  createdAt: Date;

  @Column('timestamp', {
    nullable: false,
    name: 'updated_at',
    default: () => 'CURRENT_TIMESTAMP',
  })
  updatedAt: Date;

  @ManyToOne(type => Mentee, mentee => mentee.requests, {})
  @JoinColumn({ name: 'mentee_id' })
  mentee: Mentee | null;

  @ManyToOne(type => Mentor, mentor => mentor.requests, {})
  @JoinColumn({ name: 'mentor_id' })
  mentor: Mentor | null;

  @ManyToOne(type => Story, story => story.requests, {})
  @JoinColumn({ name: 'story_id' })
  story: Story | null;

  @Column('integer', {
    nullable: true,
    default: () => '0',
    name: 'rescheduling_proposal_index',
  })
  reschedulingProposalIndex: number | null;

  @Column('integer', {
    nullable: true,
    name: 'bridge_channel_id',
  })
  bridgeChannelId: number | null;

  @Column('boolean', {
    nullable: false,
    default: () => 'false',
    name: 'ltm_declined_by_mentee',
  })
  ltmDeclinedByMentee: boolean;

  @Column('text', {
    nullable: true,
    name: 'shared_qualitative_feedback',
  })
  sharedQualitativeFeedback: string | null;

  @Column('text', {
    nullable: true,
    name: 'internal_qualitative_feedback',
  })
  internalQualitativeFeedback: string | null;

  @Column('integer', {
    nullable: true,
    name: 'call_issue_mentee_message_id',
  })
  callIssueMenteeMessageId: number | null;

  @Column('integer', {
    nullable: true,
    name: 'call_issue_mentor_message_id',
  })
  callIssueMentorMessageId: number | null;

  @Column('boolean', {
    nullable: false,
    default: () => 'false',
    name: 'internal',
  })
  internal: boolean;

  @Column('varchar', {
    nullable: true,
    name: 'previous_matchings',
  })
  previousMatchings: string | null;

  @Column('integer', {
    nullable: true,
    name: 'brief_id',
  })
  briefId: number | null;

  @Column('timestamp', {
    nullable: true,
    name: 'check_potentially_rescheduled_at',
  })
  checkPotentiallyRescheduledAt: Date | null;

  @Column('varchar', {
    nullable: true,
    name: 'structured_feedback',
  })
  structuredFeedback: string | null;

  @Column('varchar', {
    nullable: true,
    name: 'key_insight_1',
  })
  keyInsight1: string | null;

  @Column('varchar', {
    nullable: true,
    name: 'key_insight_2',
  })
  keyInsight2: string | null;

  @Column('varchar', {
    nullable: true,
    name: 'key_insight_3',
  })
  keyInsight3: string | null;

  @Column('boolean', {
    nullable: false,
    default: () => 'false',
    name: 'email_reminder_sent',
  })
  emailReminderSent: boolean;

  @Column('text', {
    nullable: true,
    name: 'mentor_feedback',
  })
  mentorFeedback: string | null;

  @Column('timestamp', {
    nullable: true,
    name: 'suggest_ltm_email_sent_at',
  })
  suggestLtmEmailSentAt: Date | null;

  @Column('varchar', {
    nullable: true,
    name: 'key_actions',
  })
  keyActions: string | null;

  @Column('timestamp', {
    nullable: true,
    name: 'key_actions_status_email_sent_at',
  })
  keyActionsStatusEmailSentAt: Date | null;

  @OneToMany(type => RequestChallenge, requestChallenge => requestChallenge.request)
  requestChallenges: RequestChallenge[];

  @OneToMany(type => RequestTag, requestTag => requestTag.request)
  requestTags: RequestTag[];
}
