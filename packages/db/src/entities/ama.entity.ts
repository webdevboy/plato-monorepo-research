import { Column, Entity, Index, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

import { Event } from './event.entity';
import { Program } from './program.entity';
import { Teaser } from './teaser.entity';

@Entity('amas', { schema: 'public' })
@Index('index_amas_on_program_id', ['program'])
export class Ama {
  @PrimaryGeneratedColumn({
    type: 'integer',
    name: 'id',
  })
  id: number;

  @Column('varchar', {
    nullable: true,
    name: 'status',
  })
  status: string | null;

  @Column('integer', {
    nullable: true,
    name: 'event_id',
  })
  eventId: number | null;

  @Column('integer', {
    nullable: false,
    name: 'mentor_id',
  })
  mentorId: number;

  @Column('integer', {
    nullable: true,
    name: 'story_id',
  })
  storyId: number | null;

  @Column('integer', {
    nullable: false,
    name: 'limit_attendances',
  })
  limitAttendances: number;

  @Column('timestamp', {
    nullable: false,
    name: 'call_scheduled_at',
  })
  callScheduledAt: Date;

  @Column('boolean', {
    nullable: true,
    default: () => 'false',
    name: 'featured',
  })
  isFeatured: boolean | null;

  @Column('boolean', {
    nullable: true,
    default: () => 'false',
    name: 'internal',
  })
  isInternal: boolean | null;

  @Column('varchar', {
    nullable: true,
    name: 'closed_reason',
  })
  closedReason: string | null;

  @Column('timestamp', {
    nullable: false,
    name: 'created_at',
    default: () => 'CURRENT_TIMESTAMP',
  })
  createdAt: Date;

  @Column('timestamp', {
    nullable: false,
    name: 'updated_at',
    default: () => 'CURRENT_TIMESTAMP',
  })
  updatedAt: Date;

  @Column('varchar', {
    nullable: true,
    name: 'mentor_invitation_status',
  })
  mentorInvitationStatus: string | null;

  @ManyToOne(type => Event, events => events.amas, {})
  @JoinColumn({ name: 'ama_calendar_event_id' })
  amaCalendarEvent: Event | null;

  @Column('integer', {
    nullable: true,
    name: 'mentor_feeling_grade',
  })
  mentorFeelingGrade: number | null;

  @Column('integer', {
    nullable: true,
    name: 'bridge_channel_id',
  })
  bridgeChannelId: number | null;

  @Column('timestamp', {
    nullable: true,
    name: 'send_feedback_email_at',
  })
  sendFeedbackEmailAt: Date | null;

  @ManyToOne(type => Teaser, teasers => teasers.amas, {})
  @JoinColumn({ name: 'teaser_id' })
  teaser: Teaser | null;

  @ManyToOne(type => Program, program => program.amas, { onDelete: 'SET NULL' })
  @JoinColumn({ name: 'program_id' })
  program: Program | null;
}
