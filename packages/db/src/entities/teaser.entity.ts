import { Column, Entity, JoinTable, ManyToMany, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

import { Ama } from './ama.entity';
import { Challenge } from './challenge.entity';
import { Story } from './story.entity';
import { Tag } from './tag.entity';

@Entity('teasers', { schema: 'public' })
export class Teaser {
  @PrimaryGeneratedColumn({
    type: 'integer',
    name: 'id',
  })
  id: number;

  @Column('varchar', {
    nullable: true,
    name: 'name',
  })
  name: string | null;

  @Column('varchar', {
    nullable: true,
    name: 'teaser',
  })
  teaser: string | null;

  @Column('timestamp', {
    nullable: false,
    name: 'created_at',
    default: () => 'CURRENT_TIMESTAMP',
  })
  createdAt: Date;

  @Column('timestamp', {
    nullable: false,
    name: 'updated_at',
    default: () => 'CURRENT_TIMESTAMP',
  })
  updatedAt: Date;

  @OneToMany(type => Ama, amas => amas.teaser)
  amas: Ama[];

  @OneToMany(type => Story, stories => stories.teaser)
  stories: Story[];

  @ManyToMany(type => Challenge, challenge => challenge.teasers, {
    nullable: false,
  })
  @JoinTable({ name: 'teasers_challenges' })
  challenges: Challenge[];

  @ManyToMany(type => Tag, tag => tag.teasers, { nullable: false })
  @JoinTable({ name: 'teasers_tags' })
  tags: Tag[];
}
