import { Column, Entity, Index, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

import { Story } from './story.entity';

@Entity('story_paragraphs', { schema: 'public' })
@Index('index_story_paragraphs_on_story_id', ['story'])
export class StoryParagraph {
  @PrimaryGeneratedColumn({
    type: 'integer',
    name: 'id',
  })
  id: number;

  @ManyToOne(type => Story, story => story.storyParagraphs, { nullable: false })
  @JoinColumn({ name: 'story_id' })
  story: Story;

  @Column('varchar', {
    nullable: true,
    name: 'title',
  })
  title: string | null;

  @Column('varchar', {
    nullable: true,
    name: 'body',
  })
  body: string | null;

  @Column('integer', {
    nullable: true,
    name: 'index',
  })
  index: number | null;

  @Column('timestamp', {
    nullable: false,
    name: 'created_at',
    default: () => 'CURRENT_TIMESTAMP',
  })
  createdAt: Date;

  @Column('timestamp', {
    nullable: false,
    name: 'updated_at',
    default: () => 'CURRENT_TIMESTAMP',
  })
  updatedAt: Date;
}
