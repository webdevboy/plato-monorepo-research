import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

import { Program } from './program.entity';

@Entity('tracks', { schema: 'public' })
export class Track {
  @PrimaryGeneratedColumn({
    type: 'integer',
    name: 'id',
  })
  id: number;

  @Column('varchar', {
    nullable: false,
    name: 'name',
  })
  name: string;

  @Column('varchar', {
    nullable: false,
    name: 'description',
  })
  description: string;

  @Column('varchar', {
    nullable: false,
    name: 'criteria',
  })
  criteria: string;

  @Column('timestamp', {
    nullable: false,
    name: 'created_at',
    default: () => 'CURRENT_TIMESTAMP',
  })
  createdAt: Date;

  @Column('timestamp', {
    nullable: false,
    name: 'updated_at',
    default: () => 'CURRENT_TIMESTAMP',
  })
  updatedAt: Date;

  @OneToMany(type => Program, program => program.track, { onDelete: 'CASCADE' })
  programs: Program[];
}
