import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

import { Company } from './company.entity';
import { SubscriptionItem } from './subscription-item.entity';

@Entity('subscriptions', { schema: 'public' })
@Index('index_subscriptions_on_company_id', ['company'])
export class Subscription {
  @PrimaryGeneratedColumn({
    type: 'integer',
    name: 'id',
  })
  id: number;

  @Column('varchar', {
    nullable: true,
    name: 'stripe_id',
  })
  stripeId: string | null;

  @Column('varchar', {
    nullable: true,
    name: 'customer_stripe_id',
  })
  customerStripeId: string | null;

  @Column('varchar', {
    nullable: true,
    name: 'payment_method',
  })
  paymentMethod: string | null;

  @ManyToOne(type => Company, company => company.subscriptions, { nullable: false })
  @JoinColumn({ name: 'company_id' })
  company: Company;

  @Column('timestamp', {
    nullable: false,
    name: 'created_at',
    default: () => 'CURRENT_TIMESTAMP',
  })
  createdAt: Date;

  @Column('timestamp', {
    nullable: false,
    name: 'updated_at',
    default: () => 'CURRENT_TIMESTAMP',
  })
  updatedAt: Date;

  @OneToMany(type => SubscriptionItem, subscriptionItem => subscriptionItem.subscription)
  subscriptionItems: SubscriptionItem[];
}
