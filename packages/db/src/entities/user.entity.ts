import * as bcrypt from 'bcrypt';
import {
  BeforeInsert,
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

import { DashboardUser } from './dashboard-user.entity';
import { ManagerUser } from './manager-user.entity';
import { Operator } from './operator.entity';

@Entity('users', { schema: 'public' })
@Index('index_users_on_manager_user_id', ['managerUser'])
@Index('index_users_on_operator_id', ['operator'])
export class User {
  @PrimaryGeneratedColumn({
    type: 'integer',
    name: 'id',
  })
  id: number;

  @Column('varchar', {
    nullable: true,
    name: 'first_name',
  })
  firstName: string | null;

  @Column('varchar', {
    nullable: true,
    name: 'last_name',
  })
  lastName: string | null;

  @Column('varchar', {
    nullable: false,
    name: 'email',
    unique: true
  })
  email: string;

  @Column('varchar', {
    nullable: true,
    name: 'password',
  })
  password: string | null;

  @Column('boolean', {
    nullable: true,
    name: 'enabled',
  })
  isEnabled: boolean | null;

  @Column('timestamp', {
    nullable: true,
    name: 'last_password_reset_date',
  })
  lastPasswordResetDate: Date | null;

  @Column('timestamp', {
    nullable: true,
    name: 'name_or_email_updated_at',
  })
  nameOrEmailUpdatedAt: Date | null;

  @ManyToOne(type => ManagerUser, managerUser => managerUser.users, {})
  @JoinColumn({ name: 'manager_user_id' })
  managerUser: ManagerUser | null;

  @ManyToOne(type => Operator, operator => operator.users, {})
  @JoinColumn({ name: 'operator_id' })
  operator: Operator | null;

  @Column('timestamp', {
    nullable: false,
    name: 'created_at',
    default: () => 'CURRENT_TIMESTAMP',
  })
  createdAt: Date;

  @Column('timestamp', {
    nullable: false,
    name: 'updated_at',
    default: () => 'CURRENT_TIMESTAMP',
  })
  updatedAt: Date;

  @Column('varchar', {
    nullable: true,
    name: 'reset_password_token',
  })
  resetPasswordToken: string | null;

  @Column('timestamp', {
    nullable: true,
    name: 'reset_password_token_expiration_date',
  })
  resetPasswordTokenExpirationDate: Date | null;

  @Column('character varying', {
    nullable: true,
    name: 'linkedin_id',
  })
  linkedinId: string | null;

  @Column('character varying', {
    nullable: true,
    name: 'google_id',
  })
  googleId: string | null;

  @Column('boolean', {
    nullable: true,
    default: () => 'false',
    name: 'external_app',
  })
  isExternalApp: boolean | null;

  @Column('uuid', {
    nullable: false,
    default: () => 'uuid_generate_v4()',
    name: 'uuid',
  })
  uuid: string;

  @Column('varchar', {
    nullable: true,
    name: 'time_zone',
  })
  timeZone: string | null;

  @ManyToOne(type => DashboardUser, dashboardUser => dashboardUser.users, {})
  @JoinColumn({ name: 'dashboard_user_id' })
  dashboardUser: DashboardUser | null;

  @BeforeInsert()
  hashPassword() {
    if (this.password) {
      this.password = bcrypt.hashSync(this.password, 10);
    }
  }
}
