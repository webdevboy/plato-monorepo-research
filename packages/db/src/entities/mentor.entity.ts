import { Column, Entity, ManyToMany, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

import { ManagerUser } from './manager-user.entity';
import { Request } from './request.entity';
import { Story } from './story.entity';
import { Tag } from './tag.entity';

@Entity('mentors', { schema: 'public' })
export class Mentor {
  @PrimaryGeneratedColumn({
    type: 'integer',
    name: 'id',
  })
  id: number;

  @Column('varchar', {
    nullable: true,
    name: 'about_myself',
  })
  aboutMyself: string | null;

  @Column('varchar', {
    nullable: true,
    name: 'company_name',
  })
  companyName: string | null;

  @Column('varchar', {
    nullable: true,
    name: 'next_availabilities',
  })
  nextAvailabilities: string | null;

  @Column('varchar', {
    nullable: true,
    name: 'company_type',
  })
  companyType: string | null;

  @Column('varchar', {
    nullable: true,
    name: 'source',
  })
  source: string | null;

  @Column('integer', {
    nullable: true,
    name: 'ltm_max_nb',
  })
  ltmMaxNb: number | null;

  @Column('timestamp', {
    nullable: true,
    name: 'next_free_slot',
  })
  nextFreeSlot: Date | null;

  @Column('timestamp', {
    nullable: true,
    name: 'delete_story_gathering_proposal_at',
  })
  deleteStoryGatheringProposalAt: Date | null;

  @Column('timestamp', {
    nullable: true,
    name: 'slot_time',
  })
  slotTime: Date | null;

  @Column('boolean', {
    nullable: false,
    default: () => 'false',
    name: 'do_not_propose_ltm',
  })
  doNotProposeLtm: boolean;

  @Column('timestamp', {
    nullable: false,
    name: 'created_at',
    default: () => 'CURRENT_TIMESTAMP',
  })
  createdAt: Date;

  @Column('timestamp', {
    nullable: false,
    name: 'updated_at',
    default: () => 'CURRENT_TIMESTAMP',
  })
  updatedAt: Date;

  @Column('varchar', {
    nullable: true,
    name: 'company_logo_file_name',
  })
  companyLogoFileName: string | null;

  @Column('varchar', {
    nullable: true,
    name: 'company_logo_content_type',
  })
  companyLogoContentType: string | null;

  @Column('integer', {
    nullable: true,
    name: 'company_logo_file_size',
  })
  companyLogoFileSize: number | null;

  @Column('timestamp', {
    nullable: true,
    name: 'company_logo_updated_at',
  })
  companyLogoUpdatedAt: Date | null;

  @Column('boolean', {
    nullable: false,
    default: () => 'false',
    name: 'integrated_to_site',
  })
  isIntegratedToSite: boolean;

  @Column('boolean', {
    nullable: false,
    default: () => 'false',
    name: 'refused_website_appearance',
  })
  hasRefusedWebsiteAppearance: boolean;

  @Column('varchar', {
    nullable: true,
    name: 'company_logo_attribute_path',
  })
  companyLogoAttributePath: string | null;

  @Column('integer', {
    nullable: true,
    name: 'caliber',
  })
  caliber: number | null;

  @Column('varchar', {
    nullable: true,
    name: 'story_interviewer_email',
  })
  storyInterviewerEmail: string | null;

  @Column('text', {
    nullable: true,
    name: 'notes',
  })
  notes: string | null;

  @Column('varchar', {
    nullable: false,
    default: () => `'Active'`,
    name: 'status',
  })
  status: string;

  @Column('boolean', {
    nullable: false,
    default: () => 'false',
    name: 'intro_sent',
  })
  isIntroSent: boolean;

  @Column('timestamp', {
    nullable: true,
    name: 'intro_last_reminder',
  })
  introLastReminder: Date | null;

  @Column('boolean', {
    nullable: false,
    default: () => 'false',
    name: 'onboarded',
  })
  isOnboarded: boolean;

  @Column('boolean', {
    nullable: false,
    default: () => 'false',
    name: 'internal_only',
  })
  isInternalOnly: boolean;

  @Column('integer', {
    nullable: false,
    default: () => '0',
    name: 'sessions_in_a_row_nb',
  })
  sessionsInARowNb: number;

  @OneToMany(type => ManagerUser, managerUser => managerUser.mentor)
  managerUsers: ManagerUser[];

  @OneToMany(type => Story, story => story.mentor)
  stories: Story[];

  @ManyToMany(type => Tag, tags => tags.mentors)
  tags: Tag[];

  @OneToMany(type => Request, requests => requests.mentor)
  requests: Request[];
}
