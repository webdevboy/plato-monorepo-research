import { Column, Entity, Index, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

import { Mentee } from './mentee.entity';
import { Program } from './program.entity';

@Entity('program_applications', { schema: 'public' })
@Index('index_program_applications_on_mentee_id', ['mentee'])
@Index('index_program_applications_on_program_id', ['program'])
export class ProgramApplication {
  @PrimaryGeneratedColumn({
    type: 'integer',
    name: 'id',
  })
  id: number;

  @Column('varchar', {
    nullable: true,
    name: 'goal',
  })
  goal: string | null;

  @Column('boolean', {
    nullable: true,
    default: () => 'false',
    name: 'criteria_met',
  })
  hasMetCriteria: boolean | null;

  @ManyToOne(type => Mentee, mentee => mentee.programApplications, {
    nullable: false,
    onDelete: 'SET NULL',
  })
  @JoinColumn({ name: 'mentee_id' })
  mentee: Mentee;

  @ManyToOne(type => Program, program => program.programApplications, {
    nullable: false,
    onDelete: 'SET NULL',
  })
  @JoinColumn({ name: 'program_id' })
  program: Program;

  @Column('varchar', {
    nullable: true,
    name: 'status',
  })
  status: string | null;

  @Column('timestamp', {
    nullable: false,
    name: 'created_at',
    default: () => 'CURRENT_TIMESTAMP',
  })
  createdAt: Date;

  @Column('timestamp', {
    nullable: false,
    name: 'updated_at',
    default: () => 'CURRENT_TIMESTAMP',
  })
  updatedAt: Date;

  @Column('integer', {
    nullable: true,
    default: () => `'{}'::integer[]`,
    array: true,
    name: 'amas_committed',
  })
  amasCommitted: number[];
}
