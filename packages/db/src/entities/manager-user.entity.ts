import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

import { Company } from './company.entity';
import { Mentee } from './mentee.entity';
import { Mentor } from './mentor.entity';
import { Persona } from './persona.entity';
import { User } from './user.entity';

@Entity('manager_users', { schema: 'public' })
@Index('index_manager_users_on_company_id', ['company'])
@Index('index_manager_users_on_mentee_id', ['mentee'])
@Index('index_manager_users_on_mentor_id', ['mentor'])
export class ManagerUser {
  @PrimaryGeneratedColumn({
    type: 'integer',
    name: 'id',
  })
  id: number;

  @Column('varchar', {
    nullable: true,
    name: 'title',
  })
  title: string | null;

  @Column('varchar', {
    nullable: true,
    name: 'linkedin',
  })
  linkedin: string | null;

  @Column('varchar', {
    nullable: true,
    name: 'hierarchy',
  })
  hierarchy: string | null;

  @Column('integer', {
    nullable: true,
    name: 'size_of_team',
  })
  sizeOfTeam: number | null;

  @Column('varchar', {
    nullable: true,
    name: 'picture_file_name',
  })
  pictureFileName: string | null;

  @Column('varchar', {
    nullable: true,
    name: 'picture_content_type',
  })
  pictureContentType: string | null;

  @Column('integer', {
    nullable: true,
    name: 'picture_file_size',
  })
  pictureFileSize: number | null;

  @Column('timestamp', {
    nullable: true,
    name: 'picture_updated_at',
  })
  pictureUpdatedAt: Date | null;

  @Column('varchar', {
    nullable: true,
    name: 'picture_attribute_path',
  })
  pictureAttributePath: string | null;

  @Column('boolean', {
    nullable: true,
    default: () => 'false',
    name: 'refused_recording',
  })
  hasRefusedRecording: boolean | null;

  @Column('boolean', {
    nullable: true,
    default: () => 'false',
    name: 'on_beta',
  })
  isOnBeta: boolean | null;

  @Column('text', {
    nullable: true,
    name: 'location',
  })
  location: string | null;

  @Column('integer', {
    nullable: true,
    name: 'direct_reports_nb',
  })
  directReportsNb: number | null;

  @Column('double precision', {
    nullable: true,
    name: 'years_in_management_nb',
  })
  yearsInManagementNb: number | null;

  @Column('integer', { nullable: true, name: 'company_id' })
  companyId: number;

  @ManyToOne(type => Company, companies => companies.managerUsers)
  @JoinColumn({ name: 'company_id' })
  company: Company;

  @ManyToOne(type => Mentor, mentors => mentors.managerUsers)
  @JoinColumn({ name: 'mentor_id' })
  mentor: Mentor | null;

  @Column('integer', { nullable: true, name: 'mentee_id' })
  menteeId: number;

  @ManyToOne(type => Mentee, mentees => mentees.managerUsers)
  @JoinColumn({ name: 'mentee_id' })
  mentee: Mentee | null;

  @Column('timestamp', {
    nullable: false,
    name: 'created_at',
    default: () => 'CURRENT_TIMESTAMP',
  })
  createdAt: Date;

  @Column('timestamp', {
    nullable: false,
    name: 'updated_at',
    default: () => 'CURRENT_TIMESTAMP',
  })
  updatedAt: Date;

  @Column('double precision', {
    nullable: true,
    name: 'years_in_current_role_nb',
  })
  yearsInCurrentRoleNb: number | null;

  @Column('varchar', {
    nullable: true,
    name: 'objectives',
  })
  objectives: string | null;

  @Column('varchar', {
    nullable: true,
    name: 'strengths',
  })
  strengths: string | null;

  @Column('varchar', {
    nullable: true,
    name: 'weaknesses',
  })
  weaknesses: string | null;

  @Column('varchar', {
    nullable: true,
    name: 'trainings',
  })
  trainings: string | null;

  @Column('timestamp', {
    nullable: true,
    name: 'first_time_slot',
  })
  firstTimeSlot: Date | null;

  @Column('timestamp', {
    nullable: true,
    name: 'second_time_slot',
  })
  secondTimeSlot: Date | null;

  @Column('jsonb', {
    nullable: true,
    name: 'additional_data',
  })
  additionalData: object | null;

  @OneToMany(type => User, user => user.managerUser)
  users: User[];

  @ManyToOne(type => Persona, persona => persona.managerUsers, {
    onDelete: 'SET NULL',
  })
  @JoinColumn({ name: 'persona_id' })
  persona: Persona | null;
}
