import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

import { Company } from './company.entity';
import { Mentee } from './mentee.entity';
import { User } from './user.entity';

@Entity('operators', { schema: 'public' })
export class Operator {
  @PrimaryGeneratedColumn({
    type: 'integer',
    name: 'id',
  })
  id: number;

  @Column('timestamp', {
    nullable: false,
    name: 'created_at',
    default: () => 'CURRENT_TIMESTAMP',
  })
  createdAt: Date;

  @Column('timestamp', {
    nullable: false,
    name: 'updated_at',
    default: () => 'CURRENT_TIMESTAMP',
  })
  updatedAt: Date;

  @Column('varchar', {
    nullable: true,
    name: 'calendly',
  })
  calendly: string | null;

  @Column('boolean', {
    nullable: true,
    default: () => 'false',
    name: 'pm_manager',
  })
  pmManager: boolean | null;

  @Column('boolean', {
    nullable: true,
    default: () => 'false',
    name: 'ka_manager',
  })
  isKeyAccountManager: boolean | null;

  @Column('integer', {
    nullable: true,
    default: () => '0',
    name: 'tc_capacity',
  })
  tcCapacity: number | null;

  @OneToMany(type => Company, company => company.operator)
  companies: Company[];

  @OneToMany(type => Mentee, mentee => mentee.operator)
  mentees: Mentee[];

  @OneToMany(type => User, user => user.operator)
  users: User[];
}
