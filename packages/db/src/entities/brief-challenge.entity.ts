import { Entity, Index, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

import { Brief } from './brief.entity';
import { Challenge } from './challenge.entity';

@Entity('brief_challenges', { schema: 'public' })
@Index('index_brief_challenges_on_brief_id_and_challenge_id', ['brief', 'challenge'], {
  unique: true,
})
export class BriefChallenge {
  @PrimaryGeneratedColumn({
    type: 'integer',
    name: 'id',
  })
  id: number;

  @ManyToOne(type => Brief, brief => brief.briefChallenges, { nullable: false })
  @JoinColumn({ name: 'brief_id' })
  brief: Brief;

  @ManyToOne(type => Challenge, challenge => challenge.briefChallenges, {
    nullable: false,
  })
  @JoinColumn({ name: 'challenge_id' })
  challenge: Challenge;
}
