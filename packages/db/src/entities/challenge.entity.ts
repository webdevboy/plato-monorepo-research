import { Column, Entity, ManyToMany, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

import { BriefChallenge } from './brief-challenge.entity';
import { Persona } from './persona.entity';
import { RequestChallenge } from './request-challenge.entity';
import { Tag } from './tag.entity';
import { Teaser } from './teaser.entity';

@Entity('challenges', { schema: 'public' })
export class Challenge {
  @PrimaryGeneratedColumn({
    type: 'integer',
    name: 'id',
  })
  id: number;

  @Column('varchar', {
    nullable: false,
    name: 'title',
  })
  title: string;

  @Column('varchar', {
    nullable: true,
    name: 'description',
  })
  description: string | null;

  @Column('timestamp', {
    nullable: false,
    name: 'created_at',
    default: () => 'CURRENT_TIMESTAMP',
  })
  createdAt: Date;

  @Column('timestamp', {
    nullable: false,
    name: 'updated_at',
    default: () => 'CURRENT_TIMESTAMP',
  })
  updatedAt: Date;

  @OneToMany(type => BriefChallenge, briefChallenge => briefChallenge.challenge)
  briefChallenges: BriefChallenge[];

  @OneToMany(type => RequestChallenge, requestChallenge => requestChallenge.challenge)
  requestChallenges: RequestChallenge[];

  @ManyToMany(type => Persona, persona => persona.challenges)
  personas: Persona[];

  @ManyToMany(type => Teaser, teaser => teaser.challenges)
  teasers: Teaser[];

  @ManyToMany(type => Tag, tags => tags.challenges)
  tags: Tag[];
}
