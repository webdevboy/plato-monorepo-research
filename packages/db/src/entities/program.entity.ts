import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

import { Ama } from './ama.entity';
import { ProgramApplication } from './program-application.entity';
import { Track } from './track.entity';

@Entity('programs', { schema: 'public' })
@Index('index_programs_on_status', ['status'])
@Index('index_programs_on_track_id', ['track'])
export class Program {
  @PrimaryGeneratedColumn({
    type: 'integer',
    name: 'id',
  })
  id: number;

  @ManyToOne(type => Track, track => track.programs, {
    nullable: false,
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'track_id' })
  track: Track;

  @Column('varchar', {
    nullable: false,
    name: 'status',
  })
  status: string;

  @Column('integer', {
    nullable: false,
    name: 'limit_attendances',
  })
  limitAttendances: number;

  @Column('timestamp', {
    nullable: false,
    name: 'created_at',
    default: () => 'CURRENT_TIMESTAMP',
  })
  createdAt: Date;

  @Column('timestamp', {
    nullable: false,
    name: 'updated_at',
    default: () => 'CURRENT_TIMESTAMP',
  })
  updatedAt: Date;

  @Column('varchar', {
    nullable: true,
    name: 'topics_discussed',
  })
  topicsDiscussed: string | null;

  @Column('boolean', {
    nullable: true,
    default: () => 'false',
    name: 'european_timezone_compatible',
  })
  isEuropeanTimezoneCompatible: boolean | null;

  @OneToMany(type => Ama, ama => ama.program, { onDelete: 'SET NULL' })
  amas: Ama[];

  @OneToMany(type => ProgramApplication, programApplication => programApplication.program, {
    onDelete: 'SET NULL',
  })
  programApplications: ProgramApplication[];
}
