import { Column, Entity, Index, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

import { Subscription } from './subscription.entity';

@Entity('subscription_items', { schema: 'public' })
@Index('index_subscription_items_on_subscription_id', ['subscription'])
export class SubscriptionItem {
  @PrimaryGeneratedColumn({
    type: 'integer',
    name: 'id',
  })
  id: number;

  @Column('varchar', {
    nullable: true,
    name: 'stripe_id',
  })
  stripeId: string | null;

  @Column('varchar', {
    nullable: false,
    name: 'plan_stripe_id',
  })
  planStripeId: string;

  @Column('integer', {
    nullable: false,
    name: 'quantity',
  })
  quantity: number;

  @ManyToOne(type => Subscription, subscription => subscription.subscriptionItems, {
    nullable: false,
  })
  @JoinColumn({ name: 'subscription_id' })
  subscription: Subscription;

  @Column('timestamp', {
    nullable: false,
    name: 'created_at',
    default: () => 'CURRENT_TIMESTAMP',
  })
  createdAt: Date;

  @Column('timestamp', {
    nullable: false,
    name: 'updated_at',
    default: () => 'CURRENT_TIMESTAMP',
  })
  updatedAt: Date;
}
