import { Entity, Index, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

import { Challenge } from './challenge.entity';
import { Request } from './request.entity';

@Entity('request_challenges', { schema: 'public' })
@Index('index_request_challenges_on_request_id_and_challenge_id', ['challenge', 'request'], {
  unique: true,
})
export class RequestChallenge {
  @PrimaryGeneratedColumn({
    type: 'integer',
    name: 'id',
  })
  id: number;

  @ManyToOne(type => Request, request => request.requestChallenges, {
    nullable: false,
  })
  @JoinColumn({ name: 'request_id' })
  request: Request;

  @ManyToOne(type => Challenge, challenge => challenge.requestChallenges, {
    nullable: false,
  })
  @JoinColumn({ name: 'challenge_id' })
  challenge: Challenge;
}
