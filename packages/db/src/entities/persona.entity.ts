import { Column, Entity, JoinTable, ManyToMany, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

import { Challenge } from './challenge.entity';
import { ManagerUser } from './manager-user.entity';

@Entity('personas', { schema: 'public' })
export class Persona {
  @PrimaryGeneratedColumn({
    type: 'integer',
    name: 'id',
  })
  id: number;

  @Column('varchar', {
    nullable: false,
    name: 'name',
  })
  name: string;

  @Column('varchar', {
    nullable: true,
    name: 'description',
  })
  description: string | null;

  @Column('timestamp', {
    nullable: false,
    name: 'created_at',
    default: () => 'CURRENT_TIMESTAMP',
  })
  createdAt: Date;

  @Column('timestamp', {
    nullable: false,
    name: 'updated_at',
    default: () => 'CURRENT_TIMESTAMP',
  })
  updatedAt: Date;

  @OneToMany(type => ManagerUser, managerUser => managerUser.persona, {
    onDelete: 'SET NULL',
  })
  managerUsers: ManagerUser[] | null;

  @ManyToMany(type => Challenge, challenge => challenge.personas, {
    nullable: false,
  })
  @JoinTable({ name: 'persona_challenges' })
  challenges: Challenge[];
}
