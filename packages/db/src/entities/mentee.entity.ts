import { Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

import { Brief } from './brief.entity';
import { ManagerUser } from './manager-user.entity';
import { Operator } from './operator.entity';
import { ProgramApplication } from './program-application.entity';
import { Request } from './request.entity';

@Entity('mentees', { schema: 'public' })
export class Mentee {
  @PrimaryGeneratedColumn({
    type: 'integer',
    name: 'id',
  })
  id: number;

  @Column('varchar', {
    nullable: true,
    name: 'price_status',
  })
  priceStatus: string | null;

  @Column('varchar', {
    nullable: true,
    name: 'channel_prefix',
  })
  channelPrefix: string | null;

  @Column('varchar', {
    nullable: true,
    name: 'ob_status',
  })
  obStatus: string | null;

  @Column('varchar', {
    nullable: true,
    name: 'bridge_status',
  })
  bridgeStatus: string | null;

  @Column('varchar', {
    nullable: true,
    name: 'rsp_status',
  })
  rspStatus: string | null;

  @Column('varchar', {
    nullable: true,
    name: 'about_direct_reports',
  })
  aboutDirectReports: string | null;

  @Column('varchar', {
    nullable: true,
    name: 'operator_channel_name',
  })
  operatorChannelName: string | null;

  @Column('integer', {
    nullable: true,
    name: 'rsp_frequency',
  })
  rspFrequency: number | null;

  @Column('boolean', {
    nullable: false,
    default: () => 'false',
    name: 'is_setup',
  })
  isSetup: boolean;

  @Column('timestamp', {
    nullable: false,
    name: 'created_at',
    default: () => 'CURRENT_TIMESTAMP',
  })
  createdAt: Date;

  @Column('timestamp', {
    nullable: false,
    name: 'updated_at',
    default: () => 'CURRENT_TIMESTAMP',
  })
  updatedAt: Date;

  @Column('varchar', {
    nullable: true,
    name: 'lead_id',
  })
  leadId: string | null;

  @Column('varchar', {
    nullable: true,
    name: 'client_type',
  })
  clientType: string | null;

  @Column('date', {
    nullable: true,
    name: 'churn_date',
  })
  churnDate: string | null;

  @Column('varchar', {
    nullable: true,
    name: 'city',
  })
  city: string | null;

  @Column('varchar', {
    nullable: true,
    name: 'intercom_id',
  })
  intercomId: string | null;

  @Column('varchar', {
    nullable: true,
    name: 'cs_owner',
  })
  csOwner: string | null;

  @Column('boolean', {
    nullable: false,
    default: () => 'true',
    name: 'rsp_activated',
  })
  hasRspActivated: boolean;

  @Column('boolean', {
    nullable: true,
    default: () => 'false',
    name: 'website_onboarded',
  })
  isWebsiteOnboarded: boolean | null;

  @Column('boolean', {
    nullable: false,
    default: () => 'true',
    name: 'summary_activated',
  })
  summaryActivated: boolean;

  @Column('boolean', {
    nullable: false,
    default: () => 'true',
    name: 'invitation_reminder_activated',
  })
  isInvitationReminderActivated: boolean;

  @Column('boolean', {
    nullable: false,
    default: () => 'true',
    name: 'follow_up_activated',
  })
  isFollowUpActivated: boolean;

  @Column('boolean', {
    nullable: false,
    default: () => 'true',
    name: 'featured_ama_promo_activated',
  })
  isFeaturedAmaPromoActivated: boolean;

  @ManyToOne(type => Operator, operator => operator.mentees)
  @JoinColumn({ name: 'operator_id' })
  operator: Operator;

  @Column('boolean', {
    nullable: false,
    default: () => 'false',
    name: 'ob_call_scheduled',
  })
  isOBCallScheduled: boolean;

  @Column('text', {
    nullable: true,
    name: 'notes',
  })
  notes: string | null;

  @Column('varchar', {
    nullable: true,
    default: () => `'{}'::varchar[]`,
    array: true,
    name: 'onboarding_steps',
  })
  onboardingSteps: string[];

  @Column('boolean', {
    nullable: false,
    default: () => 'false',
    name: 'is_profile_complete',
  })
  isProfileComplete: boolean;

  @OneToMany(type => ManagerUser, managerUser => managerUser.mentee)
  managerUsers: ManagerUser[];

  @OneToMany(type => Brief, briefs => briefs.mentee)
  briefs: Brief[];

  @OneToMany(type => Request, requests => requests.mentee)
  requests: Request[];

  @OneToMany(type => ProgramApplication, programApplication => programApplication.mentee)
  programApplications: ProgramApplication[];
}
