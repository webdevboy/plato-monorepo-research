# DB package

Database manager using TypeORM.

## Setup

### Local environment configuration

Create the `/packages/db/.env` file and add the following value according to your machine.
```
TYPEORM_CONNECTION = postgres
TYPEORM_URL = postgres://username:password@localhost:5432/plato_test
TYPEORM_SYNCHRONIZE = false
TYPEORM_LOGGING = true
TYPEORM_CACHE = true
TYPEORM_ENTITIES = ./**/entities/*.entity.ts
```

### Start Postgres

```
brew services postgres start
```

### Create a local database and load the schema

```
createdb plato_test
cd packages/db
yarn typeorm query 'CREATE EXTENSION IF NOT EXISTS "uuid-ossp";'
yarn typeorm schema:sync
```

To populate the database with random data
```
yarn fixtures -d src/fixtures/ -c ormconfig.yml
```

To load the schema and populate the database with random data use `-s` option
```
yarn fixtures -d src/fixtures/ -c ormconfig.yml -s
```

### Run a raw SQL query

```
yarn typeorm query SELECT * FROM users
```

### Drop the schema

```
yarn typeorm schema:drop
```

## Links

[TypeORM documentation](https://typeorm.io/#/)

[TypeORM fixtures documentation](https://github.com/RobinCK/typeorm-fixtures)
