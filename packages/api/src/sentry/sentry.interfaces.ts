import * as Sentry from '@sentry/types';

export type ISentryFilterFunction = (exception: any) => () => boolean;

export interface ISentryInterceptorOptionsFilter {
  type: any;
  filter?: ISentryFilterFunction;
}

export interface IRavenInterceptorOptions {
  filters?: ISentryInterceptorOptionsFilter[];
  tags?: { [key: string]: string };
  extra?: { [key: string]: any };
  fingerprint?: string[];
  level?: Sentry.Severity;
}
