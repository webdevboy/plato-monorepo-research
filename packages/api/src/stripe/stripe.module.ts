import { ConfigModule } from '@plato/core';
import { Module } from '@nestjs/common';
import { StripeService } from './stripe.service';
import { Subscription } from '@plato/db';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  providers: [StripeService],
  exports: [StripeService],
  imports: [ConfigModule, TypeOrmModule.forFeature([Subscription])],
})
export class StripeModule {}
