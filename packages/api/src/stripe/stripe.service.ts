import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { Logger } from '@nestjs/common/services/logger.service';
import { InjectRepository } from '@nestjs/typeorm';
import { ConfigService } from '@plato/core';
import { Subscription } from '@plato/db';
import Stripe from 'stripe';
import { Repository } from 'typeorm';

import { SubscriptionUpdatedDTO } from './stripe.models';

/**
 * A service wrapper around the Stripe library.
 */
@Injectable()
export class StripeService {
  /**
   * An instance of the Stripe client.
   */
  private readonly stripe: Stripe = new Stripe(this.configService.get('STRIPE_SECRET_KEY'));
  private readonly webhookSecret: string = this.configService.get(
    'STRIPE_SUBSCRIPTION_WEBHOOK_SECRET'
  );

  /** @ignore */
  constructor(
    private readonly configService: ConfigService,
    @InjectRepository(Subscription)
    private readonly subscriptionRepository: Repository<Subscription>
  ) {}

  /**
   * Creates a Stripe customer.
   * @param options Stripe customer creation options.
   */
  createCustomer(
    options: Stripe.customers.ICustomerCreationOptions
  ): Promise<Stripe.customers.ICustomer> {
    return this.stripe.customers.create(options);
  }

  /**
   * Handle a subscription updated webhook request.
   * @param options A set of subscription data from Stripe.
   */
  async handleSubscriptionUpdated(
    stripeHeaderSignature: string,
    subscriptionUpdatedDTO: SubscriptionUpdatedDTO
  ) {
    const rawBody = 'raw body';

    try {
      this.stripe.webhooks.constructEvent(rawBody, stripeHeaderSignature, this.webhookSecret);
    } catch (error) {
      Logger.error(error.message, error.stack, StripeService.name, true);
      throw new HttpException('Forbidden', HttpStatus.FORBIDDEN);
    }

    // Retrieve the subscription for the customer ID and update the Stripe subscription ID.
    const subscription = await this.subscriptionRepository.findOne({
      customerStripeId: subscriptionUpdatedDTO.customer,
    });
    subscription.stripeId = subscriptionUpdatedDTO.id;
    this.subscriptionRepository.save(subscription);
  }
}
