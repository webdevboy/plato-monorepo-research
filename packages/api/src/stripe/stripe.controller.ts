import { Body, Controller, Headers, Post } from '@nestjs/common';
import { ApiCreatedResponse } from '@nestjs/swagger';
import { SubscriptionUpdatedDTO } from './stripe.models';
import { StripeService } from './stripe.service';

/** @ignore */
@Controller('stripe')
export class StripeController {
  constructor(private readonly stripeService: StripeService) {}

  /**
   * Receive a call from Stripe for the event "customer.subscrition.updated".
   * @see {@link https://stripe.com/docs/webhooks}
   * @see {@link https://stripe.com/docs/api}
   */
  @Post('/webhook')
  @ApiCreatedResponse({ description: 'Handle Stripe webhook for an updated subscription.' })
  async handleSubscriptionUpdated(
    @Headers('stripe-signature') stripeHeaderSignature: string,
    @Body() subscriptionUpdatedDTO: SubscriptionUpdatedDTO
  ): Promise<string> {
    await this.stripeService.handleSubscriptionUpdated(
      stripeHeaderSignature,
      subscriptionUpdatedDTO
    );
    return 'ok';
  }
}
