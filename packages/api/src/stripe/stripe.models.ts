import { IsNotEmpty } from 'class-validator';

/**
 * DTO for subscription updated webhook.
 */
export class SubscriptionUpdatedDTO {
  /** The customer ID of the subscription. */
  @IsNotEmpty()
  readonly customer: string;

  /** The subscription ID. */
  @IsNotEmpty()
  readonly id: string;

  /** The status of the subscription. */
  @IsNotEmpty()
  readonly status: string;
}
