import 'sqreen';
import 'newrelic';
import 'reflect-metadata';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import * as Sentry from '@sentry/node';
import { ApplicationModule } from './application.module';
import { SentryInterceptor } from './sentry';

/**
 * Generates and serves Swagger API documentation at /api-docs.
 * @param app The Nest app to generate documentation for.
 */
function setupSwagger(app: INestApplication) {
  const options = new DocumentBuilder()
    .setTitle('Plato API')
    .setDescription('Plato core API documentation')
    .setVersion(process.env.npm_package_version)
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api-docs', app, document);
}

/**
 * Setup Sentry.
 */
function setupSentry() {
  Sentry.init({ dsn: 'https://f8d00882693c4569a685b0497270b9e3@sentry.io/1453787' });
}

/**
 * Starts the API.
 */
async function bootstrap() {
  const app = await NestFactory.create(ApplicationModule);
  app.enableCors({
    origin: '*',
    methods: '*',
    credentials: true,
  });
  setupSwagger(app);
  setupSentry();

  app.enableCors();
  app.setGlobalPrefix('v1');
  app.useGlobalPipes(new ValidationPipe({ whitelist: true, forbidNonWhitelisted: true }));
  app.useGlobalInterceptors(new SentryInterceptor());

  await app.listen(process.env.PORT, '0.0.0.0');
}

bootstrap();
