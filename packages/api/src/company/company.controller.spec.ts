import { Test, TestingModule } from '@nestjs/testing';
import { getConnectionToken, getRepositoryToken } from '@nestjs/typeorm';
import { ConfigModule } from '@plato/core';
import { Company, Subscription } from '@plato/db';

import { AnalyticsModule } from '../analytics';
import { HashModule } from '../hash';
import { StripeModule } from '../stripe';
import { CompanyController } from './company.controller';
import { companiesFixtures } from './company.fixtures';
import { CompanyService } from './company.service';

describe('Company Controller', () => {
  let companyModule: TestingModule;
  let companyService: CompanyService;
  let companyController: CompanyController;

  beforeAll(async () => {
    companyModule = await Test.createTestingModule({
      imports: [AnalyticsModule, HashModule, StripeModule, ConfigModule],
      controllers: [CompanyController],
      providers: [
        CompanyService,
        {
          provide: getRepositoryToken(Company),
          useValue: {},
        },
        {
          provide: getRepositoryToken(Subscription),
          useValue: {},
        },
        {
          provide: getConnectionToken(),
          useValue: {},
        },
      ],
    })
      .overrideProvider(getRepositoryToken(Company))
      .useValue({})
      .overrideProvider(getRepositoryToken(Subscription))
      .useValue({})
      .overrideProvider(getConnectionToken())
      .useValue({})
      .compile();

    companyService = companyModule.get<CompanyService>(CompanyService);
    companyController = companyModule.get<CompanyController>(CompanyController);
  });

  afterAll(() => companyModule.close());

  it('should be defined', () => {
    expect(companyService).toBeDefined();
  });

  describe('get', () => {
    it('should return a company object', async () => {
      jest.spyOn(companyService, 'get').mockResolvedValue(companiesFixtures[0]);
      await expect(companyController.get(companiesFixtures[0].id)).resolves.toBe(
        companiesFixtures[0]
      );
    });
  });

  describe('list', () => {
    it('should return an array of companies', async () => {
      jest.spyOn(companyService, 'list').mockResolvedValue(companiesFixtures);
      await expect(companyController.list()).resolves.toBe(companiesFixtures);
    });
  });

  describe('create', () => {
    it('should return the created company', async () => {
      jest.spyOn(companyService, 'create').mockResolvedValue(companiesFixtures[0]);
      expect(await companyController.create(companiesFixtures[0] as any)).toBe(
        companiesFixtures[0]
      );
    });
  });
});
