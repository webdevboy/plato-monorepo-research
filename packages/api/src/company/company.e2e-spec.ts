import { INestApplication } from '@nestjs/common';
import { Test } from '@nestjs/testing';
import { getConnectionToken, getRepositoryToken } from '@nestjs/typeorm';
import { ConfigModule } from '@plato/core';
import { Company, Subscription } from '@plato/db';
import * as supertest from 'supertest';

import { CompanyController, CompanyService } from '.';
import { AnalyticsModule } from '../analytics';
import { HashModule } from '../hash';
import { StripeModule } from '../stripe';
import { companiesFixtures } from './company.fixtures';

describe('Company module e2e tests', () => {
  let app: INestApplication;
  const companyService = {
    list: () => companiesFixtures,
    get: (id: number) => companiesFixtures.find(company => company.id === id),
  };

  beforeAll(async () => {
    const companyModule = await Test.createTestingModule({
      imports: [AnalyticsModule, HashModule, StripeModule, ConfigModule],
      controllers: [CompanyController],
      providers: [
        CompanyService,
        {
          provide: getRepositoryToken(Company),
          useValue: {},
        },
        {
          provide: getRepositoryToken(Subscription),
          useValue: {},
        },
        {
          provide: getConnectionToken(),
          useValue: {},
        },
      ],
    })
      .overrideProvider(CompanyService)
      .useValue(companyService)
      .overrideProvider(getRepositoryToken(Company))
      .useValue({})
      .overrideProvider(getRepositoryToken(Subscription))
      .useValue({})
      .overrideProvider(getConnectionToken())
      .useValue({})
      .compile();

    app = companyModule.createNestApplication();
    await app.init();
  });

  afterAll(() => app.close());

  describe('GET /companies', () => {
    it('should return array of companies', () => {
      return supertest
        .agent(app.getHttpServer())
        .get('/companies')
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200)
        .expect({ data: companyService.list() });
    });
  });

  describe('GET /companies/:id', () => {
    it('should return object of company', async () => {
      const companyId = companiesFixtures[0].id;
      return supertest
        .agent(app.getHttpServer())
        .get(`/companies/${companyId}`)
        .set('Accept', 'application/json')
        .expect(200)
        .expect(companyService.get(companyId));
    });
  });
});
