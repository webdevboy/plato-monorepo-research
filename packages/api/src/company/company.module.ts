import { AnalyticsModule } from '../analytics';
import { CompanyController } from './company.controller';
import { CompanyService } from './company.service';
import { HashModule } from '../hash';
import { Module } from '@nestjs/common';
import { StripeModule } from '../stripe';
import { ConfigModule } from '@plato/core';

@Module({
  providers: [CompanyService],
  controllers: [CompanyController],
  imports: [AnalyticsModule, ConfigModule, HashModule, StripeModule],
  exports: [CompanyService],
})
export class CompanyModule {}
