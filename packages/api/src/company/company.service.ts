import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectConnection, InjectRepository } from '@nestjs/typeorm';
import { ConfigService } from '@plato/core';
import { Company, Subscription } from '@plato/db';
import Stripe from 'stripe';
import { Connection, Repository } from 'typeorm';

import { AnalyticsService } from '../analytics';
import { HashService } from '../hash';
import { StripeService } from '../stripe';
import { CompanyRTO, CreateCompanyDTO } from './company.models';

/**
 * A service for managing companies.
 */
@Injectable()
export class CompanyService {
  /** @ignore */
  constructor(
    @InjectRepository(Company)
    private readonly companyRepository: Repository<Company>,
    @InjectConnection()
    private readonly connection: Connection,
    private readonly analyticsService: AnalyticsService,
    private readonly hashService: HashService,
    private readonly stripeService: StripeService,
    private readonly configService: ConfigService
  ) {}

  /**
   * Retrieves a list of all companies.
   * @returns A list of companies that match the find options.
   */
  async list(): Promise<Company[]> {
    return this.companyRepository.find();
  }

  /**
   * Creates a company in the database with corresponding Stripe data.
   * @param createCompanyDTO An instance of CreateCompanyDTO.
   * @returns The created company.
   */
  async create(createCompanyDTO: CreateCompanyDTO): Promise<Company> {
    return this.connection.transaction(async manager => {
      const {
        email,
        name,
        isKeyAccount,
        location,
        nbEmployees,
        nbTechManagers,
        nbEngineers,
        description,
      } = createCompanyDTO;

      const company = new Company();
      company.name = name;
      company.isKeyAccount = isKeyAccount;
      company.location = location;
      company.nbEmployees = nbEmployees;
      company.nbTechManagers = nbTechManagers;
      company.nbEngineers = nbEngineers;
      company.description = description;
      company.createdAt = new Date(); // @todo remove
      company.updatedAt = new Date(); // @todo remove
      await manager.save(company);

      const stripeCustomer = await this.createCompanyStripeCustomer(company, email);
      const subscription = new Subscription();
      subscription.company = company;
      subscription.customerStripeId = stripeCustomer.id;
      subscription.createdAt = new Date(); // @todo remove
      subscription.updatedAt = new Date(); // @todo remove
      await manager.save(subscription);

      this.analyticsService.trackCompanyCreated(company);
      return company;
    });
  }

  /**
   * Retrieves a single company, or fails.
   * @param id A company ID.
   * @returns A single company.
   * @throws {EntityNotFound} If the company is not found.
   */
  async get(id: number): Promise<CompanyRTO> {
    const company: CompanyRTO = await this.companyRepository.findOneOrFail(id, {
      relations: ['subscriptions'],
    });

    const slackUrl = this.getSlackUrl(id);
    const stripeUrl =
      company.subscriptions.length > 0 && company.subscriptions[0].customerStripeId
        ? this.getStripeUrl(company.subscriptions[0].customerStripeId)
        : null;

    return { ...company, slackUrl, stripeUrl };
  }

  /**
   * Attempts to create a customer record in Stripe for the given company.
   * @param company A company to create a corresponding Stripe customer record for.
   * @param email The email address of the person at the company in charge of paying us.
   * @throws {HttpException} If an error occurs trying to communicate with Stripe.
   */
  private createCompanyStripeCustomer(
    company: Company,
    email: string
  ): Promise<Stripe.customers.ICustomer> {
    try {
      // We have to use `as any` for the time being since the
      // @types/stripe package isn't up-to-date with the new
      // `name` property yet.
      return this.stripeService.createCustomer({
        name: company.name,
        email,
        description: company.name,
        metadata: { company_id: company.id },
      } as any);
    } catch (error) {
      throw new HttpException(
        `An error occurred while creating a Stripe customer: ${error.message}`,
        HttpStatus.INTERNAL_SERVER_ERROR
      );
    }
  }

  /**
   * Returns a Slack bot installation URL for a given company.
   * @param companyId A company ID.
   * @returns The Slack bot installation URL.
   */
  private getSlackUrl(companyId: number): string {
    const hashedCompanyId = this.hashService.encode(companyId);
    return `https://slack.com/oauth/pick_reflow?client_id=${this.configService.get(
      'SLACK_CLIENT_ID'
    )}&scope=bot%2Cteam%3Aread%2Cgroups%3Awrite&state=${hashedCompanyId}`;
  }

  /**
   * Returns a Stripe payment URL for internal use.
   * @param customerId A Stripe customer ID.
   * @returns The Stripe URL of the company.
   */
  private getStripeUrl(customerId: string): string {
    return `${this.configService.get('STRIPE_DASHBOARD_URL')}/customers/${customerId}`;
  }
}
