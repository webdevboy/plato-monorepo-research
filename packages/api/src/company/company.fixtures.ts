import { Company } from '@plato/db';

import { fixturesGenerator } from '../testing';

const companiesFixtures: Company[] = fixturesGenerator<Company>({
  id: '{{random.number}}',
  name: '{{company.companyName}}',
  location: '{{address.state}}',
  description: '{{lorem.sentence}}',
  nbEmployees: '{{random.number}}',
  nbEngineers: '{{random.number}}',
  nbTechManagers: '{{random.number}}',
  notes: '{{lorem.sentence}}',
  createdAt: '{{date.past}}',
  updatedAt: '{{date.past}}',
});

export { companiesFixtures };
