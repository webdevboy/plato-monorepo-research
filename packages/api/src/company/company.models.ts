import { Company } from '@plato/db';
import { IsEmail, IsNotEmpty, IsOptional } from 'class-validator';

/**
 * DTO for company creation.
 */
export class CreateCompanyDTO {
  /** The name of the company. */
  @IsNotEmpty()
  readonly name: string;

  /** The accounting email for the company. */
  @IsNotEmpty()
  @IsEmail()
  readonly email: string;

  /** Whether the company is a key account. */
  @IsOptional()
  readonly isKeyAccount: boolean = false;

  /** The location of the company. */
  @IsNotEmpty()
  readonly location: string;

  /** The number of employees of the company. */
  @IsNotEmpty()
  readonly nbEmployees: number;

  /** The number of technical managers of the company. */
  @IsNotEmpty()
  readonly nbTechManagers: number;

  /** The number of engineers of the company. */
  @IsNotEmpty()
  readonly nbEngineers: number;

  /** A description of the company. */
  @IsNotEmpty()
  readonly description: string;
}

/**
 * RTO for a company.
 */
export class CompanyRTO extends Company {
  /** The Slack installation URL. */
  slackUrl?: string;

  /** The Stripe URL. */
  stripeUrl?: string;
}
