import { Body, Controller, Get, Param, Post, UseInterceptors } from '@nestjs/common';
import { ApiCreatedResponse, ApiOkResponse } from '@nestjs/swagger';
import { Company } from '@plato/db';

import { FormatListResponseInterceptor } from '../interceptors';
import { CompanyRTO, CreateCompanyDTO } from './company.models';
import { CompanyService } from './company.service';

/** @ignore */
@Controller('companies')
export class CompanyController {
  constructor(private readonly companyService: CompanyService) {}

  @Get()
  @ApiOkResponse({ description: 'Returns a list of all companies.' })
  @UseInterceptors(FormatListResponseInterceptor)
  list(): Promise<Company[]> {
    return this.companyService.list();
  }

  @Post()
  @ApiCreatedResponse({ description: 'Creates a company.' })
  create(@Body() createCompanyDTO: CreateCompanyDTO): Promise<Company> {
    return this.companyService.create(createCompanyDTO);
  }

  @Get('/:id')
  @ApiOkResponse({ description: 'Returns a single company.' })
  get(@Param('id') id: number): Promise<CompanyRTO> {
    return this.companyService.get(id);
  }
}
