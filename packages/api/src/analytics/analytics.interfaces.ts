/**
 * Interface to hold relevant data for sending Segment events for a
 * company.
 */
export interface ITrackedCompany {
  /** The company id. */
  id: number;

  /** The company name. */
  name: string;
}
