import { AnalyticsService } from './analytics.service';
import { ConfigModule } from '@plato/core';
import { Module } from '@nestjs/common';

@Module({
  providers: [AnalyticsService],
  exports: [AnalyticsService],
  imports: [ConfigModule],
})
export class AnalyticsModule {}
