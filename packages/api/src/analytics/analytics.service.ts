import Analytics from 'analytics-node';
import { ITrackedCompany } from './analytics.interfaces';
import { Injectable } from '@nestjs/common';
import { ConfigService } from '@plato/core';

/**
 * A service for managing analytics.
 */
@Injectable()
export class AnalyticsService {
  /** An instance of the Segment analytics class. */
  private readonly analytics: Analytics = new Analytics(
    this.configService.get('SEGMENT_WRITE_KEY')
  );

  /** A prefix to prepend to all keys in a tracked object when sent to Segment. */
  private readonly PROPERTY_PREFIX: string = 'ep_';

  /** Initializes the service with the SEGMENT_WRITE_KEY environment variable. */
  constructor(private readonly configService: ConfigService) {}

  /**
   * Sends a Segment track event.
   * @param event The name of the event. It should match the Segment
   *    event naming conventions.
   * @param properties Object whose data should go into the properties
   *    object. The keys in this object will be prepended with a
   *    property prefix to differentiate them from metadata.
   */
  track(event: string, properties: object) {
    const prefixedProperties = Object.entries(properties).reduce(
      (acc: object, [key, value]: [string, object]) => ({
        ...acc,
        [`${this.PROPERTY_PREFIX}${key}`]: value,
      }),
      {}
    );
    this.analytics.track({
      // TODO:  Get user id instead of using anonymous id here
      anonymousId: 1,
      event,
      properties: {
        actor_id: '',
        actor_role: '',
        actor_source: '',
        is_originator: false,
        originator_id: '',
        originator_role: '',
        track_common_id: '',
        user_role: '',
        ...prefixedProperties,
      },
    });
  }

  /**
   * Sends a Segment track event to indicate company creation.
   * @param {ITrackedCompany} company The created company.
   */
  trackCompanyCreated(company: ITrackedCompany): void {
    this.track('Company created', {
      company_id: company.id,
      company_name: company.name,
    });
  }
}
