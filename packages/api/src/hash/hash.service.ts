import HashIds from 'hashids';
import { Injectable } from '@nestjs/common';

/**
 * A service for hashing IDs.
 */
@Injectable()
export class HashService {
  /**
   * An instance of the hashids library used to generate our hashes
   */
  hashIds: HashIds;

  /** @ignore */
  constructor() {
    const salt = '6cbv^2xUJZnXP0`w8Z$`.hzK.L!Sl)u%pus!msy0{+ZaMw*9v,QS#Dl*nNW.H7=y';
    const alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    const minLength = 10;

    this.hashIds = new HashIds(salt, minLength, alphabet);
  }

  /**
   * Hashes the given number.
   * @param id A non-negative number to hash.
   */
  encode(id: number): string {
    return this.hashIds.encode(id);
  }

  /**
   * Decodes a hash back into a number.
   * @param hash A hashed value to convert back into a number.
   */
  decode(hash: string): number {
    return this.hashIds.decode(hash)[0];
  }
}
