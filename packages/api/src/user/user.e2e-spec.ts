import { INestApplication } from '@nestjs/common';
import { Test } from '@nestjs/testing';
import * as supertest from 'supertest';

import { usersFixtures } from './user.fixtures';
import { UserModule } from './user.module';
import { UserService } from './user.service';

describe('User module e2e tests', () => {
  let app: INestApplication;
  const userService = {
    list: () => usersFixtures,
    get: (id: number) => usersFixtures.find(user => user.id === id),
    create: (id: number) => usersFixtures.find(user => user.id === id),
  };

  beforeAll(async () => {
    const userModule = await Test.createTestingModule({
      imports: [UserModule],
    })
      .overrideProvider(UserService)
      .useValue(userService)
      .compile();

    app = userModule.createNestApplication();
    await app.init();
  });

  afterAll(() => app.close());

  describe('GET /users', () => {
    it('should return array of users', () => {
      return supertest
        .agent(app.getHttpServer())
        .get('/users')
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200)
        .expect({ data: userService.list() });
    });
  });

  describe('GET /users/:id', () => {
    it('should return object of user', async () => {
      const userId = usersFixtures[0].id;
      return supertest
        .agent(app.getHttpServer())
        .get(`/users/${userId}`)
        .set('Accept', 'application/json')
        .expect(200)
        .expect(userService.get(userId));
    });
  });
});
