import { IsEmail, IsNotEmpty, IsOptional } from 'class-validator';

/** @ignore */
interface IDashboardUser {
  /** A company ID. */
  companyId: number;
}

/**
 * DTO for creating a user.
 */
export class CreateUserDTO {
  /** The user's first name. */
  @IsNotEmpty()
  readonly firstName: string;

  /** The user's last name. */
  @IsNotEmpty()
  readonly lastName: string;

  /** The user's email address. */
  @IsNotEmpty()
  @IsEmail()
  readonly email: string;

  /** The user's email address. */
  @IsOptional()
  readonly password: string;

  /** Data for a dashboard user to be created. */
  @IsOptional()
  readonly dashboardUser: IDashboardUser;

  /** Indicates whether we want to create a new mentee */
  @IsOptional()
  readonly newMentee: boolean;

  /** LinkedIn id of the user */
  @IsOptional()
  readonly linkedinId: string;

  /** Google id of the user */
  @IsOptional()
  readonly googleId: string;
}

export class UpdateUserDTO {
  /** The user's first name. */
  @IsNotEmpty()
  readonly id: number;

  /** The user's first name. */
  @IsNotEmpty()
  readonly firstName: string;

  /** The user's last name. */
  @IsNotEmpty()
  readonly lastName: string;

  /** The user's email address. */
  @IsNotEmpty()
  @IsEmail()
  readonly email: string;

  /** Data for a dashboard user to be created. */
  @IsOptional()
  readonly dashboardUser: IDashboardUser;

  /** LinkedIn id of the user */
  @IsOptional()
  readonly linkedinId: string;

  /** Google id of the user */
  @IsOptional()
  readonly googleId: string;
}
