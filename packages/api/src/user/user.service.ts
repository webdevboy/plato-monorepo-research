import { FindManyOptions, Repository } from 'typeorm';

import { CreateUserDTO, UpdateUserDTO } from './user.models';
import { InjectRepository } from '@nestjs/typeorm';
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { DashboardUser, ManagerUser, Mentee, User } from '@plato/db';
import { plainToClass } from 'class-transformer';

/**
 * A service for managing users.
 */
@Injectable()
export class UserService {
  /** @ignore */
  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
    @InjectRepository(DashboardUser)
    private readonly dashboardUserRepository: Repository<DashboardUser>,
    @InjectRepository(ManagerUser)
    private managerUserRepository: Repository<ManagerUser>,
    @InjectRepository(Mentee)
    private menteeRepository: Repository<Mentee>
  ) { }

  /**
   * Retrieves a list of all users.
   * @returns A list of all users.
   */
  list(): Promise<User[]> {
    return this.userRepository.find({
      select: ['email', 'firstName', 'id', 'lastName', 'timeZone', 'uuid'],
      relations: ['managerUser', 'dashboardUser'],
    });
  }

  /**
   * Retrieves a single user, or fails.
   * @param id A user ID.
   * @param options Find options.
   * @returns A single user.
   * @throws {EntityNotFound} If the user is not found.
   */
  get(id: number, options?: FindManyOptions<User>): Promise<User> {
    return this.userRepository.findOneOrFail(id, options);
  }

  /**
   * Finds a single user, or fails.
   * @param id A user ID.
   * @param options Find options.
   * @returns A single user.
   * @throws {EntityNotFound} If the user is not found.
   */
  find(options): Promise<User> {
    return this.userRepository.findOne(options);
  }

  /**
   * Creates a user.
   * @param createUserDTO The create user DTO.
   * @returns The created user.
   */
  async create(createUserDTO: CreateUserDTO): Promise<User> {
    const user = new User();
    user.firstName = createUserDTO.firstName;
    user.lastName = createUserDTO.lastName;
    user.email = createUserDTO.email;

    if (createUserDTO.dashboardUser) {
      const createdDashboardUser = new DashboardUser();
      createdDashboardUser.companyId = createUserDTO.dashboardUser.companyId;
      createdDashboardUser.users = [user];
      await this.dashboardUserRepository.save(createdDashboardUser);
    }

    if (createUserDTO.newMentee) {
      if (createUserDTO.googleId) {
        user.googleId = createUserDTO.googleId;
      }
      if (createUserDTO.linkedinId) {
        user.linkedinId = createUserDTO.linkedinId;
      }

      const newManagerUser = new ManagerUser();
      const createdMentee = await this.menteeRepository.save(new Mentee());
      createdMentee.managerUsers = [newManagerUser];

      const createdManagerUser = await this.managerUserRepository.save(newManagerUser);
      createdManagerUser.users = [user];
      await this.managerUserRepository.save(createdManagerUser);

      user.isEnabled = true;
    }

    await this.userRepository.save(user);

    return this.userRepository.findOne(user.id, {
      select: ['email', 'firstName', 'id', 'lastName', 'timeZone', 'uuid'],
      relations: ['managerUser', 'dashboardUser'],
    });
  }

  /**
   * Updates a user.
   * @param updateUserDTO The update user DTO.
   * @returns The updated user.
   */
  async update(updateUserDTO: UpdateUserDTO): Promise<User> {
    const user = await this.userRepository.findOne(updateUserDTO.id);
    if (user) {
      return await this.userRepository.save(plainToClass(User, { ...user, ...updateUserDTO }));
    } else {
      throw new HttpException(
        `User not found. An error occurred while updating user with an id ${updateUserDTO.id}`,
        HttpStatus.INTERNAL_SERVER_ERROR
      );
    }
  }
}
