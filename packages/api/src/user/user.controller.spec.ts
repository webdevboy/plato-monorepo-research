import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { DashboardUser, User } from '@plato/db';

import { UserController, UserService } from '.';
import { usersFixtures } from './user.fixtures';

describe('User Controller', () => {
  let userModule: TestingModule;
  let userService: UserService;
  let userController: UserController;

  beforeAll(async () => {
    userModule = await Test.createTestingModule({
      imports: [],
      exports: [UserService],
      controllers: [UserController],
      providers: [
        UserService,
        {
          provide: getRepositoryToken(User),
          useValue: {},
        },
        {
          provide: getRepositoryToken(DashboardUser),
          useValue: {},
        },
      ],
    })
      .overrideProvider(getRepositoryToken(User))
      .useValue({})
      .overrideProvider(getRepositoryToken(DashboardUser))
      .useValue({})
      .compile();

    userService = userModule.get<UserService>(UserService);
    userController = userModule.get<UserController>(UserController);
  });

  afterAll(() => userModule.close());

  it.skip('Should be defined', () => {
    expect(userService).toBeDefined();
  });

  describe('get', () => {
    it.skip('Should return a user object', async () => {
      jest.spyOn(userService, 'get').mockResolvedValue(usersFixtures[0]);
      expect(await userController.get(usersFixtures[0].id)).toBe(usersFixtures[0]);
    });
  });

  describe('list', () => {
    it.skip('Should return an array of users', async () => {
      jest.spyOn(userService, 'list').mockResolvedValue(usersFixtures);
      expect(await userController.list()).toBe(usersFixtures);
    });
  });

  describe('create', () => {
    it.skip('Should create a user object', async () => {
      jest.spyOn(userService, 'create').mockResolvedValue(usersFixtures[0]);
      expect(await userController.create(usersFixtures[0] as any)).toBe(usersFixtures[0]);
    });
  });
});
