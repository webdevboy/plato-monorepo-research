import { Body, Controller, Get, Param, Post, UseInterceptors } from '@nestjs/common';

import { CreateUserDTO } from './user.models';
import { User } from '@plato/db';
import { UserService } from './user.service';
import { FormatListResponseInterceptor } from '../interceptors';

/** @ignore */
@Controller('users')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Get()
  @UseInterceptors(FormatListResponseInterceptor)
  list(): Promise<User[]> {
    return this.userService.list();
  }

  @Post()
  create(@Body() createUserDTO: CreateUserDTO): Promise<User> {
    return this.userService.create(createUserDTO);
  }

  @Get('/:id')
  get(@Param('id') id: any): Promise<User> {
    return this.userService.get(id);
  }
}
