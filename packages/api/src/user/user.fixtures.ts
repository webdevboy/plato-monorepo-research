import { User } from '@plato/db';

import { fixturesGenerator } from '../testing';

const usersFixtures: User[] = fixturesGenerator<User>({
  id: '{{random.number}}',
  firstName: '{{name.firstName}}',
  lastName: '{{name.lastName}}',
  email: '{{internet.email}}',
  password: '{{internet.password}}',
  enabled: '{{random.boolean}}',
  lastPasswordResetDate: '{{date.recent}}',
  nameOrEmailUpdatedAt: '{{date.recent}}',
  createdAt: '{{date.past}}',
  updatedAt: '{{date.past}}',
  resetPasswordToken: '{{lorem.word}}',
  resetPasswordTokenExpirationDate: '{{date.future}}',
  externalApp: '{{random.boolean}}',
  uuid: '{{random.uuid}}',
  timeZone: '{{lorem.word}}',
});

export { usersFixtures };
