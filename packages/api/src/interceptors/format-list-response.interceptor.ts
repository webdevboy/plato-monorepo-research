import { Injectable, NestInterceptor, ExecutionContext, CallHandler } from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

/**
 * Format of a response holding a list of objects.
 */
export class Response<T> {
  data: T;
  hasMore: boolean;
}

/**
 * Receptor to automatically format a response holding a list of objects.
 */
@Injectable()
export class FormatListResponseInterceptor<T> implements NestInterceptor<T, Response<T>> {
  intercept(context: ExecutionContext, next: CallHandler): Observable<Response<T>> {
    const request = context.switchToHttp().getRequest();
    return next.handle().pipe(
      map(data => ({
        data,
        hasMore: request.query.limit && data.length >= +request.query.limit,
      }))
    );
  }
}
