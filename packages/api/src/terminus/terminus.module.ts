import { TerminusModule, TerminusModuleOptions, TypeOrmHealthIndicator } from '@nestjs/terminus';

import { Module } from '@nestjs/common';

/**
 * Returns configuration options for Terminus.
 * @param db A TypeOrm health indicator.
 */
const getTerminusOptions = (db: TypeOrmHealthIndicator): TerminusModuleOptions => ({
  endpoints: [
    {
      // The health check will be available with /health
      url: '/health',
      // All the indicator which will be checked when requesting /health
      healthIndicators: [
        // Set the timeout for a response to 300ms
        async () => db.pingCheck('database', { timeout: 300 }),
      ],
    },
  ],
});

@Module({
  imports: [
    TerminusModule.forRootAsync({
      inject: [TypeOrmHealthIndicator],
      useFactory: getTerminusOptions,
    }),
  ],
})
export class HealthModule {}
