import { CompressionMiddleware } from '@nest-middlewares/compression';
import { HelmetMiddleware } from '@nest-middlewares/helmet';
import { ResponseTimeMiddleware } from '@nest-middlewares/response-time';
import { MiddlewareConsumer, Module, NestModule, RequestMethod } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@plato/core';
import { DatabaseModule } from '@plato/db';

import { AuthenticationModule } from './authentication';
import { CompanyModule } from './company';
import { HealthModule } from './terminus';
import { UserModule } from './user';

@Module({
  imports: [DatabaseModule, HealthModule, CompanyModule, UserModule, ConfigModule, AuthenticationModule],
  providers: [ConfigService],
})
export class ApplicationModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(HelmetMiddleware).forRoutes({ path: '*', method: RequestMethod.ALL });
    consumer.apply(ResponseTimeMiddleware).forRoutes({ path: '*', method: RequestMethod.ALL });
    consumer.apply(CompressionMiddleware).forRoutes({ path: '*', method: RequestMethod.ALL });
  }
}
