import { Test, TestingModule } from '@nestjs/testing';
import { AuthenticationController } from './authentication.controller';
import { AuthenticationService } from './authentication.service';
import { AuthenticationModule } from './authentication.module';
import { registerMockData } from './authentication.fixtures';
import { DatabaseModule } from '@plato/db';
import { HealthModule } from '../terminus';

describe('Authentication Controller', () => {
  let service: AuthenticationService;
  let authenticationController: AuthenticationController;
  let registerSpy;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [DatabaseModule, HealthModule, AuthenticationModule],
      controllers: [AuthenticationController],
      providers: [AuthenticationService],
    }).compile();

    service = module.get<AuthenticationService>(AuthenticationService);
    authenticationController = module.get<AuthenticationController>(AuthenticationController);
    registerSpy = jest.spyOn(service, 'register');
  });

  it('Should be defined', () => {
    expect(service).toBeDefined();
  });

  it('Should register user', async () => {
    registerSpy.mockImplementation(() => registerMockData);
    expect(
      await authenticationController.register({
        body: { email: 'test@test.com', password: '12345678' },
      })
    ).toBe(registerMockData);
  });
});
