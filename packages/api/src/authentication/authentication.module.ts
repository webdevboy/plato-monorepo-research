import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthenticationController } from './authentication.controller';
import { AuthenticationService } from './authentication.service';

import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { UserService } from '../user';

import { LinkedInStrategy } from './strategies/linkedin.strategy';
import { JwtStrategy } from './strategies/jwt.strategy';
import { LocalStrategy } from './strategies/local.strategy';
import { GoogleStrategy } from './strategies/google.strategy';

import { User, ManagerUser, Mentee } from '@plato/db';
import { ConfigModule, ConfigService } from '@plato/core';

const config = new ConfigService();

@Module({
  imports: [
    ConfigModule,
    TypeOrmModule.forFeature([User, ManagerUser, Mentee]),

    PassportModule.register({ defaultStrategy: 'jwt' }),
    JwtModule.register({
      secret: config.get('JWT_SECRET_SALT'),
      signOptions: {
        expiresIn: config.get('JWT_TOKEN_EXPIRES_IN'),
      },
    }),
  ],
  controllers: [AuthenticationController],
  providers: [UserService, AuthenticationService, JwtStrategy, LocalStrategy, LinkedInStrategy, GoogleStrategy],
  exports: [PassportModule, AuthenticationService],
})
export class AuthenticationModule { }
