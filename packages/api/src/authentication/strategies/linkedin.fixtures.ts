import * as faker from 'faker';

const userMockData = {
  user: {
    familyName: faker.lorem.word(),
    givenName: faker.lorem.word(),
  },
  emails: [faker.internet.email()],
  id: 1,
};

const mockToken = faker.random.uuid();

export { userMockData, mockToken };
