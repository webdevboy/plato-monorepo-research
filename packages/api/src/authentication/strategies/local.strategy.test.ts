import { Test, TestingModule } from '@nestjs/testing';
import { LocalStrategy } from './local.strategy';
import { DatabaseModule } from '@plato/db';
import { AuthenticationService } from '../authentication.service';
import { AuthenticationModule } from '../authentication.module';
import { userMockData } from './local.fixtures';

describe('Local Strategy', () => {
  let localStrategy: LocalStrategy;
  let authenticationService: AuthenticationService;
  let validateSpy;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [DatabaseModule, AuthenticationModule],
      providers: [LocalStrategy, AuthenticationService],
    }).compile();

    authenticationService = module.get<AuthenticationService>(AuthenticationService);
    localStrategy = module.get<LocalStrategy>(LocalStrategy);
    validateSpy = jest.spyOn(authenticationService, 'login');
  });

  it('Should be defined', () => {
    expect(localStrategy).toBeDefined();
  });

  it('Should validate user', async () => {
    validateSpy.mockImplementation(() => userMockData);
    localStrategy.validate('test@test.com', '123456', (error, result) => {
      expect(result).toBe(userMockData);
    });
  });
});
