import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy } from 'passport-google-oauth20';
import { AuthenticationService } from '../authentication.service';
import { ConfigService } from '@plato/core';

@Injectable()
export class GoogleStrategy extends PassportStrategy(Strategy, 'google') {
  constructor(readonly configService: ConfigService, private readonly authService: AuthenticationService) {
    super({
      clientID: configService.get('GOOGLE_CLIENT_ID'),
      clientSecret: configService.get('GOOGLE_CLIENT_SECRET'),
      callbackURL: `${configService.get('API_URI')}${configService.get('GOOGLE_CALLBACK_URL')}`,
      passReqToCallback: true,
      scope: ['profile', 'email'],
    });
  }

  async validate(request: any, accessToken: string, refreshToken: string, profile, done) {
    try {
      const {
        name: { familyName, givenName },
        emails,
        id,
      } = profile;
      const user = await this.authService.findOrCreateUser(
        { firstName: givenName, lastName: familyName, email: emails[0].value, googleId: id },
        'googleId'
      );

      if (user) {
        const token = this.authService.generateToken(profile.emails[0].value);
        const enrichedUser = { ...user, token };
        done(null, enrichedUser);
      } else {
        done('Unauthorized', false);
      }
    } catch (err) {
      done(err, false);
    }
  }
}
