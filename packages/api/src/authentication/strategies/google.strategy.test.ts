import { Test, TestingModule } from '@nestjs/testing';
import { GoogleStrategy } from './google.strategy';
import { DatabaseModule } from '@plato/db';
import { AuthenticationService } from '../authentication.service';
import { AuthenticationModule } from '../authentication.module';
import { userMockData, mockToken } from './google.fixtures';

describe('Google Strategy', () => {
  let googleStrategy: GoogleStrategy;
  let authenticationService: AuthenticationService;
  let validateSpy;
  let generateTokenSpy;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [DatabaseModule, AuthenticationModule],
      providers: [GoogleStrategy, AuthenticationService],
    }).compile();

    authenticationService = module.get<AuthenticationService>(AuthenticationService);
    googleStrategy = module.get<GoogleStrategy>(GoogleStrategy);
    validateSpy = jest.spyOn(authenticationService, 'findOrCreateUser');
    generateTokenSpy = jest.spyOn(authenticationService, 'generateToken');
  });

  it('Should be defined', () => {
    expect(googleStrategy).toBeDefined();
  });

  it('Should validate user', async () => {
    validateSpy.mockImplementation(() => userMockData);
    generateTokenSpy.mockImplementation(() => mockToken);

    googleStrategy.validate(
      '',
      '',
      '',
      { name: { familyName: 'test', givenName: 'test' }, emails: ['test@test.com'], id: 1 },
      (error, result) => {
        expect(result).toBe({ ...userMockData, token: mockToken });
      }
    );
  });
});
