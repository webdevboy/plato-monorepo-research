import { Test, TestingModule } from '@nestjs/testing';
import { JwtStrategy } from './jwt.strategy';
import { DatabaseModule } from '@plato/db';
import { AuthenticationService } from '../authentication.service';
import { AuthenticationModule } from '../authentication.module';
import { userMockData } from './jwt.fixtures';

describe('Jwt Strategy', () => {
  let jwtStrategy: JwtStrategy;
  let authenticationService: AuthenticationService;
  let validateSpy;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [DatabaseModule, AuthenticationModule],
      providers: [JwtStrategy, AuthenticationService],
    }).compile();

    authenticationService = module.get<AuthenticationService>(AuthenticationService);
    jwtStrategy = module.get<JwtStrategy>(JwtStrategy);
    validateSpy = jest.spyOn(authenticationService, 'findOrCreateUser');
  });

  it('Should be defined', () => {
    expect(jwtStrategy).toBeDefined();
  });

  it('Should validate user', async () => {
    validateSpy.mockImplementation(() => userMockData);
    jwtStrategy.validate({ sub: 'test@test.com' }, (error, result) => {
      expect(result).toBe(userMockData);
    });
  });
});
