import { Test, TestingModule } from '@nestjs/testing';
import { LinkedInStrategy } from './linkedin.strategy';
import { DatabaseModule } from '@plato/db';
import { AuthenticationService } from '../authentication.service';
import { AuthenticationModule } from '../authentication.module';
import { userMockData, mockToken } from './linkedin.fixtures';

describe('LinkedIn Strategy', () => {
  let linkedInStrategy: LinkedInStrategy;
  let authenticationService: AuthenticationService;
  let validateSpy;
  let generateTokenSpy;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [DatabaseModule, AuthenticationModule],
      providers: [LinkedInStrategy, AuthenticationService],
    }).compile();

    authenticationService = module.get<AuthenticationService>(AuthenticationService);
    linkedInStrategy = module.get<LinkedInStrategy>(LinkedInStrategy);
    validateSpy = jest.spyOn(authenticationService, 'findOrCreateUser');
    generateTokenSpy = jest.spyOn(authenticationService, 'generateToken');
  });

  it('Should be defined', () => {
    expect(linkedInStrategy).toBeDefined();
  });

  it('Should validate user', async () => {
    validateSpy.mockImplementation(() => userMockData);
    generateTokenSpy.mockImplementation(() => mockToken);

    linkedInStrategy.validate(
      '',
      '',
      '',
      { name: { familyName: 'test', givenName: 'test' }, emails: ['test@test.com'], id: 1 },
      (error, result) => {
        expect(result).toBe({ ...userMockData, token: mockToken });
      }
    );
  });
});
