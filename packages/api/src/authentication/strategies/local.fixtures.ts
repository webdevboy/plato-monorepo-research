import * as faker from 'faker';

const userMockData = {
  email: faker.internet.email(),
  password: faker.lorem.word(),
};

export { userMockData };
