import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy } from '@sokratis/passport-linkedin-oauth2'; // A fork of https://github.com/auth0/passport-linkedin-oauth2
import { AuthenticationService } from '../authentication.service';
import { ConfigService } from '@plato/core';

const config = new ConfigService();

@Injectable()
export class LinkedInStrategy extends PassportStrategy(Strategy, 'linkedin') {
  constructor(private readonly authService: AuthenticationService) {
    super({
      clientID: config.get('LINKED_IN_CLIENT_ID'),
      clientSecret: config.get('LINKED_IN_CLIENT_SECRET'),
      callbackURL: `${config.get('API_URI')}${config.get('LINKED_IN_CALLBACK_URL')}`,
      passReqToCallback: true,
      scope: ['r_emailaddress', 'r_liteprofile', 'w_member_social'],
    });
  }

  async validate(request: any, accessToken: string, refreshToken: string, profile, done) {
    try {
      const {
        name: { givenName, familyName },
        emails,
        id,
      } = profile;
      const user = await this.authService.findOrCreateUser(
        { firstName: givenName, lastName: familyName, email: emails[0].value, linkedinId: id },
        'linkedinId'
      );

      if (user) {
        const token = this.authService.generateToken(profile.emails[0].value);
        const enrichedUser = { ...user, token };
        done(null, enrichedUser);
      } else {
        done('Unauthorized', false);
      }
    } catch (err) {
      done(err, false);
    }
  }
}
