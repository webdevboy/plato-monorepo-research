import * as faker from 'faker';

const registerMockData = {
  email: faker.internet.email(),
  password: faker.lorem.word(),
  firstName: faker.name.firstName(),
  lastName: faker.name.lastName(),
  lastPasswordResetDate: faker.date.recent().toString(),
  nameOrEmailUpdatedAt: faker.date.recent().toString(),
  resetPasswordToken: faker.lorem.word(),
  resetPasswordTokenExpirationDate: faker.date.recent().toString(),
  linkedinId: faker.random.uuid(),
  googleId: faker.random.uuid(),
  externalApp: faker.random.boolean(),
  timeZone: faker.lorem.word(),
  id: 1,
  createdAt: faker.date.recent().toString(),
  updatedAt: faker.date.recent().toString(),
  uuid: faker.random.uuid(),
  token: faker.random.uuid(),
};

export { registerMockData };
