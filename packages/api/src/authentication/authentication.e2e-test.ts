import * as supertest from 'supertest';
import { Test } from '@nestjs/testing';
import { AuthenticationModule } from './authentication.module';
import { AuthenticationService } from './authentication.service';
import { INestApplication } from '@nestjs/common';
import { registerMockData } from './authentication.fixtures';
import { DatabaseModule } from '@plato/db';
import { HealthModule } from '../terminus';

describe('Authentication module e2e tests', () => {
  const request: any = supertest as any;
  let app: INestApplication;
  const authenticationService = { register: () => registerMockData };

  beforeAll(async () => {
    const module = await Test.createTestingModule({
      imports: [DatabaseModule, HealthModule, AuthenticationModule],
    })
      .overrideProvider(AuthenticationService)
      .useValue(authenticationService)
      .compile();

    app = module.createNestApplication();
    await app.init();
  });

  afterAll(async () => {
    await app.close();
  });

  it('Should register user', () => {
    return request(app.getHttpServer())
      .post('/auth/register')
      .send({
        email: 'test@test.com',
        password: '12345678',
      })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200)
      .expect(authenticationService.register());
  });
});
