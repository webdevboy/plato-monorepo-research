import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { User } from '@plato/db';
import * as bcrypt from 'bcrypt';
import { ConfigService } from '@plato/core';
import { UserService } from '../user';
import { plainToClass } from 'class-transformer';
import { CreateUserDTO } from '../user/user.models';

@Injectable()
export class AuthenticationService {
  constructor(
    private readonly configService: ConfigService,
    private readonly userService: UserService,
    private readonly jwtService: JwtService
  ) { }

  /**
   * Creates a new user (mentee).
   * @param email Email of the user.
   * @param password Password of the user.
   * @returns The new user with his/her token.
   */
  async register(email, password) {
    const user = await this.findByEmail(email);
    if (!user) {
      const newUser = await this.userService.create(plainToClass(CreateUserDTO, { email, password, newMentee: true }));
      if (newUser) {
        const token = this.generateToken(email);
        return { ...newUser, token };
      }
    } else {
      throw new InternalServerErrorException('User already exists', 'Register');
    }
  }

  /**
   * Authenticates a user via email and password.
   * @param email Email of the user.
   * @param password Password of the user.
   * @returns The authenticated user with his/her token.
   */
  async login(email, password) {
    const user = await this.findByEmail(email);

    if (user && (await this.compareHash(password, user.password))) {
      const token = this.generateToken(user.email);
      return { ...user, token };
    } else {
      throw new InternalServerErrorException(
        'Invalid email or password, please try again.',
        'EmailLogin'
      );
    }
  }

  /**
   * Authenticates a user via OAuth.
   * Creates a new user if there is none.
   * Updates a user if a non-OAuth account already exists.
   * @param profile User object received from Google or LinkedIn.
   * @param key The column we want to use to identify the user (email, googleId, or linkedinId).
   * @returns The authenticated user with his/her token.
   */
  async findOrCreateUser(profile, key = 'email') {
    const user = await this.userService.find({ where: [{ [key]: profile[key] }, { email: profile.email }] });
    if (user) {
      if (!user[key]) {
        return await this.userService.update(profile);
      }
      return user;
    }
    return await this.userService.create({ ...profile, newMentee: true });
  }

  /**
   * Finds a user by their email.
   * @param email Email of a user.
   * @returns The user with matching email.
   */
  async findByEmail(email: string): Promise<User> {
    return await this.userService.find({
      where: {
        email,
      },
    });
  }

  /**
   * Compares a string to a hashed password.
   * @param password Password the user submitted.
   * @Param hash The hash stored from the database.
   * @returns Boolean indicating the user submitted the right password.
   */
  async compareHash(password: string | undefined, hash: string | undefined): Promise<boolean> {
    return bcrypt.compare(password, hash);
  }

  /**
   * Generates a new JWT token.
   * @param subject Email of the subject.
   * @returns A valid JWT token.
   */
  generateToken(subject) {
    return this.jwtService.sign({
      sub: subject,
      aud: this.configService.get('JWT_TOKEN_AUD'),
      iss: this.configService.get('JWT_TOKEN_ISS'),
    });
  }
}
