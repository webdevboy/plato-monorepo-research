import { Controller, Get, Post, Req, Res, UseGuards, HttpCode, HttpStatus } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { AuthenticationService } from './authentication.service';
import { ConfigService } from '@plato/core';
import { ApiOkResponse } from '@nestjs/swagger';

@Controller('auth')
export class AuthenticationController {
  constructor(private readonly configService: ConfigService, private readonly authenticationService: AuthenticationService) { }

  @Post('register')
  @ApiOkResponse({ description: 'Creates a user.' })
  @HttpCode(HttpStatus.OK)
  async register(@Req() req) {
    return await this.authenticationService.register(req.body.email, req.body.password);
  }

  @Post('/')
  @ApiOkResponse({ description: 'Returns a user with his/her token.' })
  @UseGuards(AuthGuard('local'))
  @HttpCode(HttpStatus.OK)
  emailLogin(@Req() req) {
    return req.user;
  }

  @Get('linkedin')
  @ApiOkResponse({ description: 'Public endpoint to start the LinkedIn auth flow.' })
  @UseGuards(AuthGuard('linkedin'))
  linkedinLogin() {
    return true;
  }

  @Get('linkedin/callback')
  @ApiOkResponse({ description: 'Returns a user with his/her id and token.' })
  @UseGuards(AuthGuard('linkedin'))
  linkedinLoginCallback(@Req() req, @Res() res) {
    const { id, token } = req.user;
    if (token) {
      res.redirect(`${this.configService.get('WEB_APP_URI')}/oauth/success?userId=${id}&userToken=${token}`);
    } else {
      res.redirect(`${this.configService.get('WEB_APP_URI')}/oauth/failure`);
    }
  }

  @Get('google')
  @ApiOkResponse({ description: 'Public endpoint to start the Google auth flow.' })
  @UseGuards(AuthGuard('google'))
  googleLogin() {
    return true;
  }

  @Get('google/callback')
  @ApiOkResponse({ description: 'Returns a user with his/her id and token.' })
  @UseGuards(AuthGuard('google'))
  googleLoginCallback(@Req() req, @Res() res) {
    const { id, token } = req.user;
    if (token) {
      res.redirect(`${this.configService.get('WEB_APP_URI')}/oauth/success?userId=${id}&userToken=${token}`);
    } else {
      res.redirect(`${this.configService.get('WEB_APP_URI')}/oauth/failure`);
    }
  }
}
