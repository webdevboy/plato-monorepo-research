import * as faker from 'faker';

export function fixturesGenerator<T>(schema: any, min: number = 1, max: number = 10): T[] {
  return Array.from({ length: faker.random.number({ min, max }) }).map(() =>
    Object.keys(schema).reduce((entity: T, key) => {
      entity[key] = faker.fake(schema[key]);
      return entity;
    }, {})
  ) as T[];
}
