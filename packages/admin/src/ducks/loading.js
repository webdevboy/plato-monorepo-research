import { combineActions, handleActions } from 'redux-actions';
import { createUser, createUserCompleted } from './users';
import { createCompany, createCompanyCompleted } from './companies';

export default handleActions(
  {
    [combineActions(createCompany, createUser)]: (state, action) => ({
      ...state,
      [action.type]: true,
    }),

    [combineActions(createCompanyCompleted, createUserCompleted)]: (state, action) => ({
      ...state,
      [action.type.replace(/_COMPLETED$/, '')]: false,
    }),
  },
  {}
);
