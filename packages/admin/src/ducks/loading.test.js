import { createUser, createUserCompleted } from './users';
import { createCompany, createCompanyCompleted } from './companies';
import loadingReducer from './loading';

const defaultState = {};

it('should return the initial state', () => {
  expect(loadingReducer(undefined, {})).toEqual(defaultState);
});

[createUser, createCompany, createUserCompleted, createCompanyCompleted].forEach(action => {
  // eslint-disable-next-line jest/valid-describe
  describe(action, () => {
    it('should show the query loader', () => {
      const isCompleted = !action().type.endsWith('_COMPLETED');
      const result = loadingReducer(undefined, action());
      expect(result).toEqual({
        [action().type.replace('_COMPLETED', '')]: isCompleted,
      });
    });
  });
});
