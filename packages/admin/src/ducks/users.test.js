import reducer, { fetchUsersCompleted } from './users';

const defaultState = {};

it('should return the initial state', () => {
  expect(reducer(undefined, {})).toEqual(defaultState);
});

describe('fetchUsersCompleted', () => {
  it('should set the list of users', () => {
    expect(
      reducer(
        defaultState,
        fetchUsersCompleted({ data: [{ id: 'test-user-id-0' }, { id: 'test-user-id-1' }] })
      )
    ).toEqual({
      'test-user-id-0': { id: 'test-user-id-0' },
      'test-user-id-1': { id: 'test-user-id-1' },
    });
  });

  it('should not override the current state value', () => {
    expect(
      reducer(
        {
          'test-user-id-0': { id: 'test-user-id-0' },
          'test-user-id-1': { id: 'test-user-id-1' },
        },
        fetchUsersCompleted({ data: [{ id: 'test-user-id-2' }, { id: 'test-user-id-3' }] })
      )
    ).toEqual({
      'test-user-id-0': { id: 'test-user-id-0' },
      'test-user-id-1': { id: 'test-user-id-1' },
      'test-user-id-2': { id: 'test-user-id-2' },
      'test-user-id-3': { id: 'test-user-id-3' },
    });
  });

  it('should do nothing on error', () => {
    expect(reducer(defaultState, fetchUsersCompleted(new Error()))).toEqual(defaultState);
  });
});
