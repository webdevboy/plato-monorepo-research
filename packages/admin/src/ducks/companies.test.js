import reducer, { fetchCompaniesCompleted, fetchCompanyCompleted } from './companies';

const defaultState = {};

it('should return the initial state', () => {
  expect(reducer(undefined, {})).toEqual(defaultState);
});

describe('fetchCompaniesCompleted', () => {
  it('should set the list of companies', () => {
    expect(
      reducer(
        defaultState,
        fetchCompaniesCompleted({
          data: [{ id: 'test-company-id-0' }, { id: 'test-company-id-1' }],
        })
      )
    ).toEqual({
      'test-company-id-0': { id: 'test-company-id-0' },
      'test-company-id-1': { id: 'test-company-id-1' },
    });
  });

  it('should merge the new state to the current state value based on id', () => {
    expect(
      reducer(
        {
          'test-company-id-0': { id: 'test-company-id-0', name: 'Shawn' },
          'test-company-id-1': { id: 'test-company-id-1', name: 'Paul' },
        },
        fetchCompaniesCompleted({
          data: [
            { id: 'test-company-id-1', name: 'Louis' },
            { id: 'test-company-id-2', name: 'Jeff' },
          ],
        })
      )
    ).toEqual({
      'test-company-id-0': { id: 'test-company-id-0', name: 'Shawn' },
      'test-company-id-1': { id: 'test-company-id-1', name: 'Louis' },
      'test-company-id-2': { id: 'test-company-id-2', name: 'Jeff' },
    });
  });

  it('should do nothing on error', () => {
    expect(reducer(defaultState, fetchCompaniesCompleted(new Error()))).toEqual(defaultState);
  });
});

describe('fetchCompanyCompleted', () => {
  it('should insert the company', () => {
    expect(reducer(defaultState, fetchCompanyCompleted({ id: 'test-company-id-0' }))).toEqual({
      'test-company-id-0': { id: 'test-company-id-0' },
    });
  });

  it('should do nothing on error', () => {
    expect(reducer(defaultState, fetchCompanyCompleted(new Error()))).toEqual(defaultState);
  });
});
