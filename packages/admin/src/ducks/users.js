import { createAction, handleActions } from 'redux-actions';

export const createUser = createAction('CREATE_USER');
export const createUserCompleted = createAction('CREATE_USER_COMPLETED');
export const fetchUsers = createAction('FETCH_USERS');
export const fetchUsersCompleted = createAction('FETCH_USERS_COMPLETED');

export default handleActions(
  {
    [createUserCompleted]: {
      next: (state, { payload }) => ({ ...state, [payload.id]: payload }),
    },

    [fetchUsersCompleted]: {
      next: (state, { payload }) =>
        payload.data.reduce((acc, user) => ({ ...acc, [user.id]: user }), state),
    },
  },
  {}
);
