import { createAction } from 'redux-actions';

export const login = createAction('LOGIN');
export const loginCompleted = createAction('LOGIN_COMPLETED');
