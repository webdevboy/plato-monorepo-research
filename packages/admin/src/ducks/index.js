import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import companies from './companies';
import users from './users';
import loading from './loading';

export * from './companies';
export * from './login';
export * from './users';

export default function mainReducer(history) {
  return combineReducers({
    companies,
    loading,
    router: connectRouter(history),
    users,
  });
}
