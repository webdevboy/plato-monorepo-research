import { combineActions, createAction, handleActions } from 'redux-actions';

export const createCompany = createAction('CREATE_COMPANY');
export const createCompanyCompleted = createAction('CREATE_COMPANY_COMPLETED');
export const fetchCompanies = createAction('FETCH_COMPANIES');
export const fetchCompaniesCompleted = createAction('FETCH_COMPANIES_COMPLETED');
export const fetchCompany = createAction('FETCH_COMPANY');
export const fetchCompanyCompleted = createAction('FETCH_COMPANY_COMPLETED');

export default handleActions(
  {
    [fetchCompaniesCompleted]: {
      next: (state, { payload }) =>
        payload.data.reduce((acc, company) => ({ ...acc, [company.id]: company }), state),
    },

    [combineActions(createCompanyCompleted, fetchCompanyCompleted)]: {
      next: (state, { payload }) => ({ ...state, [payload.id]: payload }),
    },
  },
  {}
);
