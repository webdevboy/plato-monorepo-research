import * as Actions from './index';

Object.entries(Actions)
  .filter(([actionName]) => actionName !== 'default')
  .forEach(([actionName, action]) => {
    // eslint-disable-next-line jest/valid-describe
    describe(actionName, () => {
      it('should have a type as the uppercased snake case of the name', () => {
        expect(
          action()
            .type.toLowerCase()
            .replace(/(_\w)/g, char => char[1].toUpperCase())
        ).toBe(actionName);
      });
    });
  });
