import { Component } from 'react';
import PropTypes from 'prop-types';
import { captureSentryException } from 'utils/Sentry';

class ErrorBoundary extends Component {
  static getDerivedStateFromError() {
    return null;
  }

  componentDidCatch(error, errorInfo) {
    captureSentryException(error, errorInfo);
  }

  render() {
    const { children } = this.props;
    return children;
  }
}

ErrorBoundary.propTypes = {
  children: PropTypes.node.isRequired,
};

export default ErrorBoundary;
