import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme';
import ErrorBoundary from './ErrorBoundary';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(
    <ErrorBoundary>
      <div />
    </ErrorBoundary>,
    div
  );
  ReactDOM.unmountComponentAtNode(div);

  shallow(
    <ErrorBoundary>
      <div />
    </ErrorBoundary>
  );
});
