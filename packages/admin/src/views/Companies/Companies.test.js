import { MemoryRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import React from 'react';
import ReactDOM from 'react-dom';
import configureMockStore from 'redux-mock-store';
import Companies from './Companies';

const mockStore = configureMockStore();

describe('Companies', () => {
  it('renders without crashing', () => {
    const store = mockStore({
      companies: {
        companies: [],
        query: '',
      },
    });

    const div = document.createElement('div');
    ReactDOM.render(
      <Provider store={store}>
        <MemoryRouter>
          <Companies />
        </MemoryRouter>
      </Provider>,
      div
    );
    ReactDOM.unmountComponentAtNode(div);
  });
});
