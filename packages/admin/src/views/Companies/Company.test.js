import { MemoryRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import React from 'react';
import ReactDOM from 'react-dom';
import configureMockStore from 'redux-mock-store';
import Company from './Company';

const mockStore = configureMockStore();

describe('Company', () => {
  it('renders without crashing', () => {
    const store = mockStore({
      companies: {
        'test-compagny-id': {},
      },
      users: {},
    });

    const div = document.createElement('div');
    ReactDOM.render(
      <Provider store={store}>
        <MemoryRouter>
          <Company match={{ params: { companyId: 'test-company-id' } }} />
        </MemoryRouter>
      </Provider>,
      div
    );
    ReactDOM.unmountComponentAtNode(div);
  });
});
