import React, { Component } from 'react';
import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit';
import { getDateColumn, getIdColumn } from 'utils/Table';
import BootstrapTable from 'react-bootstrap-table-next';
import { CompanyForm } from 'views';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getCompaniesList } from 'selectors';
import history from 'utils/History';

class Companies extends Component {
  state = {
    showCompanyForm: false,
  };

  handleShowCompanyForm = () => {
    this.setState({ showCompanyForm: true });
  };

  handleHideCompanyForm = () => {
    this.setState({ showCompanyForm: false });
  };

  render() {
    const { companies } = this.props;
    const { showCompanyForm } = this.state;

    if (companies.length <= 0) {
      return <div className="sk-rotating-plane" />;
    }

    const columns = [
      getIdColumn({ sort: true }),
      { dataField: 'name', text: 'Name', sort: true },
      { dataField: 'location', text: 'Location', sort: true },
      getDateColumn({ dataField: 'createdAt', text: 'Created At', sort: true }),
      getDateColumn({ dataField: 'updatedAt', text: 'Last Updated', sort: true }),
    ];

    return (
      <>
        {showCompanyForm && <CompanyForm onCancel={this.handleHideCompanyForm} />}
        <div className="card">
          <div className="card-body">
            <button
              type="button"
              className="btn btn-secondary float-right"
              onClick={this.handleShowCompanyForm}
              disabled={showCompanyForm}
            >
              New
            </button>
            <ToolkitProvider keyField="id" columns={columns} data={companies} search bootstrap4>
              {props => (
                <>
                  <Search.SearchBar placeholder="Search..." {...props.searchProps} />
                  <BootstrapTable
                    {...props.baseProps}
                    hover
                    striped
                    rowClasses="cursor-pointer"
                    rowEvents={{
                      onClick: (event, row) =>
                        history.push(`/companies/${encodeURIComponent(row.id)}`),
                    }}
                  />
                </>
              )}
            </ToolkitProvider>
          </div>
        </div>
      </>
    );
  }
}

Companies.propTypes = {
  companies: PropTypes.array.isRequired,
};

function mapStateToProps(state) {
  return {
    companies: getCompaniesList(state),
  };
}

export default connect(mapStateToProps)(Companies);
