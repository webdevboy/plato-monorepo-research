import { connect } from 'react-redux';
import { fetchCompany } from 'ducks';
import { getCompany, getCompanyUsers } from 'selectors';
import { getIdColumn } from 'utils/Table';
import { toast } from 'react-toastify';
import { UserForm } from 'views';
import BootstrapTable from 'react-bootstrap-table-next';
import PropTypes from 'prop-types';
import React, { Component } from 'react';

class Company extends Component {
  state = {
    showUserForm: false,
  };

  componentDidMount() {
    const {
      match: {
        params: { companyId },
      },
      onFetchCompany,
    } = this.props;
    onFetchCompany(companyId);
  }

  handleShowUserForm = () => {
    this.setState({ showUserForm: true });
  };

  handleHideUserForm = () => {
    this.setState({ showUserForm: false });
  };

  handleCopySlackUrl = () => {
    const {
      company: { slackUrl },
    } = this.props;
    const textarea = document.createElement('textarea');
    textarea.value = slackUrl;
    document.body.appendChild(textarea);
    textarea.select();
    document.execCommand('copy');
    document.body.removeChild(textarea);
    toast('Slack install link copied to clipboard!');
  };

  render() {
    const {
      company,
      match: {
        params: { companyId },
      },
      users,
    } = this.props;
    const { showUserForm } = this.state;

    if (!company) {
      return <div className="sk-rotating-plane" />;
    }

    const columns = [
      getIdColumn({ sort: true }),
      { dataField: 'firstName', text: 'First Name' },
      { dataField: 'lastName', text: 'Last Name' },
      { dataField: 'email', text: 'Email Address' },
      { dataField: 'timeZone', text: 'Time Zone' },
    ];

    return (
      <>
        {showUserForm && <UserForm companyId={companyId} onCancel={this.handleHideUserForm} />}
        <div className="card">
          <div className="card-body">
            {company.pictureUrl && (
              <img
                src="https://cdn.dribbble.com/users/1078339/screenshots/3778212/mister-ours.jpg"
                alt={company.name}
                className="img-thumbnail rounded mb-2 float-right"
                width={200}
              />
            )}

            <h1 className="mb-2">
              <small className="text-muted mr-2">{company.id}</small>
              {company.name}
            </h1>
            {company.keyAccount && <p className="text-danger mb-1">Key account</p>}
            {company.accountImportance && (
              <p className="text-warning mb-1">{company.accountImportance}</p>
            )}
            {company.enterpriseOnly && <p className="text-muted mb-1">Entreprise only</p>}
            <p>
              <b>Creation date:</b> {company.createdAt}
              <br />
              <b>Location:</b> {company.location}
              <br />
              <b>Number of employees:</b> {company.nbEmployees}
              <br />
              <b>Number of engineers:</b> {company.nbEngineers}
              <br />
              <b>Number of tech managers:</b> {company.nbTechManagers}
              <br />
              <b>Number of users:</b> {users.length}
              <br />
              <b>Note:</b> {company.note}
            </p>

            <p className="mb-4">{company.description}</p>

            <div className="container mb-4">
              {Boolean(company.stripeUrl) && (
                <a
                  href={company.stripeUrl}
                  role="button"
                  className="btn btn-info"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  <span className="cui-dollar" />
                  &nbsp;See on Stripe
                </a>
              )}
              <button
                type="button"
                className="btn btn-secondary ml-2"
                onClick={this.handleCopySlackUrl}
              >
                <span className="fa fa-slack" />
                &nbsp;Copy Slack install link
              </button>
              <button
                type="button"
                className="btn btn-success ml-2"
                onClick={this.handleShowUserForm}
                disabled={showUserForm}
              >
                <span className="cui-user-follow" />
                &nbsp;Create user
              </button>
            </div>

            <BootstrapTable keyField="id" hover striped columns={columns} data={users} />
          </div>
        </div>
      </>
    );
  }
}

Company.propTypes = {
  company: PropTypes.object,
  match: PropTypes.object.isRequired,
  onFetchCompany: PropTypes.func.isRequired,
  users: PropTypes.array.isRequired,
};

Company.defaultProps = {
  company: null,
};

function mapStateToProps(state, ownProps) {
  const {
    match: {
      params: { companyId },
    },
  } = ownProps;
  return {
    company: getCompany(state, companyId),
    users: getCompanyUsers(state, companyId),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    onFetchCompany: companyId => dispatch(fetchCompany(companyId)),
  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Company);
