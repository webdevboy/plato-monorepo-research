import React from 'react';
import PropTypes from 'prop-types';
import ImpersonateButton from './ImpersonateButton';

const UserRowActions = ({ row }) => <ImpersonateButton userId={row.id} />;

UserRowActions.propTypes = {
  row: PropTypes.object.isRequired,
};

export default UserRowActions;
