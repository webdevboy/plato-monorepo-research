import { MemoryRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import React from 'react';
import ReactDOM from 'react-dom';
import configureMockStore from 'redux-mock-store';
import Users from './Users';

const mockStore = configureMockStore();

describe('Users', () => {
  it('renders without crashing', () => {
    const store = mockStore({
      users: {},
    });

    const div = document.createElement('div');
    ReactDOM.render(
      <Provider store={store}>
        <MemoryRouter>
          <Users />
        </MemoryRouter>
      </Provider>,
      div
    );
    ReactDOM.unmountComponentAtNode(div);
  });
});
