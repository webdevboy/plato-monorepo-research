import React from 'react';
import ReactDOM from 'react-dom';
import ImpersonateButton from './ImpersonateButton';

describe('ImpersonateButton', () => {
  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<ImpersonateButton userId={1} />, div);
    ReactDOM.unmountComponentAtNode(div);
  });
});
