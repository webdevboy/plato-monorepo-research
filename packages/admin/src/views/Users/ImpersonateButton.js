import PropTypes from 'prop-types';
import React from 'react';

const ImpersonateButton = ({ userId }) => (
  <a
    href={`${process.env.REACT_APP_WEB_APP_URI}/impersonate?user-id=${userId}`}
    title="Impersonate"
    target="_blank"
    rel="noopener noreferrer"
  >
    <i className="fa fa-user-secret fa-lg" />
  </a>
);

ImpersonateButton.propTypes = {
  userId: PropTypes.number.isRequired,
};

export default ImpersonateButton;
