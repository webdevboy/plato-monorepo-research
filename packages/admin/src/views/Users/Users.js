import React, { Component } from 'react';
import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit';

import BootstrapTable from 'react-bootstrap-table-next';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getIdColumn } from 'utils/Table';
import { getUsersList } from 'selectors';
import UserRowActions from './UserRowActions';

class Users extends Component {
  render() {
    const { users } = this.props;

    if (users.length <= 0) {
      return <div className="sk-rotating-plane" />;
    }

    const columns = [
      getIdColumn({ sort: true }),
      { dataField: 'firstName', text: 'First Name', sort: true },
      { dataField: 'lastName', text: 'Last Name', sort: true },
      { dataField: 'email', text: 'Email Address', sort: true },
      { dataField: 'timeZone', text: 'Time Zone', sort: true },
      {
        text: 'Actions',
        formatter: (cell, row) => <UserRowActions row={row} />,
        isDummyField: true,
        dataField: '_actions',
      },
    ];

    return (
      <div className="card">
        <div className="card-body">
          <ToolkitProvider keyField="id" columns={columns} data={users} search bootstrap4>
            {props => (
              <>
                <Search.SearchBar placeholder="Search..." {...props.searchProps} />
                <BootstrapTable
                  selectRow={{ mode: 'checkbox' }}
                  hover
                  striped
                  {...props.baseProps}
                />
              </>
            )}
          </ToolkitProvider>
        </div>
      </div>
    );
  }
}

Users.propTypes = {
  users: PropTypes.array.isRequired,
};

function mapStateToProps(state) {
  return {
    users: getUsersList(state),
  };
}

export default connect(mapStateToProps)(Users);
