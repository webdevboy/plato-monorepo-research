import { MemoryRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import React from 'react';
import ReactDOM from 'react-dom';
import configureMockStore from 'redux-mock-store';
import UserForm from './UserForm';

const mockStore = configureMockStore();

describe('UserForm', () => {
  it('renders without crashing', () => {
    const store = mockStore({
      loading: {},
    });

    const div = document.createElement('div');
    ReactDOM.render(
      <Provider store={store}>
        <MemoryRouter>
          <UserForm isLoading={false} companyId="test-company-id" onCancel={() => {}} />
        </MemoryRouter>
      </Provider>,
      div
    );
    ReactDOM.unmountComponentAtNode(div);
  });
});
