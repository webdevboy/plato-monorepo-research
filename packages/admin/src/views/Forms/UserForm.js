import React, { Component } from 'react';
import { Formik } from 'formik';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createUser } from 'ducks';
import { isActionLoading } from 'selectors';

class UserForm extends Component {
  handleSubmit = values => {
    const { companyId, onCancel, onCreate } = this.props;
    onCreate(values, companyId, onCancel);
  };

  handleValidate = values => {
    const errors = {};
    if (!values.email) {
      errors.email = 'Required';
    }
    if (values.email && !values.email.includes('@')) {
      errors.email = 'Invalid';
    }
    if (!values.firstName) {
      errors.firstName = 'Required';
    }
    if (!values.lastName) {
      errors.lastName = 'Required';
    }
    return errors;
  };

  handleRenderForm = formikProps => {
    const { isLoading, onCancel } = this.props;
    const { isValid, values, handleChange, handleSubmit } = formikProps;
    return (
      <form onSubmit={handleSubmit}>
        <div className="form-row align-items-center">
          <div className="col-auto">
            <label>
              <span>First name</span>
              <input
                type="text"
                autoFocus
                className="form-control"
                name="firstName"
                value={values.firstName}
                onChange={handleChange}
                placeholder="Enter user first name..."
              />
            </label>
          </div>
          <div className="col-auto">
            <label>
              <span>Last name</span>
              <input
                type="text"
                className="form-control"
                name="lastName"
                value={values.lastName}
                onChange={handleChange}
                placeholder="Enter user last name..."
              />
            </label>
          </div>
          <div className="col-auto">
            <label>
              <span>Email address</span>
              <input
                type="text"
                className="form-control"
                name="email"
                value={values.email}
                onChange={handleChange}
                placeholder="Enter email address..."
              />
            </label>
          </div>
        </div>
        <button
          type="submit"
          className="btn btn-primary ml-2 float-right"
          data-style="expand-left"
          disabled={!isValid || isLoading}
        >
          Create user
        </button>
        <button type="button" className="btn btn-secondary float-right" onClick={onCancel}>
          Cancel
        </button>
      </form>
    );
  };

  render() {
    return (
      <div className="card">
        <div className="card-body">
          <Formik
            validate={this.handleValidate}
            initialValues={{ firstName: '', lastName: '', email: '' }}
            onSubmit={this.handleSubmit}
          >
            {this.handleRenderForm}
          </Formik>
        </div>
      </div>
    );
  }
}

UserForm.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  companyId: PropTypes.string.isRequired,
  onCancel: PropTypes.func.isRequired,
  onCreate: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
  return {
    isLoading: isActionLoading(state, createUser),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    onCreate: (user, companyId, callback) => dispatch(createUser({ ...user, companyId, callback })),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserForm);
