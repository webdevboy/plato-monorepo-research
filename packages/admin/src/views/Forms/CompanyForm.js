import React, { Component } from 'react';
import { Formik } from 'formik';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createCompany } from 'ducks';
import { isActionLoading } from 'selectors';

class CompanyForm extends Component {
  handleSubmit = values => {
    const { onCancel, onCreate } = this.props;
    onCreate(values, onCancel);
  };

  handleValidate = values => {
    const errors = {};
    if (!values.email) {
      errors.email = 'Required';
    }
    if (values.email && !values.email.includes('@')) {
      errors.email = 'Invalid';
    }
    if (!values.name) {
      errors.name = 'Required';
    }
    if (!values.location) {
      errors.location = 'Required';
    }
    if (!values.description) {
      errors.description = 'Required';
    }
    return errors;
  };

  handleRenderForm = formikProps => {
    const { isLoading, onCancel } = this.props;
    const { isValid, values, handleChange, handleSubmit } = formikProps;
    return (
      <form onSubmit={handleSubmit}>
        <div className="form-row align-items-center">
          <label className="col-md-4 col-sm-12">
            <span>Company name</span>
            <input
              type="text"
              autoFocus
              className="form-control"
              name="name"
              value={values.name}
              onChange={handleChange}
              placeholder="Enter company name..."
            />
          </label>
          <label className="col-md-4 col-sm-12">
            <span>Email address</span>
            <input
              type="text"
              className="form-control"
              name="email"
              value={values.email}
              onChange={handleChange}
              placeholder="Enter email address..."
            />
          </label>
          <label className="col-md-4 col-sm-12">
            <span>Location</span>
            <input
              type="text"
              className="form-control"
              name="location"
              value={values.location}
              onChange={handleChange}
              placeholder="Enter location..."
            />
          </label>
          <label className="col-md-4 col-sm-12">
            <span>Number of employees</span>
            <input
              type="number"
              className="form-control"
              name="nbEmployees"
              value={values.nbEmployees}
              onChange={handleChange}
            />
          </label>
          <label className="col-md-4 col-sm-12">
            <span>Number of tech managers</span>
            <input
              type="number"
              className="form-control"
              name="nbTechManagers"
              value={values.nbTechManagers}
              onChange={handleChange}
            />
          </label>
          <label className="col-md-4 col-sm-12">
            <span>Number of engineers</span>
            <input
              type="number"
              className="form-control"
              name="nbEngineers"
              value={values.nbEngineers}
              onChange={handleChange}
            />
          </label>
          <label className="col-md-8 col-sm-12">
            <span>Description</span>
            <textarea
              className="form-control"
              name="description"
              value={values.description}
              onChange={handleChange}
              placeholder="Enter description..."
              rows="4"
            />
          </label>
        </div>
        <button
          type="submit"
          className="btn btn-primary ml-2 float-right"
          disabled={!isValid || isLoading}
        >
          Create company
        </button>
        <button type="button" className="btn btn-secondary float-right" onClick={onCancel}>
          Cancel
        </button>
      </form>
    );
  };

  render() {
    return (
      <div className="card">
        <div className="card-body">
          <Formik
            validate={this.handleValidate}
            initialValues={{
              description: '',
              email: '',
              location: '',
              name: '',
              nbEmployees: 0,
              nbEngineers: 0,
              nbTechManagers: 0,
            }}
            onSubmit={this.handleSubmit}
          >
            {this.handleRenderForm}
          </Formik>
        </div>
      </div>
    );
  }
}

CompanyForm.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  onCancel: PropTypes.func.isRequired,
  onCreate: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
  return {
    isLoading: isActionLoading(state, createCompany),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    onCreate: (company, callback) => dispatch(createCompany({ ...company, callback })),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CompanyForm);
