import { MemoryRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import React from 'react';
import ReactDOM from 'react-dom';
import configureMockStore from 'redux-mock-store';
import CompanyForm from './CompanyForm';

const mockStore = configureMockStore();

describe('CompanyForm', () => {
  it('renders without crashing', () => {
    const store = mockStore({
      loading: {},
    });

    const div = document.createElement('div');
    ReactDOM.render(
      <Provider store={store}>
        <MemoryRouter>
          <CompanyForm onCancel={() => {}} />
        </MemoryRouter>
      </Provider>,
      div
    );
    ReactDOM.unmountComponentAtNode(div);
  });
});
