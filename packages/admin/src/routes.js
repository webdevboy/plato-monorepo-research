import React from 'react';

const Companies = React.lazy(() => import('views/Companies/Companies'));
const Company = React.lazy(() => import('views/Companies/Company'));
const Users = React.lazy(() => import('views/Users'));

const routes = [
  { path: '/users', name: 'Users', component: Users, exact: true },
  { path: '/companies', name: 'Companies', component: Companies, exact: true },
  { path: '/companies/:companyId', name: 'Detail', component: Company, exact: true },
];

export default routes;
