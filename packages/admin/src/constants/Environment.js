export const API_VERSION = process.env.REACT_APP_API_VERSION || 'v1';
export const ENVIRONMENT = process.env.NODE_ENV || 'local';
export const VERSION = process.env.REACT_APP_VERSION;
