import { toast } from 'react-toastify';
import { call, takeEvery } from 'redux-saga/effects';

/**
 * Handle the error coming from the sagas.
 * @param {Boolean} action.payload The error triggered by the saga.
 */
export function* handleErrorsSaga(action) {
  const error = action.payload;
  yield call(console.error, error); // eslint-disable-line no-console
  yield call(toast.error, error.message);
}

/**
 * A action that capture any error from the sagas.
 * @param {Action} action Any dispatched actions.
 * @returns {Boolean} True if the action is an error, false otherwise.
 */
export function hasFailed(action) {
  return action.error;
}

export default function* mainSaga() {
  yield takeEvery(hasFailed, handleErrorsSaga);
}
