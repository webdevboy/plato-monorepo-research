import { all, fork } from 'redux-saga/effects';

import companies from './companies';
import users from './users';
import errors from './errors';

export default function* mainSaga() {
  yield all([fork(users), fork(companies), fork(errors)]);
}
