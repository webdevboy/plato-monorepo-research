import { all, takeLatest } from 'redux-saga/effects';
import { toast } from 'react-toastify';
import { createUser, createUserCompleted, fetchUsers, fetchUsersCompleted } from '../ducks';
import usersSaga, { createUserSaga, fetchUsersSaga } from './users';
import { recordSaga } from '../utils/TestHelpers';
import services from '../services';

jest.mock('../services');

it('should setup the sagas', () => {
  const generator = usersSaga();
  expect(generator.next().value).toEqual(
    all([takeLatest(createUser, createUserSaga), takeLatest(fetchUsers, fetchUsersSaga)])
  );
  expect(generator.next().done).toBe(true);
});

describe('fetchUsers', () => {
  it('should dispatch fetchUsersCompleted', async () => {
    services.fetchUsers.mockResolvedValueOnce({ data: [{ id: 'test-persona-id' }] });
    const dispatched = await recordSaga(fetchUsersSaga, fetchUsers());
    expect(dispatched).toContainEqual(fetchUsersCompleted({ data: [{ id: 'test-persona-id' }] }));
  });

  it('should dispatch fetchUsersCompleted on error', async () => {
    services.fetchUsers.mockRejectedValueOnce(new Error('test-error'));
    const dispatched = await recordSaga(fetchUsersSaga, fetchUsers());
    expect(dispatched).toContainEqual(fetchUsersCompleted(new Error('test-error')));
  });
});

describe('createUser', () => {
  it('should dispatch createUserCompleted', async () => {
    jest.spyOn(services, 'createUser').mockResolvedValueOnce({ id: 'test-persona-id' });
    const dispatched = await recordSaga(
      createUserSaga,
      createUser({ firstName: 'test-first-name' })
    );
    expect(dispatched).toContainEqual(createUserCompleted({ id: 'test-persona-id' }));
  });

  it('should call the callback provided', async () => {
    const callback = jest.fn();
    jest.spyOn(services, 'createUser').mockResolvedValueOnce({ id: 'test-persona-id' });
    await recordSaga(createUserSaga, createUser({ callback, firstName: 'test-first-name' }));
    expect(callback).toHaveBeenNthCalledWith(1);
  });

  it('should display a toast', async () => {
    jest.spyOn(services, 'createUser').mockResolvedValueOnce({ id: 'test-company-id' });
    const spy = jest.spyOn(toast, 'success');
    const callback = jest.fn();
    await recordSaga(
      createUserSaga,
      createUser({ callback, firstName: 'test-first-name', lastName: 'test-last-name' })
    );
    expect(spy).toHaveBeenNthCalledWith(
      1,
      'The user test-first-name test-last-name has been created!'
    );
  });

  it('should dispatch createUserCompleted on error', async () => {
    jest.spyOn(services, 'createUser').mockRejectedValueOnce(new Error('test-error'));
    const dispatched = await recordSaga(
      createUserSaga,
      createUser({ firstName: 'test-first-name' })
    );
    expect(dispatched).toContainEqual(createUserCompleted(new Error('test-error')));
  });
});
