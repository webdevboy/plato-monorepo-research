import { createAction } from 'redux-actions';
import { takeEvery } from 'redux-saga/effects';
import errorsSaga, { handleErrorsSaga, hasFailed } from './errors';
import { recordSaga } from '../utils/TestHelpers';

describe('errorsSaga', () => {
  it('should setup the sagas', () => {
    const generator = errorsSaga();
    expect(generator.next().value).toEqual(takeEvery(hasFailed, handleErrorsSaga));
    expect(generator.next().done).toBe(true);
  });
});

describe('hasFailed', () => {
  const doSomething = createAction('DO_SOMETHING');

  it('should return undefined if the action succeed', () => {
    expect(hasFailed(doSomething())).toBe(undefined);
  });

  it('should return true if the action failed', () => {
    expect(hasFailed(doSomething(new Error()))).toBe(true);
  });
});

describe('handleErrorsSaga', () => {
  it('should log the error', async () => {
    const spy = jest.spyOn(global.console, 'error').mockImplementationOnce(() => {});
    const doSomething = createAction('DO_SOMETHING');
    await recordSaga(handleErrorsSaga, doSomething(new Error('test-error')));
    expect(spy).toHaveBeenNthCalledWith(1, new Error('test-error'));
  });
});
