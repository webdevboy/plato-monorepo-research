import { all, takeLatest } from 'redux-saga/effects';
import { toast } from 'react-toastify';
import {
  createCompany,
  createCompanyCompleted,
  fetchCompanies,
  fetchCompaniesCompleted,
  fetchCompany,
  fetchCompanyCompleted,
} from 'ducks';
import { recordSaga } from 'utils/TestHelpers';
import services from 'services';
import companiesSaga, {
  createCompanySaga,
  fetchCompaniesSaga,
  fetchCompanySaga,
} from './companies';

it('should setup the sagas', () => {
  const generator = companiesSaga();
  expect(generator.next().value).toEqual(
    all([
      takeLatest(createCompany, createCompanySaga),
      takeLatest(fetchCompanies, fetchCompaniesSaga),
      takeLatest(fetchCompany, fetchCompanySaga),
    ])
  );
  expect(generator.next().done).toBe(true);
});

describe('fetchCompanies', () => {
  it('should dispatch fetchCompaniesCompleted', async () => {
    jest
      .spyOn(services, 'fetchCompanies')
      .mockResolvedValueOnce({ data: [{ id: 'test-company-id' }] });
    const dispatched = await recordSaga(fetchCompaniesSaga, fetchCompanies());
    expect(dispatched).toContainEqual(
      fetchCompaniesCompleted({ data: [{ id: 'test-company-id' }] })
    );
  });

  it('should dispatch fetchCompaniesCompleted on error', async () => {
    jest.spyOn(services, 'fetchCompanies').mockRejectedValueOnce(new Error('test-error'));
    const dispatched = await recordSaga(fetchCompaniesSaga, fetchCompanies());
    expect(dispatched).toContainEqual(fetchCompaniesCompleted(new Error('test-error')));
  });
});

describe('fetchCompany', () => {
  it('should dispatch fetchCompanyCompleted', async () => {
    jest.spyOn(services, 'fetchCompany').mockResolvedValueOnce({ id: 'test-company-id' });
    const dispatched = await recordSaga(fetchCompanySaga, fetchCompany('test-company-id'));
    expect(dispatched).toContainEqual(fetchCompanyCompleted({ id: 'test-company-id' }));
  });

  it('should dispatch fetchCompanyCompleted on error', async () => {
    jest.spyOn(services, 'fetchCompany').mockRejectedValueOnce(new Error('test-error'));
    const dispatched = await recordSaga(fetchCompanySaga, fetchCompany('test-company-id'));
    expect(dispatched).toContainEqual(fetchCompanyCompleted(new Error('test-error')));
  });
});

describe('createCompany', () => {
  it('should dispatch createCompanyCompleted', async () => {
    jest.spyOn(services, 'createCompany').mockResolvedValueOnce({ id: 'test-company-id' });
    const dispatched = await recordSaga(createCompanySaga, createCompany({}));
    expect(dispatched).toContainEqual(createCompanyCompleted({ id: 'test-company-id' }));
  });

  it('should call the callback', async () => {
    jest.spyOn(services, 'createCompany').mockResolvedValueOnce({ id: 'test-company-id' });
    const callback = jest.fn();
    await recordSaga(createCompanySaga, createCompany({ callback }));
    expect(callback).toHaveBeenNthCalledWith(1);
  });

  it('should display a toast', async () => {
    jest.spyOn(services, 'createCompany').mockResolvedValueOnce({ id: 'test-company-id' });
    const spy = jest.spyOn(toast, 'success');
    const callback = jest.fn();
    await recordSaga(createCompanySaga, createCompany({ callback, name: 'test-name' }));
    expect(spy).toHaveBeenNthCalledWith(1, 'The company test-name has been created!');
  });

  it('should dispatch createCompanyCompleted on error', async () => {
    jest.spyOn(services, 'createCompany').mockRejectedValueOnce(new Error('test-error'));
    const dispatched = await recordSaga(createCompanySaga, createCompany({}));
    expect(dispatched).toContainEqual(createCompanyCompleted(new Error('test-error')));
  });
});
