import { all, call, put, takeLatest } from 'redux-saga/effects';
import { createUser, createUserCompleted, fetchUsers, fetchUsersCompleted } from 'ducks';
import services from 'services';
import { toast } from 'react-toastify';

/**
 * Get the list of users from the API.
 */
export function* createUserSaga({ payload }) {
  const { callback, companyId, ...data } = payload;
  try {
    const token = null; // @todo Get the token somehow, usually from the cookie.
    const response = yield call(services.createUser, token, {
      ...data,
      dashboardUser: { companyId },
    });
    if (callback) callback();
    yield call(toast.success, `The user ${data.firstName} ${data.lastName} has been created!`);
    yield put(createUserCompleted(response));
  } catch (error) {
    yield put(createUserCompleted(error));
  }
}

/**
 * Get the list of users from the API.
 */
export function* fetchUsersSaga() {
  try {
    const token = null; // @todo Get the token somehow, usually from the cookie.
    const response = yield call(services.fetchUsers, token);
    yield put(fetchUsersCompleted(response));
  } catch (error) {
    yield put(fetchUsersCompleted(error));
  }
}

export default function* main() {
  yield all([takeLatest(createUser, createUserSaga), takeLatest(fetchUsers, fetchUsersSaga)]);
}
