import { all, call, put, takeLatest } from 'redux-saga/effects';
import {
  createCompany,
  createCompanyCompleted,
  fetchCompanies,
  fetchCompaniesCompleted,
  fetchCompany,
  fetchCompanyCompleted,
} from 'ducks';
import services from 'services';
import { toast } from 'react-toastify';

/**
 * Create a company.
 */
export function* createCompanySaga({ payload }) {
  const { callback, ...data } = payload;
  try {
    const token = null; // @todo Get the token somehow, usually from the cookie.
    const company = yield call(services.createCompany, token, data);
    if (callback) callback();
    yield call(toast.success, `The company ${data.name} has been created!`);
    yield put(createCompanyCompleted(company));
  } catch (error) {
    yield put(createCompanyCompleted(error));
  }
}

/**
 * Fetch the list of companies.
 */
export function* fetchCompaniesSaga({ payload }) {
  try {
    const token = null; // @todo Get the token somehow, usually from the cookie.
    const companies = yield call(services.fetchCompanies, token, payload);
    yield put(fetchCompaniesCompleted(companies));
  } catch (error) {
    yield put(fetchCompaniesCompleted(error));
  }
}

/**
 * Fetch a single company.
 */
export function* fetchCompanySaga({ payload }) {
  try {
    const token = null; // @todo Get the token somehow, usually from the cookie.
    const company = yield call(services.fetchCompany, token, payload);
    yield put(fetchCompanyCompleted(company));
  } catch (error) {
    yield put(fetchCompanyCompleted(error));
  }
}

export default function* main() {
  yield all([
    takeLatest(createCompany, createCompanySaga),
    takeLatest(fetchCompanies, fetchCompaniesSaga),
    takeLatest(fetchCompany, fetchCompanySaga),
  ]);
}
