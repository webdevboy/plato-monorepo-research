import { all, fork } from 'redux-saga/effects';

import companies from './companies';
import mainSaga from '.';
import users from './users';
import errors from './errors';

it('should setup the sagas', () => {
  const generator = mainSaga();
  expect(generator.next().value).toEqual(all([fork(users), fork(companies), fork(errors)]));
  expect(generator.next().done).toBe(true);
});
