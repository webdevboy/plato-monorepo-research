import moment from 'moment-timezone';
/**
 * Format time in UNIX timestamp into a date in desired time zone, or PST by default (set in env).
 * @param {Timestamp} timestamp A date in UNIX timestamp.
 * @returns {String} A formatted string of the date in the time zone provided, or PST by default (set in env).
 */
export function formatTimestampToDate(timestamp, format = 'MMM D YYYY') {
  return moment(timestamp)
    .tz(moment.tz.guess())
    .format(format);
}
