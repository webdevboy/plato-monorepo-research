import * as Sentry from '@sentry/browser';

import { captureSentryException, initializeSentry } from './Sentry';

jest.mock('@sentry/browser');

describe('initializeSentry', () => {
  it('should initialize Sentry', () => {
    initializeSentry();
    expect(Sentry.init).toHaveBeenNthCalledWith(1, {
      dsn: undefined,
      release: undefined,
      environment: 'test',
    });
  });
});

describe('captureSentryException', () => {
  it('should send the error to Sentry', () => {
    captureSentryException(new Error(), 'test-error-info');
    expect(Sentry.withScope).toHaveBeenNthCalledWith(1, expect.any(Function));
  });
});
