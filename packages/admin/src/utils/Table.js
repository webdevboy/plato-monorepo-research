import React from 'react';
import { formatTimestampToDate } from 'utils/Time';

/**
 * Returns default ID column configuration for react-bootstrap-table-next.
 * @param {Object} options Configuration options for react-bootstrap-table-next.
 * @returns {Object} An object containing ID column defaults for a table combined with passed in options.
 */
export function getIdColumn(options) {
  return {
    dataField: 'id',
    text: 'ID',
    headerStyle: { width: 80 },
    align: 'center',
    headerAlign: 'center',
    ...options,
  };
}

/**
 * Gets an image to render in a table.
 * @param {Object} cell The data used for rendering an image in a cell.
 * @param {String} cell.thumbnailUrl The thumbnail url of the image.
 * @returns {JSX} An image to be rendered in a table, or null if the cell is invalid.
 */
function defaultImageFormatter(cell) {
  if (!cell || !cell.thumbnailUrl) {
    return null;
  }

  return <img width="40" height="40" src={cell.thumbnailUrl} alt="" />;
}

/**
 * Returns default image column configuration for react-bootstrap-table-next.
 * @param {Object} args Configuration options for react-bootstrap-table-next.
 * @param {String} args.dataField The key in the source data used to render this column.
 * @param {String} args.text The title of the column.
 * @param {Function} args.formatter A function that returns the JSX to render for this image.
 * @returns {Object} An object containing image column defaults for a table combined with passed in options.
 */
export function getImageColumn({ dataField, text, formatter = defaultImageFormatter, ...rest }) {
  return {
    dataField,
    text,
    formatter,
    headerStyle: { width: 80 },
    align: 'center',
    headerAlign: 'center',
    ...rest,
  };
}

/**
 * Returns default date column configuration for react-bootstrap-table-next.
 * @param {Object} args Configuration options for react-bootstrap-table-next.
 * @param {String} args.dataField The key in the source data used to render this column.
 * @param {String} args.text The title of the column.
 * @param {String} args.format The moment format string the date should be formatted with.
 * @returns {Object} An object containing date column defaults for a table combined with passed in options.
 */
export function getDateColumn({ dataField, text, format = 'LLL', ...rest }) {
  return {
    dataField,
    text,
    formatter: cell => formatTimestampToDate(cell, format),
    ...rest,
  };
}
