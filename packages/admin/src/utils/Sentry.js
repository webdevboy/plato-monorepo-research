import * as Sentry from '@sentry/browser';

import { ENVIRONMENT, VERSION } from '../constants/Environment';

import { SENTRY_DNS } from '../constants/Sentry';

/**
 * Initialize Sentry.
 */
export function initializeSentry() {
  Sentry.init({
    dsn: SENTRY_DNS,
    release: VERSION,
    environment: ENVIRONMENT,
  });
}

/**
 * Capture Sentry exception.
 * @param {Error} error The error to send to Sentry.
 * @param {Error} errorInfo An additional set of information to send to Sentry.
 */
export function captureSentryException(error, errorInfo) {
  Sentry.withScope(scope => {
    scope.setExtras(errorInfo);
    Sentry.captureException(error);
  });
}
