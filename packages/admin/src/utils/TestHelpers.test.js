import { call, put } from 'redux-saga/effects';

import { recordSaga } from './TestHelpers';

describe('recordSaga', () => {
  it('should record the dispatched actions', async () => {
    function* saga() {
      yield put({ action: 'test-action' });
    }
    const dispatched = await recordSaga(saga);
    expect(dispatched).toEqual([{ action: 'test-action' }]);
  });

  it('should record the side effects', async () => {
    const sideEffect = jest.fn();
    function* saga(action) {
      yield call(sideEffect, action);
    }
    await recordSaga(saga, { action: 'test-action' });
    expect(sideEffect).toHaveBeenNthCalledWith(1, { action: 'test-action' });
  });
});
