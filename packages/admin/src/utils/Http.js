import { API_BASE_URL } from 'constants/Urls';
import { API_VERSION } from 'constants/Environment';

/**
 * Apply HTTP headers for application content type.
 * @param {String} token The JWT token.
 */
function headers(token, jsonContent = false) {
  return {
    Authorization: `Bearer ${token}`,
    ...(jsonContent && { 'Content-Type': 'application/json' }),
  };
}

/**
 * Handle HTTP error code.
 * @param {Object} response The reponse object to validate.
 * @returns {Object} The response.
 * @throws {Error} Throws if the response status was not in the 200 range.
 */
async function handleResponse(response) {
  const body = await response.json();
  if (!response.ok) {
    const error = Array.isArray(body.message)
      ? new Error('An error occured, check the network log')
      : new Error(body.message || 'An error occured');
    error.status = body.statusCode;
    throw error;
  }
  return body;
}

/**
 * Perform a GET request to the API.
 * @param {String} path The path to the object to fetch.
 * @param {String} token A JWT token.
 */
export function get(path, token) {
  return fetch(`${API_BASE_URL}/${API_VERSION}${path}`, {
    method: 'GET',
    headers: headers(token),
  }).then(handleResponse);
}

/**
 * Perform a POST request to the API.
 * @param {String} path The path to the object to create.
 * @param {String} token A JWT token.
 * @param {Object} data The data of the request.
 */
export function post(path, token, data) {
  return fetch(`${API_BASE_URL}/${API_VERSION}${path}`, {
    method: 'POST',
    body: data ? JSON.stringify(data) : null,
    headers: headers(token, true),
  }).then(handleResponse);
}

/**
 * Perform a PUT request to the API.
 * @param {String} path The path to the object to update.
 * @param {String} token A JWT token.
 * @param {Object} data The data of the request.
 */
export function put(path, token, data) {
  return fetch(`${API_BASE_URL}/${API_VERSION}${path}`, {
    method: 'PUT',
    body: data ? JSON.stringify(data) : null,
    headers: headers(token, true),
  }).then(handleResponse);
}

/**
 * Perform a DELETE request to the API.
 * @param {String} path The path to the object to delete.
 * @param {String} token A JWT token.
 */
export function del(path, token) {
  return fetch(`${API_BASE_URL}/${API_VERSION}${path}`, {
    method: 'DELETE',
    headers: headers(token),
  }).then(handleResponse);
}
