export default {
  items: [
    {
      name: 'Users',
      url: '/users',
      icon: 'fa fa-users',
    },
    {
      name: 'Companies',
      url: '/companies',
      icon: 'fa fa-building',
    },
  ],
};
