import * as companies from './companies';
import * as users from './users';

export default {
  ...users,
  ...companies,
};
