import { stringify } from 'query-string';
import { get, post } from '../utils/Http';

/**
 * Create an user.
 * @param {String} token A JWT token.
 * @param {Object} data The user object to create.
 * @returns {Promise} A promise that, when resolved, will contain a user.
 */
export function createUser(token, data) {
  return post(`/users`, token, data);
}

/**
 * Get the list of users.
 * @param {String} token A JWT token.
 */
export function fetchUsers(token, options = {}) {
  return get(`/users?${stringify(options)}`, token);
}
