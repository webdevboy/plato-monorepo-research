import { stringify } from 'query-string';
import { get, post } from '../utils/Http';

/**
 * Create a company.
 * @param {String} token A JWT token.
 * @param {Object} data The company object to create.
 * @returns {Promise} A promise that, when resolved, will contain a company.
 */
export function createCompany(token, data) {
  return post(`/companies`, token, data);
}

/**
 * Fetch a page of companies.
 * @param {String} token A JWT token.
 * @param {Object} options Query parameters to pass to the API.
 * @returns {Promise} A promise that, when resolved, will contain a page of companies.
 */
export function fetchCompanies(token, options = {}) {
  return get(`/companies?${stringify(options)}`, token);
}

/**
 * Fetch a company.
 * @param {String} token A JWT token.
 * @param {String} companyId A company ID.
 * @returns {Promise} A promise that, when resolved, will contain a page of companies.
 */
export function fetchCompany(token, companyId) {
  return get(`/companies/${encodeURIComponent(companyId)}`, token);
}
