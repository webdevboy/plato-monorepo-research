import { getCompaniesList, getCompany } from './companies';

const state = {
  companies: {
    'test-company-id-0': { id: 'test-company-id-0' },
    'test-company-id-1': { id: 'test-company-id-1' },
  },
};

describe('getCompaniesList', () => {
  it('should get a list of companies', () => {
    expect(getCompaniesList(state)).toEqual([
      { id: 'test-company-id-0' },
      { id: 'test-company-id-1' },
    ]);
  });
});

describe('getCompany', () => {
  it('should get a single company', () => {
    expect(getCompany(state, 'test-company-id-0')).toEqual({ id: 'test-company-id-0' });
  });
});
