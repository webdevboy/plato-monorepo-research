/**
 * Get all the users.
 * @param {Object} state The application state.
 * @returns {Array} A list of users.
 */
export function getUsersList(state) {
  return Object.values(state.users);
}

/**
 * Get all the users for a company.
 * @param {Object} state The application state.
 * @param {String} companyId The company ID.
 * @returns {Array} A list of users.
 */
export function getCompanyUsers(state, companyId) {
  return Object.values(state.users).filter(
    user =>
      (user.managerUser && String(user.managerUser.companyId) === companyId) ||
      (user.dashboardUser && String(user.dashboardUser.companyId) === companyId)
  );
}
