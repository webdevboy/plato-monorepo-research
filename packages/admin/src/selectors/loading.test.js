import { createAction } from 'redux-actions';
import { isActionLoading } from './loading';

describe('isActionLoading', () => {
  it('should return true when the action is loading', () => {
    const doSomething = createAction('DO_SOMETHING');
    const state = {
      loading: { DO_SOMETHING: true },
    };
    expect(isActionLoading(state, doSomething)).toBe(true);
  });

  it('should return false when the action is done loading', () => {
    const doSomething = createAction('DO_SOMETHING');
    const state = {
      loading: { DO_SOMETHING: false },
    };
    expect(isActionLoading(state, doSomething)).toBe(false);
  });

  it('should return false when the action is not loading', () => {
    const doSomething = createAction('DO_SOMETHING');
    const state = { loading: {} };
    expect(isActionLoading(state, doSomething)).toBe(false);
  });
});
