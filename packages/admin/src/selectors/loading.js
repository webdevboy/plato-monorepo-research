/**
 * Get whether an asynchronous action is in progress.
 * @param {Object} state The application state.
 * @param {Action} action The action to verify.
 * @returns {Boolean} Whether the action is being performed.
 */
export function isActionLoading(state, action) {
  return Boolean(state.loading[action.toString()]);
}
