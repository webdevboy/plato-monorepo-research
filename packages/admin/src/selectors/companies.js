/**
 * Get the companies from state.
 * @param {Object} state The application state.
 * @param {Object} state.companies The companies view state.
 * @param {Array} state.companies.companies The list of companies in the companies view state.
 * @returns {Object} A mapping of companies.
 */
export function getCompaniesList(state) {
  return Object.values(state.companies);
}

/**
 * Get a company from its ID.
 * @param {Object} state The application state.
 * @param {String} id The id of the company.
 * @returns {Object} A company.
 */
export function getCompany(state, id) {
  return state.companies[id];
}
