import { getUsersList, getCompanyUsers } from './users';

describe('getUsersList', () => {
  const state = {
    users: {
      'test-user-id-0': { id: 'test-user-id-0' },
      'test-user-id-1': { id: 'test-user-id-1' },
    },
  };

  it('should get an array of users', () => {
    expect(getUsersList(state)).toEqual([{ id: 'test-user-id-0' }, { id: 'test-user-id-1' }]);
  });
});

describe('getCompanyUsers', () => {
  const state = {
    users: {
      'test-user-id-0': { id: 'test-user-id-0', managerUser: { companyId: 'test-company-id-0' } },
      'test-user-id-1': { id: 'test-user-id-1', managerUser: { companyId: 'test-company-id-1' } },
      'test-user-id-2': { id: 'test-user-id-2', managerUser: { companyId: 'test-company-id-2' } },
      'test-user-id-3': { id: 'test-user-id-3', dashboardUser: { companyId: 'test-company-id-0' } },
      'test-user-id-4': { id: 'test-user-id-4', dashboardUser: { companyId: 'test-company-id-1' } },
      'test-user-id-5': { id: 'test-user-id-5', dashboardUser: { companyId: 'test-company-id-2' } },
    },
  };

  it('should get an array of the users from the company', () => {
    expect(getCompanyUsers(state, 'test-company-id-0')).toEqual([
      { id: 'test-user-id-0', managerUser: { companyId: 'test-company-id-0' } },
      { id: 'test-user-id-3', dashboardUser: { companyId: 'test-company-id-0' } },
    ]);
  });
});
