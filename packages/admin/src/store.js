import { applyMiddleware, compose, createStore } from 'redux';
import { createLogger } from 'redux-logger';
import createSagaMiddleware from 'redux-saga';
import { routerMiddleware } from 'connected-react-router';
import { ENVIRONMENT } from './constants/Environment';
import history from './utils/History';
import mainReducer from './ducks';
import mySaga from './sagas';

const sagaMiddleware = createSagaMiddleware();
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose; // eslint-disable-line no-underscore-dangle
const middleware = [routerMiddleware(history), sagaMiddleware];

if (ENVIRONMENT === 'development') {
  middleware.push(createLogger({ collapsed: true, duration: true }));
}

export default createStore(mainReducer(history), composeEnhancers(applyMiddleware(...middleware)));

sagaMiddleware.run(mySaga);
