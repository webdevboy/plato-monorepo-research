import './App.scss';
import 'react-toastify/dist/ReactToastify.min.css';
import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import { ConnectedRouter } from 'connected-react-router';
import { ErrorBoundary } from 'views';
import { Provider } from 'react-redux';
import { ToastContainer } from 'react-toastify';
import history from 'utils/History';
import store from 'store';
import { fetchCompanies, fetchUsers } from 'ducks';

const loading = () => <div className="animated fadeIn pt-3 text-center">Loading...</div>;

// Containers
const DefaultLayout = React.lazy(() => import('./containers/DefaultLayout'));

// Pages
const Login = React.lazy(() => import('./views/Pages/Login'));
const Page404 = React.lazy(() => import('./views/Pages/Page404'));

store.dispatch(fetchCompanies());
store.dispatch(fetchUsers());

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <ConnectedRouter history={history}>
          <ErrorBoundary>
            <React.Suspense fallback={loading()}>
              <Switch>
                <Route
                  exact
                  path="/login"
                  name="Login Page"
                  render={props => <Login {...props} />}
                />
                <Route exact path="/404" name="Page 404" render={props => <Page404 {...props} />} />
                <Route path="/" name="Home" render={props => <DefaultLayout {...props} />} />
              </Switch>
              <ToastContainer />
            </React.Suspense>
          </ErrorBoundary>
        </ConnectedRouter>
      </Provider>
    );
  }
}

export default App;
