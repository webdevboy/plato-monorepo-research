# Web app

Plato admin application.

## Setup

### Development

Here are the steps to build and run the project locally.

```
npm i
npm start
```

To customize the application you can edit the `.env.local` file.

#### Lint code

```
npm run lint
```

#### Run tests

```
npm test
```

#### Prepare the production build

```
npm run build
```

### Docker

#### Build the image

In order to build the Docker image, you need to pass build arguments to target the right endpoints and compile the application.

Note that we use a multi-stage build to optimise the image size and you need to interpolate values given the environment you are targeting.

```
docker build \
    --build-arg REACT_APP_VERSION=<PACKAGE_VERSION> \
    -t plato-admin .
```

#### Run the image

Run the application and expose the port 8080.

```
docker run -it -p 8080:80 plato-admin
```

## Configuration

### Environment variables

Access environment variables in the app by referencing properties on `process.env`. You must prepend the name of your environment variables with `REACT_APP_` in order to reference them in the code.

| NAME                                     | Type   | Description                                                              |
| ---------------------------------------- | ------ | ------------------------------------------------------------------------ |
| PORT                                     | Number | The port where the API will run.                                         |
| REACT_APP_API_VERSION                    | String | Current major version of the API. This value is prepended to API routes. |
| REACT_APP_DEFAULT_PAGE_SIZE              | Number | The default size of pages requested from the API.                        |
| REACT_APP_WEB_APP_URI                    | String | Environment-specific mentor/mentee app base URI.                         |
| REACT_APP_SENTRY_APP                     | String | Sentry app URL.                                                          |
| REACT_APP_VERSION                        | String | Current version of the app.                                              |
