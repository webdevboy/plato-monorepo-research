import {
  CreateAccount,
  Login,
  Onboarding,
  ResetPassword,
  SignUp,
  UpdatePassword,
} from 'containers';
import React, { Component } from 'react';
import { Redirect, Route, Switch, matchPath, withRouter } from 'react-router-dom';
import { page } from 'utils/analytics';
import { login, setView } from 'ducks';
import {
  getIsDataLoaded,
  getIsMenteeProfileCompleted,
  getUser,
  getView,
  isActionLoading,
} from 'selectors';

import Ama from 'components/pages/mentee/ama/Ama';
import AmaDetail from 'components/pages/mentee/AmaDetail';
import AppLoading from 'components/appState/AppLoading';
import Challenges from 'components/pages/mentee/challenges/Challenges';
import Dashboard from 'components/pages/dashboard/Dashboard';
import DashboardUserHeader from 'components/header/DashboardUserHeader';
import { MAINTENANCE_MODE_ENABLED } from 'constants/Maintenance';
import Maintenance from 'components/appState/Maintenance';
import MenteeHeader from 'components/header/MenteeHeader';
import MenteeMentorProfile from 'components/pages/mentee/mentors/mentorProfile/MentorProfile';
import MentorAmaDetail from 'components/pages/mentor/mentorAmaDetail/MentorAmaDetail';
import MentorHeader from 'components/header/MentorHeader';
import MentorProfile from 'components/pages/mentor/mentorProfile/MentorProfile';
import MentorSessions from 'components/pages/mentor/mentorSessions/MentorSessions';
import MentorStories from 'components/pages/mentor/mentorStories/MentorStories';
import MentorStory from 'components/pages/mentor/mentorStories/MentorStory';
import Mentors from 'components/pages/mentee/mentors/Mentors';
import Performance from 'components/pages/mentor/performance/Performance';
import PropTypes from 'prop-types';
import Sessions from 'components/pages/mentee/sessions/Sessions';
import Stories from 'components/pages/mentee/stories/Stories';
import Story from 'components/pages/mentee/stories/Story';
import Tracks from 'components/pages/mentee/tracks/Tracks';
import { saveUtmParametersCookie } from 'utils/cookies';
import { views } from 'constants/Views';
import { connect } from 'react-redux';

class App extends Component {
  componentDidMount() {
    const { location, history } = this.props;
    saveUtmParametersCookie(location.search);
    history.listen(page);
  }

  componentDidUpdate(prevProps) {
    const { user, view, location, onSetView } = this.props;
    if (
      location.pathname.startsWith('/dashboard') &&
      view !== views.DASHBOARD_USER &&
      !prevProps.user &&
      user.dashboardUserId
    ) {
      // When the user lands on /dashboard, redirect to the dashboard view
      onSetView(views.DASHBOARD_USER);
    }
  }

  render() {
    const {
      isDataLoaded,
      isLoggingIn,
      isMenteeProfileCompleted,
      user,
      view,
      location,
    } = this.props;

    if (MAINTENANCE_MODE_ENABLED) {
      return <Maintenance />;
    }

    if (!isDataLoaded) {
      // Fetching initial data
      return <AppLoading />;
    }

    if (!user || isLoggingIn) {
      // The user is not logged in
      return (
        <>
          <Switch>
            <Route path="/create-account" component={CreateAccount} />
            <Route
              path="/create-password"
              render={() => <ResetPassword title="Create your password" />}
            />
            <Route path="/reset-password" component={ResetPassword} />
            <Route
              path="/forgot-password"
              render={() => <UpdatePassword title="Reset your password" />}
            />
            <Route
              path="/account"
              render={() => (
                <UpdatePassword
                  // Page still in use to onboard the mentors
                  title="Create your account"
                  description="** Make sure to use the email your Plato account is connected to - Thanks! **"
                />
              )}
            />
            <Route path="/login" component={Login} />
            <Route path="/sign-up" component={SignUp} />
            <Redirect from="/reset_pw" to={{ ...location, pathname: '/reset-password' }} />
            <Redirect from="/forgot_pw" to="/forgot-password" />
            <Redirect from="/beta" to={{ ...location, pathname: '/account' }} />
            <Redirect
              to={{
                pathname: '/login',
                state: { referrer: `${location.pathname}${location.search}` },
              }}
            />
          </Switch>
        </>
      );
    }

    if (view === views.MENTEE) {
      if (!isMenteeProfileCompleted) {
        return (
          <>
            <Switch>
              <Route path="/onboarding" component={Onboarding} />
              <Redirect to="/onboarding" />
            </Switch>
          </>
        );
      }

      const pageMatch = matchPath(location.pathname, { path: '/:page' });
      const isFullPage = pageMatch && pageMatch.params.page === 'onboarding';

      return (
        <>
          {!isFullPage && <MenteeHeader />}
          <Switch>
            <Route exact path="/ama" component={Ama} />
            <Route exact path="/ama/:amaId" component={AmaDetail} />
            <Route exact path="/challenges" component={Challenges} />
            <Route exact path="/mentors" component={Mentors} />
            <Route exact path="/mentors/:mentorId" component={MenteeMentorProfile} />
            <Route exact path="/sessions" component={Sessions} />
            <Route exact path="/stories" component={Stories} />
            <Route exact path="/stories/:storyId" component={Story} />
            <Route exact path="/story" component={Story} />
            <Route exact path="/tracks" component={Tracks} />
            <Route path="/onboarding" component={Onboarding} />
            <Redirect to={(location.state && location.state.referrer) || '/sessions'} />
          </Switch>
        </>
      );
    }

    if (view === views.MENTOR) {
      return (
        <>
          <MentorHeader />
          <Switch>
            <Route path="/mentor_ama/:amaId" component={MentorAmaDetail} />
            <Route path="/mentor_sessions" component={MentorSessions} />
            <Route path="/mentor_stories" component={MentorStories} />
            <Route path="/mentor_story/:storyId" component={MentorStory} />
            <Route path="/mentor_story" component={MentorStory} />
            <Route path="/mentor_performance" component={Performance} />
            <Route path="/mentor_profile" component={MentorProfile} />
            <Redirect to={(location.state && location.state.referrer) || '/mentor_sessions'} />
          </Switch>
        </>
      );
    }

    if (view === views.DASHBOARD_USER) {
      return (
        <>
          <DashboardUserHeader />
          <Switch>
            <Route path="/dashboard" component={Dashboard} />
            <Redirect to="/dashboard" />
          </Switch>
        </>
      );
    }

    return null;
  }
}

App.propTypes = {
  history: PropTypes.object.isRequired,
  isDataLoaded: PropTypes.bool.isRequired,
  isLoggingIn: PropTypes.bool.isRequired,
  isMenteeProfileCompleted: PropTypes.bool.isRequired,
  location: PropTypes.object.isRequired,
  onSetView: PropTypes.func.isRequired,
  user: PropTypes.object,
  view: PropTypes.oneOf(Object.values(views)),
};

App.defaultProps = {
  view: null,
  user: null,
};

function mapStateToProps(state) {
  return {
    isDataLoaded: getIsDataLoaded(state),
    isLoggingIn: isActionLoading(state, login),
    isMenteeProfileCompleted: getIsMenteeProfileCompleted(state),
    user: getUser(state),
    view: getView(state),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    onSetView: toView => dispatch(setView({ toView })),
  };
}

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(App)
);
