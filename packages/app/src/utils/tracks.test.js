import { getTrackAppUrl } from './tracks';

describe('getTrackAppUrl', () => {
  it('should return the URL to the tracks app', () => {
    expect(getTrackAppUrl('test-mentee-id', 'test-uid', 'test-token')).toBe(
      'https://tracks.platohq.com/version-test?menteeId=test-mentee-id&token=test-token&uuid=test-uid'
    );
  });
});
