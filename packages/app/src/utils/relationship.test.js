import { isRelationshipAvailable } from './relationship';

describe('isRelationshipAvailable', () => {
  let mentor;
  let mentee;
  beforeEach(() => {
    mentor = { availableForRelationship: true, doNotProposeLtm: false };
    mentee = { hasLtm: false };
  });

  it('should be true for available mentor and mentee with at least one call', () => {
    expect(isRelationshipAvailable(mentor, mentee, true)).toBe(true);
  });

  it('should be false if the mentor and the mentee never met', () => {
    expect(isRelationshipAvailable(mentor, mentee, false)).toBe(false);
  });

  it('should be false if the mentor is not available for relationship', () => {
    mentor.availableForRelationship = false;
    expect(isRelationshipAvailable(mentor, mentee, true)).toBe(false);
  });

  it('should be false if the mentor is set to not propose relationship', () => {
    mentor.doNotProposeLtm = true;
    expect(isRelationshipAvailable(mentor, mentee, true)).toBe(false);
  });

  it('should be false if the mentee already has a relationship', () => {
    mentee.hasLtm = true;
    expect(isRelationshipAvailable(mentor, mentee, true)).toBe(false);
  });
});
