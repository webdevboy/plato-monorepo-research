/**
 * Format a tag name to a more user friendly version.
 * @param {String} name The name of the tag to format.
 * @returns {String} A user friendly version of the tag name.
 */
export function formatTagName(name) {
  const tagNameMapping = {
    EM: 'Engineering Manager',
    PM: 'Product Manager',
    'COMPANY CULTURE': 'Building a company culture',
    'CONFLICT SOLVING': 'Solving internal conflict',
    'TEAM PROCESSES': 'Team processes',
    'TOXIC EMPLOYEE': 'Dealing with toxic employees',
    HIRING: 'Hiring',
    'PRESSURE/WORKLOAD': 'Handling pressure/workload/stress',
    'CAREER PATH': 'Guiding reports in their career',
    REMOTE: 'Working with remote teams',
    PRODUCTIVITY: 'Increasing productivity',
    DEADLINES: 'Respecting deadlines',
    'NEW MANAGER': 'Being a new manager',
    'SCALING TEAM': 'Scaling your team',
    'INTERNAL COMMUNICATION': 'Communicating internaly',
    'PERSONAL MATTERS': 'Handling personal matters',
    DELEGATE: 'Delegating',
    UNDERPERFORMANCE: 'Handling underperformance',
    ONBOARDING: 'Onboarding new employees',
    MOTIVATION: 'Motivating your team',
    'CULTURAL DIFFERENCES': 'Navigating cultural differences',
    'ONE-ON-ONE': 'Doing One on Ones',
    'WORK QUALITY': 'Verifying work quality',
    REORGANIZATION: 'Reorganizing your team/company',
    'SALARY/WORK CONDITIONS': 'Establishing salary',
    PROMOTION: 'Promoting engineers',
    'SHARING THE VISION': 'Sharing the company vision',
    'COACHING / TRAINING / MENTORSHIP': 'Coaching, training and mentoring',
  };
  return tagNameMapping[name.toUpperCase()] || name;
}
