import { VALIDATIONS_FEATURE_MENTEE_ID_WHITELIST } from 'constants/FeatureFlags';

/**
 * Check whether the mentee has access to the validations feature.
 * @param {Number} menteeId The id of the current mentee.
 * @returns {Boolean} Whether the mentee has access to the validations feature.
 */
export function isValidationsFeatureEnabled(menteeId) {
  const whitelist = VALIDATIONS_FEATURE_MENTEE_ID_WHITELIST.split(',');
  return whitelist.includes(String(menteeId));
}
