import * as Sentry from '@sentry/browser';

/**
 * Initialize Sentry.
 * @todo Add tests.
 */
export function initializeSentry() {
  Sentry.init({
    dsn: process.env.REACT_APP_SENTRY_DSN,
    release: process.env.REACT_APP_VERSION,
    environment: process.env.NODE_ENV,
  });
}

/**
 * Identify a user with Sentry.
 * @todo Add tests.
 * @param {UUID} id The id of the user.
 * @param {String} username The user's full name.
 * @param {String} email The user's email address.
 */
export function identifySentryUser(id, username, email) {
  Sentry.configureScope(scope => {
    scope.setUser({ id, username, email });
  });
}

/**
 * Capture Sentry exception.
 * @todo Add tests.
 * @param {Error} error The error to send to Sentry.
 * @param {Error} errorInfo An additional set of information to send to Sentry.
 */
export function captureSentryException(error, errorInfo) {
  Sentry.withScope(scope => {
    scope.setExtras(errorInfo);
    Sentry.captureException(error);
  });
}
