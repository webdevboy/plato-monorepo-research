/**
 * Convert a tag of type Tag::Face into a user friendly name.
 * @param {String} faceName The value of the Tag::Face tag.
 * @returns {String} A user friendly name for the Tag::Face tag.
 */
function getFaceDisplayName(faceName) {
  switch (faceName) {
    case 'EM':
      return 'Engineering Manager';
    case 'PM':
      return 'Product Manager';
    default:
      return faceName;
  }
}

export default getFaceDisplayName;
