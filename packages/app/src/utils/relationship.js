/**
 * Get whether the mentor is available for relationship.
 * @param {Object} mentor The mentor to check the availability from.
 * @param {Object} mentee The mentee requesting the relationship.
 * @param {Boolean} hadCall Whether the mentee and the mentor already had a call.
 * @returns {Boolean} Whether the mentor is available for relationship.
 */
export function isRelationshipAvailable(mentor, mentee, hadCall) {
  return mentor.availableForRelationship && !mentee.hasLtm && !mentor.doNotProposeLtm && hadCall;
}
