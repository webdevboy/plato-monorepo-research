import {
  TIME_SLOT_10AM_12PM,
  TIME_SLOT_12AM_6AM,
  TIME_SLOT_12PM_2PM,
  TIME_SLOT_2PM_4PM,
  TIME_SLOT_4PM_6PM,
  TIME_SLOT_6AM_8AM,
  TIME_SLOT_8AM_10AM,
  TIME_SLOT_ANY,
} from 'constants/TimeSlots';
import {
  formatRemainingTime,
  formatTimestampToDate,
  formatTimestampToString,
  getDayAndTimeSlotFromDate,
  getTimeSlotBounds,
  getTimeSlotFromDayAndTime,
  getWeeksToDate,
} from './time';

import moment from 'moment-timezone';

describe('formatTimestampToString', () => {
  it('should format a timestamp into a formatted string', () => {
    const timestamp = new Date('Tue Nov 24 1987 15:57:04 GMT-0800').getTime();
    const string = formatTimestampToString(timestamp);
    expect(string).toBe(moment.tz().isDST() ? '3:57 PM (PST)' : '3:57 PM (PDT)');
  });

  it('should format a timestamp into a formatted string in a different time zone', () => {
    const timestamp = new Date('Tue Nov 24 1987 15:57:04 GMT-0800').getTime();
    const string = formatTimestampToString(timestamp, 'Asia/Shanghai');
    expect(string).toBe('7:57 AM (CST)');
  });

  it('should format a timestamp into a formatted string in PST', () => {
    const timestamp = new Date('Tue Nov 24 1987 15:57:04 GMT-0100').getTime();
    const string = formatTimestampToString(timestamp);
    expect(string).toBe(moment.tz().isDST() ? '8:57 AM (PST)' : '8:57 AM (PDT)');
  });

  it('should not display 0 minutes', () => {
    const timestamp = new Date('Tue Nov 24 1987 15:00:04 GMT-0800').getTime();
    const string = formatTimestampToString(timestamp);
    expect(string).toBe(moment.tz().isDST() ? '3 PM (PST)' : '3 PM (PDT)');
  });
});

describe('formatTimestampToDate', () => {
  it('should format a timestamp into a formatted date in PST', () => {
    const timestamp = new Date('Tue Nov 24 1987 15:57:04 GMT-0800').getTime();
    const string = formatTimestampToDate(timestamp);
    expect(string).toBe('Nov 24 1987');
  });

  it('should format a timestamp into a formatted date in the required time zone', () => {
    const timestamp = new Date('Tue Nov 24 1987 15:57:04 GMT-0800').getTime();
    const string = formatTimestampToDate(timestamp, 'Asia/Shanghai');
    expect(string).toBe('Nov 25 1987');
  });
});

describe.skip('getTimeSlotBounds', () => {
  it('should get the bounds for the TIME_SLOT_12AM_6AM time slot', () => {
    const bounds = getTimeSlotBounds(TIME_SLOT_12AM_6AM);
    expect(bounds).toEqual(
      moment.tz().isDST() ? { lower: 8, higher: 14 } : { lower: 7, higher: 13 }
    );
  });

  it('should get the bounds for the TIME_SLOT_6AM_8AM time slot', () => {
    const bounds = getTimeSlotBounds(TIME_SLOT_6AM_8AM);
    expect(bounds).toEqual(
      moment.tz().isDST() ? { lower: 14, higher: 16 } : { lower: 13, higher: 15 }
    );
  });

  it('should get the bounds for the TIME_SLOT_8AM_10AM time slot', () => {
    const bounds = getTimeSlotBounds(TIME_SLOT_8AM_10AM);
    expect(bounds).toEqual(
      moment.tz().isDST() ? { lower: 16, higher: 18 } : { lower: 15, higher: 17 }
    );
  });

  it('should get the bounds for the TIME_SLOT_10AM_12PM time slot', () => {
    const bounds = getTimeSlotBounds(TIME_SLOT_10AM_12PM);
    expect(bounds).toEqual(
      moment.tz().isDST() ? { lower: 18, higher: 20 } : { lower: 17, higher: 19 }
    );
  });

  it('should get the bounds for the TIME_SLOT_12PM_2PM time slot', () => {
    const bounds = getTimeSlotBounds(TIME_SLOT_12PM_2PM);
    expect(bounds).toEqual(
      moment.tz().isDST() ? { lower: 20, higher: 22 } : { lower: 19, higher: 21 }
    );
  });

  it('should get the bounds for the TIME_SLOT_2PM_4PM time slot', () => {
    const bounds = getTimeSlotBounds(TIME_SLOT_2PM_4PM);
    expect(bounds).toEqual(
      moment.tz().isDST() ? { lower: 22, higher: 0 } : { lower: 21, higher: 23 }
    );
  });

  it('should get the bounds for the TIME_SLOT_4PM_6PM time slot', () => {
    const bounds = getTimeSlotBounds(TIME_SLOT_4PM_6PM);
    expect(bounds).toEqual(
      moment.tz().isDST() ? { lower: 0, higher: 2 } : { lower: 23, higher: 1 }
    );
  });

  it('should get the bounds for the TIME_SLOT_ANY time slot', () => {
    const bounds = getTimeSlotBounds(TIME_SLOT_ANY);
    expect(bounds).toEqual({});
  });

  it('should get the bounds for the TIME_SLOT_12AM_6AM time slot and Pacific/Auckland time zone', () => {
    const bounds = getTimeSlotBounds(TIME_SLOT_8AM_10AM, 'Pacific/Auckland');
    expect(bounds).toEqual({ lower: 19, higher: 21 });
  });

  it('should get the bounds for the TIME_SLOT_12AM_6AM time slot and Asia/Hong_Kong', () => {
    const bounds = getTimeSlotBounds(TIME_SLOT_12PM_2PM, 'Asia/Hong_Kong');
    expect(bounds).toEqual({ lower: 4, higher: 6 });
  });

  it('should get the bounds for the TIME_SLOT_12AM_6AM time slot and Europe/Paris', () => {
    const bounds = getTimeSlotBounds(TIME_SLOT_2PM_4PM, 'Europe/Paris');
    expect(bounds).toEqual({ lower: 12, higher: 14 });
  });
});

describe.skip('formatRemainingTime', () => {
  it("should format today's date", () => {
    expect(formatRemainingTime(new Date())).toBe('Today');
  });

  it("should format tomorrow's date", () => {
    const tomorrow = new Date();
    tomorrow.setDate(new Date().getDate() + 1);
    expect(formatRemainingTime(tomorrow)).toBe('Tomorrow');
  });

  it("should format yesterday's date", () => {
    const yesterday = new Date();
    yesterday.setDate(new Date().getDate() - 1);
    expect(formatRemainingTime(yesterday)).toBe('Yesterday');
  });

  it("should format next week's date", () => {
    const weekDays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    const nextWeek = new Date();
    nextWeek.setDate(new Date().getDate() + 2);
    expect(formatRemainingTime(nextWeek)).toBe(weekDays[(new Date().getDay() + 2) % 7]);
  });

  it("should format last week's date", () => {
    const weekDays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    const lastWeek = new Date();
    lastWeek.setDate(lastWeek.getDate() - 2);
    expect(formatRemainingTime(lastWeek)).toBe(
      `Last ${weekDays[(new Date().getDay() - 2 + 7) % 7]}`
    );
  });

  it('should format future date with a default PST time zone', () => {
    moment.tz.setDefault('PST');
    const future = new Date();
    future.setDate(new Date().getDate() + 8);
    future.setHours(23);
    future.setMinutes(0);
    expect(formatRemainingTime(future)).toBe('In 8 days');
  });

  it('should format future date with a default GMT time zone', () => {
    moment.tz.setDefault('GMT');
    const future = new Date();
    future.setDate(new Date().getDate() + 8);
    future.setHours(0);
    future.setMinutes(0);
    expect(formatRemainingTime(future)).toBe('In 7 days');
  });

  it('should format past date with a default PST time zone', () => {
    moment.tz.setDefault('PST');
    const past = new Date();
    past.setDate(new Date().getDate() - 8);
    expect(formatRemainingTime(past)).toBe('8 days ago');
  });

  it('should format past date with a default GMT time zone', () => {
    moment.tz.setDefault('GMT');
    const past = new Date();
    past.setDate(new Date().getDate() - 8);
    expect(formatRemainingTime(past)).toBe('8 days ago');
  });
});

describe('getTimeSlotFromDayAndTime', () => {
  it('should return a date holding the data for the date and time of the time slot', () => {
    const timeSlot = getTimeSlotFromDayAndTime(
      'SUNDAY',
      TIME_SLOT_10AM_12PM,
      null,
      new Date('Tue Nov 24 1987 15:57:04 GMT-0800')
    );
    expect(new Date(timeSlot)).toEqual(new Date('1987-11-22T18:57:04.000Z'));
  });

  it('should return a date holding the data for the date and time of the time slot with a time zone', () => {
    const timeSlot = getTimeSlotFromDayAndTime(
      'SUNDAY',
      TIME_SLOT_10AM_12PM,
      'Asia/Hong_Kong',
      new Date('Tue Nov 24 1987 15:57:04 GMT-0800')
    );
    expect(new Date(timeSlot)).toEqual(new Date('1987-11-22T02:57:04.000Z'));
  });

  it('should handle insensitive case', () => {
    const timeSlot = getTimeSlotFromDayAndTime(
      'moNDaY',
      TIME_SLOT_10AM_12PM,
      null,
      new Date('Tue Nov 24 1987 15:57:04 GMT-0800')
    );
    expect(new Date(timeSlot)).toEqual(new Date('1987-11-23T18:57:04.000Z'));
  });
});

describe('getDayAndTimeSlotFromDate', () => {
  it('should extract the day and time from the date', () => {
    const date = new Date('1987-11-23T18:57:04.000Z');
    const dayAndTimeSlot = getDayAndTimeSlotFromDate(date);
    expect(dayAndTimeSlot).toEqual({
      day: 'monday',
      timeSlot: TIME_SLOT_10AM_12PM,
    });
  });

  it('should extract the day and time from the date based on the time zone', () => {
    const date = new Date('1987-11-23T22:57:04.000Z');
    const dayAndTimeSlot = getDayAndTimeSlotFromDate(date, 'Asia/Hong_Kong');
    expect(dayAndTimeSlot).toEqual({
      day: 'tuesday',
      timeSlot: TIME_SLOT_6AM_8AM,
    });
  });
});

describe('getWeeksToDate', () => {
  it('should return the number of weeks between now and a given timestamp', () => {
    const inFiveWeeks = moment().add(36, 'days');
    const weeks = getWeeksToDate(inFiveWeeks);
    expect(weeks).toEqual(5);
  });

  it('should return 7 weeks from a timestamp in 52 days', () => {
    const inSevenWeeks = moment().add(52, 'days');
    const weeks = getWeeksToDate(inSevenWeeks, 'America/Los_Angeles');
    expect(weeks).toEqual(7);
  });
});
