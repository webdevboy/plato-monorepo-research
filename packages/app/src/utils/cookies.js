import queryString from 'query-string';

/**
 * Gets the value of a specific cookie
 * @param {string} name The name of the cookie.
 * @returns {String|Null} The value of the cookie.
 */
export function getCookie(name) {
  const match = document.cookie.match(new RegExp(`(^| )${name}=([^;]+)`));
  return match ? match[2] : null;
}

/**
 * Saves the UTM url parameters in a cookie
 * @param {String} query The search query to parse.
 * @returns {Object} The UTM object saved.
 */
export function saveUtmParametersCookie(query) {
  const search = queryString.parse(query);
  const utm = {
    source: search.utm_source,
    medium: search.utm_medium,
    campaign: search.utm_campaign,
    content: search.utm_content,
    term: search.utm_term,
  };
  document.cookie = `utm=${btoa(JSON.stringify(utm))}; path=/`;
  return utm;
}
