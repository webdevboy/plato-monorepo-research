/* eslint-disable camelcase */
import { getCookie } from 'utils/cookies';

/**
 * Send a page event to identify the current location of the user.
 * @returns {void}
 */
export function page() {
  window.analytics.page();
}

/**
 * Track an event
 * @param {string} event The name of the event.
 * @param {UUID} uuid The uuid of the originator.
 * @param {string} email The email of the originator.
 * @param {Object} [properties] The properties specific to the event.
 * @returns {void}
 */
export function track(event, uuid, email, properties = {}) {
  const eventProperties = Object.entries(properties).reduce(
    (acc, [key, value]) => Object.assign(acc, { [`ep_${key}`]: value }),
    {}
  );

  const utmCookie = getCookie('utm');
  const utm = utmCookie ? JSON.parse(atob(utmCookie)) : {};

  const role = localStorage.getItem('view');
  window.analytics.track(
    event,
    {
      user_role: role,
      originator_id: uuid,
      originator_role: role,
      actor_id: uuid,
      actor_role: role,
      is_originator: uuid && true,
      ...eventProperties,
    },
    {
      campaign: {
        source: utm.source,
        medium: utm.medium,
        name: utm.campaign,
        content: utm.content,
        term: utm.term,
      },
    }
  );
}

/**
 * Identify the user in third parties.
 * @param {UUID} uuid The uuid of the user.
 * @param {Object} [traits] The traits of the user.
 * @returns {void}
 */
export function identify(uuid, traits = {}) {
  window.analytics.identify(uuid, traits);
}

/**
 * Send a track event when the user logs in.
 * @param {UUID} uuid The uuid of the user.
 * @param {string} email The email address of the user.
 * @param {string} name The name of the user.
 * @returns {void}
 */
export function trackLogin({ uuid, email, name }) {
  identify(uuid, { name, email });
  track('User logged in', uuid, email);
}

/**
 * Send a track event when the user logs out.
 * @param {Object} [traits] An object containing different traits.
 * @returns {void}
 */
export function trackLogout(traits = {}) {
  const { uuid, email } = traits;
  track('User logged out', uuid, email);
  window.analytics.reset();
}

/**
 * Send a track event when the user land on the app.
 * @param {Object} [traits] An object containing the following traits:
 * @param {UUID} traits.uuid The uuid of the user.
 * @param {String} traits.email The email address of the user.
 * @returns {void}
 */
export function trackAppOpened(traits = null) {
  if (traits) {
    track('App opened', traits.uuid, traits.email);
  } else {
    track('App opened');
  }
}

/**
 * Send a track event when the user share a challenge.
 * @param {UUID} uuid The uuid of the user.
 * @param {string} email The email address of the user.
 * @param {number} menteeId The user's mentee id.
 * @param {number} briefId The id of the shared brief.
 * @param {string} location The page the event occurred on.
 * @param {string} source The marketing source.
 * @returns {void}
 */
export function trackChallengeShared({ uuid, email, menteeId, briefId, location, source }) {
  track('Challenge shared', uuid, email, {
    brief_id: briefId,
    mentee_id: menteeId,
    location,
    source,
  });
}

/**
 * Send a track event when the user book a 1:1 or a AMA.
 * @param {UUID} uuid The uuid of the user.
 * @param {string} email The email address of the user.
 * @param {number} menteeId The user's mentee id.
 * @param {Date} eventCallScheduledDate The scheduled date of the call.
 * @param {number} eventId The id of the event.
 * @param {string} eventType The type of the event.
 * @returns {void}
 */
export function trackEventBooked({
  uuid,
  email,
  menteeId,
  eventCallScheduledDate,
  eventId,
  eventType,
}) {
  track('Event booked', uuid, email, {
    event_call_scheduled_date: eventCallScheduledDate,
    event_id: eventId,
    event_type: eventType,
    mentee_id: menteeId,
    origin: 'web',
  });
}

/**
 * Send a track event when the user read a story.
 * @param {UUID} uuid The uuid of the user.
 * @param {string} email The email address of the user.
 * @param {number} menteeId The user's mentee id.
 * @param {number} storyId The id of the story the mentee read.
 * @param {string} source The marketing source.
 * @returns {void}
 */
export function trackMenteeStoryRead({ uuid, email, menteeId, storyId, source }) {
  track('Story read', uuid, email, {
    mentee_id: menteeId,
    story_id: storyId,
    source,
  });
}

/**
 * Send a track event when the user save a story.
 * @param {UUID} uuid The uuid of the user.
 * @param {string} email The email address of the user.
 * @param {number} mentorId The user's mentor id.
 * @param {string} location The page the event occurred on.
 * @returns {void}
 */
export function trackStoryWritten({ uuid, email, mentorId, location }) {
  track('Story written', uuid, email, {
    location,
    mentor_id: mentorId,
  });
}

/**
 * Send a track event when the user re-open a challenge.
 * @param {UUID} uuid The uuid of the user.
 * @param {string} email The email address of the user.
 * @param {number} menteeId The user's mentee id.
 * @param {number} briefId The id of the reopened challenge.
 * @returns {void}
 */
export function trackChallengeReopened({ uuid, email, menteeId, briefId }) {
  track('Challenge reopened', uuid, email, {
    brief_id: briefId,
    mentee_id: menteeId,
  });
}

/**
 * Send a track event when the user close a challenge.
 * @param {Object} traits An object containing different traits.
 * @returns {void}
 */
export function trackChallengeClosed({ uuid, email, menteeId, briefId }) {
  track('Challenge closed', uuid, email, {
    brief_id: briefId,
    mentee_id: menteeId,
  });
}

/**
 * Send a track event when the user submit a search query.
 * @param {Object} traits An object containing different traits.
 * @returns {void}
 */
export function trackSearchSubmitted({ menteeId, location, searchString, email, uuid }) {
  track('Search submitted', uuid, email, {
    search_string: searchString,
    mentee_id: menteeId,
    location,
  });
}

/**
 * Send a track event when the user use a filter.
 * @param {Object} traits An object containing different traits.
 * @returns {void}
 */
export function trackFilterUsed({
  email,
  filterName,
  filterType,
  filterValue,
  location,
  menteeId,
  uuid,
}) {
  track('Filter used', uuid, email, {
    filter_name: filterName,
    filter_type: filterType,
    filter_value: filterValue,
    mentee_id: menteeId,
    location,
  });
}

/**
 * Send a track event when the user import the AMA calendar.
 * @param {Object} traits An object containing different traits.
 * @returns {void}
 */
export function trackAmaCalendarImported({ uuid, email, menteeId }) {
  track('Ama calendar imported', uuid, email, {
    mentee_id: menteeId,
  });
}

/**
 * Send a track event when the user switches space.
 * @param {Object} traits An object containing different traits.
 * @returns {void}
 */
export function trackSpaceSwitched({
  uuid,
  email,
  menteeId,
  dashboardUserId,
  mentorId,
  fromView,
  toView,
}) {
  track('Space switched', uuid, email, {
    mentee_id: menteeId,
    mentor_id: mentorId,
    dashboard_user_id: dashboardUserId,
    from_view: fromView,
    to_view: toView,
  });
}

/**
 * Send a track event when the user change his password.
 * @returns {void}
 */
export function trackPasswordChanged({ uuid, email }) {
  track('Password changed', uuid, email);
}

/**
 * Send a track event when the user requested an mentor introduction.
 * @param {Object} traits An object containing different traits.
 * @returns {void}
 */
export function trackMentorIntroMade({ uuid, email, mentorId, referral }) {
  track('Mentor intro made', uuid, email, {
    mentor_id: mentorId,
    referral,
  });
}

/**
 * Send a track event when the user cancel an event.
 * @param {Object} traits An object containing different traits.
 * @returns {void}
 */
export function trackMentorEventCanceled({ uuid, email, mentorId, eventId, eventType }) {
  track('Event canceled', uuid, email, {
    mentor_id: mentorId,
    source: 'web',
    event_id: eventId,
    event_type: eventType,
  });
}

/**
 * Send a track event when the user press the Learn More CTA.
 * @todo Use a more generic event name.
 * @param {Object} traits An object containing different traits.
 * @returns {void}
 */
export function trackPerformanceLearnMore({ uuid, email, mentorId, infoType }) {
  track('Performance learn more', uuid, email, {
    mentor_id: mentorId,
    info_type: infoType,
  });
}

/**
 * Send a track event when the user validate a story.
 * @param {Object} traits An object containing different traits.
 * @returns {void}
 */
export function trackStoryValidated({ uuid, email, mentorId, storyId }) {
  track('Story validated', uuid, email, {
    mentor_id: mentorId,
    story_id: storyId,
  });
}

/**
 * Send a track event when the user edit a story.
 * @param {Object} traits An object containing different traits.
 * @returns {void}
 */
export function trackStoryEdited({ uuid, email, mentorId, storyId }) {
  track('Story edited', uuid, email, {
    story_id: storyId,
    mentor_id: mentorId,
  });
}

/**
 * Send a track event when the user read a story.
 * @param {Object} traits An object containing different traits.
 * @returns {void}
 */
export function trackMentorStoryRead({ uuid, email, mentorId, storyId }) {
  track('Story read', uuid, email, {
    mentor_id: mentorId,
    story_id: storyId,
  });
}

/**
 * Send a track event when the user click the View More CTA.
 * @todo Use a more generic event name.
 * @param {Object} traits An object containing different traits.
 * @returns {void}
 */
export function trackRequestViewMore({ uuid, email, mentorId, requestId, infoType }) {
  track('View more requested', uuid, email, {
    mentor_id: mentorId,
    request_id: requestId,
    info_type: infoType,
  });
}

/**
 * Send a track event when the user view the public profile.
 * @param {Object} traits An object containing different traits.
 * @returns {void}
 */
export function trackPublicProfileViewed({ uuid, email, mentorId }) {
  track('Public profile viewed', uuid, email, {
    mentor_id: mentorId,
  });
}
