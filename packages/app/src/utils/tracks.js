import { TRACKS_APP_URL } from 'constants/Urls';
import queryString from 'query-string';

/**
 * Get the URL to the tracks app.
 * @param {String} menteeId The id of the mentee.
 * @param {String} uuid The user's uuid.
 * @param {String} token The user's authentication token.
 * @returns {String} The URL to the tracks app.
 */
export function getTrackAppUrl(menteeId, uuid, token) {
  const query = queryString.stringify({ menteeId, uuid, token });
  return `${TRACKS_APP_URL}?${query}`;
}
