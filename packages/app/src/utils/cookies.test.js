import { getCookie } from './cookies';

describe('getCookie', () => {
  it('should return the value of a cookie', () => {
    document.cookie = 'test-cookie=test-value';
    expect(getCookie('test-cookie')).toBe('test-value');
  });
});
