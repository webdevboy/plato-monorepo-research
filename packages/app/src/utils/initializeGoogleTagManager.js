import TagManager from 'react-gtm-module';

if (process.env.NODE_ENV === 'production') {
  TagManager.initialize({
    gtmId: process.env.REACT_APP_TAG_MANAGER_ID,
  });
}
