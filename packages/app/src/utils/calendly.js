import { CALENDLY_CHECK_IN_URL } from 'constants/Urls';
import moment from 'moment';
import queryString from 'query-string';

/**
 * Check whether the payload is a valid signature from Calendly.
 * @see {@link https://help.calendly.com/hc/en-us/articles/226767207}
 * @param {Object} payload The payload to validate.
 * @returns {Boolean} Whether the payload is set from Calendly.
 */
export function validateCalendlyCallback(payload) {
  return Boolean(
    payload.event_type_name &&
      payload.event_type_uuid &&
      payload.invitee_uuid &&
      payload.invitee_email
  );
}

/**
 * Get the Calendly check-in url.
 * @todo Force operators to have calendly links.
 * @param {String} id An ID to send to Calendly.
 * @param {String} name The name to send to Calendly.
 * @param {String} email The email address to send to Calendly.
 * @param {Date} date The date to get the Calemdly from.
 * @returns {String} The link to Calendly.
 */
export function getCalendlyCheckInUrl(id, name, email, date) {
  const formattedDate = moment(date).format('MM-DD-YYYY');
  const queryParams = queryString.stringify({
    email,
    name,
    utm_content: id, // eslint-disable-line camelcase
    utm_medium: 'web-app', // eslint-disable-line camelcase
    utm_source: 'embed', // eslint-disable-line camelcase
  });
  return `${CALENDLY_CHECK_IN_URL}/${formattedDate}?${queryParams}`;
}
