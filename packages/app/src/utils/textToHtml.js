import React from 'react';

/**
 * Convert multiline text into HTML paragraphs.
 * @param {String} text The text to convert.
 * @return {JSX} A JSX representation of the text.
 */
function textToHtml(text) {
  return text
    .split('\n')
    .filter(chunk => chunk.length > 0)
    .map((chunk, index) => <p key={index}>{chunk}</p>);
}

export default textToHtml;
