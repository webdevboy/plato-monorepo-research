import { isValidationsFeatureEnabled } from './featureFlags';

describe('isValidationsFeatureEnabled', () => {
  it('should return true if the mentee is whitelisted', () => {
    expect(isValidationsFeatureEnabled(1)).toBe(true);
  });
  it('should return false if the mentee is not whitelisted', () => {
    expect(isValidationsFeatureEnabled(42)).toBe(false);
  });
});
