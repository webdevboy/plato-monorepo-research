import React from 'react';
import textToHtml from './textToHtml';

it('should convert string into JSX', () => {
  const jsx = textToHtml('test-text');
  expect(jsx).toEqual([<p key={0}>test-text</p>]);
});

it('should convert line return into multiple paragraphes', () => {
  const jsx = textToHtml('test-text\ntest-text');
  expect(jsx).toEqual([<p key={0}>test-text</p>, <p key={1}>test-text</p>]);
});
