import * as Sentry from '@sentry/browser';

import { LOGROCKET_ID } from 'constants/LogRocket';
import LogRocket from 'logrocket';
import setupLogRocketReact from 'logrocket-react';

/**
 * Initialize LogRocket.
 * @todo Add tests.
 */
export function initializeLogRocket() {
  if (!LOGROCKET_ID) {
    return;
  }

  LogRocket.init(LOGROCKET_ID);
  setupLogRocketReact(LogRocket);
  Sentry.configureScope(scope => {
    scope.addEventProcessor(event => {
      event.extra.sessionURL = LogRocket.sessionURL; // eslint-disable-line no-param-reassign
      return event;
    });
  });
}

/**
 * Identify a user with LogRocket.
 * @todo Add tests.
 * @param {UUID} id The id of the user.
 * @param {String} name The user's full name.
 * @param {String} email The user's email address.
 */
export function identifyLogRocketUser(id, name, email) {
  if (!LOGROCKET_ID) {
    return;
  }

  LogRocket.identify(id, { name, email });
}
