import Fuse from 'fuse.js'; // eslint-disable-line import/extensions

const DEFAULT_SEARCH_OPTIONS = {
  shouldSort: true,
  threshold: 0.3,
  location: 0,
  distance: 100,
  maxPatternLength: 32,
  minMatchCharLength: 0,
};

/**
 * Performs a fuzzy search on the given list of objects
 * @param {Array<Object>} list List of objects to search
 * @param {String} query The information being searched for
 * @param {Array<String>} keys A list of keys in the objects in list to match on the search string
 * @param {Object} optionOverrides Fuse.js search option overrides
 * @returns {Array<Object>} List of objects that match the search string
 */
export default function performSearch(list, query, keys, optionOverrides = {}) {
  const searchOptions = {
    ...DEFAULT_SEARCH_OPTIONS,
    ...optionOverrides,
    keys,
  };

  const fuse = new Fuse(list, searchOptions);
  return fuse.search(query);
}
