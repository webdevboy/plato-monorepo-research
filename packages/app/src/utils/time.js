import {
  TIME_SLOT_10AM_12PM,
  TIME_SLOT_12AM_6AM,
  TIME_SLOT_12PM_2PM,
  TIME_SLOT_2PM_4PM,
  TIME_SLOT_4PM_6PM,
  TIME_SLOT_6AM_8AM,
  TIME_SLOT_6PM_12AM,
  TIME_SLOT_8AM_10AM,
  TIME_SLOT_ANY,
} from 'constants/TimeSlots';

import { DEFAULT_IANA_TIME_ZONE } from 'constants/TimeZones';
import moment from 'moment-timezone';

/**
 * Format time in UNIX timestamp into a string of the time in desired time zone, or PST by default (set in env).
 * @param {Timestamp} timestamp A date in UNIX timestamp.
 * @param {String} [timeZone] A time zone in IANA format
 * @returns {String} A formatted string of the time in the time zone provided, or PST by default (set in env).
 */
export function formatTimestampToString(timestamp, timeZone = null) {
  const newDate = moment(timestamp).tz(timeZone || DEFAULT_IANA_TIME_ZONE);
  const timeZoneName = moment.tz(timeZone || DEFAULT_IANA_TIME_ZONE).zoneName();
  const time = newDate.minutes() === 0 ? newDate.format('h A') : newDate.format('h:mm A');
  return `${time} (${timeZoneName})`;
}

/**
 * Format time in UNIX timestamp into a date in desired time zone, or PST by default (set in env).
 * @param {Timestamp} timestamp A date in UNIX timestamp.
 * @param {String} timeZone A time zone in IANA format
 * @returns {String} A formatted string of the date in the time zone provided, or PST by default (set in env).
 */
export function formatTimestampToDate(timestamp, timeZone, format = 'MMM D YYYY') {
  return moment(timestamp)
    .tz(timeZone || DEFAULT_IANA_TIME_ZONE)
    .format(format);
}

/**
 * Normalize a given local hour to UTC time.
 * @param {Number} localHour The hour slot to convert.
 * @param {String} timeZone The time zone in IANA format.
 * @returns {Number} The normalized hour in UTC time.
 */
export function toUTCHour(localHour, timeZone) {
  const now = moment.utc();
  const offset = moment.tz.zone(timeZone || DEFAULT_IANA_TIME_ZONE).utcOffset(now) / 60;
  const hour = localHour + offset > 0 ? localHour + offset : localHour + offset + 24;
  return hour % 24;
}

/**
 * Get bounds from a time slot, convert the time of the day in UTC.
 * @param {Object} timeSlot The time slot to convert.
 * @param {String} timeZone The time zone in IANA format.
 * @returns {Object} The translated availabilities.
 */
export function getTimeSlotBounds(timeSlot, timeZone = DEFAULT_IANA_TIME_ZONE) {
  switch (timeSlot) {
    case TIME_SLOT_12AM_6AM: // 12am - 6am
      return {
        lower: toUTCHour(0, timeZone),
        higher: toUTCHour(6, timeZone),
      };
    case TIME_SLOT_6AM_8AM: // 6am - 8am
      return {
        lower: toUTCHour(6, timeZone),
        higher: toUTCHour(8, timeZone),
      };
    case TIME_SLOT_8AM_10AM: // 8am - 10am
      return {
        lower: toUTCHour(8, timeZone),
        higher: toUTCHour(10, timeZone),
      };
    case TIME_SLOT_10AM_12PM: // 10am - 12am
      return {
        lower: toUTCHour(10, timeZone),
        higher: toUTCHour(12, timeZone),
      };
    case TIME_SLOT_12PM_2PM: // 12pm - 2pm
      return {
        lower: toUTCHour(12, timeZone),
        higher: toUTCHour(14, timeZone),
      };
    case TIME_SLOT_2PM_4PM: // 2pm - 4pm
      return {
        lower: toUTCHour(14, timeZone),
        higher: toUTCHour(16, timeZone),
      };
    case TIME_SLOT_4PM_6PM: // 4pm - 6pm
      return {
        lower: toUTCHour(16, timeZone),
        higher: toUTCHour(18, timeZone),
      };
    case TIME_SLOT_6PM_12AM: // 6pm - 12am
      return {
        lower: toUTCHour(18, timeZone),
        higher: toUTCHour(0, timeZone),
      };
    case TIME_SLOT_ANY: // Any time
    default:
      return {};
  }
}

/**
 * Format a timestamp into a user friendly representation of the remaining time.
 * @param {Timestamp} timestamp A date in UNIX timestamp.
 * @returns {String} A user friendly representation of the remaining time.
 */
export function formatRemainingTime(timestamp) {
  return moment(timestamp).calendar(null, {
    sameDay: '[Today]',
    nextDay: '[Tomorrow]',
    nextWeek: 'dddd',
    lastDay: '[Yesterday]',
    lastWeek: '[Last] dddd',
    sameElse: () =>
      moment().isBefore(timestamp)
        ? `[In ${moment(timestamp).diff(moment(), 'days')} days]`
        : `[${moment().diff(timestamp, 'days')} days ago]`,
  });
}

/**
 * Get time slot from a day and time.
 * @todo Add tests for this function
 * @param {String} day The day of the time slot.
 * @param {String} time The time of the time slot.
 * @param {String} [timeZone] The time zone to get the time slot at.
 * @param {Date} [fromDate] The initial date to calculate the time slot from.
 * @returns {Date} The time slot.
 */
export function getTimeSlotFromDayAndTime(day, time, timeZone = null, fromDate = new Date()) {
  const weekDays = ['SUNDAY', 'MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY'];

  const timeMap = {
    [TIME_SLOT_10AM_12PM]: 10,
    [TIME_SLOT_12PM_2PM]: 12,
    [TIME_SLOT_12AM_6AM]: 0,
    [TIME_SLOT_2PM_4PM]: 14,
    [TIME_SLOT_4PM_6PM]: 16,
    [TIME_SLOT_6AM_8AM]: 6,
    [TIME_SLOT_8AM_10AM]: 8,
    [TIME_SLOT_6PM_12AM]: 18,
  };

  return moment(fromDate)
    .tz(timeZone || DEFAULT_IANA_TIME_ZONE)
    .day(weekDays.indexOf(day.toUpperCase()))
    .hour(timeMap[time]);
}

/**
 * Extract day and time slot from a date.
 * @param {Date} date The date to get the time slot and day from.
 * @returns {Object} An object containing the day and time from the date.
 */
export function getDayAndTimeSlotFromDate(date, timeZone) {
  const timeMap = {
    0: TIME_SLOT_12AM_6AM,
    6: TIME_SLOT_6AM_8AM,
    8: TIME_SLOT_8AM_10AM,
    10: TIME_SLOT_10AM_12PM,
    12: TIME_SLOT_12PM_2PM,
    14: TIME_SLOT_2PM_4PM,
    16: TIME_SLOT_4PM_6PM,
    18: TIME_SLOT_6PM_12AM,
  };
  const localDate = moment(date).tz(timeZone || DEFAULT_IANA_TIME_ZONE);
  const day = localDate.format('dddd').toLowerCase();
  const timeSlot = timeMap[localDate.hour()] || TIME_SLOT_ANY;
  return { day, timeSlot };
}

/**
 * Calculates the number of weeks until a given timestamp.
 * @param {Timestamp} timestamp a date in UNIX timestamp.
 * @param {String} timeZone The time zone of the user.
 * @returns {Number} The number of weeks between now and the given timestamp.
 */
export function getWeeksToDate(timestamp, timeZone) {
  const targetDate = moment(timestamp).tz(timeZone || moment.tz.guess());
  const localTime = moment().tz(timeZone || moment.tz.guess());

  return moment(targetDate).diff(localTime, 'weeks');
}
