import { formatTagName } from './tag';

describe('formatTagName', () => {
  // We are not testing each and every tag for the sake of scalability

  it('should convert the EM tag', () => {
    expect(formatTagName('EM')).toBe('Engineering Manager');
  });

  it('should convert the PM tag', () => {
    expect(formatTagName('PM')).toBe('Product Manager');
  });

  it('should convert the Company Culture tag', () => {
    expect(formatTagName('Company Culture')).toBe('Building a company culture');
  });

  it('should be case insensitive', () => {
    expect(formatTagName('CoMPany cUlTure')).toBe('Building a company culture');
  });

  it('should handle missing key', () => {
    expect(formatTagName('Lorem ipsum')).toBe('Lorem ipsum');
  });
});
