import getFaceDisplayName from './faceDisplayNames';

it('should get the display name for the EM tag', () => {
  const displayName = getFaceDisplayName('EM');
  expect(displayName).toBe('Engineering Manager');
});

it('should get the display name for the PM tag', () => {
  const displayName = getFaceDisplayName('PM');
  expect(displayName).toBe('Product Manager');
});

it('should return the tag value for a non Tag::Face tag', () => {
  const displayName = getFaceDisplayName('R&D');
  expect(displayName).toBe('R&D');
});
