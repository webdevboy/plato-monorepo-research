import moment from 'moment-timezone';
import { getCalendlyCheckInUrl, validateCalendlyCallback } from './calendly';

describe('validateCalendlyCallback', () => {
  it('should validate a valid payload', () => {
    const payload = {
      assigned_to: 'Shawn R', // eslint-disable-line camelcase
      event_type_uuid: 'EHCHYDD5O22CQN2X', // eslint-disable-line camelcase
      event_type_name: 'Plato%20Check-In', // eslint-disable-line camelcase
      event_start_time: '2019-02-12T10%3A30%3A00-08%3A00', // eslint-disable-line camelcase
      event_end_time: '2019-02-12T11%3A00%3A00-08%3A00', // eslint-disable-line camelcase
      invitee_uuid: 'CBFU3YFELE3EVRIA', // eslint-disable-line camelcase
      invitee_full_name: 'Jake%20Colman', // eslint-disable-line camelcase
      invitee_email: 'jcolmanstage%40yopmail.com', // eslint-disable-line camelcase
    };
    const isValid = validateCalendlyCallback(payload);
    expect(isValid).toBe(true);
  });

  it('should invalidate a payload missing event_type_uuid', () => {
    const payload = {
      assigned_to: 'Shawn R', // eslint-disable-line camelcase
      event_type_name: 'Plato%20Check-In', // eslint-disable-line camelcase
      event_start_time: '2019-02-12T10%3A30%3A00-08%3A00', // eslint-disable-line camelcase
      event_end_time: '2019-02-12T11%3A00%3A00-08%3A00', // eslint-disable-line camelcase
      invitee_uuid: 'CBFU3YFELE3EVRIA', // eslint-disable-line camelcase
      invitee_full_name: 'Jake%20Colman', // eslint-disable-line camelcase
      invitee_email: 'jcolmanstage%40yopmail.com', // eslint-disable-line camelcase
    };
    const isValid = validateCalendlyCallback(payload);
    expect(isValid).toBe(false);
  });

  it('should invalidate a payload missing event_type_name', () => {
    const payload = {
      assigned_to: 'Shawn R', // eslint-disable-line camelcase
      event_type_uuid: 'EHCHYDD5O22CQN2X', // eslint-disable-line camelcase
      event_start_time: '2019-02-12T10%3A30%3A00-08%3A00', // eslint-disable-line camelcase
      event_end_time: '2019-02-12T11%3A00%3A00-08%3A00', // eslint-disable-line camelcase
      invitee_uuid: 'CBFU3YFELE3EVRIA', // eslint-disable-line camelcase
      invitee_full_name: 'Jake%20Colman', // eslint-disable-line camelcase
      invitee_email: 'jcolmanstage%40yopmail.com', // eslint-disable-line camelcase
    };
    const isValid = validateCalendlyCallback(payload);
    expect(isValid).toBe(false);
  });

  it('should invalidate a payload missing invitee_uuid', () => {
    const payload = {
      assigned_to: 'Shawn R', // eslint-disable-line camelcase
      event_type_uuid: 'EHCHYDD5O22CQN2X', // eslint-disable-line camelcase
      event_type_name: 'Plato%20Check-In', // eslint-disable-line camelcase
      event_start_time: '2019-02-12T10%3A30%3A00-08%3A00', // eslint-disable-line camelcase
      event_end_time: '2019-02-12T11%3A00%3A00-08%3A00', // eslint-disable-line camelcase
      invitee_full_name: 'Jake%20Colman', // eslint-disable-line camelcase
      invitee_email: 'jcolmanstage%40yopmail.com', // eslint-disable-line camelcase
    };
    const isValid = validateCalendlyCallback(payload);
    expect(isValid).toBe(false);
  });

  it('should invalidate a payload missing invitee_email', () => {
    const payload = {
      assigned_to: 'Shawn R', // eslint-disable-line camelcase
      event_type_uuid: 'EHCHYDD5O22CQN2X', // eslint-disable-line camelcase
      event_type_name: 'Plato%20Check-In', // eslint-disable-line camelcase
      event_start_time: '2019-02-12T10%3A30%3A00-08%3A00', // eslint-disable-line camelcase
      event_end_time: '2019-02-12T11%3A00%3A00-08%3A00', // eslint-disable-line camelcase
      invitee_uuid: 'CBFU3YFELE3EVRIA', // eslint-disable-line camelcase
      invitee_full_name: 'Jake%20Colman', // eslint-disable-line camelcase
    };
    const isValid = validateCalendlyCallback(payload);
    expect(isValid).toBe(false);
  });
});

describe('getCalendlyCheckInUrl', () => {
  it('should return a Calendly URL', () => {
    moment.tz.setDefault('GMT');
    const url = getCalendlyCheckInUrl(
      'test-id',
      'test-name',
      'test-email',
      new Date('2019-01-01T00:00:00Z')
    );
    expect(url).toBe(
      'https://calendly.com/platohq-qa/check-in/01-01-2019?email=test-email&name=test-name&utm_content=test-id&utm_medium=web-app&utm_source=embed'
    );
  });
});
