export function getPlanPriceByInterval(plan) {
  return (plan.amount / 100 / plan.interval_count).toFixed(0);
}
