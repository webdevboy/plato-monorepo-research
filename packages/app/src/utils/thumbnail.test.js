import getProfilePictureThumbnail from './thumbnail';

it('should return the thumbnail URL', () => {
  const thumbnailUrl = getProfilePictureThumbnail('test-name', 'test-thumbnail-url');
  expect(thumbnailUrl).toBe('test-thumbnail-url');
});

it('should return an URL with the name when the thumbnail URL is null', () => {
  const thumbnailUrl = getProfilePictureThumbnail('test-name');
  expect(thumbnailUrl).toBe('https://ui-avatars.com/api/?length=1&rounded=1&name=test-name');
});
