import { runSaga } from 'redux-saga';

/**
 * Record all dispatched action from a saga.
 * @param {Generator} saga A saga generator to capture.
 * @param {Action} initialAction The action dispatched to trigger the saga.
 * @returns {Array<Object>} An array of all the actions dispatched while running the saga.
 */
export async function recordSaga(saga, initialAction) {
  const dispatched = [];
  await runSaga(
    {
      dispatch: action => dispatched.push(action),
    },
    saga,
    initialAction
  ).done;
  return dispatched;
}
