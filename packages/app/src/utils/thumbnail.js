/**
 * Get a thumbnail URL with a fallback to a rounded image with the name initial.
 * @param {String} name The name to display when the thumbnail does not exists.
 * @param {String} [thumbnailUrl] The optional url of the thumbnail.
 * @returns {String} An URL to the thumbnail.
 */
function getProfilePictureThumbnail(name, thumbnailUrl = null) {
  if (thumbnailUrl) {
    return thumbnailUrl;
  }

  return `https://ui-avatars.com/api/?length=1&rounded=1&name=${name}`;
}

export default getProfilePictureThumbnail;
