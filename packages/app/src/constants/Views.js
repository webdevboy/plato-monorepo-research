export const views = Object.freeze({
  MENTEE: 'mentee',
  MENTOR: 'mentor',
  DASHBOARD_USER: 'dashboard_user',
});
