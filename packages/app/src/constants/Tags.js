export const ENGINEERING_MANAGER_TAG_ID = parseInt(
  process.env.REACT_APP_ENGINEERING_MANAGER_TAG_ID || 122,
  10
);
export const PRODUCT_MANAGER_TAG_ID = parseInt(
  process.env.REACT_APP_PRODUCT_MANAGER_TAG_ID || 123,
  10
);
