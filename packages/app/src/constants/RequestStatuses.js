export const CALL_DONE_REQUEST_STATUS = 'Call done';
export const CALL_SCHEDULED_REQUEST_STATUS = 'Call scheduled';
export const CALL_TO_RESCHEDULE_REQUEST_STATUS = 'Call to reschedule - Mentor';
export const SCHEDULING_IN_PROGRESS_REQUEST_STATUS = 'Scheduling in progress';
