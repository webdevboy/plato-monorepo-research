export const DEFAULT_IANA_TIME_ZONE =
  process.env.REACT_APP_DEFAULT_IANA_TIMEZONE || 'America/Los_Angeles';
