import hexToRgba from 'hex-rgba';

export default {
  fontFamily: "'Source Sans Pro', 'Helvetica Neue', 'Segoe UI', 'Helvetica', 'Arial', 'sans-serif'",
  palette: {
    white: '#FFF',
    black: '#222A3F',

    // notifications
    warn: '#ffad0d',
    success: '#0bb07b',
    error: '#DD2727',

    // reds
    sunglo: '#E16868',

    // greens
    emerald: '#52C38F',

    // yellows
    fuelYellow: '#EBAB28',

    // blues
    polar: '#F7FBFD',
    java: '#1BBFE5',
    cerulean: '#00A8CF',
    royalBlue: '#3D74DA',
    blueLagoon: '#067A95',
    ebonyClay: '#222A3F',
    mirage: '#1E2538',

    // greys
    aquaHaze: '#f2f6f8',
    geyser: '#D7DCE2',
    cadetBlue: '#ABB4BF',
    osloGrey: '#899099',

    withOpacity: (hexColor, opacity) => hexToRgba(hexColor, opacity),
  },
  transitions: {
    default: '300ms ease',
  },
  shadows: [
    'rgba(27, 126, 247, 0.1) 0px 2px 10px 0px',
    'rgba(30, 37, 56, 0.2) 0px 0px 40px 0px',
    'rgba(27, 126, 247, 0.1) 6px 6px 10px 0px;',
  ],
};
