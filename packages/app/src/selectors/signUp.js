/**
 * Get the data from the sign up form user input.
 * @param {Object} state The application state.
 * @returns {Object} The sign up form user input.
 */
export function getSignUpData(state) {
  return state.signUp;
}

/**
 * Get the price of the selected plan.
 * @param {Object} state The application state.
 * @returns {Number} The price of the selected product.
 */
export function getSignUpPlanPrice(state) {
  const {
    signUp: { plan },
  } = state;
  if (!plan) {
    return null;
  }
  return (plan.amount / 100).toFixed(2);
}

/**
 * Get the error message from the sign up action.
 * @param {Object} state The application state.
 * @returns {String} An error message.
 */
export function getSignUpErrorMessage(state) {
  return state.signUp.errorMessage;
}
