/**
 * Get a snackbar to display.
 * @param {Object} state The application state.
 * @returns {Object} An object of information for the snackbar.
 */
export function getSnackbar(state) {
  return state.snackbar;
}
