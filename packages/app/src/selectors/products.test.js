import { getProducts } from './products';

describe('getProducts', () => {
  it('should get the list of products', () => {
    const state = {
      products: [{ id: 'test-product-id-0' }, { id: 'test-product-id-1' }],
    };
    expect(getProducts(state)).toEqual([{ id: 'test-product-id-0' }, { id: 'test-product-id-1' }]);
  });
});
