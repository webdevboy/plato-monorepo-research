/**
 * Get the stats for the dashboard
 * @param {Object} state The application state.
 * @returns {Array<Object>} A list of organization stats information.
 */
export function getDashboardStats(state) {
  return state.dashboard.stats;
}

/**
 * Get the seats for the dashboard
 * @param {Object} state The application state.
 * @returns {Array<Object>} A list of organization seats information.
 */
export function getDashboardSeats(state) {
  return state.dashboard.seats;
}

/**
 * Get the common challenges for the dashboard
 * @param {Object} state The application state.
 * @returns {Array<Object>} A list of organization common challenges information.
 */
export function getDashboardCommonChallenges(state) {
  return state.dashboard.commonChallenges
    .sort(
      (cc1, cc2) =>
        cc2.nbCallDoneRequests +
        cc2.nbCallDoneAttendances -
        (cc1.nbCallDoneRequests + cc1.nbCallDoneAttendances)
    )
    .slice(0, 10);
}

/**
 * Get the qualitative feedback for the dashboard
 * @param {Object} state The application state.
 * @returns {Array<Object>} A list of organization qualitative feedback information.
 */
export function getDashboardQualitativeFeedback(state) {
  return state.dashboard.qualitativeFeedback
    .sort((f1, f2) => f2.callScheduledAt - f1.callScheduledAt)
    .slice(0, 10);
}
