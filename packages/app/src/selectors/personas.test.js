import { getPersonas, getPersonasList } from './personas';

describe('getPersonas', () => {
  it('should get a mapping of personas', () => {
    const state = {
      personas: {
        'test-persona-id-0': { id: 'test-persona-id-0' },
        'test-persona-id-1': { id: 'test-persona-id-1' },
      },
    };
    expect(getPersonas(state)).toEqual({
      'test-persona-id-0': { id: 'test-persona-id-0' },
      'test-persona-id-1': { id: 'test-persona-id-1' },
    });
  });
});

describe('getPersonasList', () => {
  it('should get the list of personas', () => {
    const state = {
      personas: {
        'test-persona-id-0': {
          id: 'test-persona-id-0',
          challenges: [{ id: 'test-challenge-id-0' }],
        },
        'test-persona-id-1': {
          id: 'test-persona-id-1',
          challenges: [{ id: 'test-challenge-id-1' }],
        },
      },
    };
    expect(getPersonasList(state)).toEqual([
      { id: 'test-persona-id-0', challenges: [{ id: 'test-challenge-id-0' }] },
      { id: 'test-persona-id-1', challenges: [{ id: 'test-challenge-id-1' }] },
    ]);
  });

  it('should filter out personas without challenges', () => {
    const state = {
      personas: {
        'test-persona-id-0': { id: 'test-persona-id-0', challenges: [] },
        'test-persona-id-1': {
          id: 'test-persona-id-1',
          challenges: [{ id: 'test-challenge-id-1' }],
        },
      },
    };
    expect(getPersonasList(state)).toEqual([
      { id: 'test-persona-id-1', challenges: [{ id: 'test-challenge-id-1' }] },
    ]);
  });
});
