import {
  getActiveDashboardMenteeCount,
  getAverageGrade,
  getNumMeetings,
  getSorter,
  searchDashboardMentees,
  sortByEngagement,
  sortBySatisfaction,
} from './dashboardMentees';

import performSearch from 'utils/search';

jest.mock('utils/search');

const dashboardMentees = [
  {
    mentee: { id: 1 },
    statistics: {
      nbCallDoneRequests: 1,
      nbGradedRequests: 1,
      nbCallDoneAttendances: 2,
      nbGradedAttendances: 2,
      requestGradeAverage: 5,
      attendanceGradeAverage: 7,
    },
  },
  {
    mentee: { id: 2 },
    statistics: {
      nbCallDoneRequests: 0,
      nbGradedRequests: 0,
      nbCallDoneAttendances: 0,
      nbGradedAttendances: 0,
      requestGradeAverage: null,
      attendanceGradeAverage: null,
    },
  },
  {
    mentee: { id: 3 },
    statistics: {
      nbCallDoneRequests: 4,
      nbGradedRequests: 0,
      nbCallDoneAttendances: 5,
      nbGradedAttendances: 0,
      requestGradeAverage: 1,
      attendanceGradeAverage: 1,
    },
  },
];

describe('getAverageGrade', () => {
  const testCases = [
    {
      input: {
        statistics: {
          nbGradedRequests: 0,
          nbGradedAttendances: 0,
          requestGradeAverage: null,
          attendanceGradeAverage: null,
        },
      },
      expected: undefined,
    },
    {
      input: {
        statistics: {
          nbGradedRequests: 10,
          nbGradedAttendances: 0,
          requestGradeAverage: 5,
          attendanceGradeAverage: null,
        },
      },
      expected: 5,
    },
    {
      input: {
        statistics: {
          nbGradedRequests: 0,
          nbGradedAttendances: 10,
          requestGradeAverage: null,
          attendanceGradeAverage: 5,
        },
      },
      expected: 5,
    },
    {
      input: {
        statistics: {
          nbGradedRequests: 10,
          nbGradedAttendances: 10,
          requestGradeAverage: 5,
          attendanceGradeAverage: 5,
        },
      },
      expected: 5,
    },
    {
      input: {
        statistics: {
          nbGradedRequests: 10,
          nbGradedAttendances: 10,
          requestGradeAverage: 0,
          attendanceGradeAverage: 10,
        },
      },
      expected: 5,
    },
    {
      input: {
        statistics: {
          nbGradedRequests: 1,
          nbGradedAttendances: 0,
          requestGradeAverage: 7,
          attendanceGradeAverage: null,
        },
      },
      expected: 7,
    },
    {
      input: {
        statistics: {
          nbGradedRequests: 1,
          nbGradedAttendances: 2,
          requestGradeAverage: 8.5,
          attendanceGradeAverage: 4.2,
        },
      },
      expected: 5.6,
    },
  ];

  testCases.forEach(
    ({
      input,
      input: {
        statistics: {
          nbGradedRequests,
          nbGradedAttendances,
          requestGradeAverage,
          attendanceGradeAverage,
        },
      },
      expected,
    }) => {
      it(`should return ${expected} when nbGradedRequests=${nbGradedRequests}, nbGradedAttendances=${nbGradedAttendances}, requestGradeAverage=${requestGradeAverage}, attendanceGradeAverage=${attendanceGradeAverage}`, () => {
        const actual = getAverageGrade(input);

        expect(expected).toEqual(actual);
      });
    }
  );
});

describe('searchDashboardMentees', () => {
  it('should return all mentees if search string is empty', () => {
    const query = '';
    expect(searchDashboardMentees(dashboardMentees, query)).toEqual(dashboardMentees);
  });

  it('should search on the expected values when the search string is not empty', () => {
    searchDashboardMentees(dashboardMentees, 'search');

    expect(performSearch).toHaveBeenCalledWith(dashboardMentees, 'search', [
      'mentee.firstName',
      'mentee.lastName',
      'mentee.email',
      'mentee.title',
    ]);
  });
});

describe('getNumMeetings', () => {
  it('should return the number of meetings for the given dashboard user', () => {
    const dashboardMentee = {
      statistics: {
        nbCallDoneRequests: 3,
        nbCallDoneAttendances: 5,
      },
    };
    expect(getNumMeetings(dashboardMentee)).toEqual(8);
  });
});

describe('sortByEngagement', () => {
  it('should sort by engagement', () => {
    const sorted = sortByEngagement(dashboardMentees);

    expect(sorted.map(dm => dm.mentee.id)).toEqual([3, 1, 2]);
  });
});

describe('sortBySatisfaction', () => {
  it('should sort by satisfaction', () => {
    const sorted = sortBySatisfaction(dashboardMentees);

    expect(sorted.map(dm => dm.mentee.id)).toEqual([1, 3, 2]);
  });
});

describe('getSorter', () => {
  it('should return engagement sort by default', () => {
    const sorter = getSorter();

    expect(sorter).toEqual(sortByEngagement);
  });

  it(`should return engagement sort when argument is 'engagement'`, () => {
    const sorter = getSorter('engagement');

    expect(sorter).toEqual(sortByEngagement);
  });

  it(`should return satisfaction sort when argument is 'satisfaction'`, () => {
    const sorter = getSorter('satisfaction');

    expect(sorter).toEqual(sortBySatisfaction);
  });

  it(`should return engagement sort when argument is unknown`, () => {
    const sorter = getSorter('oupsidou');

    expect(sorter).toEqual(sortByEngagement);
  });
});

describe('getActiveDashboardMenteeCount', () => {
  const state = {
    dashboardMentees: {
      list: dashboardMentees,
      activeMenteeCount: 3,
    },
  };

  it('should return the number of active mentees', () => {
    const activeMenteeCount = getActiveDashboardMenteeCount(state);
    expect(activeMenteeCount).toEqual(3);
  });
});
