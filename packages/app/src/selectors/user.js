import moment from 'moment-timezone';

export function getIsMenteeProfileCompleted(state) {
  return Boolean(state.user && state.user.isProfileComplete);
}

export function getUserTimeZone(state) {
  return state.user.timeZone || moment.tz.guess();
}

export function getUser(state) {
  return state.user;
}

export function getIsProductManager(state) {
  const { faces } = state.user;
  return faces ? faces.some(face => face.name === 'PM') : false;
}

export function getUserMenteeId(state) {
  return state.user.menteeId;
}

export function getUserFaces(state) {
  return state.user.faces.map(face => face.id).join(',');
}

export function getUserTags(state) {
  return state.user.tags.map(tag => tag.id).join(',');
}
