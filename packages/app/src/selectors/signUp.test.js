import { getSignUpData, getSignUpErrorMessage, getSignUpPlanPrice } from './signUp';

describe('getSignUpData', () => {
  it('should get the data', () => {
    const state = {
      signUp: {
        companyName: 'test-company-name',
        email: 'test-email',
      },
    };
    expect(getSignUpData(state)).toEqual({
      companyName: 'test-company-name',
      email: 'test-email',
    });
  });
});

describe('getSignUpErrorMessage', () => {
  it('should get the error message', () => {
    const state = {
      signUp: { errorMessage: 'test-error-message' },
    };
    expect(getSignUpErrorMessage(state)).toBe('test-error-message');
  });
});

describe('getSignUpPlanPrice', () => {
  it('should get the plan price', () => {
    const state = {
      signUp: { plan: { amount: 4200 } },
    };
    expect(getSignUpPlanPrice(state)).toBe('42.00');
  });

  it('should handle a missing plan', () => {
    const state = {
      signUp: {},
    };
    expect(getSignUpPlanPrice(state)).toBe(null);
  });
});
