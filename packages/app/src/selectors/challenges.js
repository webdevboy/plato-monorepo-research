import { ONBOARDING_CHALLENGE_IDS } from 'constants/Onboarding';

/**
 * Get the list of common challenges enriched with the list of the user persona.
 * @param {Object} state The application state.
 * @returns {Array<Object>} A list of challenges.
 */
export function getOnboardingChallenges(state) {
  const { persona } = state.user;
  const challenges = Object.values(state.challenges);
  const onboardingChallengeIds = ONBOARDING_CHALLENGE_IDS.split(',').map(id => +id);
  const personaChallengeIds = persona ? persona.challenges.map(({ id }) => id) : [];
  const onboardingChallenges = challenges.filter(
    challenge =>
      onboardingChallengeIds.includes(challenge.id) && !personaChallengeIds.includes(challenge.id)
  );
  return persona ? [...onboardingChallenges, ...persona.challenges] : onboardingChallenges;
}
