/**
 * Get the list of face tags.
 * @param {Object} state The application state.
 * @returns {Array<Object>} The list of face tags.
 */
export function getFaceTags(state) {
  return Object.values(state.tags).filter(tag => tag.type === 'Face');
}

/**
 * Get the list of industry tags.
 * @param {Object} state The application state.
 * @returns {Array<Object>} The list of industry tags.
 */
export function getIndustryTags(state) {
  return Object.values(state.tags).filter(tag => tag.type === 'Industry');
}
