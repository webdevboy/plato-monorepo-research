/**
 * Get the list of Slack user IDs from the team Slack installation.
 * @param {Object} state The application state.
 * @returns {Array<String>} A list of Slack user IDs.
 */
export function getSlackUsers(state) {
  return state.slack.users;
}
