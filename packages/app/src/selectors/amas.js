/**
 * Get all the AMAs as a mapping of the AMA ID to the AMA.
 * @param {Object} state The application state.
 * @returns {Object} A mapping of the AMA ID to the AMA.
 */
export function getAmas(state) {
  return state.amas;
}

/**
 * Get all the AMAs as a mapping of the AMA ID to the AMA.
 * @param {Object} state The application state.
 * @returns {Object} A mapping of the AMA ID to the AMA.
 */
export function getAmasList(state) {
  return Object.values(state.amas);
}
