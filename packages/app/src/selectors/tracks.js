/**
 * Get a map of all the track ID to the track.
 * @param {Object} state The application state.
 * @returns {Object} A map of track ID to the track.
 */
export function getTracks(state) {
  return state.tracks;
}

/**
 * Get the list of tracks.
 * @param {Object} state The application state.
 * @param {String} trackId The ID of the track to retrieve.
 * @returns {Array<Track>} An array of track.
 */
export function getTrack(state, trackId) {
  return state.tracks[trackId];
}

/**
 * Get the list of program.
 * @todo Filter and sort server side.
 * @param {Object} state The application state.
 * @returns {Array<Program>} An array of program.
 */
export function getPrograms(state) {
  return Object.values(state.programs)
    .filter(program => program.status === 'Open')
    .sort((programA, programB) => {
      return programA.startDate - programB.startDate;
    });
}

/**
 * Get a program from an ID.
 * @param {Object} state The application state.
 * @param {String} programId The ID of the program to retrieve.
 * @returns {Program} An array of track.
 */
export function getProgram(state, programId) {
  return state.programs[programId];
}

/**
 * Get a list of AMA for a program.
 * @todo Sort server side.
 * @param {Object} state The application state.
 * @param {String} programId The ID of the program.
 * @returns {Array<Ama>} An array of ama.
 */
export function getProgramAmas(state, programId) {
  return (((state.programs || [])[programId] || []).amas || []).sort(
    (amaA, amaB) => amaA.startDate - amaB.startDate
  );
}
