import performSearch from 'utils/search';
import roundTo from 'round-to';

export function getAverageGrade(dashboardMentee) {
  const {
    nbGradedRequests,
    nbGradedAttendances,
    requestGradeAverage,
    attendanceGradeAverage,
  } = dashboardMentee.statistics;

  const weightedRequestGradeAverage = requestGradeAverage * nbGradedRequests;
  const weightedAttendanceGradeAverage = attendanceGradeAverage * nbGradedAttendances;
  const totalCalls = nbGradedRequests + nbGradedAttendances;

  if (totalCalls === 0) {
    return undefined;
  }

  return roundTo((weightedRequestGradeAverage + weightedAttendanceGradeAverage) / totalCalls, 1);
}

export function searchDashboardMentees(dashboardMentees, query) {
  if (!query) {
    return dashboardMentees;
  }

  return performSearch(dashboardMentees, query, [
    'mentee.firstName',
    'mentee.lastName',
    'mentee.email',
    'mentee.title',
  ]);
}

export function getNumMeetings(dashboardMentee) {
  const { nbCallDoneRequests, nbCallDoneAttendances } = dashboardMentee.statistics;
  return nbCallDoneRequests + nbCallDoneAttendances;
}

export function sortByEngagement(dashboardMentees) {
  return dashboardMentees.sort((dm1, dm2) => getNumMeetings(dm2) - getNumMeetings(dm1));
}

export function sortBySatisfaction(dashboardMentees) {
  return dashboardMentees.sort(
    (dm1, dm2) => (getAverageGrade(dm2) || 0) - (getAverageGrade(dm1) || 0)
  );
}

export function getSorter(sortBy) {
  switch (sortBy) {
    case 'engagement':
      return sortByEngagement;
    case 'satisfaction':
      return sortBySatisfaction;
    default:
      return sortByEngagement;
  }
}

export function getEnrichedDashboardMentees(dashboardMentees) {
  return dashboardMentees.map(dashboardMentee => ({
    ...dashboardMentee,
    averageGrade: getAverageGrade(dashboardMentee),
    numMeetings: getNumMeetings(dashboardMentee),
  }));
}

/**
 * Get the mentees for the dashboard
 * @param {Object} state The application state.
 * @returns {Array<Object>} A list of mentee information.
 */
export function getDashboardMentees(state) {
  const {
    dashboardMentees: { sortBy, list, query },
  } = state;

  const sort = getSorter(sortBy);
  if (query.length > 0) {
    const searchResults = searchDashboardMentees(list, query);
    return getEnrichedDashboardMentees(sort(searchResults));
  }

  const onboardedMentees = list.filter(({ mentee }) => mentee.isProfileComplete);
  const nonOnboardedMentees = list.filter(({ mentee }) => !mentee.isProfileComplete);

  return getEnrichedDashboardMentees([...sort(onboardedMentees), ...nonOnboardedMentees]);
}

/**
 * Get the dashboard mentees sort by value
 * @param {Object} state The application state.
 * @returns {String} The current sort by value;
 */
export function getDashboardMenteesSortBy(state) {
  return state.dashboardMentees.sortBy;
}

/**
 * Get the dashboard mentees query value
 * @param {Object} state The application state.
 * @returns {String} The current query value.
 */
export function getDashboardMenteesQuery(state) {
  return state.dashboardMentees.query;
}

/**
 * Get the number of active mentees for the organization
 * @param {Object} state The application state.
 * @returns {Number} The number of active mentees.
 */
export function getActiveDashboardMenteeCount(state) {
  return state.dashboardMentees.activeMenteeCount;
}

/**
 * Get the slack users for the dashboard
 * @param {Object} state The application state.
 * @returns {Array<Object>} The slack users for the current dashboard.
 */
export function getDashboardSlackUsers(state) {
  return state.dashboardMentees.slackUsers;
}

/**
 * Get the open/closed state of the add mentee dialog
 * @param {Object} state The application state.
 * @returns {Boolean} The open/closed state of the add mentee dialog.
 */
export function getIsAddMenteeDialogOpen(state) {
  return state.dashboardMentees.isAddMenteeDialogOpen;
}
