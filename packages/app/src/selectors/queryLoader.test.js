import { isActionLoading, isQueryLoaderVisible } from './queryLoader';

import { createAction } from 'redux-actions';

describe('isQueryLoaderVisible', () => {
  it('should get whether the query loader is showing', () => {
    const state = {
      queryLoader: { isVisible: true },
    };
    expect(isQueryLoaderVisible(state)).toBe(true);
  });
});

describe('isActionLoading', () => {
  it('should return true when the action is loading', () => {
    const doSomething = createAction('DO_SOMETHING');
    const state = {
      queryLoader: { DO_SOMETHING: true },
    };
    expect(isActionLoading(state, doSomething)).toBe(true);
  });

  it('should return false when the action is done loading', () => {
    const doSomething = createAction('DO_SOMETHING');
    const state = {
      queryLoader: { DO_SOMETHING: false },
    };
    expect(isActionLoading(state, doSomething)).toBe(false);
  });

  it('should return false when the action is not loading', () => {
    const doSomething = createAction('DO_SOMETHING');
    const state = { queryLoader: {} };
    expect(isActionLoading(state, doSomething)).toBe(false);
  });
});
