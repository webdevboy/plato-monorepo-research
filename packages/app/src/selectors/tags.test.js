import { ENGINEERING_MANAGER_TAG_ID, PRODUCT_MANAGER_TAG_ID } from 'constants/Tags';

import { getFaceTags } from './tags';

describe('getFaceTags', () => {
  it('should return the list of face tags', () => {
    const state = {
      tags: {
        [ENGINEERING_MANAGER_TAG_ID]: {
          id: ENGINEERING_MANAGER_TAG_ID,
          name: 'Engineering Manager',
          type: 'Face',
        },
        [PRODUCT_MANAGER_TAG_ID]: {
          id: PRODUCT_MANAGER_TAG_ID,
          name: 'Product Manager',
          type: 'Face',
        },
      },
    };
    expect(getFaceTags(state)).toEqual([
      {
        id: ENGINEERING_MANAGER_TAG_ID,
        name: 'Engineering Manager',
        type: 'Face',
      },
      {
        id: PRODUCT_MANAGER_TAG_ID,
        name: 'Product Manager',
        type: 'Face',
      },
    ]);
  });
});
