/**
 * Get whether we should display the query loader.
 * @param {Object} state The application state.
 * @returns {Boolean} Whether the app should display the query loader.
 */
export function isQueryLoaderVisible(state) {
  return Boolean(state.queryLoader.isVisible);
}

/**
 * Get whether an asynchronous action is in progress.
 * @param {Object} state The application state.
 * @param {Action} action The action to verify.
 * @returns {Boolean} Whether the action is being performed.
 */
export function isActionLoading(state, action) {
  return Boolean(state.queryLoader[action.toString()]);
}
