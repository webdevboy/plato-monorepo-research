import { getSlackUsers } from './slack';

describe('getSlackUsers', () => {
  it('should get the list of Slack user IDs', () => {
    const state = {
      slack: { users: ['test-slack-user-id-0', 'test-slack-user-id-1'] },
    };
    expect(getSlackUsers(state)).toEqual(['test-slack-user-id-0', 'test-slack-user-id-1']);
  });
});
