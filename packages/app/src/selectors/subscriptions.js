/**
 * Get the current list of organization subscriptions
 * @param {Object} state The application state.
 * @returns {Array<Object>} A list of subscriptions
 */
export function getSubscriptions(state) {
  return state.subscriptions;
}
