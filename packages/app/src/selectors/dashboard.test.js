import {
  getDashboardCommonChallenges,
  getDashboardQualitativeFeedback,
  getDashboardSeats,
  getDashboardStats,
} from './dashboard';

describe('getDashboardStats', () => {
  const stats = [
    {
      month: '04/2017',
      nbCallDoneRequests: 11,
      nbGradedRequests: 11,
      nbCallDoneAttendances: 1,
      nbGradedAttendances: 1,
      requestGradeAverage: 5.5,
      attendanceGradeAverage: 4.5,
    },
    {
      month: '05/2017',
      nbCallDoneRequests: 10,
      nbGradedRequests: 10,
      nbCallDoneAttendances: 2,
      nbGradedAttendances: 2,
      requestGradeAverage: 5,
      attendanceGradeAverage: 5,
    },
  ];

  it('should get stats', () => {
    const state = {
      dashboard: {
        stats,
      },
    };
    expect(getDashboardStats(state)).toEqual(stats);
  });
});

describe('getDashboardSeats', () => {
  const seats = {
    nbTakenSeats: 7,
    nbPaidSeats: 8,
    nbOnboardedSeats: 4,
  };

  it('should get seats', () => {
    const state = {
      dashboard: {
        seats,
      },
    };
    expect(getDashboardSeats(state)).toEqual(seats);
  });
});

describe('getDashboardQualitativeFeedback', () => {
  const qualitativeFeedback = [
    {
      qualitativeFeedback: 'Feedback 1',
      callScheduledAt: 1,
    },
    {
      qualitativeFeedback: 'Feedback 2',
      callScheduledAt: 2,
    },
    {
      qualitativeFeedback: 'Feedback 3',
      callScheduledAt: 3,
    },
    {
      qualitativeFeedback: 'Feedback 4',
      callScheduledAt: 4,
    },
    {
      qualitativeFeedback: 'Feedback 5',
      callScheduledAt: 5,
    },
    {
      qualitativeFeedback: 'Feedback 6',
      callScheduledAt: 6,
    },
    {
      qualitativeFeedback: 'Feedback 7',
      callScheduledAt: 7,
    },
    {
      qualitativeFeedback: 'Feedback 8',
      callScheduledAt: 8,
    },
    {
      qualitativeFeedback: 'Feedback 9',
      callScheduledAt: 9,
    },
    {
      qualitativeFeedback: 'Feedback 10',
      callScheduledAt: 10,
    },
    {
      qualitativeFeedback: 'Feedback 11',
      callScheduledAt: 11,
    },
  ];

  const state = {
    dashboard: {
      qualitativeFeedback,
    },
  };

  it('should get a maximum of 10 qualitative feedbacks in order from most to least recent', () => {
    expect(
      getDashboardQualitativeFeedback(state).map(feedback => feedback.qualitativeFeedback)
    ).toEqual([
      'Feedback 11',
      'Feedback 10',
      'Feedback 9',
      'Feedback 8',
      'Feedback 7',
      'Feedback 6',
      'Feedback 5',
      'Feedback 4',
      'Feedback 3',
      'Feedback 2',
    ]);
  });
});

describe('getDashboardCommonChallenges', () => {
  const commonChallenges = [
    {
      challengeTitle: 'Challenge 1',
      nbCallDoneRequests: 0,
      nbCallDoneAttendances: 1,
    },
    {
      challengeTitle: 'Challenge 11',
      nbCallDoneRequests: 11,
      nbCallDoneAttendances: 0,
    },
    {
      challengeTitle: 'Challenge 2',
      nbCallDoneRequests: 2,
      nbCallDoneAttendances: 0,
    },
    {
      challengeTitle: 'Challenge 10',
      nbCallDoneRequests: 9,
      nbCallDoneAttendances: 1,
    },
    {
      challengeTitle: 'Challenge 3',
      nbCallDoneRequests: 1,
      nbCallDoneAttendances: 2,
    },
    {
      challengeTitle: 'Challenge 9',
      nbCallDoneRequests: 7,
      nbCallDoneAttendances: 2,
    },
    {
      challengeTitle: 'Challenge 4',
      nbCallDoneRequests: 3,
      nbCallDoneAttendances: 1,
    },
    {
      challengeTitle: 'Challenge 8',
      nbCallDoneRequests: 5,
      nbCallDoneAttendances: 3,
    },
    {
      challengeTitle: 'Challenge 5',
      nbCallDoneRequests: 2,
      nbCallDoneAttendances: 3,
    },
    {
      challengeTitle: 'Challenge 7',
      nbCallDoneRequests: 3,
      nbCallDoneAttendances: 4,
    },
    {
      challengeTitle: 'Challenge 6',
      nbCallDoneRequests: 4,
      nbCallDoneAttendances: 2,
    },
  ];

  it('should get a maximum of 10 common challenges in descending meeting count order', () => {
    const state = {
      dashboard: {
        commonChallenges,
      },
    };
    expect(getDashboardCommonChallenges(state).map(cc => cc.challengeTitle)).toEqual([
      'Challenge 11',
      'Challenge 10',
      'Challenge 9',
      'Challenge 8',
      'Challenge 7',
      'Challenge 6',
      'Challenge 5',
      'Challenge 4',
      'Challenge 3',
      'Challenge 2',
    ]);
  });
});
