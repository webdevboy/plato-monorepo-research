/**
 * Get the list of payment plans.
 * @param {Object} state The application state.
 * @returns {Array<Object>} A list of payment plans information.
 */
export function getProducts(state) {
  return state.products;
}
