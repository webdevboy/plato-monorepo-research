/**
 * Get all the hierarchies.
 * @param {Object} state The application state.
 * @returns {Array} A list of hierarchies.
 */
export function getHierarchies(state) {
  return state.hierarchies;
}

/**
 * Get all hierarchies 2 levels above a given hierarchy.
 * @param {Object} state The application state.
 * @returns {Array} A list of hierarchies.
 */
export function getHierarchiesForMentorSearch(state) {
  const upperBound = state.hierarchies.length - 3;
  const pos = state.hierarchies.indexOf(state.user.hierarchy);

  if (pos >= upperBound) {
    return state.hierarchies.slice(upperBound);
  }

  return state.hierarchies.slice(pos, pos + 3);
}
