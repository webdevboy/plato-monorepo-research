/**
 * Get all the personas as a mapping of the persona ID to the persona.
 * @param {Object} state The application state.
 * @returns {Object} A mapping of the persona ID to the persona.
 */
export function getPersonas(state) {
  return state.personas;
}

/**
 * Get all the personas as an array or persona.
 * @param {Object} state The application state.
 * @returns {Array<Persona>} An array of persona.
 */
export function getPersonasList(state) {
  return Object.values(state.personas).filter(persona => persona.challenges.length !== 0);
}

/**
 * Get the persona of a user.
 * @param {Object} state The application state.
 * @returns {<Persona>} A persona object of the user.
 */
export function getUserPersona(state) {
  return state.user.persona;
}
