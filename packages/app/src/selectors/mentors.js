/**
 * Get a list of mentors.
 * @param {Object} state The application state.
 * @returns {Array<Object>} A list of mentors.
 */
export function getMentorsList(state) {
  return Object.values(state.mentors);
}
/**
 * Get a list of mentors.
 * @param {Object} state The application state.
 * @returns {Array<Object>} A list of mentors.
 */
export function getMentors(state) {
  return state.mentors;
}

export function getMentor(state, mentorId) {
  return state.mentors[mentorId];
}

export function getMentorPictureUrl(state, mentorId) {
  return (
    (state.mentors[mentorId] &&
      state.mentors[mentorId].picture &&
      state.mentors[mentorId].picture.originalUrl) ||
    undefined
  );
}

export function getMentorRelationshipSlots(state, mentorId) {
  if (!state.mentors[mentorId] || !state.mentors[mentorId].relationshipSlots) {
    return [];
  }

  return state.mentors[mentorId].relationshipSlots;
}
