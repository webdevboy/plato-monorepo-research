import { applyMiddleware, compose, createStore } from 'redux';

import { ENVIRONMENT } from 'constants/Environment';
import { createLogger } from 'redux-logger';
import createSagaMiddleware from 'redux-saga';
import history from 'utils/history';
import mainReducer from 'ducks';
import mySaga from 'sagas';
import { routerMiddleware } from 'connected-react-router';

const sagaMiddleware = createSagaMiddleware();
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const middleware = [routerMiddleware(history), sagaMiddleware];

if (ENVIRONMENT === 'development') {
  middleware.push(createLogger({ collapsed: true, duration: true }));
}

export default createStore(mainReducer(history), composeEnhancers(applyMiddleware(...middleware)));

sagaMiddleware.run(mySaga);
