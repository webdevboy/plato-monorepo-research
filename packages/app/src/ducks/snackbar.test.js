import { resetPasswordCompleted, updatePasswordCompleted } from './user';
import snackbarReducer, { hideSnackbar, showSnackbar } from './snackbar';

it('should return the initial state', () => {
  expect(snackbarReducer(undefined, {})).toEqual(null);
});

describe('showSnackbar', () => {
  it('should set the snackbar with a message and a variant', () => {
    expect(
      snackbarReducer(null, showSnackbar({ message: 'test-message', variant: 'test-variant' }))
    ).toEqual({
      message: 'test-message',
      variant: 'test-variant',
      isVisible: true,
    });
  });
});

describe('hideSnackbar', () => {
  it('should set the snackbar with a message and a variant', () => {
    expect(snackbarReducer(null, hideSnackbar())).toEqual({
      isVisible: false,
    });
  });
});

describe('resetPasswordCompleted', () => {
  it('should set the snackbar', () => {
    expect(snackbarReducer(null, resetPasswordCompleted())).toEqual({
      message: 'Password successfully reset!',
      variant: 'success',
      isVisible: true,
    });
  });

  it('should do nothing on error', () => {
    expect(snackbarReducer(null, resetPasswordCompleted(new Error()))).toEqual(null);
  });
});

describe('updatePasswordCompleted', () => {
  it('should set the snackbar', () => {
    expect(snackbarReducer(null, updatePasswordCompleted())).toEqual({
      message: 'Check your email for a password reset link!',
      variant: 'success',
      isVisible: true,
    });
  });

  it('should do nothing on error', () => {
    expect(snackbarReducer(null, updatePasswordCompleted(new Error()))).toEqual(null);
  });
});
