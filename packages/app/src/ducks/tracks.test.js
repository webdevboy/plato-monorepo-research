import tracksReducer, { fetchTracksCompleted } from './tracks';

const defaultState = {};

it('should return the initial state', () => {
  expect(tracksReducer(undefined, {})).toEqual(defaultState);
});

describe('fetchTracksCompleted', () => {
  it('should set the list of tracks', () => {
    expect(
      tracksReducer(
        defaultState,
        fetchTracksCompleted([{ id: 'test-track-id-0' }, { id: 'test-track-id-1' }])
      )
    ).toEqual({
      'test-track-id-0': { id: 'test-track-id-0' },
      'test-track-id-1': { id: 'test-track-id-1' },
    });
  });

  it('should do nothing on error', () => {
    expect(tracksReducer(defaultState, fetchTracksCompleted(new Error()))).toEqual(defaultState);
  });
});
