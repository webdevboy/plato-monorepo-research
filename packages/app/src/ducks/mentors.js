import { createAction, handleActions } from 'redux-actions';

export const fetchMentor = createAction('FETCH_MENTOR');
export const fetchMentorCompleted = createAction('FETCH_MENTOR_COMPLETED');

export const fetchMentors = createAction('FETCH_MENTORS');
export const fetchMentorsCompleted = createAction('FETCH_MENTORS_COMPLETED');

export const fetchMentorRelationshipSlots = createAction('FETCH_MENTOR_RELATIONSHIP_SLOTS');
export const fetchMentorRelationshipSlotsCompleted = createAction(
  'FETCH_MENTOR_RELATIONSHIP_SLOTS_COMPLETED'
);

export default handleActions(
  {
    [fetchMentorsCompleted]: {
      next: (state, { payload }) =>
        payload.reduce(
          (acc, mentor) => ({
            ...acc,
            [mentor.id]: state[mentor.id] ? { ...state[mentor.id], ...mentor } : mentor,
          }),
          state
        ),
    },

    [fetchMentorCompleted]: {
      next: (state, { payload }) => ({
        ...state,
        [payload.id]: state[payload.id] ? { ...state[payload.id], ...payload } : payload,
      }),
    },

    [fetchMentorRelationshipSlotsCompleted]: {
      next: (state, { payload }) => ({
        ...state,
        [payload.mentorId]: state[payload.mentorId]
          ? { ...state[payload.mentorId], relationshipSlots: payload.slots }
          : { relationshipSlots: payload.slots },
      }),
    },
  },
  {}
);
