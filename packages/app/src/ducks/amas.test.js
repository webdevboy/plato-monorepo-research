import reducer, { fetchAmaAttendancesCompleted, fetchAmasCompleted } from './amas';

const defaultState = {};

it('should return the initial state', () => {
  expect(reducer(undefined, {})).toEqual(defaultState);
});

describe('fetchAmasCompleted', () => {
  it('should set the list of amas', () => {
    expect(
      reducer(defaultState, fetchAmasCompleted([{ id: 'test-ama-id-0' }, { id: 'test-ama-id-1' }]))
    ).toEqual({
      'test-ama-id-0': { id: 'test-ama-id-0' },
      'test-ama-id-1': { id: 'test-ama-id-1' },
    });
  });

  it('should do nothing on error', () => {
    expect(reducer(defaultState, fetchAmasCompleted(new Error()))).toEqual(defaultState);
  });
});

describe('fetchAmaAttendancesCompleted', () => {
  it('should set the list of amas', () => {
    expect(
      reducer(
        defaultState,
        fetchAmaAttendancesCompleted({
          attendances: [{ id: 'test-ama-attendance-id-0' }],
          amaId: 'test-ama-id-0',
        })
      )
    ).toEqual({
      'test-ama-id-0': {
        attendances: [{ id: 'test-ama-attendance-id-0' }],
        amaId: 'test-ama-id-0',
      },
    });
  });

  it('should enrich an existing ama', () => {
    expect(
      reducer(
        {
          'test-ama-id-0': {
            id: 'test-ama-id-0',
          },
        },
        fetchAmaAttendancesCompleted({
          attendances: [{ id: 'test-ama-attendance-id-0' }],
          amaId: 'test-ama-id-0',
        })
      )
    ).toEqual({
      'test-ama-id-0': {
        id: 'test-ama-id-0',
        attendances: [{ id: 'test-ama-attendance-id-0' }],
        amaId: 'test-ama-id-0',
      },
    });
  });

  it('should do nothing on error', () => {
    expect(reducer(defaultState, fetchAmaAttendancesCompleted(new Error()))).toEqual(defaultState);
  });
});
