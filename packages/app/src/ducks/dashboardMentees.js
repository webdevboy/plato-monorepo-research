import { createAction, handleActions } from 'redux-actions';

export const fetchDashboardMentees = createAction('FETCH_DASHBOARD_MENTEES');
export const fetchDashboardMenteesCompleted = createAction('FETCH_DASHBOARD_MENTEES_COMPLETED');

export const fetchDashboardSlackUserUser = createAction('FETCH_DASHBOARD_SLACK_USER_USER');
export const fetchDashboardSlackUserUserCompleted = createAction(
  'FETCH_DASHBOARD_SLACK_USER_USER_COMPLETED'
);

export const fetchDashboardSlackUsers = createAction('FETCH_DASHBOARD_SLACK_USERS');
export const fetchDashboardSlackUsersCompleted = createAction(
  'FETCH_DASHBOARD_SLACK_USERS_COMPLETED'
);

export const updateDashboardMenteesSortBy = createAction('UPDATE_DASHBOARD_MENTEES_SORT_BY');
export const updateDashboardMenteesQuery = createAction('UPDATE_DASHBOARD_MENTEES_QUERY');

export const addDashboardMentee = createAction('ADD_DASHBOARD_MENTEE');
export const addDashboardMenteeCompleted = createAction('ADD_DASHBOARD_MENTEE_COMPLETED');

export const setIsAddMenteeDialogOpen = createAction('SET_IS_ADD_MENTEE_DIALOG_OPEN');

const initialState = {
  list: [],
  sortBy: 'engagement',
  query: '',
  activeMenteeCount: 0,
  slackUsers: [],
  isAddMenteeDialogOpen: false,
};

export default handleActions(
  {
    [fetchDashboardMenteesCompleted]: {
      next: (state, { payload }) => ({
        ...state,
        list: payload,
        activeMenteeCount: payload.length,
      }),
    },
    [updateDashboardMenteesSortBy]: {
      next: (state, { payload }) => ({
        ...state,
        sortBy: payload,
      }),
    },
    [updateDashboardMenteesQuery]: {
      next: (state, { payload }) => ({
        ...state,
        query: payload,
      }),
    },
    [fetchDashboardSlackUsersCompleted]: {
      next: (state, { payload }) => ({
        ...state,
        slackUsers: payload.map(slackUser => ({ ...slackUser, user: {} })),
      }),
    },
    [fetchDashboardSlackUserUserCompleted]: {
      next: (state, { payload }) => ({
        ...state,
        slackUsers: [
          ...state.slackUsers.filter(slackUser => slackUser.email !== payload.email),
          {
            ...state.slackUsers.filter(slackUser => slackUser.email === payload.email)[0],
            user: payload,
          },
        ],
      }),
    },
    [setIsAddMenteeDialogOpen]: {
      next: (state, { payload }) => ({
        ...state,
        isAddMenteeDialogOpen: payload,
      }),
    },
  },
  initialState
);
