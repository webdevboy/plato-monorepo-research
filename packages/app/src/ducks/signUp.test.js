import signUpReducer, { setSignUpData, signUp, signUpCompleted } from './signUp';

const defaultState = {
  companyName: '',
  email: '',
  errorMessage: null,
  faceId: '',
  firstName: '',
  lastName: '',
  plan: null,
  product: null,
  slackUrl: '',
};

it('should return the initial state', () => {
  expect(signUpReducer(undefined, {})).toEqual(defaultState);
});

describe('setSignUpData', () => {
  it('should set the provided value', () => {
    expect(
      signUpReducer(defaultState, setSignUpData({ companyName: 'test-company-name' }))
    ).toMatchObject({ companyName: 'test-company-name' });
  });

  it('should not set the other value', () => {
    expect(
      signUpReducer(defaultState, setSignUpData({ companyName: 'test-company-name' }))
    ).toMatchObject({ email: '' });
  });
});

describe('signUp', () => {
  it('should reset the error message', () => {
    expect(
      signUpReducer({ ...defaultState, errorMessage: 'test-error-message' }, signUp())
    ).toMatchObject({ errorMessage: null });
  });
});

describe('signUpCompleted', () => {
  it('should set the slack URL', () => {
    expect(signUpReducer(defaultState, signUpCompleted('test-slack-url'))).toMatchObject({
      slackUrl: 'test-slack-url',
    });
  });

  it('should set the error message on failure', () => {
    expect(
      signUpReducer(defaultState, signUpCompleted(new Error('test-error-message')))
    ).toMatchObject({ errorMessage: 'test-error-message' });
  });
});
