import { createAction, handleActions } from 'redux-actions';

export const setView = createAction('SET_VIEW');

export default handleActions(
  {
    [setView]: (state, { payload }) => payload.toView,
  },
  null
);
