import slackReducer, { fetchSlackUsersCompleted } from './slack';

const defaultState = { users: [] };

it('should return the initial state', () => {
  expect(slackReducer(undefined, {})).toEqual(defaultState);
});

describe('fetchSlackUsersCompleted', () => {
  it('should set the list of Slack user IDs', () => {
    expect(slackReducer(defaultState, fetchSlackUsersCompleted(['test-slack-user-id-0']))).toEqual({
      users: ['test-slack-user-id-0'],
    });
  });

  it('should do nothing on error', () => {
    expect(slackReducer(defaultState, fetchSlackUsersCompleted(new Error()))).toEqual(defaultState);
  });
});
