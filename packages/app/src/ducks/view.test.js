import reducer, { setView } from './view';

it('should return the initial state', () => {
  expect(reducer(undefined, {})).toEqual(null);
});

describe('setView', () => {
  it('should set the view', () => {
    expect(reducer(null, setView({ toView: 'test-view' }))).toEqual('test-view');
  });
});
