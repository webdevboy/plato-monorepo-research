import { createAction, handleActions } from 'redux-actions';

export const fetchTracks = createAction('FETCH_TRACKS');
export const fetchTracksCompleted = createAction('FETCH_TRACKS_COMPLETED');

export default handleActions(
  {
    [fetchTracksCompleted]: {
      next: (state, { payload }) =>
        payload.reduce((acc, track) => ({ ...acc, [track.id]: track }), state),
    },
  },
  {}
);
