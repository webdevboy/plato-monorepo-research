import dashboardReducer, {
  fetchDashboardCommonChallengesCompleted,
  fetchDashboardQualitativeFeedbackCompleted,
  fetchDashboardSeatsCompleted,
  fetchDashboardStatsCompleted,
} from './dashboard';

const defaultState = {
  stats: [],
  seats: {},
  commonChallenges: [],
  qualitativeFeedback: [],
};

it('should return the initial state', () => {
  expect(dashboardReducer(undefined, {})).toEqual(defaultState);
});

describe('fetchDashboardStatsCompleted', () => {
  it('should set the stats', () => {
    const stats = [
      {
        month: '04/2017',
        nbCallDoneRequests: 11,
        nbCallDoneAttendances: 1,
        requestGradeAverage: 5.5,
        attendanceGradeAverage: 4.5,
      },
      {
        month: '05/2017',
        nbCallDoneRequests: 10,
        nbCallDoneAttendances: 2,
        requestGradeAverage: 5,
        attendanceGradeAverage: 5,
      },
    ];
    expect(dashboardReducer(defaultState, fetchDashboardStatsCompleted(stats)).stats).toEqual(
      stats
    );
  });

  it('should do nothing on error', () => {
    expect(dashboardReducer(defaultState, fetchDashboardStatsCompleted(new Error()))).toEqual(
      defaultState
    );
  });
});

describe('fetchDashboardSeatsCompleted', () => {
  it('should set the seats', () => {
    const seats = {
      nbTakenSeats: 7,
      nbPaidSeats: 8,
      nbOnboardedSeats: 4,
    };
    expect(dashboardReducer(defaultState, fetchDashboardSeatsCompleted(seats)).seats).toEqual(
      seats
    );
  });

  it('should do nothing on error', () => {
    expect(dashboardReducer(defaultState, fetchDashboardSeatsCompleted(new Error()))).toEqual(
      defaultState
    );
  });
});

describe('fetchDashboardCommonChallengesCompleted', () => {
  it('should set the common challenges', () => {
    const commonChallenges = [
      {
        challengeTitle: 'Transitioning from IC to management',
        nbCallDoneRequests: 23,
        nbCallDoneAttendances: 12,
      },
      {
        challengeTitle: 'Balancing time and prioritizing your work',
        nbCallDoneRequests: 13,
        nbCallDoneAttendances: 4,
      },
    ];
    expect(
      dashboardReducer(defaultState, fetchDashboardCommonChallengesCompleted(commonChallenges))
        .commonChallenges
    ).toEqual(commonChallenges);
  });

  it('should do nothing on error', () => {
    expect(
      dashboardReducer(defaultState, fetchDashboardCommonChallengesCompleted(new Error()))
    ).toEqual(defaultState);
  });
});

describe('fetchDashboardQualitativeFeedbackCompleted', () => {
  it('should set the qualitative feedback', () => {
    const qualitativeFeedback = [
      {
        qualitativeFeedback: "Great job helping me see things from other people's perspective.",
        callScheduledAt: 1,
      },
      {
        qualitativeFeedback:
          'The tactics Tido has given me are very helpful. As we discussed, I would work with HR earlier on in term of review/comp discussions. I will alsotry out the weekly updates on the metrics I am looking at with the team to help forest a good practice within the team. Thanks again for your good advices!',
        callScheduledAt: 2,
      },
    ];
    expect(
      dashboardReducer(
        defaultState,
        fetchDashboardQualitativeFeedbackCompleted(qualitativeFeedback)
      ).qualitativeFeedback
    ).toEqual(qualitativeFeedback);
  });

  it('should do nothing on error', () => {
    expect(
      dashboardReducer(defaultState, fetchDashboardQualitativeFeedbackCompleted(new Error()))
    ).toEqual(defaultState);
  });
});
