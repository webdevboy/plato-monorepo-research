import { createAction, handleActions } from 'redux-actions';

export const fetchPersonas = createAction('FETCH_PERSONAS');
export const fetchPersonasCompleted = createAction('FETCH_PERSONAS_COMPLETED');

export default handleActions(
  {
    [fetchPersonasCompleted]: {
      next: (state, { payload }) =>
        payload.reduce((acc, persona) => ({ ...acc, [persona.id]: persona }), state),
    },
  },
  {}
);
