import { createAction, handleActions } from 'redux-actions';

export const fetchHierarchies = createAction('FETCH_HIERARCHIES');
export const fetchHierarchiesCompleted = createAction('FETCH_HIERARCHIES_COMPLETED');

export default handleActions(
  {
    [fetchHierarchiesCompleted]: {
      next: (state, { payload }) => payload,
    },
  },
  []
);
