import { combineActions, createAction, handleActions } from 'redux-actions';

export const fetchDashboardUser = createAction('FETCH_DASHBOARD_USER');
export const fetchDashboardUserCompleted = createAction('FETCH_DASHBOARD_USER_COMPLETED');
export const fetchManagerUser = createAction('FETCH_MANAGER_USER');
export const fetchManagerUserCompleted = createAction('FETCH_MANAGER_USER_COMPLETED');
export const fetchUser = createAction('FETCH_USER');
export const fetchUserCompleted = createAction('FETCH_USER_COMPLETED');
export const fetchUserMentee = createAction('FETCH_USER_MENTEE');
export const fetchUserMenteeCompleted = createAction('FETCH_USER_MENTEE_COMPLETED');
export const fetchUserMentor = createAction('FETCH_USER_MENTOR');
export const fetchUserMentorCompleted = createAction('FETCH_USER_MENTOR_COMPLETED');
export const login = createAction('LOGIN');
export const loginCompleted = createAction('LOGIN_COMPLETED');
export const logout = createAction('LOGOUT');
export const updateMentee = createAction('UPDATE_MENTEE');
export const updateMenteeCompleted = createAction('UPDATE_MENTEE_COMPLETED');
export const updatePassword = createAction('UPDATE_PASSWORD');
export const updatePasswordCompleted = createAction('UPDATE_PASSWORD_COMPLETED');
export const resetPassword = createAction('RESET_PASSWORD');
export const resetPasswordCompleted = createAction('RESET_PASSWORD_COMPLETED');

export default handleActions(
  {
    [fetchUserCompleted]: {
      next: (state, { payload }) => ({ ...state, ...payload }),
    },

    [combineActions(
      fetchDashboardUserCompleted,
      fetchManagerUserCompleted,
      fetchUserMenteeCompleted,
      fetchUserMentorCompleted,
      updateMenteeCompleted
    )]: {
      next: (state, { payload }) => ({ ...state, ...payload, id: state.id }),
    },

    [logout]: () => null,
  },
  null
);
