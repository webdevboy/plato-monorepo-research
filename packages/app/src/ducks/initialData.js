import { createAction, handleActions } from 'redux-actions';

export const fetchInitialData = createAction('FETCH_INITIAL_DATA');
export const fetchInitialDataCompleted = createAction('FETCH_INITIAL_DATA_COMPLETED');

export default handleActions(
  {
    [fetchInitialDataCompleted]: {
      next: () => ({ isDataLoaded: true }),
    },
  },
  { isDataLoaded: false }
);
