import { addDashboardMentee, addDashboardMenteeCompleted } from './dashboardMentees';
import { combineActions, handleActions } from 'redux-actions';
import {
  createProgramApplication,
  createProgramApplicationCompleted,
  fetchPrograms,
  fetchProgramsCompleted,
} from './programs';
import { fetchAmas, fetchAmasCompleted } from './amas';
import { fetchPersonas, fetchPersonasCompleted } from './personas';
import { fetchProducts, fetchProductsCompleted } from './products';
import {
  login,
  loginCompleted,
  resetPassword,
  resetPasswordCompleted,
  updateMentee,
  updateMenteeCompleted,
  updatePassword,
  updatePasswordCompleted,
} from './user';
import { signUp, signUpCompleted } from './signUp';

export default handleActions(
  {
    [combineActions(
      createProgramApplication,
      fetchPersonas,
      fetchProducts,
      fetchPrograms,
      fetchAmas,
      login,
      resetPassword,
      signUp,
      updateMentee,
      updatePassword,
      addDashboardMentee
    )]: (state, action) => ({
      ...state,
      [action.type]: true,
      isVisible: true,
    }),

    [combineActions(
      createProgramApplicationCompleted,
      fetchPersonasCompleted,
      fetchProductsCompleted,
      fetchProgramsCompleted,
      fetchAmasCompleted,
      loginCompleted,
      resetPasswordCompleted,
      signUpCompleted,
      updateMenteeCompleted,
      updatePasswordCompleted,
      addDashboardMenteeCompleted
    )]: (state, action) => ({
      ...state,
      [action.type.replace(/_COMPLETED$/, '')]: false,
      isVisible: false,
    }),
  },
  {}
);
