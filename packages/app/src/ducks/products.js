import { createAction, handleActions } from 'redux-actions';

export const fetchProducts = createAction('FETCH_PRODUCTS');
export const fetchProductsCompleted = createAction('FETCH_PRODUCTS_COMPLETED');

export default handleActions(
  {
    [fetchProductsCompleted]: {
      next: (state, { payload }) => payload,
    },
  },
  []
);
