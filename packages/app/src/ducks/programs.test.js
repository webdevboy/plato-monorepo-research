import programsReducer, { fetchProgramsCompleted } from './programs';

const defaultState = {};

it('should return the initial state', () => {
  expect(programsReducer(undefined, {})).toEqual(defaultState);
});

describe('fetchProgramsCompleted', () => {
  it('should set the list of programs', () => {
    expect(
      programsReducer(
        defaultState,
        fetchProgramsCompleted([{ id: 'test-program-id-0' }, { id: 'test-program-id-1' }])
      )
    ).toEqual({
      'test-program-id-0': { id: 'test-program-id-0' },
      'test-program-id-1': { id: 'test-program-id-1' },
    });
  });

  it('should do nothing on error', () => {
    expect(programsReducer(defaultState, fetchProgramsCompleted(new Error()))).toEqual(
      defaultState
    );
  });
});
