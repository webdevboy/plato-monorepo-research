import { createAction, handleActions } from 'redux-actions';

export const fetchSubscriptions = createAction('FETCH_SUBSCRIPTIONS');
export const fetchSubscriptionsCompleted = createAction('FETCH_SUBSCRIPTIONS_COMPLETED');

export default handleActions(
  {
    [fetchSubscriptionsCompleted]: {
      next: (state, { payload }) => [...state, ...payload],
    },
  },
  []
);
