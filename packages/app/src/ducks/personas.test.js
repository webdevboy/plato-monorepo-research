import personasReducer, { fetchPersonasCompleted } from './personas';

const defaultState = {};

it('should return the initial state', () => {
  expect(personasReducer(undefined, {})).toEqual(defaultState);
});

describe('fetchPersonasCompleted', () => {
  it('should set the list of personas', () => {
    expect(
      personasReducer(
        defaultState,
        fetchPersonasCompleted([{ id: 'test-persona-id-0' }, { id: 'test-persona-id-1' }])
      )
    ).toEqual({
      'test-persona-id-0': { id: 'test-persona-id-0' },
      'test-persona-id-1': { id: 'test-persona-id-1' },
    });
  });

  it('should do nothing on error', () => {
    expect(personasReducer(defaultState, fetchPersonasCompleted(new Error()))).toEqual(
      defaultState
    );
  });
});
