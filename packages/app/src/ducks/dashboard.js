import { createAction, handleActions } from 'redux-actions';

export const fetchDashboardStats = createAction('FETCH_DASHBOARD_STATS');
export const fetchDashboardStatsCompleted = createAction('FETCH_DASHBOARD_STATS_COMPLETED');

export const fetchDashboardSeats = createAction('FETCH_DASHBOARD_SEATS');
export const fetchDashboardSeatsCompleted = createAction('FETCH_DASHBOARD_SEATS_COMPLETED');

export const fetchDashboardCommonChallenges = createAction('FETCH_DASHBOARD_COMMON_CHALLENGES');
export const fetchDashboardCommonChallengesCompleted = createAction(
  'FETCH_DASHBOARD_COMMON_CHALLENGES_COMPLETED'
);

export const fetchDashboardQualitativeFeedback = createAction(
  'FETCH_DASHBOARD_QUALITATIVE_FEEDBACK'
);
export const fetchDashboardQualitativeFeedbackCompleted = createAction(
  'FETCH_DASHBOARD_QUALITATIVE_FEEDBACK_COMPLETED'
);

const initialState = {
  stats: [],
  seats: {},
  commonChallenges: [],
  qualitativeFeedback: [],
};

export default handleActions(
  {
    [fetchDashboardStatsCompleted]: {
      next: (state, { payload }) => ({
        ...state,
        stats: payload,
      }),
    },
    [fetchDashboardSeatsCompleted]: {
      next: (state, { payload }) => ({
        ...state,
        seats: payload,
      }),
    },
    [fetchDashboardCommonChallengesCompleted]: {
      next: (state, { payload }) => ({
        ...state,
        commonChallenges: payload,
      }),
    },
    [fetchDashboardQualitativeFeedbackCompleted]: {
      next: (state, { payload }) => ({
        ...state,
        qualitativeFeedback: payload,
      }),
    },
  },
  initialState
);
