import { ENGINEERING_MANAGER_TAG_ID, PRODUCT_MANAGER_TAG_ID } from 'constants/Tags';
import { createAction, handleActions } from 'redux-actions';

export const fetchIndustryTags = createAction('FETCH_INDUSTRY_TAGS');
export const fetchIndustryTagsCompleted = createAction('FETCH_INDUSTRY_TAGS_COMPLETED');

const initialState = {
  [ENGINEERING_MANAGER_TAG_ID]: {
    id: ENGINEERING_MANAGER_TAG_ID,
    name: 'Engineering Manager',
    type: 'Face',
  },
  [PRODUCT_MANAGER_TAG_ID]: {
    id: PRODUCT_MANAGER_TAG_ID,
    name: 'Product Manager',
    type: 'Face',
  },
};

export default handleActions(
  {
    [fetchIndustryTagsCompleted]: {
      next: (state, { payload }) =>
        payload.reduce((acc, tag) => ({ ...acc, [tag.id]: { ...tag, type: 'Industry' } }), state),
    },
  },
  initialState
);
