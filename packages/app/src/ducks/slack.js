import { createAction, handleActions } from 'redux-actions';

export const fetchSlackUsers = createAction('FETCH_SLACK_USERS');
export const fetchSlackUsersCompleted = createAction('FETCH_SLACK_USERS_COMPLETED');

export default handleActions(
  {
    [fetchSlackUsersCompleted]: {
      next: (state, { payload }) => ({
        ...state,
        users: payload,
      }),
    },
  },
  { users: [] }
);
