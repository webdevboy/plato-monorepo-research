import { createAction, handleActions } from 'redux-actions';
import { resetPasswordCompleted, updatePasswordCompleted } from './user';

export const showSnackbar = createAction('SHOW_SNACKBAR');
export const hideSnackbar = createAction('HIDE_SNACKBAR');

export default handleActions(
  {
    [showSnackbar]: (state, { payload }) => ({
      message: payload.message,
      variant: payload.variant,
      isVisible: true,
    }),

    [hideSnackbar]: state => ({
      ...state,
      isVisible: false,
    }),

    [resetPasswordCompleted]: {
      next: () => ({
        message: 'Password successfully reset!',
        variant: 'success',
        isVisible: true,
      }),
    },

    [updatePasswordCompleted]: {
      next: () => ({
        message: 'Check your email for a password reset link!',
        variant: 'success',
        isVisible: true,
      }),
    },
  },
  null
);
