import reducer, { fetchChallengesCompleted } from './challenges';

const defaultState = {};

it('should return the initial state', () => {
  expect(reducer(undefined, {})).toEqual(defaultState);
});

describe('fetchChallengesCompleted', () => {
  it('should set the list of challenges', () => {
    expect(
      reducer(
        defaultState,
        fetchChallengesCompleted([{ id: 'test-challenge-id-0' }, { id: 'test-challenge-id-1' }])
      )
    ).toEqual({
      'test-challenge-id-0': { id: 'test-challenge-id-0' },
      'test-challenge-id-1': { id: 'test-challenge-id-1' },
    });
  });

  it('should do nothing on error', () => {
    expect(reducer(defaultState, fetchChallengesCompleted(new Error()))).toEqual(defaultState);
  });
});
