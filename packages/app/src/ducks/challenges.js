import { createAction, handleActions } from 'redux-actions';

export const fetchChallenges = createAction('FETCH_CHALLENGES');
export const fetchChallengesCompleted = createAction('FETCH_CHALLENGES_COMPLETED');

export default handleActions(
  {
    [fetchChallengesCompleted]: {
      next: (state, { payload }) =>
        payload.reduce((acc, challenge) => ({ ...acc, [challenge.id]: challenge }), state),
    },
  },
  {}
);
