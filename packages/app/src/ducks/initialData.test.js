import reducer, { fetchInitialDataCompleted } from './initialData';

const defaultState = { isDataLoaded: false };

it('should return the initial state', () => {
  expect(reducer(undefined, {})).toEqual(defaultState);
});

describe('fetchInitialDataCompleted', () => {
  it('should set the data loaded flag to true', () => {
    expect(reducer(defaultState, fetchInitialDataCompleted())).toEqual({ isDataLoaded: true });
  });

  it('should do nothing on error', () => {
    expect(reducer(defaultState, fetchInitialDataCompleted(new Error()))).toEqual(defaultState);
  });
});
