import { createAction, handleActions } from 'redux-actions';

export const setSignUpData = createAction('SET_SIGN_UP_DATA');
export const signUp = createAction('SIGN_UP');
export const signUpCompleted = createAction('SIGN_UP_COMPLETED');

const initialState = {
  companyName: '',
  email: '',
  errorMessage: null,
  faceId: '',
  firstName: '',
  lastName: '',
  plan: null,
  product: null,
  slackUrl: '',
};

export default handleActions(
  {
    [setSignUpData]: (state, { payload }) => ({ ...state, ...payload }),
    [signUp]: state => ({ ...state, errorMessage: null }),
    [signUpCompleted]: {
      next: (state, { payload }) => ({ ...state, slackUrl: payload }),
      throw: (state, { payload }) => ({
        ...state,
        errorMessage: payload.message,
      }),
    },
  },
  initialState
);
