import reducer, {
  fetchMentorCompleted,
  fetchMentorRelationshipSlotsCompleted,
  fetchMentorsCompleted,
} from './mentors';

const defaultState = {};

it('should return the initial state', () => {
  expect(reducer(undefined, {})).toEqual(defaultState);
});

describe('fetchMentorsCompleted', () => {
  it('should set the list of mentors', () => {
    expect(
      reducer(
        defaultState,
        fetchMentorsCompleted([{ id: 'test-mentor-id-0' }, { id: 'test-mentor-id-1' }])
      )
    ).toEqual({
      'test-mentor-id-0': { id: 'test-mentor-id-0' },
      'test-mentor-id-1': { id: 'test-mentor-id-1' },
    });
  });

  it('should do nothing on error', () => {
    expect(reducer(defaultState, fetchMentorsCompleted(new Error()))).toEqual(defaultState);
  });
});

describe('fetchMentorCompleted', () => {
  it('should set the list of mentors', () => {
    expect(reducer(defaultState, fetchMentorCompleted({ id: 'test-id' }))).toEqual({
      'test-id': { id: 'test-id' },
    });
  });

  it('should enrich an existing mentor', () => {
    expect(
      reducer(
        {
          'test-id': { id: 'test-id' },
        },
        fetchMentorCompleted({ id: 'test-id', name: 'test-name' })
      )
    ).toEqual({
      'test-id': { id: 'test-id', name: 'test-name' },
    });
  });

  it('should do nothing on error', () => {
    expect(reducer(defaultState, fetchMentorCompleted(new Error()))).toEqual(defaultState);
  });
});

describe('fetchMentorRelationshipSlotsCompleted', () => {
  it('should set the list of mentors', () => {
    expect(
      reducer(
        defaultState,
        fetchMentorRelationshipSlotsCompleted({
          slots: [{ id: 'test-mentor-relationship-id-0' }],
          mentorId: 'test-mentor-id-0',
        })
      )
    ).toEqual({
      'test-mentor-id-0': {
        relationshipSlots: [{ id: 'test-mentor-relationship-id-0' }],
      },
    });
  });

  it('should enrich an existing mentor', () => {
    expect(
      reducer(
        {
          'test-mentor-id-0': {
            id: 'test-mentor-id-0',
          },
        },
        fetchMentorRelationshipSlotsCompleted({
          slots: [{ id: 'test-mentor-relationship-id-0' }],
          mentorId: 'test-mentor-id-0',
        })
      )
    ).toEqual({
      'test-mentor-id-0': {
        id: 'test-mentor-id-0',
        relationshipSlots: [{ id: 'test-mentor-relationship-id-0' }],
      },
    });
  });

  it('should do nothing on error', () => {
    expect(reducer(defaultState, fetchMentorRelationshipSlotsCompleted(new Error()))).toEqual(
      defaultState
    );
  });
});
