import { createAction, handleActions } from 'redux-actions';

export const fetchPrograms = createAction('FETCH_PROGRAMS');
export const fetchProgramsCompleted = createAction('FETCH_PROGRAMS_COMPLETED');
export const createProgramApplication = createAction('CREATE_PROGRAM_APPLICATION');
export const createProgramApplicationCompleted = createAction(
  'CREATE_PROGRAM_APPLICATION_COMPLETED'
);

export default handleActions(
  {
    [fetchProgramsCompleted]: {
      next: (state, { payload }) =>
        payload.reduce((acc, program) => ({ ...acc, [program.id]: program }), state),
    },
  },
  {}
);
