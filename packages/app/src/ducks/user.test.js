import userReducer, {
  fetchDashboardUserCompleted,
  fetchManagerUserCompleted,
  fetchUserCompleted,
  fetchUserMenteeCompleted,
  fetchUserMentorCompleted,
  updateMenteeCompleted,
} from './user';

it('should return the initial state', () => {
  expect(userReducer(undefined, {})).toEqual(null);
});

describe('fetchUserCompleted', () => {
  it('should set the user', () => {
    expect(userReducer(null, fetchUserCompleted({ id: 'test-user-id' }))).toEqual({
      id: 'test-user-id',
    });
  });

  it('should do nothing on error', () => {
    expect(userReducer(null, fetchUserCompleted(new Error()))).toEqual(null);
  });
});

describe('fetchUserMenteeCompleted', () => {
  it('should set the user mentee', () => {
    expect(
      userReducer({ id: 'test-user-id' }, fetchUserMenteeCompleted({ name: 'test-mentee-name' }))
    ).toEqual({
      id: 'test-user-id',
      name: 'test-mentee-name',
    });
  });

  it('should not override the user id', () => {
    expect(
      userReducer({ id: 'test-user-id' }, fetchUserMenteeCompleted({ id: 'test-mentee-id' }))
    ).toEqual({
      id: 'test-user-id',
    });
  });

  it('should do nothing on error', () => {
    expect(userReducer(null, fetchUserMenteeCompleted(new Error()))).toEqual(null);
  });
});

describe('fetchDashboardUserCompleted', () => {
  it('should set the dashboard user', () => {
    expect(
      userReducer(
        { id: 'test-user-id' },
        fetchDashboardUserCompleted({ name: 'test-dashboard-user-name' })
      )
    ).toEqual({
      id: 'test-user-id',
      name: 'test-dashboard-user-name',
    });
  });

  it('should not override the user id', () => {
    expect(
      userReducer(
        { id: 'test-user-id' },
        fetchDashboardUserCompleted({ id: 'test-dashboard-user-id' })
      )
    ).toEqual({
      id: 'test-user-id',
    });
  });

  it('should do nothing on error', () => {
    expect(userReducer(null, fetchDashboardUserCompleted(new Error()))).toEqual(null);
  });
});

describe('fetchManagerUserCompleted', () => {
  it('should set the manager user', () => {
    expect(
      userReducer(
        { id: 'test-user-id' },
        fetchManagerUserCompleted({ name: 'test-manager-user-name' })
      )
    ).toEqual({
      id: 'test-user-id',
      name: 'test-manager-user-name',
    });
  });

  it('should not override the user id', () => {
    expect(
      userReducer({ id: 'test-user-id' }, fetchManagerUserCompleted({ id: 'test-manager-user-id' }))
    ).toEqual({
      id: 'test-user-id',
    });
  });

  it('should do nothing on error', () => {
    expect(userReducer(null, fetchManagerUserCompleted(new Error()))).toEqual(null);
  });
});

describe('fetchUserMentorCompleted', () => {
  it('should set the mentor user', () => {
    expect(
      userReducer({ id: 'test-user-id' }, fetchUserMentorCompleted({ name: 'test-mentor-name' }))
    ).toEqual({
      id: 'test-user-id',
      name: 'test-mentor-name',
    });
  });

  it('should not override the user id', () => {
    expect(
      userReducer({ id: 'test-user-id' }, fetchUserMentorCompleted({ id: 'test-mentor-id' }))
    ).toEqual({
      id: 'test-user-id',
    });
  });

  it('should do nothing on error', () => {
    expect(userReducer(null, fetchUserMentorCompleted(new Error()))).toEqual(null);
  });
});

describe('updateMenteeCompleted', () => {
  it('should update the user', () => {
    expect(
      userReducer({ id: 'test-user-id' }, updateMenteeCompleted({ name: 'test-mentee-name' }))
    ).toEqual({
      id: 'test-user-id',
      name: 'test-mentee-name',
    });
  });

  it('should not override the user id', () => {
    expect(
      userReducer({ id: 'test-user-id' }, updateMenteeCompleted({ id: 'test-mentee-id' }))
    ).toEqual({
      id: 'test-user-id',
    });
  });

  it('should do nothing on error', () => {
    expect(userReducer(null, updateMenteeCompleted(new Error()))).toEqual(null);
  });
});
