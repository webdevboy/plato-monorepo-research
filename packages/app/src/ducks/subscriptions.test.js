import reducer, { fetchSubscriptionsCompleted } from './subscriptions';

it('should return the initial state', () => {
  expect(reducer(undefined, {})).toEqual([]);
});

describe('fetchSubscriptionsCompleted', () => {
  it('should set the list of subscriptions', () => {
    expect(reducer([], fetchSubscriptionsCompleted([{ id: 'test-id' }]))).toEqual([
      { id: 'test-id' },
    ]);
  });

  it('should update the list of subscriptions', () => {
    expect(
      reducer([{ id: 'test-id-0' }], fetchSubscriptionsCompleted([{ id: 'test-id-1' }]))
    ).toEqual([{ id: 'test-id-0' }, { id: 'test-id-1' }]);
  });
});
