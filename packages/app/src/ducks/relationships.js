import { createAction } from 'redux-actions';

export const createRelationship = createAction('CREATE_RELATIONSHIP');
export const createRelationshipCompleted = createAction('CREATE_RELATIONSHIP_COMPLETED');
