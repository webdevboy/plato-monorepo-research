import reducer, { fetchHierarchiesCompleted } from './hierarchies';

const defaultState = [];

it('should return the initial state', () => {
  expect(reducer(undefined, {})).toEqual(defaultState);
});

describe('fetchHierarchiesCompleted', () => {
  it('should set the list of hierarchies', () => {
    expect(
      reducer(
        defaultState,
        fetchHierarchiesCompleted([{ id: 'test-hierarchy-id-0' }, { id: 'test-hierarchy-id-1' }])
      )
    ).toEqual([{ id: 'test-hierarchy-id-0' }, { id: 'test-hierarchy-id-1' }]);
  });

  it('should do nothing on error', () => {
    expect(reducer(defaultState, fetchHierarchiesCompleted(new Error()))).toEqual(defaultState);
  });
});
