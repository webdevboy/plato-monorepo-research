import { createAction, handleActions } from 'redux-actions';

export const fetchAmaAttendances = createAction('FETCH_AMA_ATTENDANCES');
export const fetchAmaAttendancesCompleted = createAction('FETCH_AMA_ATTENDANCES_COMPLETED');

export const fetchAmas = createAction('FETCH_AMAS');
export const fetchAmasCompleted = createAction('FETCH_AMAS_COMPLETED');

export default handleActions(
  {
    [fetchAmasCompleted]: {
      next: (state, { payload }) =>
        payload.reduce(
          (acc, ama) => ({
            ...acc,
            [ama.id]: state[ama.id] ? { ...state[ama.id], ...ama } : ama,
          }),
          state
        ),
    },

    [fetchAmaAttendancesCompleted]: {
      next: (state, { payload }) => ({
        ...state,
        [payload.amaId]: state[payload.amaId] ? { ...state[payload.amaId], ...payload } : payload,
      }),
    },
  },
  {}
);
