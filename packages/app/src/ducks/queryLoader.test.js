import { fetchProducts, fetchProductsCompleted } from './products';
import { signUp, signUpCompleted } from './signUp';

import queryLoader from './queryLoader';

const defaultState = {};

it('should return the initial state', () => {
  expect(queryLoader(undefined, {})).toEqual(defaultState);
});

[fetchProducts, signUp, fetchProductsCompleted, signUpCompleted].forEach(action => {
  // eslint-disable-next-line jest/valid-describe
  describe(action, () => {
    it('should show the query loader', () => {
      const isCompleted = !action().type.endsWith('_COMPLETED');
      const result = queryLoader(undefined, action());
      expect(result).toEqual({
        isVisible: isCompleted,
        [action().type.replace('_COMPLETED', '')]: isCompleted,
      });
    });
  });
});
