import { ENGINEERING_MANAGER_TAG_ID, PRODUCT_MANAGER_TAG_ID } from 'constants/Tags';
import reducer, { fetchIndustryTagsCompleted } from './tags';

const defaultState = {
  [ENGINEERING_MANAGER_TAG_ID]: {
    id: ENGINEERING_MANAGER_TAG_ID,
    name: 'Engineering Manager',
    type: 'Face',
  },
  [PRODUCT_MANAGER_TAG_ID]: {
    id: PRODUCT_MANAGER_TAG_ID,
    name: 'Product Manager',
    type: 'Face',
  },
};

it('should return the initial state', () => {
  expect(reducer(undefined, {})).toEqual(defaultState);
});

describe('fetchIndustryTagsCompleted', () => {
  it('should set the user', () => {
    expect(
      reducer({}, fetchIndustryTagsCompleted([{ id: 'test-tag-id-0' }, { id: 'test-tag-id-1' }]))
    ).toEqual({
      'test-tag-id-0': { id: 'test-tag-id-0', type: 'Industry' },
      'test-tag-id-1': { id: 'test-tag-id-1', type: 'Industry' },
    });
  });

  it('should do nothing on error', () => {
    expect(reducer({}, fetchIndustryTagsCompleted(new Error()))).toEqual({});
  });
});
