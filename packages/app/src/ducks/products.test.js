import productsReducer, { fetchProductsCompleted } from './products';

const defaultState = [];

it('should return the initial state', () => {
  expect(productsReducer(undefined, {})).toEqual(defaultState);
});

describe('fetchProductsCompleted', () => {
  it('should set the list of products', () => {
    expect(
      productsReducer(
        defaultState,
        fetchProductsCompleted([{ id: 'test-product-id-0' }, { id: 'test-product-id-1' }])
      )
    ).toEqual([{ id: 'test-product-id-0' }, { id: 'test-product-id-1' }]);
  });

  it('should do nothing on error', () => {
    expect(productsReducer(defaultState, fetchProductsCompleted(new Error()))).toEqual(
      defaultState
    );
  });
});
