import amas from './amas';
import challenges from './challenges';
import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import dashboard from './dashboard';
import dashboardMentees from './dashboardMentees';
import hierarchies from './hierarchies';
import initialData from './initialData';
import mentors from './mentors';
import personas from './personas';
import products from './products';
import programs from './programs';
import queryLoader from './queryLoader';
import signUp from './signUp';
import slack from './slack';
import snackbar from './snackbar';
import subscriptions from './subscriptions';
import tags from './tags';
import tracks from './tracks';
import user from './user';
import view from './view';

export * from './amas';
export * from './challenges';
export * from './dashboard';
export * from './dashboardMentees';
export * from './hierarchies';
export * from './initialData';
export * from './mentors';
export * from './personas';
export * from './products';
export * from './programs';
export * from './relationships';
export * from './signUp';
export * from './slack';
export * from './snackbar';
export * from './tags';
export * from './tracks';
export * from './subscriptions';
export * from './user';
export * from './view';

function mainReducer(history) {
  return combineReducers({
    amas,
    challenges,
    dashboard,
    dashboardMentees,
    hierarchies,
    initialData,
    mentors,
    personas,
    products,
    programs,
    queryLoader,
    router: connectRouter(history),
    signUp,
    slack,
    subscriptions,
    snackbar,
    tags,
    tracks,
    user,
    view,
  });
}

export default mainReducer;
