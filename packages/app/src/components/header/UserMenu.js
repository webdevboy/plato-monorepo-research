import React, { Component } from 'react';

import Avatar from 'components/shared/Avatar';
import ChangePasswordDialog from './ChangePasswordDialog';
import { Fade } from '@material-ui/core';
import PropTypes from 'prop-types';
import getProfilePictureThumbnail from 'utils/thumbnail';
import styled from 'styled-components';
import { views } from 'constants/Views';
import { withUser } from 'components/context/UserContext';

const UserAvatar = styled(Avatar)`
  cursor: pointer;
  border: none;
`;

const Dropdown = styled.div`
  position: absolute;
  right: 0.1rem;
  top: 6rem;
  display: flex;
  flex-direction: column;
  width: 22rem;
  background: ${props => props.theme.palette.white};
  border: 1px solid ${props => props.theme.palette.geyser};
  border-radius: 0.2rem;
  z-index: 1;
  font-size: 1.6rem;
`;

const DropdownHeader = styled.div`
  line-height: 2.24rem;
  padding: 1.5rem;
`;

const Divider = styled.span`
  width: 100%;
  border-bottom: 1px solid ${props => props.theme.palette.geyser};
`;

const Name = styled.p`
  font-size: 1em;
  font-weight: 600;
`;

const Company = styled.p`
  font-size: 1em;
  color: ${props => props.theme.palette.cadetBlue};
`;

const DropdownItem = styled.div`
  color: ${props => props.theme.palette.cadetBlue};
  font-size: 1em;
  padding: 1rem 1.5rem;
  font-weight: 600;
  transition: color ${props => props.theme.transitions.default};

  &:hover {
    color: ${props => props.theme.palette.osloGrey};
    cursor: pointer;
  }
`;

class UserMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      companyName: '',
      changePasswordDialogIsOpen: false,
    };
  }

  async componentDidMount() {
    const {
      userContext: { managerUser, dashboardUser },
    } = this.props;

    const companyName = managerUser ? managerUser.companyName : dashboardUser.companyName;
    this.setState({ companyName });

    document.addEventListener('mousedown', this.handleClickOutside);
  }

  componentWillUnmount() {
    document.removeEventListener('mousedown', this.handleClickOutside);
  }

  toggleOpen = () => {
    this.setState(prevState => ({
      isOpen: !prevState.isOpen,
    }));
  };

  openChangePasswordDialog = () => {
    this.setState({
      changePasswordDialogIsOpen: true,
    });
  };

  closeChangePasswordDialog = () => {
    this.setState({
      changePasswordDialogIsOpen: false,
    });
  };

  logout = () => {
    const { userContext } = this.props;

    this.toggleOpen();

    userContext.logout();
  };

  handleClickOutside = () => {
    this.setState({
      isOpen: false,
    });
  };

  switchToMenteeSpace = () => {
    const {
      userContext: { switchToView },
    } = this.props;
    switchToView(views.MENTEE);
  };

  switchToMentorSpace = () => {
    const {
      userContext: { switchToView },
    } = this.props;
    switchToView(views.MENTOR);
  };

  switchToDashboardUserSpace = () => {
    const {
      userContext: { switchToView },
    } = this.props;
    switchToView(views.DASHBOARD_USER);
  };

  render() {
    const { isOpen, companyName, changePasswordDialogIsOpen } = this.state;
    const {
      userContext: { user, managerUser, view, isMentor, isMentee, isDashboardUser },
    } = this.props;

    const thumbnailUrl = managerUser && managerUser.picture && managerUser.picture.thumbnailUrl;

    return (
      <React.Fragment>
        <UserAvatar
          onClick={this.toggleOpen}
          src={getProfilePictureThumbnail(user.firstName, thumbnailUrl)}
          alt="User's avatar"
          size="sm"
        />
        <Fade in={isOpen} unmountOnExit style={{ zIndex: 10 }}>
          <Dropdown className="pb1">
            <DropdownHeader>
              <Name>{user.fullName}</Name>
              <Company>{companyName}</Company>
            </DropdownHeader>
            <Divider className="mb1" />
            {view !== views.MENTEE && isMentee() ? (
              <DropdownItem onClick={this.switchToMenteeSpace}>Mentee Space</DropdownItem>
            ) : null}
            {view !== views.MENTOR && isMentor() ? (
              <DropdownItem onClick={this.switchToMentorSpace}>Mentor Space</DropdownItem>
            ) : null}
            {view !== views.DASHBOARD_USER && isDashboardUser() ? (
              <DropdownItem onClick={this.switchToDashboardUserSpace}>
                Organization Space
              </DropdownItem>
            ) : null}
            <DropdownItem onClick={this.openChangePasswordDialog}>Change Password</DropdownItem>
            <DropdownItem onClick={this.logout}>Log Out</DropdownItem>
          </Dropdown>
        </Fade>

        <ChangePasswordDialog
          isOpen={changePasswordDialogIsOpen}
          onClose={this.closeChangePasswordDialog}
        />
      </React.Fragment>
    );
  }
}

UserMenu.propTypes = {
  userContext: PropTypes.object.isRequired,
};

export default withUser(UserMenu);
