import React, { Component } from 'react';

import Button from 'components/shared/Button';
import PlatoDialog from 'components/shared/PlatoDialog';
import PropTypes from 'prop-types';
import TextInput from 'components/shared/TextInput';
import UserService from 'services/api/plato/user/user';
import styled from 'styled-components';
import { trackPasswordChanged } from 'utils/analytics';
import { withQueryLoader } from 'components/context/QueryLoaderContext';
import { withSnackbar } from 'components/context/SnackbarContext';
import { withUser } from 'components/context/UserContext';

const Body = styled.div`
  display: flex;
  flex-direction: column;
  padding: 3rem;
  width: 54rem;

  & h1 {
    margin-bottom: 3rem;
  }

  & h4 {
    margin-bottom: 1rem;
  }

  & ${Button} {
    display: block;
    margin-left: auto;
  }
`;

class ChangePasswordDialog extends Component {
  constructor(props) {
    super(props);

    this.state = {
      oldPassword: '',
      newPassword: '',
      saving: false,
    };
  }

  changePassword = async event => {
    event.preventDefault();

    const {
      queryLoader,
      userContext,
      userContext: {
        user: { uuid, email },
      },
      snackbar,
    } = this.props;
    const { oldPassword, newPassword } = this.state;

    queryLoader.showLoader();
    this.setState({ saving: true });

    try {
      await UserService.changePassword({ oldPassword, newPassword });

      trackPasswordChanged({ uuid, email });

      userContext.logout();
      snackbar.success('Password changed.  Please log in again.');
    } catch (error) {
      if (error.length > 0 && error[0].field === 'oldPassword') {
        snackbar.error('The password you entered is incorrect.');
      } else {
        console.error(error);
        snackbar.error('Something went wrong - sorry!');
      }

      this.setState({ saving: false });
    }

    queryLoader.hideLoader();
  };

  onChange = event => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  render() {
    const { oldPassword, newPassword, saving } = this.state;
    const { isOpen, onClose } = this.props;

    return (
      <PlatoDialog open={isOpen} onClose={onClose}>
        <Body>
          <form onSubmit={this.changePassword}>
            <h1>Change Password</h1>

            <TextInput
              type="password"
              name="oldPassword"
              label="Old Password"
              value={oldPassword}
              onChange={this.onChange}
            />

            <TextInput
              type="password"
              name="newPassword"
              label="New Password"
              value={newPassword}
              onChange={this.onChange}
            />

            <Button type="submit" variant="primary" disabled={saving}>
              Save
            </Button>
          </form>
        </Body>
      </PlatoDialog>
    );
  }
}

ChangePasswordDialog.propTypes = {
  onClose: PropTypes.func.isRequired,
  isOpen: PropTypes.bool.isRequired,
  queryLoader: PropTypes.object.isRequired,
  snackbar: PropTypes.object.isRequired,
  userContext: PropTypes.object.isRequired,
};

export default withSnackbar(withQueryLoader(withUser(ChangePasswordDialog)));
