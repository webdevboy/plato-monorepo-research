import Header from './Header';
import HeaderLink from './HeaderLink';
import NavContainer from 'components/header/NavContainer';
import React from 'react';
import styled from 'styled-components';

const StyledNav = styled.nav`
  flex-grow: 1;
  justify-content: flex-end;
`;

const DashboardUserHeader = () => (
  <Header>
    <NavContainer>
      <StyledNav>
        <HeaderLink exact to="/dashboard">
          Dashboard
        </HeaderLink>
        <HeaderLink exact to="/dashboard/mentees">
          Mentees
        </HeaderLink>
      </StyledNav>
    </NavContainer>
  </Header>
);

export default DashboardUserHeader;
