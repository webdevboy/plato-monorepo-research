import React, { Component } from 'react';

import { NavLink } from 'react-router-dom';
import styled from 'styled-components';

const StyledHeaderLink = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  position: relative;
  height: 100%;

  &:not(:last-child) {
    margin-right: 4rem;
  }

  a {
    display: flex;
    white-space: nowrap;
    color: ${props => props.theme.palette.cadetBlue};
    text-decoration: none;
    text-transform: uppercase;
    font-size: 1.4rem;
    transition: color ${props => props.theme.transitions.default};
    font-weight: bold;
  }

  a:hover {
    color: ${props => props.theme.palette.white};
    text-decoration: none;
  }

  a.active {
    color: ${props => props.theme.palette.white};
  }

  a.active::before {
    content: '';
    width: 4rem;
    height: 0.3rem;
    background: ${props => props.theme.palette.cerulean};
    position: absolute;
    bottom: 0;
    left: 50%;
    transform: translate(-50%);
  }
`;

class HeaderLink extends Component {
  render() {
    const { children, to, ...otherProps } = this.props;
    const isExternalLink = to.startsWith('https://');
    return (
      <StyledHeaderLink>
        {isExternalLink ? (
          <a href={to} target="_blank" rel="noopener noreferrer">
            {children}
          </a>
        ) : (
          <NavLink activeClassName="active" to={to} {...otherProps}>
            {children}
          </NavLink>
        )}
      </StyledHeaderLink>
    );
  }
}

export default HeaderLink;
