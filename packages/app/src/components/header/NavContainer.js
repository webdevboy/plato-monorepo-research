import styled from 'styled-components';

const NavContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  height: 100%;

  nav {
    display: flex;
    align-items: center;
    height: 100%;
  }
`;

export default NavContainer;
