import { Link } from 'react-router-dom';
import React from 'react';
import UserMenu from './UserMenu';
import styled from 'styled-components';

const StyledHeader = styled.header`
  display: flex;
  align-items: center;
  height: 6rem;
  background: ${props => props.theme.palette.ebonyClay};
  padding: 0px 2rem;
`;

const Logo = styled.img`
  height: 2.5rem;
  margin-right: 4.8rem;
`;

const UserMenuContainer = styled.div`
  margin-left: 4rem;
`;

const Header = props => (
  <StyledHeader>
    <Link to="/">
      <Logo
        src="https://d1muf25xaso8hp.cloudfront.net/https%3A%2F%2Fs3.amazonaws.com%2Fappforest_uf%2Ff1528975785447x249209639616310600%2FArtboard%25203%2520Copy.png?w=128&h=32&auto=compress&fit=crop"
        alt="Plato"
      />
    </Link>

    {props.children}

    <UserMenuContainer>
      <UserMenu />
    </UserMenuContainer>
  </StyledHeader>
);

export default Header;
