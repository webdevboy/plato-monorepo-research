import React from 'react';
import styled from 'styled-components';
import Header from './Header';
import NavContainer from './NavContainer';
import HeaderLink from './HeaderLink';

const StyledNav = styled.nav`
  flex-grow: 1;
  justify-content: flex-end;
`;

const MentorHeader = () => (
  <Header>
    <NavContainer>
      <StyledNav>
        <HeaderLink to="/mentor_sessions">Sessions</HeaderLink>
        <HeaderLink to="/mentor_stories">Stories</HeaderLink>
        <HeaderLink to="/mentor_performance">Performance</HeaderLink>
        <HeaderLink to="/mentor_profile">Profile</HeaderLink>
      </StyledNav>
    </NavContainer>
  </Header>
);

export default MentorHeader;
