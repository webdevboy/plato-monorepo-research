import React, { Component } from 'react';

import Header from './Header';
import HeaderLink from './HeaderLink';
import PropTypes from 'prop-types';
import Tag from 'components/shared/Tag';
import { VALIDATIONS_APP_URL } from 'constants/Urls';
import { isValidationsFeatureEnabled } from 'utils/featureFlags';
import styled from 'styled-components';
import { withUser } from 'components/context/UserContext';

const NavContainer = styled.div`
  position: relative;
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  height: 100%;

  nav {
    display: flex;
    align-items: center;
    height: 100%;
  }

  ${Tag} {
    margin-left: 4px;
  }
`;

class MenteeHeader extends Component {
  render() {
    const {
      userContext: { mentee },
    } = this.props;
    return (
      <Header>
        <NavContainer>
          <nav>
            <HeaderLink to="/ama">AMA Sessions</HeaderLink>
            <HeaderLink to="/stories">Stories</HeaderLink>
            <HeaderLink to="/mentors">Mentors</HeaderLink>
            <HeaderLink to="/tracks">
              Tracks
              <Tag size="small" text="NEW" variant="primary" />
            </HeaderLink>
          </nav>

          <nav>
            {isValidationsFeatureEnabled(mentee.id) && (
              <HeaderLink to={`${VALIDATIONS_APP_URL}?user=${encodeURIComponent(mentee.id)}`}>
                My Validations
              </HeaderLink>
            )}
            <HeaderLink to="/challenges">My Challenges</HeaderLink>
            <HeaderLink to="/sessions">My Sessions</HeaderLink>
          </nav>
        </NavContainer>
      </Header>
    );
  }
}

MenteeHeader.propTypes = {
  userContext: PropTypes.object.isRequired,
};

export default withUser(MenteeHeader);
