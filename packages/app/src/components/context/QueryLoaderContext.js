import PlatoLinearProgress from 'components/shared/PlatoLinearProgress';
import React from 'react';

const QueryLoaderContext = React.createContext({});

// TODO:  Rename this to something like IndeterminateProvider
export class QueryLoaderProvider extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
    };
  }

  showLoader = () => {
    this.setState({ loading: true });
  };

  hideLoader = () => {
    this.setState({ loading: false });
  };

  render() {
    const { children } = this.props;
    const { loading } = this.state;

    return (
      <QueryLoaderContext.Provider
        value={{
          // TODO:  Rename these to show/hide
          showLoader: this.showLoader,
          hideLoader: this.hideLoader,
          loading,
        }}
      >
        {loading && <PlatoLinearProgress variant="indeterminate" />}
        {children}
      </QueryLoaderContext.Provider>
    );
  }
}

export const QueryLoaderConsumer = QueryLoaderContext.Consumer;

export const withQueryLoader = WrappedComponent => props => (
  <QueryLoaderConsumer>
    {context => <WrappedComponent queryLoader={context} {...props} />}
  </QueryLoaderConsumer>
);
