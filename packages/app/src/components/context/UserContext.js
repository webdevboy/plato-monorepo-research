import { getUser, getView } from 'selectors';
import { logout, setView } from 'ducks';

import { DEFAULT_IANA_TIME_ZONE } from 'constants/TimeZones';
import React from 'react';
import { connect } from 'react-redux';

const UserContext = React.createContext({});

class UserProvider extends React.Component {
  logout = () => {
    const { user, onLogout } = this.props;
    onLogout(user.uuid, user.email);
  };

  switchToView = toView => {
    const { view, onSetView } = this.props;
    onSetView(view, toView);
  };

  isManagerUser = () => {
    return this.props.user ? this.props.user.managerUserId : false;
  };

  isMentee = () => {
    return this.props.user ? this.props.user.menteeId : false;
  };

  isMentor = () => {
    return this.props.user ? this.props.user.mentorId : false;
  };

  isDashboardUser = () => {
    return this.props.user ? this.props.user.dashboardUserId : false;
  };

  getTimeZone = () => {
    const { user } = this.props;
    return (user && user.timeZone) || DEFAULT_IANA_TIME_ZONE;
  };

  isProductManager = () => {
    const { user } = this.props;
    if (user && user.faces) {
      return user.faces.some(face => face.name === 'PM');
    }
    return false;
  };

  render() {
    const { children, view } = this.props;
    const user = this.props.user || {};
    return (
      <UserContext.Provider
        value={{
          view,
          user,
          managerUser: { ...user, id: user.managerUserId },
          mentee: { ...user, id: user.menteeId },
          mentor: { ...user, id: user.mentorId },
          dashboardUser: { ...user, id: user.dashboardUserId },
          logout: this.logout,
          switchToView: this.switchToView,
          isMentee: this.isMentee,
          isMentor: this.isMentor,
          isDashboardUser: this.isDashboardUser,
          timeZone: this.getTimeZone,
          isProductManager: this.isProductManager,
        }}
      >
        {children}
      </UserContext.Provider>
    );
  }
}

function mapStateToProps(state) {
  return {
    user: getUser(state),
    view: getView(state),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    onLogout: (uuid, email) => dispatch(logout({ uuid, email })),
    onSetView: (fromView, toView) => dispatch(setView({ fromView, toView })),
  };
}

export const ConnectedUserProvider = connect(
  mapStateToProps,
  mapDispatchToProps
)(UserProvider);

export const UserConsumer = UserContext.Consumer;

export const withUser = WrappedComponent => props => (
  <UserConsumer>{context => <WrappedComponent userContext={context} {...props} />}</UserConsumer>
);
