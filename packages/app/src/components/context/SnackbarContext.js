import React, { Component } from 'react';

import Snackbar from 'components/shared/Snackbar';

const SnackbarContext = React.createContext();

export class SnackbarProvider extends Component {
  constructor(props) {
    super(props);

    this.state = {
      open: false,
      message: '',
      variant: 'info',
    };
  }

  show = (message, variant) => {
    this.setState({
      open: true,
      message,
      variant,
    });
  };

  success = message => this.show(message, 'success');

  info = message => this.show(message, 'info');

  warn = message => this.show(message, 'warn');

  error = message => this.show(message, 'error');

  hide = () => {
    this.setState({
      open: false,
    });
  };

  onExited = () => {
    this.setState({
      message: '',
      variant: 'info',
    });
  };

  render() {
    const { children } = this.props;
    const { open, message, variant } = this.state;

    return (
      <SnackbarContext.Provider
        value={{
          show: this.show,
          success: this.success,
          info: this.info,
          warn: this.warn,
          error: this.error,
          hide: this.hide,
        }}
      >
        <Snackbar
          open={open}
          onClose={this.hide}
          onExited={this.onExited}
          message={message}
          variant={variant}
        />

        {children}
      </SnackbarContext.Provider>
    );
  }
}

export const SnackbarConsumer = SnackbarContext.Consumer;

export const withSnackbar = WrappedComponent => props => (
  <SnackbarConsumer>
    {context => <WrappedComponent snackbar={context} {...props} />}
  </SnackbarConsumer>
);
