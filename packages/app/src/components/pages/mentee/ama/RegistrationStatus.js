import { withUser } from 'components/context/UserContext';

function RegistrationStatus({
  attendances = [],
  limitAttendances,
  userContext: {
    user: { menteeId },
  },
  children,
}) {
  const userIsRegistered = attendances.some(
    attendance =>
      attendance.menteeId === menteeId &&
      (attendance.status === 'Scheduled' || attendance.status === 'Pending')
  );

  const userOnWaitingList = attendances.some(
    attendance => attendance.menteeId === menteeId && attendance.status === 'Pending'
  );

  const numSeatsTaken = attendances.filter(attendance => attendance.status === 'Scheduled').length;

  const numSeatsAvailable = limitAttendances - numSeatsTaken;

  let status;
  if (userIsRegistered) {
    status = userOnWaitingList ? 'REGISTERED_ON_WAITING_LIST' : 'REGISTERED';
  } else {
    status = numSeatsAvailable > 0 ? 'NOT_REGISTERED_AVAILABLE' : 'NOT_REGISTERED_FULL';
  }

  let header;
  switch (status) {
    case 'REGISTERED':
      header = 'Registered';
      break;
    case 'REGISTERED_ON_WAITING_LIST':
      header = 'Registered on waiting list';
      break;
    case 'NOT_REGISTERED_FULL':
      header = 'Full';
      break;
    case 'NOT_REGISTERED_AVAILABLE':
      header = numSeatsAvailable > 1 ? `${numSeatsAvailable} seats available` : '1 seat available';
      break;
    default:
      header = '';
  }

  return children({
    userIsRegistered,
    userOnWaitingList,
    numSeatsTaken,
    header,
    status,
  });
}

export default withUser(RegistrationStatus);
