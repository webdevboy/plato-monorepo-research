import React, { Component } from 'react';

import AttendanceService from 'services/api/plato/attendance/attendance';
import Button from 'components/shared/Button';
import PlatoDialog from 'components/shared/PlatoDialog';
import SessionEventDetails from './SessionEventDetails';
import SuccessMessage from 'components/shared/SuccessMessage';
import SuccessfulBookingMessage from 'components/shared/SuccessfulBookingMessage';
import styled from 'styled-components';
import { trackEventBooked } from 'utils/analytics';
import { withQueryLoader } from 'components/context/QueryLoaderContext';
import { withRouter } from 'react-router-dom';
import { withSnackbar } from 'components/context/SnackbarContext';
import { withUser } from 'components/context/UserContext';

const Body = styled.div`
  width: 54rem;
  padding: 3rem;

  & ${SuccessMessage} {
    margin-left: -3rem;
    margin-right: -3rem;
    margin-bottom: -3rem;
  }
`;

const Topic = styled.h4`
  margin-bottom: 0.6rem;
`;

const StoryName = styled.h1`
  margin-bottom: 0.6rem;
`;

const Host = styled.p`
  margin-bottom: 2.9rem;
`;

const HostedBy = styled.span`
  color: ${props => props.theme.palette.cerulean};
`;

const DetailsContainer = styled.div`
  width: 100%;
  padding: 2rem 3rem;
  margin-bottom: 3rem;
  background: ${props => props.theme.palette.polar};
  border: 1px solid ${props => props.theme.palette.geyser};
`;

const QuestionsLabel = styled.h4`
  margin-bottom: 1rem;
`;

const QuestionsInput = styled.textarea`
  border: 1px solid ${props => props.theme.palette.geyser};
  width: 100%;
  height: 14rem;
  padding: 1.5rem 1rem;
  margin-bottom: 4rem;
`;

const StyledButton = styled(Button)`
  position: relative;
  left: 100%;
  transform: translate(-100%);
`;

class SignUpDialog extends Component {
  constructor(props) {
    super(props);

    this.state = {
      questions: '',
      successfullyBooked: false,
      loading: false,
    };
  }

  onChange = event => {
    this.setState({
      questions: event.target.value,
    });
  };

  onConfirmSignUpClick = async () => {
    const {
      amaId,
      callScheduledAt,
      onConfirm,
      queryLoader,
      snackbar,
      userContext: { user },
    } = this.props;
    const { questions } = this.state;

    this.setState({
      loading: true,
    });
    queryLoader.showLoader();

    try {
      await AttendanceService.createAttendance(user.menteeId, amaId, questions);

      trackEventBooked({
        email: user.email,
        eventCallScheduledDate: callScheduledAt,
        eventId: amaId,
        eventType: 'ama',
        menteeId: user.menteeId,
        uuid: user.uuid,
      });

      this.setState({ successfullyBooked: true });
      onConfirm();
    } catch (error) {
      this.setState({ successfullyBooked: false });
      snackbar.error('Something went wrong - sorry!');
      console.error(error);
    }

    queryLoader.hideLoader();
    this.setState({ loading: false });
  };

  resetState = () => {
    this.setState({
      loading: false,
      questions: '',
      successfullyBooked: false,
    });
  };

  render() {
    const {
      open,
      onClose,
      storyName,
      mentorFullName,
      mentorTitle,
      mentorCompanyName,
      limitAttendances,
      attendances,
      callScheduledAt,
      history,
    } = this.props;

    const { successfullyBooked } = this.state;

    return (
      <PlatoDialog open={open} onClose={onClose} onExited={this.resetState}>
        <Body>
          <Topic>Topic</Topic>
          <StoryName>{storyName}</StoryName>
          <Host>
            Hosted by{' '}
            <HostedBy>
              <b>{mentorFullName}</b>
            </HostedBy>
            , {mentorTitle} at <b>{mentorCompanyName}</b>
          </Host>
          {!successfullyBooked && (
            <React.Fragment>
              <DetailsContainer>
                <SessionEventDetails
                  attendances={attendances}
                  limitAttendances={limitAttendances}
                  callScheduledAt={callScheduledAt}
                />
              </DetailsContainer>
              <QuestionsLabel>What questions do you want to ask</QuestionsLabel>
              <QuestionsInput
                value={this.state.questions}
                onChange={this.onChange}
                placeholder="Add some questions you would like to ask or problems you would like to discuss (optional but recommended)"
              />
              <StyledButton
                onClick={this.onConfirmSignUpClick}
                variant="primary"
                disabled={this.state.loading}
              >
                Confirm Sign Up
              </StyledButton>
            </React.Fragment>
          )}
          {successfullyBooked && (
            <SuccessMessage onClick={() => history.push('/sessions')} buttonText="View My Sessions">
              <SuccessfulBookingMessage
                message={`You successfully booked an AMA session with ${mentorFullName}`}
                timestamp={callScheduledAt}
              />
            </SuccessMessage>
          )}
        </Body>
      </PlatoDialog>
    );
  }
}

export default withSnackbar(withUser(withQueryLoader(withRouter(SignUpDialog))));
