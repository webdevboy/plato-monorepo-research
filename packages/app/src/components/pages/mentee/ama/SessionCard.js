import React, { Component } from 'react';

import AmaInfo from 'components/shared/AmaInfo';
import Button from 'components/shared/Button';
import Card from 'components/shared/Card';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import RegistrationStatus from './RegistrationStatus';
import SessionEventDetails from './SessionEventDetails';

const Body = styled.div`
  position: relative;
  display: flex;
  min-height: 22.6rem;
  width: 70rem;
`;

const Left = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  position: relative;
  min-height: 100%;
  padding: 2rem;
  min-width: 27rem;
  max-width: 27rem;
  border-right: 1px solid ${props => props.theme.palette.geyser};
  background: ${props => props.theme.palette.polar};

  ${Button} {
    width: 100%;
    margin-top: 2rem;
  }
`;

const Right = styled.div`
  height: 100%;
  padding: 2rem;
`;

class SessionCard extends Component {
  render() {
    const {
      attendances,
      callScheduledAt,
      limitAttendances,
      storyId,
      storyName,
      mentorId,
      mentorFullName,
      mentorPictureUrl,
      mentorTitle,
      mentorCompanyName,
      topics,
      onSignUpClick,
      className,
    } = this.props;

    return (
      <RegistrationStatus attendances={attendances} limitAttendances={limitAttendances}>
        {({ status }) => (
          <Card className={className}>
            <Body>
              <Left>
                <SessionEventDetails
                  attendances={attendances}
                  limitAttendances={limitAttendances}
                  callScheduledAt={callScheduledAt}
                />
                {status === 'NOT_REGISTERED_FULL' && (
                  <Button variant="primary-link" onClick={onSignUpClick}>
                    Sign Up for Waiting List
                  </Button>
                )}
                {status === 'NOT_REGISTERED_AVAILABLE' && (
                  <Button className="sign-up-button" variant="primary" onClick={onSignUpClick}>
                    Sign Up
                  </Button>
                )}
              </Left>
              <Right>
                <AmaInfo
                  storyId={storyId}
                  storyName={storyName}
                  mentorId={mentorId}
                  mentorPictureUrl={mentorPictureUrl}
                  mentorFullName={mentorFullName}
                  mentorTitle={mentorTitle}
                  mentorCompanyName={mentorCompanyName}
                  topics={topics}
                />
              </Right>
            </Body>
          </Card>
        )}
      </RegistrationStatus>
    );
  }
}

SessionCard.propTypes = {
  attendances: PropTypes.array,
  callScheduledAt: PropTypes.number.isRequired,
  className: PropTypes.string,
  limitAttendances: PropTypes.number.isRequired,
  mentorCompanyName: PropTypes.string,
  mentorFullName: PropTypes.string.isRequired,
  mentorId: PropTypes.number,
  mentorPictureUrl: PropTypes.string.isRequired,
  mentorTitle: PropTypes.string,
  onSignUpClick: PropTypes.func.isRequired,
  storyId: PropTypes.number,
  storyName: PropTypes.string.isRequired,
  topics: PropTypes.array.isRequired,
};

SessionCard.defaultProps = {
  className: null,
  mentorId: null,
  storyId: null,
  attendances: [],
  mentorCompanyName: '',
  mentorTitle: '',
};

export default styled(SessionCard)``;
