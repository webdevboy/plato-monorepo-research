import { Link } from 'react-router-dom';
import React from 'react';
import styled from 'styled-components';

const StyledWrapper = styled.div`
  position: relative;
  background-image: url(${({ pictureUrl }) => pictureUrl});
  background-position: center;
  background-size: cover;
  background-clip: padding-box;
  min-width: 27rem;
  max-width: 27rem;
  color: ${({ theme }) => theme.palette.white};
  overflow: hidden;
  border-radius: 4px 0 0 4px;

  .link-details {
    position: absolute;
    display: flex;
    flex-direction: column;
    width: 100%;
    bottom: 0;
    padding: 4rem 0.5rem 0 2.5rem;
    background-image: linear-gradient(
      to bottom,
      ${({ theme }) => theme.palette.withOpacity(theme.palette.black, 1)},
      ${({ theme }) => theme.palette.black}
    );

    h1 {
      font-weight: normal;
    }

    sub {
      margin-bottom: 2rem;
    }
  }
`;

const AmaDetailLeft = ({ ama }) => (
  <StyledWrapper pictureUrl={ama.mentorPicture.originalUrl}>
    <Link className="link-details" to={`/mentors?mentor_id=${ama._mentor.id}&source=ama`}>
      <h1 className="name">{ama.mentorFullName}</h1>
      <sub>
        {ama._mentor.title} at <strong>{ama._mentor.companyName}</strong>
      </sub>
    </Link>
  </StyledWrapper>
);

export default AmaDetailLeft;
