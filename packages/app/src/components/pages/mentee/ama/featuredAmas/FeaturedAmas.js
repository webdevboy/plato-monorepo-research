import React, { Component } from 'react';

import AmaDetail from './AmaDetail';
import AmaList from './AmaList';
import Fade from '@material-ui/core/Fade';
import styled from 'styled-components';

const StyledWrapper = styled.div`
  display: flex;
  min-height: 33rem;

  & ${AmaDetail} {
    width: 100%;
  }
`;

const TRANSITION_INTERVAL = 5000; // 5s

class FeaturedAmaSessions extends Component {
  constructor(props) {
    super(props);
    const { amas } = props;
    this.state = {
      selectedAma: amas[0],
      transitioningAma: undefined,
      cardIn: true,
    };
  }

  componentDidMount() {
    this.interval = setInterval(() => {
      const { amas } = this.props;
      const { selectedAma } = this.state;
      const selectedAmaIndex = amas.indexOf(selectedAma);
      const nextIndex = selectedAmaIndex === amas.length - 1 ? 0 : selectedAmaIndex + 1;

      const newSelectedAma = amas[nextIndex];

      this.handleAmaChange(newSelectedAma);
    }, TRANSITION_INTERVAL);
  }

  componentWillUnmount() {
    this.stopAutoSelecting();
  }

  handleAmaChange = newSelectedAma => {
    this.setState({
      transitioningAma: newSelectedAma,
      cardIn: false,
    });
  };

  handleClick = newSelectedAma => {
    this.handleAmaChange(newSelectedAma);

    this.stopAutoSelecting();
  };

  onTransitionComplete = () => {
    this.setState(prevState => ({
      cardIn: true,
      selectedAma: prevState.transitioningAma,
    }));
  };

  stopAutoSelecting = () => {
    clearInterval(this.interval);
  };

  render() {
    const { amas, openSignUpDialog, className } = this.props;
    const { selectedAma, cardIn } = this.state;

    return (
      <StyledWrapper className={className}>
        <AmaList
          amas={amas}
          selectedAma={selectedAma}
          onChange={this.handleAmaChange}
          handleClick={this.handleClick}
        />
        {/* TODO:  Figure out a nice animated transition here */}
        <Fade in={cardIn} onExited={this.onTransitionComplete} unmountOnExit>
          <AmaDetail ama={selectedAma} openSignUpDialog={openSignUpDialog} />
        </Fade>
      </StyledWrapper>
    );
  }
}

export default styled(FeaturedAmaSessions)``;
