import React, { Component } from 'react';

import AmaListItem from './AmaListItem';
import styled from 'styled-components';

const StyledWrapper = styled.div`
  position: relative;
  display: flex;
  height: fit-content;
  min-width: 29rem;
  max-width: 29rem;
  padding: 0rem 2rem;
  border-left: 1px solid ${({ theme: { palette } }) => palette.withOpacity(palette.cadetBlue, 20)};
`;

const Slider = styled.div`
  position: absolute;
  left: -1px;
  width: 1px;
  ${({ theme }) => `
    background: ${theme.palette.java};
    transition: ${theme.transitions.default} height,
                ${theme.transitions.default} top;
  `};
`;

class AmaList extends Component {
  constructor(props) {
    super(props);

    this.selectedAmaRef = React.createRef();

    this.state = {
      sliderHeight: 0,
      sliderTop: 0,
    };
  }

  updateSliderPosition = amaRef => {
    this.setState({
      sliderHeight: amaRef.clientHeight,
      sliderTop: amaRef.offsetTop,
    });
  };

  handleSelectedAmaChange = (amaRef, ama) => {
    this.updateSliderPosition(amaRef);

    this.props.onChange(ama);
  };

  handleClick = (amaRef, ama) => {
    this.updateSliderPosition(amaRef);

    this.props.handleClick(ama);
  };

  render() {
    const { amas, selectedAma } = this.props;
    const { sliderHeight, sliderTop } = this.state;

    return (
      <StyledWrapper>
        <Slider style={{ top: sliderTop, height: sliderHeight }} />
        <div>
          {amas.map((ama, index) => (
            <AmaListItem
              key={ama.id}
              ama={ama}
              selected={ama === selectedAma}
              onRender={index === 0 ? this.updateSliderPosition : () => {}}
              onChange={this.handleSelectedAmaChange}
              handleClick={this.handleClick}
            />
          ))}
        </div>
      </StyledWrapper>
    );
  }
}

export default AmaList;
