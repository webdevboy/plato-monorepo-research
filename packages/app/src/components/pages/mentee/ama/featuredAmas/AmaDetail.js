import React, { Component } from 'react';
import styled from 'styled-components';
import AmaDetailLeft from './AmaDetailLeft';
import AmaDetailRight from './AmaDetailRight';

const StyledWrapper = styled.div`
  display: flex;
  width: 100%;
  background: white;
  border-radius: 4px;
  box-shadow: ${({ theme }) => theme.shadows[0]};
  overflow: hidden;
`;

class AmaDetail extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    const { ama, openSignUpDialog, className } = this.props;

    return (
      <StyledWrapper className={className}>
        <AmaDetailLeft ama={ama} />
        <AmaDetailRight ama={ama} openSignUpDialog={openSignUpDialog} />
      </StyledWrapper>
    );
  }
}

export default styled(AmaDetail)``;
