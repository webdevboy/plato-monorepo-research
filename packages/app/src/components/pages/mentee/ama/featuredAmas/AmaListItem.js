import React, { Component } from 'react';

import ReactDOM from 'react-dom';
import moment from 'moment';
import styled from 'styled-components';

const StyledWrapper = styled.div`
  cursor: pointer;

  &:not(:last-child) {
    margin-bottom: 3rem;
  }

  & .story-name {
    font-size: 1.6rem;
    margin-bottom: 0.5rem;
    line-height: 1.5;
    color: ${({ theme }) => theme.palette.white};
    ${({ selected }) => (selected ? 'font-weight: bold' : '')}
    transition: ${({ theme }) => theme.transitions.default} font-weight;
  }

  & .date {
    color: ${({ theme }) => theme.palette.cadetBlue};
  }
`;

class AmaListItem extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    this.props.onRender(ReactDOM.findDOMNode(this)); // eslint-disable-line react/no-find-dom-node
  }

  componentDidUpdate(prevProps) {
    if (!prevProps.selected && this.props.selected) {
      this.props.onChange(ReactDOM.findDOMNode(this), this.props.ama); // eslint-disable-line react/no-find-dom-node
    }
  }

  render() {
    const { ama, handleClick, selected } = this.props;

    return (
      <StyledWrapper
        key={ama.id}
        onClick={() => handleClick(ReactDOM.findDOMNode(this), ama)} // eslint-disable-line react/no-find-dom-node
        selected={selected}
      >
        <p className="story-name">{ama.storyName}</p>
        <p className="date">{moment(ama.callScheduledAt).format('MMMM D YYYY')}</p>
      </StyledWrapper>
    );
  }
}

export default AmaListItem;
