import Button from 'components/shared/Button';
import DateTimeWithIcons from 'components/shared/DateTimeWithIcons';
import IconLabel from 'components/shared/IconLabel';
import { Link } from 'react-router-dom';
import React from 'react';
import RegistrationStatus from '../RegistrationStatus';
import TagList from 'components/shared/TagList';
import styled from 'styled-components';

const StyledWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  padding: 3rem;
  padding-right: 2rem;
  width: 100%;
  border-radius: 0 4px 4px 0;

  ${({ theme }) => `
    border: 1px solid ${theme.palette.geyser};
    border-left: none;
  `} & h4 {
    font-size: 1.4rem;
    margin-bottom: 0.5rem;
  }

  & h1 {
    margin-bottom: 1rem;
  }

  & ${TagList} {
    margin-bottom: 2rem;
  }

  & .event-details {
    display: inline-flex;
    margin-bottom: 3rem;
  }
`;

const Availability = styled.p`
  font-weight: bold;
  font-size: 1.6rem;
  margin-bottom: 1rem;
`;

const AmaDetailRight = ({ ama, openSignUpDialog }) => (
  <RegistrationStatus attendances={ama._attendances} limitAttendances={ama.limitAttendances}>
    {({ status, header }) => (
      <StyledWrapper>
        <div>
          <h4>Topic</h4>
          {ama.storyId ? (
            <Link to={`/stories/${ama.storyId}?source=ama`}>
              <h1>{ama.storyName}</h1>
            </Link>
          ) : (
            <h1>{ama.storyName}</h1>
          )}

          <TagList tags={ama.storyTagNames} />
          <Availability>{header}</Availability>
          <div className="event-details">
            <DateTimeWithIcons timestamp={ama.callScheduledAt} />
            <IconLabel icon="user">
              {ama.seatsScheduledNb} / {ama.limitAttendances} attendees
            </IconLabel>
          </div>
        </div>
        <div>
          {status === 'NOT_REGISTERED_AVAILABLE' && (
            <Button variant="primary" onClick={() => openSignUpDialog(ama)}>
              Sign Up
            </Button>
          )}
          {status === 'NOT_REGISTERED_FULL' && (
            <Button variant="primary-link" onClick={() => openSignUpDialog(ama)}>
              Sign Up for Wait List
            </Button>
          )}
        </div>
      </StyledWrapper>
    )}
  </RegistrationStatus>
);

export default AmaDetailRight;
