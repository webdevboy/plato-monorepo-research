import AmaService from 'services/api/plato/ama/ama';
import MenteeService from 'services/api/plato/mentee/mentee';
import MentorService from 'services/api/plato/mentor/mentor';
import TagService from 'services/api/plato/tag/tag';
import getFaceDisplayName from 'utils/faceDisplayNames';
import moment from 'moment';

export default {
  async getTopicFilterOptions() {
    const topics = await TagService.getTopicsForFilter();

    return topics
      .filter(tag => tag.amaCount > 0)
      .sort((tagA, tagB) => tagB.amaCount - tagA.amaCount)
      .map(topic => ({
        id: topic.id,
        name: `${topic.name} (${topic.amaCount})`,
        checked: false,
      }));
  },

  async getRoleFilterOptions(menteeId) {
    const [allFaces, mentee] = await Promise.all([TagService.getFaces(), this.getMentee(menteeId)]);

    return allFaces.map(face => ({
      id: face.id,
      name: getFaceDisplayName(face.name),
      checked: mentee.faces.some(tag => tag.id === face.id),
    }));
  },

  async getMentee(menteeId) {
    return MenteeService.getMentee(menteeId);
  },

  async getAmaPage({
    faceIds,
    isFeatured,
    page,
    query,
    scheduledAfterHour,
    scheduledBeforeHour,
    scheduledOnDays,
    size,
    minAttendees,
    tagIds,
  }) {
    const response = await AmaService.getAmasForMentee({
      faceIds,
      isFeatured,
      page,
      query,
      scheduledAfterHour,
      scheduledBeforeHour,
      scheduledOnDays,
      size,
      tagIds,
      minAttendees,
      hasProgram: false,
    });
    await this.getAdditionalAmaInfo(response.result);
    response.result.sort((amaA, amaB) =>
      moment(amaA.callScheduledAt).diff(moment(amaB.callScheduledAt))
    );
    return response;
  },

  async getAdditionalAmaInfo(amas) {
    const attendancesPromises = amas.map(ama => AmaService.getAttendances(ama.id));
    const mentorPromises = amas.map(ama => MentorService.getMentor(ama.mentorId));

    const allAttendances = await Promise.all(attendancesPromises);
    const allMentors = await Promise.all(mentorPromises);

    amas.forEach((ama, index) => {
      ama._attendances = allAttendances[index]; // eslint-disable-line no-param-reassign
      ama._mentor = allMentors[index]; // eslint-disable-line no-param-reassign
    });
  },
};
