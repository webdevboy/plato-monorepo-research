import React, { Fragment, PureComponent } from 'react';
import {
  TIME_SLOT_10AM_12PM,
  TIME_SLOT_12AM_6AM,
  TIME_SLOT_12PM_2PM,
  TIME_SLOT_2PM_4PM,
  TIME_SLOT_4PM_6PM,
  TIME_SLOT_6AM_8AM,
  TIME_SLOT_6PM_12AM,
  TIME_SLOT_8AM_10AM,
  TIME_SLOT_ANY,
} from 'constants/TimeSlots';
import { trackAmaCalendarImported, trackFilterUsed } from 'utils/analytics';

import AmaCardList from 'components/pages/mentee/ama/AmaCardList';
import AmaService from 'services/api/plato/ama/ama';
import Button from 'components/shared/Button';
import Checkbox from 'components/shared/Checkbox';
import EmptyStateCard from 'components/shared/EmptyStateCard';
import FeaturedAmas from './featuredAmas/FeaturedAmas';
import FilterList from 'components/shared/FilterList';
import Hero from 'components/shared/Hero';
import IconLabel from 'components/shared/IconLabel';
import Layout from 'components/shared/Layout';
import PageHeader from 'components/shared/PageHeader';
import PageLoadingSpinner from 'components/shared/PageLoadingSpinner';
import SearchInput from 'components/shared/SearchInput';
import SessionCard from './SessionCard';
import SignUpDialog from './SignUpDialog';
import Tag from 'components/shared/Tag';
import amaService from './amaService';
import calendarPages from 'images/calendar-pages.png';
import { getTimeSlotBounds } from 'utils/time';
import moment from 'moment';
import queryString from 'query-string';
import styled from 'styled-components';
import { withQueryLoader } from 'components/context/QueryLoaderContext';
import { withSnackbar } from 'components/context/SnackbarContext';
import { withUser } from 'components/context/UserContext';

const StyledLayout = styled(Layout)`
  margin-top: 8rem;
  margin-bottom: 3rem;

  header {
    margin-bottom: 8rem;
  }

  ${EmptyStateCard}, ${SessionCard} {
    margin: 1.9rem 0;
  }

  .calendar-empty-state {
    border: none;
  }

  .sessions-header {
    margin-bottom: 0.4rem;
    display: flex;
    flex-direction: row;
    height: 20px;

    .spacer {
      flex: 1;
    }
  }
`;

const AmaSessionsContainer = styled.div`
  display: flex;
  width: 100%;
`;

const FiltersColumn = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: 4.2rem;
  width: 29rem;
  padding: 0rem 2rem 0 0;

  > * {
    width: 100%;
    margin-bottom: 2.9rem;
  }

  .tag {
    margin: 0 0.5rem 0.5rem 0;
  }

  .days-filter,
  .times-filter {
    margin-bottom: 2rem;

    h4 {
      margin-bottom: 1rem;
    }
  }
`;

const PagesColumn = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
`;

const selectableDays = ['Any day', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'];

const selectableTimeSlots = [
  TIME_SLOT_ANY,
  TIME_SLOT_12AM_6AM,
  TIME_SLOT_6AM_8AM,
  TIME_SLOT_8AM_10AM,
  TIME_SLOT_10AM_12PM,
  TIME_SLOT_12PM_2PM,
  TIME_SLOT_2PM_4PM,
  TIME_SLOT_4PM_6PM,
  TIME_SLOT_6PM_12AM,
];

const numberOfAmaByPage = 5;

class Ama extends PureComponent {
  state = {
    amas: [],
    currentPageIndex: 0,
    featuredAmas: [],
    loading: false,
    roleFilterOptions: [],
    searchQuery: '',
    selectedDayIndexes: [],
    selectedTimeSlot: TIME_SLOT_ANY,
    selectedView: 'list', // Two views available: list or calendar
    signUpAma: undefined,
    signUpDialogIsOpen: false,
    topicFilterOptions: [],
    totalItems: undefined,
    totalPages: undefined,
  };

  async componentDidMount() {
    const { location, snackbar } = this.props;
    this.setState({ loading: true });
    try {
      await this.getFilterOptions();
      await Promise.all([this.getFeaturedAmas(), this.fetchAma(0)]);

      const { ama_id: amaId } = queryString.parse(location.search);
      if (amaId) {
        // Prompt the user to log into an AMA
        const signUpAma = await AmaService.getAma(amaId);
        await amaService.getAdditionalAmaInfo([signUpAma]);
        this.setState({ signUpDialogIsOpen: true, signUpAma });
      }
    } catch (error) {
      console.error(error);
      snackbar.error('Something went wrong - sorry!');
    }

    this.setState({
      loading: false,
    });
  }

  async getFilterOptions() {
    const {
      userContext: {
        user: { menteeId },
      },
    } = this.props;
    const [roleFilterOptions, topicFilterOptions] = await Promise.all([
      amaService.getRoleFilterOptions(menteeId),
      amaService.getTopicFilterOptions(),
    ]);

    this.setState({
      roleFilterOptions,
      topicFilterOptions,
    });
  }

  async getFeaturedAmas() {
    const { roleFilterOptions } = this.state;
    const { result: featuredAmas } = await amaService.getAmaPage({
      page: 0,
      size: 3,
      faceIds: roleFilterOptions.map(option => option.id).join(','),
      tagIds: [],
      isFeatured: true,
      minAttendees: 0,
    });

    this.setState({ featuredAmas });
  }

  handleLoadAmaPage = async (page = 0) => {
    const { snackbar, queryLoader } = this.props;
    queryLoader.showLoader();

    try {
      await this.fetchAma(page);
    } catch (error) {
      console.error(error);
      snackbar.error(error);
    }

    queryLoader.hideLoader();
  };

  handleRoleFilterChanged = option => {
    const {
      userContext: {
        user: { uuid, email, menteeId },
      },
    } = this.props;
    const { roleFilterOptions: oldRoleFilterOptions } = this.state;
    const roleFilterOptions = [...oldRoleFilterOptions];
    const toggledOption = roleFilterOptions.find(roleOption => roleOption === option);

    toggledOption.checked = !option.checked;

    this.setState(
      {
        amas: [],
        roleFilterOptions,
        currentPageIndex: 0,
      },
      this.handleLoadAmaPage
    );

    trackFilterUsed({
      menteeId,
      location: 'ama',
      filterName: toggledOption.name,
      filterValue: toggledOption.checked,
      filterType: 'role',
      uuid,
      email,
    });
  };

  handleTopicFilterChanged = option => {
    const {
      userContext: {
        user: { uuid, email, menteeId },
      },
    } = this.props;
    const { topicFilterOptions: oldTopicFilterOptions } = this.state;
    const topicFilterOptions = [...oldTopicFilterOptions];
    const toggledOption = topicFilterOptions.find(topicOption => topicOption === option);
    toggledOption.checked = !option.checked;

    this.setState(
      {
        amas: [],
        topicFilterOptions,
        currentPageIndex: 0,
      },
      this.handleLoadAmaPage
    );

    trackFilterUsed({
      menteeId,
      location: 'ama',
      filterName: toggledOption.name,
      filterValue: toggledOption.checked,
      filterType: 'topic',
      uuid,
      email,
    });
  };

  handleCloseSignUpDialog = () => {
    this.setState({
      signUpDialogIsOpen: false,
    });
  };

  handleOpenSignUpDialog = ama => {
    this.setState({ signUpDialogIsOpen: true, signUpAma: ama });
  };

  handleShowListView = () => {
    this.setState({ selectedView: 'list' });
  };

  handleShowCalendarView = () => {
    this.setState({ selectedView: 'calendar' });
  };

  handleAddToCalendar = () => {
    const {
      userContext: {
        user: { uuid, email, menteeId },
      },
    } = this.props;
    trackAmaCalendarImported({ uuid, email, menteeId });
  };

  handleSelectDay = dayIndex => {
    const { selectedDayIndexes } = this.state;
    // Handle the 'Any day' option
    if (dayIndex === 0) {
      this.setState({ selectedDayIndexes: [] }, this.handleLoadAmaPage);
      return;
    }

    this.setState(
      {
        selectedDayIndexes: selectedDayIndexes.includes(dayIndex)
          ? selectedDayIndexes.filter(selectedDayIndex => selectedDayIndex !== dayIndex)
          : [dayIndex, ...selectedDayIndexes],
      },
      this.handleLoadAmaPage
    );
  };

  handleSelectTime = selectedTimeSlot => {
    this.setState({ selectedTimeSlot }, this.handleLoadAmaPage);
  };

  handleQueryChange = event => {
    const {
      target: { value },
    } = event;
    this.setState({ searchQuery: value }, this.handleLoadAmaPage);
  };

  async fetchAma(page) {
    const { userContext } = this.props;
    const {
      roleFilterOptions,
      searchQuery,
      selectedDayIndexes,
      selectedTimeSlot,
      topicFilterOptions,
    } = this.state;
    const { lower: scheduledAfterHour, higher: scheduledBeforeHour } = getTimeSlotBounds(
      selectedTimeSlot,
      userContext.timeZone()
    );
    const { result: amas, totalPages, totalItems } = await amaService.getAmaPage({
      page,
      query: searchQuery,
      size: numberOfAmaByPage,
      scheduledBeforeHour,
      scheduledAfterHour,
      scheduledOnDays: selectedDayIndexes.join(','),
      faceIds: roleFilterOptions
        .reduce((acc, option) => (option.checked ? acc.concat(option.id) : acc), [])
        .join(','),
      tagIds: topicFilterOptions
        .reduce((acc, option) => (option.checked ? acc.concat(option.id) : acc), [])
        .join(','),
      isFeatured: false,
      minAttendees: 0,
    });

    this.setState(prevState => ({
      amas: page === 0 ? amas : [...prevState.amas, ...amas],
      totalPages,
      totalItems,
      currentPageIndex: page,
    }));
  }

  renderView() {
    const {
      amas,
      currentPageIndex,
      selectedView,
      topicFilterOptions,
      totalItems,
      totalPages,
    } = this.state;

    if (selectedView === 'calendar') {
      return (
        <EmptyStateCard
          title="The calendar view is not ready yet!"
          className="calendar-empty-state"
          image={<img src={calendarPages} alt="Some calendar pages with confetti around them" />}
          description={
            <>
              We see a lot of our users interested in this feature!
              <br />
              While we are working to make this happen, you can add the AMA
              <br />
              schedule into your google calendar with the following link.
            </>
          }
        >
          <a
            href="https://calendar.google.com/calendar/r?cid=cGxhdG8tYW1hQHBsYXRvaHEuY29t"
            target="_blank"
            rel="noopener noreferrer"
          >
            <Button variant="primary" onClick={this.handleAddToCalendar}>
              Add to Calendar
            </Button>
          </a>
        </EmptyStateCard>
      );
    }

    if (totalItems === 0) {
      return <EmptyStateCard title="We didn't find any matches." />;
    }

    return (
      <AmaCardList
        amas={amas}
        onLoadMore={this.handleLoadAmaPage}
        topicFilterOptions={topicFilterOptions}
        openSignUpDialog={this.handleOpenSignUpDialog}
        currentPageIndex={currentPageIndex}
        totalPages={totalPages}
      />
    );
  }

  render() {
    const {
      featuredAmas,
      loading,
      roleFilterOptions,
      searchQuery,
      selectedDayIndexes,
      selectedTimeSlot,
      selectedView,
      signUpAma,
      signUpDialogIsOpen,
      topicFilterOptions,
      totalItems,
    } = this.state;

    const { userContext } = this.props;

    if (loading) {
      return (
        <>
          <Hero height="440" />
          <PageLoadingSpinner />
        </>
      );
    }

    return (
      <>
        <Hero height="440" />
        <StyledLayout width="1118">
          <header>
            {featuredAmas.length > 0 ? (
              <>
                <PageHeader>Featured AMA Sessions</PageHeader>
                <FeaturedAmas
                  loading={loading}
                  amas={featuredAmas}
                  openSignUpDialog={this.handleOpenSignUpDialog}
                />
              </>
            ) : (
              <EmptyStateCard
                title={
                  <>
                    Oh! It appears there are no featured AMA sessions
                    <br />
                    at the moment.
                  </>
                }
                description="Hang in there, new sessions are created every day!"
              />
            )}
          </header>
          <AmaSessionsContainer>
            <FiltersColumn>
              <SearchInput
                placeholder="Search"
                onChange={this.handleQueryChange}
                value={searchQuery}
              />
              <div className="days-filter">
                <h4>Days available ({moment.tz(userContext.timeZone()).zoneName()})</h4>
                {selectableDays.map((day, index) => (
                  <Fragment key={index}>
                    <Tag
                      className="tag"
                      value={index}
                      text={day}
                      size="small"
                      isSelectable
                      isSelected={
                        index === 0
                          ? selectedDayIndexes.length === 0
                          : selectedDayIndexes.includes(index)
                      }
                      variant="light"
                      onClick={this.handleSelectDay}
                    />
                    {index === 0 && <br />}
                  </Fragment>
                ))}
              </div>
              <div className="times-filter">
                <h4>Times available ({moment.tz(userContext.timeZone()).zoneName()})</h4>
                {selectableTimeSlots.map((timeSlot, index) => (
                  <Fragment key={index}>
                    <Tag
                      className="tag"
                      value={timeSlot}
                      text={timeSlot}
                      size="small"
                      isSelectable
                      isSelected={selectedTimeSlot === timeSlot}
                      variant="light"
                      onClick={this.handleSelectTime}
                    />
                    {index === 0 && <br />}
                  </Fragment>
                ))}
              </div>
              <FilterList title="Filter by Roles">
                {roleFilterOptions.map(option => (
                  <Checkbox
                    key={option.id}
                    label={option.name}
                    checked={option.checked}
                    onChange={() => this.handleRoleFilterChanged(option)}
                  />
                ))}
              </FilterList>
              <FilterList title="Filter by Topics">
                {topicFilterOptions.map(option => (
                  <Checkbox
                    key={option.id}
                    label={option.name}
                    checked={option.checked}
                    onChange={() => this.handleTopicFilterChanged(option)}
                  />
                ))}
              </FilterList>
            </FiltersColumn>
            <PagesColumn>
              <div className="sessions-header">
                {selectedView === 'list' && <h3>{totalItems} AMA Sessions</h3>}
                <span className="spacer" />
                <Button
                  variant={selectedView === 'list' ? 'primary-link' : 'secondary-link'}
                  onClick={this.handleShowListView}
                >
                  <IconLabel icon="list" />
                </Button>
                <Button
                  variant={selectedView === 'calendar' ? 'primary-link' : 'secondary-link'}
                  onClick={this.handleShowCalendarView}
                >
                  <IconLabel icon="calendar" />
                </Button>
              </div>
              {this.renderView()}
            </PagesColumn>
          </AmaSessionsContainer>
        </StyledLayout>

        {signUpAma && (
          <SignUpDialog
            open={signUpDialogIsOpen}
            onClose={this.handleCloseSignUpDialog}
            storyName={signUpAma.storyName}
            mentorFullName={signUpAma.mentorFullName}
            mentorTitle={signUpAma._mentor.title}
            mentorCompanyName={signUpAma._mentor.companyName}
            limitAttendances={signUpAma.limitAttendances}
            attendances={signUpAma._attendances}
            callScheduledAt={signUpAma.callScheduledAt}
            amaId={signUpAma.id}
            onConfirm={() => this.handleLoadAmaPage(0)}
          />
        )}
      </>
    );
  }
}

export default withSnackbar(withUser(withQueryLoader(Ama)));
