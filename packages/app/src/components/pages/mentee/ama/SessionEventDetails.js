import DateWithIcon from 'components/shared/DateWithIcon';
import IconLabel from 'components/shared/IconLabel';
import React from 'react';
import RegistrationStatus from './RegistrationStatus';
import TimeWithIcon from 'components/shared/TimeWithIcon';
import styled from 'styled-components';

const Wrapper = styled.div`
  h1,
  .date-info,
  .time-info {
    margin-bottom: 1rem;
  }

  .grey {
    color: ${props => props.theme.palette.cadetBlue};
  }
`;

const SessionEventDetails = props => {
  const { attendances, limitAttendances, callScheduledAt } = props;

  return (
    <RegistrationStatus attendances={attendances} limitAttendances={limitAttendances}>
      {({ status, header, numSeatsTaken }) => {
        const headerClassName = status === 'NOT_REGISTERED_FULL' ? 'grey' : '';

        return (
          <Wrapper>
            <h1 className={headerClassName}>{header}</h1>
            <DateWithIcon className="date-info" date={callScheduledAt} />
            <TimeWithIcon className="time-info" time={callScheduledAt} />
            <IconLabel icon="user">
              {numSeatsTaken}/{limitAttendances} attendees
            </IconLabel>
          </Wrapper>
        );
      }}
    </RegistrationStatus>
  );
};

export default styled(SessionEventDetails)``;
