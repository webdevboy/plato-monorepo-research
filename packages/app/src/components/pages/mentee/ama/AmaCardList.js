import React, { Component } from 'react';

import InfiniteScroll from 'react-infinite-scroller';
import LoadingSpinner from 'components/shared/LoadingSpinner';
import SessionCard from 'components/pages/mentee/ama/SessionCard';
import getProfilePictureThumbnail from 'utils/thumbnail';
import styled from 'styled-components';

const StyledInfiniteScroll = styled(InfiniteScroll)`
  display: flex;
  flex-direction: column;

  ${LoadingSpinner} {
    align-self: center;
  }
`;

class AmaCardList extends Component {
  shouldComponentUpdate(nextProps) {
    return nextProps.amas !== this.props.amas;
  }

  handleLoadMore = () => {
    const { currentPageIndex, onLoadMore } = this.props;
    onLoadMore(currentPageIndex + 1);
  };

  render() {
    const { amas, topicFilterOptions, openSignUpDialog, currentPageIndex, totalPages } = this.props;

    return (
      <StyledInfiniteScroll
        hasMore={currentPageIndex < totalPages - 1}
        loadMore={this.handleLoadMore}
        // https://github.com/CassetteRocks/react-infinite-scroller/issues/133
        loader={<LoadingSpinner key={0} />}
      >
        {amas.map(ama => {
          const topics = ama.tagIds.map(
            tagId => topicFilterOptions.find(option => option.id === tagId).name
          );

          return (
            <SessionCard
              key={ama.id}
              attendances={ama._attendances}
              limitAttendances={ama.limitAttendances}
              callScheduledAt={ama.callScheduledAt}
              storyId={ama.storyId}
              storyName={ama.storyName}
              mentorTitle={ama._mentor.title}
              mentorCompanyName={ama._mentor.companyName}
              mentorId={ama.mentorId}
              mentorFullName={ama.mentorFullName}
              mentorPictureUrl={getProfilePictureThumbnail(
                ama._mentor.firstName,
                ama.mentorPicture.thumbnailUrl
              )}
              topics={topics}
              onSignUpClick={() => openSignUpDialog(ama)}
            />
          );
        })}
      </StyledInfiniteScroll>
    );
  }
}

export default AmaCardList;
