import React, { Component } from 'react';

import EmptyStateCard from 'components/shared/EmptyStateCard';
import Layout from 'components/shared/Layout';
import LoadingSpinner from 'components/shared/LoadingSpinner';
import PropTypes from 'prop-types';
import { getTrackAppUrl } from 'utils/tracks';
import styled from 'styled-components';
import { withUser } from 'components/context/UserContext';

const StyledLayout = styled(Layout)`
  padding-top: 4rem;

  a {
    color: ${props => props.theme.palette.cerulean};
  }
`;

class Tracks extends Component {
  componentDidMount() {
    const {
      userContext: {
        user: { uuid, menteeId },
      },
    } = this.props;
    window.location = getTrackAppUrl(menteeId, uuid, localStorage.userToken);
  }

  render() {
    const {
      userContext: {
        user: { uuid, menteeId },
      },
    } = this.props;
    return (
      <StyledLayout>
        <EmptyStateCard
          title="Hang tight!"
          image={<LoadingSpinner />}
          description={
            <>
              You are being redirected. If nothing happen, you can&nbsp;
              <a
                href={getTrackAppUrl(menteeId, uuid, localStorage.userToken)}
                rel="noopener noreferrer"
              >
                click here
              </a>
              .
            </>
          }
        />
      </StyledLayout>
    );
  }
}

Tracks.propTypes = {
  userContext: PropTypes.object.isRequired,
};

export default withUser(Tracks);
