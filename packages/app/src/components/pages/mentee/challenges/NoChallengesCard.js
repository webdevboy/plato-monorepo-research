import Card from 'components/shared/Card';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';
import styled from 'styled-components';

const Body = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  padding: 3rem;

  & .emoji {
    font-size: 3.4rem;
    margin-bottom: 1rem;
  }

  & h1 {
    width: 48rem;
    text-align: center;
    margin-bottom: 3rem;
  }

  & span {
    font-size: 1.4rem;
  }

  & .list-label {
    margin-bottom: 1rem;
    line-spacing: 1.57;
  }

  & .examples {
    display: flex;
    flex-direction: column;
    font-weight: 700;

    & .example {
      margin-bottom: 1rem;
    }
  }
`;

const Icon = styled(FontAwesomeIcon)`
  margin-right: 1rem;
  color: ${props => props.theme.palette.cerulean};
`;

const NoChallengesCard = () => (
  <Card>
    <Body>
      <span className="emoji" role="img" aria-label="Hmm">
        👆
      </span>
      <h1>Looks like you haven&apos;t sent us any challenges. Open one up there!</h1>

      <span className="list-label">Here&apos;s some stuff you can type:</span>

      <div className="examples">
        <span className="example">
          <Icon icon="check" />
          <em>&quot;Building a career path plan&quot;</em>
        </span>
        <span className="example">
          <Icon icon="check" />
          <em>&quot;How to get more value out of my one on ones?&quot;</em>
        </span>
        <span className="example">
          <Icon icon="check" />
          <em>&quot;Revamping my hiring process&quot;</em>
        </span>
      </div>
    </Body>
  </Card>
);

export default NoChallengesCard;
