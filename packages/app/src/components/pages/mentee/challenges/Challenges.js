import React, { Component } from 'react';

import BookMeetingDialog from 'components/shared/BookMeetingDialog';
import BriefService from 'services/api/plato/brief/brief';
import Hero from 'components/shared/Hero';
import Layout from 'components/shared/Layout';
import MatchCard from './MatchCard';
import MenteeService from 'services/api/plato/mentee/mentee';
import MentorService from 'services/api/plato/mentor/mentor';
import NewChallengePrompt from './NewChallengePrompt';
import NoChallengesCard from './NoChallengesCard';
import PageHeader from 'components/shared/PageHeader';
import PageLoadingSpinner from 'components/shared/PageLoadingSpinner';
import styled from 'styled-components';
import { withQueryLoader } from 'components/context/QueryLoaderContext';
import { withSnackbar } from 'components/context/SnackbarContext';
import { withUser } from 'components/context/UserContext';

const Container = styled.div`
  & ${Layout} {
    padding-top: 6rem;
  }

  & .open-challenges {
    color: ${props => props.theme.palette.white};
    margin-bottom: 2rem;
  }

  & ${NewChallengePrompt} {
    margin-bottom: 6rem;
  }

  & .closed-challenges {
    text-transform: uppercase;
    margin-bottom: 2rem;
  }
`;

class Challenges extends Component {
  constructor(props) {
    super(props);
    this.state = {
      closedBriefs: [],
      isBookMeetingDialogOpen: false,
      loading: false,
      lookingForMatchBriefs: [],
      matchFoundBriefs: [],
      request: null,
    };
  }

  async componentDidMount() {
    await this.handleLoadPage();
  }

  loadBriefs = async () => {
    const {
      userContext: {
        user: { menteeId },
      },
    } = this.props;
    const briefs = await MenteeService.getBriefs({ menteeId });

    await Promise.all(
      briefs
        .filter(brief => !brief.closed)
        .map(brief =>
          BriefService.getBriefRequests(brief.id).then(requests => {
            brief.requests = requests; // eslint-disable-line no-param-reassign

            return Promise.all(
              requests
                .filter(request => request.mentorId)
                .map(request =>
                  MentorService.getMentor(request.mentorId).then(mentor => {
                    request.mentor = mentor;
                  })
                )
            );
          })
        )
    );
    this.setState({
      closedBriefs: briefs
        .filter(brief => brief.closed)
        .sort((briefA, briefB) => briefB.closeDate - briefA.closeDate),
      lookingForMatchBriefs: briefs.filter(brief => !brief.closed && brief.toMatch),
      matchFoundBriefs: briefs.filter(brief => !brief.closed && !brief.toMatch),
    });
  };

  handleBook = (request, brief) => {
    this.setState({ isBookMeetingDialogOpen: true, request, brief });
  };

  handleCloseBookMeetingDialog = () => {
    this.setState({
      isBookMeetingDialogOpen: false,
      request: null,
      brief: null,
    });
  };

  handleLoadPage = async () => {
    const { queryLoader, snackbar } = this.props;
    this.setState({ loading: true });
    queryLoader.showLoader();

    try {
      await this.loadBriefs();
    } catch (error) {
      console.error(error);
      snackbar.error('Something went wrong - sorry!');
    }

    this.setState({ loading: false });
    queryLoader.hideLoader();
  };

  render() {
    const {
      brief,
      closedBriefs,
      isBookMeetingDialogOpen,
      loading,
      lookingForMatchBriefs,
      matchFoundBriefs,
      request,
    } = this.state;

    if (loading) {
      return (
        <Container>
          <Hero />
          <PageLoadingSpinner />
        </Container>
      );
    }
    const hasBriefs =
      closedBriefs.length > 0 || lookingForMatchBriefs.length > 0 || matchFoundBriefs.length > 0;
    return (
      <Container>
        <Hero />
        <Layout width="780">
          <PageHeader>Open Challenges</PageHeader>
          <NewChallengePrompt onTriggerRefresh={this.handleLoadPage} />

          {hasBriefs ? (
            <>
              {[...matchFoundBriefs, ...lookingForMatchBriefs].map(briefItem => (
                <MatchCard
                  key={briefItem.id}
                  brief={briefItem}
                  onBook={this.handleBook}
                  onUpdate={this.handleLoadPage}
                />
              ))}

              {closedBriefs.length > 0 && <h2 className="closed-challenges">Closed Challenges</h2>}

              {closedBriefs.map(item => (
                <MatchCard key={item.id} brief={item} onUpdate={this.handleLoadPage} />
              ))}
            </>
          ) : (
            <NoChallengesCard />
          )}
        </Layout>

        {isBookMeetingDialogOpen && (
          <BookMeetingDialog
            challenge={brief}
            open={isBookMeetingDialogOpen}
            onClose={this.handleCloseBookMeetingDialog}
            mentorId={request.mentorId}
            requestId={request.id}
          />
        )}
      </Container>
    );
  }
}

export default withSnackbar(withUser(withQueryLoader(Challenges)));
