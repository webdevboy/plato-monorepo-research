import React, { Component } from 'react';

import BriefService from 'services/api/plato/brief/brief';
import Button from 'components/shared/Button';
import PropTypes from 'prop-types';
import SuccessDialog from 'components/shared/SuccessDialog';
import styled from 'styled-components';
import { trackChallengeShared } from 'utils/analytics';
import { withQueryLoader } from 'components/context/QueryLoaderContext';
import { withSnackbar } from 'components/context/SnackbarContext';
import { withUser } from 'components/context/UserContext';

const Container = styled.div`
  padding: 1.5rem 2rem;
  background: ${props => props.theme.palette.white};
  border-radius: 0.2rem;

  & h4 {
    margin-bottom: 1rem;
    color: ${props => props.theme.palette.cadetBlue};
  }

  & .describe {
    border: none;
    background: transparent;
    font-size: 2.4rem;
    color: ${props => props.theme.palette.cadetBlue};
    cursor: pointer;
  }

  & input,
  & textarea {
    border: none;
    width: 100%;

    &::placeholder {
      color: ${props => props.theme.palette.cadetBlue};
    }
  }

  & .one-liner {
    font-size: 2.4rem;
    margin-bottom: 2rem;

    &::placeholder {
      font-weight: 700;
    }
  }

  & .description {
    height: 9rem;
    margin-bottom: 4rem;
  }

  & .submit-container {
    display: flex;
    justify-content: space-between;

    p {
      width: 41.5rem;
      color: ${props => props.theme.palette.cadetBlue};
      line-height: 1.4rem;
    }
  }
`;

class NewChallengePrompt extends Component {
  constructor(props) {
    super(props);

    this.state = {
      open: false,
      oneLiner: '',
      description: '',
      saving: false,
      successDialogOpen: false,
    };
  }

  handleOpenPrompt = () => {
    this.setState({ open: true });
  };

  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  onSuccessDialogClose = () => {
    const { onTriggerRefresh } = this.props;
    this.setState({
      successDialogOpen: false,
      oneLiner: '',
      description: '',
      saving: false,
      open: false,
    });
    onTriggerRefresh();
  };

  sendChallenge = async () => {
    const {
      onTriggerRefresh,
      queryLoader,
      snackbar,
      userContext: { user },
    } = this.props;
    const { oneLiner, description } = this.state;
    this.setState({ saving: true });
    queryLoader.showLoader();

    try {
      const { result: brief } = await BriefService.createBrief({
        menteeId: user.menteeId,
        briefingOneLiner: oneLiner,
        briefingDescription: description,
        shareType: 'Spontaneous from web',
      });

      trackChallengeShared({
        menteeId: user.menteeId,
        briefId: brief.id,
        location: 'challenge',
        source: '', // TODO:  Get this value from the URL
        email: user.email,
        uuid: user.uuid,
      });

      this.setState({ successDialogOpen: true });
      onTriggerRefresh();
      localStorage.hasBriefs = true;
    } catch (error) {
      console.error(error);
      snackbar.error('Something went wrong - sorry!');
    }

    queryLoader.hideLoader();
    this.setState({
      saving: false,
    });
  };

  render() {
    const { className } = this.props;
    const { open, oneLiner, description, saving, successDialogOpen } = this.state;

    return (
      <Container className={className}>
        <h4>New Challenge</h4>
        {!open && (
          <button type="button" className="describe" onClick={this.handleOpenPrompt}>
            Describe your challenge in one line
          </button>
        )}
        {open && (
          <React.Fragment>
            <input
              type="text"
              className="one-liner"
              name="oneLiner"
              value={oneLiner}
              onChange={this.handleChange}
              placeholder="In one line, describe your challenge"
              autoFocus
            />

            <textarea
              className="description"
              name="description"
              value={description}
              onChange={this.handleChange}
              placeholder="In a few lines, add any relevant information for the mentor"
            />

            <div className="submit-container">
              <p>
                Once you submit your challenge, we will start searching for mentors that can help
                you with your challenge.
              </p>
              <Button
                variant="primary"
                disabled={saving || oneLiner.length === 0 || description.length === 0}
                onClick={this.sendChallenge}
              >
                Send Challenge
              </Button>
            </div>
          </React.Fragment>
        )}
        <SuccessDialog
          open={successDialogOpen}
          onClose={this.onSuccessDialogClose}
          title="Thanks for sharing!"
          description="We are working on identifying the perfect mentors for you. We take matching seriously, therefore it's done by a human. Expect to hear from us in the next 24h."
          buttonText="Back to my challenges"
        />
      </Container>
    );
  }
}

NewChallengePrompt.propTypes = {
  className: PropTypes.string,
  onTriggerRefresh: PropTypes.func.isRequired,
  queryLoader: PropTypes.object.isRequired,
  snackbar: PropTypes.object.isRequired,
  userContext: PropTypes.object.isRequired,
};

NewChallengePrompt.defaultProps = {
  className: null,
};

export default styled(withSnackbar(withUser(withQueryLoader(NewChallengePrompt))))``;
