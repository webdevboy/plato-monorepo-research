import React, { Component } from 'react';
import { trackChallengeClosed, trackChallengeReopened } from 'utils/analytics';

import BriefService from 'services/api/plato/brief/brief';
import Button from 'components/shared/Button';
import Card from 'components/shared/Card';
import CloseBriefDialog from './CloseBriefDialog';
import DaysUntil from 'components/shared/DaysUntil';
import EditChallengeDialog from 'components/shared/EditChallengeDialog';
import PropTypes from 'prop-types';
import RequestListItem from './RequestListItem';
import SuccessDialog from 'components/shared/SuccessDialog';
import styled from 'styled-components';
import { withQueryLoader } from 'components/context/QueryLoaderContext';
import { withSnackbar } from 'components/context/SnackbarContext';
import { withUser } from 'components/context/UserContext';

const StyledCard = styled(Card)`
  padding: 3rem;
  margin-bottom: 3rem;

  .header {
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin-bottom: 2rem;
  }

  .chip {
    display: flex;
    justify-content: center;
    align-items: center;
    border: 1px solid ${props => props.theme.palette.cadetBlue};
    color: ${props => props.theme.palette.cadetBlue};
    min-width: 13.5rem;
    height: 3rem;
    text-transform: uppercase;
    font-size: 1.2rem;
    font-weight: 700;
    padding: 0 1rem;

    &.primary-chip {
      border-color: ${props => props.theme.palette.cerulean};
      color: ${props => props.theme.palette.cerulean};
    }

    ${DaysUntil} {
      font-size: inherit;
      margin: 0;
    }
  }

  .one-liner {
    margin-bottom: 1rem;
  }

  .mentors-count {
    display: block;
    margin-top: 2rem;
    margin-bottom: 1rem;
    text-transform: uppercase;
    color: ${props => props.theme.palette.cadetBlue};
    font-weight: bold;
  }

  &.closed {
    .description,
    .one-liner {
      color: ${props => props.theme.palette.cadetBlue};
    }
  }
`;

const Actions = styled.div`
  display: flex;
  align-items: center;

  .separator {
    height: 2rem;
    width: 1px;
    background-color: ${props => props.theme.palette.geyser};
    margin: 0 1rem;
  }
`;

class MatchCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isEditDialogOpen: false,
      isCloseDialogOpen: false,
      isSuccessfullyClosedDialogOpen: false,
    };
  }

  getRequests() {
    const { brief } = this.props;
    if (brief.closed) {
      return [];
    }
    return brief.requests.filter(request => request.mentor);
  }

  handleOpenEditDialog = () => {
    this.setState({ isEditDialogOpen: true });
  };

  handleCloseEditDialog = () => {
    this.setState({ isEditDialogOpen: false });
  };

  handleOpenCloseDialog = () => {
    this.setState({ isCloseDialogOpen: true });
  };

  handleCloseCloseDialog = () => {
    this.setState({ isCloseDialogOpen: false });
  };

  handleBook = request => {
    const { brief, onBook } = this.props;
    onBook(request, brief);
  };

  handleCloseBrief = async () => {
    const {
      brief,
      queryLoader,
      userContext: { user },
    } = this.props;

    queryLoader.showLoader();
    this.setState({ isLoading: true });

    try {
      await BriefService.closeBrief(brief.id);

      trackChallengeClosed({
        briefId: brief.id,
        email: user.email,
        menteeId: user.menteeId,
        uuid: user.uuid,
      });
    } catch (error) {
      console.error(error);
      this.props.snackbar.error('Something went wrong - sorry!');
    }

    this.setState({
      isLoading: false,
      isCloseDialogOpen: false,
      isSuccessfullyClosedDialogOpen: true,
    });
    queryLoader.hideLoader();
  };

  handleEditDialogSave = async (oneLiner, description) => {
    const { brief, queryLoader, userContext, snackbar, onUpdate } = this.props;
    queryLoader.showLoader();
    this.setState({ isLoading: true });

    try {
      await BriefService.updateBrief(
        userContext.user.email,
        brief.id,
        oneLiner,
        description,
        brief.tags.map(({ id }) => id),
        brief.challenges.map(({ id }) => id),
        brief.shareType
      );

      onUpdate();
      snackbar.success('Challenge updated!');
    } catch (error) {
      console.error(error);
      snackbar.error('Something went wrong - sorry!');
    }

    this.setState({ isLoading: false, isEditDialogOpen: false });
    queryLoader.hideLoader();
  };

  handleRequestNewMatches = async () => {
    const {
      brief,
      queryLoader,
      snackbar,
      onUpdate,
      userContext: { user },
    } = this.props;

    this.setState({ isLoading: true });
    queryLoader.showLoader();

    try {
      await BriefService.reopenBrief(brief.id);

      trackChallengeReopened({
        briefId: brief.id,
        email: user.email,
        menteeId: user.menteeId,
        uuid: user.uuid,
      });

      onUpdate();
    } catch (error) {
      console.error(error);
      snackbar.error('Something went wrong - sorry!');
    }

    this.setState({ isLoading: false });
    queryLoader.hideLoader();
  };

  renderHeader() {
    const {
      brief: { closed, toMatch, closeDate },
    } = this.props;

    if (closed) {
      return (
        <>
          <div className="chip">
            Closed&nbsp;
            <DaysUntil timestamp={closeDate} />
          </div>
          <Button variant="primary-link" onClick={this.handleRequestNewMatches}>
            Request new matches
          </Button>
        </>
      );
    }

    if (!toMatch) {
      return <div className="chip primary-chip">Match found!</div>;
    }

    return (
      <>
        <div className="chip">Looking for a match</div>
        <Actions>
          <Button variant="primary-link" onClick={this.handleOpenEditDialog}>
            Edit
          </Button>
          <div className="separator" />
          <Button variant="secondary-link" onClick={this.handleOpenCloseDialog}>
            Close
          </Button>
        </Actions>
      </>
    );
  }

  renderModals() {
    const { brief, onUpdate } = this.props;
    const {
      isEditDialogOpen,
      isLoading,
      isCloseDialogOpen,
      isSuccessfullyClosedDialogOpen,
    } = this.state;

    if (brief.closed || !brief.toMatch) {
      return null;
    }

    return (
      <>
        <EditChallengeDialog
          briefId={brief.id}
          oneLiner={brief.briefingOneLiner}
          description={brief.briefingDescription}
          open={isEditDialogOpen}
          saving={isLoading}
          onSave={this.handleEditDialogSave}
          onClose={this.handleCloseEditDialog}
        />

        <CloseBriefDialog
          open={isCloseDialogOpen}
          onClose={this.handleCloseCloseDialog}
          onSave={this.handleCloseBrief}
          saving={isLoading}
        />

        <SuccessDialog
          open={isSuccessfullyClosedDialogOpen}
          onClose={onUpdate}
          title="Your challenge has been closed!"
          description={`You can still open it again later by clicking on "Ask for a new match"`}
          buttonText="Back to my challenges"
        />
      </>
    );
  }

  render() {
    const { brief } = this.props;
    const requests = this.getRequests();
    return (
      <StyledCard className={brief.closed && 'closed'}>
        <div className="header">{this.renderHeader()}</div>

        <h1 className="one-liner">{brief.briefingOneLiner}</h1>
        <p className="description">{brief.briefingDescription}</p>

        {!brief.closed && requests.length > 0 && (
          <>
            <sub className="mentors-count">
              {requests.length} Mentor
              {requests.length !== 1 && 's'} Found
            </sub>

            {requests.map(request => (
              <RequestListItem
                key={request.id}
                showBookButton
                onBook={this.handleBook}
                request={request}
              />
            ))}
          </>
        )}

        {this.renderModals()}
      </StyledCard>
    );
  }
}

MatchCard.propTypes = {
  brief: PropTypes.object.isRequired,
  onBook: PropTypes.func,
  onUpdate: PropTypes.func.isRequired,
};

MatchCard.defaultProps = {
  onBook: () => {},
};

export default withSnackbar(withUser(withQueryLoader(MatchCard)));
