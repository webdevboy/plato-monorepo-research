import {
  CALL_DONE_REQUEST_STATUS,
  CALL_SCHEDULED_REQUEST_STATUS,
  CALL_TO_RESCHEDULE_REQUEST_STATUS,
  SCHEDULING_IN_PROGRESS_REQUEST_STATUS,
} from 'constants/RequestStatuses';
import React, { Component } from 'react';
import { formatTimestampToDate, formatTimestampToString } from 'utils/time';

import Button from 'components/shared/Button';
import PersonInfo from 'components/shared/PersonInfo';
import PropTypes from 'prop-types';
import getProfilePictureThumbnail from 'utils/thumbnail';
import styled from 'styled-components';
import { withUser } from 'components/context/UserContext';

const StyledWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  flex-wrap: no-wrap;
  padding: 2rem;
  border: 1px solid ${props => props.theme.palette.geyser};

  ${PersonInfo} {
    max-width: 450px;
  }

  &:not(:last-child) {
    border-bottom: none;
  }

  .status {
    display: flex;
    flex-direction: column;
    align-items: flex-end;

    .date {
      color: ${props => props.theme.palette.osloGrey};
      font-weight: 400;
      font-size: 1.2rem;
    }

    .label {
      font-size: 1.4rem;
      text-transform: uppercase;
      color: ${props => props.theme.palette.cadetBlue};
      font-weight: bold;
    }
  }
`;

class RequestListItem extends Component {
  handleBook = () => {
    const { request, onBook } = this.props;
    onBook(request);
  };

  renderStatus() {
    const { request, userContext } = this.props;

    if (request.status === SCHEDULING_IN_PROGRESS_REQUEST_STATUS) {
      return (
        <Button variant="primary" onClick={this.handleBook}>
          Book now
        </Button>
      );
    }

    if (request.status === CALL_SCHEDULED_REQUEST_STATUS) {
      return (
        <div className="status">
          <p className="label">Session Scheduled</p>
          <p className="date">
            {formatTimestampToDate(request.callScheduledAt, userContext.timeZone())}
            ,&nbsp;
            {formatTimestampToString(request.callScheduledAt, userContext.timeZone())}
          </p>
        </div>
      );
    }

    if (request.status === CALL_DONE_REQUEST_STATUS) {
      return (
        <div className="status">
          <p className="label">Session done</p>
        </div>
      );
    }

    if (request.status === CALL_TO_RESCHEDULE_REQUEST_STATUS) {
      return (
        <div className="status">
          <p className="label">Rescheduling from mentor</p>
        </div>
      );
    }

    return null;
  }

  render() {
    const { className, request } = this.props;
    return (
      <StyledWrapper className={className}>
        <PersonInfo
          fullName={request.mentor.fullName}
          title={request.mentor.title}
          thumbnailUrl={getProfilePictureThumbnail(
            request.mentor.firstName,
            request.mentor.picture.thumbnailUrl
          )}
          companyName={request.mentor.companyName}
          linkUrl={`/mentors?mentor_id=${request.mentor.id}`}
        />

        {this.renderStatus()}
      </StyledWrapper>
    );
  }
}

RequestListItem.propTypes = {
  className: PropTypes.string,
  onBook: PropTypes.func,
  request: PropTypes.object.isRequired,
  userContext: PropTypes.object.isRequired,
};

RequestListItem.defaultProps = {
  className: null,
  onBook: () => {},
};

export default styled(withUser(RequestListItem))``;
