import React from 'react';
import PlatoDialog from 'components/shared/PlatoDialog';
import styled from 'styled-components';
import Button from 'components/shared/Button';

const Body = styled.div`
  padding: 3rem;
  width: 54rem;

  & h1 {
    margin-bottom: 1rem;
  }

  & .actions {
    display: flex;
    justify-content: flex-end;
    align-items: center;
    margin-top: 5rem;

    & ${Button} {
      margin-left: 2rem;
    }
  }
`;

const CloseBriefDialog = props => (
  <PlatoDialog open={props.open} onClose={props.onClose}>
    <Body>
      <h1>Are you sure you want to close your challenge?</h1>
      <p>Your challenge will be closed, but you can open it again later.</p>

      <div className="actions">
        <Button variant="secondary-link" onClick={props.onClose}>
          Cancel
        </Button>
        <Button variant="primary" onClick={props.onSave} disabled={props.saving}>
          Save
        </Button>
      </div>
    </Body>
  </PlatoDialog>
);

export default CloseBriefDialog;
