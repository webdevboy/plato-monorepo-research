import React, { Component } from 'react';

import Checkbox from 'components/shared/Checkbox';
import FilterList from 'components/shared/FilterList';
import mentorService from 'services/api/plato/mentor/mentor';
import { trackFilterUsed } from 'utils/analytics';
import { withSnackbar } from 'components/context/SnackbarContext';
import { withUser } from 'components/context/UserContext';

class IndustryFilterList extends Component {
  state = {
    loading: true,
    industries: [],
  };

  async componentDidMount() {
    const { snackbar } = this.props;
    try {
      const industries = await mentorService.getTags('industry');
      this.setState({
        industries,
      });
    } catch (error) {
      console.error(error);
      snackbar.error('Something went wrong - sorry!');
    }
    this.setState({ loading: false });
  }

  handleChange = (industryId, checked) => {
    const {
      onChange,
      userContext: {
        user: { uuid, email, menteeId },
      },
    } = this.props;
    const { industries } = this.state;
    onChange(industryId, checked);

    const selectedIndustry = industries.find(industry => industry.id === industryId);
    this.setState({
      industries: industries.map(industry =>
        industry.id === industryId ? Object.assign(industry, { checked }) : industry
      ),
    });

    trackFilterUsed({
      menteeId,
      location: 'mentors',
      filterName: selectedIndustry.name,
      filterValue: checked,
      filterType: 'industry',
      uuid,
      email,
    });
  };

  render() {
    const { loading, industries } = this.state;

    return (
      <FilterList title="Filter by Industry" loading={loading}>
        {industries.map(industry => (
          <Checkbox
            key={industry.id}
            name={industry.id}
            color="primary"
            label={industry.name}
            checked={industry.checked}
            onChange={this.handleChange}
          />
        ))}
      </FilterList>
    );
  }
}

export default withSnackbar(withUser(IndustryFilterList));
