import React, { Component } from 'react';

import Checkbox from 'components/shared/Checkbox';
import FilterList from 'components/shared/FilterList';
import TagService from 'services/api/plato/tag/tag';
import { trackFilterUsed } from 'utils/analytics';
import { withSnackbar } from 'components/context/SnackbarContext';
import { withUser } from 'components/context/UserContext';

class TopicFilterList extends Component {
  state = {
    loading: true,
    topics: [],
  };

  async componentDidMount() {
    const { snackbar } = this.props;
    try {
      const topics = await TagService.getTopicsForFilter();
      this.setState({
        topics: topics
          .filter(topic => topic.mentorCount > 0)
          .sort((topicA, topicB) => topicB.mentorCount - topicA.mentorCount),
      });
    } catch (error) {
      console.error(error);
      snackbar.error('Something went wrong - sorry!');
    }
    this.setState({ loading: false });
  }

  handleChange = (topicId, checked) => {
    const {
      onChange,
      userContext: {
        user: { uuid, email, menteeId },
      },
    } = this.props;
    const { topics } = this.state;
    onChange(topicId, checked);

    const selectedTopic = topics.find(topic => topic.id === topicId);
    this.setState({
      topics: topics.map(topic =>
        topic.id === topicId ? Object.assign(topic, { checked }) : topic
      ),
    });

    trackFilterUsed({
      menteeId,
      location: 'mentors',
      filterName: selectedTopic.name,
      filterValue: checked,
      filterType: 'topic',
      uuid,
      email,
    });
  };

  render() {
    const { loading, topics } = this.state;

    return (
      <FilterList title="Filter by Topic" loading={loading}>
        {topics.map(topic => (
          <Checkbox
            key={topic.id}
            name={topic.id}
            color="primary"
            label={`${topic.name} (${topic.mentorCount})`}
            checked={topic.checked}
            onChange={this.handleChange}
          />
        ))}
      </FilterList>
    );
  }
}

export default withSnackbar(withUser(TopicFilterList));
