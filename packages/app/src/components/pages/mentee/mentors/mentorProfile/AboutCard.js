import React, { Component } from 'react';

import Button from 'components/shared/Button';
import Card from 'components/shared/Card';
import DateWithIcon from 'components/shared/DateWithIcon';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import Text from 'components/shared/Text';
import TimeWithIcon from 'components/shared/TimeWithIcon';
import { formatTimestampToString } from 'utils/time';
import moment from 'moment';
import styled from 'styled-components';
import { withUser } from 'components/context/UserContext';

const CustomCard = styled(Card)`
  display: flex;
  margin-top: 4rem;
`;

const Body = styled.div`
  position: relative;
  display: flex;
  width: 60%;
  font-size: 1.6rem;
  border-right: 1px solid ${({ theme }) => theme.palette.geyser};
`;

const Description = styled.div`
  padding: 3rem;
`;

const ActionCard = styled.div`
  background-color: ${props => props.theme.palette.polar};
  width: 40%;
  padding: 3rem;

  h1 {
    margin-bottom: 1rem;
  }

  ${Button} {
    width: 100%;
    margin-top: 20px;
  }

  &.primary,
  &.primary svg {
    background-color: ${props => props.theme.palette.cerulean};
    color: ${props => props.theme.palette.white};
  }

  .secondary-button {
    border: none;
    padding: 0;
    color: ${props => props.theme.palette.blueLagoon};
  }

  .primary-button {
    border-color: ${props => props.theme.palette.white};
    color: ${props => props.theme.palette.white};
  }
`;

class AboutCard extends Component {
  renderBooking() {
    const {
      firstName,
      isRelationship,
      nextAvailability,
      nextSession,
      onIgnore,
      onOpenBookDialog,
      onOpenRelationshipDialog,
      isRelationshipPromptShown,
      userContext,
    } = this.props;

    if (isRelationship && nextSession) {
      return (
        <ActionCard className="primary">
          <h1>{firstName} is your long-term mentor!</h1>
          <p>Here&apos;s is your next session:</p>
          <DateWithIcon date={nextSession.callScheduledAt} />
          <TimeWithIcon className="label-info" time={nextSession.callScheduledAt} />
          <Link to="/sessions">
            <Button variant="secondary" className="primary-button">
              Go to my sessions
            </Button>
          </Link>
        </ActionCard>
      );
    }

    if (isRelationshipPromptShown) {
      return (
        <ActionCard className="primary">
          <h1>Book a long-term mentorship!</h1>
          <p>
            Hey, it seems you already had
            <br />a call with {firstName}
          </p>
          <Button variant="secondary" className="primary-button" onClick={onOpenRelationshipDialog}>
            Learn more
          </Button>
          <Button variant="secondary" className="secondary-button" onClick={onIgnore}>
            Not right now
          </Button>
        </ActionCard>
      );
    }

    if (nextAvailability) {
      // DbMentor.slot_time is obsolete, we calculate it
      // using the next nextAvailability
      return (
        <ActionCard>
          <h1>
            Hey, I&apos;m available every&nbsp;
            {moment(nextAvailability.startTime)
              .tz(userContext.timeZone())
              .format('dddd')}
            &nbsp;at&nbsp;
            {formatTimestampToString(nextAvailability.startTime, userContext.timeZone())}
          </h1>
          <p>Next time slot available</p>
          <DateWithIcon date={nextAvailability.startTime} />
          <TimeWithIcon time={nextAvailability.startTime} />
          <Button variant="primary" onClick={onOpenBookDialog}>
            Book now
          </Button>
        </ActionCard>
      );
    }

    return (
      <ActionCard>
        <Text type="heading2">Sorry, I&apos;m currently unavailable for 1-on-1 sessions.</Text>
      </ActionCard>
    );
  }

  render() {
    const { text } = this.props;
    return (
      <CustomCard>
        <Body>
          <Description>{text}</Description>
        </Body>
        {this.renderBooking()}
      </CustomCard>
    );
  }
}

AboutCard.propTypes = {
  firstName: PropTypes.string.isRequired,
  isRelationship: PropTypes.bool.isRequired,
  nextAvailability: PropTypes.object,
  nextSession: PropTypes.bool, // eslint-disable-line react/boolean-prop-naming
  onIgnore: PropTypes.func.isRequired,
  onOpenBookDialog: PropTypes.func.isRequired,
  onOpenRelationshipDialog: PropTypes.func.isRequired,
  isRelationshipPromptShown: PropTypes.bool.isRequired,
  text: PropTypes.string,
  userContext: PropTypes.object.isRequired,
};

AboutCard.defaultProps = {
  nextAvailability: null,
  nextSession: null,
  text: '',
};

export default withUser(AboutCard);
