import React, { Component } from 'react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import TagList from 'components/shared/TagList';
import styled from 'styled-components';

const StyledSideProfile = styled.div`
  display: flex;
  min-width: 26rem;
  max-width: 26rem;
  margin-right: 2rem;
  flex-direction: column;

  .small-header {
    font-size: 1.4rem;
    line-height: 1.43;
  }

  .large-header {
    font-weight: bold;
  }

  .text-info {
    margin: 0 0 1rem;
  }

  ${TagList} {
    margin: 1rem 0;
  }
`;

const ProfilePic = styled.img`
  border: 0.5rem solid white;
  width: 19rem;
  height: 19rem;
  box-sizing: border-box;
  border-radius: 10rem;
`;

const MentorName = styled.h1`
  margin: 2rem 0 1rem;
`;

const MentorTitle = styled.p`
  margin: 0 0 1rem;
`;

const LinkedIn = styled.a`
  color: #1bbfe5;
  font-size: 1.4rem;
`;

const Separator = styled.div`
  height: 2px;
  width: 5rem;
  background: #1bbfe5;
  margin: 2rem 0;
`;

class SideProfile extends Component {
  render() {
    const {
      profile: {
        picture,
        fullName,
        title,
        companyName,
        linkedin,
        managementExperience,
        hierarchy,
        sizeOfTeam,
        expertise,
        domainExpertise,
        specialExpertise,
        industries,
      },
    } = this.props;
    return (
      <StyledSideProfile>
        <ProfilePic src={picture.originalUrl} />
        <MentorName>{fullName}</MentorName>
        <MentorTitle>
          {title} at <b>{companyName}</b>
        </MentorTitle>
        <LinkedIn href={linkedin} target="_blank">
          <FontAwesomeIcon icon={['fab', 'linkedin']} color="#0073b1" />
          &nbsp; LINKEDIN PROFILE
        </LinkedIn>

        <Separator />

        <h3 className="small-header">SENIORITY</h3>
        <p className="text-info">{managementExperience} Years</p>

        <h3 className="small-header">HIERARCHY</h3>
        <p className="text-info">{hierarchy}</p>

        <h3 className="small-header">TEAM SIZE</h3>
        <p className="text-info">{sizeOfTeam > 0 ? `${sizeOfTeam} people` : '0'}</p>

        <Separator />

        {expertise.length > 0 && (
          <>
            <p className="large-header">Management Expertise</p>
            <TagList tags={expertise.map(({ name }) => name)} />
          </>
        )}

        {domainExpertise.length > 0 && (
          <>
            <p className="large-header">Domain Expertise</p>
            <TagList tags={domainExpertise.map(({ name }) => name)} />
          </>
        )}

        {specialExpertise.length > 0 && (
          <>
            <p className="large-header">Other Expertise</p>
            <TagList tags={specialExpertise.map(({ name }) => name)} />
          </>
        )}

        {industries.length > 0 && (
          <>
            <p className="large-header">Industries</p>
            <TagList tags={industries.map(industry => industry.name)} />
          </>
        )}
      </StyledSideProfile>
    );
  }
}

export default SideProfile;
