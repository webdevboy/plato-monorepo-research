import React from 'react';
import SessionCard from 'components/pages/mentee/ama/SessionCard';
import Text from 'components/shared/Text';
import styled from 'styled-components';

const SessionsContainer = styled.div`
  margin-bottom: 1.9rem;
`;

const Padding = styled.div`
  padding: 1.9rem 0;
`;

const UpcomingAmas = ({ data, allTopics, openSignUpDialog }) => {
  if (data.length === 0) {
    return <div />;
  }
  return (
    <React.Fragment>
      <Text type="label" margin="4rem 0 0 0">
        UPCOMING AMA SESSIONS
      </Text>

      <SessionsContainer>
        {data.length > 0 &&
          data.map(ama => {
            const topics = ama.tagIds.map(
              tagId => allTopics.find(topic => topic.id === tagId).name
            );

            return (
              <Padding key={ama.id}>
                <SessionCard
                  attendances={ama._attendances}
                  limitAttendances={ama.limitAttendances}
                  callScheduledAt={ama.callScheduledAt}
                  storyId={ama.storyId}
                  storyName={ama.storyName}
                  mentorTitle={ama._mentor.title}
                  mentorCompanyName={ama._mentor.companyName}
                  mentorId={ama.mentorId}
                  mentorFullName={ama.mentorFullName}
                  mentorPictureUrl={ama.mentorPicture.thumbnailUrl}
                  topics={topics}
                  onSignUpClick={() => openSignUpDialog(ama)}
                />
              </Padding>
            );
          })}
      </SessionsContainer>
    </React.Fragment>
  );
};

export default UpcomingAmas;
