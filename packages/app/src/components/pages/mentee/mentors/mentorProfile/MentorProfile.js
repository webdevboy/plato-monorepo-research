import { CALL_DONE_REQUEST_STATUS, CALL_SCHEDULED_REQUEST_STATUS } from 'constants/RequestStatuses';
import React, { Component } from 'react';

import AboutCard from './AboutCard';
import AmaService from 'components/pages/mentee/ama/amaService';
import BookMeetingDialog from 'components/shared/BookMeetingDialog';
import RelationshipDialog from 'components/shared/RelationshipDialog';
import Hero from 'components/shared/Hero';
import IconLabel from 'components/shared/IconLabel';
import Layout from 'components/shared/Layout';
import { Link } from 'react-router-dom';
import MentorService from 'services/api/plato/mentor/mentor';
import PageLoadingSpinner from 'components/shared/PageLoadingSpinner';
import RequestService from 'services/api/plato/request/request';
import SideProfile from './SideProfile';
import SignUpDialog from 'components/pages/mentee/ama/SignUpDialog';
import StoryCard from 'components/shared/StoryCard';
import TagService from 'services/api/plato/tag/tag';
import Text from 'components/shared/Text';
import UpcomingAmas from './UpcomingAmas';
import ViewService from 'services/api/plato/view/view';
import { isRelationshipAvailable } from 'utils/relationship';
import queryString from 'query-string';
import styled from 'styled-components';
import { withSnackbar } from 'components/context/SnackbarContext';
import { withUser } from 'components/context/UserContext';

const ProfileWrapper = styled.div`
  display: flex;
  margin-top: 7rem;
`;

const StyledLayout = styled(Layout)`
  .back-button {
    margin-top: 3rem;
    font-size: 14px;
    font-weight: bold;
    text-transform: uppercase;
    color: ${({ theme }) => theme.palette.white};
  }
`;

const StyledStoryCard = styled(StoryCard)`
  margin-top: 2rem;
`;

class MentorProfile extends Component {
  constructor(props) {
    super(props);
    const { location } = this.props;
    const queryParams = queryString.parse(location.search);

    this.state = {
      allTopics: null,
      bookMeetingDialogIsOpen: false,
      relationshipDialogIsOpen: false,
      loading: true,
      mentor: null,
      mentorAmas: null,
      mentorAvailabilities: [],
      mentorStories: null,
      requestId: queryParams.booking === 'yes' ? +queryParams.request_id : undefined,
      signUpAmaDialog: null,
      signUpDialogIsOpen: null,
      showRelationshipBannerPrompt: false,
      isRelationshipPossible: false,
      hadACall: false,
      relationshipId: null,
    };
  }

  async componentDidMount() {
    const {
      location,
      snackbar,
      history,
      match: {
        params: { mentorId },
      },
    } = this.props;
    const queryParams = queryString.parse(location.search);

    if (queryParams.booking === 'yes') {
      this.handleOpenBookMeetingDialog();
    }

    if (queryParams.relationship_id) {
      const relationshipId = queryParams.relationship_id;

      this.setState(
        {
          relationshipId,
        },
        () => this.handleOpenRelationshipDialog()
      );
      delete queryParams.relationship_id;
      history.replace(`/mentors/${mentorId}?${queryString.stringify(queryParams)}`);
    }

    this.setState({ loading: true });
    try {
      await this.getPage();

      this.createView();
    } catch (error) {
      console.error(error);
      snackbar.error('Something went wrong - sorry!');
    }

    this.setState({ loading: false });
  }

  getPage = async () => {
    const {
      match: {
        params: { mentorId },
      },
      userContext: { mentee },
    } = this.props;
    const [mentor, mentorAvailabilities, stories, topics, amas, requests] = await Promise.all([
      MentorService.getMentor(mentorId),
      MentorService.getAvailabilities(mentorId),
      MentorService.getStoriesByMentor(mentorId),
      TagService.getTopicsForFilter(),
      MentorService.getAmasByMentor({ mentorId, scheduled: true }),
      // TODO: Implement lookup API to check whether a mentor and a mentee already met
      // rather than getting all the requests and filter them
      RequestService.getMentorMenteeRequests({ mentorId, menteeId: mentee.id }),
    ]);

    // Enrich object in input with the list of attendance and the mentor information
    await AmaService.getAdditionalAmaInfo(amas.result);
    const mentorAmas = amas.result
      .filter(ama => ama.callScheduledAt > Date.now())
      .sort((amaA, amaB) => amaA.callScheduledAt - amaB.callScheduledAt);

    const { result } = requests;
    const hasResults = result && result.length > 0;

    const nextSession =
      hasResults && result.find(request => request.status === CALL_SCHEDULED_REQUEST_STATUS);

    const isRelationshipPossible = isRelationshipAvailable(mentor, mentee, true);
    const hadACall = requests.result.some(({ status }) => status === CALL_DONE_REQUEST_STATUS);

    this.setState({
      allTopics: topics,
      hadACall,
      isRelationshipPossible,
      showRelationshipBannerPrompt: isRelationshipPossible && hadACall,
      mentor,
      mentorAmas,
      mentorAvailabilities,
      mentorStories: stories.result,
      nextSession,
    });
  };

  createView = () => {
    const {
      userContext: {
        user: { menteeId },
      },
    } = this.props;
    const { mentor } = this.state;
    ViewService.create({
      viewedId: mentor.id,
      viewerId: menteeId,
      type: 'Views::MentorByMentee',
    });
  };

  handleOpenBookMeetingDialog = () => {
    const { isRelationshipPossible, hadACall } = this.state;
    if (isRelationshipPossible && !hadACall) {
      this.setState({
        relationshipDialogIsOpen: true,
      });
    } else {
      this.setState({
        bookMeetingDialogIsOpen: true,
      });
    }
  };

  closeBookMeetingDialog = () => {
    const {
      history,
      match: {
        params: { mentorId },
      },
    } = this.props;
    this.setState({
      bookMeetingDialogIsOpen: false,
    });
    history.push(`/mentors/${mentorId}`);
  };

  handleOpenSignUpDialog = ama => {
    this.setState({
      signUpDialogIsOpen: true,
      signUpAmaDialog: ama,
    });
  };

  handleCloseSignUpDialog = () => {
    this.setState({
      signUpDialogIsOpen: false,
    });
  };

  handleIgnoreRelationshipBanner = () => {
    this.setState({ showRelationshipBannerPrompt: false });
  };

  handleOpenRelationshipDialog = () => {
    this.setState({ relationshipDialogIsOpen: true });
  };

  handleCloseRelationshipDialog = () => {
    this.setState({
      relationshipDialogIsOpen: false,
      showRelationshipBannerPrompt: false,
    });
  };

  handleIgnoreRelationshipDialog = () => {
    this.setState({
      relationshipDialogIsOpen: false,
      bookMeetingDialogIsOpen: true,
    });
  };

  render() {
    const {
      userContext: { mentee },
    } = this.props;
    const {
      allTopics,
      loading,
      bookMeetingDialogIsOpen,
      relationshipDialogIsOpen,
      mentor,
      mentorAmas,
      mentorAvailabilities,
      mentorStories,
      nextSession,
      requestId,
      showRelationshipBannerPrompt,
      hadACall,
      isRelationshipPossible,
      signUpAmaDialog,
      signUpDialogIsOpen,
      relationshipId,
    } = this.state;

    if (loading) {
      return (
        <>
          <Hero />
          <PageLoadingSpinner />
        </>
      );
    }

    const isRelationship = mentee.relationships.some(
      relationship => relationship.status === 'On-going' && relationship.mentorId === mentor.id
    );

    return (
      <>
        <Hero height="210" />
        <StyledLayout width={1025}>
          <Link to="/mentors" className="back-button">
            <IconLabel icon="arrow-left">Back</IconLabel>
          </Link>
          <ProfileWrapper>
            <SideProfile profile={mentor} />
            <div>
              <AboutCard
                firstName={mentor.firstName}
                isRelationship={isRelationship}
                isRelationshipPromptShown={showRelationshipBannerPrompt}
                nextAvailability={mentorAvailabilities[0]}
                nextSession={nextSession}
                onIgnore={this.handleIgnoreRelationshipBanner}
                onOpenBookDialog={this.handleOpenBookMeetingDialog}
                onOpenRelationshipDialog={this.handleOpenRelationshipDialog}
                text={mentor.aboutMyself}
              />
              <UpcomingAmas
                data={mentorAmas}
                allTopics={allTopics}
                openSignUpDialog={this.handleOpenSignUpDialog}
                closeSignUpDialog={this.handleCloseSignUpDialog}
              />
              {mentorStories.length > 0 ? (
                <Text type="label" margin="4rem 0 0 0" uppercase>
                  Read {mentor.firstName}
                  &apos;s Stories
                </Text>
              ) : null}
              {mentorStories.map(story => (
                <StyledStoryCard key={story.id} story={story} />
              ))}
            </div>
          </ProfileWrapper>
        </StyledLayout>

        <BookMeetingDialog
          open={bookMeetingDialogIsOpen}
          onClose={this.closeBookMeetingDialog}
          mentorId={mentor.id}
          requestId={requestId}
        />

        {signUpAmaDialog && (
          <SignUpDialog
            open={signUpDialogIsOpen}
            onClose={this.handleCloseSignUpDialog}
            storyName={signUpAmaDialog.storyName}
            mentorFullName={signUpAmaDialog.mentorFullName}
            mentorTitle={signUpAmaDialog._mentor.title}
            mentorCompanyName={signUpAmaDialog._mentor.companyName}
            limitAttendances={signUpAmaDialog.limitAttendances}
            attendances={signUpAmaDialog._attendances}
            callScheduledAt={signUpAmaDialog.callScheduledAt}
            amaId={signUpAmaDialog.id}
            onConfirm={this.getPage}
          />
        )}

        {(isRelationshipPossible || relationshipId) && (
          <RelationshipDialog
            open={relationshipDialogIsOpen}
            onClose={this.handleCloseRelationshipDialog}
            onBookOneOff={this.handleIgnoreRelationshipDialog}
            mentorId={mentor.id}
            mentorFirstName={mentor.firstName}
            mentorFullName={mentor.fullName}
            mentorPictureUrl={mentor.picture.thumbnailUrl}
            hadACall={hadACall}
            relationshipId={relationshipId}
          />
        )}
      </>
    );
  }
}

export default withSnackbar(withUser(MentorProfile));
