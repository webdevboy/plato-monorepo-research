import React, { Component, Fragment } from 'react';
import {
  TIME_SLOT_10AM_12PM,
  TIME_SLOT_12AM_6AM,
  TIME_SLOT_12PM_2PM,
  TIME_SLOT_2PM_4PM,
  TIME_SLOT_4PM_6PM,
  TIME_SLOT_6AM_8AM,
  TIME_SLOT_6PM_12AM,
  TIME_SLOT_8AM_10AM,
  TIME_SLOT_ANY,
} from 'constants/TimeSlots';

import AvailabilityFilterList from './AvailabilityFilterList';
import CompanyTypeFilterList from './CompanyTypeFilterList';
import EmptyStateCard from 'components/shared/EmptyStateCard';
import IndustryFilterList from './IndustryFilterList';
import Layout from 'components/shared/Layout';
import MentorGrid from './MentorGrid';
import MentorService from 'services/api/plato/mentor/mentor';
import RoleFilterList from './RoleFilterList';
import SearchInput from 'components/shared/SearchInput';
import Tag from 'components/shared/Tag';
import TopicFilterList from './TopicFilterList';
import { getTimeSlotBounds } from 'utils/time';
import moment from 'moment-timezone';
import queryString from 'query-string';
import styled from 'styled-components';
import { withQueryLoader } from 'components/context/QueryLoaderContext';
import { withSnackbar } from 'components/context/SnackbarContext';
import { withUser } from 'components/context/UserContext';

const StyledLayout = styled(Layout)`
  margin-top: 5rem;

  .row {
    display: flex;
  }

  h3 {
    margin-bottom: 3rem;
  }

  .grid {
    width: 100%;
  }
`;

const FilterColumn = styled.div`
  min-width: 26.5rem;
  max-width: 26.5rem;
  margin-right: 2rem;
  margin-top: 5rem;

  > * {
    margin-bottom: 2rem;
  }

  .tag {
    margin: 0 0.5rem 0.5rem 0;
  }

  .days-filter,
  .times-filter {
    margin-bottom: 2rem;

    h4 {
      margin-bottom: 1rem;
    }
  }
`;

const selectableDays = ['Any day', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'];

const selectableTimeSlots = [
  TIME_SLOT_ANY,
  TIME_SLOT_12AM_6AM,
  TIME_SLOT_6AM_8AM,
  TIME_SLOT_8AM_10AM,
  TIME_SLOT_10AM_12PM,
  TIME_SLOT_12PM_2PM,
  TIME_SLOT_2PM_4PM,
  TIME_SLOT_4PM_6PM,
  TIME_SLOT_6PM_12AM,
];

const numberOfMentorByPage = 12;

class Mentors extends Component {
  constructor(props) {
    super(props);
    const { location, history } = props;
    const queryParams = queryString.parse(location.search);
    if (queryParams.mentor_id) {
      const mentorId = queryParams.mentor_id;
      delete queryParams.mentor_id;
      history.replace(`/mentors/${mentorId}?${queryString.stringify(queryParams)}`);
    }

    this.state = {
      availableBefore: '',
      companyTypes: [],
      faceIds: [],
      faces: [],
      isLoading: false,
      mentors: [],
      page: 0,
      selectedIndustries: [],
      searchQuery: '',
      selectedDayIndexes: [],
      selectedTimeSlot: TIME_SLOT_ANY,
      tagIds: [],
      totalItems: 0,
      totalPages: 0,
    };
  }

  componentDidMount() {
    this.setState({ isLoading: true });
  }

  handleLoadMore = pageIndex => {
    this.fetchMentors(pageIndex);
  };

  handleRoleFilterChange = (id, checked) => {
    this.setState(
      prevState => ({
        faceIds: checked
          ? prevState.faceIds.concat(id)
          : prevState.faceIds.filter(faceId => faceId !== id),
      }),
      () => {
        if (!this.pmRoleSelected()) {
          this.setState({ selectedIndustries: [] }, this.fetchMentors);
        } else {
          this.fetchMentors();
        }
      }
    );
  };

  pmRoleSelected = () => {
    const { faces, faceIds } = this.state;
    const pm = faces && faces.find(face => face.name === 'Product Manager');
    return faceIds.filter(faceId => faceId === pm.id).length > 0;
  };

  handleRoleFilterLoad = roleFilterOptions => {
    this.setState(
      {
        faces: roleFilterOptions,
        faceIds: roleFilterOptions.filter(option => option.checked).map(option => option.id),
      },
      this.fetchMentors
    );
  };

  handleAvailabilityFilterChange = weeksOut => {
    this.setState(
      {
        availableBefore: weeksOut
          ? moment()
              .add(weeksOut, 'weeks')
              .toISOString()
          : undefined,
      },
      this.fetchMentors
    );
  };

  handleCompanyTypeFilterChange = (companyType, checked) => {
    this.setState(
      prevState => ({
        companyTypes: checked
          ? prevState.companyTypes.concat(companyType)
          : prevState.companyTypes.filter(stateCompanyType => stateCompanyType !== companyType),
      }),
      this.fetchMentors
    );
  };

  handleTopicFilterChange = (id, checked) => {
    this.setState(
      prevState => ({
        tagIds: checked
          ? prevState.tagIds.concat(id)
          : prevState.tagIds.filter(tagId => tagId !== id),
      }),
      this.fetchMentors
    );
  };

  handleIndustryChange = (industry, checked) => {
    this.setState(
      prevState => ({
        selectedIndustries: checked
          ? prevState.selectedIndustries.concat(industry)
          : prevState.selectedIndustries.filter(stateIndustry => stateIndustry !== industry),
      }),
      this.fetchMentors
    );
  };

  handleQueryChange = event => {
    const {
      target: { value },
    } = event;
    this.setState({ searchQuery: value }, this.fetchMentors);
  };

  handleSelectDay = dayIndex => {
    const { selectedDayIndexes } = this.state;
    // Handle the 'Any day' option
    if (dayIndex === 0) {
      this.setState({ selectedDayIndexes: [] }, this.fetchMentors);
      return;
    }

    this.setState(
      {
        selectedDayIndexes: selectedDayIndexes.includes(dayIndex)
          ? selectedDayIndexes.filter(selectedDayIndex => selectedDayIndex !== dayIndex)
          : [dayIndex, ...selectedDayIndexes],
      },
      this.fetchMentors
    );
  };

  handleSelectTime = selectedTimeSlot => {
    this.setState({ selectedTimeSlot }, this.fetchMentors);
  };

  async fetchMentors(page = 0) {
    const { snackbar, queryLoader, userContext } = this.props;
    const {
      availableBefore,
      companyTypes,
      faceIds,
      searchQuery,
      selectedDayIndexes,
      selectedTimeSlot,
      selectedIndustries,
      tagIds,
    } = this.state;
    const { lower: availableAfterHour, higher: availableBeforeHour } = getTimeSlotBounds(
      selectedTimeSlot,
      userContext.timeZone()
    );
    queryLoader.showLoader();
    this.setState({ isLoading: true });
    try {
      const { result: mentors, totalPages, totalItems } = await MentorService.searchForMentee({
        availableAfter: moment().toISOString(),
        availableAfterHour,
        availableBefore,
        availableBeforeHour,
        availableOnDays: selectedDayIndexes.join(','),
        companyTypes: companyTypes.join(','),
        faceIds: faceIds.join(','),
        industryIds: selectedIndustries.join(','),
        page,
        query: searchQuery,
        size: numberOfMentorByPage,
        tagIds: tagIds.join(','),
      });

      this.setState(prevState => ({
        mentors: page === 0 ? mentors : [...prevState.mentors, ...mentors],
        totalPages,
        totalItems,
        page,
      }));
    } catch (error) {
      console.error(error);
      snackbar.error('Something went wrong - sorry!');
    }
    this.setState({ isLoading: false });
    queryLoader.hideLoader();
  }

  render() {
    const {
      userContext,
      userContext: { mentee },
    } = this.props;
    const {
      isLoading,
      mentors,
      page,
      searchQuery,
      selectedDayIndexes,
      selectedTimeSlot,
      totalItems,
      totalPages,
    } = this.state;

    const relationship = mentee.relationships.find(
      menteeRelationship => menteeRelationship.status === 'On-going'
    );
    return (
      <StyledLayout width="1120">
        <div className="row">
          <FilterColumn>
            <SearchInput
              placeholder="Search"
              onChange={this.handleQueryChange}
              value={searchQuery}
            />
            <div className="days-filter">
              <h4>Days available ({moment.tz(userContext.timeZone()).zoneName()})</h4>
              {selectableDays.map((day, index) => (
                <Fragment key={index}>
                  <Tag
                    className="tag"
                    value={index}
                    text={day}
                    size="small"
                    isSelectable
                    isSelected={
                      index === 0
                        ? selectedDayIndexes.length === 0
                        : selectedDayIndexes.includes(index)
                    }
                    variant="light"
                    onClick={this.handleSelectDay}
                  />
                  {index === 0 && <br />}
                </Fragment>
              ))}
            </div>
            <div className="times-filter">
              <h4>Times available ({moment.tz(userContext.timeZone()).zoneName()})</h4>
              {selectableTimeSlots.map((timeSlot, index) => (
                <Fragment key={index}>
                  <Tag
                    className="tag"
                    value={timeSlot}
                    text={timeSlot}
                    size="small"
                    isSelectable
                    isSelected={selectedTimeSlot === timeSlot}
                    variant="light"
                    onClick={this.handleSelectTime}
                  />
                  {index === 0 && <br />}
                </Fragment>
              ))}
            </div>
            <RoleFilterList
              onChange={this.handleRoleFilterChange}
              onLoad={this.handleRoleFilterLoad}
            />
            <AvailabilityFilterList onChange={this.handleAvailabilityFilterChange} />
            {this.pmRoleSelected() && <IndustryFilterList onChange={this.handleIndustryChange} />}

            <CompanyTypeFilterList onChange={this.handleCompanyTypeFilterChange} />
            <TopicFilterList onChange={this.handleTopicFilterChange} />
          </FilterColumn>
          <div className="grid">
            <h3>{totalItems} Mentors</h3>
            {totalItems > 0 ? (
              <MentorGrid
                relationshipMentorId={relationship && relationship.mentorId}
                mentors={mentors}
                onLoadMore={this.handleLoadMore}
                page={page}
                totalPages={totalPages}
              />
            ) : (
              !isLoading && <EmptyStateCard title="We didn't find any matches." />
            )}
          </div>
        </div>
      </StyledLayout>
    );
  }
}

export default withUser(withSnackbar(withQueryLoader(Mentors)));
