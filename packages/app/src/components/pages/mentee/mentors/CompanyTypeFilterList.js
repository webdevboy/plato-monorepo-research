import React, { Component } from 'react';

import CheckboxFilterList from 'components/shared/CheckboxFilterList';
import MentorService from 'services/api/plato/mentor/mentor';
import { trackFilterUsed } from 'utils/analytics';
import { withSnackbar } from 'components/context/SnackbarContext';
import { withUser } from 'components/context/UserContext';

class CompanyTypeFilterList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      companyTypes: [],
    };
  }

  async componentDidMount() {
    const { snackbar } = this.props;

    try {
      const companyTypes = await MentorService.getCompanyTypes();

      this.setState({
        companyTypes: companyTypes.map(companyType => ({
          id: companyType,
          name: this.getCompanyTypeDisplayValue(companyType),
          defaultChecked: false,
        })),
      });
    } catch (error) {
      console.error(error);
      snackbar.error('Something went wrong - sorry!');
    }

    this.setState({ loading: false });
  }

  getCompanyTypeDisplayValue = companyType => {
    return companyType
      .match(/^[^(]+/)
      .toString()
      .trim()
      .split(' ')
      .map(word => `${word[0].toUpperCase()}${word.slice(1)}`)
      .join(' ');
  };

  handleChange = (companyTypeId, checked) => {
    const {
      onChange,
      userContext: {
        user: { uuid, email, menteeId },
      },
    } = this.props;
    const { companyTypes } = this.state;

    onChange(companyTypeId, checked);

    const selectedCompanyType = companyTypes.find(companyType => companyType.id === companyTypeId);
    trackFilterUsed({
      menteeId,
      location: 'mentors',
      filterName: selectedCompanyType.name,
      filterValue: checked,
      filterType: 'company_type',
      uuid,
      email,
    });
  };

  render() {
    const { loading, companyTypes } = this.state;

    return (
      <CheckboxFilterList
        title="Filter by Company Type"
        loading={loading}
        options={companyTypes}
        onChange={this.handleChange}
      />
    );
  }
}

export default withSnackbar(withUser(CompanyTypeFilterList));
