import React, { Component } from 'react';

import InfiniteScroll from 'react-infinite-scroller';
import LoadingSpinner from 'components/shared/LoadingSpinner';
import MentorCard from './MentorCard';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const StyledInfiniteScroll = styled(InfiniteScroll)`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  grid-column-gap: 20px;
  grid-row-gap: 20px;
  padding-bottom: 3rem;

  ${LoadingSpinner} {
    grid-column: 1/4;
    justify-self: center;
  }
`;

class MentorGrid extends Component {
  shouldComponentUpdate(nextProps) {
    const { mentors } = this.props;
    return nextProps.mentors !== mentors;
  }

  handleLoadMore = () => {
    const { onLoadMore, page } = this.props;
    onLoadMore(page + 1);
  };

  render() {
    const { relationshipMentorId, mentors, page, totalPages } = this.props;
    return (
      <StyledInfiniteScroll
        hasMore={page < totalPages - 1}
        loadMore={this.handleLoadMore}
        // https://github.com/CassetteRocks/react-infinite-scroller/issues/133
        loader={<LoadingSpinner key={0} />}
      >
        {mentors.map(mentor => {
          const isRelationship = relationshipMentorId === mentor.id;
          return (
            <MentorCard
              key={mentor.id}
              buttonTitle={isRelationship ? 'Go to my sessions' : 'View profile'}
              footerTitle={
                isRelationship && (
                  <>
                    {mentor.fullName}
                    <br />
                    is your long-term mentor
                  </>
                )
              }
              headerLink={`/mentors/${mentor.id}`}
              footerLink={isRelationship ? '/sessions' : `/mentors/${mentor.id}`}
              mentor={mentor}
              isNextAvailabilitiesShown={!isRelationship}
              variant={isRelationship ? 'primary' : 'secondary'}
            />
          );
        })}
      </StyledInfiniteScroll>
    );
  }
}

MentorGrid.propTypes = {
  page: PropTypes.number.isRequired,
  totalPages: PropTypes.number.isRequired,
  onLoadMore: PropTypes.func.isRequired,
  mentors: PropTypes.array.isRequired,
  relationshipMentorId: PropTypes.number,
};

MentorGrid.defaultProps = {
  relationshipMentorId: null,
};

export default MentorGrid;
