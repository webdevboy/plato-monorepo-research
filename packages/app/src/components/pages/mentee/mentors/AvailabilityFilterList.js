import React, { Component } from 'react';

import FilterList from 'components/shared/FilterList';
import RadioInput from 'components/shared/RadioInput';

class AvailabilityFilterList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedWeeksOut: undefined,
    };
  }

  onChange = weeksOut => {
    this.setState({
      selectedWeeksOut: weeksOut,
    });

    this.props.onChange(weeksOut);
  };

  render() {
    const options = [
      { label: 'Any', checked: true },
      { label: 'In the next week', weeksOut: 1 },
      { label: 'In the next 2 weeks', weeksOut: 2 },
      { label: 'In the next 3 weeks', weeksOut: 3 },
    ];

    return (
      <FilterList title="Filter by Availability">
        {options.map(option => (
          <RadioInput
            key={option.label}
            name="availability"
            isChecked={option.weeksOut === this.state.selectedWeeksOut}
            label={option.label}
            onChange={() => this.onChange(option.weeksOut)}
          />
        ))}
      </FilterList>
    );
  }
}

export default AvailabilityFilterList;
