import React, { Component } from 'react';

import CheckboxFilterList from 'components/shared/CheckboxFilterList';
import MenteeService from 'services/api/plato/mentee/mentee';
import TagService from 'services/api/plato/tag/tag';
import faceDisplayNames from 'utils/faceDisplayNames';
import { trackFilterUsed } from 'utils/analytics';
import { withSnackbar } from 'components/context/SnackbarContext';
import { withUser } from 'components/context/UserContext';

class RoleFilterList extends Component {
  constructor(props) {
    super(props);

    this.menteeId = this.props.userContext.user.menteeId;

    this.state = {
      loading: true,
      roles: [],
    };
  }

  async componentDidMount() {
    const { snackbar, onLoad } = this.props;

    try {
      const [faces, mentee] = await Promise.all([
        TagService.getFaces(),
        MenteeService.getMentee(this.menteeId),
      ]);

      this.setState(
        {
          roles: faces.map(face => ({
            ...face,
            name: faceDisplayNames(face.name),
            checked: mentee.faces.some(menteeFace => menteeFace.id === face.id),
          })),
        },
        () => onLoad(this.state.roles)
      );
    } catch (error) {
      console.error(error);
      snackbar.error('Something went wrong - sorry!');
    }

    this.setState({ loading: false });
  }

  handleChange = (roleId, checked) => {
    const { onChange, userContext } = this.props;
    const { roles } = this.state;

    onChange(roleId, checked);

    const selectedRole = roles.find(role => role.id === roleId);
    trackFilterUsed({
      menteeId: this.menteeId,
      location: 'mentors',
      filterName: selectedRole.name,
      filterValue: selectedRole.checked,
      filterType: 'role',
      uuid: userContext.user.uuid,
      email: userContext.user.email,
    });
  };

  render() {
    const { loading, roles } = this.state;

    return (
      <CheckboxFilterList
        title="Filter by Role"
        loading={loading}
        options={roles}
        onChange={this.handleChange}
      />
    );
  }
}

export default withUser(withSnackbar(RoleFilterList));
