import React, { Component } from 'react';

import Avatar from 'components/shared/Avatar';
import Button from 'components/shared/Button';
import Card from 'components/shared/Card';
import DateWithIcon from 'components/shared/DateWithIcon';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import RadioInput from 'components/shared/RadioInput';
import TimeWithIcon from 'components/shared/TimeWithIcon';
import getProfilePictureThumbnail from 'utils/thumbnail';
import styled from 'styled-components';
import { withUser } from 'components/context/UserContext';

const StyledMentorCard = styled(Card)`
  position: relative;
  text-align: center;
  display: flex;
  flex-direction: column;
  align-items: center;

  ${RadioInput} {
    position: absolute;
    top: 1rem;
    left: 1rem;
  }

  .header {
    height: 100%;
    width: 100%;
    padding: 2rem;
    cursor: pointer;

    h2 {
      margin-top: 2rem;
    }
  }
`;

const FooterButtonsWrapper = styled.div`
  display: flex;
  justify-content: space-around;
  width: 90%;
  margin-top: 2rem;

  ${Button} {
    width: 10rem;
  }
`;

const Footer = styled.div`
  align-items: center;
  border-top: 1px solid ${props => props.theme.palette.geyser};
  display: flex;
  flex-direction: column;
  flex-grow: 1;
  justify-content: space-between;
  min-height: 14rem;
  padding: 2rem 0;
  width: 100%;

  .title {
    text-transform: uppercase;
    font-weight: bold;
    font-size: 1.3rem;
    color: ${props => props.theme.palette.cadetBlue};
  }

  &.primary {
    background-color: ${props => props.theme.palette.cerulean};

    .title {
      color: ${props => props.theme.palette.white};
    }
  }

  .time-stamp-container {
    display: flex;
  }

  .time-stamp,
  .time-stamp p {
    font-size: 1.2rem;
  }

  .time-stamp:first-child {
    margin-right: 2rem;
  }
`;

class MentorCard extends Component {
  handleClick = () => {
    const { mentor, onClick } = this.props;
    onClick(mentor);
  };

  handleHeaderClick = () => {
    const { mentor, onClick, onSelect, isSelectable } = this.props;
    if (isSelectable) {
      onSelect(mentor);
    } else {
      onClick(mentor);
    }
  };

  renderFooterTitle() {
    const { mentor, footerTitle, isNextAvailabilitiesShown } = this.props;
    if (footerTitle) {
      return footerTitle;
    }

    if (mentor.nextAvailability && isNextAvailabilitiesShown) {
      return 'Next availability';
    }

    if (!mentor.nextAvailability && isNextAvailabilitiesShown) {
      return (
        <>
          {mentor.fullName}
          <br />
          has no availability left
        </>
      );
    }

    return null;
  }

  renderHeader() {
    const { mentor } = this.props;
    return (
      <>
        <Avatar
          size="lg"
          src={getProfilePictureThumbnail(mentor.fullName, mentor.profilePicture)}
          alt="Mentor thumbnail"
        />
        <h2>{mentor.fullName}</h2>
        <sub>
          {mentor.title} at <b>{mentor.companyName}</b>
        </sub>
      </>
    );
  }

  render() {
    const {
      buttonTitle,
      isSelectable,
      isSelected,
      headerLink,
      footerLink,
      mentor,
      isNextAvailabilitiesShown,
      variant,
      onBook,
    } = this.props;
    return (
      <StyledMentorCard>
        {isSelectable && (
          <RadioInput
            className="checkbox"
            isChecked={isSelected}
            name={String(mentor.id)}
            onChange={this.handleHeaderClick}
          />
        )}

        {headerLink ? (
          <Link to={headerLink} className="header" onClick={this.handleHeaderClick}>
            {this.renderHeader()}
          </Link>
        ) : (
          <div className="header" onClick={this.handleHeaderClick}>
            {this.renderHeader()}
          </div>
        )}
        <Footer className={variant}>
          <p className="title">{this.renderFooterTitle()}</p>
          {isNextAvailabilitiesShown && mentor.nextAvailability && (
            <div className="time-stamp-container">
              <DateWithIcon date={mentor.nextAvailability} className="time-stamp" />
              <TimeWithIcon time={mentor.nextAvailability} className="time-stamp" />
            </div>
          )}
          {footerLink ? (
            <Link to={footerLink}>
              <Button variant={variant} onClick={this.handleClick}>
                {buttonTitle}
              </Button>
            </Link>
          ) : (
            <FooterButtonsWrapper>
              <Button variant={variant} onClick={this.handleClick}>
                {buttonTitle}
              </Button>
              {onBook && (
                <Button variant="primary" onClick={onBook}>
                  Book
                </Button>
              )}
            </FooterButtonsWrapper>
          )}
        </Footer>
      </StyledMentorCard>
    );
  }
}

MentorCard.propTypes = {
  buttonTitle: PropTypes.string.isRequired,
  footerTitle: PropTypes.node,
  headerLink: PropTypes.string,
  footerLink: PropTypes.string,
  mentor: PropTypes.object.isRequired,
  onClick: PropTypes.func,
  isNextAvailabilitiesShown: PropTypes.bool,
  onBook: PropTypes.func,
  variant: PropTypes.oneOf(['secondary', 'primary']),
};

MentorCard.defaultProps = {
  footerTitle: null,
  headerLink: null,
  footerLink: null,
  onClick: () => {},
  isNextAvailabilitiesShown: false,
  variant: 'secondary',
  onBook: null,
};

export default styled(withUser(MentorCard))``;
