import React, { Component } from 'react';

import AmaBasicInfo from 'components/shared/AmaBasicInfo';
import AmaService from 'services/api/plato/ama/ama';
import Card from 'components/shared/Card';
import Hero from 'components/shared/Hero';
import IconLabel from 'components/shared/IconLabel';
import Layout from 'components/shared/Layout';
import { Link } from 'react-router-dom';
import MentorService from 'services/api/plato/mentor/mentor';
import PageLoadingSpinner from '../../../shared/PageLoadingSpinner';
import PersonInfo from 'components/shared/PersonInfo';
import SessionHeader from 'components/shared/SessionHeader';
import Text from 'components/shared/Text';
import { faLinkedin } from '@fortawesome/free-brands-svg-icons';
import styled from 'styled-components';
import { withSnackbar } from 'components/context/SnackbarContext';

const StyledMentorSession = styled.div`
  & ${Layout} {
    position: relative;
    top: 6rem;
    padding-bottom: 10rem;
  }

  & .back-button {
    font-size: 14px;
    font-weight: bold;
    text-transform: uppercase;
    color: ${({ theme }) => theme.palette.white};
  }
`;

const StyledColumns = styled.div`
  margin-top: 12rem;
  display: flex;
`;

const LeftCard = styled(Card)`
  width: 265px;
  margin-right: 20px;
  padding: 20px;

  & h4 {
    margin: 20px 0 15px;
    font-size: 14px;

    &:nth-of-type(1) {
      margin-top: 0;
    }
  }

  & .info {
    margin-bottom: 10px;
  }
`;

const PersonWrapper = styled.div`
  display: flex;
  flex-direction: row;
  margin-bottom: 1rem;
`;

const CustomPersonInfo = styled(PersonInfo)`
  width: 95%;
`;

const MentorCard = styled(Card)`
  padding: 3rem;
  width: 80rem;
  display: flex;
  flex-direction: column;
`;

const MenteeCard = styled(Card)`
  padding: 3rem;
  width: 80rem;
  display: flex;
  flex-direction: row;
  margin-bottom: 1.5rem;
`;

const LinkWrapper = styled.a`
  display: flex;
`;

class MenteeAmaDetail extends Component {
  state = {
    mentor: null,
    attendances: null,
    status: null,
    isLoading: false,
    storyId: null,
    storyName: null,
    storyTagNames: [],
    callScheduledAt: null,
    zoomLink: null,
  };

  async componentDidMount() {
    try {
      this.setState({
        isLoading: true,
      });

      const ama = await AmaService.getAma(this.props.match.params.amaId);
      const mentor = await MentorService.getMentor(ama.mentorId);

      const attendances = await AmaService.getAttendances(this.props.match.params.amaId);

      const filteredAttendances = attendances.filter(
        attendance => attendance.status === 'Scheduled'
      );

      const { storyId, storyName, storyTagNames, callScheduledAt, zoomLink } = ama;

      this.setState({
        storyId,
        storyName,
        storyTagNames,
        callScheduledAt: new Date(callScheduledAt),
        zoomLink,
        mentor,
        attendances: filteredAttendances,
        status: ama.status,
        isLoading: false,
      });
    } catch (error) {
      console.error(error);
      this.props.snackbar.error('Something went wrong - sorry!');
    }
  }

  render() {
    const {
      mentor,
      attendances,
      isLoading,
      storyId,
      storyName,
      storyTagNames,
      callScheduledAt,
      zoomLink,
      status,
    } = this.state;

    if (isLoading || !mentor) {
      return (
        <StyledMentorSession>
          <Hero />
          <PageLoadingSpinner />
        </StyledMentorSession>
      );
    }

    return (
      <StyledMentorSession>
        <Hero />
        <Layout width="1025">
          <Link to="/mentor_sessions" className="back-button">
            <IconLabel icon="arrow-left">Back</IconLabel>
          </Link>
          <SessionHeader
            name={storyName}
            tags={storyTagNames}
            startAt={callScheduledAt}
            zoomLink={zoomLink}
            status={status}
          />
          <StyledColumns>
            <div className="left">
              <LeftCard>
                <AmaBasicInfo
                  callScheduledAt={callScheduledAt}
                  storyId={storyId}
                  storyName={storyName}
                />
              </LeftCard>
            </div>
            <div className="right">
              <React.Fragment>
                <MentorCard>
                  <PersonWrapper>
                    <CustomPersonInfo
                      fullName={mentor.fullName}
                      title={mentor.title}
                      thumbnailUrl={mentor.picture.thumbnailUrl}
                      companyName={mentor.companyName}
                      linkUrl={`/mentors/${mentor.id}`}
                    />
                    <a href={mentor.linkedin} target="_blank" rel="noopener noreferrer">
                      <IconLabel
                        icon={faLinkedin}
                        style={{
                          fontSize: '2rem',
                          color: '#067a95',
                          verticalAlign: 'middle',
                        }}
                      />
                    </a>
                  </PersonWrapper>
                  <Text>{mentor.aboutMyself}</Text>
                </MentorCard>

                <div>
                  <Text margin="3rem 0 1rem" uppercase>
                    Attendees ({attendances.length})
                  </Text>
                  {attendances.map(attendee => (
                    <React.Fragment key={attendee.id}>
                      <MenteeCard>
                        <CustomPersonInfo
                          fullName={attendee.menteeFullName}
                          title={attendee.menteeTitle}
                          thumbnailUrl={attendee.menteePicture.thumbnailUrl}
                          companyName={attendee.menteeCompany}
                        />
                        <LinkWrapper
                          href={attendee.menteeLinkedin}
                          target="_blank"
                          rel="noopener noreferrer"
                        >
                          <IconLabel
                            icon={faLinkedin}
                            style={{
                              fontSize: '2rem',
                              color: '#067a95',
                              verticalAlign: 'middle',
                            }}
                          />
                        </LinkWrapper>
                      </MenteeCard>
                    </React.Fragment>
                  ))}
                </div>
              </React.Fragment>
            </div>
          </StyledColumns>
        </Layout>
      </StyledMentorSession>
    );
  }
}

export default withSnackbar(MenteeAmaDetail);
