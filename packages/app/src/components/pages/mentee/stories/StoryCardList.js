import React, { Component } from 'react';

import InfiniteScroll from 'react-infinite-scroller';
import PropTypes from 'prop-types';
import StoryCard from 'components/shared/StoryCard';
import styled from 'styled-components';

const StyledStoryCard = styled(StoryCard)`
  margin-bottom: 2rem;
`;

class StoryCardList extends Component {
  shouldComponentUpdate(nextProps) {
    return nextProps.stories !== this.props.stories;
  }

  render() {
    const { currentPageIndex, totalPages, onLoadMore, stories } = this.props;

    return (
      <InfiniteScroll hasMore={currentPageIndex < totalPages - 1} loadMore={onLoadMore}>
        {stories.map(story => (
          <StyledStoryCard key={story.id} story={story} isMentorInfoShown />
        ))}
      </InfiniteScroll>
    );
  }
}

StoryCardList.propTypes = {
  currentPageIndex: PropTypes.number.isRequired,
  totalPages: PropTypes.number.isRequired,
  onLoadMore: PropTypes.func.isRequired,
  stories: PropTypes.array.isRequired,
};

export default StoryCardList;
