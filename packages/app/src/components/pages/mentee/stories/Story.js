import { Link, withRouter } from 'react-router-dom';
import React, { Component, Fragment } from 'react';

import AmaService from 'components/pages/mentee/ama/amaService';
import BookMeetingCard from './BookMeetingCard';
import BookMeetingDialog from 'components/shared/BookMeetingDialog';
import Button from 'components/shared/Button';
import Card from 'components/shared/Card';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Layout from 'components/shared/Layout';
import MentorService from 'services/api/plato/mentor/mentor';
import PageLoadingSpinner from 'components/shared/PageLoadingSpinner';
import PersonInfo from 'components/shared/PersonInfo';
import SessionCard from 'components/pages/mentee/ama/SessionCard';
import SignUpDialog from 'components/pages/mentee/ama/SignUpDialog';
import StoryCard from 'components/shared/StoryCard';
import StoryService from 'services/api/plato/story/story';
import TagList from 'components/shared/TagList';
import TagService from 'services/api/plato/tag/tag';
import ViewService from 'services/api/plato/view/view';
import getProfilePictureThumbnail from 'utils/thumbnail';
import moment from 'moment';
import queryString from 'query-string';
import styled from 'styled-components';
import textToHtml from 'utils/textToHtml';
import { trackMenteeStoryRead } from 'utils/analytics';
import { withSnackbar } from 'components/context/SnackbarContext';
import { withUser } from 'components/context/UserContext';

const StyledLayout = styled(Layout)`
  margin-top: 6.9rem;

  .card,
  .list-title,
  .back-button,
  .teaser {
    margin-bottom: 3rem;
  }

  .story-title {
    margin-bottom: 2rem;
  }

  ${TagList} {
    margin-bottom: 0.5rem;
  }

  ${SessionCard} {
    margin-bottom: 2rem;
  }

  .profile-card {
    display: flex;
    transition: all ${props => props.theme.transitions.default};

    ${PersonInfo} {
      width: 100%;
      flex: 1;
      padding: 2rem 2rem 2rem 3rem;
    }

    .view-profile-button {
      margin-right: 3rem;
      display: flex;
      align-items: center;
    }
  }

  .story-content {
    p {
      text-align: justify;
      margin-bottom: 1rem;
    }

    h1 {
      margin: 3rem 0 1rem 0;
    }
  }
`;

class Story extends Component {
  constructor(props) {
    super(props);
    const { location, history } = props;
    let {
      match: {
        params: { storyId },
      },
    } = props;
    const queryParams = queryString.parse(location.search);

    // Needed to support link from legacy email and Slack campaign
    if (!storyId) {
      storyId = +queryParams.story_id;
      if (!storyId) {
        history.replace('/stories');
        return;
      }
    }

    this.state = {
      amas: [],
      loading: true,
      story: undefined,
      storyId,
      source: queryParams.source,
      mentorAvailabilities: [],
      relatedStories: [],
      bookMeetingDialogOpen: false,
      signUpDialogOpen: false,
      signUpAma: undefined,
    };
  }

  async componentDidMount() {
    await this.loadPage();

    const {
      userContext: {
        user: { email, uuid, menteeId },
      },
    } = this.props;
    const { source, storyId } = this.state;

    trackMenteeStoryRead({
      email,
      menteeId,
      source,
      storyId,
      uuid,
    });

    ViewService.create({
      viewedId: storyId,
      viewerId: menteeId,
      type: 'Views::StoryByMentee',
    });
  }

  componentWillReceiveProps(nextProps) {
    const {
      match: {
        params: { storyId },
      },
    } = this.props;
    const nextStoryId = nextProps.match.params.storyId;
    if (nextStoryId !== storyId) {
      this.setState({ storyId: nextStoryId }, this.loadPage);
    }
  }

  getRelatedStoriesAlgoliaFilter = () => {
    const { story } = this.state;
    const tagIdsFilter = story.tagIds.map(tagId => `tagIds:${tagId}`).join(' OR ');
    return tagIdsFilter
      ? `internalOnly:false AND status:"Published" AND (${tagIdsFilter})`
      : 'internalOnly:false AND status:"Published"';
  };

  handleOpenBookMeetingDialog = () => {
    this.setState({ bookMeetingDialogOpen: true });
  };

  handleCloseBookMeetingDialog = () => {
    this.setState({ bookMeetingDialogOpen: false });
    this.loadPage();
  };

  handleOpenSignUpDialog = ama => {
    this.setState({ signUpDialogOpen: true, signUpAma: ama });
  };

  handleCloseSignUpDialog = () => {
    this.setState({
      signUpDialogOpen: false,
    });
  };

  loadPage = async () => {
    const { snackbar } = this.props;
    const { storyId } = this.state;

    this.setState({ loading: true });
    try {
      const story = await StoryService.getStory(storyId);
      this.setState({ story });

      const { mentorId, tagIds } = story;
      const [relatedStories, amas, mentorAvailabilities, tags] = await Promise.all([
        StoryService.getStories(this.getRelatedStoriesAlgoliaFilter()),
        this.getAmas(mentorId),
        tagIds.length > 0 ? MentorService.getAvailabilities(mentorId) : null,
        TagService.getTopicsForFilter(),
      ]);

      this.setState({
        amas,
        mentorAvailabilities: mentorAvailabilities || [],
        relatedStories: relatedStories.result.filter(({ id }) => id !== +storyId).slice(0, 3),
        tags,
      });
    } catch (error) {
      console.error(error);
      snackbar.error('Something went wrong - sorry!');
    }

    this.setState({
      loading: false,
    });
  };

  getAmas = async mentorId => {
    const amas = await MentorService.getAmas({
      mentorId,
      scheduled: true,
      closed: false,
      done: false,
      error: false,
      minDate: moment().toISOString(true),
    });

    await AmaService.getAdditionalAmaInfo(amas.result);

    return amas.result;
  };

  loadAmas = async mentorId => {
    const amas = await this.getAmas(mentorId);

    this.setState({
      amas,
    });
  };

  render() {
    const {
      amas,
      bookMeetingDialogOpen,
      loading,
      mentorAvailabilities,
      relatedStories,
      story,
      signUpAma,
      signUpDialogOpen,
    } = this.state;

    if (loading) {
      return <PageLoadingSpinner />;
    }

    const mentorHasAvailabilities = mentorAvailabilities.length > 0;
    return (
      <StyledLayout width={780}>
        <Link className="back-button" to="/stories">
          <Button variant="secondary-link">
            <FontAwesomeIcon icon="arrow-left" />
            &nbsp; View All Stories
          </Button>
        </Link>
        <h1 className="story-title">{story.name}</h1>
        <TagList tags={story.tags.map(({ name }) => name)} />
        <p className="teaser">{story.teaser}</p>
        <Card className="profile-card">
          <PersonInfo
            fullName={story.mentor.fullName}
            title={story.mentor.title}
            thumbnailUrl={getProfilePictureThumbnail(
              story.mentor.firstName,
              story.mentor.picture.thumbnailUrl
            )}
            companyName={story.mentor.companyName}
            linkUrl={`/mentors?mentor_id=${story.mentor.id}`}
          />
          <Link className="view-profile-button" to={`/mentors/${story.mentor.id}`}>
            <Button variant="secondary">View profile</Button>
          </Link>
        </Card>
        <div className="story-content">
          {story.storyParagraphs.map(paragraph => (
            <Fragment key={paragraph.id}>
              <h1>{paragraph.title}</h1>
              {textToHtml(paragraph.body)}
            </Fragment>
          ))}
        </div>

        {mentorHasAvailabilities && (
          <>
            <hr />
            <BookMeetingCard
              mentor={story.mentor}
              nextAvailabilityStartTime={mentorAvailabilities[0].startTime}
              onBookMeetingClick={this.handleOpenBookMeetingDialog}
            />
          </>
        )}

        {amas.length > 0 && (
          <>
            <hr />
            <h3 className="list-title">Upcoming AMAs from {story.mentor.firstName}</h3>
            {amas.map(ama => {
              const topics = ama.tagIds.map(
                tagId => this.state.tags.find(tag => tag.id === tagId).name
              );

              return (
                <SessionCard
                  key={ama.id}
                  attendances={ama._attendances}
                  limitAttendances={ama.limitAttendances}
                  callScheduledAt={ama.callScheduledAt}
                  storyId={ama.storyId}
                  storyName={ama.storyName}
                  mentorTitle={ama._mentor.title}
                  mentorCompanyName={ama._mentor.companyName}
                  mentorId={ama.mentorId}
                  mentorFullName={ama.mentorFullName}
                  mentorPictureUrl={getProfilePictureThumbnail(
                    ama._mentor.firstName,
                    ama.mentorPicture.thumbnailUrl
                  )}
                  topics={topics}
                  onSignUpClick={() => this.handleOpenSignUpDialog(ama)}
                />
              );
            })}
          </>
        )}

        {relatedStories.length > 0 && (
          <>
            <hr />
            <h3 className="list-title">Related Stories</h3>
            {relatedStories.map(relatedStory => (
              <StoryCard
                key={relatedStory.id}
                className="card"
                story={relatedStory}
                isMentorInfoShown
              />
            ))}
          </>
        )}

        <BookMeetingDialog
          open={bookMeetingDialogOpen}
          onClose={this.handleCloseBookMeetingDialog}
          story={story}
          mentorId={story.mentorId}
          availabilities={mentorAvailabilities}
        />

        {signUpAma && (
          <SignUpDialog
            open={signUpDialogOpen}
            onClose={this.handleCloseSignUpDialog}
            storyName={signUpAma.storyName}
            mentorFullName={signUpAma.mentorFullName}
            mentorTitle={signUpAma._mentor.title}
            mentorCompanyName={signUpAma._mentor.companyName}
            limitAttendances={signUpAma.limitAttendances}
            attendances={signUpAma._attendances}
            callScheduledAt={signUpAma.callScheduledAt}
            amaId={signUpAma.id}
            onConfirm={() => this.loadAmas(story.mentorId)}
          />
        )}
      </StyledLayout>
    );
  }
}

export default withSnackbar(withUser(withRouter(Story)));
