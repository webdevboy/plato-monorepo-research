import React, { Component } from 'react';
import { trackFilterUsed, trackSearchSubmitted } from 'utils/analytics';

import Checkbox from 'components/shared/Checkbox';
import EmptyStateCard from 'components/shared/EmptyStateCard';
import FilterList from 'components/shared/FilterList';
import Layout from 'components/shared/Layout';
import PageLoadingSpinner from 'components/shared/PageLoadingSpinner';
import SearchInput from 'components/shared/SearchInput';
import StoryCardList from './StoryCardList';
import StoryService from 'services/api/plato/story/story';
import TagService from 'services/api/plato/tag/tag';
import getFaceDisplayName from 'utils/faceDisplayNames';
import styled from 'styled-components';
import { withQueryLoader } from 'components/context/QueryLoaderContext';
import { withSnackbar } from 'components/context/SnackbarContext';
import { withUser } from 'components/context/UserContext';

const StyledLayout = styled(Layout)`
  margin-top: 5rem;
`;

const StoriesContainer = styled.div`
  display: flex;
  justify-content: space-between;
  width: 100%;
`;

const FiltersColumn = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: 5rem;
  width: 26.5rem;

  > * {
    width: 100%;
    margin-bottom: 2.9rem;
  }
`;

const StoriesColumn = styled.div`
  display: flex;
  flex-direction: column;
  margin-left: 1.9rem;
  flex: 1;

  h3 {
    margin-bottom: 3rem;
  }
`;

const maximumPageSize = 6;

/**
 * Create search query for Algolia by role and topic.
 * @todo
 * @param {Array<Object>} roleFilterOptions An array of options for roles.
 * @param {Array<Object>} topicFilterOptions An array of options for topics.
 * @return {String} An Algolia search query string.
 */
function createAlgoliaFilter(roleFilterOptions, topicFilterOptions) {
  const mentorFilter = roleFilterOptions
    .reduce(
      (acc, { checked, name }) => (checked ? acc.concat(`mentorFaceNames:"${name}"`) : acc),
      []
    )
    .join(' OR ');
  const tagFilter = topicFilterOptions
    .reduce((acc, { checked, id }) => (checked ? acc.concat(`tagIds:"${id}"`) : acc), [])
    .join(' OR ');

  return ['status:"Published"', mentorFilter, tagFilter]
    .reduce((acc, filter) => (filter !== '' ? acc.concat(`(${filter})`) : acc), [])
    .join(' AND ');
}

class Stories extends Component {
  constructor(props) {
    super(props);

    this.state = {
      stories: [],
      currentPageIndex: 0,
      searchQuery: '',
      roleFilterOptions: [],
      topicFilterOptions: [],
      totalPages: undefined,
      totalItems: undefined,
      loading: false,
    };
  }

  async componentDidMount() {
    const {
      snackbar,
      userContext: { mentee },
    } = this.props;

    this.setState({ loading: true });
    try {
      // Get the roles and topics to filter the list of stories.
      const [allFaces, topics] = await Promise.all([
        TagService.getFaces(),
        TagService.getTopicsForFilter(),
      ]);

      this.setState({
        roleFilterOptions: allFaces.map(face => ({
          id: face.id,
          name: face.name,
          label: getFaceDisplayName(face.name),
          checked: mentee.faces.some(menteeFace => menteeFace.id === face.id),
        })),
        topicFilterOptions: topics
          .filter(topic => topic.storyCount > 0)
          .sort((topicA, topicB) => topicB.storyCount - topicA.storyCount)
          .map(topic => ({
            id: topic.id,
            name: topic.name,
            label: `${topic.name} (${topic.storyCount})`,
            checked: false,
          })),
      });

      await this.handleLoadStoriesPage(0);
    } catch (error) {
      console.error(error);
      snackbar.error('Something went wrong - sorry!');
    }
    this.setState({ loading: false });
  }

  handleLoadStoriesPage = async pageIndex => {
    const { queryLoader, snackbar } = this.props;
    const { roleFilterOptions, topicFilterOptions, searchQuery } = this.state;

    queryLoader.showLoader();
    const filter = createAlgoliaFilter(roleFilterOptions, topicFilterOptions);
    try {
      const response = await StoryService.getStories(
        filter,
        searchQuery,
        pageIndex,
        maximumPageSize
      );

      this.setState(prevState => ({
        stories: [...prevState.stories, ...response.result],
        currentPageIndex: pageIndex,
        totalPages: response.totalPages,
        totalItems: response.totalItems,
      }));
    } catch (error) {
      console.error(error);
      snackbar.error(error);
    }
    queryLoader.hideLoader();
  };

  handleRoleFilterChanged = (name, checked) => {
    const {
      userContext: {
        user: { uuid, email, menteeId },
      },
    } = this.props;
    const { roleFilterOptions } = this.state;
    this.setState({
      stories: [],
      roleFilterOptions: roleFilterOptions.map(option =>
        option.name === name ? Object.assign(option, { checked }) : option
      ),
    });
    this.handleLoadStoriesPage(0);

    trackFilterUsed({
      menteeId,
      location: 'stories',
      filterName: name,
      filterValue: checked,
      filterType: 'role',
      email,
      uuid,
    });
  };

  handleTopicFilterChanged = (name, checked) => {
    const {
      userContext: {
        user: { uuid, email, menteeId },
      },
    } = this.props;
    const { topicFilterOptions } = this.state;
    this.setState({
      stories: [],
      topicFilterOptions: topicFilterOptions.map(option =>
        option.name === name ? Object.assign(option, { checked }) : option
      ),
    });
    this.handleLoadStoriesPage(0);

    trackFilterUsed({
      menteeId,
      location: 'stories',
      filterName: name,
      filterValue: checked,
      filterType: 'topic',
      email,
      uuid,
    });
  };

  handleQueryChange = event => {
    const {
      userContext: {
        user: { uuid, email, menteeId },
      },
    } = this.props;
    const {
      target: { value },
    } = event;

    this.setState({ stories: [], searchQuery: value });
    this.handleLoadStoriesPage(0);

    if (value.length > 0) {
      trackSearchSubmitted({
        email,
        location: 'stories',
        menteeId,
        searchString: value,
        uuid,
      });
    }
  };

  render() {
    const {
      totalItems,
      currentPageIndex,
      roleFilterOptions,
      topicFilterOptions,
      searchQuery,
      loading,
      stories,
      totalPages,
    } = this.state;

    if (loading) {
      return <PageLoadingSpinner />;
    }

    return (
      <StyledLayout width="1120">
        <StoriesContainer>
          <FiltersColumn>
            <SearchInput
              placeholder="Search"
              onChange={this.handleQueryChange}
              value={searchQuery}
            />
            <FilterList title="Filter by Roles">
              {roleFilterOptions.map(option => (
                <Checkbox
                  key={option.id}
                  label={option.label}
                  name={option.name}
                  checked={option.checked}
                  onChange={this.handleRoleFilterChanged}
                />
              ))}
            </FilterList>
            <FilterList title="Filter by Topics" isExpandable>
              {topicFilterOptions.map(option => (
                <Checkbox
                  key={option.id}
                  label={option.label}
                  name={option.name}
                  checked={option.checked}
                  onChange={this.handleTopicFilterChanged}
                />
              ))}
            </FilterList>
          </FiltersColumn>
          <StoriesColumn>
            <h3>{totalItems} stories</h3>
            {stories.length > 0 ? (
              <StoryCardList
                currentPageIndex={currentPageIndex}
                totalPages={totalPages}
                onLoadMore={this.handleLoadStoriesPage}
                stories={stories}
              />
            ) : (
              <EmptyStateCard title="We didn't find any matches." />
            )}
          </StoriesColumn>
        </StoriesContainer>
      </StyledLayout>
    );
  }
}

export default withSnackbar(withUser(withQueryLoader(Stories)));
