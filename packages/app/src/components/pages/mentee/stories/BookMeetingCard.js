import React, { Component } from 'react';

import Button from 'components/shared/Button';
import Card from 'components/shared/Card';
import DateTimeWithIcons from 'components/shared/DateTimeWithIcons';
import PersonInfo from 'components/shared/PersonInfo';
import { formatTimestampToString } from 'utils/time';
import getProfilePictureThumbnail from 'utils/thumbnail';
import moment from 'moment-timezone';
import styled from 'styled-components';
import { withUser } from 'components/context/UserContext';

const StyledBookMeetingCard = styled.div`
  position: relative;
  display: flex;
  align-items: flex-start;

  .linkedin-logo {
    position: absolute;
    top: 0;
    right: 0;
  }

  .availability {
    display: flex;
    flex-direction: column;
    justify-content: center;
    width: 30rem;
    background: ${props => props.theme.palette.polar};
    padding: 3rem;
    border-right: 1px solid ${props => props.theme.palette.geyser};
    flex-shrink: 0;

    h1 {
      margin-bottom: 2rem;
    }

    > p {
      font-weight: 600;
      margin-bottom: 1rem;
    }

    ${DateTimeWithIcons} {
      margin-bottom: 2rem;
    }

    ${Button} {
      width: 100%;
    }
  }

  .person-info {
    padding: 3rem 1rem 3rem 0;
  }

  .mentor {
    display: flex;
    flex-direction: column;
    padding: 3rem;
    flex-grow: 1;
  }
`;

class BookMeetingCard extends Component {
  render() {
    const { mentor, nextAvailabilityStartTime, onBookMeetingClick, userContext } = this.props;
    // DbMentor.slot_time is obsolete, we calculate it
    // using the next availabilities

    return (
      <StyledBookMeetingCard>
        <PersonInfo
          about={mentor.aboutMyself}
          className="person-info"
          companyName={mentor.companyName}
          fullName={mentor.fullName}
          linkUrl={`/mentors?mentor_id=${mentor.id}`}
          size="md"
          tags={mentor.storyTagNames}
          thumbnailUrl={getProfilePictureThumbnail(mentor.firstName, mentor.picture.thumbnailUrl)}
          title={mentor.title}
        />
        <Card className="availability">
          <h1>
            Hey, I&apos;m available every&nbsp;
            {moment(nextAvailabilityStartTime)
              .tz(userContext.timeZone())
              .format('dddd')}
            &nbsp;at&nbsp;
            {formatTimestampToString(nextAvailabilityStartTime, userContext.timeZone())}
          </h1>
          <p>Next time slot available</p>
          <DateTimeWithIcons timestamp={nextAvailabilityStartTime} />
          <Button variant="primary" onClick={onBookMeetingClick}>
            Book a Meeting
          </Button>
        </Card>
      </StyledBookMeetingCard>
    );
  }
}

export default styled(withUser(BookMeetingCard))``;
