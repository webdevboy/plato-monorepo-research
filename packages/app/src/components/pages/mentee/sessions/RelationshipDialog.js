import React, { Component } from 'react';

import Button from 'components/shared/Button';
import EmptyStateCard from 'components/shared/EmptyStateCard';
import { Link } from 'react-router-dom';
import PersonInfo from 'components/shared/PersonInfo';
import PlatoDialog from 'components/shared/PlatoDialog';
import getProfilePictureThumbnail from 'utils/thumbnail';
import { isRelationshipAvailable } from 'utils/relationship';
import sessionsService from 'components/pages/mentee/sessions/sessionsService';
import styled from 'styled-components';
import { withSnackbar } from 'components/context/SnackbarContext';
import { withUser } from 'components/context/UserContext';

const Body = styled.div`
  display: flex;
  flex-direction: column;
  padding: 3rem;
  max-width: 70rem;

  .title {
    margin-bottom: 2.5rem;
  }

  .description {
    font-size: 1.6rem;
    margin-bottom: 4rem;
  }

  ${Button} {
    margin-top: auto;
    margin-left: auto;
  }
`;

const StyledEmptyStateCard = styled(EmptyStateCard)`
  a {
    color: ${({ theme }) => theme.palette.cerulean};
  }
`;

class RelationshipDialog extends Component {
  constructor(props) {
    super(props);

    this.state = {
      saving: false,
    };
  }

  setRelationship = async () => {
    const {
      snackbar,
      triggerRefresh,
      onClose,
      mentor,
      userContext: { user },
    } = this.props;

    this.setState({ saving: true });

    try {
      await sessionsService.createRelationship({
        menteeId: user.menteeId,
        mentorId: mentor.id,
      });
    } catch (error) {
      console.error(error);
      snackbar.error('Something went wrong - sorry!');
    }

    this.setState({ saving: false });

    triggerRefresh();
    onClose();
  };

  render() {
    const {
      open,
      onClose,
      mentor,
      userContext: { mentee },
    } = this.props;
    const { saving } = this.state;

    if (!mentor) {
      return null;
    }

    if (!isRelationshipAvailable(mentor, mentee, true)) {
      // Or the mentee already has a relationship,
      // or the mentor is not available anymore
      return (
        <PlatoDialog open={open} onClose={onClose} saving={saving}>
          {mentee.hasLtm ? (
            <StyledEmptyStateCard
              title="Oh! you already have a long-term mentorship."
              description="You can not have two at the same time for now!"
            />
          ) : (
            <StyledEmptyStateCard
              image="🕓"
              title={
                <>
                  Oh! {mentor.fullName} is not available
                  <br />
                  for a long-term mentorship anymore.
                </>
              }
              description={
                <>
                  Look at our <Link to="/mentors">list of mentors</Link> to find the right one for
                  you.
                </>
              }
            />
          )}
        </PlatoDialog>
      );
    }

    return (
      <PlatoDialog open={open} onClose={onClose} saving={saving}>
        <Body>
          <PersonInfo
            fullName={mentor.fullName}
            title={mentor.title}
            thumbnailUrl={getProfilePictureThumbnail(mentor.firstName, mentor.picture.thumbnailUrl)}
            companyName={mentor.companyName}
            tags={mentor.storyTagNames}
            linkUrl={`/mentors?mentor_id=${mentor.id}`}
          />
          <h1 className="title">Subscribe to a long-term mentorship with {mentor.fullName}</h1>
          <p className="description">
            You and {mentor.firstName} already had a talk that you rated a 9 or a 10. You can choose
            to start a long-term mentorship with {mentor.firstName} for the next 6 months. Long-term
            mentorships are a great way to improve skills by receiving deep, personal feedback and
            strengthening your network.
          </p>
          <Button variant="primary" onClick={this.setRelationship} disabled={saving}>
            Set Long-Term Mentorship
          </Button>
        </Body>
      </PlatoDialog>
    );
  }
}

export default withUser(withSnackbar(RelationshipDialog));
