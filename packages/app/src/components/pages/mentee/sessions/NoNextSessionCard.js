import React from 'react';
import Button from 'components/shared/Button';
import { Link } from 'react-router-dom';
import EmptyStateCard from 'components/shared/EmptyStateCard';
import styled from 'styled-components';

const StyledEmptyCard = styled(EmptyStateCard)`
  .links {
    display: flex;

    .mentors-link {
      margin-right: 3rem;
    }
  }
`;

const NoNextSessionCard = ({ className }) => (
  <StyledEmptyCard
    className={className}
    title="Oh! It appears you don't have any upcoming sessions."
    description="Click below to find a mentor and book a session!"
  >
    <div className="links">
      <Link to="/mentors" className="mentors-link">
        <Button variant="primary">Find a Mentor</Button>
      </Link>
      <Link to="/stories">
        <Button variant="secondary">Read Stories</Button>
      </Link>
    </div>
  </StyledEmptyCard>
);

export default NoNextSessionCard;
