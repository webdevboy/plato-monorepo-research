import React, { Component } from 'react';

import InfiniteScroll from 'react-infinite-scroller';
import LoadingSpinner from 'components/shared/LoadingSpinner';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import NoSessionsCard from './NoSessionsCard';
import OneOnOneCard from './OneOnOneCard';
import AmaCard from './AmaCard';

const StyledWrapper = styled.div`
  position: relative;
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;

  .card {
    margin-bottom: 3rem;
  }

  .scroll-content {
    display: flex;
    flex-direction: column;
    align-items: center;
  }

  ${LoadingSpinner} {
    justify-self: center;
  }
`;

class CardList extends Component {
  shouldComponentUpdate(nextProps) {
    return (
      nextProps.sessions.length !== this.props.sessions.length ||
      nextProps.timeframe !== this.props.timeframe ||
      nextProps.type !== this.props.type ||
      nextProps.page !== this.props.page
    );
  }

  render() {
    const {
      timeframe,
      type,
      sessions,
      page,
      totalPages,
      onLoadMore,
      onChange,
      onCancelClick,
      editRequestId,
      clearRequestId,
    } = this.props;
    return (
      <StyledWrapper>
        {sessions.length > 0 && (
          <InfiniteScroll
            className="scroll-content"
            hasMore={page < totalPages}
            loadMore={onLoadMore}
            // https://github.com/CassetteRocks/react-infinite-scroller/issues/133
            loader={<LoadingSpinner key={0} />}
          >
            {sessions.map(session =>
              type === 'ama' ? (
                <AmaCard
                  key={session.id}
                  session={session}
                  onCancelClick={onCancelClick}
                  isEditable={timeframe === 'future'}
                  className="card"
                />
              ) : (
                <OneOnOneCard
                  key={session.id}
                  session={session}
                  onChange={onChange}
                  onCancelClick={onCancelClick}
                  isEditable={timeframe === 'future'}
                  className="card"
                  editRequestId={editRequestId}
                  clearRequestId={clearRequestId}
                />
              )
            )}
          </InfiniteScroll>
        )}

        {sessions.length === 0 && <NoSessionsCard timeframe={timeframe} type={type} />}
      </StyledWrapper>
    );
  }
}

CardList.propTypes = {
  onCancelClick: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  onLoadMore: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  sessions: PropTypes.array.isRequired,
  timeframe: PropTypes.oneOf(['future', 'past']).isRequired,
  totalPages: PropTypes.number.isRequired,
  type: PropTypes.oneOf(['ama', 'oneOnOne']).isRequired,
};

export default styled(CardList)``;
