import React from 'react';
import PlatoDialog from 'components/shared/PlatoDialog';
import styled from 'styled-components';
import Button from 'components/shared/Button';

const Body = styled.div`
  display: flex;
  flex-direction: column;
  padding: 3rem;

  & h1 {
    margin-bottom: 2.5rem;
  }

  & .explanation {
    font-size: 1.6rem;
    margin-bottom: 4rem;
  }

  & ${Button} {
    margin-top: auto;
    margin-left: auto;
  }
`;

const CancelSessionDialog = ({ open, onClose }) => (
  <PlatoDialog open={open} onClose={onClose}>
    <Body>
      <h1>Are you sure you want to cancel?</h1>

      <p className="explanation">
        This feature is not supported yet. In order to decline this session, simply decline the
        event in your calendar!
      </p>

      <Button variant="primary" onClick={onClose}>
        Got it!
      </Button>
    </Body>
  </PlatoDialog>
);

export default CancelSessionDialog;
