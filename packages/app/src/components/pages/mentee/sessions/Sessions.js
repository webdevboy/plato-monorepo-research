import React, { Component } from 'react';

import AmaCard from './AmaCard';
import CancelSessionDialog from './CancelSessionDialog';
import CardList from './CardList';
import Hero from 'components/shared/Hero';
import Layout from 'components/shared/Layout';
import MentorService from 'services/api/plato/mentor/mentor';
import NoNextSessionCard from './NoNextSessionCard';
import OneOnOneCard from './OneOnOneCard';
import PageHeader from 'components/shared/PageHeader';
import PageLoadingSpinner from 'components/shared/PageLoadingSpinner';
import RelationshipDialog from './RelationshipDialog';
import RequestService from 'services/api/plato/request/request';
import SessionOptions from './SessionOptions';
import queryString from 'query-string';
import sessionsService from './sessionsService';
import styled from 'styled-components';
import { withQueryLoader } from 'components/context/QueryLoaderContext';
import { withSnackbar } from 'components/context/SnackbarContext';
import { withUser } from 'components/context/UserContext';

const StyledWrapper = styled.div`
  & ${Layout} {
    padding-top: 7rem;
    padding-bottom: 3rem;

    & .next-session {
      margin-bottom: 6rem;
    }
  }
`;

class Sessions extends Component {
  state = {
    cancelSessionDialogIsOpen: false,
    currentPageIndex: 0,
    doingInitialLoad: true,
    isRelationshipDialogOpen: false,
    nextSession: undefined,
    sessions: [],
    timeframe: 'future',
    type: 'oneOnOne',
    editRequestId: '',
  };

  async componentDidMount() {
    const { location, history } = this.props;

    this.initialLoad();

    const {
      mentor_id: mentorId,
      request_id: requestId,
      key_actions_status: keyActionsStatus,
      edit,
    } = queryString.parse(location.search);
    if (mentorId) {
      // Prompt the user to start a relationship
      const mentor = await MentorService.getMentor(mentorId);
      this.setState({ isRelationshipDialogOpen: true, mentor });
    } else if (requestId && keyActionsStatus) {
      RequestService.updateKeyActionsStatus({
        requestId: parseInt(requestId, 10),
        status: keyActionsStatus.toUpperCase(),
      });
    } else if (edit) {
      this.setState({
        editRequestId: edit,
      });
      delete queryString.parse(location.search).edit;
      history.replace('/sessions');
    }
  }

  initialLoad = async () => {
    const {
      userContext: {
        mentee: { id },
      },
    } = this.props;
    this.setState({ doingInitialLoad: true });
    await this.getPage({});
    const nextSession = await sessionsService.getNextSession(id);
    this.setState({ nextSession, doingInitialLoad: false });
  };

  loadPage = async ({
    timeframe = this.state.timeframe,
    type = this.state.type,
    pageIndex = this.state.currentPageIndex,
  }) => {
    this.props.queryLoader.showLoader();
    await this.getPage({ timeframe, type, pageIndex });
    this.props.queryLoader.hideLoader();
  };

  getPage = async ({
    timeframe = this.state.timeframe,
    type = this.state.type,
    pageIndex = this.state.currentPageIndex,
  }) => {
    const {
      snackbar,
      userContext: {
        mentee: { id },
      },
    } = this.props;

    let response;
    try {
      if (type === 'oneOnOne' && timeframe === 'future') {
        response = await sessionsService.getFutureRequests(id, pageIndex);
      } else if (type === 'oneOnOne' && timeframe === 'past') {
        response = await sessionsService.getPastRequests(id, pageIndex);
      } else if (type === 'ama' && timeframe === 'future') {
        response = await sessionsService.getFutureAmaAttendances(id, pageIndex);
      } else if (type === 'ama' && timeframe === 'past') {
        response = await sessionsService.getPastAmaAttendances(id, pageIndex);
      }
    } catch (error) {
      console.error(error);
      snackbar.error('Something went wrong - sorry!');
    }

    if (!response) {
      return;
    }

    const shouldReplaceList = timeframe !== this.state.timeframe || type !== this.state.type;

    this.setState(prevState => ({
      sessions: [...(shouldReplaceList ? [] : prevState.sessions), ...response.result],
      currentPageIndex: pageIndex,
      timeframe,
      type,
      totalPages: response.totalPages,
    }));
  };

  handleLoadMore = async () => {
    await this.loadPage({ pageIndex: this.state.currentPageIndex + 1 });
  };

  onTimeframeChange = timeframe => {
    this.loadPage({ timeframe, pageIndex: 0 });
  };

  onTypeChange = event => {
    this.loadPage({ type: event.target.value, pageIndex: 0 });
  };

  getCardListSessions = () => {
    const { nextSession, sessions, type } = this.state;

    // we only want to display the first relationship session, not all of them
    const futureRelationshipSessions = sessions.reduce(
      (acc, session) =>
        session.relationshipId &&
        acc.some(accSession => accSession.relationshipId === session.relationshipId)
          ? acc
          : acc.concat(session),
      []
    );

    return nextSession && nextSession._type === type
      ? futureRelationshipSessions.filter(session => session.id !== nextSession.id)
      : futureRelationshipSessions;
  };

  clearEditRequestId = () => {
    this.setState({ editRequestId: '' });
  };

  openCancelSessionDialog = () => {
    this.setState({ cancelSessionDialogIsOpen: true });
  };

  closeCancelSessionDialog = () => {
    this.setState({ cancelSessionDialogIsOpen: false });
  };

  handleCloseRelationshipDialog = () => {
    this.setState({ isRelationshipDialogOpen: false });
  };

  render() {
    const {
      cancelSessionDialogIsOpen,
      currentPageIndex,
      doingInitialLoad,
      isRelationshipDialogOpen,
      mentor,
      nextSession,
      timeframe,
      totalPages,
      type,
      editRequestId,
    } = this.state;

    return (
      <StyledWrapper>
        <Hero />
        {doingInitialLoad && <PageLoadingSpinner />}
        {!doingInitialLoad && (
          <Layout width="780">
            {nextSession && (
              <React.Fragment>
                <PageHeader>Your Next Session</PageHeader>
                {nextSession._type === 'ama' && (
                  <AmaCard
                    className="next-session"
                    session={nextSession}
                    onCancelClick={this.openCancelSessionDialog}
                    isEditable
                  />
                )}
                {nextSession._type === 'oneOnOne' && (
                  <OneOnOneCard
                    className="next-session"
                    session={nextSession}
                    onChange={this.initialLoad}
                    onCancelClick={this.openCancelSessionDialog}
                    isEditable
                    editRequestId={editRequestId}
                    clearRequestId={this.clearEditRequestId}
                  />
                )}
              </React.Fragment>
            )}
            {!nextSession && <NoNextSessionCard className="next-session" />}

            <SessionOptions
              timeframe={timeframe}
              type={type}
              onTimeframeChange={this.onTimeframeChange}
              onTypeChange={this.onTypeChange}
            />

            <CardList
              onCancelClick={this.openCancelSessionDialog}
              onChange={this.initialLoad}
              onLoadMore={this.handleLoadMore}
              page={currentPageIndex + 1}
              sessions={this.getCardListSessions()}
              timeframe={timeframe}
              totalPages={totalPages}
              type={type}
              editRequestId={editRequestId}
              clearRequestId={this.clearEditRequestId}
            />

            <CancelSessionDialog
              open={cancelSessionDialogIsOpen}
              onClose={this.closeCancelSessionDialog}
            />

            <RelationshipDialog
              open={isRelationshipDialogOpen}
              onClose={this.handleCloseRelationshipDialog}
              mentor={mentor}
            />
          </Layout>
        )}
      </StyledWrapper>
    );
  }
}

export default withSnackbar(withUser(withQueryLoader(Sessions)));
