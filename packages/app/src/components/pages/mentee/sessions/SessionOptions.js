import React from 'react';
import styled from 'styled-components';
import SessionTimeframeTabs from 'components/shared/SessionTimeframeTabs';
import Select from 'components/shared/Select';
import { MenuItem } from '@material-ui/core';

const StyledWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  height: 10rem;
`;

const SessionOptions = ({ timeframe, type, onTimeframeChange, onTypeChange }) => (
  <StyledWrapper>
    <SessionTimeframeTabs selectedTimeframe={timeframe} onChange={onTimeframeChange} />
    <Select value={type} onChange={onTypeChange}>
      <MenuItem value="oneOnOne">One-On-One</MenuItem>
      <MenuItem value="ama">AMA Sessions</MenuItem>
    </Select>
  </StyledWrapper>
);

export default SessionOptions;
