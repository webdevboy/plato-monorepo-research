import AmaService from 'services/api/plato/ama/ama';
import BriefService from 'services/api/plato/brief/brief';
import MenteeService from 'services/api/plato/mentee/mentee';
import MentorService from 'services/api/plato/mentor/mentor';
import RelationshipService from 'services/api/plato/relationship/relationship';
import RequestService from 'services/api/plato/request/request';
import moment from 'moment';

/*
  For each of the mentee's AMA attendances, we need to retrieve the
  associated AMA, that AMA's mentors (and the mentor's stories), and
  the AMA's full list of attendances
*/
const getAdditionalAttendanceInfo = async attendances => {
  const amaPromises = attendances.map(attendance => AmaService.getAma(attendance.amaId));

  const amaAttendanceListPromises = attendances.map(attendance =>
    AmaService.getAttendances(attendance.amaId)
  );

  const amas = await Promise.all(amaPromises);

  const mentorPromises = amas.map(ama => MentorService.getMentor(ama.mentorId));
  const mentorStoriesPromises = amas.map(ama => MentorService.getStories(ama.mentorId));

  const amaAttendanceLists = await Promise.all(amaAttendanceListPromises);
  const mentors = await Promise.all(mentorPromises);
  const mentorStories = await Promise.all(mentorStoriesPromises);

  attendances.forEach((attendance, index) => {
    attendance._ama = amas[index]; // eslint-disable-line no-param-reassign
    attendance._ama._mentor = mentors[index]; // eslint-disable-line no-param-reassign
    attendance._ama._mentor._stories = mentorStories[index]; // eslint-disable-line no-param-reassign
    attendance._ama._attendanceList = amaAttendanceLists[index]; // eslint-disable-line no-param-reassign
  });
};

const getAdditionalRequestInfo = async requests => {
  const relationshipRequests = requests.filter(
    request => request.shareType === 'Long term relationship' && request.relationshipId
  );

  const requestsWithBriefs = requests.filter(request => request.briefId);

  const briefPromises = requestsWithBriefs.map(request => BriefService.getBrief(request.briefId));

  const futureRelationshipRequestPromises = relationshipRequests.map(request =>
    RelationshipService.getFutureRequests({
      relationshipId: request.relationshipId,
      scheduled: true,
      done: false,
      closed: false,
      error: false,
      scMentee: false,
      scMentor: false,
      matching: false,
    })
  );

  const mentorPromises = requests.map(request => MentorService.getMentor(request.mentorId));
  const mentorStoriesPromises = requests.map(request => MentorService.getStories(request.mentorId));

  const futureRelationshipRequestLists = await Promise.all(futureRelationshipRequestPromises);
  relationshipRequests.forEach((request, index) => {
    request._futureRelationshipRequests = futureRelationshipRequestLists[index]; // eslint-disable-line no-param-reassign
  });

  const briefs = await Promise.all(briefPromises);
  requestsWithBriefs.forEach((request, index) => {
    request._brief = briefs[index]; // eslint-disable-line no-param-reassign
  });

  const mentors = await Promise.all(mentorPromises);
  const mentorStories = await Promise.all(mentorStoriesPromises);
  requests.forEach((request, index) => {
    request._mentor = mentors[index];
    request._mentor._stories = mentorStories[index];
  });
};

export default {
  async getFutureRequests(menteeId, page) {
    const response = await MenteeService.getRequests({
      menteeId,
      done: false,
      scheduled: true,
      scMentee: false,
      scMentor: false,
      matching: false,
      error: false,
      closed: false,
      page,
      size: 5,
      firstOfRelationship: true,
    });

    response.result = response.result.filter(request => request.callScheduledAt > Date.now());
    await getAdditionalRequestInfo(response.result);
    response.result.sort(
      (requestA, requestB) => requestA.callScheduledAt - requestB.callScheduledAt
    );

    return response;
  },

  async getPastRequests(menteeId, page) {
    const response = await MenteeService.getRequests({
      menteeId,
      done: true,
      scheduled: false,
      scMentee: false,
      scMentor: false,
      matching: false,
      error: false,
      closed: false,
      page,
      size: 5,
      firstOfRelationship: true,
    });

    response.result = response.result.filter(request => request.callScheduledAt < Date.now());
    await getAdditionalRequestInfo(response.result);
    response.result.sort(
      (requestA, requestB) => requestB.callScheduledAt - requestA.callScheduledAt
    );

    return response;
  },

  async getFutureAmaAttendances(menteeId, page) {
    const response = await MenteeService.getAttendances({
      menteeId,
      pending: false,
      done: false,
      scheduled: true,
      error: false,
      closed: false,
      page,
      size: 5,
      minDate: moment().toISOString(true),
    });

    await getAdditionalAttendanceInfo(response.result);
    response.result.sort(
      (requestA, requestB) => requestA._ama.callScheduledAt - requestB._ama.callScheduledAt
    );

    return response;
  },

  async getPastAmaAttendances(menteeId, page) {
    const response = await MenteeService.getAttendances({
      menteeId,
      pending: false,
      done: true,
      scheduled: false,
      error: false,
      closed: false,
      page,
      size: 5,
    });

    await getAdditionalAttendanceInfo(response.result);
    response.result.sort(
      (requestA, requestB) => requestB._ama.callScheduledAt - requestA._ama.callScheduledAt
    );

    return response;
  },

  async getNextSession(menteeId) {
    const [nextOneOnOne] = (await this.getFutureRequests(menteeId, 0)).result;
    const [nextAma] = (await this.getFutureAmaAttendances(menteeId, 0)).result;

    if (nextOneOnOne) nextOneOnOne._type = 'oneOnOne';
    if (nextAma) nextAma._type = 'ama';

    if (nextOneOnOne && nextAma) {
      return nextOneOnOne.callScheduledAt < nextAma._ama.callScheduledAt ? nextOneOnOne : nextAma;
    }
    if (!nextOneOnOne) {
      return nextAma;
    }
    if (!nextAma) {
      return nextOneOnOne;
    }

    return null;
  },

  createRelationship({ menteeId, mentorId, nbOfWeeks }) {
    return MenteeService.createRelationship({
      menteeId,
      mentorId,
      nbOfWeeks,
    });
  },

  getMentorMenteeRequests({ mentorId, menteeId }) {
    return RequestService.getMentorMenteeRequests({
      mentorId,
      menteeId,
    }).then(({ result }) => result);
  },
};
