import React, { Component } from 'react';

import Button from 'components/shared/Button';
import { CALL_DONE_REQUEST_STATUS } from 'constants/RequestStatuses';
import { isRelationshipAvailable } from 'utils/relationship';
import sessionsService from 'components/pages/mentee/sessions/sessionsService';
import styled from 'styled-components';

const StyledButton = styled(Button)`
  display: flex;
  justify-content: space-between;
  width: calc(100% + 6rem);
  border-radius: 0;
  margin-top: auto;
  margin-left: -3rem;
  margin-bottom: -3rem;

  & .secondary {
    text-transform: initial;
  }
`;

class RelationshipButton extends Component {
  constructor(props) {
    super(props);

    this.state = {
      shouldDisplay: false,
    };
  }

  async componentDidMount() {
    const { mentee, mentor } = this.props;

    try {
      const requests = await sessionsService.getMentorMenteeRequests({
        mentorId: mentor.id,
        menteeId: mentee.id,
      });

      this.setState({
        shouldDisplay: isRelationshipAvailable(
          mentor,
          mentee,
          requests.some(({ status }) => status === CALL_DONE_REQUEST_STATUS)
        ),
      });
    } catch (error) {
      console.error(error);
    }
  }

  render() {
    const { className, onClick } = this.props;

    return this.state.shouldDisplay ? (
      <StyledButton variant="primary" className={className} onClick={onClick}>
        <span className="secondary">Available for a long-term mentorship!</span>
        <span>Learn More</span>
      </StyledButton>
    ) : null;
  }
}

export default styled(RelationshipButton)``;
