import React, { Component } from 'react';

import AmaInfo from 'components/shared/AmaInfo';
import Button from 'components/shared/Button';
import Card from 'components/shared/Card';
import DateTimeWithIcons from 'components/shared/DateTimeWithIcons';
import DaysUntil from 'components/shared/DaysUntil';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import IconLabel from 'components/shared/IconLabel';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import getProfilePictureThumbnail from 'utils/thumbnail';
import styled from 'styled-components';

const Body = styled.div`
  display: flex;

  & .section {
    display: flex;
    flex-direction: column;
    padding: 3rem;

    &.left {
      min-width: 30rem;
      max-width: 30rem;
      background: ${({ theme }) => theme.palette.polar};
      border-right: 1px solid ${({ theme }) => theme.palette.geyser};

      & .details {
        display: flex;
        flex-wrap: wrap;
        margin-bottom: 1.5rem;
      }

      & .cant-attend-button {
        margin-top: auto;
      }
    }

    &.right {
      & ${AmaInfo} {
        margin-bottom: 2rem;
      }

      & h4 {
        margin-bottom: 1rem;
        margin-top: auto;
      }

      & .story-link {
        display: flex;
        justify-content: space-between;
        align-items: center;
        cursor: pointer;

        & .name {
          transition: color ${({ theme }) => theme.transitions.default};

          &:hover {
            color: ${({ theme }) => theme.palette.cerulean};
          }
        }

        & .arrow {
          color: ${({ theme }) => theme.palette.cerulean};
        }
      }
    }
  }
`;

class AmaCard extends Component {
  render() {
    const {
      className,
      isEditable,
      onCancelClick,
      session: { _ama },
    } = this.props;

    return (
      <Card className={className}>
        <Body>
          <div className="left section">
            <DaysUntil timestamp={_ama.callScheduledAt} />

            <div className="details">
              <DateTimeWithIcons timestamp={_ama.callScheduledAt} />
              <IconLabel icon="user">
                {_ama.seatsScheduledNb} / {_ama.limitAttendances} attendees
              </IconLabel>
            </div>

            <Link to={`/ama/${_ama.id}`}>
              <Button variant="primary-link">View Details</Button>
            </Link>

            {isEditable && (
              <Button
                className="cant-attend-button"
                variant="secondary-link"
                onClick={onCancelClick}
              >
                I can&apos;t attend anymore
              </Button>
            )}
          </div>
          <div className="right section">
            <AmaInfo
              storyId={_ama.storyId}
              storyName={_ama.storyName}
              mentorId={_ama._mentor.id}
              mentorPictureUrl={getProfilePictureThumbnail(
                _ama._mentor.firstName,
                _ama._mentor.picture.thumbnailUrl
              )}
              mentorFullName={_ama._mentor.fullName}
              mentorTitle={_ama._mentor.title}
              mentorCompanyName={_ama._mentor.companyName}
              topics={_ama._mentor.storyTagNames}
            />

            {_ama._mentor._stories && _ama._mentor._stories.length > 0 && (
              <React.Fragment>
                <h4>
                  Read {_ama._mentor.firstName}
                  &apos;s Stories
                </h4>
                {_ama._mentor._stories
                  .filter(story => story.status === 'Published')
                  .slice(0, 2)
                  .map(story => (
                    <Link key={story.id} className="story-link" to={`/stories/${story.id}`}>
                      <p className="name">{story.name}</p>
                      <FontAwesomeIcon className="arrow" icon="arrow-right" />
                    </Link>
                  ))}
              </React.Fragment>
            )}
          </div>
        </Body>
      </Card>
    );
  }
}

AmaCard.propTypes = {
  className: PropTypes.string,
  isEditable: PropTypes.bool,
  onCancelClick: PropTypes.func,
  session: PropTypes.object.isRequired,
};

AmaCard.defaultProps = {
  className: null,
  isEditable: false,
  onCancelClick: () => {},
};

export default styled(AmaCard)``;
