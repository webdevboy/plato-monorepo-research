import Button from 'components/shared/Button';
import PlatoDialog from 'components/shared/PlatoDialog';
import React from 'react';
import styled from 'styled-components';
import textToHtml from 'utils/textToHtml';

const Body = styled.div`
  display: flex;
  flex-direction: column;
  padding: 3rem;
  width: 54rem;
  min-height: 30rem;

  & h1 {
    margin-bottom: 2.5rem;
  }

  & p {
    font-size: 1.6rem;
  }

  & .one-liner {
    margin-bottom: 1rem;
    font-weight: 700;
  }

  & .description {
    margin-bottom: 4rem;
  }

  & ${Button} {
    margin-top: auto;
    margin-left: auto;
  }
`;

const ShowMoreDialog = ({ open, onClose, oneLiner, description }) => (
  <PlatoDialog open={open} onClose={onClose}>
    <Body>
      <h1>Challenge</h1>

      <p className="one-liner">{oneLiner}</p>
      <div className="description">{textToHtml(description || '')}</div>

      <Button variant="primary" onClick={onClose}>
        Close
      </Button>
    </Body>
  </PlatoDialog>
);

export default ShowMoreDialog;
