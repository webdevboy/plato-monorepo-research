import React, { Component } from 'react';

import Button from 'components/shared/Button';
import Card from 'components/shared/Card';
import DateTimeWithIcons from 'components/shared/DateTimeWithIcons';
import DaysUntil from 'components/shared/DaysUntil';
import EditChallengeDialog from 'components/shared/EditChallengeDialog';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Link } from 'react-router-dom';
import PersonInfo from 'components/shared/PersonInfo';
import PropTypes from 'prop-types';
import RelationshipButton from './RelationshipButton';
import RelationshipDialog from './RelationshipDialog';
import RequestService from 'services/api/plato/request/request';
import ShowMoreDialog from './ShowMoreDialog';
import Truncate from 'components/shared/Truncate';
import getProfilePictureThumbnail from 'utils/thumbnail';
import moment from 'moment';
import styled from 'styled-components';
import { withQueryLoader } from 'components/context/QueryLoaderContext';
import { withSnackbar } from 'components/context/SnackbarContext';
import { withUser } from 'components/context/UserContext';

const Body = styled.div`
  display: flex;
  min-height: 45rem;

  & .section {
    display: flex;
    flex-direction: column;
    padding: 3rem;

    &.left {
      min-width: 30rem;
      max-width: 30rem;
      background: ${({ theme }) => theme.palette.polar};
      border-right: 1px solid ${({ theme }) => theme.palette.geyser};

      & ${DateTimeWithIcons} {
        margin-bottom: 3rem;
      }

      & .relationship {
        ${({ theme }) => `
          width: fit-content;
          padding: 0.5rem 1rem;
          font-size: 1rem;
          color: ${theme.palette.cerulean};
          border: 1px solid ${theme.palette.cerulean};
          margin-bottom: 1rem;
        `};
      }

      & .cant-attend,
      & .row > ${Button} {
        font-size: 1.2rem;
        font-weight: 700;
      }

      & .one-liner,
      & .description {
        line-height: initial;
        margin-bottom: 1rem;
      }

      & .one-liner {
        font-weight: 600;
      }

      & .row {
        display: flex;
        margin-bottom: 3rem;

        & ${Button} {
          padding: 0 1rem;
          border-left: 1px solid ${({ theme }) => theme.palette.geyser};
        }

        & ${Button}:first-child {
          padding-left: 0;
          border: none;
        }
      }

      & .meetings .header {
        font-weight: 600;
        margin-bottom: 1rem;
      }

      & .meeting {
        display: flex;
        justify-content: space-between;
        margin-top: 1rem;
        color: ${({ theme }) => theme.palette.osloGrey};

        & ${Button} {
          font-size: 1.2rem;
        }
      }

      & .cant-attend,
      & .meetings {
        margin-top: auto;
      }
    }

    &.right {
      & ${PersonInfo} {
        padding-bottom: 6rem;
      }

      & h4 {
        margin-bottom: 1rem;
      }

      & .stories {
        margin-top: auto;
        margin-bottom: 3rem;

        & .story-link {
          display: flex;
          justify-content: space-between;
          align-items: center;
          cursor: pointer;

          & .name {
            transition: color ${({ theme }) => theme.transitions.default};

            &:hover {
              color: ${({ theme }) => theme.palette.cerulean};
            }
          }

          & .arrow {
            color: ${({ theme }) => theme.palette.cerulean};
          }
        }
      }
    }
  }
`;

class OneOnOneCard extends Component {
  state = {
    showMoreDialogIsOpen: false,
    editDialogIsOpen: false,
    relationshipDialogIsOpen: false,
  };

  componentDidMount() {
    const { session, editRequestId } = this.props;

    if (parseInt(session.id, 10) === parseInt(editRequestId, 10)) {
      this.setState({
        editDialogIsOpen: true,
      });
    }
  }

  openShowMoreDialog = () => {
    this.setState({ showMoreDialogIsOpen: true });
  };

  closeShowMoreDialog = () => {
    this.setState({ showMoreDialogIsOpen: false });
  };

  openEditDialog = () => {
    this.setState({ editDialogIsOpen: true });
  };

  closeEditDialog = () => {
    this.setState({ editDialogIsOpen: false });
  };

  openRelationshipDialog = () => {
    this.setState({ relationshipDialogIsOpen: true });
  };

  closeRelationshipDialog = () => {
    this.setState({ relationshipDialogIsOpen: false });
  };

  onEditDialogSave = async (oneLiner, description) => {
    const { session, queryLoader, snackbar, onChange, clearRequestId } = this.props;

    queryLoader.showLoader();
    this.setState({
      saving: true,
    });

    let success = false;
    try {
      await RequestService.updateBrief({
        requestId: session.id,
        oneLiner,
        description,
      });

      success = true;
      clearRequestId();
    } catch (error) {
      console.error(error);
      snackbar.error('Something went wrong - sorry!');
    }

    this.setState({
      saving: false,
    });
    queryLoader.hideLoader();
    this.closeEditDialog();
    if (success) {
      onChange();
    }
  };

  render() {
    const { showMoreDialogIsOpen, editDialogIsOpen, relationshipDialogIsOpen, saving } = this.state;

    const {
      className,
      isEditable,
      onCancelClick,
      onChange,
      session: {
        _mentor,
        _futureRelationshipRequests,
        briefingDescription,
        briefingOneLiner,
        callScheduledAt,
        shareType,
      },
      userContext,
    } = this.props;

    const isRelationship = shareType === 'Long term relationship';

    return (
      <div className={className}>
        <Card>
          <Body>
            <div className="left section">
              {isRelationship && <h4 className="relationship">Long-Term Mentorship</h4>}
              <DaysUntil timestamp={callScheduledAt} />
              <DateTimeWithIcons timestamp={callScheduledAt} />

              <p className="one-liner">
                <Truncate maxLength={70}>{briefingOneLiner}</Truncate>
              </p>
              <p className="description">
                <Truncate maxLength={70}>{briefingDescription}</Truncate>
              </p>
              <div className="row">
                <Button variant="secondary-link" onClick={this.openShowMoreDialog}>
                  Show More
                </Button>
                <Button variant="primary-link" onClick={this.openEditDialog}>
                  Edit
                </Button>
              </div>

              {!isRelationship && isEditable && (
                <Button className="cant-attend" variant="secondary-link" onClick={onCancelClick}>
                  I can&apos;t attend anymore
                </Button>
              )}

              {isRelationship &&
                _futureRelationshipRequests &&
                _futureRelationshipRequests.length > 0 && (
                  <div className="meetings">
                    <p className="header">Your meetings</p>
                    {_futureRelationshipRequests
                      .sort(
                        (requestA, requestB) => requestA.callScheduledAt - requestB.callScheduledAt
                      )
                      .slice(0, 3)
                      .map(request => (
                        <div key={request.id} className="meeting">
                          <p className="meeting-date">
                            {moment(request.callScheduledAt).format('MMMM Do YYYY h:mm A')}
                          </p>
                          <Button variant="secondary-link" onClick={onCancelClick}>
                            Cancel
                          </Button>
                        </div>
                      ))}
                  </div>
                )}
            </div>
            <div className="right section">
              <PersonInfo
                fullName={_mentor.fullName}
                title={_mentor.title}
                thumbnailUrl={getProfilePictureThumbnail(
                  _mentor.firstName,
                  _mentor.picture.thumbnailUrl
                )}
                companyName={_mentor.companyName}
                tags={_mentor.storyTagNames}
                about={_mentor.aboutMyself}
                linkUrl={`/mentors?mentor_id=${_mentor.id}`}
                linkedin={_mentor.linkedin}
              />

              {_mentor._stories.length > 0 && (
                <div className="stories">
                  <h4>
                    Read {_mentor.firstName}
                    &apos;s Stories
                  </h4>
                  {_mentor._stories
                    .filter(story => story.status === 'Published')
                    .slice(0, 2)
                    .map(story => (
                      <Link key={story.id} className="story-link" to={`/stories/${story.id}`}>
                        <p className="name">{story.name}</p>
                        <FontAwesomeIcon className="arrow" icon="arrow-right" />
                      </Link>
                    ))}
                </div>
              )}
              {!isRelationship && (
                <RelationshipButton
                  onClick={this.openRelationshipDialog}
                  mentor={_mentor}
                  mentee={userContext.mentee}
                />
              )}
            </div>
          </Body>
        </Card>
        <ShowMoreDialog
          open={showMoreDialogIsOpen}
          onClose={this.closeShowMoreDialog}
          oneLiner={briefingOneLiner}
          description={briefingDescription}
        />
        <RelationshipDialog
          open={relationshipDialogIsOpen}
          onClose={this.closeRelationshipDialog}
          triggerRefresh={onChange}
          mentor={_mentor}
        />
        <EditChallengeDialog
          oneLiner={briefingOneLiner}
          description={briefingDescription}
          open={editDialogIsOpen}
          saving={saving}
          onSave={this.onEditDialogSave}
          onClose={this.closeEditDialog}
        />
      </div>
    );
  }
}

OneOnOneCard.propTypes = {
  className: PropTypes.string,
  isEditable: PropTypes.bool,
  onCancelClick: PropTypes.func,
  onChange: PropTypes.func,
  queryLoader: PropTypes.object.isRequired,
  session: PropTypes.object.isRequired,
  snackbar: PropTypes.object.isRequired,
  userContext: PropTypes.object.isRequired,
};

OneOnOneCard.defaultProps = {
  className: null,
  isEditable: false,
  onCancelClick: () => {},
  onChange: () => {},
};

export default styled(withUser(withQueryLoader(withSnackbar(OneOnOneCard))))``;
