import Button from 'components/shared/Button';
import Card from 'components/shared/Card';
import { Link } from 'react-router-dom';
import React from 'react';
import styled from 'styled-components';

const StyledCard = styled(Card)`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  min-height: 30rem;
  border: 1px solid ${props => props.theme.palette.geyser};
  box-shadow: none;
  padding: 3rem;
  width: 100%;

  & h1,
  & .emoji {
    margin-bottom: 1rem;
  }

  & h2 {
    margin-bottom: 2rem;
  }

  & .emoji {
    font-size: 3.4rem;
  }

  & .links {
    display: flex;

    & .mentors-link {
      margin-right: 3rem;
    }
  }
`;

// TODO:  This should be broken into two components
const NoSessionsCard = ({ timeframe, type, className }) => {
  const typeStr = type === 'ama' ? 'AMA' : 'one-on-one';

  return (
    <StyledCard className={className}>
      {timeframe === 'past' && (
        <React.Fragment>
          <span className="emoji" role="img" aria-label="Hmm">
            🤞
          </span>

          <h2>Looks like you haven&apos;t had any {typeStr} sessions... yet!</h2>
        </React.Fragment>
      )}
      {timeframe === 'future' && (
        <React.Fragment>
          <span className="emoji" role="img" aria-label="Hmm">
            😔
          </span>

          <h2>No upcoming {typeStr} sessions found.</h2>
        </React.Fragment>
      )}
      {type === 'oneOnOne' && (
        <div className="links">
          <Link to="/mentors" className="mentors-link">
            <Button variant="primary">Find a Mentor</Button>
          </Link>
          <Link to="/stories">
            <Button variant="secondary">Read Stories</Button>
          </Link>
        </div>
      )}
      {type === 'ama' && (
        <Link to="/ama">
          <Button variant="primary">Browse AMAs</Button>
        </Link>
      )}
    </StyledCard>
  );
};

export default NoSessionsCard;
