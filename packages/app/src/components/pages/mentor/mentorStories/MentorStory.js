import { Link, withRouter } from 'react-router-dom';
import React, { Component } from 'react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Layout from 'components/shared/Layout';
import PageLoadingSpinner from 'components/shared/PageLoadingSpinner';
import StoryService from 'services/api/plato/story/story';
import queryString from 'query-string';
import styled from 'styled-components';
import textToHtml from 'utils/textToHtml';
import { trackMentorStoryRead } from 'utils/analytics';
import { withSnackbar } from 'components/context/SnackbarContext';
import { withUser } from 'components/context/UserContext';

const ViewAllStoriesLink = styled(Link)`
  margin-bottom: 2.8rem;
  cursor: pointer;
`;

const ViewAllStories = styled.h4`
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 11.5rem;
  transition: all ${props => props.theme.transitions.default};
  color: ${props => props.theme.palette.osloGrey};

  &:hover {
    color: ${props => props.theme.palette.ebonyClay};
  }
`;

const StoryTitle = styled.h1`
  font-size: 3.4rem;
  font-weight: 600;
  margin-bottom: 1.9rem;
`;

const BodyText = styled.p`
  font-size: 1.6rem;
`;

const Teaser = styled(BodyText)`
  margin-bottom: 3rem;
`;

const ParagraphSection = styled.div`
  & h1 {
    margin: 3rem 0 2rem 0;
    font-weight: 600;
  }

  & ${BodyText} {
    line-height: 2.4rem;
  }
`;

const StyledLayout = styled(Layout)`
  margin-top: 6.9rem;
  && > div {
    width: 78rem;
  }

  & .paragraphs {
    margin-bottom: 8rem;
  }
`;

class Story extends Component {
  constructor(props) {
    super(props);
    const {
      location,
      history,
      match: { params },
    } = props;

    // Needed to support link from legacy email and Slack campaign
    const { source, story_id: storyId = params.storyId } = queryString.parse(location.search);
    if (!storyId) {
      history.replace('/mentor_stories');
    }

    this.state = { loading: true, story: undefined, storyId, source };
  }

  async componentDidMount() {
    const {
      snackbar,
      userContext: {
        user: { uuid, email, mentorId },
      },
    } = this.props;
    const { source, storyId } = this.state;
    this.setState({ loading: true });
    try {
      const story = await StoryService.getStory(storyId);
      this.setState({ story });
    } catch (error) {
      console.error(error);
      snackbar.error('Something went wrong - sorry!');
    }
    this.setState({ loading: false });
    trackMentorStoryRead({
      uuid,
      email,
      mentorId,
      source,
      storyId,
    });
  }

  render() {
    const { loading, story } = this.state;

    if (loading) {
      return <PageLoadingSpinner />;
    }

    return (
      <StyledLayout>
        <ViewAllStoriesLink to="/mentor_stories">
          <ViewAllStories>
            <FontAwesomeIcon icon="arrow-left" />
            View All Stories
          </ViewAllStories>
        </ViewAllStoriesLink>
        <StoryTitle>{story.name}</StoryTitle>
        <Teaser>{story.teaser}</Teaser>
        <div className="paragraphs">
          {story.storyParagraphs.map(paragraph => (
            <ParagraphSection key={paragraph.id}>
              <h1>{paragraph.title}</h1>
              <BodyText>{textToHtml(paragraph.body)}</BodyText>
            </ParagraphSection>
          ))}
        </div>
      </StyledLayout>
    );
  }
}

export default withSnackbar(withUser(withRouter(Story)));
