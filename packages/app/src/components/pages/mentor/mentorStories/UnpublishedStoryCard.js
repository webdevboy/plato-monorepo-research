import React, { Component } from 'react';

import Button from 'components/shared/Button';
import Card from 'components/shared/Card';
import StatusChip from 'components/shared/StatusChip';
import TagList from 'components/shared/TagList';
import Truncate from 'components/shared/Truncate';
import styled from 'styled-components';

const StyledWrapper = styled.div`
  & ${Card} {
    cursor: pointer;

    transition: border ${props => props.theme.transitions.default},
      box-shadow ${props => props.theme.transitions.default};
  }

  & ${Card}:hover {
    cursor: pointer;
    border: 1px solid ${props => props.theme.palette.cerulean};
    box-shadow: ${props => props.theme.shadows[2]};
  }
`;

const Body = styled.div`
  display: flex;
  flex-direction: column;

  & ${TagList} {
    margin-bottom: 1rem;
  }

  & .top {
    display: flex;
    flex-direction: column;

    & .row {
      display: flex;
      justify-content: space-between;
      align-items: center;
      margin-bottom: 1.5rem;
    }
  }

  & .bottom {
    display: flex;
    align-items: center;
    background: ${props => props.theme.palette.polar};
    color: ${props => props.theme.palette.cadetBlue};
    border-top: 1px solid ${props => props.theme.palette.geyser};
    justify-content: space-between;

    & ${Button} {
      flex-shrink: 0;
    }

    &.primary {
      background-color: ${props => props.theme.palette.cerulean};
      color: ${props => props.theme.palette.white};
    }
  }

  & .section {
    padding: 3rem;
  }

  & .story-name {
    margin-bottom: 1rem;
  }

  & .teaser {
    line-height: 1.5;
    margin-bottom: 1rem;
  }
`;

class UnpublishedStoryCard extends Component {
  onApproveClick = event => {
    event.stopPropagation();

    this.props.onApproveClick();
  };

  render() {
    const { story, onClick, className } = this.props;

    return (
      <StyledWrapper className={className} onClick={onClick}>
        <Card className={className}>
          <Body>
            <div className="top section">
              <div className="row">
                {story.status === 'Validated' && <StatusChip title="Story Validated" />}
                {story.status === 'Validation in progress' && (
                  <React.Fragment>
                    <StatusChip title="Waiting for your approval" />
                    <a
                      href={story.link}
                      rel="noopener noreferrer"
                      onClick={event => event.stopPropagation()}
                    >
                      <Button variant="primary-link">Edit</Button>
                    </a>
                  </React.Fragment>
                )}
              </div>
              <h1 className="story-name">{story.name}</h1>
              <TagList tags={story.tags.map(tag => tag.name)} />
              <p className="teaser">
                <Truncate maxLength={230}>{story.teaser}</Truncate>
              </p>
            </div>
            {story.status === 'Validated' && (
              <div className="bottom section primary">
                <p>
                  Last check from Plato Team. Once reviewed, your story will be published.
                  <br />
                  After published, you won’t be able to edit your story
                </p>
              </div>
            )}
            {story.status === 'Validation in progress' && (
              <div className="bottom section">
                <p>
                  You can edit your story as much as you want. Once you consider
                  <br />
                  your story ready, you can submit it to be reviewed by Plato&apos;s team.
                </p>
                <Button variant="primary" onClick={this.onApproveClick}>
                  Approve This Story
                </Button>
              </div>
            )}
          </Body>
        </Card>
      </StyledWrapper>
    );
  }
}

export default styled(UnpublishedStoryCard)``;
