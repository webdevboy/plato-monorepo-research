import React, { Component } from 'react';

import Button from 'components/shared/Button';
import EditStoryDialog from './EditStoryDialog';
import Hero from 'components/shared/Hero';
import Layout from 'components/shared/Layout';
import MentorService from 'services/api/plato/mentor/mentor';
import PageLoadingSpinner from 'components/shared/PageLoadingSpinner';
import PostStoryDialog from 'components/shared/PostStoryDialog';
import PublishedStoryCard from './PublishedStoryCard';
import StoryService from 'services/api/plato/story/story';
import UnpublishedStoryCard from './UnpublishedStoryCard';
import ValidateStoryDialog from './ValidateStoryDialog';
import VisibilityInfoDialog from './VisibilityInfoDialog';
import styled from 'styled-components';
import { trackStoryValidated } from 'utils/analytics';
import { withQueryLoader } from 'components/context/QueryLoaderContext';
import { withRouter } from 'react-router-dom';
import { withSnackbar } from 'components/context/SnackbarContext';
import { withUser } from 'components/context/UserContext';

const StyledWrapper = styled.div`
  & ${Layout} {
    position: relative;
    top: 6rem;

    & .post-story-button {
      align-self: flex-end;
    }

    & ${PublishedStoryCard}, & ${UnpublishedStoryCard} {
      margin-bottom: 3rem;
    }
  }
`;

const Header = styled.h3`
  margin-bottom: 1.5rem;
  color: ${props => props.theme.palette.cadetBlue};

  &:nth-of-type(1) {
    color: white;
  }
`;

class MentorStories extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      saving: false,
      stories: [],
      postNewStoryDialogIsOpen: false,
      visibilityInfoDialogIsOpen: false,
      validateStoryDialogIsOpen: false,
      editStoryDialogIsOpen: false,
      dialogStory: undefined,
    };
  }

  async componentDidMount() {
    this.setState({
      loading: true,
    });

    try {
      await this.getStories();
    } catch (error) {
      console.error(error);
      this.props.snackbar.error('Something went wrong - sorry!');
    }

    this.setState({
      loading: false,
    });
  }

  toggleVisibility = async story => {
    const { queryLoader, snackbar } = this.props;

    queryLoader.showLoader();

    try {
      await StoryService.updateStory({
        id: story.id,
        tagIds: story.tags.map(tag => ({ id: tag.id })),
        hideOnWebsite: !story.hideOnWebsite,
        challenges: story.challenges.map(challenge => ({ id: challenge.id })),
        usagePrevented: story.usagePrevented,
        companyType: story.companyType,
        actionability: story.actionability,
        authenticity: story.authenticity,
        obviousness: story.obviousness,
      });

      snackbar.success(story.hideOnWebsite ? 'Story is now visible' : 'Story is now hidden');

      await this.getStories();
    } catch (error) {
      console.error(error);
      snackbar.error('Something went wrong - sorry!');
    }

    queryLoader.hideLoader();
  };

  getStories = async () => {
    const {
      userContext: {
        user: { mentorId },
      },
    } = this.props;
    const stories = await MentorService.getStories(mentorId);

    this.setState({
      stories,
    });
  };

  openPostNewStoryDialog = () => {
    this.setState({
      postNewStoryDialogIsOpen: true,
    });
  };

  closePostNewStoryDialog = () => {
    this.setState({
      postNewStoryDialogIsOpen: false,
    });
  };

  openVisibilityInfoDialog = () => {
    this.setState({
      visibilityInfoDialogIsOpen: true,
    });
  };

  closeVisibilityInfoDialog = () => {
    this.setState({
      visibilityInfoDialogIsOpen: false,
    });
  };

  openValidateStoryDialog = story => {
    this.setState({
      validateStoryDialogIsOpen: true,
      dialogStory: story,
    });
  };

  closeValidateStoryDialog = () => {
    this.setState({
      validateStoryDialogIsOpen: false,
      dialogStory: undefined,
    });
  };

  openEditStoryDialog = story => {
    this.setState({
      editStoryDialogIsOpen: true,
      dialogStory: story,
    });
  };

  closeEditStoryDialog = () => {
    this.setState({
      editStoryDialogIsOpen: false,
      dialogStory: undefined,
    });
  };

  validateStory = async () => {
    const {
      queryLoader,
      snackbar,
      userContext: {
        user: { uuid, email, mentorId },
      },
    } = this.props;

    queryLoader.showLoader();
    this.setState({
      saving: true,
    });

    try {
      const storyId = this.state.dialogStory.id;
      await StoryService.validateStory(storyId);

      this.closeValidateStoryDialog();

      await this.getStories();
      snackbar.success('Thank you!');

      trackStoryValidated({
        uuid,
        email,
        mentorId,
        storyId,
      });
    } catch (error) {
      console.error(error);
      snackbar.error('Something went wrong - sorry!');
    }

    queryLoader.hideLoader();
    this.setState({
      saving: false,
    });
  };

  render() {
    const {
      loading,
      stories,
      dialogStory,
      postNewStoryDialogIsOpen,
      visibilityInfoDialogIsOpen,
      validateStoryDialogIsOpen,
      editStoryDialogIsOpen,
      saving,
    } = this.state;

    const { history } = this.props;

    const publishedStories = stories.filter(story => story.status === 'Published');

    const nonPublishedStories = stories.filter(
      story => story.status === 'Validation in progress' || story.status === 'Validated'
    );

    return (
      <StyledWrapper>
        <Hero />
        {loading && <PageLoadingSpinner />}
        {!loading && (
          <React.Fragment>
            <Layout width="780">
              <Button
                className="post-story-button"
                variant="primary"
                onClick={this.openPostNewStoryDialog}
              >
                Post a New Story
              </Button>

              {nonPublishedStories.length > 0 && <Header>In progress stories</Header>}

              {nonPublishedStories.map(story => (
                <UnpublishedStoryCard
                  key={story.id}
                  story={story}
                  onClick={() => history.push(`/mentor_story/${story.id}`)}
                  onApproveClick={() => this.openValidateStoryDialog(story)}
                />
              ))}

              {publishedStories.length > 0 && <Header>Published Stories</Header>}
              {publishedStories.map(story => (
                <PublishedStoryCard
                  key={story.id}
                  story={story}
                  visible={!story.hideOnWebsite}
                  onVisibilityToggle={() => this.toggleVisibility(story)}
                  onInfoClick={this.openVisibilityInfoDialog}
                  onClick={() => history.push(`/mentor_story/${story.id}`)}
                />
              ))}
            </Layout>

            <PostStoryDialog
              open={postNewStoryDialogIsOpen}
              onClose={this.closePostNewStoryDialog}
              location="stories"
            />

            <VisibilityInfoDialog
              open={visibilityInfoDialogIsOpen}
              onClose={this.closeVisibilityInfoDialog}
            />

            <ValidateStoryDialog
              open={validateStoryDialogIsOpen}
              onClose={this.closeValidateStoryDialog}
              onSubmit={this.validateStory}
              saving={saving}
            />

            <EditStoryDialog
              open={editStoryDialogIsOpen}
              onClose={this.closeEditStoryDialog}
              story={dialogStory}
              saving={saving}
            />
          </React.Fragment>
        )}
      </StyledWrapper>
    );
  }
}

export default withSnackbar(withQueryLoader(withUser(withRouter(MentorStories))));
