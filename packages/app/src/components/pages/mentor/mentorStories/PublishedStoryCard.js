import React, { Component } from 'react';

import Card from 'components/shared/Card';
import Checkbox from 'components/shared/Checkbox';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import StatusChip from 'components/shared/StatusChip';
import TagList from 'components/shared/TagList';
import Truncate from 'components/shared/Truncate';
import styled from 'styled-components';

const StyledWrapper = styled.div`
  & ${Card} {
    cursor: pointer;

    transition: border ${props => props.theme.transitions.default},
      box-shadow ${props => props.theme.transitions.default};
  }

  & ${Card}:hover {
    cursor: pointer;
    border: 1px solid ${props => props.theme.palette.cerulean};
    box-shadow: ${props => props.theme.shadows[2]};
  }
`;

const Body = styled.div`
  display: flex;
  flex-direction: column;
  padding: 3rem;

  & ${TagList} {
    margin-bottom: 1rem;
  }

  & .row {
    display: flex;
    justify-content: space-between;
    margin-bottom: 1.5rem;

    & .visible-container {
      display: flex;
      justify-content: space-between;
      align-items: center;
      width: 10rem;
    }
  }

  & .story-name {
    margin-bottom: 1rem;
  }

  & .teaser {
    line-height: 1.5;
    margin-bottom: 1rem;
  }
`;

const StyledIcon = styled(FontAwesomeIcon)`
  font-size: 1.5rem;
  color: ${props => props.theme.palette.cerulean};
`;

class PublishedStoryCard extends Component {
  onCheckboxClick = (name, checked, event) => {
    event.stopPropagation();

    this.props.onVisibilityToggle();
  };

  onInfoClick = event => {
    event.stopPropagation();

    this.props.onInfoClick();
  };

  render() {
    const { story, visible, onClick, className } = this.props;

    return (
      <StyledWrapper className={className} onClick={onClick}>
        <Card>
          <Body>
            <div className="row">
              <StatusChip title={story.status} />
              <div className="visible-container">
                <StyledIcon icon="info-circle" onClick={this.onInfoClick} />
                <h4>Visible</h4>
                <Checkbox
                  checked={visible}
                  disabled={story.internalOnly}
                  onChange={this.onCheckboxClick}
                />
              </div>
            </div>

            <h1 className="story-name">{story.name}</h1>
            <TagList tags={story.tags.map(tag => tag.name)} />
            <p className="teaser">
              <Truncate maxLength={230}>{story.teaser}</Truncate>
            </p>
          </Body>
        </Card>
      </StyledWrapper>
    );
  }
}

export default styled(PublishedStoryCard)``;
