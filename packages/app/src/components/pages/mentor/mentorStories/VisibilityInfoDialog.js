import Button from 'components/shared/Button';
import { Link } from 'react-router-dom';
import PlatoDialog from 'components/shared/PlatoDialog';
import React from 'react';
import styled from 'styled-components';
import { withUser } from 'components/context/UserContext';

const Body = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 3rem;
  padding-top: 6rem;
  text-align: center;
  width: 54rem;

  & h1 {
    margin-bottom: 2rem;
  }

  & .text-container {
    padding-left: 3rem;
    padding-right: 3rem;
    margin-bottom: 1rem;
  }

  & p {
    margin-bottom: 2rem;
  }

  & .manage-profile {
    margin-bottom: 2rem;
  }
`;

const VisibilityInfoDialog = props => (
  <PlatoDialog open={props.open} onClose={props.onClose}>
    <Body>
      <h1>&quot;Visible&quot; Story</h1>
      <div className="text-container">
        <p>
          When a published story is visible it means that this story will be displayed on your
          public profile.
        </p>
        <p>
          Feel free to hide stories by unticking the &quot;visible&quot; checkbox. You can also
          choose to hide your profile from your profile page.
        </p>
      </div>
      <Link to="/mentor_profile" className="manage-profile">
        <Button variant="primary">Manage My Profile</Button>
      </Link>
      <a
        href={`https://community.platohq.com/mentors/${
          props.userContext.user.mentorId
        }?utm_source=sharing&utm_medium=link&utm_campaign=launchprofile4`}
        rel="noopener noreferrer"
        target="_blank"
      >
        <Button variant="secondary-link">
          <h4>Go To My Public Profile</h4>
        </Button>
      </a>
    </Body>
  </PlatoDialog>
);

export default withUser(VisibilityInfoDialog);
