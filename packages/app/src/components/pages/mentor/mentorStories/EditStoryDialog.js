import React, { Component } from 'react';

import Button from 'components/shared/Button';
import PlatoDialog from 'components/shared/PlatoDialog';
import styled from 'styled-components';
import { trackStoryEdited } from 'utils/analytics';
import { withUser } from 'components/context/UserContext';

const Body = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 3rem;
  padding-top: 6rem;
  width: 54rem;
  text-align: center;

  & h1 {
    margin-bottom: 2rem;
  }

  & p {
    margin-bottom: 4rem;
  }
`;

class EditStoryDialog extends Component {
  handleClick = () => {
    const {
      story: { id: storyId },
      userContext: {
        user: { uuid, email, mentorId },
      },
    } = this.props;
    trackStoryEdited({ uuid, email, mentorId, storyId });
  };

  render() {
    const { open, onClose, story, onSubmit } = this.props;
    return (
      <PlatoDialog open={open} onClose={onClose}>
        <Body>
          <h1>Edit a Story</h1>
          <p>
            When you edit a story it can take up to 1h to be taken into account by us, don&apos;t
            worry if you don&apos;t directly see your changes here!{' '}
            <span role="img" aria-label="Smiley">
              🙂
            </span>
          </p>
          {story && (
            <a href={story.link} rel="noopener noreferrer" onClick={this.handleClick}>
              <Button variant="primary" onClick={onSubmit}>
                Edit
              </Button>
            </a>
          )}
        </Body>
      </PlatoDialog>
    );
  }
}

export default withUser(EditStoryDialog);
