import React, { Component } from 'react';

import Button from 'components/shared/Button';
import PlatoDialog from 'components/shared/PlatoDialog';
import styled from 'styled-components';

const Body = styled.div`
  display: flex;
  flex-direction: column;
  padding: 3rem;
  width: 54rem;

  & h1 {
    margin-bottom: 1rem;
  }

  & p {
    margin-bottom: 2rem;
  }

  & ${Button} {
    align-self: flex-end;
  }
`;

class ValidateStoryDialog extends Component {
  render() {
    return (
      <PlatoDialog open={this.props.open} onClose={this.props.onClose}>
        <Body>
          <h1>Are you sure you want to approve this story?</h1>
          <p>
            Once you consider your story ready, you can submit it to be reviewed by Plato&apos;s
            team. After it is approved, you won&apos;t be able to edit the story.
          </p>
          <Button variant="primary" onClick={this.props.onSubmit} disabled={this.props.saving}>
            Yes
          </Button>
        </Body>
      </PlatoDialog>
    );
  }
}

export default ValidateStoryDialog;
