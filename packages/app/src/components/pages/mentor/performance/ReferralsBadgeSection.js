import React, { Component } from 'react';

import Badge from './Badge';
import BadgeRow from './BadgeRow';
import BadgeSectionHeader from './BadgeSectionHeader';
import Button from 'components/shared/Button';
import InviteMentorDialog from './InviteMentorDialog';
import LearnMoreDialog from './LearnMoreDialog';
import { trackPerformanceLearnMore } from 'utils/analytics';
import { withSnackbar } from 'components/context/SnackbarContext';
import { withTheme } from 'styled-components';
import { withUser } from 'components/context/UserContext';

class ReferralsBadgeSection extends Component {
  constructor(props) {
    super(props);

    const activeImageUrl =
      '//s3.amazonaws.com/appforest_uf/f1529561724033x332446608226746300/badge-referal.png';
    const inactiveImageUrl =
      '//s3.amazonaws.com/appforest_uf/f1529561755868x109245120547711840/badge-referal-copy.png';

    this.badgeMeta = {
      title: 'Referrals',
      activeColor: props.theme.palette.fuelYellow,
      badges: [
        {
          name: 'Referral_1',
          description: '2 New Mentors Invited',
          activeImageUrl,
          inactiveImageUrl,
        },
        {
          name: 'Referral_2',
          description: '5 New Mentors Invited',
          activeImageUrl,
          inactiveImageUrl,
        },
      ],
    };

    this.state = {
      learnMoreDialogIsOpen: false,
      inviteMentorDialogIsOpen: false,
    };
  }

  openLearnMoreDialog = () => {
    this.setState({
      learnMoreDialogIsOpen: true,
    });

    const {
      userContext: {
        user: { uuid, email, mentorId },
      },
    } = this.props;
    trackPerformanceLearnMore({
      uuid,
      email,
      mentorId,
      infoType: 'referral',
    });
  };

  closeLearnMoreDialog = () => {
    this.setState({
      learnMoreDialogIsOpen: false,
    });
  };

  openInviteMentorDialog = () => {
    this.props.snackbar.info(`This feature isn't implemented yet - sorry!`);
    // this.setState({
    //   inviteMentorDialogIsOpen: true,
    // });
  };

  closeInviteMentorDialog = () => {
    this.setState({
      inviteMentorDialogIsOpen: false,
    });
  };

  getBadges = () => {
    const { mentorBadgeIds } = this.props;
    const { badges, activeColor } = this.badgeMeta;

    return badges.map(badge => (
      <Badge
        key={badge.name}
        activeColor={activeColor}
        activeImageUrl={badge.activeImageUrl}
        inactiveImageUrl={badge.inactiveImageUrl}
        mentorBadgeIds={mentorBadgeIds}
        badgeName={badge.name}
        description={badge.description}
      />
    ));
  };

  render() {
    const { learnMoreDialogIsOpen, inviteMentorDialogIsOpen } = this.state;

    const badgeRow = <BadgeRow badges={this.getBadges()} sequential />;

    return (
      <React.Fragment>
        <BadgeSectionHeader
          title={this.badgeMeta.title}
          actions={[
            <Button
              variant="primary-link"
              key="invite-mentor"
              onClick={this.openInviteMentorDialog}
            >
              Invite Mentor
            </Button>,
            <Button variant="primary-link" key="learn-more" onClick={this.openLearnMoreDialog}>
              Learn More
            </Button>,
          ]}
        />

        {badgeRow}

        <LearnMoreDialog
          open={learnMoreDialogIsOpen}
          onClose={this.closeLearnMoreDialog}
          title={this.badgeMeta.title}
          description="Plato is all about our community. The larger it grows, the bigger our impact on the engineering leadership community will be. If you believe in our mission (you wouldn’t be here if you didn’t already, right?!), consider introducing us to fellow engineering leaders you admire or current peers, friends, ex-bosses… We’d love to meet them! 🙂"
          badgeRow={badgeRow}
          callToAction={
            <Button variant="primary" onClick={this.openInviteMentorDialog}>
              Make an Intro
            </Button>
          }
        />

        <InviteMentorDialog
          open={inviteMentorDialogIsOpen}
          onClose={this.closeInviteMentorDialog}
        />
      </React.Fragment>
    );
  }
}

export default withTheme(withUser(withSnackbar(ReferralsBadgeSection)));
