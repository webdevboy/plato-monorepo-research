import React, { Component } from 'react';
import styled from 'styled-components';
import PerformanceCard from './PerformanceCard';
import Rating from './Rating';

const Content = styled.div`
  display: flex;
  flex-direction: column;
`;

class ImprovementsCard extends Component {
  render() {
    const {
      loading,
      noShows,
      lastMinuteDeclines,
      cancellations,
      averageNoShows,
      averageLastMinuteDeclines,
      averageCancellations,
    } = this.props;

    return (
      <PerformanceCard title="Improvements" timeframeText="On Last 8 Calls" loading={loading}>
        <Content>
          <Rating
            rating={noShows}
            average={averageNoShows}
            title="No Show"
            description="No shows are when you leave the mentee hangin’ and don’t show up for a session. We all have busy schedules, so while we know things happen, we want to avoid this. If you can’t make a session, reschedule."
          />
          <Rating
            rating={lastMinuteDeclines}
            average={averageLastMinuteDeclines}
            title="Last Minute Declines"
            description="Last minute declines are when you cancel less than two days before a scheduled session.  Since mentees prepare a lot for these calls, and count on you, we want to keep these to a minimum."
          />
          <Rating
            rating={cancellations}
            average={averageCancellations}
            title="Cancellations"
            description="The total number of calls you decline. This number still includes rescheduled calls."
          />
        </Content>
      </PerformanceCard>
    );
  }
}

export default ImprovementsCard;
