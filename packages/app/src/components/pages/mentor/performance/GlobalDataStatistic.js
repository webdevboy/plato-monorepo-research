import React from 'react';
import styled from 'styled-components';

const StyledWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  text-align: center;
  padding: 0rem 2rem 0.5rem 2rem;
  flex-grow: 1;

  &:not(:last-child) {
    border-right: 1px solid ${({ theme }) => theme.palette.geyser};
  }

  &:first-child {
    padding-left: 0;
  }

  &:last-child {
    padding-right: 0;
  }

  & .number {
    font-size: 3.4rem;
    font-weight: bold;
    margin-bottom: 2rem;
  }

  & .label {
    line-height: 1.43;
    width: 7.5rem;
  }
`;

const GlobalDataStatistic = ({ number, label, ...others }) => (
  <StyledWrapper {...others}>
    <p className="number">{number}</p>
    <p className="label">{label}</p>
  </StyledWrapper>
);

export default styled(GlobalDataStatistic)``;
