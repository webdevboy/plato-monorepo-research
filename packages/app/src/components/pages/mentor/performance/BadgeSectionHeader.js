import React from 'react';
import styled from 'styled-components';

const StyledWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding-bottom: 0.25rem;
  margin-bottom: 2rem;
  border-bottom: 1px solid ${({ theme }) => theme.palette.geyser};

  & .actions {
    display: flex;
    align-items: center;

    & > * {
      padding: 0 1rem;

      &:first-child {
        padding-left: 0;
      }

      &:last-child {
        padding-right: 0;
      }

      &:not(:last-child) {
        border-right: 1px solid ${({ theme }) => theme.palette.geyser};
      }
    }
  }
`;

const BadgeSectionHeader = ({ title, actions }) => (
  <StyledWrapper>
    <h1>{title}</h1>

    <div className="actions">{actions}</div>
  </StyledWrapper>
);

export default BadgeSectionHeader;
