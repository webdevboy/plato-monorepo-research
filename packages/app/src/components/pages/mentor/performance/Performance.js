import React, { Component } from 'react';

import EmptyStateCard from 'components/shared/EmptyStateCard';
import Hero from 'components/shared/Hero';
import Layout from 'components/shared/Layout';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { withUser } from 'components/context/UserContext';
import StatisticsColumn from './StatisticsColumn';
import PageHeader from './PageHeader';
import BadgesColumn from './BadgesColumn';

const StyledWrapper = styled.div`
  ${Layout} {
    position: relative;
    top: 6rem;
  }

  ${EmptyStateCard} {
    margin-top: 12rem;
  }

  .columns {
    margin-top: 12rem;
    display: flex;

    > .left {
      width: 47.5rem;
      padding-right: 11.5rem;
    }

    > .right {
      flex-grow: 1;
    }
  }
`;

class Performance extends Component {
  render() {
    const {
      userContext: {
        user: { mentorId },
      },
    } = this.props;
    return (
      <StyledWrapper>
        <Hero height={344} />
        <Layout width={1025}>
          <PageHeader />
          <div className="columns">
            <div className="left">
              <StatisticsColumn mentorId={mentorId} />
            </div>
            <div className="right">
              <BadgesColumn mentorId={mentorId} />
            </div>
          </div>
        </Layout>
      </StyledWrapper>
    );
  }
}

Performance.propTypes = {
  userContext: PropTypes.object.isRequired,
};

export default withUser(Performance);
