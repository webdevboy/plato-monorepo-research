import React, { Component } from 'react';

import Badge from './Badge';
import BadgeRow from './BadgeRow';
import BadgeSectionHeader from './BadgeSectionHeader';
import Button from 'components/shared/Button';
import LearnMoreDialog from './LearnMoreDialog';
import PostStoryDialog from 'components/shared/PostStoryDialog';
import { trackPerformanceLearnMore } from 'utils/analytics';
import { withSnackbar } from 'components/context/SnackbarContext';
import { withTheme } from 'styled-components';
import { withUser } from 'components/context/UserContext';

class StoriesPublishedBadgeSection extends Component {
  constructor(props) {
    super(props);

    const activeImageUrl =
      '//s3.amazonaws.com/appforest_uf/f1529561234857x954986465396359600/badge-stories.png';
    const inactiveImageUrl =
      '//s3.amazonaws.com/appforest_uf/f1529561335732x487063426058739400/badge-stories-copy-2.png';

    this.badgeMeta = {
      title: 'Stories Published',
      activeColor: props.theme.palette.royalBlue,
      badges: [
        {
          name: 'StoriesPublished_1',
          description: '3 Stories Written',
          activeImageUrl,
          inactiveImageUrl,
        },
        {
          name: 'StoriesPublished_2',
          description: '5 Stories Written',
          activeImageUrl,
          inactiveImageUrl,
        },
        {
          name: 'StoriesPublished_3',
          description: '8 Stories Written',
          activeImageUrl,
          inactiveImageUrl,
        },
      ],
    };

    this.state = {
      learnMoreDialogIsOpen: false,
      postStoryDialogIsOpen: false,
    };
  }

  openLearnMoreDialog = () => {
    this.setState({
      learnMoreDialogIsOpen: true,
    });

    const {
      userContext: {
        user: { uuid, email, mentorId },
      },
    } = this.props;
    trackPerformanceLearnMore({
      uuid,
      email,
      mentorId,
      infoType: 'stories',
    });
  };

  closeLearnMoreDialog = () => {
    this.setState({
      learnMoreDialogIsOpen: false,
    });
  };

  openPostStoryDialog = () => {
    this.setState({
      postStoryDialogIsOpen: true,
    });
  };

  closePostStoryDialog = () => {
    this.setState({
      postStoryDialogIsOpen: false,
    });
  };

  getBadges = () => {
    const { mentorBadgeIds } = this.props;
    const { badges, activeColor } = this.badgeMeta;

    return badges.map(badge => (
      <Badge
        key={badge.name}
        activeColor={activeColor}
        activeImageUrl={badge.activeImageUrl}
        inactiveImageUrl={badge.inactiveImageUrl}
        mentorBadgeIds={mentorBadgeIds}
        badgeName={badge.name}
        description={badge.description}
      />
    ));
  };

  onWriteAStoryClick = () => {
    this.openPostStoryDialog();
  };

  render() {
    const { learnMoreDialogIsOpen, postStoryDialogIsOpen } = this.state;

    const badgeRow = <BadgeRow badges={this.getBadges()} sequential />;

    return (
      <React.Fragment>
        <BadgeSectionHeader
          title={this.badgeMeta.title}
          actions={[
            <Button variant="primary-link" key="learn-more" onClick={this.openLearnMoreDialog}>
              Learn More
            </Button>,
          ]}
        />

        {badgeRow}

        <LearnMoreDialog
          open={learnMoreDialogIsOpen}
          onClose={this.closeLearnMoreDialog}
          title={this.badgeMeta.title}
          description="Not only are your stories valuable pieces of content that mentees can browse and book calls from, but they are also used in our internal matching system. You can increase your visibility in the Plato community, and be matched more accurately by adding more stories to our platform."
          badgeRow={badgeRow}
          callToAction={
            <Button variant="primary" onClick={this.onWriteAStoryClick}>
              Write a Story
            </Button>
          }
        />

        <PostStoryDialog
          open={postStoryDialogIsOpen}
          onClose={this.closePostStoryDialog}
          location="performance"
        />
      </React.Fragment>
    );
  }
}

export default withTheme(withUser(withSnackbar(StoriesPublishedBadgeSection)));
