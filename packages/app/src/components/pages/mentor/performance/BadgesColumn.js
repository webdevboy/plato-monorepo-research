import React, { Component } from 'react';

import AdditionalBadgeSection from './AdditionalBadgeSection';
import CardHeader from './CardHeader';
import ReferralsBadgeSection from './ReferralsBadgeSection';
import ReliabilityBadgeSection from './ReliabilityBadgeSection';
import StoriesPublishedBadgeSection from './StoriesPublishedBadgeSection';
import performanceService from './performanceService';
import styled from 'styled-components';
import { withSnackbar } from 'components/context/SnackbarContext';

const StyledWrapper = styled.div`
  padding: 2rem;

  & ${CardHeader} {
    margin-bottom: 6rem;
  }
`;

class BadgesColumn extends Component {
  constructor(props) {
    super(props);

    this.state = {
      mentorBadgeIds: [],
    };
  }

  async componentDidMount() {
    const { mentorId, snackbar } = this.props;
    try {
      const mentorBadgeIds = await performanceService.getMentorBadges(mentorId);
      this.setState({ mentorBadgeIds });
    } catch (error) {
      snackbar.error('Something went wrong - sorry!');
      console.error(error);
    }
  }

  render() {
    const { mentorBadgeIds } = this.state;

    return (
      <StyledWrapper>
        <CardHeader title="All Badges" timeframeText="Since Joining" />
        <ReliabilityBadgeSection mentorBadgeIds={mentorBadgeIds} />
        <StoriesPublishedBadgeSection mentorBadgeIds={mentorBadgeIds} />
        <ReferralsBadgeSection mentorBadgeIds={mentorBadgeIds} />
        <AdditionalBadgeSection mentorBadgeIds={mentorBadgeIds} />
      </StyledWrapper>
    );
  }
}

export default withSnackbar(BadgesColumn);
