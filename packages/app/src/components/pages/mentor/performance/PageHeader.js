import React from 'react';
import styled from 'styled-components';

const StyledWrapper = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  align-self: center;
  color: ${props => props.theme.palette.white};
  text-align: center;
  width: 46rem;

  & img {
    margin-bottom: 2rem;
  }

  & h1 {
    margin-bottom: 1rem;
    letter-spacing: 0.5px;
    width: 46rem;
  }

  & p {
    line-height: 1.5;
  }
`;

const PageHeader = () => (
  <StyledWrapper>
    <div>
      <img src={`${process.env.PUBLIC_URL}/img/graph.png`} alt="Graph" />
    </div>
    <div className="content">
      <h1>Here&apos;s the place where you get your personal data from your mentorship.</h1>
      <p>
        Here you&apos;ll find stats on your activity as a Plato Mentor. All data here is private.
        Please let us know if you see anything incorrect.
      </p>
    </div>
  </StyledWrapper>
);

export default PageHeader;
