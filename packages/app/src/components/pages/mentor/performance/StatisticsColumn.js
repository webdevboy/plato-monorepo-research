import React, { Component } from 'react';

import GlobalDataCard from './GlobalDataCard';
import GradesFromMenteesCard from './GradesFromMenteesCard';
import ImprovementsCard from './ImprovementsCard';
import performanceService from './performanceService';

class StatisticsColumn extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loadingGrades: true,
      grades: {},

      loadingImprovements: true,
      improvements: {},

      loadingAverageScores: true,
      averageScores: {},
    };
  }

  componentDidMount() {
    this.getMentorGrades();
    this.getMentorImprovements();
    this.getAverageScores();
  }

  getMentorGrades = async () => {
    const grades = await performanceService.getMentorGrades(this.props.mentorId);

    this.setState({ loadingGrades: false, grades });
  };

  getMentorImprovements = async () => {
    const improvements = await performanceService.getMentorImprovements(this.props.mentorId);

    this.setState({ loadingImprovements: false, improvements });
  };

  getAverageScores = async () => {
    const averageScores = await performanceService.getAverageScores(this.props.mentorId);

    this.setState({ loadingAverageScores: false, averageScores });
  };

  render() {
    const {
      loadingGrades,
      loadingImprovements,
      loadingAverageScores,
      grades: { satisfaction, usefulness },
      improvements: { noShows, lastMinuteDeclines, cancellations },
      averageScores,
    } = this.state;

    return (
      <React.Fragment>
        <GlobalDataCard mentorId={this.props.mentorId} />
        <GradesFromMenteesCard
          loading={loadingGrades || loadingAverageScores}
          satisfactionRating={satisfaction}
          usefulnessRating={usefulness}
          averageSatisfactionRating={averageScores.satisfaction}
          averageUsefulnessRating={averageScores.usefulness}
        />
        <ImprovementsCard
          loading={loadingImprovements || loadingAverageScores}
          noShows={noShows}
          lastMinuteDeclines={lastMinuteDeclines}
          cancellations={cancellations}
          averageNoShows={averageScores.noShows}
          averageLastMinuteDeclines={averageScores.lastMinuteDeclines}
          averageCancellations={averageScores.cancellations}
        />
      </React.Fragment>
    );
  }
}

export default StatisticsColumn;
