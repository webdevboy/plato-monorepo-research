import React, { Component } from 'react';
import styled from 'styled-components';
import PerformanceCard from './PerformanceCard';
import Rating from './Rating';

const Content = styled.div``;

class GradesFromMenteesCard extends Component {
  render() {
    const {
      loading,
      satisfactionRating,
      usefulnessRating,
      averageSatisfactionRating,
      averageUsefulnessRating,
    } = this.props;

    return (
      <PerformanceCard
        title="Grades From Mentees"
        timeframeText="On Last 8 Calls"
        loading={loading}
      >
        <Content>
          <Rating
            rating={satisfactionRating}
            average={averageSatisfactionRating}
            title="Satisfaction"
            description="Satisfaction ratings are based on your last eight 1-1 calls, and refer to how much mentees appreciated talking with you."
          />

          <Rating
            rating={usefulnessRating}
            average={averageUsefulnessRating}
            title="Usefulness"
            description="Usefulness ratings are based on your last eight 1-1 calls, and refer to how useful mentees found your advice."
          />
        </Content>
      </PerformanceCard>
    );
  }
}

export default GradesFromMenteesCard;
