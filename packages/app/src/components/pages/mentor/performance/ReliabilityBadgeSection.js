import React, { Component } from 'react';

import Badge from './Badge';
import BadgeRow from './BadgeRow';
import BadgeSectionHeader from './BadgeSectionHeader';
import Button from 'components/shared/Button';
import LearnMoreDialog from './LearnMoreDialog';
import { Link } from 'react-router-dom';
import { trackPerformanceLearnMore } from 'utils/analytics';
import { withSnackbar } from 'components/context/SnackbarContext';
import { withTheme } from 'styled-components';
import { withUser } from 'components/context/UserContext';

class ReliabilityBadgeSection extends Component {
  constructor(props) {
    super(props);

    const activeImageUrl =
      '//s3.amazonaws.com/appforest_uf/f1529560515296x361864426173269760/badge-relability.png';
    const inactiveImageUrl =
      '//s3.amazonaws.com/appforest_uf/f1529560794211x471617785282433000/badge-relability-copy.png';

    this.badgeMeta = {
      title: 'Reliability',
      activeColor: props.theme.palette.cerulean,
      badges: [
        {
          name: 'Reliability_1',
          description: '5 Sessions in a Row',
          activeImageUrl,
          inactiveImageUrl,
        },
        {
          name: 'Reliability_2',
          description: '10 Sessions in a Row',
          activeImageUrl,
          inactiveImageUrl,
        },
        {
          name: 'Reliability_3',
          description: '20 Sessions in a Row',
          activeImageUrl,
          inactiveImageUrl,
        },
      ],
    };

    this.state = { learnMoreDialogIsOpen: false };
  }

  openLearnMoreDialog = () => {
    this.setState({
      learnMoreDialogIsOpen: true,
    });

    const {
      userContext: {
        user: { uuid, email, mentorId },
      },
    } = this.props;
    trackPerformanceLearnMore({
      uuid,
      email,
      mentorId,
      infoType: 'reliability',
    });
  };

  closeLearnMoreDialog = () => {
    this.setState({
      learnMoreDialogIsOpen: false,
    });
  };

  getBadges = () => {
    const { mentorBadgeIds } = this.props;
    const { badges, activeColor } = this.badgeMeta;

    return badges.map(badge => (
      <Badge
        key={badge.name}
        activeColor={activeColor}
        activeImageUrl={badge.activeImageUrl}
        inactiveImageUrl={badge.inactiveImageUrl}
        mentorBadgeIds={mentorBadgeIds}
        badgeName={badge.name}
        description={badge.description}
      />
    ));
  };

  render() {
    const { learnMoreDialogIsOpen } = this.state;

    const badgeRow = <BadgeRow badges={this.getBadges()} sequential />;

    return (
      <React.Fragment>
        <BadgeSectionHeader
          title={this.badgeMeta.title}
          actions={[
            <Button variant="primary-link" key="learn-more" onClick={this.openLearnMoreDialog}>
              Learn More
            </Button>,
          ]}
        />
        {badgeRow}

        <LearnMoreDialog
          open={learnMoreDialogIsOpen}
          onClose={this.closeLearnMoreDialog}
          title={this.badgeMeta.title}
          description="Mentees typically have big expectations for a session, and prepare accordingly, so reliability is a key factor of a successful mentee <> mentor relationship. As a reliability indicator, we monitor your number of consecutive sessions in a row with no declines. You can update your availability prior to being booked as needed.  🙂"
          badgeRow={badgeRow}
          callToAction={
            <Link to="/mentor_sessions">
              <Button variant="primary">Manage My Sessions</Button>
            </Link>
          }
        />
      </React.Fragment>
    );
  }
}

export default withTheme(withUser(withSnackbar(ReliabilityBadgeSection)));
