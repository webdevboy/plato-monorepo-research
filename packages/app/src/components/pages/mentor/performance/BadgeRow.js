import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';
import styled from 'styled-components';

const StyledWrapper = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 6rem;

  & > *:not(:last-child) {
    margin-right: 3rem;
  }
`;

const StyledIcon = styled(FontAwesomeIcon)`
  color: ${({ theme }) => theme.palette.cadetBlue};
  font-size: 1.2rem;
  margin-top: -3rem;
`;

const BadgeRow = ({ badges, sequential }) => (
  <StyledWrapper>
    {badges.map((badge, index) => (
      <React.Fragment key={index}>
        {badge}
        {index !== badges.length - 1 && sequential && <StyledIcon icon="chevron-right" />}
      </React.Fragment>
    ))}
  </StyledWrapper>
);

export default BadgeRow;
