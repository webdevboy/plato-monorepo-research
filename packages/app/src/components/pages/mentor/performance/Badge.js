import React, { Component } from 'react';

import performanceService from './performanceService';
import styled from 'styled-components';
import { withSnackbar } from 'components/context/SnackbarContext';

const StyledWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;

  & .description {
    text-align: center;
    width: 7rem;

    ${({ isActive, activeColor }) => `
      ${isActive && `color: ${activeColor}`}
    `};
  }
`;

class Badge extends Component {
  constructor(props) {
    super(props);

    this.state = {
      badgeId: '',
    };
  }

  async componentDidMount() {
    const { badgeName, snackbar } = this.props;
    try {
      const badgeId = await performanceService.getBadgeId(badgeName);
      this.setState({ badgeId });
    } catch (error) {
      snackbar.error('Something went wrong - sorry!');
      console.error(error);
    }
  }

  render() {
    const {
      activeColor,
      activeImageUrl,
      inactiveImageUrl,
      mentorBadgeIds = [],
      description,
    } = this.props;
    const { badgeId } = this.state;

    const isActive = mentorBadgeIds.some(mentorBadgeId => mentorBadgeId === badgeId);

    if (!isActive && !inactiveImageUrl) {
      return null;
    }

    return (
      <StyledWrapper isActive={isActive} activeColor={activeColor}>
        <img src={isActive ? activeImageUrl : inactiveImageUrl} alt={description} />
        <h4 className="description">{description}</h4>
      </StyledWrapper>
    );
  }
}

export default withSnackbar(Badge);
