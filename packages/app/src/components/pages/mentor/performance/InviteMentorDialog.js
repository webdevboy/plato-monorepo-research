import React, { Component } from 'react';

import Button from 'components/shared/Button';
import PlatoDialog from 'components/shared/PlatoDialog';
import TextInput from 'components/shared/TextInput';
import styled from 'styled-components';
import { trackMentorIntroMade } from 'utils/analytics';
import { withSnackbar } from 'components/context/SnackbarContext';
import { withUser } from 'components/context/UserContext';

const Body = styled.div`
  position: relative;
  padding: 3rem;
  width: 48rem;

  & .title {
    margin-bottom: 3rem;
  }

  & .submit {
    display: flex;
    margin-left: auto;
  }
`;

class InviteMentorDialog extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      description: '',
      saving: false,
    };
  }

  onEmailChange = event => {
    this.setState({
      email: event.target.value,
    });
  };

  onDescriptionChange = event => {
    this.setState({
      description: event.target.value,
    });
  };

  onSubmit = event => {
    event.preventDefault();

    this.setState({ saving: true });

    // TODO:  We need an endpoint for email sending
    this.props.snackbar.info('This feature is not implemented yet.  Sorry!');

    const {
      userContext: {
        user: { uuid, email, mentorId },
      },
    } = this.props;
    trackMentorIntroMade({
      uuid,
      email,
      mentorId,
      referral: this.state.email,
    });

    this.setState({ saving: false });
  };

  render() {
    const { open, onClose } = this.props;
    const { email, description, saving } = this.state;

    return (
      <PlatoDialog open={open} onClose={onClose} saving={saving}>
        <Body>
          <h1 className="title">Invite a Mentor</h1>
          <form onSubmit={this.onSubmit}>
            <TextInput
              type="text"
              value={email}
              onChange={this.onEmailChange}
              placeholder="email@example.com"
              label="Mentor you want to refer"
            />

            <TextInput
              type="multi"
              value={description}
              onChange={this.onDescriptionChange}
              label="How would this person be a great mentor?"
            />

            <Button
              className="submit"
              variant="primary"
              disabled={saving || !email || !description}
            >
              Submit
            </Button>
          </form>
        </Body>
      </PlatoDialog>
    );
  }
}

export default withUser(withSnackbar(InviteMentorDialog));
