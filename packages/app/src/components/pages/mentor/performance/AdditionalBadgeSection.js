import React, { Component } from 'react';
import { withSnackbar } from 'components/context/SnackbarContext';
import BadgeSectionHeader from './BadgeSectionHeader';
import BadgeRow from './BadgeRow';
import Badge from './Badge';
import { withTheme } from 'styled-components';

class AdditionalBadgeSection extends Component {
  constructor(props) {
    super(props);

    this.badgeMeta = {
      title: 'Additional Badges',
      activeColor: props.theme.palette.sunglo,
      badges: [
        {
          name: 'NewMentor',
          description: 'New Mentor',
          activeImageUrl:
            '//s3.amazonaws.com/appforest_uf/f1529638456678x963090610457584300/badge_new.png',
          inactiveImageUrl: '',
        },
        {
          name: 'LTM',
          description: 'Long-Term Mentorship',
          activeImageUrl:
            '//s3.amazonaws.com/appforest_uf/f1529564276083x985485928365960700/badge-longterm.png',
          inactiveImageUrl: '',
        },
        {
          name: 'AMA',
          description: 'AMA Sessions',
          activeImageUrl:
            '//s3.amazonaws.com/appforest_uf/f1529638476459x695309029659256300/badge_ama.png',
          inactiveImageUrl: '',
        },
        {
          name: 'Birdly',
          description: 'Early Birdly',
          activeImageUrl:
            '//s3.amazonaws.com/appforest_uf/f1529564400600x590281449258327400/badge-birdly.png',
          inactiveImageUrl: '',
        },
        {
          name: 'Speaker',
          description: 'Best Speaker',
          activeImageUrl:
            '//s3.amazonaws.com/appforest_uf/f1529564440897x725722964853048300/badge-speaker.png',
          inactiveImageUrl: '',
        },
      ],
    };
  }

  getBadges = () => {
    const { mentorBadgeIds } = this.props;
    const { badges, activeColor } = this.badgeMeta;

    return badges.map(badge => (
      <Badge
        key={badge.name}
        activeColor={activeColor}
        activeImageUrl={badge.activeImageUrl}
        inactiveImageUrl={badge.inactiveImageUrl}
        mentorBadgeIds={mentorBadgeIds}
        badgeName={badge.name}
        description={badge.description}
      />
    ));
  };

  render() {
    return (
      <React.Fragment>
        <BadgeSectionHeader title={this.badgeMeta.title} />
        <BadgeRow badges={this.getBadges()} sequential={false} />
      </React.Fragment>
    );
  }
}

export default withTheme(withSnackbar(AdditionalBadgeSection));
