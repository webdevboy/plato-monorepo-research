import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import styled from 'styled-components';

const StyledWrapper = styled.div`
  display: flex;
  align-items: center;

  & h4 {
    margin-left: 1rem;
    font-size: 1.4rem;
  }
`;

const StyledIcon = styled(FontAwesomeIcon)`
  font-size: 1.6rem;
  color: ${({ theme }) => theme.palette.cadetBlue};
`;

const StatisticTimeframe = ({ text }) => (
  <StyledWrapper>
    <StyledIcon icon="clock" />
    <h4>{text}</h4>
  </StyledWrapper>
);

export default StatisticTimeframe;
