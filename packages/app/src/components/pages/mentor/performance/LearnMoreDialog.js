import React from 'react';
import PlatoDialog from 'components/shared/PlatoDialog';
import styled from 'styled-components';

const Body = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  text-align: center;
  padding: 6rem 13rem;
  width: 74rem;

  & .title {
    margin-bottom: 2rem;
  }

  & .description {
    margin-bottom: 4rem;
  }

  & .badge-row {
    margin-bottom: -4rem;
  }
`;

const LearnMoreDialog = ({ open, onClose, title, description, badgeRow, callToAction }) => (
  <PlatoDialog open={open} onClose={onClose}>
    <Body>
      <h1 className="title">{title}</h1>
      <p className="description">{description}</p>
      <div className="badge-row">{badgeRow}</div>
      {callToAction}
    </Body>
  </PlatoDialog>
);

export default LearnMoreDialog;
