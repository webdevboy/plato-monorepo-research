import React from 'react';
import Card from 'components/shared/Card';
import styled from 'styled-components';
import PlatoLinearProgress from 'components/shared/PlatoLinearProgress';
import CardHeader from './CardHeader';

const StyledCard = styled(Card)`
  position: relative;
  padding: 2rem;
  margin-bottom: 2rem;

  & ${PlatoLinearProgress} {
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    z-index: 1;
  }
`;

const PerformanceCard = ({ loading, title, timeframeText, children, ...others }) => (
  <StyledCard {...others}>
    {loading && <PlatoLinearProgress variant="indeterminate" />}
    <CardHeader title={title} timeframeText={timeframeText} />

    {children}
  </StyledCard>
);

export default styled(PerformanceCard)``;
