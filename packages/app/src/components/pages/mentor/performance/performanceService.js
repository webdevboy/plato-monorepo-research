import BackOfficeAverageGradeService from 'services/api/backOffice/averageGrade/averageGrade';
import BackOfficeBadgeService from 'services/api/backOffice/badge/badge';
import BackOfficeMentorService from 'services/api/backOffice/mentor/mentor';
import MentorPerformanceService from 'services/api/postgres/mentorPerformance/mentorPerformance';
import MentorService from 'services/api/plato/mentor/mentor';
import moment from 'moment';

export default {
  async getOneOnOneSessionsCount(mentorId) {
    const requests = await MentorService.getRequests({
      mentorId,
      done: true,
      scheduled: false,
      scMentee: false,
      scMentor: false,
      error: false,
      closed: false,
      page: 0,
      size: 1,
    });

    return requests.totalItems;
  },

  async getAmaSessionsCount(mentorId) {
    const amas = await MentorService.getAmas({
      mentorId,
      scheduled: false,
      closed: false,
      done: true,
      error: false,
      minDate: moment()
        .add(-1, 'year')
        .toISOString(),
      maxDate: moment()
        .add(1, 'year')
        .toISOString(),
      page: 0,
      size: 1,
    });

    return amas.totalItems;
  },

  async getMenteesHelpedCount(mentorId) {
    const helpedMentees = await MentorService.getRequests({
      mentorId,
      done: true,
      scheduled: false,
      scMentee: false,
      scMentor: false,
      error: false,
      closed: false,
      page: 0,
      size: 1,
    });

    return new Set(helpedMentees.result.map(mentor => mentor.menteeId)).size;
  },

  async getMentorGrades(mentorId) {
    const grades = (await MentorPerformanceService.getGrades({ mentorId })) || {};

    return {
      satisfaction: grades.avg_f,
      usefulness: grades.avg_u,
    };
  },

  async getMentorImprovements(mentorId) {
    const improvements =
      (await MentorPerformanceService.getImprovements({
        mentorId,
      })) || {};

    return {
      noShows: improvements['No show'],
      lastMinuteDeclines: improvements['Last-minute rescheduling'],
      cancellations: improvements['Total rescheduling'],
    };
  },

  async getAverageScores() {
    const averageGrades = await BackOfficeAverageGradeService.getAverageGrades();

    // prettier-ignore
    return {
      satisfaction: averageGrades.filter(grade => grade.Name === 'QualityRate')[0].Value,
      usefulness: averageGrades.filter(grade => grade.Name === 'UsefulRate')[0].Value,
      noShows: averageGrades.filter(grade => grade.Name === 'NoShow')[0].Value,
      lastMinuteDeclines: averageGrades.filter(grade => grade.Name === 'LastMinuteDecline')[0].Value,
      cancellations: averageGrades.filter(grade => grade.Name === 'Cancellation')[0].Value,
    };
  },

  async getBadgeId(name) {
    const badges = await BackOfficeBadgeService.getBadges({
      limit: 1,
      constraints: JSON.stringify([
        {
          key: 'Name',
          constraint_type: 'equals', // eslint-disable-line camelcase
          value: name,
        },
      ]),
    });

    return badges[0]._id;
  },

  async getMentorBadges(mentorId) {
    const mentors = await BackOfficeMentorService.getMentors({
      limit: 1,
      constraints: JSON.stringify([
        {
          key: 'PlatoID',
          constraint_type: 'equals', // eslint-disable-line camelcase
          value: mentorId,
        },
      ]),
    });

    return mentors.some(mentor => mentor && mentor.Badges) ? mentors[0].Badges : [];
  },
};
