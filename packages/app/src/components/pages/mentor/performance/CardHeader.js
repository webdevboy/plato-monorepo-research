import React from 'react';
import styled from 'styled-components';
import StatisticTimeframe from './StatisticTimeframe';

const StyledWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 3rem;

  & h3 {
    color: ${({ theme }) => theme.palette.black};
    text-transform: none;
  }
`;

const CardHeader = ({ title, timeframeText, className }) => (
  <StyledWrapper className={className}>
    <h3>{title}</h3>
    <StatisticTimeframe text={timeframeText} />
  </StyledWrapper>
);

export default styled(CardHeader)``;
