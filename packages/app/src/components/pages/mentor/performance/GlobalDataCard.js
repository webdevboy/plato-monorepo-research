import React, { Component } from 'react';

import GlobalDataStatistic from './GlobalDataStatistic';
import PerformanceCard from './PerformanceCard';
import performanceService from './performanceService';
import styled from 'styled-components';

const Content = styled.div`
  display: flex;
`;

class GlobalDataCard extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      oneOnOneSessionsCount: '-',
      amaSessionsCount: '-',
      menteesHelpedCount: '-',
    };
  }

  async componentDidMount() {
    const { mentorId } = this.props;

    this.setState({
      loading: true,
    });

    const [oneOnOneSessionsCount, amaSessionsCount, menteesHelpedCount] = await Promise.all([
      performanceService.getOneOnOneSessionsCount(mentorId),
      performanceService.getAmaSessionsCount(mentorId),
      performanceService.getMenteesHelpedCount(mentorId),
    ]);

    this.setState({
      loading: false,
      oneOnOneSessionsCount,
      amaSessionsCount,
      menteesHelpedCount,
    });
  }

  render() {
    const { loading, oneOnOneSessionsCount, amaSessionsCount, menteesHelpedCount } = this.state;

    return (
      <PerformanceCard title="Global Data" timeframeText="Since Joining" loading={loading}>
        <Content>
          <GlobalDataStatistic number={oneOnOneSessionsCount} label="One-on-One Sessions" />
          <GlobalDataStatistic number={amaSessionsCount} label="AMA Sessions" />
          <GlobalDataStatistic number={menteesHelpedCount} label="Mentees Helped" />
        </Content>
      </PerformanceCard>
    );
  }
}

export default GlobalDataCard;
