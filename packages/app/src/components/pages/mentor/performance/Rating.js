import React from 'react';
import styled from 'styled-components';

const StyledWrapper = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 3rem;
`;

const Left = styled.div`
  margin-right: 2rem;
  text-align: center;
  flex-shrink: 0;

  & .rating {
    font-size: 3.4rem;
    margin-bottom: 1rem;
  }

  ${({ rating, average, theme }) =>
    rating && `color: ${rating >= average ? theme.palette.emerald : theme.palette.sunglo};`}
`;

const Rating = ({ rating, average, title, description }) => (
  <StyledWrapper>
    <Left rating={rating} average={average}>
      <h1 className="rating">{Number.isNaN(rating) || rating === null ? '-' : rating}</h1>
      <p className="average">{Number.isNaN(average) || average === null ? '-' : average} Average</p>
    </Left>
    <div>
      <h1>{title}</h1>
      <p>{description}</p>
    </div>
  </StyledWrapper>
);

export default Rating;
