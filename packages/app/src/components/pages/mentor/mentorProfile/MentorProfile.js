import Grid from '@material-ui/core/Grid';
import { withSnackbar } from 'components/context/SnackbarContext';
import { withUser } from 'components/context/UserContext';
import Button from 'components/shared/Button';
import Layout from 'components/shared/Layout';
import SelectInput from 'components/shared/SelectInput';
import Text from 'components/shared/Text';
import TextInput from 'components/shared/TextInput';
import { Form, Formik } from 'formik';
import React, { Component } from 'react';
import Dropzone from 'react-dropzone';
import Switch from 'react-switch';
import mentorService from 'services/api/plato/mentor/mentor';
import styled from 'styled-components';

const hierarchyChoices = [
  { value: 'IC', label: 'IC' },
  { value: 'Lead', label: 'Lead' },
  { value: 'Manager of ICs', label: 'Manager of ICs' },
  { value: 'Manager of Managers', label: 'Manager of Managers' },
  { value: 'Executive', label: 'Executive' },
  { value: 'C-Suite', label: 'C-Suite' },
];

const companySizeChoices = [
  {
    value: 'early stage startup (<10 employee)',
    label: 'Early stage startup (<10 employee)',
  },
  {
    value: 'middle stage startup ( 10 < < 50 employees)',
    label: 'Middle stage startup (10 to 50 employees)',
  },
  {
    value: 'late stage startup ( 50 < < 100 employees)',
    label: 'Late stage startup (50 to 100 employees)',
  },
  {
    value: 'Big company ( > 100 employees)',
    label: 'Big company (>100 employees)',
  },
  {
    value: 'Big corporation (>1000 employees)',
    label: 'Big corporation (>1000 employees)',
  },
];

const StyledLabel = styled.h4`
  margin-bottom: 1rem;
  line-height: 1.5;
`;

const StyledWrapper = styled.div`
  ${Layout} {
    position: relative;
    top: 6rem;
  }

  .columns {
    display: flex;
    padding: 2em;
    line-height: 4.3rem;

    > .left {
      width: 47.5rem;
      padding-right: 11.5rem;
    }

    > .right {
      flex-grow: 1;
    }
  }
`;

class MentorProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      initialized: false,
      picture: null,
      logo: null,
      previewPicture: null,
      previewLogo: null,
      expertiseChoices: null,
      domainExpertiseChoices: null,
      specialExpertiseChoices: null,
      industryChoices: null,
      formValues: {
        // Non editable data
        firstName: null,
        lastName: null,
        email: null,
        // Editable data
        expertise: null,
        domainExpertise: null,
        specialExpertise: null,
        industries: null,
        title: null,
        companyName: null,
        linkedin: null,
        nbDirectReports: null,
        sizeOfTeam: null,
        managementExperience: null,
        hierarchy: null,
        companyType: null,
        refusedWebsiteAppearance: null,
        aboutMyself: null,
        location: null,
        // Only for PUT data
        faceIds: null,
        timeZone: null,
        doNotProposeLtm: null,
        integratedToSite: null,
        source: null,
        workedInStartup: null,
        caliber: null,
        callSummary: null,
        ltmMaxNb: null,
        status: null,
        companyId: null,
      },
    };
  }

  componentDidMount() {
    this.loadPage();
  }

  onPictureDrop = files => {
    const reader = new FileReader();
    reader.readAsDataURL(files[0]);
    reader.onload = () => {
      this.setState({
        previewPicture: reader.result,
        picture: files[0],
      });
    };
  };

  onLogoDrop = files => {
    const reader = new FileReader();
    reader.readAsDataURL(files[0]);
    reader.onload = () => {
      this.setState({
        previewLogo: reader.result,
        logo: files[0],
      });
    };
  };

  loadPage = async () => {
    const {
      snackbar,
      userContext: { user },
    } = this.props;
    try {
      const [mentor, expertise, domainExpertise, specialExpertise, industries] = await Promise.all([
        mentorService.getMentor(user.mentorId),
        mentorService.getTags('story'),
        mentorService.getTags('domainExpertise'),
        mentorService.getTags('specialExpertise'),
        mentorService.getTags('industry'),
      ]);

      this.setState({
        initialized: true,
        expertiseChoices: expertise.map(choice => ({
          label: choice.name,
          value: choice.id,
        })),
        domainExpertiseChoices: domainExpertise.map(choice => ({
          label: choice.name,
          value: choice.id,
        })),
        specialExpertiseChoices: specialExpertise.map(choice => ({
          label: choice.name,
          value: choice.id,
        })),
        industryChoices: industries.map(choice => ({
          label: choice.name,
          value: choice.id,
        })),
        formValues: {
          // Non editable data
          firstName: mentor.firstName,
          lastName: mentor.lastName,
          email: mentor.email,
          // Editable data
          expertise: mentor.expertise || [],
          domainExpertise: mentor.domainExpertise || [],
          specialExpertise: mentor.specialExpertise || [],
          industries: mentor.industries || [],
          title: mentor.title || '',
          companyName: mentor.companyName || '',
          linkedin: mentor.linkedin || '',
          nbDirectReports: mentor.nbDirectReports || '',
          sizeOfTeam: mentor.sizeOfTeam || '',
          managementExperience: mentor.managementExperience || '',
          hierarchy: hierarchyChoices.filter(choice => choice.value === mentor.hierarchy)[0] || '',
          companyType:
            companySizeChoices.filter(choice => choice.value === mentor.companyType)[0] || '',
          refusedWebsiteAppearance: mentor.refusedWebsiteAppearance,
          aboutMyself: mentor.aboutMyself || '',
          location: mentor.location || '',
          // Only for PUT data
          faceIds: mentor.faces.map(face => face.id),
          timeZone: mentor.timeZone,
          doNotProposeLtm: mentor.doNotProposeLtm,
          integratedToSite: mentor.integratedToSite,
          source: mentor.source,
          workedInStartup: mentor.workedInStartup,
          caliber: mentor.caliber,
          callSummary: mentor.callSummary,
          ltmMaxNb: mentor.ltmMaxNb,
          status: mentor.status,
          companyId: mentor.companyId,
        },
      });
    } catch (error) {
      console.error(error);
      snackbar.error('Something went wrong - sorry!');
    }
  };

  handleRenderProfilePictureDropZone = args => {
    const { getRootProps, getInputProps } = args;
    const { previewPicture } = this.state;
    const {
      userContext: { mentor },
    } = this.props;
    let picture = previewPicture;
    if (!previewPicture && mentor.picture) {
      picture = mentor.picture.originalUrl;
    }
    return (
      <div {...getRootProps()}>
        <input {...getInputProps()} />
        {picture ? (
          <img src={picture} width="100%" height="100%" alt="Profile" />
        ) : (
          <p>Please Upload Your Picture</p>
        )}
      </div>
    );
  };

  handleRenderCompanyPictureDropZone = args => {
    const { getRootProps, getInputProps } = args;
    const { previewLogo } = this.state;
    const {
      userContext: { mentor },
    } = this.props;
    let picture = previewLogo;
    if (!previewLogo && mentor.companyLogo) {
      picture = mentor.companyLogo.originalUrl;
    }
    return (
      <div {...getRootProps()}>
        <input {...getInputProps()} />
        {picture ? (
          <img src={picture} width="100%" height="100%" alt="Profile" />
        ) : (
          <p>Please Upload Your Company&apos;s Logo</p>
        )}
      </div>
    );
  };

  render() {
    const {
      snackbar,
      userContext,
      userContext: { mentor },
    } = this.props;
    const {
      picture,
      logo,
      expertiseChoices,
      domainExpertiseChoices,
      specialExpertiseChoices,
      industryChoices,
      initialized,
      formValues,
    } = this.state;

    if (!initialized) {
      return <div />;
    }

    return (
      <StyledWrapper>
        <Layout width={780}>
          <Formik
            initialValues={{ ...formValues }}
            validate={values => {
              const errors = {};

              if (!values.companyName) {
                errors.companyName = 'Company name is required';
              }

              if (values.domainExpertise.length > 3) {
                errors.domainExpertise = 'You may only pick up to 3 domain expertise.';
              }

              if (values.nbDirectReports !== 0 && !values.nbDirectReports) {
                errors.nbDirectReports = 'Number of direct reports is required';
              } else if (values.nbDirectReports && !Number.isInteger(values.nbDirectReports)) {
                errors.nbDirectReports = 'Number of direct reports should be a number';
              }

              if (values.sizeOfTeam !== 0 && !values.sizeOfTeam) {
                errors.sizeOfTeam = 'Size of team should is required';
              }

              if (values.sizeOfTeam && !Number.isInteger(values.sizeOfTeam)) {
                errors.sizeOfTeam = 'Size of team should be a number';
              }

              if (userContext.isProductManager() && values.industries.length <= 0) {
                errors.industry = 'Please choose one or more industries';
              }

              if (values.managementExperience && !Number.isInteger(values.managementExperience)) {
                errors.managementExperience = 'Years of management experience should be a number';
              }

              const urlPattern = /https?:\/\/.+/;
              if (values.linkedin && !urlPattern.test(values.linkedin)) {
                errors.linkedin = 'URL is invalid';
              }

              if (Object.keys(errors).length > 0) {
                this.isSubmitting = true;
              }

              return errors;
            }}
            onSubmit={async (values, { setSubmitting }) => {
              const formData = new FormData();
              const requestParams = {
                id: mentor.id,
                title: values.title,
                companyName: values.companyName,
                linkedin: values.linkedin,
                expertise: values.expertise.map(choice => choice.value || choice.id),
                domainExpertise: values.domainExpertise.map(choice => choice.value || choice.id),
                specialExpertise: values.specialExpertise.map(choice => choice.value || choice.id),
                industryIds: values.industries.map(choice => choice.value || choice.id),
                timeZone: values.timeZone,
                nbDirectReports: values.nbDirectReports,
                doNotProposeLtm: values.doNotProposeLtm,
                integratedToSite: values.integratedToSite,
                refusedWebsiteAppearance: values.refusedWebsiteAppearance,
                sizeOfTeam: values.sizeOfTeam,
                source: values.source,
                hierarchy: values.hierarchy.value,
                workedInStartup: values.workedInStartup,
                caliber: values.caliber,
                callSummary: values.callSummary,
                managementExperience: values.managementExperience,
                companyType: values.companyType.value,
                aboutMyself: values.aboutMyself,
                ltmMaxNb: values.ltmMaxNb,
                status: values.status,
                faceIds: values.faceIds,
                companyId: values.companyId,
                location: values.location,
              };

              if (picture) {
                formData.append('picture', picture, picture.name);
              } else {
                requestParams.pictureUrl = mentor.picture.originalUrl;
              }

              if (logo) {
                formData.append('companyLogo', logo, logo.name);
              } else {
                requestParams.companyLogoUrl = mentor.companyLogo.originalUrl;
              }

              // Read that beautiful Stack Overflow post
              // https://stackoverflow.com/questions/21329426/spring-mvc-multipart-request-with-json
              formData.append(
                'mentor',
                new Blob([JSON.stringify(requestParams)], {
                  type: 'application/json',
                })
              );

              try {
                const { data } = await mentorService.updateMentor({
                  mentorId: mentor.id,
                  formData,
                });

                if (data && data.status === 'OK') {
                  snackbar.success('Your profile has been updated!');
                }
                setSubmitting(false);
              } catch (error) {
                console.error(error);
                snackbar.error('Something went wrong - sorry!');
              }
            }}
            render={({
              values,
              errors,
              setFieldValue,
              setFieldTouched,
              handleChange,
              isSubmitting,
            }) => (
              <Form>
                <Grid container style={{ paddingBottom: '4rem' }}>
                  <Grid item xs={6}>
                    <div>
                      <StyledLabel className="label">MY PROFILE IS</StyledLabel>
                      <Switch
                        onChange={() =>
                          setFieldValue(
                            'refusedWebsiteAppearance',
                            !values.refusedWebsiteAppearance
                          )
                        }
                        checked={!values.refusedWebsiteAppearance}
                        id="normal-switch"
                      />
                      <Text>{values.refusedWebsiteAppearance ? 'Private' : 'Public'}</Text>
                    </div>
                  </Grid>
                  <Grid item xs={6} style={{ textAlign: 'right' }}>
                    <Button
                      type="submit"
                      className="post-story-button"
                      variant="primary"
                      disabled={Object.keys(errors).length > 0 || isSubmitting}
                    >
                      SAVE CHANGES
                    </Button>
                  </Grid>
                </Grid>

                <Grid container spacing={24}>
                  <Grid item xs={4}>
                    <StyledLabel className="label">Profile picture</StyledLabel>
                    <Dropzone onDrop={this.onPictureDrop} onFileDialogCancel={this.onCancel}>
                      {this.handleRenderProfilePictureDropZone}
                    </Dropzone>
                  </Grid>
                  <Grid item xs={8}>
                    <Grid container spacing={16}>
                      <Grid item xs={6}>
                        <TextInput
                          type="text"
                          name="firstName"
                          label="FIRST NAME"
                          value={values.firstName}
                          onChange={handleChange}
                          padding="2rem 2rem 0"
                          disabled
                        />
                      </Grid>
                      <Grid item xs={6}>
                        <TextInput
                          type="text"
                          name="lastName"
                          label="LAST NAME"
                          value={values.lastName}
                          onChange={handleChange}
                          padding="2rem 2rem 0"
                          disabled
                        />
                      </Grid>
                      <Grid item xs={12}>
                        <TextInput
                          type="email"
                          name="email"
                          label="EMAIL"
                          value={values.email}
                          onChange={handleChange}
                          padding="0 2rem"
                          disabled
                        />
                      </Grid>
                    </Grid>
                  </Grid>
                </Grid>

                <Grid container spacing={16} style={{ paddingTop: '4rem' }}>
                  <Grid item xs={6}>
                    <TextInput
                      type="text"
                      name="title"
                      label="JOB TITLE"
                      value={values.title}
                      onChange={handleChange}
                    />
                  </Grid>
                  <Grid item xs={6}>
                    <TextInput
                      error={errors.managementExperience}
                      type="number"
                      name="managementExperience"
                      label="MANAGEMENT EXPERIENCE"
                      value={values.managementExperience}
                      onChange={handleChange}
                    />
                  </Grid>
                  <Grid item xs={6}>
                    <SelectInput
                      label="HIERARCHY"
                      name="hierarchy"
                      options={hierarchyChoices}
                      onChange={setFieldValue}
                      onBlur={setFieldTouched}
                      field="hierarchy"
                      initialValue={values.hierarchy}
                    />
                  </Grid>
                  <Grid item xs={6}>
                    <TextInput
                      error={errors.sizeOfTeam}
                      type="number"
                      name="sizeOfTeam"
                      label="TEAM SIZE"
                      value={values.sizeOfTeam}
                      onChange={handleChange}
                    />
                  </Grid>
                  <Grid item xs={6}>
                    <TextInput
                      error={errors.companyName}
                      type="text"
                      name="companyName"
                      label="COMPANY NAME"
                      value={values.companyName}
                      onChange={handleChange}
                      padding="0 2rem"
                    />
                  </Grid>
                  <Grid item xs={6}>
                    <SelectInput
                      label="COMPANY SIZE"
                      name="companyType"
                      options={companySizeChoices}
                      onChange={setFieldValue}
                      onBlur={setFieldTouched}
                      field="companyType"
                      initialValue={values.companyType}
                    />
                  </Grid>
                  <Grid item xs={6}>
                    <TextInput
                      type="text"
                      name="location"
                      label="LOCATION"
                      value={values.location}
                      onChange={handleChange}
                      padding="0 2rem"
                    />
                  </Grid>
                  <Grid item xs={6}>
                    <TextInput
                      error={errors.linkedin}
                      pattern="https?://.+"
                      type="url"
                      name="linkedin"
                      label="LINKEDIN"
                      value={values.linkedin}
                      onChange={handleChange}
                    />
                  </Grid>
                  <Grid item xs={6}>
                    <TextInput
                      error={errors.nbDirectReports}
                      type="number"
                      name="nbDirectReports"
                      label="NUMBER OF DIRECT REPORTS"
                      value={values.nbDirectReports}
                      onChange={handleChange}
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <SelectInput
                      label="MANAGEMENT EXPERTISE"
                      name="expertise"
                      options={expertiseChoices}
                      onChange={setFieldValue}
                      onBlur={setFieldTouched}
                      field="expertise"
                      value={values.expertise.map(choice => ({
                        value: choice.id || choice.value,
                        label: choice.name || choice.label,
                      }))}
                      isMulti
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <SelectInput
                      error={errors.domainExpertise}
                      label="DOMAIN EXPERTISE (MAX 3)"
                      name="domainExpertise"
                      options={domainExpertiseChoices}
                      onChange={setFieldValue}
                      onBlur={setFieldTouched}
                      field="domainExpertise"
                      value={values.domainExpertise.map(choice => ({
                        value: choice.id || choice.value,
                        label: choice.name || choice.label,
                      }))}
                      isMulti
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <SelectInput
                      label="SPECIAL EXPERTISE"
                      name="specialExpertise"
                      options={specialExpertiseChoices}
                      onChange={setFieldValue}
                      onBlur={setFieldTouched}
                      field="specialExpertise"
                      value={values.specialExpertise.map(choice => ({
                        value: choice.id || choice.value,
                        label: choice.name || choice.label,
                      }))}
                      isMulti
                    />
                  </Grid>

                  {userContext.isProductManager() && (
                    <Grid item xs={12}>
                      <SelectInput
                        error={errors.industry}
                        label="Industry"
                        name="industry"
                        options={industryChoices}
                        onChange={setFieldValue}
                        onBlur={setFieldTouched}
                        field="industries"
                        value={values.industries.map(choice => ({
                          value: choice.id || choice.value,
                          label: choice.name || choice.label,
                        }))}
                        padding="0 2rem"
                        isMulti
                      />
                    </Grid>
                  )}
                  <Grid item xs={12}>
                    <TextInput
                      name="aboutMyself"
                      label="DESCRIPTION"
                      value={values.aboutMyself}
                      onChange={handleChange}
                      padding="4rem 2rem"
                      type="multi"
                      rows={5}
                    />
                  </Grid>
                  <Grid item xs={4}>
                    <StyledLabel className="label">COMPANY LOGO</StyledLabel>
                    <Dropzone
                      style={{
                        padding: '1rem',
                        margin: '2rem',
                        border: '2px dashed black',
                      }}
                      onDrop={this.onLogoDrop}
                      onFileDialogCancel={this.onCancel}
                    >
                      {this.handleRenderCompanyPictureDropZone}
                    </Dropzone>
                  </Grid>
                </Grid>
                <Button
                  style={{ margin: '2rem 0 10rem 2rem' }}
                  type="submit"
                  className="post-story-button"
                  variant="primary"
                  disabled={Object.keys(errors).length > 0 || isSubmitting ? 'disabled' : ''}
                >
                  SAVE CHANGES
                </Button>
              </Form>
            )}
          />
        </Layout>
      </StyledWrapper>
    );
  }
}

export default withUser(withSnackbar(MentorProfile));
