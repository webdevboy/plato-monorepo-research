import React, { Component } from 'react';

import Avatar from 'components/shared/Avatar';
import Button from 'components/shared/Button';
import CancelDialog from './CancelDialog';
import Card from 'components/shared/Card';
import DateTimeWithIcons from 'components/shared/DateTimeWithIcons';
import DaysUntil from 'components/shared/DaysUntil';
import HorizontalCardLayout from 'components/shared/HorizontalCardLayout';
import IconLabel from 'components/shared/IconLabel';
import ImageOverlayList from 'components/shared/ImageOverlayList';
import { Link } from 'react-router-dom';
import getProfilePictureThumbnail from 'utils/thumbnail';
import styled from 'styled-components';
import { trackMentorEventCanceled } from 'utils/analytics';
import { withUser } from 'components/context/UserContext';

const LeftWrapper = styled.div`
  & .relationship {
    ${({ theme }) => `
      border: 1px solid ${theme.palette.cerulean};
      color: ${theme.palette.cerulean};
      `} width: fit-content;
    padding: 0.5rem 1rem;
    text-transform: uppercase;
    font-weight: bold;
    border-radius: 0.2rem;
    margin-bottom: 1rem;
  }

  & ${Button} {
    margin-top: auto;
  }
`;

const RightWrapper = styled.div`
  height: 100%;

  & .label {
    margin-bottom: 0.5rem;
  }

  & .story-name,
  & .attendances {
    margin-bottom: 1.5rem;
  }

  & > ${Button} {
    margin-top: auto;
    margin-left: auto;
  }
`;

const StyledCard = styled(Card)`
  height: 37rem;

  & ${LeftWrapper}, & ${RightWrapper} {
    display: flex;
    flex-direction: column;
    height: 100%;
  }
`;

const Left = ({ ama, onCancelClick }) => (
  <LeftWrapper>
    <div className="relationship">AMA Session</div>
    <DaysUntil timestamp={ama.callScheduledAt} />
    <DateTimeWithIcons timestamp={ama.callScheduledAt} />
    {ama.callScheduledAt > Date.now() && (
      <Button variant="secondary-link" onClick={onCancelClick}>
        I cannot attend anymore
      </Button>
    )}
  </LeftWrapper>
);

const Right = ({ ama }) => (
  <RightWrapper>
    <h4 className="label">AMA Topic</h4>
    <Link to={`/mentor_ama/${ama.id}`}>
      <h1 className="story-name">{ama.storyName}</h1>
    </Link>
    <IconLabel className="attendances" icon="user">
      {ama.seatsScheduledNb} / {ama.limitAttendances} attendees
    </IconLabel>
    <ImageOverlayList>
      {ama._attendances.map(attendance => (
        <Avatar
          key={attendance.id}
          size="sm"
          src={getProfilePictureThumbnail(
            attendance.menteeFullName,
            attendance.menteePicture.thumbnailUrl
          )}
          alt={attendance.menteeFullName}
        />
      ))}
    </ImageOverlayList>
  </RightWrapper>
);

class AmaCard extends Component {
  constructor(props) {
    super(props);

    this.state = {
      cancelDialogIsOpen: false,
    };
  }

  openCancelDialog = () => {
    this.setState({
      cancelDialogIsOpen: true,
    });
  };

  closeCancelDialog = () => {
    this.setState({
      cancelDialogIsOpen: false,
    });
  };

  onCancelConfirm = () => {
    this.closeCancelDialog();

    const {
      userContext: {
        user: { uuid, email, mentorId },
      },
      ama: { id: amaId },
    } = this.props;
    trackMentorEventCanceled({
      uuid,
      email,
      mentorId,
      eventId: amaId,
      eventType: 'ama',
    });
  };

  render() {
    const { ama } = this.props;
    const { cancelDialogIsOpen } = this.state;
    return (
      <React.Fragment>
        <StyledCard>
          <HorizontalCardLayout
            left={<Left className="section" ama={ama} onCancelClick={this.openCancelDialog} />}
            right={<Right className="section" ama={ama} />}
          />
        </StyledCard>

        <CancelDialog
          title="Are you sure you want to cancel this AMA session?"
          description="As you know, AMA sessions are attended by multiple mentees. If something has come up and you cannot attend anymore, please decline it in your calendar."
          buttonLabel="Okay, I'll cancel in my calendar!"
          isEnabled
          open={cancelDialogIsOpen}
          onClose={this.closeCancelDialog}
          onConfirm={this.onCancelConfirm}
        />
      </React.Fragment>
    );
  }
}

export default withUser(AmaCard);
