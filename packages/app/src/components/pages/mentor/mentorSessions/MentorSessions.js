import React, { Component } from 'react';

import EmptyStateCard from 'components/shared/EmptyStateCard';
import Hero from 'components/shared/Hero';
import Layout from 'components/shared/Layout';
import MentorSessionCard from './MentorSessionCard';
import PageHeader from 'components/shared/PageHeader';
import PageLoadingSpinner from 'components/shared/PageLoadingSpinner';
import SessionTimeframeTabs from 'components/shared/SessionTimeframeTabs';
import mentorSessionsService from './mentorSessionsService';
import moment from 'moment';
import styled from 'styled-components';
import { withSnackbar } from 'components/context/SnackbarContext';
import { withUser } from 'components/context/UserContext';

const StyledWrapper = styled.div`
  & ${Layout} {
    position: relative;
    top: 6rem;

    & ${SessionTimeframeTabs} {
      margin: 3rem 0;
    }

    & .session-list > * {
      margin-bottom: 3rem;
    }
  }
`;

class MentorSessions extends Component {
  constructor(props) {
    super(props);

    this.mentorId = this.props.userContext.user.mentorId;

    this.state = {
      loading: true,
      pastSessions: [],
      futureSessions: [],
      timeframe: 'future',
    };
  }

  async componentDidMount() {
    this.loadData();
  }

  onTimeframeChange = timeframe => {
    this.setState({
      timeframe,
    });
  };

  getStartTime = session => {
    switch (session._type) {
      case 'ama':
      case 'oneOnOne':
        return session.callScheduledAt;
      case 'open':
        return session.startTime;
      case 'platoMeeting':
        return session.startTime;
      default:
        return undefined;
    }
  };

  getFutureSessions = (slots, amas, requests) => {
    return [...slots, ...amas, ...requests]
      .filter(session => moment(this.getStartTime(session)).isAfter(moment()))
      .sort((sessionA, sessionB) => this.getStartTime(sessionA) - this.getStartTime(sessionB));
  };

  getPastSessions = (slots, amas, requests) => {
    return [...slots, ...amas, ...requests]
      .filter(session => moment(this.getStartTime(session)).isBefore(moment()))
      .sort((sessionA, sessionB) => this.getStartTime(sessionB) - this.getStartTime(sessionA));
  };

  loadData = async () => {
    const { getSlots, getAmas, getRequests } = mentorSessionsService;

    this.setState({ loading: true });

    try {
      const [slots, amas, requests] = await Promise.all([
        getSlots(this.mentorId),
        getAmas(this.mentorId),
        getRequests(this.mentorId),
      ]);

      this.setState({
        futureSessions: this.getFutureSessions(slots, amas, requests),
        pastSessions: this.getPastSessions(slots, amas, requests),
      });
    } catch (error) {
      console.error(error);
      this.props.snackbar.error('Something went wrong - sorry!');
    }

    this.setState({
      loading: false,
    });
  };

  render() {
    const { loading, timeframe, futureSessions, pastSessions } = this.state;

    const nextSession = futureSessions[0];
    const sessions = timeframe === 'future' ? futureSessions : pastSessions;
    const listSessions = sessions.filter(session => session !== nextSession);

    return (
      <StyledWrapper>
        <Hero />
        {loading && <PageLoadingSpinner />}
        {!loading && (
          <Layout width="780">
            <PageHeader>Your Next Session</PageHeader>
            {nextSession && (
              <MentorSessionCard
                className="next-session"
                session={nextSession}
                triggerRefresh={this.loadData}
                isNextSession
              />
            )}
            {!nextSession && <EmptyStateCard title="No reserved slots found." />}

            <React.Fragment>
              <SessionTimeframeTabs
                selectedTimeframe={timeframe}
                onChange={this.onTimeframeChange}
              />

              {listSessions.length > 0 && (
                <div className="session-list">
                  {listSessions.map(session => (
                    <MentorSessionCard
                      key={session.id}
                      session={session}
                      triggerRefresh={this.loadData}
                      isNextSession={false}
                    />
                  ))}
                </div>
              )}
              {listSessions.length === 0 && timeframe === 'future' && (
                <EmptyStateCard title="Looks like you don't have any booked sessions..." />
              )}
              {listSessions.length === 0 && timeframe === 'past' && (
                <EmptyStateCard
                  title="You haven't had any sessions... yet!"
                  image="🤞"
                  label="No past sessions"
                />
              )}
            </React.Fragment>
          </Layout>
        )}
      </StyledWrapper>
    );
  }
}

export default withSnackbar(withUser(MentorSessions));
