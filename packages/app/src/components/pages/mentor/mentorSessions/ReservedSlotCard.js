import React, { Component } from 'react';

import Button from 'components/shared/Button';
import CancelDialog from './CancelDialog';
import Card from 'components/shared/Card';
import DateTimeWithIcons from 'components/shared/DateTimeWithIcons';
import DaysUntil from 'components/shared/DaysUntil';
import EventService from 'services/api/plato/event/event';
import HorizontalCardLayout from 'components/shared/HorizontalCardLayout';
import PostStoryDialog from 'components/shared/PostStoryDialog';
import styled from 'styled-components';
import { trackMentorEventCanceled } from 'utils/analytics';
import { withQueryLoader } from 'components/context/QueryLoaderContext';
import { withSnackbar } from 'components/context/SnackbarContext';
import { withUser } from 'components/context/UserContext';
import { formatTimestampToDate, formatTimestampToString } from 'utils/time';

const LeftWrapper = styled.div`
  & ${Button} {
    margin-top: auto;
  }
`;

const RightWrapper = styled.div`
  & .content {
    margin: auto;
  }
`;

const StyledCard = styled(Card)`
  border: 1px dashed ${({ theme }) => theme.palette.cerulean};
  & .section.left {
    background-color: ${({ theme }) => theme.palette.white};
    border-right: 1px dashed ${({ theme }) => theme.palette.cerulean};
  }

  & ${LeftWrapper}, & ${RightWrapper} {
    display: flex;
    flex-direction: column;
    height: 100%;
    min-height: 20rem;
  }
`;

const StyledNextSessionCard = styled.div`
  display: flex;
  flex-direction: column;
  padding: 3rem;
  border: 1px dashed ${({ theme }) => theme.palette.cerulean};

  background: transparent;
  justify-content: space-between;
  align-items: center;
  height: 23rem;
  margin-bottom: 15rem;

  & h1 {
    text-align: center;
    color: ${({ theme }) => theme.palette.white};
    width: 70%;
  }

  & .actions {
    position: relative;
    align-self: flex-start;
    width: 100%;

    & .write-story {
      position: absolute;
      left: 50%;
      transform: translate(-50%);
    }
  }
`;

const Left = ({ slot, onCancelClick }) => (
  <LeftWrapper>
    <DaysUntil timestamp={slot.startTime} />
    <DateTimeWithIcons timestamp={slot.startTime} />
    {slot.startTime > Date.now() && (
      <Button variant="secondary-link" onClick={onCancelClick}>
        Cancel Empty Slot
      </Button>
    )}
  </LeftWrapper>
);

const Right = () => (
  <RightWrapper>
    <p className="content">
      You may be booked on this time slot by a mentee in the next days. You can change or cancel
      this time slot directly on your calendar.
    </p>
  </RightWrapper>
);

class ReservedSlotCard extends Component {
  constructor(props) {
    super(props);

    this.state = {
      cancelDialogIsOpen: false,
      isLoading: false,
      postStoryDialogIsOpen: false,
    };
  }

  openCancelDialog = () => {
    this.setState({
      cancelDialogIsOpen: true,
    });
  };

  closeCancelDialog = () => {
    this.setState({
      cancelDialogIsOpen: false,
    });
  };

  openPostStoryDialog = () => {
    this.setState({
      postStoryDialogIsOpen: true,
    });
  };

  closePostStoryDialog = () => {
    this.setState({
      postStoryDialogIsOpen: false,
    });
  };

  cancelReservedSlot = async () => {
    const {
      slot,
      queryLoader,
      snackbar,
      triggerRefresh,
      userContext: {
        user: { uuid, email, mentorId },
      },
    } = this.props;

    queryLoader.showLoader();
    this.setState({ isLoading: true });

    try {
      await EventService.decline({
        eventId: slot.id,
        user: email,
      });

      trackMentorEventCanceled({
        uuid,
        email,
        mentorId,
        eventId: slot.id,
        eventType: 'plato reserved slot',
      });

      snackbar.success('Successfully cancelled!');
    } catch (error) {
      console.error(error);
      snackbar.error('Something went wrong - sorry!');
    }

    queryLoader.hideLoader();
    this.setState({ isLoading: false });
    this.closeCancelDialog();
    triggerRefresh();
  };

  render() {
    const { slot, className, isNextSession, userContext } = this.props;
    const { cancelDialogIsOpen, isLoading, postStoryDialogIsOpen } = this.state;

    return (
      <React.Fragment>
        {!isNextSession && (
          <StyledCard className={className}>
            <HorizontalCardLayout
              left={<Left slot={slot} onCancelClick={this.openCancelDialog} />}
              right={<Right />}
            />
          </StyledCard>
        )}

        {isNextSession && (
          <StyledNextSessionCard className={className}>
            <h1>
              Your next time slot of {formatTimestampToDate(slot.startTime, userContext.timeZone())}{' '}
              at {formatTimestampToString(slot.startTime, userContext.timeZone())} hasn&apos;t been
              booked.
            </h1>
            <div className="actions">
              <Button variant="secondary-link" onClick={this.openCancelDialog}>
                Cancel Empty Slot
              </Button>
              <Button
                variant="primary-link"
                onClick={this.openPostStoryDialog}
                className="write-story"
              >
                Write a new story
              </Button>
            </div>
          </StyledNextSessionCard>
        )}

        <CancelDialog
          title="Are you sure you want to cancel this reserved slot?"
          buttonLabel="Yes"
          isEnabled={!isLoading}
          open={cancelDialogIsOpen}
          onClose={this.closeCancelDialog}
          onConfirm={this.cancelReservedSlot}
        />

        <PostStoryDialog
          open={postStoryDialogIsOpen}
          onClose={this.closePostStoryDialog}
          location="sessions"
        />
      </React.Fragment>
    );
  }
}

export default withUser(withSnackbar(withQueryLoader(ReservedSlotCard)));
