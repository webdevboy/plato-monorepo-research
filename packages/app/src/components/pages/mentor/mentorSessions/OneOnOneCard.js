import React, { Component } from 'react';

import Button from 'components/shared/Button';
import CancelDialog from './CancelDialog';
import Card from 'components/shared/Card';
import DateTimeWithIcons from 'components/shared/DateTimeWithIcons';
import DaysUntil from 'components/shared/DaysUntil';
import HorizontalCardLayout from 'components/shared/HorizontalCardLayout';
import PersonInfo from 'components/shared/PersonInfo';
import RequestService from 'services/api/plato/request/request';
import getProfilePictureThumbnail from 'utils/thumbnail';
import styled from 'styled-components';
import { trackMentorEventCanceled } from 'utils/analytics';
import { withQueryLoader } from 'components/context/QueryLoaderContext';
import { withSnackbar } from 'components/context/SnackbarContext';
import { withUser } from 'components/context/UserContext';

const LeftWrapper = styled.div`
  .relationship {
    ${({ theme }) => `
      border: 1px solid ${theme.palette.cerulean};
      color: ${theme.palette.cerulean};
      `} width: fit-content;
    padding: 0.5rem 1rem;
    text-transform: uppercase;
    font-weight: bold;
    border-radius: 0.2rem;
    margin-bottom: 1rem;
  }

  ${Button} {
    margin-top: auto;
  }
`;

const RightWrapper = styled.div`
  .challenge {
    padding: 1.5rem;
    margin-top: 2rem;
    border: 1px solid ${({ theme }) => theme.palette.geyser};

    .label,
    .one-liner {
      margin-bottom: 1rem;
    }
  }
`;

const StyledCard = styled(Card)`
  ${LeftWrapper}, ${RightWrapper} {
    display: flex;
    flex-direction: column;
    height: 100%;
  }
`;

const Left = ({ request, onCancelClick }) => (
  <LeftWrapper>
    {request.relationshipId && <div className="relationship">Long-Term Mentorship</div>}
    <DaysUntil timestamp={request.callScheduledAt} />
    <DateTimeWithIcons timestamp={request.callScheduledAt} />
    {request.callScheduledAt > Date.now() && (
      <Button variant="secondary-link" onClick={onCancelClick}>
        I cannot attend anymore
      </Button>
    )}
  </LeftWrapper>
);

const Right = ({
  request,
  request: {
    _mentee: {
      company: { nbEmployees, nbEngineers, location },
      companyName,
      directReportsNb,
      engineeringTeamSize,
      linkedin,
      firstName,
      fullName,
      picture: { thumbnailUrl },
      title,
    },
  },
}) => {
  const about = `
    ${firstName} is a ${title} at ${companyName} (${nbEmployees} employees, ${nbEngineers} engineers, based in ${location}).
    They lead a team of ${engineeringTeamSize} engineers and has ${directReportsNb} direct reports.
  `;

  return (
    <RightWrapper>
      <PersonInfo
        about={about}
        companyName={companyName}
        fullName={fullName}
        linkedin={linkedin}
        thumbnailUrl={getProfilePictureThumbnail(firstName, thumbnailUrl)}
        title={title}
      />

      <div className="challenge">
        <h4 className="label">Challenge</h4>
        <p className="one-liner">
          <strong>{request.briefingOneLiner}</strong>
        </p>
        <p>{request.briefingDescription}</p>
      </div>
    </RightWrapper>
  );
};

class OneOnOneCard extends Component {
  constructor(props) {
    super(props);

    this.state = {
      cancelDialogIsOpen: false,
      saving: false,
    };
  }

  openCancelDialog = () => {
    this.setState({
      cancelDialogIsOpen: true,
    });
  };

  closeCancelDialog = () => {
    this.setState({
      cancelDialogIsOpen: false,
    });
  };

  cancelOneOnOne = async () => {
    const {
      request,
      queryLoader,
      snackbar,
      triggerRefresh,
      userContext: {
        user: { uuid, email, mentorId },
      },
    } = this.props;

    queryLoader.showLoader();
    this.setState({
      saving: true,
    });

    try {
      await RequestService.declineMentor({
        requestId: request.id,
        user: email,
      });

      trackMentorEventCanceled({
        uuid,
        email,
        mentorId,
        eventId: request.id,
        eventType: 'request',
      });

      snackbar.success('Successfully cancelled!');
    } catch (error) {
      console.error(error);
      snackbar.error('Something went wrong - sorry!');
    }

    queryLoader.hideLoader();
    this.setState({ saving: false });
    this.closeCancelDialog();
    triggerRefresh();
  };

  render() {
    const { cancelDialogIsOpen, saving } = this.state;
    const { request } = this.props;

    return (
      <React.Fragment>
        <StyledCard>
          <HorizontalCardLayout
            left={
              <Left className="section" request={request} onCancelClick={this.openCancelDialog} />
            }
            right={<Right className="section" request={request} />}
          />
        </StyledCard>

        <CancelDialog
          title="Are you sure you want to cancel this session?"
          description="When you can't make it, we try to reschedule a new session with you and this mentee."
          buttonLabel="Yes"
          isEnabled={!saving}
          open={cancelDialogIsOpen}
          onClose={this.closeCancelDialog}
          onConfirm={this.cancelOneOnOne}
        />
      </React.Fragment>
    );
  }
}

export default withUser(withSnackbar(withQueryLoader(OneOnOneCard)));
