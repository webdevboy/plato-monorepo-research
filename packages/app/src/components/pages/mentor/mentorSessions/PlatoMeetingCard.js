import React, { Component } from 'react';

import Button from 'components/shared/Button';
import CancelDialog from './CancelDialog';
import Card from 'components/shared/Card';
import DateTimeWithIcons from 'components/shared/DateTimeWithIcons';
import DaysUntil from 'components/shared/DaysUntil';
import EventService from 'services/api/plato/event/event';
import HorizontalCardLayout from 'components/shared/HorizontalCardLayout';
import styled from 'styled-components';
import textToHtml from 'utils/textToHtml';
import { trackMentorEventCanceled } from 'utils/analytics';
import { withQueryLoader } from 'components/context/QueryLoaderContext';
import { withSnackbar } from 'components/context/SnackbarContext';
import { withUser } from 'components/context/UserContext';

const LeftWrapper = styled.div`
  color: ${({ theme }) => theme.palette.white};

  & ${Button} {
    margin-top: auto;
  }
`;

const RightWrapper = styled.div`
  & .title {
    margin-bottom: 1rem;
  }
  & .content {
    margin: auto;
  }
`;

const StyledCard = styled(Card)`
  & .section.left {
    background-color: ${({ theme }) => theme.palette.ebonyClay};
  }

  & ${LeftWrapper}, & ${RightWrapper} {
    display: flex;
    flex-direction: column;
    height: 100%;
    min-height: 20rem;
  }
`;

const Left = ({ slot, onCancelClick }) => (
  <LeftWrapper>
    <DaysUntil timestamp={slot.startTime} />
    <DateTimeWithIcons timestamp={slot.startTime} />
    {slot.startTime > Date.now() && (
      <Button variant="secondary-link" onClick={onCancelClick}>
        Cancel Plato Meeting
      </Button>
    )}
  </LeftWrapper>
);

const Right = ({ slot }) => (
  <RightWrapper>
    <div>
      <h1 className="title">Interview with Plato&apos;s team</h1>
      {textToHtml(slot.description || '')}
    </div>
  </RightWrapper>
);

class PlatoMeetingCard extends Component {
  constructor(props) {
    super(props);

    this.state = {
      cancelDialogIsOpen: false,
    };
  }

  openCancelDialog = () => {
    this.setState({
      cancelDialogIsOpen: true,
    });
  };

  closeCancelDialog = () => {
    this.setState({
      cancelDialogIsOpen: false,
    });
  };

  cancelPlatoMeeting = async () => {
    const {
      slot,
      queryLoader,
      snackbar,
      triggerRefresh,
      userContext: {
        user: { uuid, email, mentorId },
      },
    } = this.props;

    queryLoader.showLoader();
    this.setState({
      saving: true,
    });

    try {
      await EventService.decline({
        eventId: slot.id,
        user: email,
      });

      trackMentorEventCanceled({
        uuid,
        email,
        mentorId,
        eventId: slot.id,
        eventType: 'plato meeting',
      });

      snackbar.success('Successfully cancelled!');
    } catch (error) {
      console.error(error);
      snackbar.error('Something went wrong - sorry!');
    }

    queryLoader.hideLoader();
    this.setState({ saving: false });
    this.closeCancelDialog();
    triggerRefresh();
  };

  render() {
    const { slot, className } = this.props;
    const { cancelDialogIsOpen, saving } = this.state;

    return (
      <React.Fragment>
        <StyledCard className={className}>
          <HorizontalCardLayout
            left={<Left slot={slot} onCancelClick={this.openCancelDialog} />}
            right={<Right slot={slot} />}
          />
        </StyledCard>

        <CancelDialog
          title="Are you sure you want to cancel this Plato meeting?"
          buttonLabel="Yes"
          open={cancelDialogIsOpen}
          onClose={this.closeCancelDialog}
          onConfirm={this.cancelPlatoMeeting}
          isEnabled={!saving}
        />
      </React.Fragment>
    );
  }
}

export default withUser(withSnackbar(withQueryLoader(PlatoMeetingCard)));
