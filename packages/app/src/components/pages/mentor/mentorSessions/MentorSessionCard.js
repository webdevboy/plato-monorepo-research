import React from 'react';
import OneOnOneCard from './OneOnOneCard';
import AmaCard from './AmaCard';
import ReservedSlotCard from './ReservedSlotCard';
import PlatoMeetingCard from './PlatoMeetingCard';

const MentorSessionCard = ({ session, session: { _type }, ...other }) => (
  <React.Fragment>
    {_type === 'oneOnOne' && <OneOnOneCard request={session} {...other} />}
    {_type === 'ama' && <AmaCard ama={session} {...other} />}
    {_type === 'open' && <ReservedSlotCard slot={session} {...other} />}
    {_type === 'platoMeeting' && <PlatoMeetingCard slot={session} {...other} />}
  </React.Fragment>
);

export default MentorSessionCard;
