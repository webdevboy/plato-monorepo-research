import AmaService from 'services/api/plato/ama/ama';
import MenteeService from 'services/api/plato/mentee/mentee';
import MentorService from 'services/api/plato/mentor/mentor';
import moment from 'moment';

const reservedSlotName = 'Plato reserved slot';
const specialSlotName = 'Plato special slot';
const storyGatheringSlotName = 'Plato story gathering call';

const getAdditionalRequestInfo = async requests => {
  const mentees = await Promise.all(
    requests.map(request => MenteeService.getMentee(request.menteeId))
  );

  requests.forEach((request, index) => {
    request._mentee = mentees[index]; // eslint-disable-line no-param-reassign
  });
};

const getAdditionalAmaInfo = async amas => {
  const attendances = await Promise.all(
    amas.map(ama => AmaService.getScheduledAttendances(ama.id))
  );

  amas.forEach((ama, index) => {
    ama._attendances = attendances[index]; // eslint-disable-line no-param-reassign
  });
};

export default {
  async getAmas(mentorId) {
    const amas = await MentorService.getAmas({
      mentorId,
      scheduled: true,
      closed: true,
      done: true,
      error: false,
      minDate: moment()
        .add(-4, 'months')
        .toISOString(true),
      maxDate: moment()
        .add(4, 'months')
        .toISOString(true),
    }).then(({ result }) => result);

    amas.forEach(ama => {
      ama._type = 'ama'; // eslint-disable-line no-param-reassign
    });

    await getAdditionalAmaInfo(amas);

    return amas;
  },

  async getRequests(mentorId) {
    const requests = await MentorService.getRequests({
      mentorId,
      scheduled: true,
      closed: true,
      done: true,
      error: false,
      scMentee: false,
      scMentor: false,
    }).then(({ result }) => result);

    requests.forEach(request => {
      request._type = 'oneOnOne'; // eslint-disable-line no-param-reassign
    });

    await getAdditionalRequestInfo(requests);

    return requests;
  },

  async getSlots(mentorId) {
    const slots = await MentorService.getEvents({
      mentorId,
      minDate: moment()
        .add(-4, 'months')
        .toISOString(true),
      maxDate: moment()
        .add(4, 'months')
        .toISOString(true),
      isDeclined: false,
    }).then(({ result }) =>
      result.filter(
        event =>
          !event.ama &&
          !event.request &&
          !(event.summary !== storyGatheringSlotName && moment(event.startTime).isBefore(moment()))
      )
    );

    slots.forEach(slot => {
      switch (slot.summary) {
        case storyGatheringSlotName:
          slot._type = 'platoMeeting'; // eslint-disable-line no-param-reassign
          break;
        case reservedSlotName:
        case specialSlotName:
          slot._type = 'open'; // eslint-disable-line no-param-reassign
          break;
        default:
          slot._type = undefined; // eslint-disable-line no-param-reassign
      }
    });

    return slots;
  },
};
