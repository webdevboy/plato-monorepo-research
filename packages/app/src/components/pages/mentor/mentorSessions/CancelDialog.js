import React, { Component } from 'react';

import Button from 'components/shared/Button';
import PlatoDialog from 'components/shared/PlatoDialog';
import styled from 'styled-components';

const Body = styled.div`
  padding: 3rem;
  max-width: 600px;

  & h1 {
    margin-bottom: 1rem;
  }

  & p {
    margin-bottom: 3rem;
  }

  & ${Button} {
    display: block;
    margin-left: auto;
  }
`;

export default class CancelDialog extends Component {
  render() {
    const { buttonLabel, description, isEnabled, onClose, onConfirm, open, title } = this.props;
    return (
      <PlatoDialog open={open} onClose={onClose}>
        <Body>
          <h1>{title}</h1>
          {description && <p>{description}</p>}
          <Button variant="primary" onClick={onConfirm} disabled={!isEnabled}>
            {buttonLabel}
          </Button>
        </Body>
      </PlatoDialog>
    );
  }
}
