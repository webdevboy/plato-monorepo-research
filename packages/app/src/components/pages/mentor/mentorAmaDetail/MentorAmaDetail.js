import React, { Component } from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import Hero from 'components/shared/Hero';
import Layout from 'components/shared/Layout';
import { withUser } from 'components/context/UserContext';
import PageLoadingSpinner from 'components/shared/PageLoadingSpinner';
import SessionHeader from 'components/shared/SessionHeader';
import { withSnackbar } from 'components/context/SnackbarContext';
import IconLabel from 'components/shared/IconLabel';
import amaService from 'services/api/plato/ama/ama';
import Card from 'components/shared/Card';
import AmaAttendances from 'components/shared/AmaAttendances';
import AmaBasicInfo from 'components/shared/AmaBasicInfo';

const StyledMentorSession = styled.div`
  & ${Layout} {
    position: relative;
    top: 6rem;
    margin-bottom: 20rem;
  }

  & .back-button {
    font-size: 14px;
    font-weight: bold;
    text-transform: uppercase;
    color: ${({ theme }) => theme.palette.white};
  }
`;

const StyledColumns = styled.div`
  margin-top: 12rem;
  display: flex;
`;

const LeftCard = styled(Card)`
  width: 265px;
  margin-right: 20px;
  padding: 20px;

  & h4 {
    margin: 20px 0 15px;
    font-size: 14px;

    &:nth-of-type(1) {
      margin-top: 0;
    }
  }

  & .info {
    margin-bottom: 10px;
  }
`;

const RightWrapper = styled.div`
  padding-top: 3rem;
`;

class MentorSession extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ama: null,
      attendances: null,
      amaStatus: null,
      isLoading: false,
      storyName: null,
      storyTagNames: [],
      callScheduledAt: null,
      zoomLink: null,
      storyId: null,
    };
  }

  async componentDidMount() {
    const {
      match: {
        params: { amaId },
      },
    } = this.props;

    this.setState({ isLoading: true });

    try {
      const [ama, attendees] = await Promise.all([
        amaService.getAma(amaId),
        amaService.getAttendances(amaId),
      ]);

      const filteredAttendances = attendees.filter(attendee => attendee.status === 'Scheduled');

      const { storyId, storyName, storyTagNames, callScheduledAt, zoomLink, status } = ama;
      this.setState({
        ama,
        storyId,
        storyName,
        storyTagNames,
        callScheduledAt: new Date(callScheduledAt),
        zoomLink,
        attendances: filteredAttendances,
        amaStatus: status,
      });
    } catch (error) {
      console.error(error);
      this.props.snackbar.error('Something went wrong - sorry!');
    }

    this.setState({ isLoading: false });
  }

  render() {
    const {
      ama,
      attendances,
      isLoading,
      storyId,
      storyName,
      storyTagNames,
      callScheduledAt,
      amaStatus,
      zoomLink,
    } = this.state;

    if (isLoading) {
      return (
        <StyledMentorSession>
          <Hero />
          <PageLoadingSpinner />
        </StyledMentorSession>
      );
    }

    if (!attendances) {
      return <div />;
    }

    return (
      <StyledMentorSession>
        <Hero />
        <Layout width="1025">
          <Link to="/mentor_sessions" className="back-button">
            <IconLabel icon="arrow-left">Back</IconLabel>
          </Link>
          <SessionHeader
            name={storyName}
            tags={storyTagNames}
            startAt={callScheduledAt}
            zoomLink={zoomLink}
            status={amaStatus}
          />
          <StyledColumns>
            <div className="left">
              <LeftCard>
                <AmaBasicInfo
                  isMentor
                  storyId={storyId}
                  storyName={storyName}
                  callScheduledAt={callScheduledAt}
                />
              </LeftCard>
            </div>
            <div className="right">
              <RightWrapper>
                <AmaAttendances ama={ama} />
              </RightWrapper>
            </div>
          </StyledColumns>
        </Layout>
      </StyledMentorSession>
    );
  }
}

export default withSnackbar(withUser(MentorSession));
