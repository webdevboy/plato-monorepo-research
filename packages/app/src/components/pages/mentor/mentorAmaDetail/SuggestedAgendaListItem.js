import React, { Component } from 'react';
import styled from 'styled-components';
import IconLabel from 'components/shared/IconLabel';

const StyledSuggestedAgendaListItem = styled.li`
  font-size: 14px;
  display: flex;
  align-items: baseline;
  margin: 10px 0;

  & .clock-icon {
    color: ${props => props.theme.palette.cadetBlue};
    text-align: right;
    margin-top: 2px;
  }

  & .title {
    width: 180px;
  }
`;

class SuggestedAgendaListItem extends Component {
  render() {
    const { children, minutes } = this.props;
    return (
      <StyledSuggestedAgendaListItem>
        <p className="title">{children}</p>
        <IconLabel icon="clock" className="clock-icon">
          {minutes}
          &apos;
        </IconLabel>
      </StyledSuggestedAgendaListItem>
    );
  }
}

export default styled(SuggestedAgendaListItem)``;
