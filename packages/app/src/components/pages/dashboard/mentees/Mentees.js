import React, { Component } from 'react';
import {
  fetchDashboardMentees,
  fetchDashboardSeats,
  setIsAddMenteeDialogOpen,
  updateDashboardMenteesQuery,
  updateDashboardMenteesSortBy,
} from 'ducks';
import {
  getActiveDashboardMenteeCount,
  getDashboardMentees,
  getDashboardMenteesQuery,
  getDashboardMenteesSortBy,
  getDashboardSeats,
  getDashboardSlackUsers,
  getIsAddMenteeDialogOpen,
  getSubscriptions,
} from 'selectors';

import AddMenteeDialog from './dialogs/AddMenteeDialog';
import CardGrid from 'components/pages/dashboard/shared/CardGrid';
import HeaderWrapper from './header/HeaderWrapper';
import Hero from 'components/shared/Hero';
import HorizontalSplit from 'components/shared/HorizontalSplit';
import Layout from 'components/shared/Layout';
import MenteeCard from 'components/pages/dashboard/mentees/cards/MenteeCard';
import MenteeFilters from './filters/MenteeFilters';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import getProfilePictureThumbnail from 'utils/thumbnail';
import { withSnackbar } from 'components/context/SnackbarContext';
import { withUser } from 'components/context/UserContext';

class Mentees extends Component {
  componentDidMount() {
    const { organizationId } = this.props.userContext.dashboardUser;

    this.props.onFetchDashboardMentees({
      organizationId,
      shouldLoadFakeData: this.shouldLoadFakeData,
    });

    this.props.onFetchSeats({
      organizationId,
      shouldLoadFakeData: this.shouldLoadFakeData,
    });
  }

  onAddSeatClick = () => {
    const { snackbar } = this.props;
    snackbar.info(
      <>
        This feature is in development. Please contact{' '}
        <span className="bold">sales@platohq.com</span> to add seats.
      </>
    );
  };

  onAddMenteeClick = () => {
    const { seats, snackbar, subscriptions, slackUsers, onSetIsAddMenteeDialogOpen } = this.props;
    const { nbPaidSeats, nbTakenSeats } = seats;

    if (nbPaidSeats - nbTakenSeats <= 0) {
      return snackbar.warn('No available seats!');
    }

    if (subscriptions.length === 0 || subscriptions[0].items.length === 0) {
      return snackbar.warn(
        `Your company has no Plato subscriptions. You can't add any mentees at this time`
      );
    }

    if (slackUsers.length === 0) {
      return snackbar.warn('All the Slack users in your organization are already mentees!');
    }

    return onSetIsAddMenteeDialogOpen(true);
  };

  closeAddMenteeDialog = () => {
    this.props.onSetIsAddMenteeDialogOpen(false);
  };

  onSearchChange = event => {
    this.props.onUpdateDashboardMenteesQuery(event.target.value);
  };

  onSortByChange = event => {
    this.props.onUpdateDashboardMenteesSortBy(event.target.value);
  };

  render() {
    const {
      dashboardMentees,
      seats,
      sortBy,
      activeMenteeCount,
      isAddMenteeDialogOpen,
    } = this.props;

    return (
      <>
        <Hero height="175" />
        <Layout className="pb5">
          <HeaderWrapper
            className="mb5"
            nbActiveMentees={activeMenteeCount}
            nbAvailableSeats={Math.max(0, seats.nbPaidSeats - seats.nbTakenSeats)}
            onAddMenteeClick={this.onAddMenteeClick}
            onAddSeatClick={this.onAddSeatClick}
          />

          <HorizontalSplit
            leftSide={
              <MenteeFilters
                onSearchChange={this.onSearchChange}
                sortBy={sortBy}
                onSortByChange={this.onSortByChange}
              />
            }
            rightSide={
              <CardGrid columns={3}>
                {dashboardMentees.map(({ mentee, averageGrade, numMeetings }) => (
                  <MenteeCard
                    key={mentee.id}
                    fullName={mentee.fullName}
                    title={mentee.title}
                    imageUrl={getProfilePictureThumbnail(
                      mentee.fullName,
                      mentee.picture.originalUrl
                    )}
                    nbMeetings={numMeetings}
                    averageGrade={averageGrade}
                    isOnboarded={mentee.isProfileComplete}
                  />
                ))}
              </CardGrid>
            }
            leftColumns={1}
            rightColumns={3}
            breakpoint={768}
          />
        </Layout>
        <AddMenteeDialog open={isAddMenteeDialogOpen} onClose={this.closeAddMenteeDialog} />
      </>
    );
  }
}

Mentees.propTypes = {
  dashboardMentees: PropTypes.array.isRequired,
  seats: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  return {
    dashboardMentees: getDashboardMentees(state),
    sortBy: getDashboardMenteesSortBy(state),
    seats: getDashboardSeats(state),
    query: getDashboardMenteesQuery(state),
    activeMenteeCount: getActiveDashboardMenteeCount(state),
    subscriptions: getSubscriptions(state),
    slackUsers: getDashboardSlackUsers(state),
    isAddMenteeDialogOpen: getIsAddMenteeDialogOpen(state),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    onFetchDashboardMentees: ({ organizationId, shouldLoadFakeData }) =>
      dispatch(fetchDashboardMentees({ organizationId, shouldLoadFakeData })),
    onFetchSeats: ({ organizationId, shouldLoadFakeData }) =>
      dispatch(fetchDashboardSeats({ organizationId, shouldLoadFakeData })),
    onUpdateDashboardMenteesSortBy: sortBy => dispatch(updateDashboardMenteesSortBy(sortBy)),
    onUpdateDashboardMenteesQuery: query => dispatch(updateDashboardMenteesQuery(query)),
    onSetIsAddMenteeDialogOpen: isOpen => dispatch(setIsAddMenteeDialogOpen(isOpen)),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withUser(withSnackbar(Mentees)));
