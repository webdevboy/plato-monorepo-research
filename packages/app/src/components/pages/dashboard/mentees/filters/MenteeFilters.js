import React from 'react';
import SearchInput from 'components/shared/SearchInput';
import SortByDropdown from './SortByDropdown';

const MenteeFilters = ({ onSearchChange, sortBy, onSortByChange }) => (
  <>
    <SearchInput className="mb4" name="search" onChange={onSearchChange} placeholder="Search" />
    <SortByDropdown onChange={onSortByChange} value={sortBy} />
  </>
);

export default MenteeFilters;
