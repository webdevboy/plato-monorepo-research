import { MenuItem } from '@material-ui/core';
import React from 'react';
import Select from 'components/shared/Select';
import styled from 'styled-components';

const StyledSelect = styled(Select)`
  width: 100%;
`;

const SortByDropdown = ({ onChange, value }) => (
  <StyledSelect onChange={onChange} value={value} label="Sort by">
    <MenuItem value="engagement">Engagement</MenuItem>
    <MenuItem value="satisfaction">Satisfaction</MenuItem>
  </StyledSelect>
);

export default SortByDropdown;
