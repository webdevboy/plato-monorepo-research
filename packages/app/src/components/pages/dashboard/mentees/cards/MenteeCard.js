import Avatar from 'components/shared/Avatar';
import Card from 'components/shared/Card';
import PropTypes from 'prop-types';
import React from 'react';
import StatisticRow from 'components/pages/dashboard/mentees/cards/StatisticRow';
import styled from 'styled-components';

const StyledCard = styled(Card)`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  width: 100%;
  max-width: 30rem;

  .top,
  .bottom {
    padding: var(--x3);
  }

  .top {
    display: flex;
    flex-direction: column;
    align-items: center;

    .name {
      font-size: var(--h2);
      text-align: center;
    }
  }

  .bottom {
    border-top: 1px solid ${props => props.theme.palette.geyser};
    background: ${props => props.theme.palette.aquaHaze};
    width: 100%;

    .not-onboarded {
      color: ${props => props.theme.palette.osloGrey};
      font-size: var(--h4);
      font-style: italic;
      line-height: 2.3;
      margin: calc(var(--x1) / 2) 0;
      text-align: center;
    }
  }
`;

const MenteeCard = ({ fullName, title, imageUrl, nbMeetings, averageGrade, isOnboarded }) => (
  <StyledCard>
    <div className="top">
      <Avatar src={imageUrl} size="xl" className="mb3" />
      <p className="name bold">{fullName}</p>
      <p className="title">{title}</p>
    </div>

    <div className="bottom">
      {isOnboarded && (
        <>
          <StatisticRow className="mb1" label="Meetings" value={nbMeetings} />
          <StatisticRow label="Average grade" value={averageGrade} />
        </>
      )}
      {!isOnboarded && <p className="not-onboarded">This user hasn&apos;t been onboarded yet.</p>}
    </div>
  </StyledCard>
);

MenteeCard.propTypes = {
  fullName: PropTypes.string.isRequired,
  imageUrl: PropTypes.string.isRequired,
  nbMeetings: PropTypes.number.isRequired,
  averageGrade: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
};

MenteeCard.defaultProps = {
  averageGrade: '-',
};

export default MenteeCard;
