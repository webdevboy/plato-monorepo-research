import PropTypes from 'prop-types';
import React from 'react';
import styled from 'styled-components';

const StyledStatisticRow = styled.div`
  display: flex;
  justify-content: space-between;
  font-size: var(--h4);

  .label {
    color: ${props => props.theme.palette.osloGrey};
  }

  .value {
    color: ${props => props.theme.palette.java};
  }
`;

const StatisticRow = ({ label, value, className }) => (
  <StyledStatisticRow className={className}>
    <span className="label">{label}</span>
    <span className="value bold">{value}</span>
  </StyledStatisticRow>
);

StatisticRow.propTypes = {
  label: PropTypes.string.isRequired,
  value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
  className: PropTypes.string,
};

StatisticRow.defaultProps = {
  className: '',
};

export default StatisticRow;
