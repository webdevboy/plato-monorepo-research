import HeaderText from '../../shared/HeaderText';
import Pluralize from 'react-pluralize';
import PropTypes from 'prop-types';
import React from 'react';

const ActiveMenteesHeaderText = ({ nbActiveMentees }) => (
  <HeaderText className="mb0 mt0">
    There <Pluralize count={nbActiveMentees} singular="is" plural="are" showCount={false} />{' '}
    currently{' '}
    <span className="bold">
      <Pluralize singular="active mentee" count={nbActiveMentees} />
    </span>
  </HeaderText>
);

ActiveMenteesHeaderText.propTypes = {
  nbActiveMentees: PropTypes.number.isRequired,
};

export default ActiveMenteesHeaderText;
