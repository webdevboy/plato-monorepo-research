import ActiveMenteesHeaderText from './ActiveMenteesHeaderText';
import AvailableSeats from './AvailableSeats';
import Button from 'components/shared/Button';
import PropTypes from 'prop-types';
import React from 'react';
import styled from 'styled-components';

const StyledHeaderWrapper = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 0 var(--x1);
  top: var(--x4);
  padding-bottom: var(--x4);

  .left-container {
    display: flex;
    flex-direction: column;
    align-items: center;
    margin-bottom: var(--x2);
  }

  .add-mentee {
    margin-left: var(--x3);
  }

  @media (min-width: 867px) {
    flex-direction: row;
    justify-content: space-between;
    align-items: flex-start;
    top: var(--x5);

    .left-container {
      align-items: flex-start;
    }
  }
`;

const HeaderWrapper = ({
  nbActiveMentees,
  nbAvailableSeats,
  className,
  onAddSeatClick,
  onAddMenteeClick,
}) => (
  <StyledHeaderWrapper className={className}>
    <div className="left-container">
      <ActiveMenteesHeaderText nbActiveMentees={nbActiveMentees} />
      <AvailableSeats nbAvailableSeats={nbAvailableSeats} />
    </div>

    <div>
      <Button variant="secondary" onClick={onAddSeatClick}>
        Add Seat(s)
      </Button>
      <Button className="add-mentee" variant="primary" onClick={onAddMenteeClick}>
        Add Mentee(s)
      </Button>
    </div>
  </StyledHeaderWrapper>
);

HeaderWrapper.propTypes = {
  nbActiveMentees: PropTypes.number.isRequired,
  nbAvailableSeats: PropTypes.number.isRequired,
  className: PropTypes.string,
  onAddSeatClick: PropTypes.func.isRequired,
  onAddMenteeClick: PropTypes.func.isRequired,
};

HeaderWrapper.defaultProps = {
  className: '',
};

export default styled(HeaderWrapper)``;
