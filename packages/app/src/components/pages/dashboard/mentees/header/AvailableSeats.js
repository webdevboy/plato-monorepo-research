import Pluralize from 'react-pluralize';
import PropTypes from 'prop-types';
import React from 'react';
import styled from 'styled-components';

const StyledAvailableSeats = styled.p`
  color: ${props => props.theme.palette.cerulean};
`;

const AvailableSeats = ({ nbAvailableSeats }) => (
  <StyledAvailableSeats className="bold">
    <Pluralize count={nbAvailableSeats} singular="available seat" />
  </StyledAvailableSeats>
);

AvailableSeats.propTypes = {
  nbAvailableSeats: PropTypes.number.isRequired,
};

export default AvailableSeats;
