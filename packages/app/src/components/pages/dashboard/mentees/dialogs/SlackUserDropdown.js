import React, { Component } from 'react';

import { MenuItem } from '@material-ui/core';
import Select from 'components/shared/Select';
import { connect } from 'react-redux';
import { fetchDashboardSlackUsers } from 'ducks';
import { getDashboardSlackUsers } from 'selectors';
import styled from 'styled-components';

class SlackUserDropdown extends Component {
  componentDidMount() {
    const { onFetchSlackUsers, organizationId } = this.props;
    onFetchSlackUsers({ organizationId });
  }

  render() {
    const { slackUsers, name, onChange, value, className } = this.props;

    return (
      <Select
        className={className}
        name={name}
        onChange={onChange}
        value={value}
        label="Slack User"
      >
        {slackUsers
          .filter(slackUser => slackUser.fullName && slackUser.fullName.trim())
          .sort((slackUser1, slackUser2) => slackUser1.fullName.localeCompare(slackUser2.fullName))
          .map(slackUser => (
            <MenuItem key={slackUser.id} value={slackUser.id}>
              {`${slackUser.fullName} (${slackUser.email})`}
            </MenuItem>
          ))}
        {slackUsers
          .filter(slackUser => !slackUser.fullName || !slackUser.fullName.trim())
          .sort((slackUser1, slackUser2) => slackUser1.email.localeCompare(slackUser2.email))
          .map(slackUser => (
            <MenuItem key={slackUser.id} value={slackUser.id}>
              {slackUser.email}
            </MenuItem>
          ))}
      </Select>
    );
  }
}

function mapStateToProps(state) {
  return {
    slackUsers: getDashboardSlackUsers(state),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    onFetchSlackUsers: payload => dispatch(fetchDashboardSlackUsers(payload)),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(styled(SlackUserDropdown)``);
