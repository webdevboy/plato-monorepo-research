import { MenuItem } from '@material-ui/core';
import React from 'react';
import Select from 'components/shared/Select';
import { connect } from 'react-redux';
import { getFaceTags } from 'selectors';

const FacesDropdown = ({ className, name, value, onChange, faces }) => (
  <Select label="Role" className={className} name={name} value={value} onChange={onChange}>
    {faces.map(face => (
      <MenuItem key={face.id} value={face.id}>
        {face.name}
      </MenuItem>
    ))}
  </Select>
);

function mapStateToProps(state) {
  return {
    faces: getFaceTags(state),
  };
}

export default connect(mapStateToProps)(FacesDropdown);
