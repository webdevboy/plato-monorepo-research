import * as Yup from 'yup';

import { Form, Formik } from 'formik';
import React, { Component } from 'react';
import {
  addDashboardMentee,
  fetchDashboardSlackUserUser,
  fetchDashboardSlackUsers,
  fetchSubscriptions,
} from 'ducks';
import { getDashboardSlackUsers, getSubscriptions, isActionLoading } from 'selectors';

import Button from 'components/shared/Button';
import FacesDropdown from 'components/pages/dashboard/mentees/dialogs/FacesDropdown';
import PlatoDialog from 'components/shared/PlatoDialog';
import SlackUserDropdown from './SlackUserDropdown';
import TextInput from 'components/shared/TextInput';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { withUser } from 'components/context/UserContext';

const Body = styled.div`
  width: 42rem;
  padding: var(--x3);

  ${SlackUserDropdown}, .faces {
    width: 100%;
    margin-bottom: var(--x3);
    height: 4.5rem;
  }

  ${Button} {
    position: relative;
    left: 100%;
    transform: translateX(-100%);
  }
`;

const FormSchema = Yup.object().shape({
  firstName: Yup.string().required('Required'),
  lastName: Yup.string().required('Required'),
  email: Yup.string()
    .email('Invalid email')
    .required('Required'),
  faceId: Yup.number().required('Required'),
});

class AddMenteeDialog extends Component {
  constructor(props) {
    super(props);

    this.state = {
      slackUserId: undefined,
    };
  }

  componentDidMount() {
    const { organizationId } = this.props.userContext.dashboardUser;
    this.props.onFetchSubscriptions({ organizationId });
    this.props.onFetchSlackUsers({ organizationId });
  }

  componentDidUpdate(prevProps, prevState) {
    const { slackUserId } = this.state;
    const { userContext } = this.props;
    const { organizationId } = userContext.dashboardUser;

    if (slackUserId && slackUserId !== prevState.slackUserId) {
      const { email } = this.getSlackUser();
      this.props.onFetchSlackUserUser({ organizationId, email });
    }
  }

  getSlackUser = () => {
    const { slackUsers } = this.props;
    const { slackUserId } = this.state;

    return slackUsers.filter(user => user.id === slackUserId)[0];
  };

  handleChange = event => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleSubmit = (values, actions) => {
    const { organizationId } = this.props.userContext.dashboardUser;
    const subscriptionItemId = this.props.subscriptions[0].items[0].id;
    const { firstName, lastName, email, faceId } = values;
    const slackUser = this.getSlackUser();

    this.props.onAddMentee({
      companyId: organizationId,
      subscriptionItemId,
      firstName,
      lastName,
      email,
      faceIds: [faceId],
      slackUserId: slackUser.id,
      userId: slackUser.user.id,
      callback: () => {
        actions.setSubmitting(false);
      },
    });
  };

  render() {
    const { open, onClose, userContext } = this.props;
    const { slackUserId } = this.state;

    const slackUser = this.getSlackUser();
    const isFormEnabled = Boolean(slackUser && !slackUser.user.id);

    const initialValues = {
      firstName: (slackUser && slackUser.user.firstName) || '',
      lastName: (slackUser && slackUser.user.lastName) || '',
      email: (slackUser && (slackUser.user.email || slackUser.email)) || '',
      faceId: '',
    };

    return (
      <PlatoDialog open={open} onClose={onClose}>
        <Body>
          <h1>Add Mentee</h1>
          <Formik
            onChange={this.handleChange}
            onSubmit={this.handleSubmit}
            validationSchema={FormSchema}
            initialValues={initialValues}
            enableReinitialize
            isInitialValid={props => FormSchema.isValidSync(props.initialValues)}
          >
            {({ values, handleChange, isSubmitting, isValid }) => {
              return (
                <Form>
                  <SlackUserDropdown
                    name="slackUserId"
                    onChange={this.handleChange}
                    value={slackUserId || ''}
                    organizationId={userContext.dashboardUser.organizationId}
                  />

                  <TextInput
                    value={values.firstName}
                    type="text"
                    name="firstName"
                    label="First Name"
                    onChange={handleChange}
                    isEnabled={isFormEnabled}
                  />

                  <TextInput
                    value={values.lastName}
                    type="text"
                    name="lastName"
                    label="Last Name"
                    onChange={handleChange}
                    isEnabled={isFormEnabled}
                  />

                  <TextInput
                    value={values.email}
                    type="email"
                    name="email"
                    label="Email"
                    onChange={handleChange}
                    isEnabled={isFormEnabled}
                  />

                  <FacesDropdown
                    className="faces"
                    name="faceId"
                    onChange={handleChange}
                    value={values.faceId}
                  />

                  <Button type="submit" variant="primary" disabled={isSubmitting || !isValid}>
                    Add Mentee
                  </Button>
                </Form>
              );
            }}
          </Formik>
        </Body>
      </PlatoDialog>
    );
  }
}

function mapStateToProps(state) {
  return {
    slackUsers: getDashboardSlackUsers(state),
    subscriptions: getSubscriptions(state),
    isLoading: isActionLoading(state, addDashboardMentee),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    onFetchSlackUsers: payload => dispatch(fetchDashboardSlackUsers(payload)),
    onFetchSlackUserUser: payload => dispatch(fetchDashboardSlackUserUser(payload)),
    onFetchSubscriptions: payload => dispatch(fetchSubscriptions(payload)),
    onAddMentee: payload => dispatch(addDashboardMentee(payload)),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withUser(AddMenteeDialog));
