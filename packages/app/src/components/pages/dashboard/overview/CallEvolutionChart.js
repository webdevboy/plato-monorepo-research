import { Bar, BarChart, CartesianGrid, Legend, Tooltip, XAxis, YAxis } from 'recharts';
import styled, { withTheme } from 'styled-components';

import Card from 'components/shared/Card';
import OverviewLabel from './OverviewLabel';
import PropTypes from 'prop-types';
import React from 'react';

const StyledCard = styled(Card)`
  padding: var(--x3);
`;

const CallEvolutionChart = ({ data, theme, className }) => (
  <StyledCard className={className}>
    <div className="label-container">
      <OverviewLabel>Call Evolution</OverviewLabel>
    </div>

    <BarChart width={1000} height={300} data={data}>
      <CartesianGrid strokeDasharray="3 3" />
      <XAxis dataKey="period" />
      <YAxis />
      <Tooltip />
      <Legend verticalAlign="top" align="right" height={30} iconSize={15} iconType="square" />
      <Bar dataKey="oneOnOnes" name="One-on-Ones" fill={theme.palette.black} />
      <Bar dataKey="amas" name="AMAs" fill={theme.palette.java} />
    </BarChart>
  </StyledCard>
);

CallEvolutionChart.propTypes = {
  data: PropTypes.array.isRequired,
  theme: PropTypes.object.isRequired,
  className: PropTypes.string,
};

CallEvolutionChart.defaultProps = {
  className: '',
};

export default withTheme(CallEvolutionChart);
