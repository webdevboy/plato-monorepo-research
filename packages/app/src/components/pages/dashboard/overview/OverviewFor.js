import HeaderText from 'components/pages/dashboard/shared/HeaderText';
import PropTypes from 'prop-types';
import React from 'react';
import styled from 'styled-components';

const StyledHeader = styled.header`
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

const OverviewFor = ({ period }) => (
  <StyledHeader className="mb2">
    <HeaderText>
      Overview for <span className="bold">{period}</span>
    </HeaderText>
  </StyledHeader>
);

OverviewFor.propTypes = {
  period: PropTypes.string.isRequired,
};

export default OverviewFor;
