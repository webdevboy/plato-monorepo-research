import React, { Component } from 'react';
import {
  fetchDashboardCommonChallenges,
  fetchDashboardQualitativeFeedback,
  fetchDashboardSeats,
  fetchDashboardStats,
} from 'ducks/dashboard';
import {
  getDashboardCommonChallenges,
  getDashboardQualitativeFeedback,
  getDashboardSeats,
  getDashboardStats,
} from 'selectors/dashboard';

import AverageGradeCard from './cards/AverageGradeCard';
import CallEvolutionChart from './CallEvolutionChart';
import CallsHappenedCard from './cards/CallsHappenedCard';
import CardGrid from '../shared/CardGrid';
import CommonChallengesCard from './cards/CommonChallengesCard';
import Hero from 'components/shared/Hero';
import HorizontalSplit from 'components/shared/HorizontalSplit';
import Layout from 'components/shared/Layout';
import OnboardedCard from './cards/OnboardedCard';
import OverviewFor from './OverviewFor';
import PropTypes from 'prop-types';
import QualitativeFeedbackCard from './cards/QualitativeFeedbackCard';
import SeatsTakenCard from './cards/SeatsTakenCard';
import { connect } from 'react-redux';
import moment from 'moment';
import queryString from 'query-string';
import roundTo from 'round-to';
import styled from 'styled-components';
import { withSnackbar } from 'components/context/SnackbarContext';
import { withUser } from 'components/context/UserContext';

const StyledWrapper = styled.div`
  ${Layout} {
    position: relative;
    top: var(--x5);
  }
`;

class Overview extends Component {
  constructor(props) {
    super(props);

    const {
      location: { search },
    } = props;

    this.shouldLoadFakeData = queryString.parse(search).fake;
  }

  componentDidMount() {
    const { organizationId } = this.props.userContext.dashboardUser;
    this.props.onFetchStats({
      organizationId,
      startDate: moment()
        .subtract(36, 'months')
        .toISOString(),
      endDate: moment().toISOString(),
      shouldLoadFakeData: this.shouldLoadFakeData,
    });

    this.props.onFetchSeats({
      organizationId,
      shouldLoadFakeData: this.shouldLoadFakeData,
    });
    this.props.onFetchCommonChallenges({
      organizationId,
      shouldLoadFakeData: this.shouldLoadFakeData,
    });
    this.props.onFetchQualitativeFeedback({
      organizationId,
      shouldLoadFakeData: this.shouldLoadFakeData,
    });
  }

  getNbCallsHappened = (startPeriod, endPeriod) => {
    const { stats } = this.props;

    const startIndex = stats.indexOf(this.getStatsForMonth(startPeriod.month, startPeriod.year));
    const endIndex = stats.indexOf(this.getStatsForMonth(endPeriod.month, endPeriod.year));

    const statsInRange = stats.slice(startIndex, endIndex + 1);
    return statsInRange.reduce(
      (acc, periodStats) =>
        acc + periodStats.nbCallDoneRequests + periodStats.nbCallDoneAttendances,
      0
    );
  };

  getAverageGrade = (startPeriod, endPeriod) => {
    const { stats } = this.props;

    const startIndex = stats.indexOf(this.getStatsForMonth(startPeriod.month, startPeriod.year));
    const endIndex = stats.indexOf(this.getStatsForMonth(endPeriod.month, endPeriod.year));

    const statsInRange = stats.slice(startIndex, endIndex + 1);
    if (statsInRange.length === 0 || this.getNbCallsHappened(startPeriod, endPeriod) === 0) {
      return '-';
    }

    const averageParts = statsInRange.reduce(
      (
        acc,
        { nbGradedRequests, nbGradedAttendances, requestGradeAverage, attendanceGradeAverage }
      ) => {
        const totalGradedThisPeriod = nbGradedRequests + nbGradedAttendances;
        if (totalGradedThisPeriod === 0) {
          return acc;
        }

        const averageGradeThisPeriod =
          (requestGradeAverage * nbGradedRequests + attendanceGradeAverage * nbGradedAttendances) /
          totalGradedThisPeriod;

        return {
          weightedAveragesSum:
            acc.weightedAveragesSum + averageGradeThisPeriod * totalGradedThisPeriod,
          totalNbGradedCalls: acc.totalNbGradedCalls + totalGradedThisPeriod,
        };
      },
      { weightedAveragesSum: 0, totalNbGradedCalls: 0 }
    );

    if (averageParts.totalNbGradedCalls === 0) {
      return null;
    }

    return roundTo(averageParts.weightedAveragesSum / averageParts.totalNbGradedCalls, 1);
  };

  getMomentDate = dateStr => {
    return moment(dateStr, 'MM/YYYY');
  };

  getStatsForMonth = (month, year) => {
    return this.props.stats.filter(stat => {
      const date = this.getMomentDate(stat.month);
      return date.month() === month && date.year() === year;
    })[0];
  };

  getCurrentPeriod = () => ({
    month: moment().month(),
    year: moment().year(),
  });

  getCompanyCreationPeriod = () => {
    // Pre populated until we get the actual creation date of the company
    const companyCreationDate = moment().subtract('11', 'months');

    return {
      month: companyCreationDate.month(),
      year: companyCreationDate.year(),
    };
  };

  getCallEvolutionChartData = () => {
    return this.props.stats.map(stat => ({
      period: this.getMomentDate(stat.month).format("MMM 'YY"),
      oneOnOnes: stat.nbCallDoneRequests,
      amas: stat.nbCallDoneAttendances,
    }));
  };

  onContactUsClick = () => {
    const { snackbar } = this.props;
    snackbar.info(
      <>
        This feature is in development. Please contact{' '}
        <span className="bold">sales@platohq.com</span> to add more seats.
      </>
    );
  };

  render() {
    const { seats, commonChallenges, qualitativeFeedback } = this.props;

    const currentPeriod = this.getCurrentPeriod();

    return (
      <StyledWrapper>
        <Hero height="275" />
        <Layout width="1080" className="pb5">
          <OverviewFor period="All Time" />
          <CardGrid columns={4} className="mb3">
            <CallsHappenedCard
              callsHappened={this.getNbCallsHappened(
                this.getCompanyCreationPeriod(),
                currentPeriod
              )}
              callsHappenedBottom={this.getNbCallsHappened(currentPeriod, currentPeriod)}
              bottomLabel="This month:"
            />
            <AverageGradeCard
              averageGrade={this.getAverageGrade(this.getCompanyCreationPeriod(), currentPeriod)}
              averageGradeBottom={this.getAverageGrade(currentPeriod, currentPeriod)}
              bottomLabel="This month:"
            />
            <SeatsTakenCard
              seatsTaken={seats.nbTakenSeats}
              totalSeats={seats.nbPaidSeats}
              onContactUsClick={this.onContactUsClick}
            />
            <OnboardedCard numOnboarded={seats.nbOnboardedSeats} seatsTaken={seats.nbTakenSeats} />
          </CardGrid>

          <CallEvolutionChart className="mb3" data={this.getCallEvolutionChartData()} />

          <HorizontalSplit
            leftSide={<CommonChallengesCard commonChallenges={commonChallenges} />}
            rightSide={<QualitativeFeedbackCard qualitativeFeedback={qualitativeFeedback} />}
          />
        </Layout>
      </StyledWrapper>
    );
  }
}

Overview.propTypes = {
  stats: PropTypes.array.isRequired,
  seats: PropTypes.object.isRequired,
  commonChallenges: PropTypes.array.isRequired,
  qualitativeFeedback: PropTypes.array.isRequired,
};

function mapStateToProps(state) {
  return {
    stats: getDashboardStats(state),
    seats: getDashboardSeats(state),
    commonChallenges: getDashboardCommonChallenges(state),
    qualitativeFeedback: getDashboardQualitativeFeedback(state),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    onFetchStats: ({ organizationId, startDate, endDate, shouldLoadFakeData }) =>
      dispatch(fetchDashboardStats({ organizationId, startDate, endDate, shouldLoadFakeData })),
    onFetchSeats: ({ organizationId, shouldLoadFakeData }) =>
      dispatch(fetchDashboardSeats({ organizationId, shouldLoadFakeData })),
    onFetchCommonChallenges: ({ organizationId, shouldLoadFakeData }) =>
      dispatch(fetchDashboardCommonChallenges({ organizationId, shouldLoadFakeData })),
    onFetchQualitativeFeedback: ({ organizationId, shouldLoadFakeData }) =>
      dispatch(fetchDashboardQualitativeFeedback({ organizationId, shouldLoadFakeData })),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withUser(withSnackbar(Overview)));
