import styled from 'styled-components';

const OverviewLabel = styled.h3`
  color: ${props => props.theme.palette.cadetBlue};
`;

export default OverviewLabel;
