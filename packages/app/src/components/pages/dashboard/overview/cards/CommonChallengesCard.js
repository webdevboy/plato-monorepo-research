import CommonChallengeListItem from './CommonChallengeListItem';
import ListCard from './ListCard';
import PropTypes from 'prop-types';
import React from 'react';

const CommonChallengesCard = ({ commonChallenges }) => (
  <ListCard
    title="Common Challenges"
    subtitle={commonChallenges.length > 0 ? `Top ${commonChallenges.length}` : ''}
  >
    {commonChallenges.length > 0 ? (
      commonChallenges.map(challenge => (
        <CommonChallengeListItem
          className="mb2"
          text={challenge.challengeTitle}
          count={challenge.nbCallDoneRequests + challenge.nbCallDoneAttendances}
          key={challenge.challengeTitle}
        />
      ))
    ) : (
      <p>No common challenges found.</p>
    )}
  </ListCard>
);

CommonChallengesCard.propTypes = {
  commonChallenges: PropTypes.array.isRequired,
};

export default CommonChallengesCard;
