import KpiCard from './KpiCard';
import PropTypes from 'prop-types';
import React from 'react';
import StatValue from './StatValue';

const AverageGradeStatValue = ({ averageGrade }) => (
  <>
    <StatValue>{averageGrade}</StatValue>
    <StatValue variant="small">/ 10</StatValue>
  </>
);

const Bottom = ({ averageGrade, label }) => (
  <>
    <p>{label}</p>
    <p className="bold">{averageGrade} average grade</p>
  </>
);

const AverageGradeCard = ({ averageGrade, averageGradeBottom, bottomLabel }) => (
  <KpiCard
    icon={['far', 'star']}
    statValue={<AverageGradeStatValue averageGrade={averageGrade} />}
    label="Average Grade"
    bottomSection={<Bottom averageGrade={averageGradeBottom} label={bottomLabel} />}
  />
);

AverageGradeCard.propTypes = {
  averageGrade: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  averageGradeBottom: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  bottomLabel: PropTypes.string.isRequired,
};

AverageGradeCard.defaultProps = {
  averageGrade: '-',
  averageGradeBottom: '-',
};

export default AverageGradeCard;
