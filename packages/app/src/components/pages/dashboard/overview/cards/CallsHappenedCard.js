import KpiCard from './KpiCard';
import PropTypes from 'prop-types';
import React from 'react';
import StatValue from './StatValue';

const Bottom = ({ callsHappened, label }) => (
  <>
    <p>{label}</p>
    <p className="bold">{callsHappened} calls happened</p>
  </>
);

const CallsHappenedCard = ({ callsHappened, callsHappenedBottom, bottomLabel }) => (
  <KpiCard
    icon="headset"
    statValue={<StatValue>{callsHappened}</StatValue>}
    label="Calls Happened"
    bottomSection={<Bottom callsHappened={callsHappenedBottom} label={bottomLabel} />}
  />
);

CallsHappenedCard.propTypes = {
  callsHappened: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  callsHappenedBottom: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  bottomLabel: PropTypes.string.isRequired,
};

CallsHappenedCard.defaultProps = {
  callsHappened: '-',
  callsHappenedBottom: '-',
};

export default CallsHappenedCard;
