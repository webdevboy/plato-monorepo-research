import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import KpiCard from './KpiCard';
import PropTypes from 'prop-types';
import React from 'react';
import StatValue from './StatValue';
import styled from 'styled-components';

const SeatsTakenStatValue = ({ seatsTaken, totalSeats }) => (
  <>
    <StatValue>{seatsTaken}</StatValue>
    <StatValue variant="small">/ {totalSeats}</StatValue>
  </>
);

const StyledBottom = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  cursor: pointer;

  & .icon {
    color: ${props => props.theme.palette.osloGrey};
    font-size: var(--x3);
  }
`;

const Bottom = ({ onContactUsClick }) => (
  <StyledBottom onClick={onContactUsClick}>
    <div className="left">
      <p>If you need more seats</p>
      <p className="bold">contact us</p>
    </div>
    <FontAwesomeIcon icon="arrow-right" className="icon" />
  </StyledBottom>
);

const SeatsTakenCard = ({ seatsTaken, totalSeats, onContactUsClick }) => (
  <KpiCard
    icon="chair"
    statValue={<SeatsTakenStatValue seatsTaken={seatsTaken} totalSeats={totalSeats} />}
    label="Seats Taken"
    bottomSection={<Bottom onContactUsClick={onContactUsClick} />}
  />
);

SeatsTakenCard.propTypes = {
  seatsTaken: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  totalSeats: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  onContactUsClick: PropTypes.func.isRequired,
};

SeatsTakenCard.defaultProps = {
  seatsTaken: '-',
  totalSeats: '-',
};

export default SeatsTakenCard;
