import ListCard from './ListCard';
import PropTypes from 'prop-types';
import React from 'react';
import styled from 'styled-components';

const QualitativeFeedbackListItem = styled.p`
  font-style: italic;
`;

const QualitativeFeedbackCard = ({ qualitativeFeedback }) => (
  <ListCard
    title="Qualitative Feedback"
    subtitle={qualitativeFeedback.length > 0 ? 'Most Recent' : ''}
  >
    {qualitativeFeedback.length > 0 ? (
      qualitativeFeedback.map(feedback => (
        <QualitativeFeedbackListItem className="mb3" key={feedback.callScheduledAt}>
          {feedback.qualitativeFeedback}
        </QualitativeFeedbackListItem>
      ))
    ) : (
      <p>No qualitative feedback found.</p>
    )}
  </ListCard>
);

QualitativeFeedbackCard.propTypes = {
  qualitativeFeedback: PropTypes.array.isRequired,
};

export default QualitativeFeedbackCard;
