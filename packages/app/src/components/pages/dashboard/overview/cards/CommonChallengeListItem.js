import PropTypes from 'prop-types';
import React from 'react';
import styled from 'styled-components';

const StyledWrapper = styled.p`
  background: ${props => props.theme.palette.aquaHaze};
  border-radius: var(--x3);
  padding: var(--x2);
  width: fit-content;

  .text {
    margin-right: var(--x3);
  }

  .count {
    color: ${props => props.theme.palette.cerulean};
  }
`;

const CommonChallengeListItem = ({ text, count, className }) => (
  <StyledWrapper className={className}>
    <span className="text">{text}</span>
    <span className="count bold">{count}</span>
  </StyledWrapper>
);

CommonChallengeListItem.propTypes = {
  text: PropTypes.string.isRequired,
  count: PropTypes.number.isRequired,
  className: PropTypes.string,
};

CommonChallengeListItem.defaultProps = {
  className: '',
};

export default CommonChallengeListItem;
