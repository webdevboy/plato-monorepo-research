import PropTypes from 'prop-types';
import styled from 'styled-components';

const StatValue = styled.p`
  font-weight: 300;
  font-size: 6rem;
  color: ${props => props.theme.palette.java};
  line-height: 1;

  ${({ variant }) => {
    if (variant === 'small') {
      return `
        font-weight: 100;
        font-size: var(--h2);
        margin-bottom: 0.8rem;
        margin-left: 0.5rem;
      `;
    }

    return '';
  }}
`;

StatValue.propTypes = {
  variant: PropTypes.oneOf(['small']),
};

export default StatValue;
