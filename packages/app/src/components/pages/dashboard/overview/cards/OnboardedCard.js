import KpiCard from './KpiCard';
import PropTypes from 'prop-types';
import React from 'react';
import StatValue from './StatValue';

const OnboardedStatValue = ({ numOnboarded, seatsTaken }) => (
  <>
    <StatValue>{numOnboarded}</StatValue>
    <StatValue variant="small">/ {seatsTaken}</StatValue>
  </>
);

const Onboarded = ({ numOnboarded, seatsTaken }) => (
  <KpiCard
    icon={['far', 'clock']}
    statValue={<OnboardedStatValue numOnboarded={numOnboarded} seatsTaken={seatsTaken} />}
    label="Onboarded Mentees"
    bottomSection={<p>Number of onboarded mentees compared to the number of seats taken.</p>}
  />
);

Onboarded.propTypes = {
  numOnboarded: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  seatsTaken: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
};

Onboarded.defaultProps = {
  numOnboarded: '-',
  seatsTaken: '-',
};

export default Onboarded;
