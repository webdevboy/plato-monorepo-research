import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import PropTypes from 'prop-types';
import React from 'react';
import classNames from 'classnames';
import styled from 'styled-components';

const StyledIcon = styled(FontAwesomeIcon)`
  color: ${props => props.theme.palette.cadetBlue};
`;

const KpiCardIcon = ({ className, ...props }) => (
  <StyledIcon className={classNames('icon', 'mb3', className)} {...props} />
);

KpiCardIcon.propTypes = {
  className: PropTypes.string,
};

KpiCardIcon.defaultProps = {
  className: '',
};

export default KpiCardIcon;
