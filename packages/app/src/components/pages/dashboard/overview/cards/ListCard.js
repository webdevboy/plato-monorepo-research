import Card from 'components/shared/Card';
import OverviewLabel from '../OverviewLabel';
import PropTypes from 'prop-types';
import React from 'react';
import styled from 'styled-components';

const StyledCard = styled(Card)`
  .title-container,
  .list-container {
    padding: var(--x3);
  }

  .title-container {
    display: flex;
    justify-content: space-between;
    border-bottom: 1px solid ${props => props.theme.palette.geyser};

    .subtitle {
      font-weight: initial;
    }
  }
`;

const ListCard = ({ title, subtitle, children }) => (
  <StyledCard>
    <div className="title-container pb3">
      <OverviewLabel>{title}</OverviewLabel>
      <OverviewLabel className="subtitle">{subtitle}</OverviewLabel>
    </div>
    <div className="list-container">{children}</div>
  </StyledCard>
);

ListCard.propTypes = {
  title: PropTypes.string.isRequired,
  subtitle: PropTypes.string,
  children: PropTypes.node,
};

ListCard.defaultProps = {
  subtitle: '',
  children: null,
};

export default ListCard;
