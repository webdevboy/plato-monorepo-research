import Card from 'components/shared/Card';
import KpiCardIcon from './KpiCardIcon';
import OverviewLabel from '../OverviewLabel';
import PropTypes from 'prop-types';
import React from 'react';
import styled from 'styled-components';

const StyledCard = styled(Card)`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: var(--x2);
  padding-top: var(--x3);
  font-size: var(--h1);
  width: 100%;

  & > .stat-value {
    display: flex;
    align-items: flex-end;
  }

  & > .bottom {
    background: ${props => props.theme.palette.aquaHaze};
    width: 100%;
    padding: var(--x2);
    border-radius: 0.2rem;
    margin-top: auto;

    & p {
      color: ${props => props.theme.palette.osloGrey};
      font-size: var(--h4);
    }
  }
`;

const KpiCard = ({ icon, statValue, label, bottomSection }) => (
  <StyledCard>
    <KpiCardIcon icon={icon} />
    <span className="stat-value">{statValue}</span>
    <OverviewLabel className="mb3">{label}</OverviewLabel>
    <div className="bottom">{bottomSection}</div>
  </StyledCard>
);

KpiCard.propTypes = {
  icon: PropTypes.oneOfType([PropTypes.string, PropTypes.array]).isRequired,
  statValue: PropTypes.object.isRequired,
  label: PropTypes.string.isRequired,
  bottomSection: PropTypes.object.isRequired,
};

export default KpiCard;
