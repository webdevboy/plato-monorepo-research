import styled from 'styled-components';

export default styled.h1`
  font-size: var(--h2);
  font-weight: 100;
  color: ${props => props.theme.palette.white};

  @media (min-width: 867px) {
    font-size: var(--h1);
  }
`;
