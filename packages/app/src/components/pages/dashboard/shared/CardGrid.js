import React from 'react';
import styled from 'styled-components';

const StyledCardGrid = styled.div`
  display: inline-grid;
  column-gap: var(--x2);
  row-gap: var(--x2);
  grid-template-columns: 1fr;
  justify-items: center;
  margin: inherit var(--x2);
  max-width: 100%;
  width: 100%;

  @media (min-width: 768px) {
    grid-template-columns: ${props => [...Array(props.columns)].map(() => '1fr').join(' ')};
    margin: 0;
  }
`;

const CardGrid = ({ columns, className, children }) => (
  <StyledCardGrid columns={columns} className={className}>
    {children}
  </StyledCardGrid>
);

export default CardGrid;
