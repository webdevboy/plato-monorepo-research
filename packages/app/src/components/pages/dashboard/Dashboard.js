import React, { Component } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

import Mentees from './mentees/Mentees';
import Overview from './overview/Overview';
import PropTypes from 'prop-types';
import Settings from './settings/Settings';

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const {
      match: { path },
    } = this.props;

    return (
      <Switch>
        <Route exact path={`${path}/`} component={Overview} />
        <Route exact path={`${path}/settings`} component={Settings} />
        <Route exact path={`${path}/mentees`} component={Mentees} />
        <Redirect to={`${path}/`} />
      </Switch>
    );
  }
}

Dashboard.propTypes = {
  match: PropTypes.object.isRequired,
};

export default Dashboard;
