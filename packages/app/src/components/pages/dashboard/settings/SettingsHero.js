import Button from 'components/shared/Button';
import Hero from 'components/shared/Hero';
import React from 'react';
import styled from 'styled-components';

const StyledHero = styled(Hero)`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: var(--x5);

  .settings {
    font-size: var(--h1);
    font-weight: 100;
    color: ${props => props.theme.palette.white};
  }
`;

const SettingsHero = ({ onSaveClick }) => (
  <StyledHero height="175">
    <h1 className="settings">Settings</h1>
    <Button variant="primary" onClick={onSaveClick}>
      Save Changes
    </Button>
  </StyledHero>
);

export default SettingsHero;
