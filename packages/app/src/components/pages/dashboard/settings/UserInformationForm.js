import React, { Component } from 'react';

import { Formik } from 'formik';

class UserInformationForm extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  validate = () => {};

  render() {
    const { initialValues } = this.props;

    return (
      <Formik initialValues={initialValues} validate={this.validate}>
        <h1>Form</h1>
      </Formik>
    );
  }
}

export default UserInformationForm;
