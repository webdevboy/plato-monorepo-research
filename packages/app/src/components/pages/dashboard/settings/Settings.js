import React, { Component } from 'react';

import Button from 'components/shared/Button';
import HeaderText from 'components/pages/dashboard/shared/HeaderText';
import Hero from 'components/shared/Hero';
import Layout from 'components/shared/Layout';
import UserInformationForm from './UserInformationForm';
import styled from 'styled-components';

const StyledWrapper = styled.div`
  ${Layout} {
    position: relative;
  }

  .header-container {
    display: flex;
    align-items: center;
    justify-content: space-between;
  }
`;

class Settings extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstName: 'Feng',
      lastName: 'Shawn',
      timeZone: 'PST',
      email: 'fengshawn@yopmail.com',
    };
  }

  render() {
    const { firstName, lastName, timeZone, email } = this.state;

    return (
      <StyledWrapper>
        <Hero height="175" />
        <Layout width="700" className="pt4 pb5">
          <div className="mb5  header-container">
            <HeaderText>Settings</HeaderText>
            <Button variant="primary" onClick={() => {}}>
              Save Changes
            </Button>
          </div>
          <p className="title mb4">User Information</p>
          <UserInformationForm initialValues={{ firstName, lastName, timeZone, email }} />
        </Layout>
      </StyledWrapper>
    );
  }
}

export default Settings;
