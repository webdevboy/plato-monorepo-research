import { createGlobalStyle } from 'styled-components';

export default createGlobalStyle`
  :root {
    --h0: 4.5rem;
    --h1: 3rem;
    --h2: 2.25rem;
    --h3: 1.5rem;
    --h4: 1.125rem;
    --h5: 0.75rem;

    --lh: calc(4 / 3);
    --mx: 32em;

    --m1: calc(2 / 3 * 1em);
    --m2: calc(4 / 3 * 1em);
    --m3: calc(8 / 3 * 1em);
    --m4: calc(16 / 3 * 1em);
    --x1: 0.5rem;
    --x2: 1rem;
    --x3: 2rem;
    --x4: 4rem;
    --x5: 8rem;
    --x6: 16rem;
  }
  
  .h0 {
    font-size: var(--h0);
  }

  .h1 {
    font-size: var(--h1);
  }

  .h2 {
    font-size: var(--h2);
  }

  .h3 {
    font-size: var(--h3);
  }

  .h4 {
    font-size: var(--h4);
  }

  .h5 {
    font-size: var(--h5);
  }

  .h6 {
    font-size: var(--h6);
  }

  .bold {
    font-weight: bold;
  }

  .mt0 {
    margin-top: 0;
  }
  .mb0 {
    margin-bottom: 0;
  }
  .mt1 {
    margin-top: var(--x1);
  }
  .mb1 {
    margin-bottom: var(--x1);
  }
  .mt2 {
    margin-top: var(--x2);
  }
  .mb2 {
    margin-bottom: var(--x2);
  }
  .mt3 {
    margin-top: var(--x3);
  }
  .mb3 {
    margin-bottom: var(--x3);
  }
  .mt4 {
    margin-top: var(--x4);
  }
  .mb4 {
    margin-bottom: var(--x4);
  }
  .mt5 {
    margin-top: var(--x5);
  }
  .mb5 {
    margin-bottom: var(--x5);
  }
  .mt6 {
    margin-top: var(--x6);
  }
  .mb6 {
    margin-bottom: var(--x6);
  }

  .pt0 {
    padding-top: 0;
  }
  .pb0 {
    padding-bottom: 0;
  }
  .pt1 {
    padding-top: var(--x1);
  }
  .pb1 {
    padding-bottom: var(--x1);
  }
  .pt2 {
    padding-top: var(--x2);
  }
  .pb2 {
    padding-bottom: var(--x2);
  }
  .pt3 {
    padding-top: var(--x3);
  }
  .pb3 {
    padding-bottom: var(--x3);
  }
  .pt4 {
    padding-top: var(--x4);
  }
  .pb4 {
    padding-bottom: var(--x4);
  }
  .pt5 {
    padding-top: var(--x5);
  }
  .pb5 {
    padding-bottom: var(--x5);
  }
  .pt6 {
    padding-top: var(--x6);
  }
  .pb6 {
    padding-bottom: var(--x6);
  }

  .full-width {
    width: 100%;
  }
  
  * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }

  html {
    font-size: 10px;
    font-family: ${({ theme }) => theme.fontFamily};
    font-weight: 400;
    color: #222A3F;
  }

  li {
    list-style: none;
    font-size: 1.6rem;
    line-height: 1.5;
  }

  a {
    color: inherit;
    text-decoration: none;
  }

  h1 {
    font-size: 2.4rem;
    line-height: 1.33;
  }

  h2 {
    font-size: 1.8rem;
    font-weight: 700;
  }

  h3 {
    font-size: 1.6rem;
    color:#899099;
    text-transform: uppercase;
    font-weight: 700;
  }

  h4 {
    font-size: 1.2rem;
    color: #ABB4BF;
    font-weight: 700;
    text-transform: uppercase;
  }

  p,
  label {
    font-size: 1.6rem;
    line-height: 1.5;
  }

  sub {
    font-size: 1.4rem;
    line-height: 1.43;
  }

  textarea {
    font-size: 1.4rem;
    color: inherit;
    resize: none;
  }

  button, input, textarea {
    font-family: inherit;
    outline: none;
  }

  hr {
    display: block;
    height: 1px;
    border: 0;
    border-top: 1px solid ${({ theme }) => theme.palette.geyser};
    padding: 0;
    margin: 3rem 0;
  }

  .spacer {
    flex-grow: 1;
  }
`;
