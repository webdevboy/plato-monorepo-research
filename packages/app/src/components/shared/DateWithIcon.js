import IconLabel from './IconLabel';
import React from 'react';
import { formatTimestampToDate } from 'utils/time';
import styled from 'styled-components';
import { withUser } from 'components/context/UserContext';

const DateWithIcon = props => (
  <IconLabel icon="calendar" className={props.className}>
    {formatTimestampToDate(props.date, props.userContext.timeZone())}
  </IconLabel>
);

export default styled(withUser(DateWithIcon))``;
