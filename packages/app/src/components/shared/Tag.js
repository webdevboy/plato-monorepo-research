import React, { Component } from 'react';

import styled from 'styled-components';

const StyledTag = styled.div`
  border-radius: 5rem;
  font-weight: bold;
  text-align: center;
  text-transform: uppercase;
  padding: 0.2rem 0.8rem;
  width: fit-content;
  letter-spacing: 1px;
  display: inline-flex;

  &.small {
    font-size: 1rem;
  }

  &.medium {
    font-size: 1.2rem;
  }

  &.selectable {
    cursor: pointer;
  }

  &.primary {
    background: ${({ theme }) => theme.palette.cerulean};
    color: ${({ theme }) => theme.palette.white};
  }

  &.dark {
    background: ${({ theme }) => theme.palette.aquaHaze};
    color: ${({ theme }) => theme.palette.cadetBlue};
  }

  &.light {
    border: 1px solid ${({ theme }) => theme.palette.cadetBlue};
    background: ${({ theme }) => theme.palette.white};
    color: ${({ theme }) => theme.palette.cadetBlue};

    &.selected {
      border: 1px solid ${({ theme }) => theme.palette.withOpacity(theme.palette.cerulean, 10)};
      background: ${({ theme }) => theme.palette.withOpacity(theme.palette.cerulean, 10)};
      color: ${({ theme }) => theme.palette.cerulean};
    }
  }
`;

class Tag extends Component {
  handleClick = () => {
    const { onClick, value, isSelectable } = this.props;
    if (isSelectable) {
      onClick(value);
    }
  };

  render() {
    const {
      className,
      text,
      isSelectable,
      isSelected,
      variant = 'dark',
      size = 'medium',
    } = this.props;
    return (
      <StyledTag
        className={`${className} ${variant} ${size} ${isSelectable ? 'selectable' : ''} ${
          isSelected ? 'selected' : ''
        }`}
        onClick={this.handleClick}
      >
        {text}
      </StyledTag>
    );
  }
}

export default styled(Tag)``;
