import React from 'react';
import styled from 'styled-components';

const Typography = styled.p`
  font-family: 'Source Sans Pro', 'Helvetica Neue', 'Segoe UI', 'Helvetica', 'Arial', 'sans-serif';
  color: #222a3f;
  font-size: 1.4rem;
  margin: ${({ margin }) => margin};
  text-transform: ${({ uppercase }) => (uppercase ? 'uppercase' : 'none')};

  &.label {
    font-weight: bold;
    padding-top: 1rem;
    color: #abb4bf;
  }

  &.heading1 {
    font-size: 3.4rem;
    font-weight: bold;
  }

  &.heading2 {
    font-size: 2.4rem;
    font-weight: bold;
    margin-bottom: 2rem;
    line-height: 120%;
  }

  &.regular {
    font-size: 1.6rem;
    font-weight: normal;
  }

  &.small {
    font-size: 1.4rem;
    font-weight: normal;
  }

  &.inverse {
    color: white;
  }

  &.bubble {
    margin: 0.5rem 0.5rem 0.5rem 0;
    border-radius: 1rem;
    background: #f2f6f8;
    color: #a6aebc;
    padding: 0.5rem 0.8rem;
    font-weight: bold;
  }
`;

const Text = ({ uppercase = false, margin = 'none', type = 'regular', children }) => (
  <Typography className={type} margin={margin} uppercase={uppercase}>
    {children}
  </Typography>
);

export default styled(Text)``;
