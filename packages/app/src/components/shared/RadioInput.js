import React, { Component } from 'react';

import PropTypes from 'prop-types';
import classNames from 'classnames';
import styled from 'styled-components';

const StyledRadioInput = styled.label`
  width: 100%;
  display: flex;
  align-items: center;
  cursor: pointer;

  input {
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
  }

  .input {
    margin-right: 1rem;
    width: 2rem;
    height: 2rem;
    border-radius: 1rem;
    flex-shrink: 0;
    border: 2px solid ${({ theme }) => theme.palette.geyser};
  }

  &.checked .input {
    position: relative;
    border-color: ${({ theme }) => theme.palette.java};

    &:before {
      content: ' ';
      position: absolute;
      border-radius: 8px;
      top: 4px;
      right: 4px;
      bottom: 4px;
      left: 4px;
      background-color: ${({ theme }) => theme.palette.java};
    }
  }
`;

class RadioInput extends Component {
  render() {
    const { label, isChecked, className, name, onChange, value } = this.props;
    return (
      <StyledRadioInput className={classNames(className, { checked: isChecked })}>
        <input checked={isChecked} name={name} onChange={onChange} type="radio" value={value} />
        <div className="input" />
        {label}
      </StyledRadioInput>
    );
  }
}

RadioInput.propTypes = {
  className: PropTypes.string,
  isChecked: PropTypes.bool.isRequired,
  label: PropTypes.node,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string,
};

RadioInput.defaultProps = {
  className: null,
  label: null,
  value: null,
};

export default styled(RadioInput)``;
