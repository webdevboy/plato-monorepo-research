import React from 'react';
import styled from 'styled-components';

const StyledWrapper = styled.h4`
  padding: 10px 25px;
  border: 1px solid ${props => props.theme.palette.geyser};
  letter-spacing: 2px;
  border-radius: 4px;
`;

const StatusChip = props => {
  const { title, ...other } = props;
  return <StyledWrapper {...other}>{props.title}</StyledWrapper>;
};

export default styled(StatusChip)``;
