import React, { Component } from 'react';

import { DebounceInput } from 'react-debounce-input';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import styled from 'styled-components';

const StyledSearchInput = styled.div`
  display: flex;
  align-items: center;
  position: relative;
  height: 5rem;
  background: ${props => props.theme.palette.polar};
  border: 1px solid ${props => props.theme.palette.geyser};
`;

const StyledFontAwesomeIcon = styled(FontAwesomeIcon)`
  margin-left: 1rem;
  font-size: 2rem;
  color: ${props => props.theme.palette.cadetBlue};
`;

const StyledDebounceInput = styled(DebounceInput)`
  font-size: 1.4rem;
  margin-left: 1rem;
  background: transparent;
  border: none;
  outline: none;
  width: 100%;
  height: 100%;

  &::placeholder {
    color: ${props => props.theme.palette.cadetBlue};
  }
`;

class SearchInput extends Component {
  render() {
    const { className, name, onChange, placeholder } = this.props;

    return (
      <StyledSearchInput className={className}>
        <StyledFontAwesomeIcon icon="search" />
        <StyledDebounceInput
          type="text"
          name={name}
          placeholder={placeholder}
          debounceTimeout={500}
          onChange={onChange}
        />
      </StyledSearchInput>
    );
  }
}

export default styled(SearchInput)``;
