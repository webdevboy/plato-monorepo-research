import styled from 'styled-components';
import LoadingSpinner from './LoadingSpinner';

const PageLoadingSpinner = styled(LoadingSpinner)`
  position: absolute;
  left: 50%;
  top: 35rem;
  transform: translate(-50%, -50%);
  z-index: 2;
`;

export default PageLoadingSpinner;
