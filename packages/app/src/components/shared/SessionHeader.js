import React, { Component } from 'react';

import Button from './Button';
import Moment from 'react-moment';
import TagList from './TagList';
import moment from 'moment';
import styled from 'styled-components';

const StyledHeader = styled.header`
  position: relative;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  align-self: center;
  color: ${props => props.theme.palette.white};
  text-align: center;

  & h1 {
    font-size: 34px;
    font-weight: bold;
    letter-spacing: 0.6px;
    margin-bottom: 20px;
  }

  & ${Button} {
    margin-top: 20px;
  }

  & p {
    font-weight: bold;
    padding: 10px 20px;
    text-transform: uppercase;
    border-radius: 2px;
    border: 1px solid ${props => props.theme.palette.white};
    margin-top: 20px;

    & time {
      color: ${props => props.theme.palette.cerulean};
    }
  }
`;

class SessionHeader extends Component {
  handleClick = () => {
    const { zoomLink } = this.props;
    window.open(zoomLink, '_blank');
  };

  renderCountdown() {
    const { startAt, status } = this.props;

    if (status === 'Call done' || status === 'Closed') {
      // The session is complete
      return null;
    }

    const timeDifference = moment(startAt).diff(new Date());
    const hasStarted = timeDifference < 0;
    const isStartingSoon = moment.duration(timeDifference).asHours() <= 1;
    if (hasStarted || isStartingSoon) {
      // The session is not complete and is starting soon or is already started
      return (
        <Button variant="primary" onClick={this.handleClick}>
          Go to Zoom room
        </Button>
      );
    }

    // The session is not complete and not about to start
    return (
      <p>
        AMA starts in: <Moment date={startAt} fromNow ago />
      </p>
    );
  }

  render() {
    const { name, tags, startAt, status, ...other } = this.props;

    return (
      <StyledHeader {...other}>
        <h4>Topic</h4>
        <h1>{name}</h1>
        <TagList tags={tags} />
        {this.renderCountdown()}
      </StyledHeader>
    );
  }
}

export default styled(SessionHeader)``;
