import React, { Component } from 'react';

import Card from 'components/shared/Card';
import styled from 'styled-components';

const StyledCard = styled(Card)`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  height: 380px;
  border: 1px solid ${props => props.theme.palette.geyser};
  box-shadow: none;
  text-align: center;
  padding: 2rem 2rem;

  h1,
  .image {
    margin-bottom: 1rem;
  }

  p {
    margin-bottom: 2rem;
  }

  .image {
    font-size: 3.4rem;
  }
`;

class EmptyStateCard extends Component {
  render() {
    const { children, className, image = '🤔', label = 'Hmm', title, description } = this.props;
    return (
      <StyledCard className={className}>
        <span className="image" role="img" aria-label={label}>
          {image}
        </span>
        <h1>{title}</h1>
        {description && <p>{description}</p>}
        {children}
      </StyledCard>
    );
  }
}

export default styled(EmptyStateCard)``;
