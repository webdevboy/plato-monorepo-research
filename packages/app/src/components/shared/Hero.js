import styled from 'styled-components';

const Hero = styled.div`
  position: absolute;
  width: 100%;
  background: ${props => props.theme.palette.mirage};
  height: ${props => (props.height ? `${props.height}px` : '42rem')};
`;

export default styled(Hero)``;
