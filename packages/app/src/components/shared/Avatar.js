import React from 'react';
import styled from 'styled-components';

const dimensions = {
  xs: '3.5rem',
  sm: '4rem',
  md: '5.6rem',
  lg: '7rem',
  xl: '9rem',
};

const StyledAvatar = styled.img`
  border-radius: 10rem;
  border: 0.1rem solid ${({ theme }) => theme.palette.geyser};
  background: ${({ theme }) => theme.palette.white};
  min-width: ${({ size }) => dimensions[size]};
  min-height: ${({ size }) => dimensions[size]};
  max-width: ${({ size }) => dimensions[size]};
  max-height: ${({ size }) => dimensions[size]};
`;

const Avatar = ({ src, size, ...props }) => (
  <StyledAvatar
    src={src}
    size={size}
    onError={event => {
      event.target.src = '//dhtiece9044ep.cloudfront.net/static/img/transparent.png'; // eslint-disable-line no-param-reassign
    }}
    {...props}
  />
);

export default styled(Avatar)``;
