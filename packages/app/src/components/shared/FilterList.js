import React, { Component } from 'react';

import IconLabel from 'components/shared/IconLabel';
import PlatoLinearProgress from './PlatoLinearProgress';
import styled from 'styled-components';

const Title = styled.h4`
  margin-bottom: 1rem;
`;

const Options = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  max-height: 38.6rem;
  border: 1px solid ${props => props.theme.palette.geyser};

  & ${PlatoLinearProgress} {
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    z-index: 1;
  }
`;

const ViewMoreButton = styled.div`
  padding: 1rem 2rem;
  display: flex;
  border-top: 1px solid ${props => props.theme.palette.geyser};
  justify-content: center;
  cursor: pointer;

  h4 {
    margin-right: 1rem;
  }
`;

const ScrollContainer = styled.div`
  padding: 1rem;
  height: 100%;
  overflow-y: auto;
`;

class FilterList extends Component {
  state = {
    isExpanded: false,
  };

  handleToggle = () => {
    const { isExpanded } = this.state;
    this.setState({ isExpanded: !isExpanded });
  };

  render() {
    const { children, title, loading, maxItems = 10 } = this.props;
    const { isExpanded } = this.state;

    const childrenArray = React.Children.toArray(children);

    return (
      <div>
        <Title>{title}</Title>
        <Options>
          {loading && <PlatoLinearProgress variant="indeterminate" />}
          <ScrollContainer>
            {isExpanded ? children : childrenArray.splice(0, maxItems)}
          </ScrollContainer>
          {childrenArray.length > maxItems && !isExpanded && (
            <ViewMoreButton onClick={this.handleToggle}>
              <h4>View More</h4>
              <IconLabel icon="angle-down" />
            </ViewMoreButton>
          )}
        </Options>
      </div>
    );
  }
}

export default FilterList;
