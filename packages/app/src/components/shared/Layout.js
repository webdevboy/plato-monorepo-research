import React from 'react';
import styled from 'styled-components';

const Body = styled.div`
  position: relative;
  display: flex;
  flex-direction: ${({ direction }) => direction};
  width: 100%;
`;

const Container = styled.div.attrs({
  width: props => props.width || 960,
})`
  display: flex;
  justify-content: center;
  width: 100%;

  @media (min-width: ${props => props.width}px) {
    & ${Body} {
      width: ${props => props.width}px;
    }
  }
`;

const Layout = props => {
  const { children, width, direction = 'column', ...other } = props;

  return (
    <Container width={width} {...other}>
      <Body direction={direction}>{children}</Body>
    </Container>
  );
};

export default styled(Layout)``;

// import React from 'react';
// import styled from 'styled-components';

// const Body = styled.div`
//   display: flex;
//   flex-direction: column;
//   justify-content: center;
//   position: relative;
//   width: 100%;
// `;

// const Container = styled.div`
//   display: flex;
//   justify-content: center;
//   width: 100%;

//   @media (min-width: 960px) {
//     & ${Body} {
//       width: 960px;
//     }
//   }
// `;

// const Layout = props => {
//   const { children, ...other } = props;

//   return (
//     <Container {...other}>
//       <Body>{children}</Body>
//     </Container>
//   );
// };

// export default styled(Layout)``;
