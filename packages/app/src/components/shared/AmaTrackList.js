import PersonInfo from './PersonInfo';
import React from 'react';
import styled from 'styled-components';

const AmaTrackListWrapper = styled.div`
  width: 100%;
  padding-top: 2rem;

  @media only screen and (min-width: 1025px) {
    border-right: 1px solid ${({ theme }) => theme.palette.cadetBlue};
  }
`;

const AmaCard = styled.div`
  position: relative;
  margin: 0.5rem;
  border-left: 2px solid ${({ theme }) => theme.palette.geyser};
`;

const Oval = styled.div`
  border: 2px solid ${({ theme }) => theme.palette.java};
  height: 2rem;
  width: 2rem;
  border-radius: 50%;
  position: absolute;
  left: -1.1rem;
  top: -0.5rem;
  background-color: ${({ theme }) => theme.palette.polar};
`;

const CardContent = styled.div`
  padding-left: 2rem;
  padding-bottom: 2rem;
`;

const TopicWrapper = styled.div`
  display: flex;
  flex-direction: row;
`;

const Label = styled.p`
  font-size: 1.8rem;
  line-height: 1.8rem;
  font-weight: bold;
  color: ${({ theme }) => theme.palette.cadetBlue};
  text-transform: uppercase;
`;

const Description = styled.p`
  font-size: 1.5rem;
  margin-top: -1px;
  margin-left: 2rem;
`;

const AmaTrackList = ({ amas }) => {
  return (
    <AmaTrackListWrapper>
      {amas
        .sort((amaA, amaB) => {
          return amaA.callScheduledAt - amaB.callScheduledAt; // TODO: sort on the backend
        })
        .map((ama, index) => (
          <AmaCard key={ama.id}>
            <Oval />
            <CardContent>
              <TopicWrapper>
                <Label>Week {index + 1}</Label>
                <Description>{process.env.REACT_APP_AMA_DURATION} min</Description>
              </TopicWrapper>
              <h1>{ama.storyName}</h1>
              <div>
                <PersonInfo
                  fullName={ama.mentorFullName}
                  title={ama.mentorTitle || ama.faces[0].details} // TODO: remove default once backend returns the correct data
                  thumbnailUrl={ama.mentorPicture.thumbnailUrl}
                  companyName={ama.mentorCompany || 'Company'} // TODO: remove default once backend returns the correct data
                />
              </div>
            </CardContent>
          </AmaCard>
        ))}
    </AmaTrackListWrapper>
  );
};

export default AmaTrackList;
