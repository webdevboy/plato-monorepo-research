import React, { Component } from 'react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const StyledMessage = styled.div`
  display: grid;
  grid-template-columns: auto 1fr;
  grid-column-gap: 1rem;
  padding: 1rem 1rem 2rem;
  border-radius: 2px;

  sub {
    font-size: 1.2rem;
  }

  &.error {
    color: ${({ theme }) => theme.palette.error};
    background-color: ${({ theme: { palette } }) => palette.withOpacity(palette.error, 4)};
  }

  &.warning {
    color: ${({ theme }) => theme.palette.osloGrey};
    background-color: ${({ theme }) => theme.palette.aquaHaze};
  }

  &.no-background {
    padding: 0;
    background: none;
  }
`;

const icons = {
  error: 'exclamation-triangle',
  warning: 'exclamation-circle',
};

class Message extends Component {
  render() {
    const { className, text, type, hasBackground } = this.props;
    return (
      <StyledMessage className={`${className} ${type} ${!hasBackground ? 'no-background' : ''}`}>
        <sub>
          <FontAwesomeIcon icon={icons[type]} />
        </sub>
        <sub>{text}</sub>
      </StyledMessage>
    );
  }
}

Message.propTypes = {
  hasBackground: PropTypes.bool,
  type: PropTypes.oneOf(['error', 'warning']).isRequired,
  text: PropTypes.string.isRequired,
  className: PropTypes.string,
};

Message.defaultProps = {
  className: null,
  hasBackground: false,
};

export default styled(Message)``;
