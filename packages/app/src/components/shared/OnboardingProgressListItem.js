import Button from 'components/shared/Button';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Link } from 'react-router-dom';
import React from 'react';
import classNames from 'classnames';
import styled from 'styled-components';

const StyledOnboardingProgressListItem = styled.li`
  display: flex;
  padding: 3rem 2rem;
  color: ${props => props.theme.palette.osloGrey};

  &.selected {
    border: 1px solid ${props => props.theme.palette.geyser};
    border-radius: 4px;
  }

  &.selected h2,
  &.completed h2 {
    color: ${props => props.theme.palette.black};
  }
`;

const StyledDescription = styled.div`
  padding-right: 3rem;

  p {
    color: ${props => props.theme.palette.osloGrey};
    margin-top: 2rem;
  }
`;

const StyledStatus = styled.p`
  align-items: flex-end;
  display: flex;
  flex-direction: column;
  flex: 1;
  justify-content: center;
  color: ${props => props.theme.palette.osloGrey};
  font-weight: bold;

  &.completed {
    color: ${props => props.theme.palette.java};
  }

  ${Button} {
    margin-top: 1rem;
  }
`;

const OnboardingProgressListItem = props => {
  const { title, description, isCompleted, path } = props;
  return (
    <StyledOnboardingProgressListItem
      className={classNames({ selected: Boolean(path), completed: isCompleted })}
    >
      <StyledDescription>
        <h2>{title}</h2>
        <p>{description}</p>
      </StyledDescription>
      <StyledStatus className={classNames({ completed: isCompleted })}>
        {isCompleted ? <FontAwesomeIcon icon="check" /> : '~ 2 MIN'}
        {path && (
          <Link to={path}>
            <Button variant="primary">Continue</Button>
          </Link>
        )}
      </StyledStatus>
    </StyledOnboardingProgressListItem>
  );
};

export default OnboardingProgressListItem;
