import React, { Component } from 'react';

import Message from './Message';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const StyledLabel = styled.label`
  display: block;

  .label {
    margin-bottom: 1rem;
  }

  input,
  textarea {
    border: 1px solid ${({ theme, error }) => (!error ? theme.palette.geyser : theme.palette.error)};
    transition: border ${({ theme }) => theme.transitions.default};
    width: 100%;
    font-size: 1.4rem;
    padding: 1.4rem 4rem 1.4rem 1.2rem;
    margin-bottom: 2rem;
    border-radius: 0.4rem;
    background-color: ${({ disabled, theme }) =>
      disabled ? theme.palette.aquaHaze : theme.palette.white};

    &:focus {
      border: 1px solid
        ${({ theme, error }) => (!error ? theme.palette.cerulean : theme.palette.error)};
    }

    &::placeholder {
      color: ${({ theme }) => theme.palette.cadetBlue};
    }

    &:disabled {
      background: ${({ theme }) => theme.palette.geyser};
    }
  }

  &.error {
    input,
    textarea {
      margin-bottom: 1rem;
    }
  }
`;

class TextInput extends Component {
  render() {
    const {
      className,
      error,
      isEnabled,
      label,
      name,
      onChange,
      placeholder,
      rows,
      type,
      value,
    } = this.props;
    return (
      <StyledLabel className={`${className || ''} ${error ? 'error' : ''}`}>
        {label && <h4 className="label">{label}</h4>}
        {type === 'multi' ? (
          <textarea
            disabled={!isEnabled}
            name={name}
            onChange={onChange}
            placeholder={placeholder}
            rows={rows}
            value={value}
          />
        ) : (
          <input
            disabled={!isEnabled}
            name={name}
            onChange={onChange}
            placeholder={placeholder}
            type={type}
            value={value}
          />
        )}
        {error && <Message type="error" text={error} />}
      </StyledLabel>
    );
  }
}

TextInput.propTypes = {
  className: PropTypes.string,
  error: PropTypes.string,
  isEnabled: PropTypes.bool,
  label: PropTypes.string,
  name: PropTypes.string,
  onChange: PropTypes.func,
  placeholder: PropTypes.string,
  rows: PropTypes.number,
  type: PropTypes.oneOf(['text', 'multi', 'number', 'password', 'email', 'url']).isRequired,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};

TextInput.defaultProps = {
  className: null,
  error: null,
  isEnabled: true,
  label: null,
  name: null,
  onChange: () => {},
  placeholder: null,
  rows: 3,
  value: undefined,
};

export default styled(TextInput)``;
