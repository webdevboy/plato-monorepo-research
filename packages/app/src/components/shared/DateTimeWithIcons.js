import React from 'react';
import styled from 'styled-components';
import DateWithIcon from './DateWithIcon';
import TimeWithIcon from './TimeWithIcon';

const StyledWrapper = styled.div`
  display: inline-flex;
  flex-wrap: wrap;
  align-items: center;

  & ${DateWithIcon}, & ${TimeWithIcon} {
    margin-right: 2rem;
  }
`;

const DateTimeWithIcons = ({ timestamp, className }) => (
  <StyledWrapper className={className}>
    <DateWithIcon date={timestamp} />
    <TimeWithIcon time={timestamp} />
  </StyledWrapper>
);

export default styled(DateTimeWithIcons)``;
