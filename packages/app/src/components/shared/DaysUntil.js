import React from 'react';
import moment from 'moment';
import styled from 'styled-components';

const StyledWrapper = styled.h1`
  margin-bottom: 1rem;
`;

const StyledSpan = styled.span`
  margin-bottom: 1rem;
`;

const DaysUntil = ({ timestamp, className, inline }) => {
  const diff = moment(timestamp)
    .startOf('day')
    .diff(moment().startOf('day'), 'days');

  let text;
  if (diff === 0) {
    text = 'Today';
  } else if (diff === 1) {
    text = 'Tomorrow';
  } else if (diff === -1) {
    text = 'Yesterday';
  } else if (diff > 1) {
    text = `In ${diff} days`;
  } else if (diff < -1) {
    text = `${-diff} days ago`;
  }

  return inline ? (
    <StyledSpan className={className}>{text}</StyledSpan>
  ) : (
    <StyledWrapper className={className}>{text}</StyledWrapper>
  );
};

export default styled(DaysUntil)``;
