import React from 'react';
import Select from 'react-select';
import styled from 'styled-components';

const Wrapper = styled.div`
  padding: ${props => props.padding};

  & .label {
    margin-bottom: 1rem;
    min-height: 2.5rem;
    position: relative;

    span {
      position: absolute;
      bottom: 0;
    }
  }

  & .error {
    color: ${props => props.theme.palette.error};
    font-size: 1.2rem;
    margin-top: -0.8rem;
    margin-left: 0.8rem;
  }
`;

const Input = styled(Select)`
  ${({ theme, error, disabled }) => `
    border: 1px solid ${!error ? theme.palette.geyser : theme.palette.error};
    transition: border ${theme.transitions.default};
    width: 100%;
    font-size: 1.6rem;
    border-radius: 0.4rem;
    background-color: ${disabled ? theme.palette.aquaHaze : theme.palette.white}

    &:focus {
      border: 1px solid ${!error ? theme.palette.cerulean : theme.palette.error};
    }

    &::placeholder {
      color: ${theme.palette.cadetBlue};
    }
  `};
`;

class SelectInput extends React.Component {
  handleChange = (field, value) => {
    this.props.onChange(field, value);
  };

  handleBlur = field => {
    const { onBlur } = this.props;
    if (onBlur) {
      onBlur(field, true);
    }
  };

  render() {
    const {
      className,
      label,
      // error,
      padding,
      options,
      field,
      isMulti,
      placeholder,
      initialValue,
      value,
    } = this.props;

    const hasErrors = !!this.props.error;
    return (
      <Wrapper className={className} padding={padding}>
        {label && (
          <h4 className="label">
            <span>{label}</span>
          </h4>
        )}
        <Input
          className={hasErrors ? 'error' : ''}
          styles={{
            height: '5rem',
          }}
          id="color"
          options={options}
          onChange={event => this.handleChange(field, event)}
          onBlur={this.handleBlur}
          value={value}
          defaultValue={initialValue}
          isMulti={isMulti}
          placeholder={placeholder}
        />
        {hasErrors && <div style={{ color: 'red', marginTop: '.5rem' }}>{this.props.error}</div>}
      </Wrapper>
    );
  }
}

export default styled(SelectInput)``;
