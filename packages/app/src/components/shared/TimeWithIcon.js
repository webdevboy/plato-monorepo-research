import React, { Component } from 'react';

import IconLabel from './IconLabel';
import PropTypes from 'prop-types';
import { formatTimestampToString } from 'utils/time';
import styled from 'styled-components';
import { withUser } from 'components/context/UserContext';

class TimeWithIcon extends Component {
  render() {
    const { className, time, userContext } = this.props;
    return (
      <IconLabel icon="clock" className={className}>
        {formatTimestampToString(time, userContext.timeZone())}
      </IconLabel>
    );
  }
}

TimeWithIcon.propTypes = {
  className: PropTypes.string,
  time: PropTypes.number.isRequired,
  userContext: PropTypes.object.isRequired,
};

TimeWithIcon.defaultProps = {
  className: null,
};

export default styled(withUser(TimeWithIcon))``;
