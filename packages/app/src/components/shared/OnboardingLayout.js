import Button from 'components/shared/Button';
import Pagination from 'components/shared/Pagination';
import React from 'react';
import darkPlato from 'images/dark-plato.png';
import styled from 'styled-components';

const Grid = styled.div`
  display: grid;
  grid-template-columns: auto;
  grid-template-rows: 4px 4rem auto 3rem auto auto 1fr 2rem;
  grid-template-areas:
    'progressBar'
    '.'
    'image'
    'pagination'
    'title'
    'description'
    'content'
    '.';
  justify-items: center;
  width: 100%;
  min-width: 0 !important; /* TODO: Remove this when we don't need to support unresponsive pages */
`;

const ProgressBar = styled.div`
  width: 100%;
  background-color: ${({ theme }) => theme.palette.geyser};
  justify-self: left;
  grid-area: progressBar;

  > div {
    background-color: ${({ theme }) => theme.palette.java};
    width: ${({ width }) => width}%;
    height: 100%;
  }
`;

const Logo = styled.img`
  display: none;
  height: 4rem;
  left: 1rem;
  position: absolute;
  top: 1rem;

  @media only screen and (min-width: 768px) {
    display: block;
  }
`;

const Image = styled.img`
  grid-area: image;
  height: 13rem;
`;

const StyledPagination = styled(Pagination)`
  font-size: 1.5rem;
  color: ${props => props.theme.palette.cerulean};
  grid-area: pagination;
`;

const Title = styled.h1`
  padding: 0 5rem;
  font-size: 2rem;
  color: ${props => props.theme.palette.black};
  grid-area: title;
  text-align: center;
`;

const Description = styled.div`
  font-size: 1.6rem;
  grid-area: description;
  text-align: center;
  margin-bottom: 2rem;
  line-height: 150%;
  padding: 0 2rem;

  @media only screen and (min-width: 1025px) {
    max-width: 40%;
    padding: 0;
  }
`;

const Content = styled.section`
  grid-area: content;
  width: 100%;
  padding: 0 1rem;

  @media only screen and (min-width: 1025px) {
    width: ${({ width }) => width || 60}%;
    padding: 0;
  }
`;

const ButtonsWrapper = styled.div`
  display: grid;
  grid-template-columns: auto auto;
  width: 100%;
  margin-top: 8rem;

  .back-button {
    grid-column: 1/2;
  }

  .save-button {
    grid-column: 2/3;
    justify-self: flex-end;
  }
`;

const OnboardingLayout = ({
  currentPage = null,
  totalPages,
  title,
  description,
  image,
  children,
  onPrevious,
  onNext,
  isNextEnabled = true,
  nextButtonLabel = null,
  caption = null,
}) => (
  <Grid>
    <Logo className="logo" src={darkPlato} alt="Plato" />
    {currentPage !== null && (
      <ProgressBar width={(currentPage / totalPages) * 100}>
        <div />
      </ProgressBar>
    )}
    {image && <Image src={image} alt={title} />}
    {currentPage !== null && (
      <StyledPagination caption={caption} currentPage={currentPage} total={totalPages} />
    )}
    <Title>{title}</Title>
    <Description>{description}</Description>
    <Content>
      {children}
      <ButtonsWrapper>
        {onPrevious && (
          <Button className="back-button" onClick={onPrevious}>
            Previous
          </Button>
        )}

        {onNext && (
          <Button
            disabled={!isNextEnabled}
            className="save-button"
            variant="primary"
            onClick={onNext}
          >
            {nextButtonLabel || 'Continue'}
          </Button>
        )}
      </ButtonsWrapper>
    </Content>
  </Grid>
);

export default OnboardingLayout;
