import React, { Component } from 'react';
import styled from 'styled-components';
import Text from 'components/shared/Text';
import PersonInfo from 'components/shared/PersonInfo';
import { faLinkedin } from '@fortawesome/free-brands-svg-icons';
import IconLabel from 'components/shared/IconLabel';
import Card from 'components/shared/Card';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { withUser } from 'components/context/UserContext';
import moment from 'moment';
import AmaService from 'services/api/plato/ama/ama';
import AttendanceService from 'services/api/plato/attendance/attendance';
import { withSnackbar } from 'components/context/SnackbarContext';

const MenteeCard = styled(Card)`
  width: 80rem;
  display: flex;
  flex-direction: row;
`;

const LinkWrapper = styled.a`
  display: flex;
  padding-right: 3rem;
`;

const CustomPersonInfo = styled(PersonInfo)`
  width: 45%;
  padding: 3rem;
`;

const MenteePrompt = styled.div`
  padding: 3rem;
  border-left: 1px solid ${props => props.theme.palette.geyser};
`;

const AttendanceCard = styled.div`
  background: ${props => props.theme.palette.aquaHaze};
  padding: 2rem;
  border: 1px solid ${props => props.theme.palette.geyser};
  margin-bottom: 1.5rem;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

const BaseIcon = styled(FontAwesomeIcon)`
  font-size: 2rem;
  margin-right: 1rem;
`;

const CheckIcon = styled(BaseIcon)`
  color: ${props => props.theme.palette.emerald};
  font-size: 2rem;
  cursor: pointer;
  margin: 0 1rem;
`;

const CrossIcon = styled(BaseIcon)`
  color: ${props => props.theme.palette.sunglo};
  font-size: 2rem;
  cursor: pointer;
  margin: 0 1rem;
`;

class AmaAttendances extends Component {
  state = {
    attendees: null,
  };

  componentDidMount() {
    this.getAttendanceInfo(this.props.ama.id);
  }

  async getAttendanceInfo(amaId) {
    const attendees = await AmaService.getAttendances(amaId);
    this.setState({ attendees });
  }

  updateAttendance = async (attendanceId, present) => {
    try {
      const attendance = await AttendanceService.updateAttendance(attendanceId, present);
      this.getAttendanceInfo(attendance.amaId);
    } catch (error) {
      console.error(error);
      this.props.snackbar.error('Something went wrong - sorry!');
    }
  };

  render() {
    const { ama } = this.props;
    const { attendees } = this.state;

    if (this.props.userContext.mentor.id !== ama.mentorId || !attendees) {
      return <div />;
    }

    const timeDifference = moment(ama.callScheduledAt).diff(new Date());
    const isStartingSoon = moment.duration(timeDifference).asHours() <= 1;

    return (
      <>
        <Text margin="3rem 0 1rem" uppercase>
          Attendees ({attendees.length})
        </Text>
        {attendees.map(attendee => (
          <React.Fragment key={attendee.id}>
            <MenteeCard>
              <CustomPersonInfo
                fullName={attendee.menteeFullName}
                title={attendee.menteeTitle}
                thumbnailUrl={attendee.menteePicture.thumbnailUrl}
                companyName={attendee.menteeCompany}
              />
              <LinkWrapper href={attendee.menteeLinkedin}>
                <IconLabel
                  icon={faLinkedin}
                  style={{
                    fontSize: '2rem',
                    color: '#067a95',
                    verticalAlign: 'middle',
                  }}
                />
              </LinkWrapper>
              <MenteePrompt>
                <Text type="label">Prompts</Text>
                <Text>{attendee.question}</Text>
              </MenteePrompt>
            </MenteeCard>
            {isStartingSoon ? (
              <AttendanceCard>
                <div>
                  <Text>Was {attendee.menteeFullName} here?</Text>
                </div>
                <div>
                  {attendee.present === null ? (
                    <>
                      <CheckIcon
                        onClick={() => this.updateAttendance(attendee.id, true)}
                        icon="check"
                      />
                      <CrossIcon
                        onClick={() => this.updateAttendance(attendee.id, false)}
                        icon="times"
                      />
                    </>
                  ) : !!attendee.present === true ? (
                    <>
                      <CheckIcon icon="check-circle" />
                      <CrossIcon
                        onClick={() => this.updateAttendance(attendee.id, false)}
                        icon="times"
                      />
                    </>
                  ) : (
                    <>
                      <CheckIcon
                        onClick={() => this.updateAttendance(attendee.id, true)}
                        icon="check"
                      />
                      <CrossIcon icon="times-circle" />
                    </>
                  )}
                </div>
              </AttendanceCard>
            ) : null}
          </React.Fragment>
        ))}
      </>
    );
  }
}

export default withSnackbar(withUser(AmaAttendances));
