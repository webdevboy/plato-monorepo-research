import React from 'react';
import styled from 'styled-components';

const StyledWrapper = styled.div`
  display: flex;
  height: 100%;

  & > .section {
    padding: 3rem;
  }

  & > .left {
    min-width: 30rem;
    max-width: 30rem;
    background: ${({ theme }) => theme.palette.polar};
    border-right: 1px solid ${({ theme }) => theme.palette.geyser};
  }
`;

// TODO:  Use this everywhere
const HorizontalCardLayout = ({ left, right, className }) => (
  <StyledWrapper className={className}>
    <div className="section left">{left}</div>
    <div className="section right">{right}</div>
  </StyledWrapper>
);

export default styled(HorizontalCardLayout)``;
