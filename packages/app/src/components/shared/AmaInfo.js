import React, { PureComponent } from 'react';

import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import getProfilePictureThumbnail from 'utils/thumbnail';
import styled from 'styled-components';
import TagList from './TagList';
import PersonInfo from './PersonInfo';
import Avatar from './Avatar';

const StyledWrapper = styled.div`
  h4 {
    margin-bottom: 0.5rem;
  }

  ${TagList} {
    margin-top: 1.2rem;
  }

  ${Avatar} {
    margin-right: 1.3rem;
  }

  a:hover {
    text-decoration: none;
  }

  .mentor-link {
    font-weight: 700;
    color: ${({ theme }) => theme.palette.cerulean};
  }
`;

class AmaInfo extends PureComponent {
  render() {
    const {
      className,
      mentorCompanyName,
      mentorFullName,
      mentorId,
      mentorPictureUrl,
      mentorTitle,
      storyId,
      storyName,
      topics,
    } = this.props;
    return (
      <StyledWrapper className={className}>
        <h4>Topic</h4>
        {storyId ? (
          <Link to={`/stories/${storyId}?source=ama`}>
            <h1>{storyName}</h1>
          </Link>
        ) : (
          <h1>{storyName}</h1>
        )}
        <TagList tags={topics} />
        <PersonInfo
          companyName={mentorCompanyName}
          fullName={mentorFullName}
          linkUrl={mentorId ? `/mentors?mentor_id=${mentorId}` : null}
          size="sm"
          thumbnailUrl={getProfilePictureThumbnail(mentorFullName, mentorPictureUrl)}
          title={mentorTitle}
        />
      </StyledWrapper>
    );
  }
}

AmaInfo.propTypes = {
  className: PropTypes.string,
  mentorCompanyName: PropTypes.string,
  mentorFullName: PropTypes.string.isRequired,
  mentorId: PropTypes.number,
  mentorPictureUrl: PropTypes.string.isRequired,
  mentorTitle: PropTypes.string,
  storyId: PropTypes.number,
  storyName: PropTypes.string.isRequired,
  topics: PropTypes.array.isRequired,
};

AmaInfo.defaultProps = {
  className: null,
  mentorId: null,
  storyId: null,
  mentorCompanyName: '',
  mentorTitle: '',
};

export default styled(AmaInfo)``;
