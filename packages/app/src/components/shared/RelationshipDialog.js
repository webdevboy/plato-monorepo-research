import React, { Component } from 'react';

import MenteeService from 'services/api/plato/mentee/mentee';
import PlatoDialog from 'components/shared/PlatoDialog';
import Avatar from 'components/shared/Avatar';
import Button from 'components/shared/Button';
import styled from 'styled-components';
import { withQueryLoader } from 'components/context/QueryLoaderContext';
import { withRouter } from 'react-router-dom';
import { withSnackbar } from 'components/context/SnackbarContext';
import { withUser } from 'components/context/UserContext';
import getProfilePictureThumbnail from 'utils/thumbnail';
import textToHtml from 'utils/textToHtml';
import confetti from 'images/confetti.svg';

const StyledDialog = styled(PlatoDialog)`
  .paper {
    min-height: 30rem;
    min-width: 30rem;
  }
`;

const Body = styled.div`
  display: flex;
  flex-direction: column;
  margin: 2rem;
  padding: 6rem;
  width: 54rem;

  text-align: center;
  font-size: 1.6rem;

  .avatar-container,
  .title,
  .description {
    margin-bottom: 4rem;
  }

  .buttons {
    margin-top: 2rem;
    display: flex;
    align-items: center;

    .action {
      margin-left: auto;
      float: right;
    }
  }

  .avatar-container {
    margin-right: auto;
    margin-left: auto;
    width: 17rem;
    height: 13rem;
    background-image: url(${confetti});

    ${Avatar} {
      margin-top: 2.2rem;
    }
  }
`;

const NB_OF_MONTHS_EXTEND_RELATIONSHIP = 3;
const NB_OF_MONTHS_CREATE_RELATIONSHIP = 6;

const BOOK_RELATIONSHIP_HEADING =
  '{mentorFirstName} is currently available for a Long-Term Mentorship!';
const BOOK_RELATIONSHIP_AFTER_CALL_HEADING =
  'Subscribe to a Long-Term Mentorship with {mentorFullName}';
const EXTEND_RELATIONSHIP_HEADING = 'Extend your Long-Term Mentorship with {mentorFullName}';

const BOOK_RELATIONSHIP_DESCRIPTION =
  "By starting a Long-Term Relationship (LTM) with {mentorFirstName} you will be able to get even more individual insight and feedback by meeting regularly with the same mentor for 6 months.\nSlots are filling up quick, so book now before it's too late!\nIf the LTM isn't a good fit, no worries. At the end of the first call, you can opt out with one click.";
const BOOK_RELATIONSHIP_AFTER_CALL_DESCRIPTION =
  'It seems that you and {mentorFullName} already had a talk that you rated 9 or 10. You can choose to start a Long-Term Mentorship with {mentorFullName} for the next {nbOfMonthsRelationship} months.\nLong-Term Mentorship is a great way to improve skills by receiving deep and personal feedback thus strengthening your network.';
const EXTEND_RELATIONSHIP_DESCRIPTION =
  'Your long-term mentorship with {mentorFullName} has ended. Would you like to extend it for another {nbOfMonthsExtendRelationship} months?';

const BOOK_ONE_OFF_LABEL = 'Just book a one off';
const BOOK_RELATIONSHIP_LABEL = 'Start an LTM';
const SUBSCRIBE_RELATIONSHIP_LABEL = 'Set Long-Term Mentorship';
const EXTEND_RELATIONSHIP_LABEL = 'Yes, extend it!';

const REQUEST_RECEIVED_ALERT =
  'Request received! It might take a few seconds for it to take effect..';

const REGEX_MENTOR_FIRST_NAME = new RegExp('{mentorFirstName}', 'g');
const REGEX_MENTOR_FULL_NAME = new RegExp('{mentorFullName}', 'g');
const REGEX_NB_MONTHS_RELATIONSHIP = new RegExp('{nbOfMonthsRelationship}', 'g');
const REGEX_NB_EXTEND_RELATIONSHIP = new RegExp('{nbOfMonthsExtendRelationship}', 'g');

class RelationshipDialog extends Component {
  state = { loading: true };

  componentDidMount() {
    this.setState({ loading: false });
  }

  handleSetRelationship = async () => {
    const {
      snackbar,
      queryLoader,
      onClose,
      mentorId,
      userContext: {
        user: { menteeId },
      },
    } = this.props;

    queryLoader.showLoader();

    try {
      await MenteeService.createRelationship({
        menteeId,
        mentorId,
        nbOfMonths: NB_OF_MONTHS_CREATE_RELATIONSHIP,
      });
      snackbar.success(REQUEST_RECEIVED_ALERT);
    } catch (error) {
      snackbar.error('Something went wrong - sorry!');
      console.error(error);
    }

    queryLoader.hideLoader();

    onClose();
  };

  handleExtendRelationship = async () => {
    const {
      snackbar,
      queryLoader,
      onClose,
      relationshipId,
      userContext: {
        user: { menteeId },
      },
    } = this.props;

    queryLoader.showLoader();

    try {
      await MenteeService.extendRelationship({
        menteeId,
        relationshipId,
        nbOfMonths: NB_OF_MONTHS_EXTEND_RELATIONSHIP,
      });
      snackbar.success(REQUEST_RECEIVED_ALERT);
    } catch (error) {
      console.error(error);
      snackbar.error('Something went wrong - sorry!');
    }

    queryLoader.hideLoader();

    onClose();
  };

  render() {
    const { loading } = this.state;
    const {
      open,
      onClose,
      onBookOneOff,
      relationshipId,
      hasHadCall,
      mentorFirstName,
      mentorFullName,
      mentorPictureUrl,
    } = this.props;

    if (loading) {
      return <StyledDialog open={open} onClose={onClose} loading={loading} />;
    }

    // Calculate the text to display in the popup
    let title;
    let description;
    let actionLabel;
    let action;
    if (relationshipId) {
      title = EXTEND_RELATIONSHIP_HEADING.replace(REGEX_MENTOR_FULL_NAME, mentorFullName);
      description = EXTEND_RELATIONSHIP_DESCRIPTION.replace(
        REGEX_MENTOR_FULL_NAME,
        mentorFullName
      ).replace(REGEX_NB_EXTEND_RELATIONSHIP, NB_OF_MONTHS_EXTEND_RELATIONSHIP);
      actionLabel = EXTEND_RELATIONSHIP_LABEL;
      action = this.handleExtendRelationship;
    } else if (hasHadCall) {
      title = BOOK_RELATIONSHIP_AFTER_CALL_HEADING.replace(REGEX_MENTOR_FULL_NAME, mentorFullName);
      description = BOOK_RELATIONSHIP_AFTER_CALL_DESCRIPTION.replace(
        REGEX_MENTOR_FULL_NAME,
        mentorFullName
      ).replace(REGEX_NB_MONTHS_RELATIONSHIP, NB_OF_MONTHS_CREATE_RELATIONSHIP);
      actionLabel = SUBSCRIBE_RELATIONSHIP_LABEL;
      action = this.handleSetRelationship;
    } else {
      title = BOOK_RELATIONSHIP_HEADING.replace(REGEX_MENTOR_FIRST_NAME, mentorFirstName);
      description = BOOK_RELATIONSHIP_DESCRIPTION.replace(REGEX_MENTOR_FIRST_NAME, mentorFirstName);
      actionLabel = BOOK_RELATIONSHIP_LABEL;
      action = this.handleSetRelationship;
    }

    return (
      <StyledDialog open={open} onClose={onClose} loading={loading}>
        <Body>
          <div className="avatar-container">
            <Avatar
              src={getProfilePictureThumbnail(mentorFullName, mentorPictureUrl)}
              alt="Mentor avatar"
              size="xl"
            />
          </div>

          <h1 className="title">{title}</h1>

          <p className="description">{textToHtml(description)}</p>
          <div className="buttons">
            {onBookOneOff && !relationshipId && (
              <Button className="one-off" variant="primary-link" onClick={onBookOneOff}>
                {BOOK_ONE_OFF_LABEL}
              </Button>
            )}
            <Button className="action" variant="primary" onClick={action}>
              {actionLabel}
            </Button>
          </div>
        </Body>
      </StyledDialog>
    );
  }
}

export default styled(withSnackbar(withUser(withQueryLoader(withRouter(RelationshipDialog)))))``;
