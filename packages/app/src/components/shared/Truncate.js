/* Hack to truncate long multi-line strings */
const Truncate = ({ maxLength, children }) =>
  children.length > maxLength ? `${children.slice(0, maxLength)}...` : children;

export default Truncate;
