import styled from 'styled-components';

export default styled.h3`
  margin-bottom: 3rem;
  color: ${props => props.theme.palette.white};
`;
