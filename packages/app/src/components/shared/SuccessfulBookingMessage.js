import React from 'react';
import styled from 'styled-components';
import DateTimeWithIcons from './DateTimeWithIcons';

const Container = styled.div`
  .message {
    font-size: 1.6rem;
    font-weight: 700;
    margin-bottom: 1rem;
  }

  & ${DateTimeWithIcons} svg {
    color: ${({ theme }) => theme.palette.white};
  }
`;

const SuccessfulBookingMessage = ({ message, timestamp }) => (
  <Container>
    <p>{message}</p>
    <DateTimeWithIcons timestamp={timestamp} />
  </Container>
);

export default SuccessfulBookingMessage;
