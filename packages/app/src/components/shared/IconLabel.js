import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';
import styled from 'styled-components';

const StyledWrapper = styled.div`
  display: flex;
  align-items: center;
  font-size: 1.6rem;

  svg {
    color: ${props => props.theme.palette.cadetBlue};
    margin-right: 1rem;
  }
`;

const IconLabel = ({ icon, className, children, style }) => (
  <StyledWrapper className={className}>
    <FontAwesomeIcon icon={icon} style={style} />
    <p>{children}</p>
  </StyledWrapper>
);

export default IconLabel;
