import React from 'react';
import styled from 'styled-components';

const PageWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  background: ${props => props.theme.palette.mirage};
  background-image: url("${process.env.PUBLIC_URL}/img/plato-bg.svg");
  background-repeat: no-repeat;
  background-position: right;
  min-height: 100vh;
  min-width: 100%;

  & .logo {
    position: absolute;
    top: 3rem;
    left: 50%;
    transform: translateX(-50%);
  }
`;

const BrandedPage = props => (
  <PageWrapper className={props.className}>
    <img
      className="logo"
      src="https://d1muf25xaso8hp.cloudfront.net/https%3A%2F%2Fs3.amazonaws.com%2Fappforest_uf%2Ff1528975785447x249209639616310600%2FArtboard%25203%2520Copy.png?w=128&h=32&auto=compress&fit=crop"
      alt="Plato logo"
    />

    {props.children}
  </PageWrapper>
);

export default styled(BrandedPage)``;
