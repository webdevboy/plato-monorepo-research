import React, { Component } from 'react';

import AmaTrackList from './AmaTrackList';
import Button from './Button';
import CheckList from './CheckList';
import DateWithIcon from './DateWithIcon';
import DaysUntil from './DaysUntil';
import IconLabel from './IconLabel';
import TimeWithIcon from './TimeWithIcon';
import styled from 'styled-components';
import theme from 'styles/theme';

const Card = styled.div`
  border-radius: 4px;
  box-shadow: 0 6px 8px 0 rgba(34, 42, 63, 0.1), 0 3px 4px 0 rgba(10, 31, 68, 0.1),
    0 0 2px 0 rgba(10, 31, 68, 0.08);
  background-color: ${props => props.theme.palette.white};
  margin-bottom: 3rem;
  border-top: 0.4rem solid ${props => props.theme.palette.cerulean};

  @keyframes anim {
    0% {
      display: none;
      opacity: 0;
    }
    1% {
      display: block;
      opacity: 0;
      transform: scaleY(0);
    }
    100% {
      opacity: 1;
      transform: scaleY(1);
    }
  }
`;

const CardSection = styled.div`
  padding: 0 3rem;
  display: grid;
  grid-template-columns: auto;
  grid-template-rows: auto auto auto auto;
  grid-template-areas:
    'descriptions'
    'small-descriptions';
  justify-items: center;
  width: 100%;

  @media only screen and (min-width: 1025px) {
    grid-template-columns: 2fr 1fr;
    grid-template-rows: auto;
  }
`;

const Descriptions = styled.div`
  color: ${props => props.theme.palette.ebonyClay};
  grid-area: descriptions;
  padding-bottom: 2rem;
  margin-bottom: 1rem;
  border-bottom: 1px solid ${props => props.theme.palette.cadetBlue};

  @media only screen and (min-width: 1025px) {
    padding-right: 5rem;
    padding-bottom: 3rem;
    margin-bottom: 0;
    border-right: 1px solid ${props => props.theme.palette.cadetBlue};
    border-bottom: none;
  }
`;

const DetailsWrapper = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: space-evenly;

  @media only screen and (min-width: 1025px) {
    padding: 0 1rem 0 4rem;

    min-height: 25rem;
    max-height: 70rem;
  }
`;

const SmallDescriptions = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-template-rows: auto;
  width: 100%;
  text-align: center;
  font-size: 1.5rem;
  height: 9rem;
  padding-bottom: 1.5rem;

  @media only screen and (min-width: 1025px) {
    grid-template-columns: 1fr;
    text-align: left;
    height: 15rem;
  }
`;

const Title = styled.h1`
  margin: 2rem 0;
`;

const Description = styled.p`
  font-size: 1.5rem;
`;

const Label = styled.p`
  font-size: 1.8rem;
  font-weight: bold;
  color: ${props => props.theme.palette.cadetBlue};
  text-transform: uppercase;
`;

const InfoWrapper = styled.div`
  display: flex;
  margin: 0.5rem 0 3rem;
  justify-content: space-between;

  @media only screen and (min-width: 1025px) {
    flex-direction: column;
  }
`;

const AmaList = styled.div`
  grid-area: detail;
  opacity: ${props => (props.toggleShowAmas ? 1 : 0)};
  display: ${props => (props.toggleShowAmas ? 'block' : 'none')};

  background-color: ${props => props.theme.palette.polar};
  margin-bottom: 2rem;

  border-bottom-left-radius: 4px;
  border-bottom-right-radius: 4px;

  animation: anim 0.2s ease-in-out;
  transform-origin: top;
`;

const FullWidthButton = styled(Button)`
  width: 100%;
  min-height: 4rem;
  margin-bottom: 2.5rem;
`;

class AmaTrackCard extends Component {
  state = {
    toggleShowAmas: false,
  };

  handleToggleShowAmas = () => {
    const { toggleShowAmas } = this.state;
    this.setState({
      toggleShowAmas: !toggleShowAmas,
    });
  };

  handleApplyTrack = () => {
    const { program, onApplyTrack } = this.props;
    onApplyTrack(program);
  };

  render() {
    const { track, program } = this.props;
    const { toggleShowAmas } = this.state;

    return (
      <Card>
        <CardSection>
          <Descriptions>
            <Title>{track && track.name}</Title>
            <Description>{track && track.description}</Description>
          </Descriptions>
          <DetailsWrapper>
            <SmallDescriptions>
              <IconLabel style={{ color: theme.palette.ebonyClay }} icon={['far', 'flag']}>
                Starts <DaysUntil inline timestamp={program.startDate || Date.now()} />
              </IconLabel>
              <IconLabel style={{ color: theme.palette.ebonyClay }} icon="video">
                {program.amas.length} AMA Sessions
              </IconLabel>
              <IconLabel
                style={{
                  color: program.amas.length > 6 ? theme.palette.sunglo : theme.palette.emerald,
                }}
                icon={['far', 'map']}
              >
                {program.amas.length > 6 ? 'Long' : 'Short'} Track
              </IconLabel>
            </SmallDescriptions>
            <FullWidthButton onClick={this.handleToggleShowAmas} variant="secondary">
              {toggleShowAmas ? 'Hide' : 'Show'} Details
            </FullWidthButton>
          </DetailsWrapper>
        </CardSection>
        <AmaList toggleShowAmas={toggleShowAmas}>
          <CardSection>
            <AmaTrackList amas={program.amas} />
            <DetailsWrapper>
              <Title>Details</Title>
              <Label>Starts on</Label>
              <InfoWrapper>
                <DateWithIcon date={program.startDate} />
                <TimeWithIcon time={program.startDate} />
              </InfoWrapper>
              <Label>Track duration</Label>
              <InfoWrapper>
                <IconLabel icon="clock">{program.durationInWeeks} Weeks</IconLabel>
              </InfoWrapper>
              <Label>Prerequisites</Label>
              <InfoWrapper>
                <CheckList text={track ? track.criteria : ''} />
              </InfoWrapper>
              <FullWidthButton
                data-program={program.id}
                onClick={this.handleApplyTrack}
                variant="primary"
              >
                Apply to this track
              </FullWidthButton>
            </DetailsWrapper>
          </CardSection>
        </AmaList>
      </Card>
    );
  }
}

export default AmaTrackCard;
