import React, { Component } from 'react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import styled from 'styled-components';

const StyledLabel = styled.label`
  display: flex;
  flex-direction: row;
  cursor: pointer;

  .input {
    position: relative;
    height: 16px;
    margin: 0.5rem 1rem 0 0;
    position: relative;
    text-align: left;
    width: 16px;

    .check-icon {
      display: none;
      font-size: 12px;
      left: 0.2rem;
      position: absolute;
      top: 0.4rem;
    }

    input {
      -webkit-appearance: none;
      -moz-appearance: none;
      appearance: none;
      background-color: ${({ theme }) => theme.palette.white};
      border-radius: 2px;
      border: solid 1px ${({ theme }) => theme.palette.geyser};
      height: 16px;
      width: 16px;

      &:active {
        background-color: ${({ theme }) => theme.palette.white};
        border-color: ${({ theme }) => theme.palette.java};
      }

      &:checked {
        background-color: ${({ theme }) => theme.palette.java};
        border-color: ${({ theme }) => theme.palette.java};
      }
    }
  }

  &:hover input {
    background-color: ${({ theme }) => theme.palette.white};
    border-color: ${({ theme }) => theme.palette.java};
  }

  &:hover .check-icon {
    display: none;
  }

  &.checked .check-icon {
    display: block;
    color: ${({ theme }) => theme.palette.white};
  }
`;

class SessionCheckbox extends Component {
  render() {
    const { className, isChecked, name, label, onChange } = this.props;
    return (
      <StyledLabel
        className={classNames(className, { checked: isChecked })}
        onClick={this.handleClick}
      >
        <span className="input">
          <input type="checkbox" name={name} checked={isChecked} onChange={onChange} />
          <FontAwesomeIcon icon="check" className="check-icon" />
        </span>
        <span>{label}</span>
      </StyledLabel>
    );
  }
}

SessionCheckbox.propTypes = {
  className: PropTypes.string,
  isChecked: PropTypes.bool,
  name: PropTypes.string.isRequired,
  label: PropTypes.oneOfType([PropTypes.node, PropTypes.string]).isRequired,
  onChange: PropTypes.func,
};

SessionCheckbox.defaultProps = {
  className: null,
  isChecked: false,
  onChange: () => {},
};

export default styled(SessionCheckbox)``;
