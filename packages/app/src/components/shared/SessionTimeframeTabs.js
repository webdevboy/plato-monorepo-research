import React from 'react';
import styled from 'styled-components';
import Button from './Button';

const StyledWrapper = styled.div`
  display: flex;

  & .future {
    margin-right: 2rem;
  }
`;

const SessionTimeframeTabs = ({ selectedTimeframe, onChange, className }) => (
  <StyledWrapper className={className}>
    <Button
      className="future"
      variant="tab"
      active={selectedTimeframe === 'future'}
      onClick={() => onChange('future')}
    >
      Your Future Sessions
    </Button>
    <Button variant="tab" active={selectedTimeframe === 'past'} onClick={() => onChange('past')}>
      Your Past Sessions
    </Button>
  </StyledWrapper>
);

export default styled(SessionTimeframeTabs)``;
