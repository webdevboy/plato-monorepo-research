import React, { Component } from 'react';
import Dialog from '@material-ui/core/Dialog';
import Button from './Button';
import styled from 'styled-components';
import Fade from '@material-ui/core/Fade';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const StyledDialog = styled(({ ...other }) => (
  <Dialog
    {...other}
    classes={{
      paper: 'paper',
    }}
  />
))`
  & .step-container {
    display: flex;
    align-items: center;
    min-height: 52.5rem;
  }

  & .actions {
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: 3rem;

    & ${Button} {
      display: flex;
      align-items: center;
    }

    & .previous {
      padding-left: 0.5rem;
    }

    & .next {
      padding-right: 0.5rem;
    }
  }
`;

class CarouselDialog extends Component {
  constructor(props) {
    super(props);

    this.state = {
      step: props.initialStep || 0,
      nextStep: 0,
      transitioning: false,
      actionsEnabled: false,
    };
  }

  handlePreviousClick = () => {
    this.setState(prevState => ({
      nextStep: prevState.nextStep - 1,
      transitioning: true,
      actionsEnabled: false,
    }));
  };

  handleNextClick = () => {
    this.setState(prevState => ({
      nextStep: prevState.nextStep + 1,
      transitioning: true,
      actionsEnabled: false,
    }));
  };

  showNextStep = () => {
    this.setState(prevState => ({
      step: prevState.nextStep,
      transitioning: false,
    }));
  };

  enableActions = () => {
    this.setState({ actionsEnabled: true });
  };

  render() {
    const { step, transitioning, actionsEnabled } = this.state;
    const { steps, open, className } = this.props;

    return (
      <StyledDialog open={open} className={className}>
        <Fade in={!transitioning} onExited={this.showNextStep} onEntered={this.enableActions}>
          <div className="step-container">{steps[step]}</div>
        </Fade>
        {step !== steps.length - 1 && (
          <div className="actions">
            <Button
              variant="secondary"
              disabled={step === 0 || !actionsEnabled}
              onClick={this.handlePreviousClick}
              style={{ opacity: step === 0 ? 0 : 1 }}
            >
              <FontAwesomeIcon icon="arrow-left" />
              <span className="previous">Previous</span>
            </Button>
            <h2>
              {step + 1}/{steps.length}
            </h2>
            <Button variant="primary" disabled={!actionsEnabled} onClick={this.handleNextClick}>
              <span className="next">Next</span>
              <FontAwesomeIcon icon="arrow-right" />
            </Button>
          </div>
        )}
      </StyledDialog>
    );
  }
}

export default CarouselDialog;
