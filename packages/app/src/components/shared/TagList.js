import React from 'react';
import styled from 'styled-components';
import Tag from './Tag';

const StyledWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;

  & ${Tag} {
    margin-bottom: 1rem;

    &:not(:last-child) {
      margin-right: 1rem;
    }
  }
`;

const TagList = ({ className, tags, limit = 5 }) => (
  <StyledWrapper className={className}>
    {tags.slice(0, limit).map(tag => (
      <Tag key={tag} text={tag} />
    ))}
  </StyledWrapper>
);

export default styled(TagList)``;
