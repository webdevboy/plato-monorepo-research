import styled from 'styled-components';

const Button = styled.button`
  font-size: 1.4rem;
  font-weight: bold;
  text-transform: uppercase;
  padding: 1rem 2rem;
  border-radius: 0.4rem;
  width: fit-content;
  cursor: pointer;
  transition: color ${props => props.theme.transitions.default},
    background ${props => props.theme.transitions.default};

  &:disabled {
    cursor: default;
    border-color: ${props => props.theme.palette.geyser};
  }

  ${({ variant, theme: { palette } }) =>
    variant === 'primary' &&
    `
    background: ${palette.java};
    color: ${palette.white};
    border: 1px solid ${palette.java};

    &:not(:disabled):hover {
      background: ${palette.cerulean};
    }

    &:disabled {
      background: ${palette.geyser};
    }
  `};

  ${({ variant, theme: { palette } }) =>
    variant === 'secondary' &&
    `
    background: transparent;
    color: ${palette.java};
    border: 1px solid ${palette.java};

    &:not(:disabled):hover {
      background: rgba(0, 168, 207, 0.1);
    }

    &:disabled {
      color: ${palette.cadetBlue};
    }
  `};

  ${({ variant, theme: { palette } }) =>
    variant === 'primary-link' &&
    `
    color: ${palette.java};
    padding: 0;
    background: transparent;
    border: none;

    &:not(:disabled) svg {
      color: ${palette.java};
    }

    &:not(:disabled):hover,
    &:not(:disabled):hover svg {
      color: ${palette.cerulean};
    }

    &:disabled,
    &:disabled svg {
      color: ${palette.cadetBlue};
    }
  `};

  ${({ variant, theme: { palette } }) =>
    variant === 'secondary-link' &&
    `
    color: ${palette.cadetBlue};
    padding: 0;
    background: transparent;
    border: none;

    &:not(:disabled) svg {
      color: ${palette.cadetBlue};
    }

    &:not(:disabled):hover,
    &:not(:disabled):hover svg {
      color: ${palette.osloGrey};
    }

    &:disabled,
    &:disabled svg {
      color: ${palette.osloGrey};
    }
  `};

  ${({ variant, theme: { palette }, active }) =>
    variant === 'tab' &&
    `
    color: ${palette.java};
    padding: 0;
    padding-bottom: 2rem;
    background: transparent;
    border: none;
    border-bottom: 2px solid ${palette.java};
    border-radius: 0;

    ${!active &&
      `
        border: none;
        color: ${palette.cadetBlue};


        &:hover {
          color: ${palette.osloGrey};
        }
    `}
  `};
`;

export default styled(Button)``;
