import React, { Component } from 'react';
import { formatRemainingTime, formatTimestampToDate } from 'utils/time';

import BriefService from 'services/api/plato/brief/brief';
import Button from 'components/shared/Button';
import MenteeService from 'services/api/plato/mentee/mentee';
import MentorService from 'services/api/plato/mentor/mentor';
import PersonInfo from 'components/shared/PersonInfo';
import PlatoDialog from 'components/shared/PlatoDialog';
import RadioInput from 'components/shared/RadioInput';
import RequestService from 'services/api/plato/request/request';
import SuccessMessage from 'components/shared/SuccessMessage';
import SuccessfulBookingMessage from 'components/shared/SuccessfulBookingMessage';
import moment from 'moment-timezone';
import styled from 'styled-components';
import { trackEventBooked } from 'utils/analytics';
import { withQueryLoader } from 'components/context/QueryLoaderContext';
import { withRouter } from 'react-router-dom';
import { withSnackbar } from 'components/context/SnackbarContext';
import { withUser } from 'components/context/UserContext';

const StyledDialog = styled(PlatoDialog)`
  .paper {
    min-height: 30rem;
    min-width: 30rem;
  }
`;

const Body = styled.div`
  display: flex;
  flex-direction: column;
  padding: 2rem;
  width: 54rem;

  h4 {
    margin-top: 3rem;
  }

  ${RadioInput} {
    margin-top: 1rem;
    border: 1px solid ${({ theme }) => theme.palette.geyser};
    border-radius: 2px;
    padding: 1rem 2rem;
  }

  .new-challenge-input {
    width: 100%;
    border: none;
    font-size: 1.6rem;
  }

  textarea {
    width: 100%;
    border-color: ${({ theme }) => theme.palette.geyser};
    border-radius: 2px;
    border-top: none;
    font-size: 1.6rem;
    padding: 1rem 2rem;
  }

  .challenges-container {
    overflow-y: scroll;
    max-height: 30rem;
    margin-top: 1rem;

    ${RadioInput}:first-child {
      margin-top: 0;
    }
  }

  .selected-challenge {
    border: 1px solid ${({ theme }) => theme.palette.geyser};
    border-radius: 2px;
    padding: 1rem;
    margin-top: 2rem;

    h2 {
      margin-bottom: 1rem;
    }
  }

  .confirm-session {
    align-self: flex-end;
    margin-top: 2rem;
  }

  ${SuccessMessage} {
    margin-top: 2rem;
    margin-left: -2rem;
    margin-right: -2rem;
    margin-bottom: -2rem;
  }
`;

class BookMeetingDialog extends Component {
  constructor(props) {
    super(props);
    const { challenge } = props;
    this.state = {
      loading: true,
      selectedAvailability: undefined,
      selectedChallenge: challenge,
      saving: false,
      successfullyBooked: false,
      mentor: undefined,
      availabilities: [],
      challenges: [],
      newBriefOneLiner: '',
      newBriefDescription: '',
    };
  }

  async componentDidMount() {
    const {
      mentorId,
      snackbar,
      userContext: {
        user: { menteeId },
      },
    } = this.props;

    this.setState({ loading: true });

    try {
      const [mentor, availabilities, challenges] = await Promise.all([
        MentorService.getMentor(mentorId),
        MentorService.getAvailabilities(mentorId),
        MenteeService.getBriefs({ menteeId }),
      ]);

      this.setState({
        mentor,
        availabilities,
        challenges,
      });
    } catch (error) {
      console.error(error);
      snackbar.error('Something went wrong - sorry!');
    }

    this.setState({ loading: false });
  }

  handleChallengeSelection = (value, name) => {
    this.setState({ [name]: value });
  };

  handleNewBriefOneLinerChange = event => {
    this.setState({ newBriefOneLiner: event.target.value });
  };

  handleNewBriefDescriptionChange = event => {
    this.setState({ newBriefDescription: event.target.value });
  };

  handleRedirectToSession = () => {
    const { history } = this.props;
    history.push('/sessions');
  };

  handleConfirmSessionWithRequest = async () => {
    // A request ID has been provided, book a session for that request
    const { selectedAvailability } = this.state;
    const {
      userContext: {
        user: { menteeId },
      },
      requestId,
      snackbar,
      queryLoader,
    } = this.props;

    this.setState({ saving: true });
    queryLoader.showLoader();

    try {
      await RequestService.book({
        requestId,
        eventId: selectedAvailability.id,
      });

      trackEventBooked({
        menteeId,
        eventId: selectedAvailability.id,
        eventType: 'request',
        eventCallScheduledDate: selectedAvailability.startTime,
      });

      this.setState({
        saving: false,
        successfullyBooked: true,
      });
    } catch (error) {
      console.error(error);
      snackbar.error('Something went wrong - sorry!');
    }

    queryLoader.hideLoader();
  };

  handleConfirmSession = async () => {
    // Create a request and book a session
    const {
      selectedAvailability,
      selectedChallenge,
      newBriefOneLiner,
      newBriefDescription,
    } = this.state;
    const {
      userContext: {
        user: { uuid, email, menteeId, fullName },
      },
      mentorId,
      story,
      snackbar,
      queryLoader,
    } = this.props;

    this.setState({ saving: true });
    queryLoader.showLoader();

    try {
      let createRequestResult;

      if (story) {
        // The request is created against a story
        createRequestResult = await RequestService.createRequest({
          menteeId,
          shareType: 'From story',
          briefingOneLiner: `${fullName} seems to be facing some issues you have been through in your experience.`,
          briefingDescription: `They would like to know more about the story ${story.name}`,
        });
      }

      if (selectedChallenge === 'new-challenge') {
        // We are creating a new brief for the request
        const { result: createdBrief } = await BriefService.createBrief({
          menteeId,
          briefingOneLiner: newBriefOneLiner,
          briefingDescription: newBriefDescription,
          shareType: 'From mentor',
        });

        // Actually create the request
        createRequestResult = await RequestService.createRequest({
          menteeId,
          shareType: 'From mentor',
          briefId: createdBrief.id,
          briefingOneLiner: newBriefOneLiner,
          briefingDescription: newBriefDescription,
        });

        // Close the created brief
        await BriefService.closeBrief(createdBrief.id);
      } else {
        // Create the request for an existing brief
        createRequestResult = await RequestService.createRequest({
          menteeId,
          shareType: 'From mentor',
          briefId: selectedChallenge.id,
          briefingOneLiner: selectedChallenge.briefingOneLiner,
          briefingDescription: selectedChallenge.briefingDescription,
        });

        // Close the brief
        if (selectedChallenge.toMatch) {
          await BriefService.closeBrief(selectedChallenge.id);
        }
      }

      const { result: createdRequest } = createRequestResult;

      await RequestService.matchAndPotentialBook({
        requestId: createdRequest.id,
        mentorId,
        storyId: story ? story.id : undefined,
        eventId: selectedAvailability.id,
      });

      trackEventBooked({
        email,
        eventCallScheduledDate: selectedAvailability.startTime,
        menteeId,
        eventId: selectedAvailability.id,
        eventType: 'request',
        uuid,
      });

      this.setState({ successfullyBooked: true });
    } catch (error) {
      console.error(error);
      snackbar.error('Something went wrong - sorry!');
    }

    this.setState({ saving: false });
    queryLoader.hideLoader();
  };

  validate() {
    const {
      newBriefOneLiner,
      newBriefDescription,
      saving,
      selectedAvailability,
      selectedChallenge,
    } = this.state;
    if (!selectedAvailability) {
      return false;
    }

    if (saving) {
      // The form is beeing saved asynchronously
      return false;
    }

    if (
      selectedChallenge === 'new-challenge' &&
      newBriefOneLiner !== '' &&
      newBriefDescription !== ''
    ) {
      // A challenge is being created for the session
      return true;
    }

    if (selectedChallenge && selectedChallenge !== 'new-challenge') {
      // An existing challenge has been selected
      return true;
    }

    return false;
  }

  renderChallengeSelection() {
    const { challenges, selectedChallenge, newBriefOneLiner, newBriefDescription } = this.state;
    return (
      <>
        <h4>Choose a challenge</h4>

        <div className="challenges-container">
          <RadioInput
            name="selectedChallenge"
            label={
              selectedChallenge === 'new-challenge' ? (
                <input
                  autoFocus
                  className="new-challenge-input"
                  type="text"
                  name="oneLiner"
                  placeholder={
                    selectedChallenge === 'new-challenge'
                      ? 'In one line, describe your challenge (required)'
                      : 'Enter a new challenge'
                  }
                  value={newBriefOneLiner}
                  onChange={this.handleNewBriefOneLinerChange}
                />
              ) : (
                <sub>{newBriefOneLiner || 'Enter a new challenge'}</sub>
              )
            }
            onChange={() => this.handleChallengeSelection('new-challenge', 'selectedChallenge')}
            isChecked={selectedChallenge === 'new-challenge'}
          />
          {selectedChallenge === 'new-challenge' && (
            <textarea
              name="description"
              placeholder="In a few lines, add any relevant information for the mentor (required)"
              value={newBriefDescription}
              onChange={this.handleNewBriefDescriptionChange}
            />
          )}

          {challenges.map(challenge => (
            <RadioInput
              key={challenge.id}
              className="radio-input-box"
              name="selectedChallenge"
              label={<b>{challenge.briefingOneLiner}</b>}
              onChange={() => this.handleChallengeSelection(challenge, 'selectedChallenge')}
              isChecked={selectedChallenge && selectedChallenge.id === challenge.id}
            />
          ))}
        </div>
      </>
    );
  }

  renderContent() {
    const {
      availabilities,
      mentor,
      newBriefDescription,
      newBriefOneLiner,
      selectedAvailability,
      selectedChallenge,
      successfullyBooked,
    } = this.state;
    const { requestId, userContext } = this.props;

    if (successfullyBooked) {
      return (
        <>
          {selectedChallenge && (
            <div className="selected-challenge">
              <h2>
                {selectedChallenge === 'new-challenge'
                  ? newBriefOneLiner
                  : selectedChallenge.briefingOneLiner}
              </h2>
              <p>
                {selectedChallenge === 'new-challenge'
                  ? newBriefDescription
                  : selectedChallenge.briefingDescription}
              </p>
            </div>
          )}

          <SuccessMessage onClick={this.handleRedirectToSession} buttonText="View My Sessions">
            <SuccessfulBookingMessage
              message={`You successfully booked a session with ${mentor.firstName}`}
              timestamp={selectedAvailability.startTime}
            />
          </SuccessMessage>
        </>
      );
    }

    return (
      <>
        <h4>Choose a time slot</h4>

        {availabilities.slice(0, 3).map(availability => (
          <RadioInput
            key={availability.id}
            className="radio-input-box"
            name="selectedAvailability"
            label={
              <>
                <b>{formatRemainingTime(availability.startTime)}</b>
                &nbsp;&nbsp;
                <sub>
                  {formatTimestampToDate(
                    availability.startTime,
                    userContext.timeZone(),
                    'dddd MMMM Do YYYY, H:mm A'
                  )}
                  &nbsp;({moment.tz(userContext.timeZone()).zoneName()})
                </sub>
              </>
            }
            onChange={() => this.handleChallengeSelection(availability, 'selectedAvailability')}
            isChecked={selectedAvailability === availability}
          />
        ))}

        {this.renderChallengeSelection()}

        <Button
          className="confirm-session"
          variant="primary"
          onClick={requestId ? this.handleConfirmSessionWithRequest : this.handleConfirmSession}
          disabled={!this.validate()}
        >
          Confirm Session
        </Button>
      </>
    );
  }

  render() {
    const { loading, mentor } = this.state;
    const { open, onClose } = this.props;

    if (loading) {
      return <StyledDialog open={open} onClose={onClose} loading={loading} />;
    }

    return (
      <StyledDialog open={open} onClose={onClose} loading={loading}>
        <Body>
          <PersonInfo
            size="md"
            fullName={mentor.fullName}
            title={mentor.title}
            thumbnailUrl={mentor.picture.thumbnailUrl}
            companyName={mentor.companyName}
            linkUrl={`/mentors/${mentor.id}`}
          />
          {this.renderContent()}
        </Body>
      </StyledDialog>
    );
  }
}

export default styled(withSnackbar(withUser(withQueryLoader(withRouter(BookMeetingDialog)))))``;
