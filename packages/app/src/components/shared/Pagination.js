import React, { Component } from 'react';

import PropTypes from 'prop-types';
import styled from 'styled-components';

const StyledPagination = styled.sub`
  text-align: center;
  font-weight: bold;
  color: ${({ theme }) => theme.palette.cadetBlue};

  > span {
    text-transform: uppercase;
    color: ${({ theme }) => theme.palette.cerulean};
  }
`;

class Pagination extends Component {
  render() {
    const { className, currentPage, caption, total } = this.props;
    return (
      <StyledPagination className={className}>
        <span>
          {caption && `${caption} • `}step {currentPage}
        </span>
        &nbsp;/ {total}
      </StyledPagination>
    );
  }
}

Pagination.propTypes = {
  currentPage: PropTypes.number.isRequired,
  className: PropTypes.string,
  total: PropTypes.number.isRequired,
  caption: PropTypes.string,
};

Pagination.defaultProps = {
  className: null,
  caption: null,
};

export default styled(Pagination)``;
