import React from 'react';
import styled from 'styled-components';

const StyledCard = styled.div`
  border: 1px solid ${({ theme }) => theme.palette.geyser};
  border-radius: 0.2rem;
  box-shadow: ${({ theme }) => theme.shadows[0]};
  background: ${({ theme }) => theme.palette.white};
`;

const Card = props => {
  const { children, ...other } = props;

  return <StyledCard {...other}>{props.children}</StyledCard>;
};

export default styled(Card)``;
