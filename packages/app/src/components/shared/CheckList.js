import React from 'react';
import IconLabel from './IconLabel';

/**
 * Helper that converts a block of dash-separated text into a list of text with a check icon.
 * @param {String} text String to be converted
 * @param {String} icon Name of the FontAwesome Icon
 * @return {Component} A list of text with a check or desired icon.
 */
const CheckList = ({ icon, text }) => (
  <div>
    {text.split('- ').map(criteria =>
      criteria.length ? (
        <div style={{ padding: '0.8rem 0' }} key={criteria}>
          <IconLabel icon={icon || 'check'}>{criteria}</IconLabel>
        </div>
      ) : null
    )}
  </div>
);

export default CheckList;
