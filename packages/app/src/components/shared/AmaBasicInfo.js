import { formatTimestampToDate, formatTimestampToString } from 'utils/time';

import IconLabel from 'components/shared/IconLabel';
import { Link } from 'react-router-dom';
import React from 'react';
import SuggestedAgendaListItem from 'components/pages/mentor/mentorAmaDetail/SuggestedAgendaListItem';
import styled from 'styled-components';
import { withUser } from 'components/context/UserContext';

const StoryName = styled.div`
  font-size: 1.5rem;
  text-decoration: underline;
`;

const AmaBasicInfo = ({ isMentor, callScheduledAt, storyId, storyName, userContext }) => (
  <>
    <h4>Basic details</h4>
    <IconLabel icon="calendar" className="info">
      {formatTimestampToDate(callScheduledAt, userContext.timeZone())}
    </IconLabel>
    <IconLabel icon="clock" className="info">
      {formatTimestampToString(callScheduledAt, userContext.timeZone())}
    </IconLabel>
    {isMentor ? (
      <>
        <h4>Story&apos;s Theme</h4>
        {storyId ? (
          <Link to={`/mentor_story/${storyId}`}>
            <StoryName>{storyName}</StoryName>
          </Link>
        ) : (
          <StoryName>{storyName}</StoryName>
        )}

        <h4>Suggested agenda</h4>
        <ul>
          <SuggestedAgendaListItem minutes="5">Quick round table</SuggestedAgendaListItem>
          <SuggestedAgendaListItem minutes="10">Retell your story</SuggestedAgendaListItem>
          <SuggestedAgendaListItem minutes="15">Q&amp;A</SuggestedAgendaListItem>
          <SuggestedAgendaListItem minutes="5">
            Ask mentees to recount
            <br />
            their key takeaways
          </SuggestedAgendaListItem>
        </ul>
      </>
    ) : (
      <>
        <h4>Story&apos;s Theme</h4>
        {storyId ? (
          <Link to={`/stories/${storyId}`}>
            <StoryName>{storyName}</StoryName>
          </Link>
        ) : (
          <StoryName>{storyName}</StoryName>
        )}
      </>
    )}
  </>
);

export default withUser(AmaBasicInfo);
