import React from 'react';
import { Dialog } from '@material-ui/core';
import styled from 'styled-components';
import SuccessMessage from './SuccessMessage';

const Message = styled.div`
  display: flex;
  flex-direction: column;

  & .title {
    font-size: 1.6rem;
    font-weight: 700;
  }
`;

const SuccessDialog = ({ open, onClose, title, description, buttonText }) => (
  <Dialog open={open} onClose={onClose}>
    <SuccessMessage onClick={onClose} buttonText={buttonText}>
      <Message>
        <p className="title">{title}</p>
        <p>{description}</p>
      </Message>
    </SuccessMessage>
  </Dialog>
);

export default SuccessDialog;
