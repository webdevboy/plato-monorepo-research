import { OutlinedInput, Select } from '@material-ui/core';

import React from 'react';
import styled from 'styled-components';

const StyledSelect = styled(Select)`
  && {
    color: ${({ theme }) => theme.palette.black};
    font-size: 1.2rem;
    background: ${({ theme }) => theme.palette.white};
  }

  && fieldset {
    border-color: ${({ theme }) => theme.palette.geyser};
  }
`;

export default styled(({ children, label, ...others }) => (
  <>
    {label && <h4 className="mb2">{label}</h4>}
    <StyledSelect input={<OutlinedInput labelWidth={0} />} {...others}>
      {children}
    </StyledSelect>
  </>
))``;
