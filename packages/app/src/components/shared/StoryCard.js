import React, { Component } from 'react';
import styled, { withTheme } from 'styled-components';

import Card from 'components/shared/Card';
import IconLabel from 'components/shared/IconLabel';
import { Link } from 'react-router-dom';
import PersonInfo from 'components/shared/PersonInfo';
import PropTypes from 'prop-types';
import TagList from 'components/shared/TagList';
import Truncate from 'components/shared/Truncate';
import getProfilePictureThumbnail from 'utils/thumbnail';

const StyledCard = styled(Card)`
  transition: all ${props => props.theme.transitions.default};

  &:hover {
    border: 1px solid ${props => props.theme.palette.cerulean};
    box-shadow: ${props => props.theme.shadows[2]};
  }
`;

const TopSection = styled(Link)`
  border-bottom: 1px solid ${props => props.theme.palette.geyser};
  padding: 3rem 3rem 2rem 3rem;
  display: flex;
  flex-direction: column;
  cursor: pointer;

  h1 {
    margin-bottom: 1rem;
    margin-top: 0;
  }

  p {
    position: relative;
    margin-bottom: 2rem;
  }

  footer {
    flex-direction: row;
    display: flex;
    justify-content: space-between;

    svg {
      color: ${({ theme }) => theme.palette.java};
    }
  }
`;

const BottomSection = styled.div`
  padding: 2rem 3rem;
`;

class StoryCard extends Component {
  render() {
    const {
      className,
      isMentorInfoShown,
      story: { id, name, teaser, tagNames, mentor },
      theme,
    } = this.props;

    return (
      <StyledCard theme={theme} className={className}>
        <TopSection to={`/stories/${id}?source=stories`}>
          <h1>{name}</h1>
          {teaser && (
            <p>
              <Truncate maxLength={176}>{teaser}</Truncate>
            </p>
          )}
          <footer>
            <TagList tags={tagNames.split(', ')} />
            <IconLabel icon="arrow-right" />
          </footer>
        </TopSection>
        {isMentorInfoShown && (
          <BottomSection>
            <PersonInfo
              fullName={mentor.fullName}
              title={mentor.title}
              thumbnailUrl={getProfilePictureThumbnail(
                mentor.firstName,
                mentor.picture.thumbnailUrl
              )}
              companyName={mentor.companyName}
              linkUrl={`/mentors?mentor_id=${mentor.id}`}
            />
          </BottomSection>
        )}
      </StyledCard>
    );
  }
}

StoryCard.propTypes = {
  className: PropTypes.string,
  story: PropTypes.object.isRequired,
  isMentorInfoShown: PropTypes.bool,
  theme: PropTypes.object,
};

StoryCard.defaultProps = {
  className: null,
  isMentorInfoShown: false,
  theme: null,
};

export default withTheme(StoryCard);
