import { Dialog } from '@material-ui/core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';
import styled from 'styled-components';
import LoadingSpinner from './LoadingSpinner';
import PlatoLinearProgress from './PlatoLinearProgress';

/**
 * Helper to get the Material UI dialog in paper mode to use with styled-components.
 * @param {Object} props An object of props.
 * @return {Component} The Material UI mounted component Dialog.
 */
function getDialog(props) {
  return <Dialog {...props} classes={{ paper: 'paper' }} />;
}

const StyledDialog = styled(getDialog)`
  & .paper {
    border-radius: 0.2rem;
    max-width: initial;
  }

  & ${PlatoLinearProgress} {
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    z-index: 1;
  }

  & .loading-spinner-container {
    position: absolute;
    left: 50%;
    top: 50%;
    transform: translate(-50%, -50%);
  }
`;

const CloseIcon = styled(FontAwesomeIcon)`
  position: absolute;
  top: 1.5rem;
  right: 1.5rem;
  color: ${props => props.theme.palette.cadetBlue};
  cursor: pointer;
  font-size: 1.5rem;
  z-index: 2;

  &:hover {
    color: ${props => props.theme.palette.osloGrey};
  }
`;

const PlatoDialog = props => {
  const { open, onClose, saving, loading, children, ...other } = props;

  return (
    <StyledDialog {...other} open={open} onClose={onClose}>
      {saving && <PlatoLinearProgress variant="indeterminate" />}
      {loading && (
        <span className="loading-spinner-container">
          <LoadingSpinner />
        </span>
      )}
      <CloseIcon onClick={onClose} icon="times" />
      {children}
    </StyledDialog>
  );
};

export default PlatoDialog;
