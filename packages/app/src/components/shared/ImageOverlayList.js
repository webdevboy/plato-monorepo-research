import styled from 'styled-components';

export default styled.div`
  display: flex;
  flex-wrap: wrap;

  & > *:not(:first-child) {
    margin-left: -1.5rem;
  }
`;
