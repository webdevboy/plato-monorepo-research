import { LinearProgress } from '@material-ui/core';
/* https://material-ui.com/guides/interoperability/#deeper-elements */
import React from 'react';
import styled from 'styled-components';

const PlatoLinearProgress = styled(({ ...other }) => (
  <LinearProgress
    {...other}
    classes={{
      root: 'root',
      bar: 'bar',
    }}
  />
))`
  &.root {
    position: fixed;
    width: 100%;
    background: transparent;
    z-index: 1400;
    top: 0;
    left: 0;
  }

  & .bar {
    background: ${props => props.theme.palette.cerulean};
  }
`;

export default PlatoLinearProgress;
