import Button from './Button';
import { CALENDLY_STORY_GATHERING_URL } from 'constants/Urls';
import PlatoDialog from './PlatoDialog';
import React from 'react';
import styled from 'styled-components';
import { trackStoryWritten } from 'utils/analytics';
import { withUser } from 'components/context/UserContext';

const Body = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 3rem;
  padding-top: 6rem;
  text-align: center;
  width: 54rem;

  & h1 {
    margin-bottom: 2rem;
  }

  & p {
    margin-bottom: 2rem;
  }

  & a {
    color: ${props => props.theme.palette.cerulean};
  }
`;

const PostStoryDialog = props => (
  <PlatoDialog open={props.open} onClose={props.onClose}>
    <Body>
      <h1>Share a New Story</h1>
      <p>
        By sharing a new story through a story gathering call, you will allow us to be aware of
        additional areas of expertise you have in Engineering Management. This will be a great way
        for you to be connected with mentees regarding a wider variety of challenges and topics!
      </p>
      <p>
        Want to increase your visibility and matching probability by sharing a story, and don&apos;t
        want to go through a story gathering call? Just send over your story to{' '}
        <a href="mailto:nina@platohq.com">nina@platohq.com</a>, Problem/Actions taken/Lessons
        learned structure, examples on our{' '}
        <a href="https://community.platohq.com" target="_blank" rel="noopener noreferrer">
          stories website
        </a>
        .
      </p>
      <a
        href={CALENDLY_STORY_GATHERING_URL}
        target="_blank"
        rel="noopener noreferrer"
        onClick={() =>
          trackStoryWritten({
            email: props.userContext.user.email,
            location: props.location,
            mentorId: props.userContext.user.mentorId,
            uuid: props.userContext.user.uuid,
          })
        }
      >
        <Button variant="primary">Book a call with us</Button>
      </a>
    </Body>
  </PlatoDialog>
);

export default withUser(PostStoryDialog);
