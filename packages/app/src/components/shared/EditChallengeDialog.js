import React, { Component } from 'react';
import styled from 'styled-components';
import PlatoDialog from './PlatoDialog';
import Button from './Button';
import { withUser } from 'components/context/UserContext';
import { withQueryLoader } from 'components/context/QueryLoaderContext';

// TODO:  Create a dialog body component
const Body = styled.div`
  ${({ theme }) => `
    width: 54rem;
    padding: 3rem;

    & .title {
      margin-bottom: 3rem;
    }

    h4 {
      margin-bottom: 1rem;
    }

    & .form-input {
      border: 1px solid ${theme.palette.geyser};
      transition: all ${theme.transitions.default};
      width: 100%;
      font-size: 1.4rem;
      padding: 0.4rem;
      margin-bottom: 2rem;

      &:focus {
        border: 1px solid ${theme.palette.cerulean};
        box-shadow: ${theme.shadows[0]};
      }
    }

    & input[type='text'] {
      height: 5rem;
    }

    & textarea {
      height: 10rem;
    }

    & .actions {
      display: flex;
      justify-content: flex-end;
      align-items: center;

      & .save {
        margin-left: 2rem;
      }
    }
  `};
`;

class EditChallengeDialog extends Component {
  constructor(props) {
    super(props);

    this.state = {
      oneLiner: props.oneLiner,
      description: props.description,
    };
  }

  onChange = event => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  render() {
    const { open, onSave, onClose, saving } = this.props;

    const { oneLiner, description } = this.state;

    return (
      <PlatoDialog open={open} onClose={onClose}>
        <Body>
          <h1 className="title">Edit Challenge</h1>

          <h4>Describe your challenge in one line</h4>
          <input
            className="form-input"
            type="text"
            name="oneLiner"
            value={oneLiner}
            onChange={this.onChange}
          />

          <h4>Challenge description</h4>
          <textarea
            className="form-input"
            name="description"
            value={description}
            onChange={this.onChange}
          />

          <div className="actions">
            <Button variant="secondary-link" onClick={onClose}>
              Cancel
            </Button>
            <Button
              className="save"
              variant="primary"
              onClick={() => onSave(oneLiner, description)}
              disabled={saving || oneLiner.length === 0}
            >
              Save
            </Button>
          </div>
        </Body>
      </PlatoDialog>
    );
  }
}

export default withUser(withQueryLoader(EditChallengeDialog));
