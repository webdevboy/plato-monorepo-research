import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import styled from 'styled-components';

const StyledIcon = styled(FontAwesomeIcon)`
  color: ${props => props.theme.palette.cerulean};
  animation: fa-spin 2s infinite linear;
  font-size: 8rem;
  z-index: 2;
`;

const LoadingSpinner = props => <StyledIcon {...props} icon="circle-notch" />;

export default styled(LoadingSpinner)``;
