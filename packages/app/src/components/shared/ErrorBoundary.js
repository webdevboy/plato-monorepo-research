import React, { Component } from 'react';
import { captureSentryException } from 'utils/sentry';

import AppCrashed from 'components/appState/AppCrashed';

class ErrorBoundary extends Component {
  state = { hasError: false };

  componentDidCatch(error, errorInfo) {
    captureSentryException(error, errorInfo);
    this.setState({ hasError: true });
  }

  render() {
    const { children } = this.props;
    const { hasError } = this.state;
    return hasError ? <AppCrashed /> : children;
  }
}

export default ErrorBoundary;
