import { Snackbar, SnackbarContent } from '@material-ui/core';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';
import styled from 'styled-components';

const variantIcons = {
  success: 'check',
  info: 'info-circle',
  warn: 'exclamation-circle',
  error: 'exclamation-triangle',
};

const CloseButton = styled.button`
  color: inherit;
  background: transparent;
  border: none;
  cursor: pointer;
  font-size: var(--x3);
`;

const StyledSnackbarContent = styled(({ variant, ...other }) => <SnackbarContent {...other} />)`
  &.root {
    flex-wrap: nowrap;
    border-radius: 0.2rem;
    background: green;
    min-width: 32rem;
    max-width: 40rem;
    min-height: 4.8rem;
    ${({ theme: { palette }, variant }) => `
      ${variant === 'success' ? `background: ${palette.success};` : ''}
      ${variant === 'info' ? `background: ${palette.java};` : ''}
      ${
        variant === 'warn'
          ? `
            background: ${palette.warn};
            color: ${palette.black};
          `
          : ''
      }
      ${variant === 'error' ? `background: ${palette.error};` : ''}
    }
  `}

    & .message {
      display: flex;
      align-items: center;
      font-size: 1.8rem;
      font-family: 'Source Sans Pro', 'Helvetica Neue', 'Segoe UI', 'Helvetica', 'Arial',
        'sans-serif';

      & p {
        font-size: 1.4rem;
        margin-left: 1.4rem;
      }
    }
  }
`;

export default ({ open, onClose, onExited, message, variant }) => (
  <Snackbar
    anchorOrigin={{
      vertical: 'top',
      horizontal: 'right',
    }}
    open={open}
    autoHideDuration={6000}
    onExited={onExited}
    onClose={onClose}
  >
    <StyledSnackbarContent
      classes={{ root: 'root' }}
      message={
        <span className="message">
          <FontAwesomeIcon icon={variantIcons[variant]} />
          <p>{message}</p>
        </span>
      }
      action={[
        <CloseButton key="close" onClick={onClose}>
          <FontAwesomeIcon icon="times" />
        </CloseButton>,
      ]}
      variant={variant}
    />
  </Snackbar>
);
