import PropTypes from 'prop-types';
import React from 'react';
import styled from 'styled-components';

const StyledWrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  padding: 0 var(--x3);

  .left-side {
    margin-bottom: var(--x4);
  }

  @media (min-width: ${props => props.breakpoint || 0}px) {
    flex-direction: row;
    padding: 0;

    .left-side {
      width: ${({ leftColumns, rightColumns }) =>
        (leftColumns * 100) / (leftColumns + rightColumns)}%;
      margin-right: var(--x2);
    }

    .right-side {
      width: ${({ leftColumns, rightColumns }) =>
        (rightColumns * 100) / (leftColumns + rightColumns)}%;
      margin-left: var(--x2);
    }
  }
`;

const HorizontalSplit = ({ leftSide, rightSide, leftColumns, rightColumns, breakpoint }) => (
  <StyledWrapper leftColumns={leftColumns} rightColumns={rightColumns} breakpoint={breakpoint}>
    <div className="left-side full-width">{leftSide}</div>
    <div className="right-side full-width">{rightSide}</div>
  </StyledWrapper>
);

HorizontalSplit.propTypes = {
  leftSide: PropTypes.element.isRequired,
  rightSide: PropTypes.element.isRequired,
  leftColumns: PropTypes.number,
  rightColumns: PropTypes.number,
};

HorizontalSplit.defaultProps = {
  leftColumns: 1,
  rightColumns: 1,
};

export default HorizontalSplit;
