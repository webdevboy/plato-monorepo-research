import React, { Component } from 'react';

import Checkbox from './Checkbox';
import FilterList from './FilterList';

class CheckboxFilterList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      options: [],
    };
  }

  componentDidMount() {
    this.initializeState();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.options !== this.props.options) {
      this.initializeState();
    }
  }

  handleChange = id => {
    this.setState(
      prevState => {
        const index = prevState.options.findIndex(opt => opt.id === id);
        const option = prevState.options[index];

        return {
          options: [
            ...prevState.options.slice(0, index),
            {
              ...option,
              checked: !option.checked,
            },
            ...prevState.options.slice(index + 1),
          ],
        };
      },
      () => this.props.onChange(id, this.state.options.find(opt => opt.id === id).checked)
    );
  };

  initializeState() {
    this.setState(prevState => ({
      options: this.props.options.map(option => {
        const existingOption = prevState.options.find(opt => opt.id === option.id);

        return {
          id: option.id,
          name: option.name,
          checked: (existingOption && existingOption.checked) || !!option.checked,
        };
      }),
    }));
  }

  render() {
    const { title, loading } = this.props;
    const { options } = this.state;

    return (
      <FilterList title={title} loading={loading}>
        {Array.isArray(options) &&
          options.map(option => (
            <Checkbox
              key={option.id}
              color="primary"
              label={option.name}
              checked={option.checked}
              onChange={() => this.handleChange(option.id)}
            />
          ))}
      </FilterList>
    );
  }
}

export default CheckboxFilterList;
