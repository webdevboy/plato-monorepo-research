import React, { Component } from 'react';

import Avatar from './Avatar';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import TagList from './TagList';
import getProfilePictureThumbnail from 'utils/thumbnail';
import styled from 'styled-components';

const Container = styled.div`
  width: 100%;
  position: relative;

  .row {
    display: flex;
    align-items: center;

    ${Avatar} {
      margin-right: 1rem;
    }

    a.profile-link {
      transition: ${({ theme }) => theme.transitions.default};

      &:hover {
        color: ${({ theme }) => theme.palette.cerulean};
      }
    }

    .linkedin-logo {
      position: absolute;
      top: 5px;
      right: 5px;
    }
  }

  .about,
  ${TagList} {
    margin-top: 2rem;
  }
`;

class PersonInfo extends Component {
  renderName() {
    const { fullName, linkUrl, size } = this.props;
    const nameComponents = {
      xs: 'sub',
      sm: 'p',
      md: 'h2',
      lg: 'h1',
    };
    const NameComponent = nameComponents[size];
    return (
      <NameComponent>
        {linkUrl ? (
          <Link className="profile-link" to={linkUrl}>
            {fullName}
          </Link>
        ) : (
          fullName
        )}
      </NameComponent>
    );
  }

  render() {
    const {
      about,
      className,
      companyName,
      fullName,
      linkedin,
      size,
      tags,
      thumbnailUrl,
      title,
    } = this.props;
    return (
      <Container className={className}>
        <div className="row">
          <Avatar
            src={getProfilePictureThumbnail(fullName, thumbnailUrl)}
            alt="Mentor avatar"
            size={size}
          />
          <div>
            {this.renderName()}
            {linkedin && (
              <a
                className="linkedin-logo"
                href={linkedin}
                target="_blank"
                rel="noopener noreferrer"
              >
                <FontAwesomeIcon icon={['fab', 'linkedin']} size="2x" color="#0073b1" />
              </a>
            )}
            <sub>
              {title} at <b>{companyName}</b>
            </sub>
          </div>
        </div>
        {tags && <TagList tags={tags} />}
        {about && (
          <div className="about">
            {about.split('\n').map((paragraph, index) => (
              <sub key={index}>{paragraph}</sub>
            ))}
          </div>
        )}
      </Container>
    );
  }
}

PersonInfo.propTypes = {
  about: PropTypes.string,
  className: PropTypes.string,
  companyName: PropTypes.string,
  fullName: PropTypes.string.isRequired,
  linkedin: PropTypes.string,
  linkUrl: PropTypes.string,
  size: PropTypes.oneOf(['sm', 'md', 'lg', 'xs']),
  tags: PropTypes.array,
  thumbnailUrl: PropTypes.string,
  title: PropTypes.string,
};

PersonInfo.defaultProps = {
  about: null,
  className: null,
  linkedin: null,
  linkUrl: null,
  size: 'sm',
  tags: null,
  thumbnailUrl: null,
  companyName: '',
  title: '',
};

export default styled(PersonInfo)``;
