import React, { Component } from 'react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import styled from 'styled-components';

const StyledCheckbox = styled.div`
  margin-top: 4px;
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  min-width: 1.5rem;
  min-height: 1.5rem;
  max-width: 1.5rem;
  max-height: 1.5rem;
  border-radius: 0.2rem;
`;

const StyledIcon = styled(FontAwesomeIcon)`
  height: 1rem;
  font-weight: 400;
`;

const Container = styled.div`
  margin: 1.2rem 0;
  display: flex;
  align-items: center;
  line-height: 2.7rem;
  cursor: pointer;
  transition: all ${({ theme }) => theme.transitions.default};
  & > ${StyledCheckbox} {
    border: 1px solid
      ${({ theme, disabled }) => (disabled ? theme.palette.geyser : theme.palette.cadetBlue)};
  }
  &:hover,
  &.checked {
    color: ${({ theme, disabled }) => (disabled ? theme.palette.geyser : theme.palette.cerulean)};
    & > ${StyledCheckbox} {
      border: 1px solid
        ${({ theme, disabled }) => (disabled ? theme.palette.geyser : theme.palette.cerulean)};
    }
  }
  & ${StyledIcon} {
    color: ${({ theme, disabled }) => (disabled ? theme.palette.geyser : theme.palette.cerulean)};
  }
  &.checked {
    font-weight: 600;
  }
`;

const Label = styled.p`
  margin-left: 1rem;
  width: 100%;
`;

class Checkbox extends Component {
  shouldComponentUpdate(nextProps) {
    const { checked } = this.props;
    return checked !== nextProps.checked;
  }

  handleClick = event => {
    const { name, checked, disabled, onChange } = this.props;
    if (!disabled) {
      onChange(name, !checked, event);
    }
  };

  render() {
    const { className, checked, disabled } = this.props;

    const checkedClassName = checked ? 'checked' : '';

    return (
      <Container
        className={`${className} ${checkedClassName}`}
        onClick={this.handleClick}
        disabled={disabled}
      >
        <StyledCheckbox>{this.props.checked && <StyledIcon icon="check" />}</StyledCheckbox>
        <Label>{this.props.label}</Label>
      </Container>
    );
  }
}

export default styled(Checkbox)``;
