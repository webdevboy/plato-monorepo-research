import React from 'react';
import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Button from 'components/shared/Button';

const Container = styled.div`
  background: ${props => props.theme.palette.cerulean};
  color: white;

  & .booked {
    display: flex;
    align-items: center;
    min-height: 10.6rem;
    padding: 2.5rem;

    & .check {
      display: flex;
      justify-content: center;
      align-items: center;
      font-size: 2.7rem;
      min-width: 6.4rem;
      min-height: 6.4rem;
      max-width: 6.4rem;
      max-height: 6.4rem;
      background: ${props => props.theme.palette.java};
      border-radius: 10rem;
      margin-right: 1.5rem;
    }
  }

  & ${Button} {
    width: 100%;
    border: none;
    border-top: 1px solid rgba(255, 255, 255, 0.5);
    margin: 0;
    border-radius: 0;
    background: ${props => props.theme.palette.cerulean};
    transition: background ${props => props.theme.transitions.default};

    &:hover {
      background: ${props => props.theme.palette.blueLagoon};
    }
  }
`;

const SuccessMessage = props => (
  <Container className={props.className}>
    <div className="booked">
      <div className="check">
        <FontAwesomeIcon icon="check" />
      </div>

      {props.children}
    </div>
    <Button variant="primary" onClick={props.onClick}>
      {props.buttonText}
    </Button>
  </Container>
);

export default styled(SuccessMessage)``;
