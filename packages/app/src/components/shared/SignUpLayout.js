import React from 'react';
import banner from 'images/banner.png';
import lightPlato from 'images/light-plato.png';
import styled from 'styled-components';

const Grid = styled.div`
  display: grid;
  grid-template-columns: auto;
  grid-template-rows: 10rem 15rem 7rem auto 1fr auto;
  grid-template-areas:
    'header'
    'image'
    'title'
    'description'
    'content'
    'footer';
  justify-items: center;
  width: 100%;
  min-width: 0 !important;
`;

const Header = styled.header`
  width: 100%;
  grid-area: header;
  background-image: url(${banner});
  background-repeat: no-repeat;
  background-size: 100% 100%;
`;

const Logo = styled.img`
  height: 2.5rem;
  margin: 3rem;
`;

const Image = styled.img`
  grid-area: image;
  height: 80%;
  margin-top: 2rem;
`;

const Title = styled.h1`
  font-size: 3rem;
  color: ${props => props.theme.palette.black};
  grid-area: title;
`;

const Description = styled.div`
  font-size: 1.6rem;
  grid-area: description;
  text-align: center;
  margin-bottom: 2rem;
`;

const Content = styled.section`
  grid-area: content;
  width: 100%;
  padding: 0 1rem;

  @media only screen and (min-width: 1025px) {
    width: 60%;
    padding: 0;
  }
`;

const Footer = styled.section`
  width: 100%;
  padding: 2rem 0;
  margin-top: 5rem;
  background-color: ${props => props.theme.palette.black};
  color: white;
  font-size: 2rem;
  display: flex;
  text-align: center;
  align-items: center;
  justify-content: center;
`;

const SignUpLayout = ({ title, description, image, children }) => (
  <Grid>
    <Header>
      <Logo src={lightPlato} alt="Plato" />
    </Header>
    <Image src={image} alt="title" />
    <Title>{title}</Title>
    <Description>{description}</Description>
    <Content>{children}</Content>
    <Footer>
      <p>&copy;{new Date().getFullYear()} Plato, Inc. All rights reserved</p>
    </Footer>
  </Grid>
);

export default SignUpLayout;
