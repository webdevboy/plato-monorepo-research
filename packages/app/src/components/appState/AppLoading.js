import BrandedPage from 'components/shared/BrandedPage';
import React from 'react';
import styled from 'styled-components';

const Content = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  color: ${({ theme }) => theme.palette.white};

  & h1 {
    font-size: 4rem;
  }
`;

const AppLoading = () => (
  <BrandedPage>
    <Content>
      <h1>Loading...</h1>
    </Content>
  </BrandedPage>
);

export default AppLoading;
