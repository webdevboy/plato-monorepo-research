import BrandedPage from 'components/shared/BrandedPage';
import React from 'react';
import styled from 'styled-components';

const Content = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  color: ${({ theme }) => theme.palette.white};

  & h1 {
    font-size: 3.6rem;
    margin-bottom: 3.6rem;
  }
`;

const AppCrashed = () => (
  <BrandedPage>
    <Content>
      <h1>Gah, our app crashed! So sorry about that.</h1>
      <h2>Our team is looking into it, please try again later.</h2>
    </Content>
  </BrandedPage>
);

export default AppCrashed;
