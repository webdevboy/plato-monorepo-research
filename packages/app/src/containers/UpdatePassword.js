import { Link, withRouter } from 'react-router-dom';
import React, { Component } from 'react';

import BrandedPage from 'components/shared/BrandedPage';
import Button from 'components/shared/Button';
import Card from 'components/shared/Card';
import PropTypes from 'prop-types';
import TextInput from 'components/shared/TextInput';
import { connect } from 'react-redux';
import { isActionLoading } from 'selectors';
import styled from 'styled-components';
import { updatePassword } from 'ducks';

const Header = styled.h1`
  font-size: 3.4rem;
  color: ${props => props.theme.palette.white};
  margin-bottom: 3rem;
`;

const FormCard = styled(Card)`
  form {
    width: 62rem;
    padding: 5rem 8rem;
  }

  .actions {
    display: flex;
    justify-content: space-between;
    margin-top: 2rem;
    align-items: center;
  }
`;

const StyledDescription = styled.p`
  color: ${props => props.theme.palette.geyser};
  font-weight: bold;
  margin-top: 1rem;
`;

class UpdatePassword extends Component {
  state = {
    email: '',
  };

  handleEmailChange = event => {
    this.setState({
      email: event.target.value,
    });
  };

  handleSubmit = event => {
    const { onUpdate } = this.props;
    const { email } = this.state;
    event.preventDefault();
    onUpdate(email);
  };

  render() {
    const { description, isLoading, title } = this.props;
    const { email } = this.state;

    return (
      <BrandedPage>
        <Header>{title}</Header>
        <FormCard>
          <form onSubmit={this.handleSubmit}>
            <TextInput
              type="text"
              value={email}
              label="Your Email Address"
              onChange={this.handleEmailChange}
              placeholder="email@example.com"
            />
            <div className="actions">
              <Link to="/">
                <Button variant="primary-link" type="button">
                  Already a user?
                </Button>
              </Link>
              <Button variant="primary" type="submit" disabled={isLoading || email.length === 0}>
                Submit
              </Button>
            </div>
          </form>
        </FormCard>
        {description && <StyledDescription>{description}</StyledDescription>}
      </BrandedPage>
    );
  }
}

UpdatePassword.propTypes = {
  description: PropTypes.string,
  isLoading: PropTypes.bool.isRequired,
  onUpdate: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
};

UpdatePassword.defaultProps = {
  description: null,
};

function mapStateToProps(state) {
  return {
    isLoading: isActionLoading(state, updatePassword),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    onUpdate: email => dispatch(updatePassword({ email })),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(UpdatePassword));
