import React, { Component } from 'react';

import BrandedPage from 'components/shared/BrandedPage';
import Button from 'components/shared/Button';
import Card from 'components/shared/Card';
import TextInput from 'components/shared/TextInput';
import queryString from 'query-string';
import styled from 'styled-components';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { isActionLoading } from 'selectors';
import { resetPassword } from 'ducks';
import PropTypes from 'prop-types';

const Header = styled.h1`
  font-size: 3.4rem;
  color: ${props => props.theme.palette.white};
  margin-bottom: 3rem;
`;

const FormCard = styled(Card)`
  form {
    width: 62rem;
    padding: 5rem 8rem;
    display: grid;
    grid-template-columns: 50% 50%;

    ${TextInput} {
      grid-column: 1/3;
    }

    .error {
      color: ${props => props.theme.palette.error};
      align-self: center;
    }

    ${Button} {
      grid-column: 2/3;
      justify-self: flex-end;
    }
  }
`;

class ResetPassword extends Component {
  state = {
    password: '',
    passwordConfirmation: '',
  };

  handleChange = event => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleSubmit = event => {
    const { location, onReset } = this.props;
    const { password, passwordConfirmation } = this.state;
    const { token } = queryString.parse(location.search);
    event.preventDefault();
    onReset(password, passwordConfirmation, token);
  };

  getErrorMessage = () => {
    const { password, passwordConfirmation } = this.state;
    if (password.length === 0 || passwordConfirmation.length === 0) {
      return null;
    }
    if (password !== passwordConfirmation) {
      return 'Password must match';
    }
    return null;
  };

  validate() {
    const { isLoading } = this.props;
    const { password, passwordConfirmation } = this.state;
    return (
      !isLoading &&
      password.length !== 0 &&
      passwordConfirmation.length !== 0 &&
      password === passwordConfirmation
    );
  }

  render() {
    const { title = 'Reset your password' } = this.props;
    const { password, passwordConfirmation } = this.state;
    const errorMessage = this.getErrorMessage();
    return (
      <BrandedPage>
        <Header>{title}</Header>
        <FormCard>
          <form onSubmit={this.handleSubmit}>
            <TextInput
              type="password"
              name="password"
              label="Password"
              value={password}
              onChange={this.handleChange}
              placeholder="Your new password"
            />

            <TextInput
              type="password"
              name="passwordConfirmation"
              value={passwordConfirmation}
              onChange={this.handleChange}
              placeholder="Confirm your password"
            />

            {errorMessage && <sub className="error">{errorMessage}</sub>}

            <Button variant="primary" type="submit" disabled={!this.validate()}>
              Submit
            </Button>
          </form>
        </FormCard>
      </BrandedPage>
    );
  }
}

ResetPassword.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  onReset: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
  return {
    isLoading: isActionLoading(state, resetPassword),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    onReset: (password, passwordConfirmation, token) =>
      dispatch(resetPassword({ password, passwordConfirmation, token })),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(ResetPassword));
