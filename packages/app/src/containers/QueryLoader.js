import React, { Component } from 'react';

import PlatoLinearProgress from 'components/shared/PlatoLinearProgress';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { isQueryLoaderVisible } from 'selectors';

class QueryLoader extends Component {
  render() {
    const { children, isVisible } = this.props;
    return (
      <>
        {isVisible && <PlatoLinearProgress variant="indeterminate" />}
        {children}
      </>
    );
  }
}

QueryLoader.propTypes = {
  isVisible: PropTypes.bool,
  children: PropTypes.node,
};

QueryLoader.defaultProps = {
  isVisible: false,
  children: null,
};

function mapStateToProps(state) {
  return {
    isVisible: isQueryLoaderVisible(state),
  };
}

export default connect(mapStateToProps)(QueryLoader);
