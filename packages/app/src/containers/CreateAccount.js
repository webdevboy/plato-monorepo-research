import React, { Component } from 'react';

import Button from 'components/shared/Button';
import PropTypes from 'prop-types';
import TextInput from 'components/shared/TextInput';
import blueTriangle from 'images/blue-triangle.svg';
import { connect } from 'react-redux';
import darkPlato from 'images/dark-plato.png';
import { isActionLoading } from 'selectors';
import queryString from 'query-string';
import { resetPassword } from 'ducks';
import styled from 'styled-components';
import { withRouter } from 'react-router-dom';

const StyledCreateAccount = styled.div`
  align-items: center;
  display: flex;
  flex-direction: column;
  min-height: 100vh;

  background-image: url(${blueTriangle});
  background-position: right;
  background-repeat: no-repeat;
  background-attachment: fixed;

  .logo {
    height: 4rem;
    margin-top: 2rem;
    margin-bottom: 6rem;
  }

  h1 {
    margin-bottom: 1rem;
    text-align: center;
  }

  .description {
    text-align: center;
    margin-bottom: 6rem;
  }

  .button-container,
  ${TextInput} {
    width: 360px;
  }

  .button-container {
    display: flex;
    justify-content: flex-end;
  }
`;

class CreateAccount extends Component {
  constructor(props) {
    super(props);
    const {
      location: { search },
    } = props;
    const { token: resetPasswordToken } = queryString.parse(search);
    this.state = {
      password: '',
      passwordConfirmation: '',
      resetPasswordToken,
    };
  }

  handleSave = () => {
    const { onReset } = this.props;
    const { password, passwordConfirmation, resetPasswordToken } = this.state;
    onReset(password, passwordConfirmation, resetPasswordToken);
  };

  handleChange = event => {
    const {
      target: { name, value },
    } = event;
    this.setState({ [name]: value });
  };

  validate() {
    const { isLoading } = this.props;
    const { password, passwordConfirmation } = this.state;
    return !isLoading && password.length > 0 && passwordConfirmation.length > 0;
  }

  render() {
    const { password, passwordConfirmation } = this.state;
    return (
      <StyledCreateAccount>
        <img className="logo" src={darkPlato} alt="Plato" />
        <h1>Welcome to Plato!</h1>

        <p className="description">
          First thing first, let&apos;s create you an account.
          <br />
          We already got your email.
        </p>

        <TextInput
          type="password"
          name="password"
          label="Create your password"
          placeholder="Please use a strong password"
          value={password}
          onChange={this.handleChange}
        />

        <TextInput
          type="password"
          name="passwordConfirmation"
          label="Confirm your password"
          placeholder="Retype your password"
          value={passwordConfirmation}
          onChange={this.handleChange}
        />

        <div className="button-container">
          <Button disabled={!this.validate()} variant="primary" onClick={this.handleSave}>
            Continue
          </Button>
        </div>
      </StyledCreateAccount>
    );
  }
}

CreateAccount.propTypes = {
  location: PropTypes.object.isRequired,
  isLoading: PropTypes.bool.isRequired,
  onReset: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
  return {
    isLoading: isActionLoading(state, resetPassword),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    onReset: (password, passwordConfirmation, token) =>
      dispatch(resetPassword({ password, passwordConfirmation, token })),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(CreateAccount));
