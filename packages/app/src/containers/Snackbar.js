import React, { Component } from 'react';

import PropTypes from 'prop-types';
// eslint-disable-next-line import/no-named-default
import { default as SnackbarComponent } from 'components/shared/Snackbar';
import { connect } from 'react-redux';
import { getSnackbar } from 'selectors';
import { hideSnackbar } from 'ducks';

class Snackbar extends Component {
  render() {
    const { onHide, children, snackbar } = this.props;
    return (
      <>
        <SnackbarComponent
          open={Boolean(snackbar && snackbar.isVisible)}
          onClose={onHide}
          onExited={onHide}
          message={snackbar && snackbar.message}
          variant={snackbar && snackbar.variant}
        />
        {children}
      </>
    );
  }
}

Snackbar.propTypes = {
  children: PropTypes.node,
  snackbar: PropTypes.object,
  onHide: PropTypes.func.isRequired,
};

Snackbar.defaultProps = {
  children: null,
  snackbar: null,
};

function mapStateToProps(state) {
  return {
    snackbar: getSnackbar(state),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    onHide: () => dispatch(hideSnackbar()),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Snackbar);
