import React, { Component } from 'react';
import { fetchProducts, setSignUpData } from 'ducks';
import { getProducts, getSignUpData } from 'selectors';

import Button from 'components/shared/Button';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import PlanImage from 'images/plan.svg';
import Pluralize from 'react-pluralize';
import SignUpLayout from 'components/shared/SignUpLayout';
import { connect } from 'react-redux';
import { getPlanPriceByInterval } from 'utils/stripe';
import styled from 'styled-components';

const PlanInfo = styled.sub`
  text-align: right;
  color: ${props => props.theme.palette.osloGrey};

  b {
    color: black;
    font-size: 1.6rem;
  }
`;

const FormWrapper = styled.form`
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-column-gap: 2rem;
  grid-row-gap: 1rem;
  justify-content: center;

  @media only screen and (min-width: 1025px) {
    grid-template-columns: minmax(1fr, 30rem) minmax(1fr, 30rem);
  }

  h4 {
    margin-top: 2rem;
  }

  label {
    border-radius: 4px;
    border: 1px solid ${props => props.theme.palette.geyser};
    padding: 2rem;

    input {
      margin-right: 2rem;
    }

    &.checked {
      color: ${props => props.theme.palette.java};
      border-color: ${props => props.theme.palette.java};
    }

    .product-description {
      display: flex;
      align-items: center;
      color: ${props => props.theme.palette.osloGrey};
      padding: 1rem 0;

      svg {
        color: ${props => props.theme.palette.java};
        margin-right: 2rem;
      }
    }
  }

  .plan-input {
    display: flex;
    grid-column: 1/3;
    align-items: center;
    justify-content: space-between;
    padding: 1rem 2rem;
  }

  .continue-button {
    justify-self: right;
  }

  .continue-button,
  .previous-button {
    margin-top: 2rem;
  }
`;

class PaymentPlan extends Component {
  state = {
    product: this.props.signUp.product || null,
    plan: this.props.signUp.plan || null,
  };

  componentDidMount() {
    const { onFetchProducts } = this.props;
    onFetchProducts();
  }

  handlePrevious = event => {
    const { history } = this.props;
    event.preventDefault();
    history.goBack();
  };

  handleSelectProduct = event => {
    const { products } = this.props;
    const product = products[event.target.value];
    this.setState({ product });
  };

  handleSelectPlan = event => {
    const { product } = this.state;
    const plan = product.plans[event.target.value];
    this.setState({ plan });
  };

  handleSubmitForm = event => {
    const { plan, product } = this.state;
    const { onSetSignUpData, history } = this.props;
    event.preventDefault();
    onSetSignUpData({ plan, product });
    history.push('/sign-up/payment');
  };

  render() {
    const { plan, product } = this.state;
    const { products } = this.props;

    if (!products || products.length === 0) {
      return null;
    }

    return (
      <SignUpLayout title="Choose your plan" image={PlanImage}>
        <FormWrapper onSubmit={this.handleSubmitForm}>
          {products.map((productItem, index) => (
            <label
              key={productItem.id}
              className={product && product.id === productItem.id ? 'checked' : ''}
            >
              <input
                name="product"
                type="radio"
                value={index}
                checked={product && product.id === productItem.id}
                onChange={this.handleSelectProduct}
              />
              <br />
              <br />
              <h2>{productItem.name}</h2>
              ___
              <br />
              {productItem.metadata.description.split('|').map(label => (
                <sub key={label} className="product-description">
                  <FontAwesomeIcon icon="check" />
                  {label}
                </sub>
              ))}
            </label>
          ))}

          {product && <h4>Engagement</h4>}

          {product &&
            product.plans.map((planItem, index) => (
              <label
                key={planItem.id}
                className={`plan-input ${plan && plan.id === planItem.id && 'checked'}`}
              >
                <span>
                  <input
                    name="plan"
                    type="radio"
                    value={index}
                    checked={plan && plan.id === planItem.id}
                    onChange={this.handleSelectPlan}
                  />
                  <Pluralize singular={planItem.interval} count={planItem.interval_count} />
                </span>
                <PlanInfo>
                  <b>${getPlanPriceByInterval(planItem)}</b>
                  <div>per {planItem.interval}</div>
                </PlanInfo>
              </label>
            ))}

          <Button className="previous-button" onClick={this.handlePrevious}>
            Previous
          </Button>

          <Button
            variant="primary"
            type="submit"
            className="continue-button"
            disabled={!product || !plan}
          >
            Continue
          </Button>
        </FormWrapper>
      </SignUpLayout>
    );
  }
}

function mapStateToProps(state) {
  return {
    products: getProducts(state),
    signUp: getSignUpData(state),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    onFetchProducts: () => dispatch(fetchProducts()),
    onSetSignUpData: data => dispatch(setSignUpData(data)),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PaymentPlan);
