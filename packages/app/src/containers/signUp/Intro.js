import React, { Component } from 'react';
import { getFaceTags, getSignUpData } from 'selectors';

import Button from 'components/shared/Button';
import { Formik } from 'formik';
import IntroImage from 'images/intro.svg';
import SignUpLayout from 'components/shared/SignUpLayout';
import TextInput from 'components/shared/TextInput';
import { connect } from 'react-redux';
import { setSignUpData } from 'ducks';
import styled from 'styled-components';

const RadioWrapper = styled.div`
  display: flex;
  padding: 2rem 0 4rem 0;
`;

const StyledRadioInput = styled.label`
  width: 100%;
  display: flex;
  align-items: center;
  cursor: pointer;

  input {
    margin-right: 1rem;
  }
`;

const INTENT_TEAM = 'For a team';
const INTENT_SELF = 'For myself';

class Intro extends Component {
  handleSubmit = values => {
    const { intent, firstName, lastName, companyName, email, faceId } = values;
    const { history, onSetSignUpData } = this.props;
    onSetSignUpData({
      firstName,
      lastName,
      companyName,
      email,
      faceId,
    });
    if (intent === INTENT_TEAM) {
      history.push('/sign-up/check-in');
    } else {
      history.push('/sign-up/plans');
    }
  };

  handleValidate = values => {
    const errors = {};

    if (!values.email) {
      errors.email = 'Required';
    } else if (!values.email.includes('@')) {
      errors.email = 'Invalid email address';
    }

    if (!values.firstName) {
      errors.firstName = 'Required';
    }

    if (!values.firstName) {
      errors.firstName = 'Required';
    }

    if (!values.lastName) {
      errors.lastName = 'Required';
    }

    if (!values.companyName) {
      errors.companyName = 'Required';
    }

    if (values.intent !== INTENT_TEAM && !values.faceId) {
      errors.faceId = 'Required';
    }

    return errors;
  };

  handleRenderForm = formikProps => {
    const { faceTags } = this.props;
    const { values, isValid, isSubmitting, handleSubmit, handleChange } = formikProps;
    return (
      <form onSubmit={handleSubmit}>
        <h4 className="label">What best describes your interest in Plato?</h4>
        <RadioWrapper>
          <StyledRadioInput>
            <input name="intent" type="radio" value={INTENT_TEAM} onChange={handleChange} />
            {INTENT_TEAM}
          </StyledRadioInput>

          <StyledRadioInput>
            <input name="intent" type="radio" value={INTENT_SELF} onChange={handleChange} />
            {INTENT_SELF}
          </StyledRadioInput>
        </RadioWrapper>

        <TextInput label="First Name" type="text" name="firstName" onChange={handleChange} />

        <TextInput label="Last Name" type="text" name="lastName" onChange={handleChange} />

        <TextInput label="Company Name" type="text" name="companyName" onChange={handleChange} />

        {values.intent === INTENT_SELF && <h4 className="label">What is your role?</h4>}
        {values.intent === INTENT_SELF && faceTags.length > 0 && (
          <RadioWrapper>
            {faceTags.map(face => (
              <StyledRadioInput key={face.id}>
                <input name="faceId" type="radio" value={face.id} onChange={handleChange} />
                {face.name}
              </StyledRadioInput>
            ))}
          </RadioWrapper>
        )}

        <TextInput label="Business Email" type="text" name="email" onChange={handleChange} />

        <Button variant="primary" type="submit" disabled={!isValid || isSubmitting}>
          Continue
        </Button>
      </form>
    );
  };

  render() {
    const {
      signUp: { intent, firstName, lastName, companyName, faceId, email },
    } = this.props;
    return (
      <SignUpLayout
        title="My Business Information"
        description="Tell us more about yourself and your company"
        image={IntroImage}
      >
        <section>
          <Formik
            initialValues={{
              companyName,
              email,
              faceId,
              firstName,
              intent,
              lastName,
            }}
            validate={this.handleValidate}
            onSubmit={this.handleSubmit}
          >
            {this.handleRenderForm}
          </Formik>
        </section>
      </SignUpLayout>
    );
  }
}

function mapStateToProps(state) {
  return {
    faceTags: getFaceTags(state),
    signUp: getSignUpData(state),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    onSetSignUpData: data => dispatch(setSignUpData(data)),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Intro);
