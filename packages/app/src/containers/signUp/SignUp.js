import React, { Component } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

import CheckIn from './CheckIn';
import Intro from './Intro';
import Payment from './Payment';
import Plans from './Plans';
import PropTypes from 'prop-types';
import { STRIPE_API_KEY } from 'constants/Stripe';
import Slack from './Slack';
import { StripeProvider } from 'react-stripe-elements';
import scriptLoader from 'react-async-script-loader';

class SignUp extends Component {
  render() {
    const {
      isScriptLoaded,
      match: { path },
    } = this.props;

    if (!isScriptLoaded) {
      return null;
    }

    return (
      <StripeProvider apiKey={STRIPE_API_KEY}>
        <Switch>
          <Route exact path={`${path}/`} component={Intro} />
          <Route exact path={`${path}/check-in`} component={CheckIn} />
          <Route exact path={`${path}/payment`} component={Payment} />
          <Route exact path={`${path}/plans`} component={Plans} />
          <Route exact path={`${path}/slack`} component={Slack} />
          <Redirect to={`${path}/`} />
        </Switch>
      </StripeProvider>
    );
  }
}

SignUp.propTypes = {
  match: PropTypes.object.isRequired,
  isScriptLoaded: PropTypes.bool.isRequired,
};

export default scriptLoader('https://js.stripe.com/v3/')(SignUp);
