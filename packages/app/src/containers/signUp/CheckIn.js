import React, { Component } from 'react';

import BookImage from 'images/book.svg';
import Button from 'components/shared/Button';
import PropTypes from 'prop-types';
import { SALES_URL } from 'constants/Urls';
import SignUpLayout from 'components/shared/SignUpLayout';
import { connect } from 'react-redux';
import { getSignUpData } from 'selectors';
import queryString from 'query-string';
import styled from 'styled-components';

const FormWrapper = styled.div`
  iframe {
    width: 100%;
    height: 90vh;
    border: 1px solid ${({ theme }) => theme.palette.geyser};
    margin-bottom: 2rem;
  }

  .calendly-inline-widget {
    width: 100%;
    height: 50rem;
    border: 1px solid ${({ theme }) => theme.palette.geyser};
    margin-bottom: 2rem;
  }
`;

class CheckIn extends Component {
  handlePrevious = () => {
    const { history } = this.props;
    history.goBack();
  };

  render() {
    const {
      signUp: { firstName, lastName, email },
    } = this.props;
    const queryParams = queryString.stringify({
      email,
      firstName,
      lastName,
    });
    return (
      <SignUpLayout
        title="Pick a time"
        description="Find a time to talk with one of our team members"
        image={BookImage}
      >
        <FormWrapper>
          <iframe id="tracks" title="tracks" src={`${SALES_URL}?${queryParams}`} />
          <Button onClick={this.handlePrevious}>Previous</Button>
        </FormWrapper>
      </SignUpLayout>
    );
  }
}

CheckIn.propTypes = {
  history: PropTypes.object.isRequired,
  signUp: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  return {
    signUp: getSignUpData(state),
  };
}

export default connect(mapStateToProps)(CheckIn);
