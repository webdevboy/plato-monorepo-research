import React, { Component } from 'react';

import Button from 'components/shared/Button';
import EmptyStateCard from 'components/shared/EmptyStateCard';
import Message from 'components/shared/Message';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import SignUpLayout from 'components/shared/SignUpLayout';
import SlackImage from 'images/slack.jpg';
import { connect } from 'react-redux';
import { getSignUpData } from 'selectors';
import queryString from 'query-string';
import styled from 'styled-components';

const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  text-align: center;

  .slack-button {
    min-width: 40rem;
  }

  ${Message} {
    margin-bottom: 2rem;
  }
`;

const InfoCard = styled.div`
  background-color: ${props => props.theme.palette.aquaHaze};
  border-radius: 2px;
  color: ${props => props.theme.palette.osloGrey};
  display: grid;
  grid-template-columns: 3rem auto;
  margin-top: 2rem;
  padding: 2rem;
  text-align: left;
  width: 40rem;

  .icon {
    font-size: 16px;
    margin-top: 0.6rem;
  }

  .mailto {
    display: block;
    margin-top: 1rem;

    ${Button} {
      text-align: left;
    }
  }
`;

class Slack extends Component {
  getErrorMessage() {
    const { location } = this.props;
    const { installStatus } = queryString.parse(location.search);
    switch (installStatus) {
      case 'missing-rights':
        return "You don't have the permissions to install the Slack bot. Please contact your Slack admin.";
      case 'failure':
        return 'The Slack bot installation has failed. Try again or contact an admin.';
      default:
        return null;
    }
  }

  render() {
    const {
      signUp: { slackUrl },
    } = this.props;
    if (!slackUrl) {
      return (
        <SignUpLayout title="Install Slack bot" image={SlackImage}>
          <EmptyStateCard
            title="Something is missing"
            description="We can't proceed with the Slack installation since some required infos are missing"
          />
        </SignUpLayout>
      );
    }

    const errorMessage = this.getErrorMessage();
    return (
      <SignUpLayout
        title="Install Slack bot"
        description={
          <>
            Our product is based on Slack.
            <br />
            In order to enjoy the full experience, it is required
            <br />
            to add the Slack Bot to have Plato fully functioning.
          </>
        }
        image={SlackImage}
      >
        <Container>
          {errorMessage && <Message text={errorMessage} type="error" hasBackground />}
          <a href={slackUrl}>
            <Button variant="primary" className="slack-button">
              Add to slack
            </Button>
          </a>
          <InfoCard>
            <FontAwesomeIcon icon="exclamation-circle" className="icon" />
            <div>
              <sub>
                You don&apos;t have the admin rights?
                <br />
                Ask your Slack admin to install the Plato bot, or create your own Slack account!
              </sub>
              <a
                href={`mailto:?subject=Plato%20installation&body=Installation%20link%3A%0A%0A${encodeURIComponent(
                  slackUrl
                )}`}
                className="mailto"
              >
                <Button variant="primary-link">
                  Send an email to your Slack admin&nbsp;
                  <FontAwesomeIcon icon="chevron-right" />
                </Button>
              </a>
            </div>
          </InfoCard>
        </Container>
      </SignUpLayout>
    );
  }
}

function mapStateToProps(state) {
  return {
    signUp: getSignUpData(state),
  };
}

export default connect(mapStateToProps)(Slack);
