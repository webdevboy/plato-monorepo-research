import React, { Component } from 'react';
import { getSignUpData, getSignUpErrorMessage, getSignUpPlanPrice } from 'selectors';

import Button from 'components/shared/Button';
import CheckoutForm from './CheckoutForm';
import { Elements } from 'react-stripe-elements';
import EmptyStateCard from 'components/shared/EmptyStateCard';
import IconLabel from 'components/shared/IconLabel';
import PaymentImage from 'images/payment.svg';
import Pluralize from 'react-pluralize';
import PropTypes from 'prop-types';
import SignUpLayout from 'components/shared/SignUpLayout';
import { connect } from 'react-redux';
import { getPlanPriceByInterval } from 'utils/stripe';
import moment from 'moment-timezone';
import { signUp } from 'ducks';
import styled from 'styled-components';

const ProductName = styled.span`
  display: flex;
  flex-direction: column;
  justify-content: space-between;

  sub {
    color: ${props => props.theme.palette.osloGrey};
  }
`;

const MathOperator = styled.span`
  color: ${props => props.theme.palette.java};
  margin: 0 1rem;
`;

const FormWrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding: 1rem;
  justify-content: center;

  section {
    margin: 1rem 0 3rem;

    .container {
      border: 1px solid ${props => props.theme.palette.geyser};
      padding: 1rem 2rem;
      display: flex;
      align-items: baseline;
      justify-content: space-between;

      &:first-child {
        border-bottom: none;
      }

      .product-info {
        display: flex;
        justify-content: space-between;
        align-items: center;
      }
    }
  }
`;

class Payment extends Component {
  handlePrevious = event => {
    const { history } = this.props;
    event.stopPropagation();
    history.goBack();
  };

  handleSubmit = (stripe, name, cardEmail) => {
    const {
      onSignUp,
      signUpData: { email, firstName, lastName, plan, companyName, faceId },
    } = this.props;
    onSignUp(stripe, name, cardEmail, plan.id, {
      firstName,
      lastName,
      email,
      planId: plan.id,
      quantity: 1,
      name: companyName,
      timeZone: moment.tz.guess(),
      faceId,
    });
  };

  render() {
    const { errorMessage, signUpData, price } = this.props;

    if (!signUpData.plan) {
      return (
        <SignUpLayout title="Payment" image={PaymentImage}>
          <EmptyStateCard
            title="Something is missing"
            description="Please select a plan to start enjoying Plato!"
          />
        </SignUpLayout>
      );
    }
    return (
      <SignUpLayout title="Payment" image={PaymentImage}>
        <FormWrapper>
          <h2>
            <IconLabel icon="check">Subscription</IconLabel>
          </h2>
          <section>
            <p className="container">
              <ProductName>
                <b>
                  {signUpData.product.name} -&nbsp;
                  <Pluralize
                    singular={signUpData.plan.interval}
                    count={signUpData.plan.interval_count}
                  />
                </b>
                <sub>Auto-renewing annual subscription</sub>
              </ProductName>
              <span>
                ${getPlanPriceByInterval(signUpData.plan)}
                <MathOperator>x</MathOperator>
                <Pluralize
                  singular={signUpData.plan.interval}
                  count={signUpData.plan.interval_count}
                />
                <MathOperator>=</MathOperator>${price}
              </span>
            </p>
            <p className="container">
              <span>Subtotal</span>
              <b>${price}</b>
            </p>
          </section>

          <h2>
            <IconLabel icon="check">Payment</IconLabel>
          </h2>
          <section>
            <Elements>
              <CheckoutForm
                email={signUpData.email}
                errorMessage={errorMessage}
                firstName={signUpData.firstName}
                lastName={signUpData.lastName}
                onSubmit={this.handleSubmit}
              >
                <Button onClick={this.handlePrevious}>Previous</Button>
              </CheckoutForm>
            </Elements>
          </section>
        </FormWrapper>
      </SignUpLayout>
    );
  }
}

Payment.propTypes = {
  errorMessage: PropTypes.string,
  history: PropTypes.object.isRequired,
  onSignUp: PropTypes.func.isRequired,
  price: PropTypes.number,
  signUpData: PropTypes.object.isRequired,
};

Payment.defaultProps = {
  price: null,
  errorMessage: null,
};

function mapStateToProps(state) {
  return {
    price: getSignUpPlanPrice(state),
    signUpData: getSignUpData(state),
    errorMessage: getSignUpErrorMessage(state),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    onSignUp: (stripe, name, email, planId, mentee) =>
      dispatch(signUp({ stripe, name, email, planId, mentee })),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Payment);
