import { CardElement, injectStripe } from 'react-stripe-elements';
import React, { Component } from 'react';

import Button from 'components/shared/Button';
import Message from 'components/shared/Message';
import PropTypes from 'prop-types';
import TextInput from 'components/shared/TextInput';
import styled from 'styled-components';

// Stripe Elements use inline styling only
const cardElementStyle = {
  base: {
    border: '1px solid #D7DCE2',
    fontSize: '14px',
    padding: '14px 14px 14px 12px',
    marginBottom: '2rem',
    height: '5rem',
    borderRadius: '0.4rem',

    '::placeholder': {
      color: '#ABB4BF',
    },
  },
};

const CardDetails = styled.h4`
  margin-bottom: 1rem;
`;

const CardInput = styled.div`
  border: 1px solid ${props => props.theme.palette.geyser};
  transition: border 300ms ease;
  padding: 1.4rem 1.2rem;
  margin-bottom: 2rem;
  height: 5rem;
  border-radius: 0.4rem;
`;

const ButtonsContainer = styled.div`
  display: flex;
  justify-content: space-between;
`;

const StyledMessage = styled(Message)`
  margin-bottom: 2rem;
`;

class CheckoutForm extends Component {
  state = { name: null, email: null };

  componentDidMount() {
    const { firstName, lastName, email } = this.props;
    this.setState({
      name: `${firstName} ${lastName}`,
      email,
    });
  }

  handleSubmit = event => {
    const { onSubmit, stripe } = this.props;
    const { name, email } = this.state;
    event.preventDefault();
    onSubmit(stripe, name, email);
  };

  handleChange = event => {
    const {
      target: { name, value },
    } = event;
    this.setState({ [name]: value });
  };

  render() {
    const { children, errorMessage } = this.props;
    const { email, name } = this.state;
    return (
      <form onSubmit={this.handleSubmit}>
        <CardDetails>Card details</CardDetails>
        <CardInput>
          <CardElement style={cardElementStyle} />
        </CardInput>
        <TextInput
          label="Card holder name"
          name="name"
          placeholder="e.g. Joseph Smith"
          type="text"
          value={name || ''}
          onChange={this.handleChange}
        />
        <TextInput
          label="Card holder email"
          name="email"
          placeholder="e.g. joseph-smith@example.com"
          type="text"
          value={email || ''}
          onChange={this.handleChange}
        />
        {errorMessage && <StyledMessage text={errorMessage} type="error" />}
        <ButtonsContainer>
          {children}
          <Button variant="primary" type="submit" onClick={this.handleSubmit}>
            Confirm order
          </Button>
        </ButtonsContainer>
      </form>
    );
  }
}

CheckoutForm.propTypes = {
  children: PropTypes.node,
  onSubmit: PropTypes.func.isRequired,
  errorMessage: PropTypes.object,
  firstName: PropTypes.string.isRequired,
  lastName: PropTypes.string.isRequired,
  email: PropTypes.string.isRequired,
  stripe: PropTypes.object.isRequired,
};

CheckoutForm.defaultProps = {
  children: null,
  errorMessage: null,
};

export default injectStripe(CheckoutForm);
