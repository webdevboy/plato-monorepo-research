import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  getUserFaces,
  getUserTags,
  getMentorsList,
  getUser,
  getHierarchiesForMentorSearch,
  getHierarchies,
} from 'selectors';

import Button from 'components/shared/Button';
import LoadingSpinner from 'components/shared/LoadingSpinner';
import MentorCard from 'components/pages/mentee/mentors/MentorCard';
import MentorDialog from './MentorDialog';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import OnboardingLayout from 'components/shared/OnboardingLayout';
import TracksImage from 'images/tracks.svg';
import { toUTCHour } from 'utils/time';

import { fetchMentors, fetchHierarchies } from 'ducks';

const StyledLongTerm = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  grid-column-gap: 2rem;
  grid-row-gap: 2rem;
  padding-bottom: 2rem;
  text-align: center;

  @media only screen and (min-width: 1025px) {
    grid-template-columns: 1fr 1fr 1fr;
  }

  .back-button {
    margin-top: 2rem;
    grid-column: 1/2;
    justify-self: flex-start;
  }

  .buttons {
    margin-top: 2rem;
    grid-column: 3/4;
    justify-self: flex-end;

    ${Button} {
      margin-left: 2rem;
    }
  }
`;

const LoadingSpinnerWrapper = styled.div`
  text-align: center;
`;

class LongTerm extends Component {
  state = {
    isMentorDialogOpen: false,
    loading: false,
    mentor: null,
  };

  componentDidMount() {
    const { onFetchHierarchies } = this.props;

    onFetchHierarchies();
  }

  componentDidUpdate() {
    const {
      onFetchMentors,
      faces,
      tags,
      user: { timeZone },
      hierarchies,
      hierarchiesForSearch,
      mentors,
    } = this.props;

    if (hierarchies && hierarchies.length > 0 && mentors.length === 0) {
      onFetchMentors({
        page: 0,
        size: 8,
        faces,
        tags,
        availableForRelationship: true,
        availableBeforeHour: toUTCHour(20, timeZone),
        availableAfterHour: toUTCHour(8, timeZone),
        hierarchies: hierarchiesForSearch,
      });
    }
  }

  handleSkip = () => {
    const { history, pageAfterNext } = this.props;
    history.push(pageAfterNext);
  };

  handleContinue = mentorId => {
    const { history } = this.props;
    history.push(`/onboarding/mentor/${mentorId}/slots`);
  };

  handleClose = () => {
    this.setState({ isMentorDialogOpen: false, mentor: null });
  };

  handleShowProfile = mentor => {
    this.setState({ isMentorDialogOpen: true, mentor });
  };

  handlePrevious = () => {
    const { history } = this.props;
    history.goBack();
  };

  render() {
    const { currentPageIndex, totalPages, mentors } = this.props;
    const { isMentorDialogOpen, mentor, loading } = this.state;
    return (
      <OnboardingLayout
        currentPage={currentPageIndex}
        totalPages={totalPages}
        image={TracksImage}
        title="Select a long-term mentorship"
        description="Now it's time for you to test out a few calls with a mentor curated by Plato according to the information you've previously filled - no worries, you can always opt out if it's not a good fit."
        caption="find a mentor"
        onNext={this.handleSkip}
        nextButtonLabel="Skip"
        onPrevious={this.handlePrevious}
      >
        {mentors.length === 0 || loading ? (
          <LoadingSpinnerWrapper>
            <LoadingSpinner />
          </LoadingSpinnerWrapper>
        ) : (
          <StyledLongTerm>
            {mentors.map(mentorItem => (
              <MentorCard
                key={mentorItem.id}
                buttonTitle="Preview"
                mentor={mentorItem}
                onClick={this.handleShowProfile}
                onBook={() => this.handleContinue(mentorItem.id)}
                isNextAvailabilitiesShown
              />
            ))}
          </StyledLongTerm>
        )}

        {mentor && (
          <MentorDialog
            isOpen={isMentorDialogOpen}
            onClose={this.handleClose}
            mentorId={mentor.id}
          />
        )}
      </OnboardingLayout>
    );
  }
}

function mapStateToProps(state) {
  return {
    user: getUser(state),
    mentors: getMentorsList(state),
    faces: getUserFaces(state),
    tags: getUserTags(state),
    hierarchies: getHierarchies(state),
    hierarchiesForSearch: getHierarchiesForMentorSearch(state),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    onFetchMentors: data => dispatch(fetchMentors(data)),
    onFetchHierarchies: () => dispatch(fetchHierarchies()),
  };
}

LongTerm.propTypes = {
  currentPageIndex: PropTypes.number.isRequired,
  history: PropTypes.object.isRequired,
  totalPages: PropTypes.number.isRequired,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LongTerm);
