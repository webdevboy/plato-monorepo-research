import React, { Component } from 'react';
import {
  getPersonas,
  getPersonasList,
  isActionLoading,
  getUserPersona,
  getUserMenteeId,
} from 'selectors';

import Button from 'components/shared/Button';
import { Formik } from 'formik';
import LoadingSpinner from 'components/shared/LoadingSpinner';
import OnboardingLayout from 'components/shared/OnboardingLayout';
import PropTypes from 'prop-types';
import RadioInput from 'components/shared/RadioInput';
import classNames from 'classnames';
import { connect } from 'react-redux';
import { fetchPersonas, updateMentee } from 'ducks';
import profileImage from 'images/profile.svg';
import styled from 'styled-components';

const Challenges = styled.div`
  margin-top: 1rem;
  padding: 2rem;
  color: ${({ theme }) => theme.palette.osloGrey};
  background-color: ${({ theme }) => theme.palette.polar};

  h3 {
    margin-bottom: 1rem;
  }

  li {
    margin-bottom: 1rem;
  }
`;

const ButtonContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  margin-top: 2rem;
`;

const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const List = styled.ul`
  max-width: 60rem;
`;

const ListItem = styled.li`
  margin-bottom: 2rem;

  ${RadioInput} {
    border: 2px solid ${({ theme }) => theme.palette.geyser};
    border-radius: 4px;
    padding: 2rem;
    align-items: flex-start;

    b {
      color: ${({ theme }) => theme.palette.osloGrey};
    }

    .input {
      margin-top: 1rem;
    }
  }

  &.checked ${RadioInput} {
    border-color: ${({ theme }) => theme.palette.java};

    b {
      color: ${({ theme }) => theme.palette.java};
    }
  }
`;

class Profile extends Component {
  componentWillMount() {
    const { onFetchPersonas } = this.props;
    onFetchPersonas();
  }

  handlePrevious = () => {
    const { history } = this.props;
    history.goBack();
  };

  handleSubmit = values => {
    const {
      location: { pathname },
      nextPage,
      menteeId,
      onUpdateMentee,
    } = this.props;

    onUpdateMentee(menteeId, { onboardingStep: pathname, personaId: values.personaId }, nextPage);
  };

  handleValidate = values => {
    const errors = {};
    if (!values.personaId) {
      errors.personaId = 'Required';
    }
    return errors;
  };

  handleRenderForm = formikProps => {
    const { isFetchingPersonas, personas, personasList, isLoading } = this.props;
    const { isValid, values, handleSubmit, handleChange } = formikProps;
    const selectedPersona = personas[values.personaId];

    if (isFetchingPersonas && personasList.length === 0) {
      return (
        <Container>
          <LoadingSpinner />
        </Container>
      );
    }

    return (
      <>
        <Container>
          <List>
            {personasList.map(persona => (
              <ListItem
                key={persona.id}
                className={classNames({
                  checked: selectedPersona && selectedPersona.id === persona.id,
                })}
              >
                <RadioInput
                  isChecked={Boolean(selectedPersona && selectedPersona.id === persona.id)}
                  name="personaId"
                  value={String(persona.id)}
                  label={
                    <p>
                      <b>{persona.name}</b>
                      <br />
                      {persona.description}
                    </p>
                  }
                  onChange={handleChange}
                />
                {selectedPersona && selectedPersona.id === persona.id && (
                  <Challenges>
                    <h3>Examples of common challenges</h3>
                    <ul>
                      {selectedPersona.challenges.slice(0, 10).map(challenge => (
                        <li key={challenge.id}>{challenge.title}</li>
                      ))}
                      <li>&hellip;</li>
                    </ul>
                  </Challenges>
                )}
              </ListItem>
            ))}
          </List>
        </Container>
        <ButtonContainer>
          <Button className="back-button" onClick={this.handlePrevious}>
            Previous
          </Button>

          <Button
            disabled={!isValid || isLoading}
            className="save-button"
            variant="primary"
            type="submit"
            onClick={handleSubmit}
          >
            Continue
          </Button>
        </ButtonContainer>
      </>
    );
  };

  render() {
    const { currentPageIndex, totalPages, persona } = this.props;

    return (
      <OnboardingLayout
        currentPage={currentPageIndex}
        totalPages={totalPages}
        caption="about myself"
        image={profileImage}
        title="Which profile seems to define you best?"
        description={
          <>
            Please confirm a profile to enable a pre-selection of
            <br />
            adapted common challenges for the next steps of your onboarding
          </>
        }
      >
        <Formik
          initialValues={{ personaId: persona && persona.id }}
          isInitialValid={Boolean(persona && persona.id)}
          validate={this.handleValidate}
          onSubmit={this.handleSubmit}
          onChange={this.handleChange}
        >
          {this.handleRenderForm}
        </Formik>
      </OnboardingLayout>
    );
  }
}

Profile.propTypes = {
  currentPageIndex: PropTypes.number.isRequired,
  history: PropTypes.object.isRequired,
  isFetchingPersonas: PropTypes.bool.isRequired,
  isLoading: PropTypes.bool.isRequired,
  onFetchPersonas: PropTypes.func.isRequired,
  personas: PropTypes.object.isRequired,
  personasList: PropTypes.array.isRequired,
  totalPages: PropTypes.number.isRequired,
};

function mapStateToProps(state) {
  return {
    isFetchingPersonas: isActionLoading(state, fetchPersonas),
    persona: getUserPersona(state),
    personas: getPersonas(state),
    personasList: getPersonasList(state),
    menteeId: getUserMenteeId(state),
    isLoading: isActionLoading(state, updateMentee),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    onFetchPersonas: () => dispatch(fetchPersonas()),
    onUpdateMentee: (menteeId, data, redirectPath) =>
      dispatch(updateMentee({ menteeId, ...data, redirectPath })),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Profile);
