import React, { Component } from 'react';

import Dropzone from 'react-dropzone';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import OnboardingLayout from 'components/shared/OnboardingLayout';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { isActionLoading, getUser } from 'selectors';
import profilePictureImage from 'images/profile-picture.svg';
import styled from 'styled-components';
import { updateMentee } from 'ducks';

const DropZoneWrapper = styled.div`
  grid-column: 1/3;
  cursor: pointer;
  height: 340px;
  border-radius: 5px;
  border: 1px solid ${props => props.theme.palette.geyser};

  display: flex;
  flex-direction: column;
  align-items: center;

  h3 {
    margin-top: 6rem;
    font-size: 12px;
  }
`;

const Circle = styled.div`
  align-items: center;
  background-color: ${({ theme }) => theme.palette.polar};
  border: 1px dashed ${({ theme }) => theme.palette.cerulean};
  color: ${({ theme }) => theme.palette.cerulean};
  display: flex;
  justify-content: center;
  position: relative;
  width: 18rem;
  height: 18rem;
  border-radius: 9rem;
  margin-top: 2rem;
`;

const Image = styled.img`
  width: 18rem;
  height: 18rem;
  border-radius: 9rem;
  margin-top: 2rem;
`;

class ProfilePicture extends Component {
  constructor(props) {
    super(props);
    const {
      user: { picture },
    } = props;

    this.state = {
      picture: null,
      previewUrl: picture && picture.originalUrl,
    };
  }

  handleDrop = acceptedFiles => {
    if (!acceptedFiles || !acceptedFiles[0]) {
      return;
    }
    this.setState({
      picture: acceptedFiles[0],
      previewUrl: URL.createObjectURL(acceptedFiles[0]),
    });
  };

  handleRenderDropZone = args => {
    const { getRootProps, getInputProps, isDragActive } = args;
    const { previewUrl } = this.state;
    return (
      <DropZoneWrapper {...getRootProps()}>
        <input {...getInputProps()} />
        <h3>
          {isDragActive
            ? 'Select or drag and drop a picture here'
            : 'Upload a picture (jpg, png, 100 Kb max)'}
        </h3>
        {previewUrl ? (
          <Image src={previewUrl} alt="Profile" />
        ) : (
          <Circle>
            <FontAwesomeIcon icon="user" size="6x" />
          </Circle>
        )}
      </DropZoneWrapper>
    );
  };

  handlePrevious = () => {
    const { history } = this.props;
    history.goBack();
  };

  handleSave = () => {
    const {
      location: { pathname },
      nextPage,
      onUpdateMentee,
      user: { menteeId },
    } = this.props;
    const { picture } = this.state;
    onUpdateMentee(menteeId, { onboardingStep: pathname, picture: picture || undefined }, nextPage);
  };

  render() {
    const { currentPageIndex, isLoading, totalPages } = this.props;
    const { previewUrl } = this.state;
    return (
      <OnboardingLayout
        currentPage={currentPageIndex}
        totalPages={totalPages}
        image={profilePictureImage}
        title="Your profile picture"
        description="We will show this picture when introducing you to your mentors."
        caption="about myself"
        onPrevious={this.handlePrevious}
        onNext={this.handleSave}
        isNextEnabled={!isLoading && previewUrl}
      >
        <Dropzone onDrop={this.handleDrop}>{this.handleRenderDropZone}</Dropzone>
      </OnboardingLayout>
    );
  }
}

ProfilePicture.propTypes = {
  currentPageIndex: PropTypes.number.isRequired,
  history: PropTypes.object.isRequired,
  isLoading: PropTypes.bool.isRequired,
  location: PropTypes.object.isRequired,
  nextPage: PropTypes.string.isRequired,
  totalPages: PropTypes.number.isRequired,
};

function mapStateToProps(state) {
  return {
    isLoading: isActionLoading(state, updateMentee),
    user: getUser(state),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    onUpdateMentee: (menteeId, data, redirectPath) =>
      dispatch(updateMentee({ menteeId, ...data, redirectPath })),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProfilePicture);
