import React, { Component } from 'react';

import LoadingSpinner from 'components/shared/LoadingSpinner';
import PropTypes from 'prop-types';
import SessionCard from 'components/pages/mentee/ama/SessionCard';
import SignUpDialog from 'components/pages/mentee/ama/SignUpDialog';
import getProfilePictureThumbnail from 'utils/thumbnail';
import styled from 'styled-components';
import OnboardingLayout from 'components/shared/OnboardingLayout';
import { updateMentee, fetchAmas } from 'ducks';
import { getUser, getAmas, getMentors, getAmasList } from 'selectors';
import TracksImage from 'images/tracks.svg';
import { connect } from 'react-redux';
import { toUTCHour } from 'utils/time';

const StyledBookAma = styled.div`
  .session-card {
    margin-bottom: 2rem;
  }
`;

const LoadingSpinnerWrapper = styled.div`
  text-align: center;
`;

class BookAma extends Component {
  state = {
    ama: null,
    signUpDialogIsOpen: false,
  };

  componentDidMount() {
    const {
      onFetchAmas,
      user: { timeZone, tags },
    } = this.props;

    onFetchAmas({
      tagIds: tags.map(tag => tag.id),
      size: process.env.REACT_APP_NB_AMA_RESULTS,
      featured: false,
      minAttendees: 3,
      scheduledAfterHour: toUTCHour(8, timeZone),
      scheduledBeforeHour: toUTCHour(20, timeZone),
    });
  }

  handleContinue = () => {
    const {
      location: { pathname },
      nextPage,
      onUpdateMentee,
      user: { menteeId },
    } = this.props;
    onUpdateMentee(menteeId, { onboardingStep: pathname }, nextPage);
  };

  handlePrevious = () => {
    const { history } = this.props;
    history.goBack();
  };

  handleLoadMore = page => {
    this.fetchAmas(page);
  };

  handleOpenSignUpDialog = ama => {
    this.setState({ signUpDialogIsOpen: true, ama });
  };

  handleCloseSignUpDialog = () => {
    this.setState({ signUpDialogIsOpen: false, ama: null });
  };

  render() {
    const {
      currentPageIndex,
      totalPages: totalOnboardingPages,
      amas,
      amasList,
      mentors,
    } = this.props;
    const { ama: selectedAma, signUpDialogIsOpen } = this.state;

    return (
      <OnboardingLayout
        currentPage={currentPageIndex}
        totalPages={totalOnboardingPages}
        image={TracksImage}
        title="Book a one-off AMA"
        description='AMA (or "Ask Me Anything") sessions are group sessions where up to 10 attendees meet with a single mentor to discuss a common challenge'
        caption="Book an AMA"
        onNext={this.handleContinue}
        nextButtonLabel="Skip"
        onPrevious={this.handlePrevious}
      >
        <StyledBookAma>
          {amasList.length === 0 && (
            <LoadingSpinnerWrapper>
              <LoadingSpinner />
            </LoadingSpinnerWrapper>
          )}

          {amasList.map(ama => (
            <SessionCard
              attendances={ama.attendances}
              callScheduledAt={ama.callScheduledAt}
              className="session-card"
              key={ama.id}
              limitAttendances={ama.limitAttendances}
              mentorCompanyName={mentors[ama.mentorId] && mentors[ama.mentorId].companyName}
              mentorFullName={ama.mentorFullName}
              mentorPictureUrl={getProfilePictureThumbnail(
                mentors[ama.mentorId] && mentors[ama.mentorId].firstName,
                ama.mentorPicture.thumbnailUrl
              )}
              mentorTitle={mentors[ama.mentorId] && mentors[ama.mentorId].title}
              onSignUpClick={() => this.handleOpenSignUpDialog(ama.id)}
              storyName={ama.storyName}
              topics={ama.storyTagNames}
            />
          ))}

          {selectedAma && (
            <SignUpDialog
              amaId={selectedAma}
              attendances={amas[selectedAma].attendances}
              callScheduledAt={amas[selectedAma].callScheduledAt}
              limitAttendances={amas[selectedAma].limitAttendances}
              mentorCompanyName={mentors[amas[selectedAma].mentorId].companyName}
              mentorFullName={amas[selectedAma].mentorFullName}
              mentorTitle={mentors[amas[selectedAma].mentorId].title}
              onClose={this.handleCloseSignUpDialog}
              onConfirm={this.handleContinue}
              open={signUpDialogIsOpen}
              storyName={amas[selectedAma].storyName}
            />
          )}
        </StyledBookAma>
      </OnboardingLayout>
    );
  }
}

BookAma.propTypes = {
  history: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
  onUpdateMentee: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
  return {
    user: getUser(state),
    amas: getAmas(state),
    amasList: getAmasList(state),
    mentors: getMentors(state),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    onUpdateMentee: (menteeId, data, redirectPath) =>
      dispatch(updateMentee({ menteeId, ...data, redirectPath })),
    onFetchAmas: data => dispatch(fetchAmas(data)),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BookAma);
