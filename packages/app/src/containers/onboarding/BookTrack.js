import React, { Component } from 'react';
import { fetchPrograms, fetchTracks } from 'ducks';
import { getPrograms, getTracks } from 'selectors/tracks';

import AmaTrackCard from 'components/shared/AmaTrackCard';
import LoadingSpinner from 'components/shared/LoadingSpinner';
import OnboardingLayout from 'components/shared/OnboardingLayout';
import PropTypes from 'prop-types';
import TracksImage from 'images/tracks.svg';
import { connect } from 'react-redux';
import { isActionLoading } from 'selectors';
import styled from 'styled-components';

const LoadingSpinnerWrapper = styled.div`
  text-align: center;
`;

class BookTrack extends Component {
  componentDidMount() {
    const { onFetchTracks, onFetchPrograms } = this.props;
    onFetchPrograms();
    onFetchTracks();
  }

  handlePrevious = () => {
    const { history } = this.props;
    history.goBack();
  };

  handleApplyTrack = program => {
    const { history } = this.props;
    history.push(`/onboarding/book-track/${program.id}`);
  };

  handleSkip = () => {
    const { history, pageAfterNext } = this.props;
    history.push(pageAfterNext);
  };

  render() {
    const { currentPageIndex, isProgramFetching, totalPages, tracks, programs } = this.props;
    return (
      <OnboardingLayout
        currentPage={currentPageIndex}
        totalPages={totalPages}
        image={TracksImage}
        title="Book a track"
        description="You will join a group of peers who are facing the same challenges as you, and you will meet different mentors who have tackled those challenges before."
        onPrevious={this.handlePrevious}
        onNext={this.handleSkip}
        caption="book track"
        nextButtonLabel="Skip"
      >
        {isProgramFetching && programs.length === 0 && (
          <LoadingSpinnerWrapper>
            <LoadingSpinner />
          </LoadingSpinnerWrapper>
        )}
        {programs.map(program => (
          <AmaTrackCard
            key={program.id}
            program={program}
            track={tracks[program.trackId]}
            onApplyTrack={this.handleApplyTrack}
          />
        ))}
      </OnboardingLayout>
    );
  }
}

function mapStateToProps(state) {
  return {
    tracks: getTracks(state),
    programs: getPrograms(state),
    isProgramFetching: isActionLoading(state, fetchPrograms),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    onFetchTracks: () => dispatch(fetchTracks()),
    onFetchPrograms: () => dispatch(fetchPrograms()),
  };
}

BookTrack.propTypes = {
  currentPageIndex: PropTypes.number.isRequired,
  history: PropTypes.object.isRequired,
  totalPages: PropTypes.number.isRequired,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BookTrack);
