import React, { Component } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

import AboutYou from './AboutYou';
import BookAma from './BookAma';
import BookTrack from './BookTrack';
import CheckIn from './CheckIn';
import Index from './Index';
import LongTerm from './LongTerm';
import MentorRelationshipSlots from './MentorRelationshipSlots';
import { ONBOARDING_LOCAL_STORAGE_KEY } from 'constants/Onboarding';
import Product from './Product';
import Profile from './Profile';
import ProfilePicture from './ProfilePicture';
import Program from './Program';
import PropTypes from 'prop-types';
import TimeSlots from './TimeSlots';
import Topics from './Topics';
import { connect } from 'react-redux';
import { getIsProductManager } from 'selectors';

export class Onboarding extends Component {
  constructor(props) {
    super(props);
    const { isProductManager } = props;
    const flow = {
      aboutYou: isProductManager
        ? [AboutYou, ProfilePicture, Product, Profile, Topics, TimeSlots]
        : [AboutYou, ProfilePicture, Profile, Topics, TimeSlots],
      track: [BookTrack, Program, BookAma],
      mentor: [LongTerm, MentorRelationshipSlots],
      checkIn: [CheckIn],
    };
    this.state = {
      flow,
      totalPages: Object.values(flow).reduce((acc, steps) => acc + steps.length, 0),
    };
  }

  componentDidUpdate(prevProps) {
    const {
      location: { pathname },
    } = this.props;

    if (pathname === '/onboarding' && prevProps.location.pathname === '/onboarding/time-slots') {
      // Handle the last page of the about-you step
      this.setLocalStorage('/onboarding/book-track');
      return;
    }

    if (pathname === '/onboarding' && prevProps.location.pathname === '/onboarding/book-ama') {
      // Handle the last page of the track step
      this.setLocalStorage('/onboarding/long-term');
      return;
    }

    if (
      pathname === '/onboarding' &&
      (prevProps.location.pathname === '/onboarding/long-term' ||
        prevProps.location.pathname.startsWith('/onboarding/mentor/'))
    ) {
      // Handle the last page of the mentor step
      this.setLocalStorage('/onboarding/check-in');
      return;
    }

    this.setLocalStorage(pathname);
  }

  setLocalStorage(pathName) {
    // Set the value used in the button in the onboarding index
    const { flow } = this.state;
    const orderedSteps = Object.values(flow)
      .reduce((acc, steps) => [...acc, ...steps], [])
      .map(step => this.getPath(step));
    const currentStoredPath = localStorage.getItem(ONBOARDING_LOCAL_STORAGE_KEY);
    const indexOfCurrentStoredPath = orderedSteps.indexOf(currentStoredPath);

    // Only set the local storage to the current path name if the page is located further in the current flow
    if (indexOfCurrentStoredPath < orderedSteps.indexOf(pathName)) {
      localStorage.setItem(ONBOARDING_LOCAL_STORAGE_KEY, pathName);
      // Force a rerender as the local storage is not linked to Redux
      this.forceUpdate();
    }
  }

  getGlobalNextStepPath() {
    const { flow } = this.state;
    const nextStepPath = localStorage.getItem(ONBOARDING_LOCAL_STORAGE_KEY);
    const availablePath = Object.values(flow)
      .reduce((acc, steps) => [...acc, ...steps], [])
      .map(step => this.getPath(step));
    if (!availablePath.includes(nextStepPath)) {
      // Handle a wrong data stored in the local storage
      return this.getPath(flow.aboutYou[0]);
    }

    // Return the step to be displayed in the onboarding index
    return nextStepPath;
  }

  getPath(PageComponent) {
    // Get the path of the current component for navigation between onboarding steps
    switch (PageComponent) {
      case Index:
        return '/onboarding';
      case AboutYou:
        return '/onboarding/about-you';
      case Profile:
        return '/onboarding/profile';
      case Product:
        return '/onboarding/product';
      case ProfilePicture:
        return '/onboarding/profile-picture';
      case TimeSlots:
        return '/onboarding/time-slots';
      case Topics:
        return '/onboarding/topics';
      case BookTrack:
      case Program:
        return '/onboarding/book-track';
      case BookAma:
        return '/onboarding/book-ama';
      case LongTerm:
      case MentorRelationshipSlots:
        return '/onboarding/long-term';
      case CheckIn:
        return '/onboarding/check-in';
      default:
        return '/challenges';
    }
  }

  getNextPage(PageComponent) {
    const { flow } = this.state;
    const step = Object.entries(flow).find(([, steps]) => steps.includes(PageComponent))[0];
    const index = flow[step].indexOf(PageComponent) + 1;
    if (index >= flow[step].length) {
      // Redirect the user to the onboarding index between each onbaording steps defined in the flow
      return '/onboarding';
    }
    // Redirect the user to the next onboarding page whithin the current step
    return this.getPath(flow[step][index]);
  }

  getRender(PageComponent, ownProps = {}) {
    const { flow, totalPages } = this.state;
    const currentPageIndex =
      Object.values(flow)
        .reduce((acc, steps) => [...acc, ...steps], [])
        .indexOf(PageComponent) + 1;
    // Add extra prop to the component passed in the routes
    return props => (
      <PageComponent
        {...props}
        {...ownProps}
        nextPage={this.getNextPage(PageComponent)}
        currentPageIndex={currentPageIndex}
        totalPages={totalPages}
      />
    );
  }

  render() {
    const {
      match: { path },
    } = this.props;
    const { flow } = this.state;
    return (
      <Switch>
        <Route
          exact
          path={`${path}`}
          render={() => (
            <Index flow={flow} getPath={this.getPath} nextStepPath={this.getGlobalNextStepPath()} />
          )}
        />

        <Route exact path={`${path}/about-you`} render={this.getRender(AboutYou)} />
        <Route exact path={`${path}/profile-picture`} render={this.getRender(ProfilePicture)} />
        <Route exact path={`${path}/product`} render={this.getRender(Product)} />
        <Route exact path={`${path}/profile`} render={this.getRender(Profile)} />
        <Route exact path={`${path}/topics`} render={this.getRender(Topics)} />
        <Route exact path={`${path}/time-slots`} render={this.getRender(TimeSlots)} />

        <Route
          exact
          path={`${path}/book-track`}
          render={this.getRender(BookTrack, { pageAfterNext: '/onboarding/book-ama' })}
        />
        <Route
          exact
          path={`${path}/book-track/:programId`}
          render={this.getRender(Program, { pageAfterNext: '/onboarding' })}
        />
        <Route exact path={`${path}/book-ama`} render={this.getRender(BookAma)} />

        <Route
          exact
          path={`${path}/long-term`}
          render={this.getRender(LongTerm, { pageAfterNext: '/onboarding' })}
        />
        <Route
          exact
          path={`${path}/mentor/:mentorId/slots`}
          render={this.getRender(MentorRelationshipSlots)}
        />

        <Route exact path={`${path}/check-in`} render={this.getRender(CheckIn)} />

        <Redirect to="/" />
      </Switch>
    );
  }
}

Onboarding.propTypes = {
  match: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  return {
    isProductManager: getIsProductManager(state),
  };
}

export default connect(mapStateToProps)(Onboarding);
