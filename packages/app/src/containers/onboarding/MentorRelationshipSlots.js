import React, { Component } from 'react';
import { createRelationship, fetchMentor, fetchMentorRelationshipSlots } from 'ducks';
import { formatRemainingTime, formatTimestampToDate, getWeeksToDate } from 'utils/time';
import {
  getMentorPictureUrl,
  getMentorRelationshipSlots,
  getUserMenteeId,
  getUserTimeZone,
} from 'selectors';

import LoadingSpinner from 'components/shared/LoadingSpinner';
import OnboardingLayout from 'components/shared/OnboardingLayout';
import PropTypes from 'prop-types';
import RadioInput from 'components/shared/RadioInput';
import { connect } from 'react-redux';
import moment from 'moment';
import styled from 'styled-components';
import tracksImage from 'images/tracks.svg';

const StyledRadioInput = styled(RadioInput)`
  margin-top: 1rem;
  border: 1px solid ${({ theme }) => theme.palette.geyser};
  border-radius: 2px;
  padding: 1rem 2rem;
`;

const ContentWrapper = styled.div`
  text-align: center;
  margin-top: 5rem;
`;

class MentorRelationshipSlots extends Component {
  state = {
    selectedSlot: null,
  };

  componentDidMount() {
    const {
      onFetchMentor,
      onFetchRelationshipSlots,
      match: {
        params: { mentorId },
      },
    } = this.props;
    onFetchMentor(mentorId);
    onFetchRelationshipSlots(mentorId);
  }

  handleChallengeSelection = (value, name) => {
    this.setState({ [name]: value });
  };

  handleSave = () => {
    const {
      nextPage,
      match: {
        params: { mentorId },
      },
      menteeId,
      onCreateRelationship,
    } = this.props;
    const { selectedSlot } = this.state;
    onCreateRelationship({
      menteeId,
      mentorId,
      nbOfWeeks: getWeeksToDate(selectedSlot),
      redirectPath: nextPage,
    });
  };

  handlePrevious = () => {
    const { history } = this.props;
    history.goBack();
  };

  render() {
    const {
      mentorPictureUrl,
      mentorRelationshipSlots,
      currentPageIndex,
      totalPages,
      timeZone,
    } = this.props;
    const { selectedSlot } = this.state;
    return (
      <OnboardingLayout
        currentPage={currentPageIndex}
        totalPages={totalPages}
        image={mentorPictureUrl}
        title="Choose the slot which suits you best"
        description="This is an opportunity to choose the first slot to meet your mentor - don't worry, we offer rescheduling and opting out options at any moment."
        caption="find a mentor"
        onPrevious={this.handlePrevious}
        onNext={this.handleSave}
        nextButtonLabel="Confirm"
        isNextEnabled={Boolean(selectedSlot)}
      >
        <ContentWrapper>
          {mentorRelationshipSlots.length > 0 ? (
            <>
              <h4>Choose a time slot</h4>
              {mentorRelationshipSlots.map(slot => (
                <StyledRadioInput
                  key={slot.id}
                  className="radio-input-box"
                  name="selectedSlot"
                  value={slot.startTime.toString() || ''}
                  label={
                    <>
                      <b>{formatRemainingTime(slot.startTime)}</b>
                      &nbsp;&nbsp;
                      <sub>
                        {formatTimestampToDate(
                          slot.startTime,
                          timeZone,
                          'dddd MMMM Do YYYY, H:mm A'
                        )}
                        &nbsp;({moment.tz(timeZone).zoneName()})
                      </sub>
                    </>
                  }
                  onChange={() => this.handleChallengeSelection(slot.startTime, 'selectedSlot')}
                  isChecked={parseInt(selectedSlot, 10) === parseInt(slot.startTime, 10)}
                />
              ))}
            </>
          ) : (
            <LoadingSpinner />
          )}
        </ContentWrapper>
      </OnboardingLayout>
    );
  }
}

MentorRelationshipSlots.propTypes = {
  currentPageIndex: PropTypes.number.isRequired,
  match: PropTypes.object.isRequired,
  menteeId: PropTypes.number.isRequired,
  mentorPictureUrl: PropTypes.string,
  mentorRelationshipSlots: PropTypes.array.isRequired,
  nextPage: PropTypes.string.isRequired,
  onCreateRelationship: PropTypes.func.isRequired,
  onFetchRelationshipSlots: PropTypes.func.isRequired,
  onFetchMentor: PropTypes.func.isRequired,
  timeZone: PropTypes.string.isRequired,
  totalPages: PropTypes.number.isRequired,
};

MentorRelationshipSlots.defaultProps = {
  mentorPictureUrl: tracksImage,
};

function mapStateToProps(state, ownProps) {
  const {
    match: {
      params: { mentorId },
    },
  } = ownProps;
  return {
    menteeId: getUserMenteeId(state),
    mentorPicture: getMentorPictureUrl(state, mentorId),
    mentorRelationshipSlots: getMentorRelationshipSlots(state, mentorId),
    timeZone: getUserTimeZone(state),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    onFetchMentor: mentorId => dispatch(fetchMentor(mentorId)),
    onFetchRelationshipSlots: mentorId => dispatch(fetchMentorRelationshipSlots(mentorId)),
    onCreateRelationship: data => dispatch(createRelationship(data)),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MentorRelationshipSlots);
