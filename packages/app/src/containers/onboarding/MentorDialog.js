import React, { Component } from 'react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import LoadingSpinner from 'components/shared/LoadingSpinner';
import PersonInfo from 'components/shared/PersonInfo';
import PlatoDialog from 'components/shared/PlatoDialog';
import TagList from 'components/shared/TagList';
import getProfilePictureThumbnail from 'utils/thumbnail';
import styled from 'styled-components';
import { getMentor } from 'selectors';
import { withRouter } from 'react-router-dom';
import { fetchMentor } from 'ducks';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

const StyledBody = styled.div`
  padding: 2rem;
  width: 60rem;
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-column-gap: 2rem;
  grid-row-gap: 2rem;

  .header {
    grid-column: 1/3;
  }

  .text-info {
    margin-bottom: 1rem;
  }

  ${PersonInfo} {
    margin-bottom: 1rem;
  }

  a {
    color: ${({ theme }) => theme.palette.cerulean};
    text-transform: uppercase;
    font-size: 1.4rem;
  }
`;

const StyledLoadingSpinner = styled(LoadingSpinner)`
  margin: 10rem;
`;

class MentorDialog extends Component {
  componentDidMount() {
    const { mentorId, onFetchMentor } = this.props;
    onFetchMentor(mentorId);
  }

  render() {
    const { mentor, onClose, isOpen } = this.props;

    if (!mentor) {
      return (
        <PlatoDialog open={isOpen} onClose={onClose}>
          <StyledLoadingSpinner />
        </PlatoDialog>
      );
    }

    const {
      picture,
      aboutMyself,
      fullName,
      title,
      companyName,
      linkedin,
      managementExperience,
      hierarchy,
      sizeOfTeam,
      expertise,
      domainExpertise,
      specialExpertise,
    } = mentor;
    return (
      <PlatoDialog open={isOpen} onClose={onClose}>
        <StyledBody>
          <div className="header">
            <PersonInfo
              companyName={companyName}
              fullName={fullName}
              size="lg"
              thumbnailUrl={getProfilePictureThumbnail(fullName, picture && picture.thumbnailUrl)}
              title={title}
              about={aboutMyself}
            />
            <a href={linkedin} target="_blank" rel="noopener noreferrer">
              <FontAwesomeIcon icon={['fab', 'linkedin']} color="#0073b1" />
              &nbsp; LinkedIn profile
            </a>
          </div>

          <div>
            <h3>SENIORITY</h3>
            <p className="text-info">{managementExperience} Years</p>

            <h3>HIERARCHY</h3>
            <p className="text-info">{hierarchy}</p>

            <h3>TEAM SIZE</h3>
            <p className="text-info">{sizeOfTeam} Engineer(s)</p>
          </div>

          <div>
            {expertise && expertise.length > 0 && (
              <>
                <p className="large-header">Management Expertise</p>
                <TagList tags={expertise.map(({ name }) => name)} />
              </>
            )}

            {domainExpertise && domainExpertise.length > 0 && (
              <>
                <p className="large-header">Domain Expertise</p>
                <TagList tags={domainExpertise.map(({ name }) => name)} />
              </>
            )}

            {specialExpertise && specialExpertise.length > 0 && (
              <>
                <p className="large-header">Other Expertise</p>
                <TagList tags={specialExpertise.map(({ name }) => name)} />
              </>
            )}
          </div>
        </StyledBody>
      </PlatoDialog>
    );
  }
}

MentorDialog.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  mentor: PropTypes.object,
  mentorId: PropTypes.number.isRequired,
  onClose: PropTypes.func.isRequired,
  onFetchMentor: PropTypes.func.isRequired,
};

MentorDialog.defaultProps = {
  mentor: null,
};

function mapStateToProps(state, ownProps) {
  const { mentorId } = ownProps;
  return {
    mentor: getMentor(state, mentorId),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    onFetchMentor: mentorId => dispatch(fetchMentor(mentorId)),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(MentorDialog));
