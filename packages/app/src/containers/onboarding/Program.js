import React, { Component } from 'react';
import { getProgram, getProgramAmas, getTracks, getUser, isActionLoading } from 'selectors';

import Button from 'components/shared/Button';
import CheckList from 'components/shared/CheckList';
import DateTimeWithIcons from 'components/shared/DateTimeWithIcons';
import { Formik } from 'formik';
import Message from 'components/shared/Message';
import OnboardingLayout from 'components/shared/OnboardingLayout';
import PropTypes from 'prop-types';
import RadioInput from 'components/shared/RadioInput';
import SessionCheckbox from 'components/shared/SessionCheckbox';
import TextInput from 'components/shared/TextInput';
import TracksImage from 'images/tracks.svg';
import { connect } from 'react-redux';
import { createProgramApplication } from 'ducks';
import styled from 'styled-components';

const StyledForm = styled.form`
  display: grid;
  grid-row-gap: 1rem;
  width: 100%;

  .expand {
    grid-column: 1/3;
  }

  ul li {
    border: 1px solid ${({ theme }) => theme.palette.geyser};
    border-bottom: none;

    ${SessionCheckbox} {
      padding: 1rem;
    }

    svg {
      margin-right: 1rem;
    }

    sub {
      color: ${({ theme }) => theme.palette.osloGrey};
    }

    &:first-child {
      border-top-left-radius: 4px;
      border-top-right-radius: 4px;
    }

    &:last-child {
      border-bottom-left-radius: 4px;
      border-bottom-right-radius: 4px;
      border-bottom: 1px solid ${({ theme }) => theme.palette.geyser};
    }
  }

  ${TextInput},
  .criteria,
  .criteria-description {
    margin-top: 2rem;
  }

  ${TextInput} textarea {
    margin-bottom: 1rem;
  }

  ${Message} {
    margin-bottom: 2rem;
  }

  .back-button {
    grid-column: 1/2;
  }

  .save-button {
    grid-column: 2/3;
    justify-self: flex-end;
  }
`;

class Program extends Component {
  componentDidMount() {
    const { program, amas, history } = this.props;

    if (!program || !amas) {
      history.replace('/onboarding/book-track');
    }
  }

  handlePrevious = () => {
    const { history } = this.props;
    history.goBack();
  };

  handleSubmit = values => {
    const {
      match: {
        params: { programId },
      },
      pageAfterNext,
      onCreate,
      user: { menteeId },
    } = this.props;
    const { goal, criteriaMet, amas } = values;
    const isCriteriaMet = criteriaMet === 'YES';
    const amasCommitted = Object.entries(amas).reduce(
      (acc, [id, isCommitted]) => (isCommitted ? [...acc, id] : acc),
      []
    );
    onCreate({
      programId,
      menteeId,
      goal,
      isCriteriaMet,
      amasCommitted,
      redirectPath: pageAfterNext,
    });
  };

  handleValidate = values => {
    const errors = {};
    if (!values.goal) {
      errors.goal = 'Goals are required';
    }
    if (!values.criteriaMet) {
      errors.criteriaMet = 'Field is required';
    }
    return errors;
  };

  handleRenderForm = formikProps => {
    const { amas, isLoading, program, tracks } = this.props;
    const { handleChange, handleSubmit, isValid, values } = formikProps;

    return (
      <StyledForm onSubmit={handleSubmit}>
        <h4 className="expand">Which sessions can you attend?</h4>
        <ul className="expand">
          {amas.map(ama => (
            <li key={ama.id}>
              <SessionCheckbox
                name={`amas.${ama.id}`}
                label={
                  <>
                    <b>{ama.storyName}</b>
                    <br />
                    <sub>{<DateTimeWithIcons timestamp={ama.callScheduledAt} />}</sub>
                  </>
                }
                isChecked={values.amas[ama.id]}
                onChange={handleChange}
              />
            </li>
          ))}
        </ul>

        <div className="criteria-description expand">
          <p>In order to apply to this program, you must meet the following criteria:</p>
          <br />
          <CheckList text={tracks[program.trackId].criteria} />
        </div>

        <h4 className="criteria expand">Do you meet those criteria?</h4>
        <RadioInput
          className="checkbox expand"
          isChecked={values.criteriaMet === 'YES'}
          name="criteriaMet"
          value="YES"
          label="Yes"
          onChange={handleChange}
        />
        <RadioInput
          className="checkbox expand"
          isChecked={values.criteriaMet === 'NO'}
          name="criteriaMet"
          label="No"
          value="NO"
          onChange={handleChange}
        />

        <TextInput
          className="expand"
          label="What are your goals?"
          name="goal"
          onChange={handleChange}
          placeholder="Describe your goals"
          rows={5}
          type="multi"
        />

        <CheckList
          icon="star"
          text="- Once we receive your application, you will be automatically accepted to the program or put on a waitlist.- Upon acceptance, you will receive a calendar invitation for each session you've committed to.- A Slack channel will be created for you to communicate with your peers.- Please be on time to your sessions and decline sessions you won't be able to attend ahead of time!"
        />

        <Button className="back-button" onClick={this.handlePrevious}>
          Previous
        </Button>

        <Button
          className="save-button"
          variant="primary"
          type="submit"
          disabled={!isValid || isLoading}
        >
          Confirm Application
        </Button>
      </StyledForm>
    );
  };

  render() {
    const { amas, currentPageIndex, totalPages, program } = this.props;

    // if program or amas data are missing, let the component mount and redirect
    if (!program || !amas) {
      return null;
    }

    return (
      <OnboardingLayout
        currentPage={currentPageIndex}
        totalPages={totalPages}
        image={TracksImage}
        title="Commitment"
        description="This program is designed to have the same group of peers working together longer term. If you apply, you must be able to attend most of the sessions."
        caption="book track"
      >
        <Formik
          initialValues={{
            amas: amas.reduce((acc, ama) => ({ ...acc, [ama.id]: true }), {}),
          }}
          validate={this.handleValidate}
          onSubmit={this.handleSubmit}
        >
          {this.handleRenderForm}
        </Formik>
      </OnboardingLayout>
    );
  }
}

Program.propTypes = {
  amas: PropTypes.array,
  program: PropTypes.object,
  currentPageIndex: PropTypes.number.isRequired,
  history: PropTypes.object.isRequired,
  isLoading: PropTypes.bool.isRequired,
  totalPages: PropTypes.number.isRequired,
};

Program.defaultProps = {
  amas: [],
  program: null,
};

function mapStateToProps(state, ownProps) {
  const {
    match: {
      params: { programId },
    },
  } = ownProps;
  return {
    isLoading: isActionLoading(state, createProgramApplication),
    program: getProgram(state, programId),
    amas: getProgramAmas(state, programId),
    tracks: getTracks(state),
    user: getUser(state),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    onCreate: data => dispatch(createProgramApplication(data)),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Program);
