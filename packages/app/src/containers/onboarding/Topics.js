import React, { Component } from 'react';

import Button from 'components/shared/Button';
import CommonChallengesImage from 'images/common-challenges.svg';
import { Formik } from 'formik';
import LoadingSpinner from 'components/shared/LoadingSpinner';
import OnboardingLayout from 'components/shared/OnboardingLayout';
import PropTypes from 'prop-types';
import SearchInput from 'components/shared/SearchInput';
import SessionCheckbox from 'components/shared/SessionCheckbox';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { getOnboardingChallenges, getUser, isActionLoading } from 'selectors';
import { fetchChallenges, updateMentee } from 'ducks';

const StyledSearchInput = styled(SearchInput)`
  margin: 3rem 0;
`;

const StyledSub = styled.sub`
  margin: 1.5rem 0;
  font-size: 1.5rem;
  color: ${({ theme }) => theme.palette.sunglo};
`;

const StyledLoadingContainer = styled.div`
  text-align: center;
`;

const StyledSelectedCheckbox = styled(SessionCheckbox)`
  color: ${({ theme }) => theme.palette.java};
  font-weight: bold;
  margin-bottom: 1rem;
`;

const StyledDeselectedCheckbox = styled(SessionCheckbox)`
  color: ${({ theme }) => theme.palette.osloGrey};
  margin-bottom: 1rem;
`;

const ButtonContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  margin-top: 2rem;
`;

class Topics extends Component {
  componentDidMount() {
    const { onFetchChallenges } = this.props;
    onFetchChallenges();
  }

  handleSubmit = values => {
    const {
      challenges,
      location: { pathname },
      nextPage,
      onUpdateMentee,
      user,
    } = this.props;

    const menteeTagIds = user.tags.map(tag => tag.id);
    const tagIds = challenges
      .filter(challenge => values.challenges[challenge.id])
      .reduce((acc, challenge) => [...acc, ...challenge.tags.map(tag => tag.id)], [])
      .filter(tagId => !menteeTagIds.includes(tagId));

    onUpdateMentee(
      user.menteeId,
      {
        onboardingStep: pathname,
        tagIds: [...new Set(tagIds)],
      },
      nextPage
    );
  };

  handlePrevious = () => {
    const { history } = this.props;
    history.goBack();
  };

  handleValidate = values => {
    const errors = {};
    if (Object.values(values.challenges).filter(Boolean).length < 3) {
      errors.personaId = 'Required';
    }
    return errors;
  };

  handleRenderForm = formikProps => {
    const { isLoading, challenges } = this.props;
    const { isValid, values, handleSubmit, handleChange } = formikProps;
    if (challenges.length === 0 && isLoading) {
      return (
        <StyledLoadingContainer>
          <LoadingSpinner />
        </StyledLoadingContainer>
      );
    }
    return (
      <>
        <h3 className="mb1">
          Selected&nbsp;
          {Object.values(values.challenges).filter(Boolean).length <= 2 &&
            `(${Object.values(values.challenges).filter(Boolean).length}/3)`}
        </h3>
        {values.challenges.length === 0 ? (
          <StyledSub>No challenge selected</StyledSub>
        ) : (
          challenges.map(
            challenge =>
              values.challenges[challenge.id] && (
                <StyledSelectedCheckbox
                  key={challenge.id}
                  name={`challenges.${challenge.id}`}
                  color="primary"
                  label={challenge.title}
                  isChecked
                  onChange={handleChange}
                />
              )
          )
        )}

        <StyledSearchInput
          placeholder="Ex: structuring a scaling team, growing in your career"
          name="searchQuery"
          value={values.searchQuery}
          onChange={handleChange}
        />

        <h3 className="all-challenges-title mb1">Most popular challenges</h3>
        {challenges.map(
          challenge =>
            challenge.title.toLowerCase().includes(values.searchQuery.toLowerCase()) &&
            !values.challenges[challenge.id] && (
              <StyledDeselectedCheckbox
                key={challenge.id}
                name={`challenges.${challenge.id}`}
                color="primary"
                label={challenge.title}
                isChecked={false}
                onChange={handleChange}
              />
            )
        )}
        <ButtonContainer>
          <Button className="back-button" onClick={this.handlePrevious}>
            Previous
          </Button>

          <Button
            className="save-button"
            disabled={!isValid}
            onClick={handleSubmit}
            type="submit"
            variant="primary"
          >
            Continue
          </Button>
        </ButtonContainer>
      </>
    );
  };

  render() {
    const {
      currentPageIndex,
      totalPages,
      user: { persona },
    } = this.props;
    return (
      <OnboardingLayout
        currentPage={currentPageIndex}
        totalPages={totalPages}
        image={CommonChallengesImage}
        title="Choosing the most relevant challenges"
        description="We pre-selected some common challenges for you according to your profile - feel free to add/modify any using the search bar and the list of most popular common challenges."
        caption="about myself"
      >
        <Formik
          initialValues={{
            challenges: persona
              ? persona.challenges.reduce(
                  (acc, challenge) => ({ ...acc, [challenge.id]: true }),
                  {}
                )
              : [],
            searchQuery: '',
          }}
          isInitialValid={Boolean(persona && persona.challenges.length >= 3)}
          validate={this.handleValidate}
          onSubmit={this.handleSubmit}
        >
          {this.handleRenderForm}
        </Formik>
      </OnboardingLayout>
    );
  }
}

Topics.propTypes = {
  challenges: PropTypes.array.isRequired,
  currentPageIndex: PropTypes.number.isRequired,
  history: PropTypes.object.isRequired,
  isLoading: PropTypes.bool.isRequired,
  location: PropTypes.object.isRequired,
  nextPage: PropTypes.string.isRequired,
  onFetchChallenges: PropTypes.func.isRequired,
  onUpdateMentee: PropTypes.func.isRequired,
  totalPages: PropTypes.number.isRequired,
  user: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  return {
    user: getUser(state),
    challenges: getOnboardingChallenges(state),
    isLoading: isActionLoading(state, updateMentee),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    onFetchChallenges: () => dispatch(fetchChallenges()),
    onUpdateMentee: (menteeId, data, redirectPath) =>
      dispatch(updateMentee({ menteeId, ...data, redirectPath, markAsOnboarded: true })),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Topics);
