import React, { Component } from 'react';
import {
  TIME_SLOT_10AM_12PM,
  TIME_SLOT_12AM_6AM,
  TIME_SLOT_12PM_2PM,
  TIME_SLOT_2PM_4PM,
  TIME_SLOT_4PM_6PM,
  TIME_SLOT_6AM_8AM,
  TIME_SLOT_6PM_12AM,
  TIME_SLOT_8AM_10AM,
} from 'constants/TimeSlots';
import { getDayAndTimeSlotFromDate, getTimeSlotFromDayAndTime } from 'utils/time';

import TimeSlotImage from 'images/timeslots.svg';
import PropTypes from 'prop-types';
import SelectInput from 'components/shared/SelectInput';
import styled from 'styled-components';
import OnboardingLayout from 'components/shared/OnboardingLayout';
import { connect } from 'react-redux';
import { getUserTimeZone, getUser, isActionLoading } from 'selectors';
import { updateMentee } from 'ducks';

const weekDays = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];

const weekDayChoices = weekDays.map(weekDay => ({
  label: weekDay,
  value: weekDay,
}));

const availableTimeSlots = [
  TIME_SLOT_12AM_6AM,
  TIME_SLOT_6AM_8AM,
  TIME_SLOT_8AM_10AM,
  TIME_SLOT_10AM_12PM,
  TIME_SLOT_12PM_2PM,
  TIME_SLOT_2PM_4PM,
  TIME_SLOT_4PM_6PM,
  TIME_SLOT_6PM_12AM,
];

const availableTimeSlotChoices = availableTimeSlots.map(timeSlot => ({
  label: timeSlot,
  value: timeSlot,
}));

const TimeSlotRowWrapper = styled.div`
  margin: 1.5rem 0;
  display: grid;
  grid-template-columns: 2rem 1fr;
  grid-column-gap: 1rem;
`;

const TimeSlotRow = styled.div`
  display: grid;
  grid-template-rows: 1fr 1fr;
  grid-row-gap: 1rem;

  @media only screen and (min-width: 1025px) {
    grid-template-rows: auto;
    grid-template-columns: 1fr 1fr;
    grid-column-gap: 1rem;
  }
`;

const Label = styled.strong`
  font-size: 3rem;
`;
class TimeSlots extends Component {
  constructor(props) {
    super(props);
    const {
      user: { firstTimeSlot, secondTimeSlot },
    } = props;
    this.state = {
      firstTimeSlot: firstTimeSlot ? getDayAndTimeSlotFromDate(firstTimeSlot) : {},
      secondTimeSlot: secondTimeSlot ? getDayAndTimeSlotFromDate(secondTimeSlot) : {},
    };
  }

  handlePrevious = () => {
    const { history } = this.props;
    history.goBack();
  };

  handleSave = () => {
    const {
      location: { pathname },
      nextPage,
      user: { menteeId },
      onUpdateMentee,
      timeZone,
    } = this.props;
    const { firstTimeSlot, secondTimeSlot } = this.state;
    onUpdateMentee(
      menteeId,
      {
        onboardingStep: pathname,
        // Only keep the valid choice from the user
        firstTimeSlot: getTimeSlotFromDayAndTime(
          firstTimeSlot.day,
          firstTimeSlot.timeSlot,
          timeZone
        ),
        secondTimeSlot: getTimeSlotFromDayAndTime(
          secondTimeSlot.day,
          secondTimeSlot.timeSlot,
          timeZone
        ),
      },
      nextPage
    );
  };

  handleChangeFirstTimeSlotDay = (field, event) => {
    const { firstTimeSlot } = this.state;
    firstTimeSlot.day = event.value;
    this.setState({ firstTimeSlot });
  };

  handleChangeFirstTimeSlotTime = (field, event) => {
    const { firstTimeSlot } = this.state;
    firstTimeSlot.timeSlot = event.value;
    this.setState({ firstTimeSlot });
  };

  handleChangeSecondTimeSlotDay = (field, event) => {
    const { secondTimeSlot } = this.state;
    secondTimeSlot.day = event.value;
    this.setState({ secondTimeSlot });
  };

  handleChangeSecondTimeSlotTime = (field, event) => {
    const { secondTimeSlot } = this.state;
    secondTimeSlot.timeSlot = event.value;
    this.setState({ secondTimeSlot });
  };

  validate() {
    const { isLoading } = this.props;
    const { firstTimeSlot, secondTimeSlot } = this.state;
    if (isLoading) {
      return false;
    }
    return Boolean(
      firstTimeSlot.day && firstTimeSlot.timeSlot && secondTimeSlot.day && secondTimeSlot.timeSlot
    );
  }

  render() {
    const { currentPageIndex, totalPages } = this.props;
    const { firstTimeSlot, secondTimeSlot } = this.state;
    return (
      <OnboardingLayout
        caption="About Myself"
        currentPage={currentPageIndex}
        description="Which time slots would be best for you to spend time on your own development? (Time slots are in your local time zone)"
        image={TimeSlotImage}
        isNextEnabled={this.validate()}
        onNext={this.handleSave}
        onPrevious={this.handlePrevious}
        title="Your weekly availability"
        totalPages={totalPages}
      >
        <TimeSlotRowWrapper>
          <Label>1</Label>
          <TimeSlotRow>
            <SelectInput
              className="day-select"
              value={
                firstTimeSlot.day
                  ? weekDayChoices.find(
                      weekDay => weekDay.value.toUpperCase() === firstTimeSlot.day.toUpperCase()
                    )
                  : ''
              }
              onChange={this.handleChangeFirstTimeSlotDay}
              placeholder="Day of week"
              options={weekDayChoices}
            />
            <SelectInput
              className="time-select"
              value={
                firstTimeSlot.timeSlot
                  ? availableTimeSlotChoices.find(choice => choice.value === firstTimeSlot.timeSlot)
                  : ''
              }
              onChange={this.handleChangeFirstTimeSlotTime}
              placeholder="Time slot"
              options={availableTimeSlotChoices}
            />
          </TimeSlotRow>
        </TimeSlotRowWrapper>

        <TimeSlotRowWrapper>
          <Label>2</Label>
          <TimeSlotRow>
            <SelectInput
              className="day-select"
              value={
                secondTimeSlot.day
                  ? weekDayChoices.find(
                      weekDay => weekDay.value.toUpperCase() === secondTimeSlot.day.toUpperCase()
                    )
                  : ''
              }
              onChange={this.handleChangeSecondTimeSlotDay}
              placeholder="Day of week"
              options={weekDayChoices}
            />

            <SelectInput
              className="time-select"
              value={
                secondTimeSlot.timeSlot
                  ? availableTimeSlotChoices.find(
                      choice => choice.value === secondTimeSlot.timeSlot
                    )
                  : ''
              }
              onChange={this.handleChangeSecondTimeSlotTime}
              placeholder="Time slot"
              options={availableTimeSlotChoices}
            />
          </TimeSlotRow>
        </TimeSlotRowWrapper>
      </OnboardingLayout>
    );
  }
}

TimeSlots.propTypes = {
  currentPageIndex: PropTypes.number.isRequired,
  history: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
  nextPage: PropTypes.string.isRequired,
  isLoading: PropTypes.bool.isRequired,
  onUpdateMentee: PropTypes.func.isRequired,
  totalPages: PropTypes.number.isRequired,
  user: PropTypes.object.isRequired,
  timeZone: PropTypes.string.isRequired,
};

function mapStateToProps(state) {
  return {
    user: getUser(state),
    timeZone: getUserTimeZone(state),
    isLoading: isActionLoading(state, updateMentee),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    onUpdateMentee: (menteeId, data, redirectPath) =>
      dispatch(updateMentee({ menteeId, ...data, redirectPath, markAsOnboarded: true })),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TimeSlots);
