import React, { Component } from 'react';
import { fetchHierarchies, fetchSlackUsers, updateMentee } from 'ducks';
import {
  getHierarchies,
  getIsProductManager,
  getSlackUsers,
  getUser,
  isActionLoading,
} from 'selectors';

import AboutImage from 'images/about.svg';
import { MenuItem } from '@material-ui/core';
import OnboardingLayout from 'components/shared/OnboardingLayout';
import Pagination from 'components/shared/Pagination';
import PropTypes from 'prop-types';
import Select from 'components/shared/Select';
import TextInput from 'components/shared/TextInput';
import { connect } from 'react-redux';
import moment from 'moment-timezone';
import styled from 'styled-components';

const StyledAboutYou = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-column-gap: 2rem;
  grid-row-gap: 1rem;
  padding: 0 1rem 0 1rem;

  .header,
  ${Pagination} {
    grid-column: 1/3;
    text-align: center;
  }

  .header:first-of-type {
    margin: 0 0 1rem;
  }

  ${TextInput} {
    grid-column: 1/3;

    input {
      margin-bottom: 1rem;
      padding-right: 1rem;
    }
  }

  ${Select} {
    width: 100%;
  }

  .select-title {
    margin-bottom: 1rem;
  }

  @media only screen and (min-width: 768px) {
    padding: 0;

    ${TextInput} {
      grid-column: auto/auto;
    }
  }
`;

class AboutYou extends Component {
  constructor(props) {
    super(props);
    const {
      user: {
        companyName,
        hierarchy,
        directReportsNb,
        linkedin,
        location,
        sizeOfTeam,
        title,
        yearsInCurrentRoleNb,
        yearsInManagementNb,
      },
    } = props;
    this.state = {
      companyName,
      directReportsNb,
      linkedin,
      location,
      sizeOfTeam,
      slackUserId: undefined,
      title,
      yearsInCurrentRoleNb,
      yearsInManagementNb,
      hierarchy,
      errors: {},
    };
  }

  componentDidMount() {
    const { onFetchHierarchies, onFetchUserSlackIds, onUpdateMentee, user } = this.props;
    onFetchUserSlackIds();
    onFetchHierarchies();
    if (!user.onboardingSteps.includes('/create-account')) {
      onUpdateMentee(user.menteeId, { onboardingStep: '/create-account' });
    }
  }

  handleChange = event => {
    const {
      target: { name, value },
    } = event;
    const { errors } = this.state;

    if (name === 'linkedin') {
      if (value && !value.startsWith('https://')) {
        errors.linkedin = 'The URL should start with "https://"';
      } else {
        delete errors.linkedin;
      }
    }
    this.setState({ [name]: value, errors });
  };

  handleSave = () => {
    const {
      location: { pathname },
      nextPage,
      user: { menteeId },
      onUpdateMentee,
    } = this.props;
    const {
      directReportsNb,
      linkedin,
      location,
      sizeOfTeam,
      slackUserId,
      title,
      yearsInCurrentRoleNb,
      yearsInManagementNb,
      hierarchy,
    } = this.state;
    onUpdateMentee(
      menteeId,
      {
        directReportsNb,
        timeZone: moment.tz.guess(),
        linkedin,
        location,
        onboardingStep: pathname,
        sizeOfTeam,
        slackUserId,
        title,
        yearsInCurrentRoleNb,
        yearsInManagementNb,
        hierarchy,
      },
      nextPage
    );
  };

  validate() {
    const { isLoading, isProductManager, user } = this.props;
    const {
      directReportsNb,
      linkedin,
      location,
      sizeOfTeam,
      slackUserId,
      title,
      yearsInCurrentRoleNb,
      yearsInManagementNb,
      hierarchy,
      errors,
    } = this.state;
    return (
      !isLoading &&
      linkedin &&
      (isProductManager || directReportsNb) &&
      location &&
      sizeOfTeam &&
      (user.slackUserId || slackUserId) &&
      title &&
      yearsInCurrentRoleNb &&
      yearsInManagementNb &&
      hierarchy &&
      Object.keys(errors).length === 0
    );
  }

  render() {
    const {
      currentPageIndex,
      hierarchies,
      isProductManager,
      slackUsers,
      totalPages,
      user,
    } = this.props;
    const {
      companyName,
      directReportsNb,
      errors,
      hierarchy,
      linkedin,
      location,
      sizeOfTeam,
      slackUserId,
      title,
      yearsInCurrentRoleNb,
      yearsInManagementNb,
    } = this.state;
    return (
      <OnboardingLayout
        contentWidth={45}
        currentPage={currentPageIndex}
        totalPages={totalPages}
        image={AboutImage}
        title="Let's confirm some basic information about you!"
        description=""
        caption="about myself"
        isNextEnabled={this.validate()}
        onNext={this.handleSave}
      >
        <StyledAboutYou>
          {!user.slackUserId && (
            <>
              <h1 className="header">What is your Slack username?</h1>
              <div>
                <h4 className="select-title">Slack username</h4>
                <Select
                  name="slackUserId"
                  value={slackUserId || ''}
                  onChange={this.handleChange}
                  placeholder="Slack ID"
                >
                  {slackUsers
                    .filter(slackUser => slackUser.fullName && slackUser.fullName.trim())
                    .sort((slackUser1, slackUser2) =>
                      slackUser1.fullName.localeCompare(slackUser2.fullName)
                    )
                    .map(slackUser => (
                      <MenuItem key={slackUser.id} value={slackUser.id}>
                        {`${slackUser.fullName} (${slackUser.email})`}
                      </MenuItem>
                    ))}
                  {slackUsers
                    .filter(slackUser => !slackUser.fullName || !slackUser.fullName.trim())
                    .sort((slackUser1, slackUser2) =>
                      slackUser1.email.localeCompare(slackUser2.email)
                    )
                    .map(slackUser => (
                      <MenuItem key={slackUser.id} value={slackUser.id}>
                        {slackUser.email}
                      </MenuItem>
                    ))}
                </Select>
              </div>
            </>
          )}

          <TextInput
            disabled
            label="Company name"
            name="companyName"
            onChange={this.handleChange}
            placeholder="Tell us the name of your company"
            type="text"
            value={companyName || ''}
          />

          <TextInput
            type="text"
            name="title"
            label="Job title"
            placeholder="Tell us your job title"
            value={title || ''}
            onChange={this.handleChange}
          />

          <TextInput
            type="text"
            name="location"
            label="City / country"
            placeholder="Tell us where you work from"
            value={location || ''}
            onChange={this.handleChange}
          />

          <TextInput
            type="text"
            name="linkedin"
            label="LinkedIn profile link"
            placeholder="Paste your profile link here"
            value={linkedin || ''}
            error={errors && errors.linkedin}
            onChange={this.handleChange}
          />

          <TextInput
            className={isProductManager ? 'double-height-label' : ''}
            type="number"
            name="directReportsNb"
            label="How many people report directly to you?"
            placeholder="Enter the total number of people"
            value={String(directReportsNb || '')}
            onChange={this.handleChange}
          />

          <TextInput
            type="number"
            name="sizeOfTeam"
            label={
              isProductManager
                ? 'How many people are in your sphere of influence? (engineers, designers...)'
                : 'How many people do you manage in total?'
            }
            placeholder="Enter the total number of people"
            value={String(sizeOfTeam || '')}
            onChange={this.handleChange}
          />

          <TextInput
            type="number"
            name="yearsInManagementNb"
            label="How many years have you been a manager, including at your previous companies?"
            placeholder="Enter the number of years"
            value={String(yearsInManagementNb || '')}
            onChange={this.handleChange}
          />

          <TextInput
            type="number"
            name="yearsInCurrentRoleNb"
            label="How many years have you been in your current role?"
            placeholder="Enter the number of years"
            value={String(yearsInCurrentRoleNb || '')}
            onChange={this.handleChange}
          />

          <div>
            <h4 className="select-title">What is your role?</h4>
            <Select
              name="hierarchy"
              value={hierarchy || ''}
              onChange={this.handleChange}
              placeholder="Hierarchy"
            >
              {hierarchies.map(hierarchyItem => (
                <MenuItem key={hierarchyItem} value={hierarchyItem}>
                  {hierarchyItem}
                </MenuItem>
              ))}
            </Select>
          </div>
        </StyledAboutYou>
      </OnboardingLayout>
    );
  }
}

AboutYou.propTypes = {
  currentPageIndex: PropTypes.number.isRequired,
  hierarchies: PropTypes.array.isRequired,
  isLoading: PropTypes.bool.isRequired,
  isProductManager: PropTypes.bool.isRequired,
  location: PropTypes.object.isRequired,
  nextPage: PropTypes.string.isRequired,
  onFetchHierarchies: PropTypes.func.isRequired,
  onFetchUserSlackIds: PropTypes.func.isRequired,
  onUpdateMentee: PropTypes.func.isRequired,
  slackUsers: PropTypes.array.isRequired,
  totalPages: PropTypes.number.isRequired,
  user: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  return {
    hierarchies: getHierarchies(state),
    isLoading: isActionLoading(state, updateMentee),
    isProductManager: getIsProductManager(state),
    slackUsers: getSlackUsers(state),
    user: getUser(state),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    onFetchUserSlackIds: () => dispatch(fetchSlackUsers()),
    onFetchHierarchies: () => dispatch(fetchHierarchies()),
    onUpdateMentee: (menteeId, data, redirectPath) =>
      dispatch(updateMentee({ menteeId, ...data, redirectPath })),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AboutYou);
