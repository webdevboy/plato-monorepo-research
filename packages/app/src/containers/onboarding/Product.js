import * as allProductTypes from 'constants/ProductTypes';

import React, { Component } from 'react';

import Checkbox from 'components/shared/Checkbox';
import Pagination from 'components/shared/Pagination';
import PropTypes from 'prop-types';
import OnboardingLayout from 'components/shared/OnboardingLayout';
import ProductImage from 'images/product.svg';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { getIndustryTags, getUser, isActionLoading } from 'selectors';
import { fetchIndustryTags, updateMentee } from 'ducks';

const Title = styled.h1`
  margin-top: 5rem;
  padding: 0 5rem;
  font-size: 2rem;
  color: ${props => props.theme.palette.black};
  grid-area: title;
  text-align: center;
`;

const Description = styled.div`
  font-size: 1.6rem;
  grid-area: description;
  text-align: center;
  margin-bottom: 2rem;
  line-height: 150%;
  padding: 0 2rem;

  @media only screen and (min-width: 1025px) {
    padding: 0;
  }
`;

const ListWrapper = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  margin: 0 6%;
  @media only screen and (min-width: 1025px) {
    margin: 0 20%;
  }
`;

class Product extends Component {
  constructor(props) {
    super(props);
    const {
      user: { additionalData, industries },
    } = props;
    this.state = {
      additionalData: additionalData || {},
      industryIds: industries.map(industry => industry.id),
    };
  }

  componentDidMount() {
    const { onFetchIndustryTags } = this.props;
    onFetchIndustryTags();
  }

  getProductTypeLabel(productType) {
    switch (productType) {
      case allProductTypes.ANDROID_APP:
        return 'Android app';
      case allProductTypes.API:
        return 'API';
      case allProductTypes.HARDWARE_PRODUCT:
        return 'Hardware product';
      case allProductTypes.IOS_APP:
        return 'IOS app';
      case allProductTypes.MARKETPLACE:
        return 'Marketplace';
      case allProductTypes.OTHER:
        return 'Other';
      case allProductTypes.SAAS:
        return 'SAAS';
      case allProductTypes.SEARCH_ENGINE:
        return 'Search engine';
      case allProductTypes.WEBSITE:
        return 'Website';
      default:
        return null;
    }
  }

  handleSelect = (industryId, checked) => {
    const { industryIds } = this.state;
    this.setState({
      industryIds: checked
        ? industryIds.concat(industryId)
        : industryIds.filter(id => id !== industryId),
    });
  };

  handleSave = () => {
    const {
      location: { pathname },
      nextPage,
      onUpdateMentee,
      user,
    } = this.props;
    const { industryIds, additionalData } = this.state;
    onUpdateMentee(
      user.menteeId,
      {
        additionalData,
        onboardingStep: pathname,
        industryIds: Array.from(new Set(industryIds)),
      },
      nextPage
    );
  };

  handlePrevious = () => {
    const { history } = this.props;
    history.goBack();
  };

  handleChangeProductTypes = (name, checked) => {
    const { additionalData } = this.state;
    if (!additionalData.productTypes) {
      additionalData.productTypes = [];
    }
    this.setState({
      additionalData: Object.assign(additionalData, {
        productTypes: checked
          ? [...additionalData.productTypes, name]
          : additionalData.productTypes.filter(productType => productType !== name),
      }),
    });
  };

  validate() {
    const { isLoading } = this.props;
    const { additionalData } = this.state;
    if (isLoading) {
      return false;
    }
    return additionalData.productTypes && additionalData.productTypes.length > 0;
  }

  render() {
    const { currentPageIndex, industries, totalPages, isLoading } = this.props;
    const { additionalData, industryIds } = this.state;
    return (
      <OnboardingLayout
        currentPage={currentPageIndex}
        totalPages={totalPages}
        image={ProductImage}
        title="What type of product are you working on?"
        description="Choose as many as you like"
        pagination={
          <Pagination caption="about myself" currentPage={currentPageIndex} total={totalPages} />
        }
        onPrevious={this.handlePrevious}
        onNext={this.handleSave}
        isNextEnabled={this.validate()}
      >
        <ListWrapper>
          {Object.keys(allProductTypes).map(productType => (
            <Checkbox
              key={productType}
              name={productType}
              label={this.getProductTypeLabel(productType)}
              checked={
                additionalData.productTypes && additionalData.productTypes.includes(productType)
              }
              onChange={this.handleChangeProductTypes}
            />
          ))}
        </ListWrapper>

        <Title>In which industry is your company in?</Title>
        <Description>Choose as many as you like</Description>

        <ListWrapper>
          {industries.length === 0 && isLoading
            ? null
            : industries.map(industry => (
                <Checkbox
                  key={industry.id}
                  name={industry.id}
                  color="primary"
                  label={industry.name}
                  checked={industryIds.includes(industry.id)}
                  onChange={this.handleSelect}
                />
              ))}
        </ListWrapper>
      </OnboardingLayout>
    );
  }
}

Product.propTypes = {
  currentPageIndex: PropTypes.number.isRequired,
  industries: PropTypes.array.isRequired,
  isLoading: PropTypes.bool.isRequired,
  nextPage: PropTypes.string.isRequired,
  onFetchIndustryTags: PropTypes.func.isRequired,
  onUpdateMentee: PropTypes.func.isRequired,
  totalPages: PropTypes.number.isRequired,
  user: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  return {
    user: getUser(state),
    industries: getIndustryTags(state),
    isLoading: isActionLoading(state, updateMentee),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    onUpdateMentee: (menteeId, data, redirectPath) =>
      dispatch(updateMentee({ menteeId, ...data, redirectPath })),
    onFetchIndustryTags: () => dispatch(fetchIndustryTags()),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Product);
