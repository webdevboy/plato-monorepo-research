import React, { Component } from 'react';

import OnboardingLayout from 'components/shared/OnboardingLayout';
import OnboardingProgressListItem from 'components/shared/OnboardingProgressListItem';

class Index extends Component {
  render() {
    const { nextStepPath, flow, getPath } = this.props;

    const isAboutYouStepCompleted = flow.aboutYou.every(page => getPath(page) !== nextStepPath);
    const isTrackStepCompleted =
      isAboutYouStepCompleted && flow.track.every(page => getPath(page) !== nextStepPath);
    const isMentorStepCompleted =
      isTrackStepCompleted && flow.mentor.every(page => getPath(page) !== nextStepPath);

    return (
      <OnboardingLayout title="Onboarding Progress">
        <ul>
          <OnboardingProgressListItem
            title="About myself"
            description="This section enables us to learn a bit more about you and your challenges"
            isCompleted={isAboutYouStepCompleted}
            path={!isAboutYouStepCompleted && nextStepPath}
          />
          <OnboardingProgressListItem
            title="Choose a track"
            description="Find a group of peers to discuss in an interactive setting"
            isCompleted={isTrackStepCompleted}
            path={isAboutYouStepCompleted && !isTrackStepCompleted && nextStepPath}
          />
          <OnboardingProgressListItem
            title="Choose a mentor"
            description="Select some potential fits among a list of mentors curated for your first 1-to-1 calls"
            isCompleted={isMentorStepCompleted}
            path={
              isAboutYouStepCompleted &&
              isTrackStepCompleted &&
              !isMentorStepCompleted &&
              nextStepPath
            }
          />
          <OnboardingProgressListItem
            title="Book a call with the Plato team"
            description="We want to check up on your experience after a few weeks"
            isCompleted={false}
            path={
              isAboutYouStepCompleted &&
              isTrackStepCompleted &&
              isMentorStepCompleted &&
              nextStepPath
            }
          />
        </ul>
      </OnboardingLayout>
    );
  }
}

export default Index;
