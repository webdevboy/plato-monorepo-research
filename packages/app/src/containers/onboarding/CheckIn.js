import React, { Component } from 'react';
import { getCalendlyCheckInUrl, validateCalendlyCallback } from 'utils/calendly';

import { DAYS_BEFORE_ONBOARDING_CHECK_IN_CALL } from 'constants/Onboarding';
import Helmet from 'react-helmet';
import TimeSlotImage from 'images/timeslots.svg';
import Pagination from 'components/shared/Pagination';
import PropTypes from 'prop-types';
import moment from 'moment';
import queryString from 'query-string';
import styled from 'styled-components';
import OnboardingLayout from 'components/shared/OnboardingLayout';
import { connect } from 'react-redux';
import { getUser } from 'selectors';
import { updateMentee } from 'ducks';

const CalendlyFrame = styled.div`
  width: 100%;
  height: 80rem;
  border: 1px solid ${({ theme }) => theme.palette.geyser};
  margin-bottom: 2rem;
`;

class CheckIn extends Component {
  componentDidMount() {
    const {
      location: { pathname, search },
      user: { menteeId },
      onUpdateMentee,
    } = this.props;
    // Check if the component has been mounted out of the Calendly redirect URL
    const parsedQuery = queryString.parse(search);
    if (validateCalendlyCallback(parsedQuery)) {
      onUpdateMentee(
        menteeId,
        {
          onboardingStep: pathname,
        },
        '/challenges'
      );
    }
  }

  render() {
    const {
      currentPageIndex,
      totalPages,
      user: { uuid, email, fullName },
    } = this.props;
    // Calendly displays dates for the 7 days after the given date
    const date = moment().add(DAYS_BEFORE_ONBOARDING_CHECK_IN_CALL - 7, 'days');
    const checkInUrl = getCalendlyCheckInUrl(uuid, fullName, email, date);
    return (
      <OnboardingLayout
        currentPage={currentPageIndex}
        totalPages={totalPages}
        image={TimeSlotImage}
        title="Let's schedule a call in a few weeks!"
        description="The purpose of this call is to make sure your experience and first few calls with our curated mentors went well"
        pagination={<Pagination currentPage={currentPageIndex} total={totalPages} />}
      >
        <Helmet>
          <script src="https://assets.calendly.com/assets/external/widget.js" />
        </Helmet>
        <CalendlyFrame className="calendly-inline-widget" data-url={checkInUrl} />
      </OnboardingLayout>
    );
  }
}

CheckIn.propTypes = {
  currentPageIndex: PropTypes.number.isRequired,
  location: PropTypes.object.isRequired,
  onUpdateMentee: PropTypes.func.isRequired,
  totalPages: PropTypes.number.isRequired,
  user: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  return {
    user: getUser(state),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    onUpdateMentee: (menteeId, data, redirectPath) =>
      dispatch(updateMentee({ menteeId, ...data, redirectPath })),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CheckIn);
