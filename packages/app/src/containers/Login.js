import { Link, withRouter } from 'react-router-dom';
import React, { Component } from 'react';

import BrandedPage from 'components/shared/BrandedPage';
import Button from 'components/shared/Button';
import Card from 'components/shared/Card';
import Checkbox from 'components/shared/Checkbox';
import PropTypes from 'prop-types';
import TextInput from 'components/shared/TextInput';
import { connect } from 'react-redux';
import { isActionLoading } from 'selectors';
import { login } from 'ducks';
import styled from 'styled-components';

const Header = styled.h1`
  font-size: 3.4rem;
  color: ${props => props.theme.palette.white};
  margin-bottom: 3rem;
`;

const FormCard = styled(Card)`
  position: relative;
  width: 90%;
  padding: 3rem;

  form {
    width: 100%;
  }

  .actions {
    display: flex;
    justify-content: space-between;
    margin-top: 2rem;
    align-items: center;
  }

  @media (min-width: 660px) {
    width: 660px;
    padding: 5rem 8rem;
  }
`;

class Login extends Component {
  state = {
    email: '',
    password: '',
    keepMeLoggedIn: true,
  };

  handleEmailChange = event => {
    this.setState({
      email: event.target.value,
    });
  };

  handlePasswordChange = event => {
    this.setState({
      password: event.target.value,
    });
  };

  handleToggleKeepMeLoggedIn = () => {
    this.setState(prevState => ({
      keepMeLoggedIn: !prevState.keepMeLoggedIn,
    }));
  };

  handleLogin = event => {
    const { onLogin } = this.props;
    const { email, password, keepMeLoggedIn } = this.state;
    event.preventDefault();
    onLogin(email, password, keepMeLoggedIn);
  };

  render() {
    const { isLoading } = this.props;
    const { email, password, keepMeLoggedIn } = this.state;
    return (
      <BrandedPage>
        <Header>Log into your account</Header>
        <FormCard>
          <form onSubmit={this.handleLogin}>
            <TextInput
              type="text"
              value={email}
              label="Your Email Address"
              onChange={this.handleEmailChange}
              placeholder="email@example.com"
            />
            <TextInput
              type="password"
              value={password}
              label="Password"
              onChange={this.handlePasswordChange}
              placeholder="Your password"
            />
            <Checkbox
              checked={keepMeLoggedIn}
              label="Keep me logged in"
              onChange={this.handleToggleKeepMeLoggedIn}
            />

            <div className="actions">
              <Link to="/forgot-password">
                <Button variant="primary-link" type="button">
                  Forgot Password?
                </Button>
              </Link>
              <Button
                variant="primary"
                type="submit"
                disabled={isLoading || email.length === 0 || password.length === 0}
              >
                Log in
              </Button>
            </div>
          </form>
        </FormCard>
      </BrandedPage>
    );
  }
}

Login.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  onLogin: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
  return {
    isLoading: isActionLoading(state, login),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    onLogin: (email, password, eternal) => dispatch(login({ email, password, eternal })),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(Login));
