import queryString from 'query-string';

class ApiService {
  constructor({ serverUrl }) {
    this.serverUrl = serverUrl;
  }

  buildApiUri(path, queryParams) {
    const queryParamsStr = queryParams ? `?${queryString.stringify(queryParams)}` : '';

    return `${this.serverUrl}${path}${queryParamsStr}`;
  }

  buildRequestConfig(config = {}) {
    return config;
  }

  handleRequest(request) {
    return request;
  }

  get(path, queryParams) {
    return this.handleRequest(
      fetch(
        this.buildApiUri(path, queryParams),
        this.buildRequestConfig({
          method: 'GET',
        })
      )
    );
  }

  post(path, data = {}) {
    return this.handleRequest(
      fetch(
        this.buildApiUri(path),
        this.buildRequestConfig({
          method: 'POST',
          body: JSON.stringify(data),
        })
      )
    );
  }

  put(path, data = {}, isFormData = false) {
    return this.handleRequest(
      fetch(
        this.buildApiUri(path),
        this.buildRequestConfig(
          {
            method: 'PUT',
            body: isFormData ? data : JSON.stringify(data),
          },
          isFormData
        )
      )
    );
  }
}

export default ApiService;
