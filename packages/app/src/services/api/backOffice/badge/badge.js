import BackOfficeApi from '../backOfficeApi';

export default {
  getBadges: ({ limit, cursor, sortField, descending, constraints }) =>
    BackOfficeApi.get('/badge', {
      limit,
      cursor,
      sortField,
      descending,
      constraints,
    }).then(({ response: { results } }) => results),
};
