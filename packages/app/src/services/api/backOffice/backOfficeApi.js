import ApiService from '../api';

class BackOfficeApiService extends ApiService {
  buildRequestConfig(config) {
    const headers = {
      'Content-Type': 'application/json',
    };

    return super.buildRequestConfig({
      headers,
      ...config,
    });
  }

  async handleRequest(request) {
    const response = await request;
    return response.json();
  }
}

export default new BackOfficeApiService({
  serverUrl: process.env.REACT_APP_BACK_OFFICE_API_URI,
});
