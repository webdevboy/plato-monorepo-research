import BackOfficeApi from '../backOfficeApi';

export default {
  getAverageGrades: () =>
    BackOfficeApi.get('/averagegrade').then(({ response: { results } }) => results),
};
