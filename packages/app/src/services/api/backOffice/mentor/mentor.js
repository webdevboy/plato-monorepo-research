import BackOfficeApi from '../backOfficeApi';

export default {
  getMentors: ({ limit, cursor, sortField, descending, constraints }) =>
    BackOfficeApi.get('/mentor', {
      limit,
      cursor,
      sortField,
      descending,
      constraints,
    }).then(({ response: { results } }) => results),
};
