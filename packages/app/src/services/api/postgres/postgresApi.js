import ApiService from '../api';

class PostgresApiService extends ApiService {
  buildRequestConfig(config) {
    const headers = {
      'Content-Type': 'application/json',
    };

    return super.buildRequestConfig({
      headers,
      ...config,
    });
  }

  async handleRequest(request) {
    const response = await request;
    return response.json();
  }
}

export default new PostgresApiService({
  serverUrl: process.env.REACT_APP_POSTGRES_API_URI,
});
