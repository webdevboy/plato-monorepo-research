import PostgresApi from '../postgresApi';

export default {
  getGrades: ({ mentorId }) =>
    PostgresApi.get(
      `/_QUERIES/mentorPerformance/get_mentor_performance_grades?id=${mentorId}`
    ).then(result => result[0]),

  getImprovements: ({ mentorId }) =>
    PostgresApi.get(
      `/_QUERIES/mentorPerformance/get_mentor_performance_improvements?id=${mentorId}`
    ).then(result => result[0]),
};
