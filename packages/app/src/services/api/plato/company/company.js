import PlatoApi from '../platoApi';

const CompanyService = {
  getCompany: companyId => PlatoApi.get(`/company/${companyId}`).then(({ result }) => result),
};

export default CompanyService;
