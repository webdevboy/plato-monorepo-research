import PlatoApi from '../platoApi';

const RequestService = {
  createRequest: ({ menteeId, shareType, briefingOneLiner, briefingDescription, briefId }) =>
    PlatoApi.post('/request', {
      menteeId,
      shareType,
      briefingOneLiner,
      briefingDescription,
      briefId,
    }),

  book: ({ requestId, eventId }) =>
    PlatoApi.put(`/request/${requestId}/bookSlot`, {
      id: requestId,
      eventId,
    }),

  matchAndPotentialBook: ({ requestId, mentorId, storyId, eventId }) =>
    PlatoApi.put('/request/matchAndPotentialBook', {
      id: requestId,
      mentorId,
      storyId,
      eventId,
    }),

  updateBrief: ({ requestId, oneLiner, description }) =>
    PlatoApi.put(`/request/${requestId}/brief`, {
      id: requestId,
      briefingOneLiner: oneLiner,
      briefingDescription: description,
    }),

  declineMentor: ({ requestId, user }) =>
    PlatoApi.put(`/request/${requestId}/decline/mentor?user=${user}`),

  getMentorMenteeRequests: ({ mentorId, menteeId }) =>
    PlatoApi.get(
      `/request/mentor/${encodeURIComponent(mentorId)}/mentee/${encodeURIComponent(menteeId)}`
    ),

  updateKeyActionsStatus: ({ requestId, status }) =>
    PlatoApi.put(`/request/${requestId}/keyActionsStatus`, {
      id: requestId,
      keyActionsStatus: status,
    }),
};

export default RequestService;
