import PlatoApi from '../platoApi';

const EventService = {
  decline: ({ eventId, user }) => PlatoApi.put(`/event/${eventId}/decline?user=${user}`),
};

export default EventService;
