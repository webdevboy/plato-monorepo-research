import PlatoApi from '../platoApi';

const AttendanceService = {
  createAttendance: (menteeId, amaId, question, fromOperator = false) =>
    PlatoApi.post('/attendance', {
      menteeId,
      amaId,
      question,
      fromOperator,
    }),

  updateAttendance: (attendanceId, present, fromOperator = false) =>
    PlatoApi.put(`/attendance/${attendanceId}/present`, {
      id: attendanceId,
      present,
      fromOperator,
    }).then(({ result }) => result),
};

export default AttendanceService;
