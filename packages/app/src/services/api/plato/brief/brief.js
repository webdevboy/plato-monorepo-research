import PlatoApi from '../platoApi';

const BriefService = {
  getBrief: briefId => PlatoApi.get(`/brief/${briefId}`).then(({ result }) => result),

  updateBrief: (
    menteeEmail,
    briefId,
    briefingOneLiner,
    briefingDescription,
    tagIds,
    challengeIds,
    shareType
  ) =>
    PlatoApi.put(`/brief/${briefId}?user=${menteeEmail}`, {
      briefingOneLiner,
      briefingDescription,
      tagIds,
      challengeIds,
      shareType,
      id: briefId,
    }),

  createBrief: ({ menteeId, briefingOneLiner, briefingDescription, shareType }) =>
    PlatoApi.post('/brief', {
      menteeId,
      briefingOneLiner,
      briefingDescription,
      shareType,
    }),

  reopenBrief: briefId => PlatoApi.put(`/brief/${briefId}/reopen`),

  closeBrief: briefId => PlatoApi.put(`/brief/${briefId}/doNotMatch`),

  getBriefRequests: briefId =>
    PlatoApi.get(`/brief/${briefId}/requests`).then(({ result }) => result),
};

export default BriefService;
