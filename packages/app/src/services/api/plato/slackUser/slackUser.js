import PlatoApi from '../platoApi';

const SlackUserService = {
  getSlackUsers: () => PlatoApi.get('/slackUser/sameTeam'),
};

export default SlackUserService;
