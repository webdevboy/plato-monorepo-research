import PlatoApi from '../platoApi';

const AccountService = {
  createAccount: data => PlatoApi.post('/account', data),
};

export default AccountService;
