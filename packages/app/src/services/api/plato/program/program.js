import PlatoApi from '../platoApi';

const ProgramService = {
  createApplication: (programId, data) =>
    PlatoApi.post(`/program/${encodeURIComponent(programId)}/application`, data),
};

export default ProgramService;
