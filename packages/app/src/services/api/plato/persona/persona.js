import PlatoApi from '../platoApi';

const PersonasService = {
  getPersonas: () => PlatoApi.get('/persona'),
};

export default PersonasService;
