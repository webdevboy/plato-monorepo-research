import PlatoApi from '../platoApi';
import axios from 'axios';

const MentorService = {
  updateMentor: ({ mentorId, formData }) =>
    axios({
      method: 'put',
      url: `${process.env.REACT_APP_PLATO_API_URI}/mentor/${mentorId}`,
      headers: {
        Authorization: `Bearer ${localStorage.userToken}`,
        'Content-Type': 'multipart/form-data',
      },
      data: formData,
    }),

  getTags: type => PlatoApi.get('/tag', { type }).then(({ result }) => result),

  getMentor: mentorId => PlatoApi.get(`/mentor/${mentorId}`).then(({ result }) => result),

  getAvailabilities: mentorId =>
    PlatoApi.get(`/mentor/${mentorId}/availabilities`).then(({ result }) => result),

  getStoriesByMentor: mentorId => PlatoApi.get('/story/forMentee', { mentorId }),

  getAmasByMentor: ({ mentorId, scheduled }) =>
    PlatoApi.get(`/mentor/${mentorId}/amas`, { scheduled }),

  getEvents: ({ mentorId, minDate, maxDate, page, size, isDeclined }) =>
    PlatoApi.get(`/mentor/${mentorId}/events`, {
      minDate,
      maxDate,
      page,
      size,
      isDeclined,
    }),

  getAmas: ({
    mentorId,
    hasProgram = false,
    scheduled,
    closed,
    done,
    error,
    minDate,
    maxDate,
    page,
    size,
  }) =>
    PlatoApi.get(`/mentor/${mentorId}/amas`, {
      hasProgram,
      scheduled,
      close: closed,
      done,
      error,
      minDate,
      maxDate,
      page,
      size,
    }),

  getRequests: ({ mentorId, scheduled, closed, done, error, scMentee, scMentor, page, size }) =>
    PlatoApi.get(`/mentor/${mentorId}/requests`, {
      mentorId,
      scheduled,
      close: closed,
      done,
      error,
      scMentee,
      scMentor,
      page,
      size,
    }),

  getStories: mentorId => PlatoApi.get(`/mentor/${mentorId}/stories`).then(({ result }) => result),

  getHierarchies: () => PlatoApi.get('/mentor/hierarchies').then(({ result }) => result),

  getCompanyTypes: () => PlatoApi.get('/mentor/companyTypes').then(({ result }) => result),

  searchForMentee: options => PlatoApi.get('/mentor/forMentee', options),

  getRelationships: mentorId =>
    PlatoApi.get(`/mentor/${mentorId}/relationships`).then(({ result }) => result),

  getRelationshipSlots: mentorId =>
    PlatoApi.get(`/mentor/${mentorId}/firstLtmSlots`, {
      numberOfLtms: 3,
    }).then(({ result }) => result),
};

export default MentorService;
