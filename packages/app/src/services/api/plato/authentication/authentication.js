import PlatoApi from '../platoApi';

const AuthenticationService = {
  login: (username, password, eternal = false) =>
    PlatoApi.post('/login', { username, password, eternal }).then(({ result }) => result),
};

export default AuthenticationService;
