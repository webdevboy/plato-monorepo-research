import PlatoApi from '../platoApi';

const TagService = {
  getIndustries: () => PlatoApi.get('/tag', { type: 'industry' }).then(({ result }) => result),

  getFaces: () => PlatoApi.get('/tag', { type: 'face' }).then(({ result }) => result),

  getTopicsForFilter: () =>
    PlatoApi.get('/tag/forFilter', { type: 'story' }).then(({ result }) => result),
};

export default TagService;
