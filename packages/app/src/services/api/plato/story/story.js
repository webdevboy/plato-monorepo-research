import PlatoApi from '../platoApi';

const StoryService = {
  getStories: (filters, query, page, size) =>
    PlatoApi.get('/story', {
      filters,
      query,
      page,
      size,
    }),

  getStory: storyId => PlatoApi.get(`/story/${storyId}`).then(({ result }) => result),

  updateStory: ({
    id,
    tagIds,
    hideOnWebsite,
    challenges,
    usagePrevented,
    companyType,
    actionability,
    authenticity,
    obviousness,
  }) =>
    PlatoApi.put(`/story/${id}`, {
      id,
      tags: tagIds,
      hideOnWebsite,
      challenges,
      usagePrevented,
      companyType,
      actionability,
      authenticity,
      obviousness,
    }),

  validateStory: storyId => PlatoApi.put(`/story/${storyId}/validate`),
};

export default StoryService;
