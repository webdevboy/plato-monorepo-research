import PlatoApi from '../platoApi';

const UserService = {
  get: userId => PlatoApi.get(`/user/${userId}`).then(({ result }) => result),

  getManagerUser: userId =>
    PlatoApi.get(`/user/${userId}/managerUser`).then(({ result }) => result),

  resetPassword: email => PlatoApi.post('/user/resetPassword', { email }),

  updatePassword: (password, passwordConfirmation, resetPasswordToken) =>
    PlatoApi.post('/user/newPassword', {
      password,
      passwordConfirmation,
      resetPasswordToken,
    }),

  changePassword: ({ oldPassword, newPassword }) =>
    PlatoApi.put('/user/changePassword', {
      oldPassword,
      password: newPassword,
    }),
};

export default UserService;
