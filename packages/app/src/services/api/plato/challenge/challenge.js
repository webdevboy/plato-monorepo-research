import PlatoApi from '../platoApi';

const ChallengeService = {
  getChallenges: () => PlatoApi.get('/challenge'),
};

export default ChallengeService;
