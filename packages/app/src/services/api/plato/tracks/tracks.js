import PlatoApi from '../platoApi';

const TracksService = {
  getTracks: () => PlatoApi.get('/track').then(({ result }) => result),

  getPrograms: () => PlatoApi.get('/program').then(({ result }) => result),
};

export default TracksService;
