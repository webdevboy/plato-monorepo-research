import PlatoApi from '../platoApi';

const DashboardUserService = {
  get: dashboardUserId =>
    PlatoApi.get(`/dashboardUser/${dashboardUserId}`).then(({ result }) => result),
};

export default DashboardUserService;
