import PlatoApi from '../platoApi';

const RelationshipService = {
  getFutureRequests: ({
    relationshipId,
    scheduled,
    done,
    closed,
    error,
    scMentee,
    scMentor,
    matching,
  }) =>
    PlatoApi.get(`/relationship/${relationshipId}/futureRequests`, {
      scheduled,
      done,
      close: closed,
      error,
      scMentee,
      scMentor,
      matching,
    }).then(({ result }) => result),
};

export default RelationshipService;
