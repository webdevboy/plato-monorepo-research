import PlatoApi from '../platoApi';

const ViewService = {
  create: ({ viewedId, viewerId, type }) =>
    PlatoApi.post('/view', {
      viewedId,
      viewerId,
      type,
    }).then(({ result }) => result),
};

export default ViewService;
