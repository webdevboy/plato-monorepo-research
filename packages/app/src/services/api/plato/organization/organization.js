import * as fakeResults from './fakeResults';

import PlatoApi from '../platoApi';

const OrganizationService = {
  getDashboardSeats: ({ organizationId, shouldLoadFakeData = false }) =>
    PlatoApi.get(`/organization/${organizationId}/dashboard/seats`).then(({ result }) =>
      !shouldLoadFakeData ? result : fakeResults.getDashboardSeats.result
    ),

  getDashboardStats: ({ organizationId, startDate, endDate, shouldLoadFakeData = false }) =>
    PlatoApi.get(`/organization/${organizationId}/dashboard/stats`, {
      startDate,
      endDate,
    }).then(({ result }) => (!shouldLoadFakeData ? result : fakeResults.getDashboardStats.result)),

  getDashboardCommonChallenges: ({ organizationId, shouldLoadFakeData = false }) =>
    PlatoApi.get(`/organization/${organizationId}/dashboard/commonChallenges`).then(({ result }) =>
      !shouldLoadFakeData ? result : fakeResults.getDashboardCommonChallenges.result
    ),

  getDashboardQualitativeFeedback: ({ organizationId, shouldLoadFakeData = false }) =>
    PlatoApi.get(`/organization/${organizationId}/dashboard/qualitativeFeedbacks`).then(
      ({ result }) =>
        !shouldLoadFakeData ? result : fakeResults.getDashboardQualitativeFeedback.result
    ),

  getDashboardMentees: ({ organizationId, shouldLoadFakeData = false }) =>
    !shouldLoadFakeData
      ? PlatoApi.get(`/organization/${organizationId}/dashboard/mentees`).then(
          ({ result }) => result
        )
      : fakeResults.getDashboardMentees.result,

  getUser: ({ organizationId, email }) =>
    PlatoApi.get(`/organization/${organizationId}/user?email=${email}`).then(
      ({ result }) => result
    ),

  getSubscriptions: ({ organizationId }) =>
    PlatoApi.get(`/organization/${organizationId}/subscriptions`).then(({ result }) => result),

  getSlackUsers: ({ organizationId }) =>
    PlatoApi.get(`/organization/${organizationId}/slackUsers`).then(({ result }) => result),
};

export default OrganizationService;
