import moment from 'moment';
import PlatoApi from '../platoApi';

const AmaService = {
  getAma: amaId => PlatoApi.get(`/ama/${amaId}`).then(({ result }) => result),

  getAmasForMentee: options => {
    const {
      faceIds,
      isFeatured,
      maxDate = moment()
        .add(1, 'year')
        .toISOString(true),
      minDate = moment().toISOString(true),
      page = 0,
      query,
      scheduledAfterHour,
      scheduledBeforeHour,
      scheduledOnDays,
      size = 5,
      tagIds,
      hasProgram,
      minAttendees,
    } = options;

    return PlatoApi.get('/ama/forMentee', {
      faceIds,
      isFeatured,
      maxDate,
      minDate,
      page,
      query,
      scheduledAfterHour,
      scheduledBeforeHour,
      scheduledOnDays,
      size,
      tagIds,
      hasProgram,
      minAttendees,
    });
  },

  getAttendances: amaId => PlatoApi.get(`/ama/${amaId}/attendances`).then(({ result }) => result),

  getScheduledAttendances: amaId =>
    PlatoApi.get(`/ama/${amaId}/attendances/scheduled`).then(({ result }) => result),
};

export default AmaService;
