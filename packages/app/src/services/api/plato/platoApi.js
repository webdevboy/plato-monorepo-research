import ApiService from '../api';
import { views } from 'constants/Views';

class PlatoApiService extends ApiService {
  buildRequestConfig(config, isFormData = false) {
    const headers = {
      'Content-Type': 'application/json',
      SourceApplication: process.env.REACT_APP_SOURCE_APPLICATION,
    };
    // Do not set the content type manually
    // https://stackoverflow.com/questions/39280438/fetch-missing-boundary-in-multipart-form-data-post/39281156
    if (isFormData) delete headers['Content-Type'];

    if (localStorage.userToken) {
      headers.Authorization = `Bearer ${localStorage.userToken}`;
    }

    const view = localStorage.getItem('view');
    if (Object.values(views).includes(view)) {
      headers['User-Role'] = localStorage.view;
    }

    return super.buildRequestConfig({
      headers,
      // Add cookies to all calls made to the API
      credentials: 'include',
      ...config,
    });
  }

  async handleRequest(request) {
    const response = await request;
    if (!response.ok) {
      throw new Error(response.statusText);
    }

    const data = await response.json();
    if (data.status !== 'OK') {
      throw new Error(data.errors && data.errors[0] && data.errors[0].defaultMessage);
    } else {
      return data;
    }
  }
}

export default new PlatoApiService({
  serverUrl: process.env.REACT_APP_PLATO_API_URI,
});
