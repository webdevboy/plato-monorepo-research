import PlatoApi from '../platoApi';

const MenteeService = {
  create: payload => PlatoApi.post(`/mentee`, payload).then(({ result }) => result),

  getMentee: menteeId => PlatoApi.get(`/mentee/${menteeId}`).then(({ result }) => result),

  getRequests: ({
    menteeId,
    done,
    scheduled,
    scMentee,
    scMentor,
    matching,
    error,
    closed,
    page,
    size,
    firstOfRelationship,
  }) =>
    PlatoApi.get(`/mentee/${menteeId}/requests`, {
      done,
      scheduled,
      scMentee,
      scMentor,
      matching,
      error,
      close: closed,
      page,
      size,
      firstLtm: firstOfRelationship,
    }),

  getAttendances: ({
    menteeId,
    pending,
    done,
    scheduled,
    error,
    closed,
    page,
    size,
    minDate,
    maxDate,
  }) =>
    PlatoApi.get(`/mentee/${menteeId}/attendances`, {
      pending,
      done,
      scheduled,
      error,
      close: closed,
      page,
      size,
      minDate,
      maxDate,
    }),

  getBriefs: ({ menteeId, page, size }) =>
    PlatoApi.get(`/mentee/${menteeId}/briefs`, {
      menteeId,
      page,
      size,
    }).then(({ result }) => result),

  createRelationship: ({ menteeId, mentorId, nbOfWeeks }) =>
    PlatoApi.post(`/mentee/${menteeId}/ltm`, {
      id: menteeId,
      mentorId,
      nbOfWeeks,
    }),

  extendRelationship: ({ menteeId, relationshipId, nbOfMonths }) =>
    PlatoApi.post(`/mentee/${menteeId}/ltm/${relationshipId}/extend`, {
      nbOfMonths,
    }),

  updateOnboarding: async (menteeId, data, picture) => {
    const formData = new FormData();
    if (picture) {
      formData.append('picture', picture, picture.name);
    }

    // https://stackoverflow.com/questions/21329426/spring-mvc-multipart-request-with-json
    formData.append(
      'mentee',
      new Blob([JSON.stringify(data)], {
        type: 'application/json',
      })
    );

    return PlatoApi.put(`/mentee/${menteeId}/onboarding`, formData, true);
  },

  markOnboarded: async menteeId => PlatoApi.put(`/mentee/${menteeId}/onboarded`),

  getAllHierarchies: async () => PlatoApi.get('/mentee/hierarchies').then(({ result }) => result),
};

export default MenteeService;
