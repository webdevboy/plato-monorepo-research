import PaymentApi from '../payment';

export default {
  getProducts: () => PaymentApi.get('/products'),
};
