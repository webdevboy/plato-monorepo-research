import ApiService from '../api';
import { PLATO_PAYMENT_API_URI } from 'constants/Urls';

class PaymentApiService extends ApiService {
  buildRequestConfig(config) {
    const headers = {
      'Content-Type': 'application/json',
    };

    return super.buildRequestConfig({
      headers,
      ...config,
    });
  }

  async handleRequest(request) {
    const response = await request;
    if (!response.ok) {
      throw new Error(response.statusText);
    }
    return response.json();
  }
}

export default new PaymentApiService({ serverUrl: PLATO_PAYMENT_API_URI });
