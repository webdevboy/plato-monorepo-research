import { put, takeEvery } from 'redux-saga/effects';

import { showSnackbar } from 'ducks';

/**
 * Handle the error coming from the sagas.
 * @param {Boolean} action.payload The error triggered by the saga.
 */
export function* handleErrorsSaga(action) {
  const error = action.payload;
  console.error(error); // eslint-disable-line no-console
  yield put(
    showSnackbar({ message: error.message || 'Something went wrong - sorry!', variant: 'error' })
  );
}

/**
 * A action that capture any error from the sagas.
 * @param {Action} action Any dispatched actions.
 * @returns {Boolean} True if the action is an error, false otherwise.
 */
export function hasFailed(action) {
  return action.error;
}

export default function* mainSaga() {
  yield takeEvery(hasFailed, handleErrorsSaga);
}
