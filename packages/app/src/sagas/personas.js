import { all, call, put, takeLatest } from 'redux-saga/effects';
import { fetchPersonas, fetchPersonasCompleted } from 'ducks';

import PersonaService from 'services/api/plato/persona/persona';

/**
 * Fetch the list of personas.
 */
export function* fetchPersonasSaga() {
  try {
    const { result } = yield call(PersonaService.getPersonas);
    yield put(fetchPersonasCompleted(result));
  } catch (error) {
    yield put(fetchPersonasCompleted(error));
  }
}

export default function* main() {
  yield all([takeLatest(fetchPersonas, fetchPersonasSaga)]);
}
