import { all, call, put, select, take, takeLatest } from 'redux-saga/effects';
import {
  fetchDashboardUser,
  fetchDashboardUserCompleted,
  fetchInitialData,
  fetchInitialDataCompleted,
  fetchManagerUser,
  fetchManagerUserCompleted,
  fetchUser,
  fetchUserCompleted,
  fetchUserMentee,
  fetchUserMenteeCompleted,
  fetchUserMentor,
  fetchUserMentorCompleted,
  login,
  loginCompleted,
  logout,
  resetPassword,
  resetPasswordCompleted,
  updateMentee,
  updateMenteeCompleted,
  updatePassword,
  updatePasswordCompleted,
} from 'ducks';
import { trackLogin, trackLogout } from 'utils/analytics';
import { getUser } from 'selectors';

import AuthenticationService from 'services/api/plato/authentication/authentication';
import DashboardUserService from 'services/api/plato/dashboardUser/dashboardUser';
import MenteeService from 'services/api/plato/mentee/mentee';
import MentorService from 'services/api/plato/mentor/mentor';
import UserService from 'services/api/plato/user/user';
import history from 'utils/history';

export function* fetchUserMenteeSaga(action) {
  const menteeId = action.payload;
  try {
    const mentee = yield call(MenteeService.getMentee, menteeId);
    yield put(fetchUserMenteeCompleted(mentee));
  } catch (error) {
    yield put(fetchUserMenteeCompleted(error));
  }
}

export function* fetchUserMentorSaga(action) {
  const mentorId = action.payload;
  try {
    const mentor = yield call(MentorService.getMentor, mentorId);
    yield put(fetchUserMentorCompleted(mentor));
  } catch (error) {
    yield put(fetchUserMentorCompleted(error));
  }
}

export function* fetchManagerUserSaga(action) {
  const userId = action.payload;
  try {
    const managerUser = yield call(UserService.getManagerUser, userId);
    yield put(fetchManagerUserCompleted(managerUser));
  } catch (error) {
    yield put(fetchManagerUserCompleted(error));
  }
}

export function* fetchDashboardUserSaga(action) {
  const dashboardUserId = action.payload;
  try {
    const dashboardUser = yield call(DashboardUserService.get, dashboardUserId);
    yield put(fetchDashboardUserCompleted(dashboardUser));
  } catch (error) {
    yield put(fetchDashboardUserCompleted(error));
  }
}

export function* fetchUserSaga(action) {
  const userId = action.payload;
  try {
    const user = yield call(UserService.get, userId);
    if (!user.enabled) {
      yield put(fetchUserCompleted(new Error('Forbidden')));
    }
    yield put(fetchUserCompleted(user));
  } catch (error) {
    yield put(fetchUserCompleted(error));
  }
}

export function* loginSaga(action) {
  const { email, password, eternal = true } = action.payload;

  try {
    const { userId, token } = yield call(AuthenticationService.login, email, password, eternal);

    localStorage.setItem('userId', userId);
    localStorage.setItem('userToken', token);

    yield put(fetchInitialData());
    yield take(fetchInitialDataCompleted);

    const { uuid, fullName } = yield select(getUser);
    yield call(trackLogin, { uuid, email, name: fullName });
    yield put(loginCompleted());
  } catch (error) {
    yield put(loginCompleted(error));
  }
}

export function* logoutSaga(action) {
  const { uuid, email } = action.payload;
  localStorage.removeItem('userId');
  localStorage.removeItem('userEmail');
  localStorage.removeItem('userFullName');
  localStorage.removeItem('userToken');
  localStorage.removeItem('view');
  yield call(trackLogout, { uuid, email });
}

export function* updateMenteeSaga(action) {
  const { menteeId, picture, redirectPath, markAsOnboarded = false, ...data } = action.payload;
  try {
    const { result: mentee } = yield call(MenteeService.updateOnboarding, menteeId, data, picture);

    if (markAsOnboarded) {
      yield call(MenteeService.markOnboarded, menteeId);
    }

    yield put(updateMenteeCompleted(mentee));
    if (redirectPath) {
      yield call(history.push, redirectPath);
    }
  } catch (error) {
    yield put(updateMenteeCompleted(error));
  }
}

export function* resetPasswordSaga(action) {
  const { password, passwordConfirmation, token } = action.payload;
  try {
    if (password !== passwordConfirmation) {
      yield put(resetPasswordCompleted(new Error('Passwords must match')));
      return;
    }

    const { result: user } = yield call(
      UserService.updatePassword,
      password,
      passwordConfirmation,
      token
    );
    yield put(login({ email: user.email, password }));
    yield put(resetPasswordCompleted());
  } catch (error) {
    yield put(resetPasswordCompleted(error));
  }
}

export function* updatePasswordSaga(action) {
  const { email } = action.payload;
  try {
    yield call(UserService.resetPassword, email);
    yield call(history.push, '/login');
    yield put(updatePasswordCompleted());
  } catch (error) {
    yield put(updatePasswordCompleted(error));
  }
}

export default function* main() {
  yield all([
    takeLatest(fetchDashboardUser, fetchDashboardUserSaga),
    takeLatest(fetchManagerUser, fetchManagerUserSaga),
    takeLatest(fetchUser, fetchUserSaga),
    takeLatest(fetchUserMentee, fetchUserMenteeSaga),
    takeLatest(fetchUserMentor, fetchUserMentorSaga),
    takeLatest(login, loginSaga),
    takeLatest(logout, logoutSaga),
    takeLatest(resetPassword, resetPasswordSaga),
    takeLatest(updatePassword, updatePasswordSaga),
    takeLatest(updateMentee, updateMenteeSaga),
  ]);
}
