import { all, call, put, takeLatest } from 'redux-saga/effects';
import { createRelationship, createRelationshipCompleted } from 'ducks';

import MenteeService from 'services/api/plato/mentee/mentee';
import history from 'utils/history';

export function* createRelationshipSaga(action) {
  const { redirectPath, ...data } = action.payload;
  try {
    yield call(MenteeService.createRelationship, data);
    yield call(history.push, redirectPath);
    yield put(createRelationshipCompleted());
  } catch (error) {
    yield put(createRelationshipCompleted(error));
  }
}

export default function* main() {
  yield all([takeLatest(createRelationship, createRelationshipSaga)]);
}
