import { all, takeLatest } from 'redux-saga/effects';
import challengesSaga, { fetchChallengesSaga } from './challenges';
import { fetchChallenges, fetchChallengesCompleted } from 'ducks';

import ChallengeService from 'services/api/plato/challenge/challenge';
import { recordSaga } from 'utils/testHelpers';

jest.mock('services/api/plato/challenge/challenge');

it('should setup the sagas', () => {
  const generator = challengesSaga();
  expect(generator.next().value).toEqual(all([takeLatest(fetchChallenges, fetchChallengesSaga)]));
  expect(generator.next().done).toBe(true);
});

describe('fetchChallenges', () => {
  it('should dispatch fetchChallengesCompleted', async () => {
    ChallengeService.getChallenges.mockResolvedValueOnce({
      result: [{ id: 'test-challenge-id' }],
    });
    const dispatched = await recordSaga(fetchChallengesSaga, fetchChallenges());
    expect(dispatched).toContainEqual(fetchChallengesCompleted([{ id: 'test-challenge-id' }]));
  });

  it('should dispatch fetchChallengesCompleted on error', async () => {
    ChallengeService.getChallenges.mockRejectedValueOnce(new Error('test-error'));
    const dispatched = await recordSaga(fetchChallengesSaga, fetchChallenges());
    expect(dispatched).toContainEqual(fetchChallengesCompleted(new Error('test-error')));
  });
});
