import {
  addDashboardMentee,
  addDashboardMenteeCompleted,
  fetchDashboardMentees,
  fetchDashboardMenteesCompleted,
  fetchDashboardSlackUserUser,
  fetchDashboardSlackUserUserCompleted,
  fetchDashboardSlackUsers,
  fetchDashboardSlackUsersCompleted,
  setIsAddMenteeDialogOpen,
} from 'ducks';
import { all, call, put, takeLatest } from 'redux-saga/effects';

import menteeService from 'services/api/plato/mentee/mentee';
import organizationService from 'services/api/plato/organization/organization';

export function* fetchDashboardMenteesSaga({ payload }) {
  try {
    const mentees = yield call(organizationService.getDashboardMentees, payload);
    yield put(fetchDashboardMenteesCompleted(mentees));
  } catch (error) {
    yield put(fetchDashboardMenteesCompleted(error));
  }
}

export function* fetchDashboardSlackUsersSaga({ payload }) {
  try {
    const slackUsers = yield call(organizationService.getSlackUsers, payload);
    yield put(fetchDashboardSlackUsersCompleted(slackUsers));
  } catch (error) {
    yield put(fetchDashboardSlackUsersCompleted(error));
  }
}

export function* fetchDashboardSlackUserUserSaga({ payload }) {
  try {
    const user = yield call(organizationService.getUser, payload);
    yield put(fetchDashboardSlackUserUserCompleted(user));
  } catch (error) {
    // TODO:  Determine if this was a 404 on the email. Do nothing
    // if this is true.
    // Otherwise:
    // yield put(fetchSlackUserUserCompleted(error));
  }
}

export function* addDashboardMenteeSaga({ payload }) {
  try {
    const mentee = yield call(menteeService.create, payload);
    yield put(addDashboardMenteeCompleted(mentee));
    yield put(fetchDashboardMentees({ organizationId: payload.companyId }));
    yield put(setIsAddMenteeDialogOpen(false));
    payload.callback();
  } catch (error) {
    yield put(addDashboardMenteeCompleted(error));
  }
}

export function* setIsAddMenteeDialogOpenSaga({ payload }) {
  yield put(setIsAddMenteeDialogOpen(payload));
}

export default function* main() {
  yield all([
    takeLatest(fetchDashboardMentees, fetchDashboardMenteesSaga),
    takeLatest(fetchDashboardSlackUsers, fetchDashboardSlackUsersSaga),
    takeLatest(fetchDashboardSlackUserUser, fetchDashboardSlackUserUserSaga),
    takeLatest(addDashboardMentee, addDashboardMenteeSaga),
  ]);
}
