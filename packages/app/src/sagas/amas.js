import { all, call, put, select, takeEvery, takeLatest } from 'redux-saga/effects';
import {
  fetchAmaAttendances,
  fetchAmaAttendancesCompleted,
  fetchAmas,
  fetchAmasCompleted,
  fetchMentor,
} from 'ducks';

import AmaService from 'services/api/plato/ama/ama';
import { getMentor } from 'selectors';

export function* fetchAmasSaga(action) {
  try {
    const { result } = yield call(AmaService.getAmasForMentee, action.payload);

    yield all(
      result.map(ama => put(fetchAmaAttendances({ amaId: ama.id, mentorId: ama.mentorId })))
    );

    yield put(fetchAmasCompleted(result));
  } catch (error) {
    yield put(fetchAmasCompleted(error));
  }
}

export function* fetchAmaAttendancesSaga(action) {
  const { amaId, mentorId } = action.payload;
  try {
    const result = yield call(AmaService.getAttendances, amaId);

    const mentor = yield select(getMentor, mentorId);
    if (!mentor) {
      yield put(fetchMentor(mentorId));
    }

    yield put(fetchAmaAttendancesCompleted({ amaId, attendances: result }));
  } catch (error) {
    yield put(fetchAmaAttendancesCompleted(error));
  }
}

export default function* main() {
  yield all([
    takeLatest(fetchAmas, fetchAmasSaga),
    takeEvery(fetchAmaAttendances, fetchAmaAttendancesSaga),
  ]);
}
