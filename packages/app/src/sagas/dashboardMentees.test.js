import {
  addDashboardMentee,
  fetchDashboardMentees,
  fetchDashboardMenteesCompleted,
  fetchDashboardSlackUserUser,
  fetchDashboardSlackUsers,
} from 'ducks';
import { all, takeLatest } from 'redux-saga/effects';
import dashboardMenteesSaga, {
  addDashboardMenteeSaga,
  fetchDashboardMenteesSaga,
  fetchDashboardSlackUserUserSaga,
  fetchDashboardSlackUsersSaga,
} from './dashboardMentees';

import OrganizationService from 'services/api/plato/organization/organization';
import { recordSaga } from 'utils/testHelpers';

jest.mock('services/api/plato/organization/organization');

it('should setup the sagas', () => {
  const generator = dashboardMenteesSaga();
  expect(generator.next().value).toEqual(
    all([
      takeLatest(fetchDashboardMentees, fetchDashboardMenteesSaga),
      takeLatest(fetchDashboardSlackUsers, fetchDashboardSlackUsersSaga),
      takeLatest(fetchDashboardSlackUserUser, fetchDashboardSlackUserUserSaga),
      takeLatest(addDashboardMentee, addDashboardMenteeSaga),
    ])
  );
  expect(generator.next().done).toBe(true);
});

describe('fetchDashboardMentees', () => {
  const dashboardMentees = [
    {
      mentee: {},
      statistics: {
        nbCallDoneRequests: 0,
        nbGradedRequests: 0,
        nbCallDoneAttendances: 0,
        nbGradedAttendances: 0,
        requestGradeAverage: null,
        attendanceGradeAverage: null,
      },
    },
    {
      mentee: {},
      statistics: {
        nbCallDoneRequests: 0,
        nbGradedRequests: 0,
        nbCallDoneAttendances: 0,
        nbGradedAttendances: 0,
        requestGradeAverage: null,
        attendanceGradeAverage: null,
      },
    },
  ];
  it('should dispatch fetchDashboardMenteesCompleted', async () => {
    OrganizationService.getDashboardMentees.mockResolvedValueOnce(dashboardMentees);
    const dispatched = await recordSaga(
      fetchDashboardMenteesSaga,
      fetchDashboardMentees(dashboardMentees)
    );
    expect(dispatched).toContainEqual(fetchDashboardMenteesCompleted(dashboardMentees));
  });

  it('should dispatch fetchDashboardQualitativeFeedbackCompleted on error', async () => {
    OrganizationService.getDashboardMentees.mockRejectedValueOnce(new Error('test-error'));
    const dispatched = await recordSaga(
      fetchDashboardMenteesSaga,
      fetchDashboardMentees(dashboardMentees)
    );
    expect(dispatched).toContainEqual(fetchDashboardMenteesCompleted(new Error('test-error')));
  });
});
