import { all, takeLatest } from 'redux-saga/effects';
import { createProgramApplication, createProgramApplicationCompleted } from 'ducks';
import { push } from 'connected-react-router';
import programsSaga, { createProgramApplicationSaga } from './programs';

import ProgramsService from 'services/api/plato/program/program';
import { recordSaga } from 'utils/testHelpers';

jest.mock('services/api/plato/program/program');
jest.mock('utils/history');
jest.mock('connected-react-router');

it('should setup the sagas', () => {
  const generator = programsSaga();
  expect(generator.next().value).toEqual(
    all([takeLatest(createProgramApplication, createProgramApplicationSaga)])
  );
  expect(generator.next().done).toBe(true);
});

describe('createProgramApplication', () => {
  afterEach(() => push.mockClear());

  it('should dispatch createProgramApplicationCompleted', async () => {
    ProgramsService.createApplication.mockResolvedValueOnce();
    const dispatched = await recordSaga(
      createProgramApplicationSaga,
      createProgramApplication({
        programId: 'test-program-id',
        redirectPath: 'test-redirect-path',
      })
    );
    expect(ProgramsService.createApplication).toHaveBeenNthCalledWith(1, 'test-program-id', {});
    expect(dispatched).toContainEqual(createProgramApplicationCompleted());
    expect(push).toHaveBeenNthCalledWith(1, 'test-redirect-path');
  });

  it('should dispatch createProgramApplicationCompleted on error', async () => {
    ProgramsService.createApplication.mockRejectedValueOnce(new Error('test-error'));
    const dispatched = await recordSaga(
      createProgramApplicationSaga,
      createProgramApplication({
        programId: 'test-program-id',
        redirectPath: 'test-redirect-path',
      })
    );
    expect(dispatched).toContainEqual(createProgramApplicationCompleted(new Error('test-error')));
  });
});
