import { all, call, put, takeLatest } from 'redux-saga/effects';
import { fetchSubscriptions, fetchSubscriptionsCompleted } from 'ducks';

import OrganizationService from 'services/api/plato/organization/organization';

/**
 * Fetch the list of subscriptions for an organization
 */
export function* fetchSubscriptionsSaga({ payload }) {
  try {
    const result = yield call(OrganizationService.getSubscriptions, payload);
    yield put(fetchSubscriptionsCompleted(result));
  } catch (error) {
    yield put(fetchSubscriptionsCompleted(error));
  }
}

export default function* main() {
  yield all([takeLatest(fetchSubscriptions, fetchSubscriptionsSaga)]);
}
