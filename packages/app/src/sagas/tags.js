import { all, call, put, takeLatest } from 'redux-saga/effects';
import { fetchIndustryTags, fetchIndustryTagsCompleted } from 'ducks';

import TagService from 'services/api/plato/tag/tag';

/**
 * Fetch the list of Slack users from the Slack account where the bot is installed.
 */
export function* fetchIndustryTagsSaga() {
  try {
    const industries = yield call(TagService.getIndustries);
    yield put(fetchIndustryTagsCompleted(industries));
  } catch (error) {
    yield put(fetchIndustryTagsCompleted(error));
  }
}

export default function* main() {
  yield all([takeLatest(fetchIndustryTags, fetchIndustryTagsSaga)]);
}
