import { call, select, takeLatest } from 'redux-saga/effects';

import { getUser } from 'selectors';
import { setView } from 'ducks';
import { trackSpaceSwitched } from 'utils/analytics';

/**
 * Change the view.
 */
export function* setViewSaga(action) {
  const { fromView, toView } = action.payload;
  localStorage.setItem('view', toView);
  const user = yield select(getUser);
  if (user) {
    yield call(trackSpaceSwitched, {
      uuid: user.uuid,
      email: user.email,
      menteeId: user.menteeId,
      dashboardUserId: user.dashboardUserId,
      mentorId: user.mentorId,
      fromView,
      toView,
    });
  }
}

export default function* main() {
  yield takeLatest(setView, setViewSaga);
}
