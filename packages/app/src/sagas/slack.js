import { all, call, put, takeLatest } from 'redux-saga/effects';
import { fetchSlackUsers, fetchSlackUsersCompleted } from 'ducks';

import SlackUserService from 'services/api/plato/slackUser/slackUser';

/**
 * Fetch the list of Slack users from the Slack account where the bot is installed.
 */
export function* fetchSlackUsersSaga() {
  try {
    const { result } = yield call(SlackUserService.getSlackUsers);
    yield put(fetchSlackUsersCompleted(result));
  } catch (error) {
    yield put(fetchSlackUsersCompleted(error));
  }
}

export default function* main() {
  yield all([takeLatest(fetchSlackUsers, fetchSlackUsersSaga)]);
}
