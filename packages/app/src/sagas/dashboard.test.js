import { all, takeLatest } from 'redux-saga/effects';
import dashboardSaga, {
  fetchDashboardCommonChallengesSaga,
  fetchDashboardQualitativeFeedbackSaga,
  fetchDashboardSeatsSaga,
  fetchDashboardStatsSaga,
} from './dashboard';
import {
  fetchDashboardCommonChallenges,
  fetchDashboardCommonChallengesCompleted,
  fetchDashboardQualitativeFeedback,
  fetchDashboardQualitativeFeedbackCompleted,
  fetchDashboardSeats,
  fetchDashboardSeatsCompleted,
  fetchDashboardStats,
  fetchDashboardStatsCompleted,
} from 'ducks';

import OrganizationService from 'services/api/plato/organization/organization';
import { recordSaga } from 'utils/testHelpers';

jest.mock('services/api/plato/organization/organization');

it('should setup the sagas', () => {
  const generator = dashboardSaga();
  expect(generator.next().value).toEqual(
    all([
      takeLatest(fetchDashboardStats, fetchDashboardStatsSaga),
      takeLatest(fetchDashboardSeats, fetchDashboardSeatsSaga),
      takeLatest(fetchDashboardCommonChallenges, fetchDashboardCommonChallengesSaga),
      takeLatest(fetchDashboardQualitativeFeedback, fetchDashboardQualitativeFeedbackSaga),
    ])
  );
  expect(generator.next().done).toBe(true);
});

describe('fetchDashboardSeats', () => {
  const seats = {
    nbTakenSeats: 7,
    nbPaidSeats: 8,
    nbOnboardedSeats: 4,
  };

  it('should dispatch fetchDashboardSeatsCompleted', async () => {
    OrganizationService.getDashboardSeats.mockResolvedValueOnce(seats);
    const dispatched = await recordSaga(fetchDashboardSeatsSaga, fetchDashboardSeats(seats));
    expect(dispatched).toContainEqual(fetchDashboardSeatsCompleted(seats));
  });

  it('should dispatch fetchDashboardSeatsCompleted on error', async () => {
    OrganizationService.getDashboardSeats.mockRejectedValueOnce(new Error('test-error'));
    const dispatched = await recordSaga(fetchDashboardSeatsSaga, fetchDashboardSeats(seats));
    expect(dispatched).toContainEqual(fetchDashboardSeatsCompleted(new Error('test-error')));
  });
});

describe('fetchDashboardStats', () => {
  const stats = [
    {
      month: '04/2017',
      nbCallDoneRequests: 11,
      nbCallDoneAttendances: 1,
      requestGradeAverage: 5.5,
      attendanceGradeAverage: 4.5,
    },
    {
      month: '05/2017',
      nbCallDoneRequests: 10,
      nbCallDoneAttendances: 2,
      requestGradeAverage: 5,
      attendanceGradeAverage: 5,
    },
  ];

  it('should dispatch fetchDashboardStatsCompleted', async () => {
    OrganizationService.getDashboardStats.mockResolvedValueOnce(stats);
    const dispatched = await recordSaga(fetchDashboardStatsSaga, fetchDashboardStats(stats));
    expect(dispatched).toContainEqual(fetchDashboardStatsCompleted(stats));
  });

  it('should dispatch fetchDashboardStatsCompleted on error', async () => {
    OrganizationService.getDashboardStats.mockRejectedValueOnce(new Error('test-error'));
    const dispatched = await recordSaga(fetchDashboardStatsSaga, fetchDashboardStats(stats));
    expect(dispatched).toContainEqual(fetchDashboardStatsCompleted(new Error('test-error')));
  });
});

describe('fetchDashboardCommonChallenges', () => {
  const commonChallenges = [
    {
      challengeTitle: 'Transitioning from IC to management',
      nbCallDoneRequests: 23,
      nbCallDoneAttendances: 12,
    },
    {
      challengeTitle: 'Balancing time and prioritizing your work',
      nbCallDoneRequests: 13,
      nbCallDoneAttendances: 4,
    },
  ];

  it('should dispatch fetchDashboardCommonChallengesCompleted', async () => {
    OrganizationService.getDashboardCommonChallenges.mockResolvedValueOnce(commonChallenges);
    const dispatched = await recordSaga(
      fetchDashboardCommonChallengesSaga,
      fetchDashboardStats(commonChallenges)
    );
    expect(dispatched).toContainEqual(fetchDashboardCommonChallengesCompleted(commonChallenges));
  });

  it('should dispatch fetchDashboardCommonChallengesCompleted on error', async () => {
    OrganizationService.getDashboardCommonChallenges.mockRejectedValueOnce(new Error('test-error'));
    const dispatched = await recordSaga(
      fetchDashboardCommonChallengesSaga,
      fetchDashboardStats(commonChallenges)
    );
    expect(dispatched).toContainEqual(
      fetchDashboardCommonChallengesCompleted(new Error('test-error'))
    );
  });
});

describe('fetchDashboardQualitativeFeedback', () => {
  const qualitativeFeedback = [
    {
      qualitativeFeedback: "Great job helping me see things from other people's perspective.",
      callScheduledAt: 1,
    },
    {
      qualitativeFeedback:
        'The tactics Tido has given me are very helpful. As we discussed, I would work with HR earlier on in term of review/comp discussions. I will alsotry out the weekly updates on the metrics I am looking at with the team to help forest a good practice within the team. Thanks again for your good advices!',
      callScheduledAt: 2,
    },
  ];

  it('should dispatch fetchDashboardQualitativeFeedbackCompleted', async () => {
    OrganizationService.getDashboardQualitativeFeedback.mockResolvedValueOnce(qualitativeFeedback);
    const dispatched = await recordSaga(
      fetchDashboardQualitativeFeedbackSaga,
      fetchDashboardQualitativeFeedback(qualitativeFeedback)
    );
    expect(dispatched).toContainEqual(
      fetchDashboardQualitativeFeedbackCompleted(qualitativeFeedback)
    );
  });

  it('should dispatch fetchDashboardQualitativeFeedbackCompleted on error', async () => {
    OrganizationService.getDashboardQualitativeFeedback.mockRejectedValueOnce(
      new Error('test-error')
    );
    const dispatched = await recordSaga(
      fetchDashboardQualitativeFeedbackSaga,
      fetchDashboardQualitativeFeedback(qualitativeFeedback)
    );
    expect(dispatched).toContainEqual(
      fetchDashboardQualitativeFeedbackCompleted(new Error('test-error'))
    );
  });
});
