import { all, takeLatest } from 'redux-saga/effects';
import { fetchHierarchies, fetchHierarchiesCompleted } from 'ducks';
import saga, { fetchHierarchiesSaga } from './hierarchies';

import MenteeService from 'services/api/plato/mentee/mentee';
import { recordSaga } from 'utils/testHelpers';

jest.mock('services/api/plato/mentee/mentee');

it('should setup the sagas', () => {
  const generator = saga();
  expect(generator.next().value).toEqual(all([takeLatest(fetchHierarchies, fetchHierarchiesSaga)]));
  expect(generator.next().done).toBe(true);
});

describe('fetchHierarchies', () => {
  it('should dispatch fetchHierarchiesCompleted', async () => {
    MenteeService.getAllHierarchies.mockResolvedValueOnce([{ id: 'test-hierarchy-id' }]);
    const dispatched = await recordSaga(fetchHierarchiesSaga, fetchHierarchies());
    expect(dispatched).toContainEqual(fetchHierarchiesCompleted([{ id: 'test-hierarchy-id' }]));
  });

  it('should dispatch fetchHierarchiesCompleted on error', async () => {
    MenteeService.getAllHierarchies.mockRejectedValueOnce(new Error('test-error'));
    const dispatched = await recordSaga(fetchHierarchiesSaga, fetchHierarchies());
    expect(dispatched).toContainEqual(fetchHierarchiesCompleted(new Error('test-error')));
  });
});
