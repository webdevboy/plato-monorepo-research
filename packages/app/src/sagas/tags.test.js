import { all, takeLatest } from 'redux-saga/effects';
import { fetchIndustryTags, fetchIndustryTagsCompleted } from 'ducks';
import saga, { fetchIndustryTagsSaga } from './tags';

import TagService from 'services/api/plato/tag/tag';
import { recordSaga } from 'utils/testHelpers';

jest.mock('services/api/plato/tag/tag');

it('should setup the sagas', () => {
  const generator = saga();
  expect(generator.next().value).toEqual(
    all([takeLatest(fetchIndustryTags, fetchIndustryTagsSaga)])
  );
  expect(generator.next().done).toBe(true);
});

describe('fetchIndustryTags', () => {
  it('should dispatch fetchIndustryTagsCompleted', async () => {
    TagService.getIndustries.mockResolvedValueOnce([{ id: 'test-tag-id' }]);
    const dispatched = await recordSaga(fetchIndustryTagsSaga, fetchIndustryTags());
    expect(dispatched).toContainEqual(fetchIndustryTagsCompleted([{ id: 'test-tag-id' }]));
  });

  it('should dispatch fetchIndustryTagsCompleted on error', async () => {
    TagService.getIndustries.mockRejectedValueOnce(new Error('test-error'));
    const dispatched = await recordSaga(fetchIndustryTagsSaga, fetchIndustryTags());
    expect(dispatched).toContainEqual(fetchIndustryTagsCompleted(new Error('test-error')));
  });
});
