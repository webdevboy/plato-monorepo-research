import { all, call, put, take, takeLatest } from 'redux-saga/effects';
import {
  fetchDashboardUser,
  fetchDashboardUserCompleted,
  fetchInitialData,
  fetchInitialDataCompleted,
  fetchManagerUser,
  fetchManagerUserCompleted,
  fetchUser,
  fetchUserCompleted,
  fetchUserMentee,
  fetchUserMenteeCompleted,
  fetchUserMentor,
  fetchUserMentorCompleted,
  setView,
} from 'ducks';
import { identify, trackAppOpened } from 'utils/analytics';

import { identifyLogRocketUser } from 'utils/logRocket';
import { identifySentryUser } from 'utils/sentry';
import { views } from 'constants/Views';

/**
 * Initialise the applicaton by fetching the required data.
 */
export function* fetchInitialDataSaga() {
  const userId = localStorage.getItem('userId');

  if (!userId) {
    yield call(trackAppOpened);
    yield put(fetchInitialDataCompleted());
    return;
  }

  try {
    yield put(fetchUser(userId));
    const action = yield take(fetchUserCompleted);
    // Fail the action with the error if any
    if (action.error) {
      throw action.payload;
    }

    // Store some user information in the local storage
    const user = action.payload;
    localStorage.setItem('userEmail', user.email);
    localStorage.setItem('userFullName', user.fullName);

    // Initialize the user view
    const view = localStorage.getItem('view');
    if (
      !view ||
      (view === views.MENTEE && user.menteeId === null) ||
      (view === views.MENTOR && user.mentorId === null) ||
      (view === views.DASHBOARD_USER && user.dashboardUserId === null) ||
      !Object.values(views).includes(view)
    ) {
      // This view is not supported
      if (user.menteeId) yield put(setView({ toView: views.MENTEE }));
      if (user.mentorId) yield put(setView({ toView: views.MENTOR }));
      if (user.dashboardUserId) yield put(setView({ toView: views.DASHBOARD_USER }));
    } else {
      yield put(setView({ toView: view }));
    }

    // Get all the required information for that user
    yield all([
      user.managerUserId && put(fetchManagerUser(user.id)),
      user.menteeId && put(fetchUserMentee(user.menteeId)),
      user.mentorId && put(fetchUserMentor(user.mentorId)),
      user.dashboardUserId && put(fetchDashboardUser(user.dashboardUserId)),
    ]);

    // Wait for all the request to finish
    const results = yield all([
      user.managerUserId && take(fetchManagerUserCompleted),
      user.menteeId && take(fetchUserMenteeCompleted),
      user.mentorId && take(fetchUserMentorCompleted),
      user.dashboardUserId && take(fetchDashboardUserCompleted),
    ]);

    // Fail the action with the first error if any
    const error = results.filter(Boolean).find(result => result.error);
    if (error) {
      throw error.payload;
    }

    yield call(identifyLogRocketUser, user.uuid, user.fullName, user.email);
    yield call(identifySentryUser, user.uuid, user.fullName, user.email);
    yield call(identify, user.uuid);
    yield call(trackAppOpened, { uuid: user.uuid, email: user.email });
    yield put(fetchInitialDataCompleted());
  } catch (error) {
    localStorage.removeItem('userId');
    localStorage.removeItem('userEmail');
    localStorage.removeItem('userFullName');
    localStorage.removeItem('userToken');
    localStorage.removeItem('view');
    yield put(fetchInitialDataCompleted(error));
  }
}

export default function* main() {
  yield all([takeLatest(fetchInitialData, fetchInitialDataSaga)]);
}
