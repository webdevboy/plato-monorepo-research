import { all, call, put, takeLatest } from 'redux-saga/effects';
import { createProgramApplication, createProgramApplicationCompleted } from 'ducks';
import { push } from 'connected-react-router';
import ProgramService from 'services/api/plato/program/program';

/**
 * Fetch the list of Stripe products and plans associated to those products.
 */
export function* createProgramApplicationSaga(action) {
  const { programId, redirectPath, ...data } = action.payload;
  try {
    yield call(ProgramService.createApplication, programId, data);
    yield put(createProgramApplicationCompleted());
    yield put(push(redirectPath));
  } catch (error) {
    yield put(createProgramApplicationCompleted(error));
  }
}

export default function* main() {
  yield all([takeLatest(createProgramApplication, createProgramApplicationSaga)]);
}
