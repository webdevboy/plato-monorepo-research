import { all, takeLatest } from 'redux-saga/effects';
import { fetchPersonas, fetchPersonasCompleted } from 'ducks';
import personasSaga, { fetchPersonasSaga } from './personas';

import PersonaService from 'services/api/plato/persona/persona';
import { recordSaga } from 'utils/testHelpers';

jest.mock('services/api/plato/persona/persona');

it('should setup the sagas', () => {
  const generator = personasSaga();
  expect(generator.next().value).toEqual(all([takeLatest(fetchPersonas, fetchPersonasSaga)]));
  expect(generator.next().done).toBe(true);
});

describe('fetchPersonas', () => {
  it('should dispatch fetchPersonasCompleted', async () => {
    PersonaService.getPersonas.mockResolvedValueOnce({
      result: [{ id: 'test-persona-id' }],
    });
    const dispatched = await recordSaga(fetchPersonasSaga, fetchPersonas());
    expect(dispatched).toContainEqual(fetchPersonasCompleted([{ id: 'test-persona-id' }]));
  });

  it('should dispatch fetchPersonasCompleted on error', async () => {
    PersonaService.getPersonas.mockRejectedValueOnce(new Error('test-error'));
    const dispatched = await recordSaga(fetchPersonasSaga, fetchPersonas());
    expect(dispatched).toContainEqual(fetchPersonasCompleted(new Error('test-error')));
  });
});
