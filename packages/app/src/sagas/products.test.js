import { all, takeLatest } from 'redux-saga/effects';
import { fetchProducts, fetchProductsCompleted } from 'ducks';
import productsSaga, { fetchProductsSaga } from './products';

import ProductsService from 'services/api/payment/products/products';
import { recordSaga } from 'utils/testHelpers';

jest.mock('services/api/payment/products/products');

it('should setup the sagas', () => {
  const generator = productsSaga();
  expect(generator.next().value).toEqual(all([takeLatest(fetchProducts, fetchProductsSaga)]));
  expect(generator.next().done).toBe(true);
});

describe('fetchProducts', () => {
  it('should dispatch fetchProductsCompleted', async () => {
    ProductsService.getProducts.mockResolvedValueOnce([{ id: 'test-product-id' }]);
    const dispatched = await recordSaga(fetchProductsSaga, fetchProducts());
    expect(dispatched).toContainEqual(fetchProductsCompleted([{ id: 'test-product-id' }]));
  });

  it('should dispatch fetchProductsCompleted on error', async () => {
    ProductsService.getProducts.mockRejectedValueOnce(new Error('test-error'));
    const dispatched = await recordSaga(fetchProductsSaga, fetchProducts());
    expect(dispatched).toContainEqual(fetchProductsCompleted(new Error('test-error')));
  });
});
