import { all, call, put, takeLatest } from 'redux-saga/effects';
import { fetchPrograms, fetchProgramsCompleted, fetchTracks, fetchTracksCompleted } from 'ducks';

import TracksService from 'services/api/plato/tracks/tracks';

/**
 * Fetch the list of programs.
 */
export function* fetchProgramsSaga() {
  try {
    const response = yield call(TracksService.getPrograms);
    yield put(fetchProgramsCompleted(response));
  } catch (error) {
    yield put(fetchProgramsCompleted(error));
  }
}

/**
 * Fetch the list of tracks.
 */
export function* fetchTracksSaga() {
  try {
    const response = yield call(TracksService.getTracks);
    yield put(fetchTracksCompleted(response));
  } catch (error) {
    yield put(fetchTracksCompleted(error));
  }
}

export default function* main() {
  yield all([
    takeLatest(fetchPrograms, fetchProgramsSaga),
    takeLatest(fetchTracks, fetchTracksSaga),
  ]);
}
