import { all, takeLatest } from 'redux-saga/effects';
import { fetchSlackUsers, fetchSlackUsersCompleted } from 'ducks';
import slackSaga, { fetchSlackUsersSaga } from './slack';

import SlackUserService from 'services/api/plato/slackUser/slackUser';
import { recordSaga } from 'utils/testHelpers';

jest.mock('services/api/plato/slackUser/slackUser');

it('should setup the sagas', () => {
  const generator = slackSaga();
  expect(generator.next().value).toEqual(all([takeLatest(fetchSlackUsers, fetchSlackUsersSaga)]));
  expect(generator.next().done).toBe(true);
});

describe('fetchSlackUsers', () => {
  it('should dispatch fetchSlackUsersCompleted', async () => {
    SlackUserService.getSlackUsers.mockResolvedValueOnce({
      result: [{ id: 'test-slack-user-id' }],
    });
    const dispatched = await recordSaga(fetchSlackUsersSaga, fetchSlackUsers());
    expect(dispatched).toContainEqual(fetchSlackUsersCompleted([{ id: 'test-slack-user-id' }]));
  });

  it('should dispatch fetchSlackUsersCompleted on error', async () => {
    SlackUserService.getSlackUsers.mockRejectedValueOnce(new Error('test-error'));
    const dispatched = await recordSaga(fetchSlackUsersSaga, fetchSlackUsers());
    expect(dispatched).toContainEqual(fetchSlackUsersCompleted(new Error('test-error')));
  });
});
