import { all, takeEvery, takeLatest } from 'redux-saga/effects';
import {
  fetchMentor,
  fetchMentorCompleted,
  fetchMentorRelationshipSlots,
  fetchMentorRelationshipSlotsCompleted,
  fetchMentors,
  fetchMentorsCompleted,
} from 'ducks';
import saga, {
  fetchMentorRelationshipSlotsSaga,
  fetchMentorSaga,
  fetchMentorsSaga,
} from './mentors';

import MentorService from 'services/api/plato/mentor/mentor';
import { recordSaga } from 'utils/testHelpers';

jest.mock('services/api/plato/mentor/mentor');

it('should setup the sagas', () => {
  const generator = saga();
  expect(generator.next().value).toEqual(
    all([
      takeEvery(fetchMentor, fetchMentorSaga),
      takeLatest(fetchMentorRelationshipSlots, fetchMentorRelationshipSlotsSaga),
      takeLatest(fetchMentors, fetchMentorsSaga),
    ])
  );
  expect(generator.next().done).toBe(true);
});

describe('fetchMentor', () => {
  it('should dispatch fetchMentorCompleted', async () => {
    MentorService.getMentor.mockResolvedValueOnce({ id: 'test-mentor-id' });
    const dispatched = await recordSaga(fetchMentorSaga, fetchMentor());
    expect(dispatched).toContainEqual(fetchMentorCompleted({ id: 'test-mentor-id' }));
  });

  it('should dispatch fetchMentorCompleted on error', async () => {
    MentorService.getMentor.mockRejectedValueOnce(new Error('test-error'));
    const dispatched = await recordSaga(fetchMentorSaga, fetchMentor());
    expect(dispatched).toContainEqual(fetchMentorCompleted(new Error('test-error')));
  });
});

describe('fetchMentorRelationshipSlots', () => {
  it('should dispatch fetchMentorRelationshipSlotsCompleted', async () => {
    MentorService.getRelationshipSlots.mockResolvedValueOnce([{ slot: 'test-slot' }]);
    const dispatched = await recordSaga(
      fetchMentorRelationshipSlotsSaga,
      fetchMentorRelationshipSlots('test-mentor-id')
    );
    expect(dispatched).toContainEqual(
      fetchMentorRelationshipSlotsCompleted({
        mentorId: 'test-mentor-id',
        slots: [{ slot: 'test-slot' }],
      })
    );
  });

  it('should dispatch fetchMentorRelationshipSlotsCompleted on error', async () => {
    MentorService.getRelationshipSlots.mockRejectedValueOnce(new Error('test-error'));
    const dispatched = await recordSaga(
      fetchMentorRelationshipSlotsSaga,
      fetchMentorRelationshipSlots('test-mentor-id')
    );
    expect(dispatched).toContainEqual(
      fetchMentorRelationshipSlotsCompleted(new Error('test-error'))
    );
  });
});

describe('fetchMentors', () => {
  it('should dispatch fetchMentorsCompleted', async () => {
    MentorService.searchForMentee.mockResolvedValueOnce({ result: [{ id: 'test-mentor-id' }] });
    const dispatched = await recordSaga(fetchMentorsSaga, fetchMentors());
    expect(dispatched).toContainEqual(fetchMentorsCompleted([{ id: 'test-mentor-id' }]));
  });

  it('should dispatch fetchMentorsCompleted on error', async () => {
    MentorService.searchForMentee.mockRejectedValueOnce(new Error('test-error'));
    const dispatched = await recordSaga(fetchMentorsSaga, fetchMentors());
    expect(dispatched).toContainEqual(fetchMentorsCompleted(new Error('test-error')));
  });
});
