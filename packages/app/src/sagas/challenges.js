import { all, call, put, takeLatest } from 'redux-saga/effects';
import { fetchChallenges, fetchChallengesCompleted } from 'ducks';

import ChallengeService from 'services/api/plato/challenge/challenge';

/**
 * Fetch the common challenges.
 */
export function* fetchChallengesSaga() {
  try {
    const { result } = yield call(ChallengeService.getChallenges);
    yield put(fetchChallengesCompleted(result));
  } catch (error) {
    yield put(fetchChallengesCompleted(error));
  }
}

export default function* main() {
  yield all([takeLatest(fetchChallenges, fetchChallengesSaga)]);
}
