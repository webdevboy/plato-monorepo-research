import { all, call, put, takeLatest } from 'redux-saga/effects';
import { fetchProducts, fetchProductsCompleted } from 'ducks';

import ProductsService from 'services/api/payment/products/products';

/**
 * Fetch the list of Stripe products and plans associated to those products.
 */
export function* fetchProductsSaga() {
  try {
    const products = yield call(ProductsService.getProducts);
    yield put(fetchProductsCompleted(products));
  } catch (error) {
    yield put(fetchProductsCompleted(error));
  }
}

export default function* main() {
  yield all([takeLatest(fetchProducts, fetchProductsSaga)]);
}
