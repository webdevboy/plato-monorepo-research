import { all, takeLatest } from 'redux-saga/effects';
import { fetchPrograms, fetchProgramsCompleted, fetchTracks, fetchTracksCompleted } from 'ducks';
import saga, { fetchProgramsSaga, fetchTracksSaga } from './tracks';

import TracksService from 'services/api/plato/tracks/tracks';
import { recordSaga } from 'utils/testHelpers';

jest.mock('services/api/plato/tracks/tracks');

it('should setup the sagas', () => {
  const generator = saga();
  expect(generator.next().value).toEqual(
    all([takeLatest(fetchPrograms, fetchProgramsSaga), takeLatest(fetchTracks, fetchTracksSaga)])
  );
  expect(generator.next().done).toBe(true);
});

describe('fetchPrograms', () => {
  it('should dispatch fetchProgramsCompleted', async () => {
    TracksService.getPrograms.mockResolvedValueOnce([{ id: 'test-track-id' }]);
    const dispatched = await recordSaga(fetchProgramsSaga, fetchPrograms());
    expect(dispatched).toContainEqual(fetchProgramsCompleted([{ id: 'test-track-id' }]));
  });

  it('should dispatch fetchProgramsCompleted on error', async () => {
    TracksService.getPrograms.mockRejectedValueOnce(new Error('test-error'));
    const dispatched = await recordSaga(fetchProgramsSaga, fetchPrograms());
    expect(dispatched).toContainEqual(fetchProgramsCompleted(new Error('test-error')));
  });
});

describe('fetchTracks', () => {
  it('should dispatch fetchTracksCompleted', async () => {
    TracksService.getTracks.mockResolvedValueOnce([{ id: 'test-track-id' }]);
    const dispatched = await recordSaga(fetchTracksSaga, fetchTracks());
    expect(dispatched).toContainEqual(fetchTracksCompleted([{ id: 'test-track-id' }]));
  });

  it('should dispatch fetchTracksCompleted on error', async () => {
    TracksService.getTracks.mockRejectedValueOnce(new Error('test-error'));
    const dispatched = await recordSaga(fetchTracksSaga, fetchTracks());
    expect(dispatched).toContainEqual(fetchTracksCompleted(new Error('test-error')));
  });
});
