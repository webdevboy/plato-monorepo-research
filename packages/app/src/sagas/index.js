import { all, fork } from 'redux-saga/effects';

import account from './account';
import amas from './amas';
import challenges from './challenges';
import dashboard from './dashboard';
import dashboardMentees from './dashboardMentees';
import errors from './errors';
import hierarchies from './hierarchies';
import initialData from './initialData';
import mentors from './mentors';
import personas from './personas';
import products from './products';
import programs from './programs';
import relationships from './relationships';
import slack from './slack';
import subscriptions from './subscriptions';
import tags from './tags';
import tracks from './tracks';
import user from './user';
import view from './view';

export default function* mainSaga() {
  yield all([
    fork(account),
    fork(amas),
    fork(challenges),
    fork(dashboard),
    fork(dashboardMentees),
    fork(errors),
    fork(hierarchies),
    fork(initialData),
    fork(mentors),
    fork(personas),
    fork(products),
    fork(programs),
    fork(relationships),
    fork(slack),
    fork(subscriptions),
    fork(tags),
    fork(tracks),
    fork(user),
    fork(view),
  ]);
}
