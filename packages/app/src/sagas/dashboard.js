import { all, call, put, takeLatest } from 'redux-saga/effects';
import {
  fetchDashboardCommonChallenges,
  fetchDashboardCommonChallengesCompleted,
  fetchDashboardQualitativeFeedback,
  fetchDashboardQualitativeFeedbackCompleted,
  fetchDashboardSeats,
  fetchDashboardSeatsCompleted,
  fetchDashboardStats,
  fetchDashboardStatsCompleted,
} from 'ducks';

import organizationService from 'services/api/plato/organization/organization';

/**
 * Fetch the dashboard stats for the given organization.
 */
export function* fetchDashboardStatsSaga({ payload }) {
  try {
    const stats = yield call(organizationService.getDashboardStats, payload);
    yield put(fetchDashboardStatsCompleted(stats));
  } catch (error) {
    yield put(fetchDashboardStatsCompleted(error));
  }
}

/**
 * Fetch the dashboard seats for the given organization.
 */
export function* fetchDashboardSeatsSaga({ payload }) {
  try {
    const stats = yield call(organizationService.getDashboardSeats, payload);
    yield put(fetchDashboardSeatsCompleted(stats));
  } catch (error) {
    yield put(fetchDashboardSeatsCompleted(error));
  }
}

/**
 * Fetch the common challenges for the given organization.
 */
export function* fetchDashboardCommonChallengesSaga({ payload }) {
  try {
    const stats = yield call(organizationService.getDashboardCommonChallenges, payload);
    yield put(fetchDashboardCommonChallengesCompleted(stats));
  } catch (error) {
    yield put(fetchDashboardCommonChallengesCompleted(error));
  }
}

/**
 * Fetch the qualitative feedback for the given organization.
 */
export function* fetchDashboardQualitativeFeedbackSaga({ payload }) {
  try {
    const stats = yield call(organizationService.getDashboardQualitativeFeedback, payload);
    yield put(fetchDashboardQualitativeFeedbackCompleted(stats));
  } catch (error) {
    yield put(fetchDashboardQualitativeFeedbackCompleted(error));
  }
}

export default function* main() {
  yield all([
    takeLatest(fetchDashboardStats, fetchDashboardStatsSaga),
    takeLatest(fetchDashboardSeats, fetchDashboardSeatsSaga),
    takeLatest(fetchDashboardCommonChallenges, fetchDashboardCommonChallengesSaga),
    takeLatest(fetchDashboardQualitativeFeedback, fetchDashboardQualitativeFeedbackSaga),
  ]);
}
