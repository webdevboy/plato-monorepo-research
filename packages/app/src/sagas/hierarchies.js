import { all, call, put, takeLatest } from 'redux-saga/effects';
import { fetchHierarchies, fetchHierarchiesCompleted } from 'ducks';

import MenteeService from 'services/api/plato/mentee/mentee';

export function* fetchHierarchiesSaga() {
  try {
    const hierarchies = yield call(MenteeService.getAllHierarchies);
    yield put(fetchHierarchiesCompleted(hierarchies));
  } catch (error) {
    yield put(fetchHierarchiesCompleted(error));
  }
}

export default function* main() {
  yield all([takeLatest(fetchHierarchies, fetchHierarchiesSaga)]);
}
