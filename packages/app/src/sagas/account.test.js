import accountSaga, { signUpSaga } from './account';
import { all, takeLatest } from 'redux-saga/effects';
import { signUp, signUpCompleted } from 'ducks';

import AccountService from 'services/api/plato/account/account';
import history from 'utils/history';
import { recordSaga } from 'utils/testHelpers';

jest.mock('services/api/plato/account/account');
jest.mock('utils/history');

it('should setup the sagas', () => {
  const generator = accountSaga();
  expect(generator.next().value).toEqual(all([takeLatest(signUp, signUpSaga)]));
  expect(generator.next().done).toBe(true);
});

describe('signUp', () => {
  afterEach(() => {
    AccountService.createAccount.mockClear();
    history.push.mockClear();
  });

  it('should dispatch signUpCompleted on success', async () => {
    AccountService.createAccount.mockResolvedValueOnce({
      result: 'test-slack-url',
    });
    const action = signUp({
      stripe: {
        createToken: jest.fn().mockResolvedValueOnce({ token: { id: 'test-token-id' } }),
      },
      name: 'test-name',
      email: 'test-email',
      planId: 'test-plan-id',
      mentee: {},
    });
    const dispatched = await recordSaga(signUpSaga, action);
    expect(dispatched).toContainEqual(signUpCompleted('test-slack-url'));
    expect(AccountService.createAccount).toHaveBeenNthCalledWith(1, {
      cardId: 'test-token-id',
    });
    expect(history.push).toHaveBeenNthCalledWith(1, '/sign-up/slack');
  });

  it('should dispatch signUpCompleted on Stripe object error', async () => {
    const action = signUp({
      stripe: {
        createToken: jest.fn().mockResolvedValueOnce({ error: new Error('test-error') }),
      },
      name: 'test-name',
      email: 'test-email',
      planId: 'test-plan-id',
      mentee: {},
    });
    const dispatched = await recordSaga(signUpSaga, action);
    expect(action.payload.stripe.createToken).toHaveBeenNthCalledWith(1, {
      name: 'test-name',
      email: 'test-email',
      plan: 'test-plan-id',
    });
    expect(dispatched).toContainEqual(signUpCompleted(new Error('test-error')));
  });

  it('should dispatch signUpCompleted on account creation error', async () => {
    AccountService.createAccount.mockRejectedValueOnce(new Error('test-error'));
    const action = signUp({
      stripe: {
        createToken: jest.fn().mockResolvedValueOnce({ token: 'test-token' }),
      },
      name: 'test-name',
      email: 'test-email',
      planId: 'test-plan-id',
      mentee: {},
    });
    const dispatched = await recordSaga(signUpSaga, action);
    expect(dispatched).toContainEqual(signUpCompleted(new Error('test-error')));
  });
});
