import { all, takeLatest } from 'redux-saga/effects';
import { fetchSubscriptions, fetchSubscriptionsCompleted } from 'ducks';
import saga, { fetchSubscriptionsSaga } from './subscriptions';

import OrganizationService from 'services/api/plato/organization/organization';
import { recordSaga } from 'utils/testHelpers';

jest.mock('services/api/plato/organization/organization');

it('should setup the sagas', () => {
  const generator = saga();
  expect(generator.next().value).toEqual(
    all([takeLatest(fetchSubscriptions, fetchSubscriptionsSaga)])
  );
  expect(generator.next().done).toBe(true);
});

describe('fetchSubscriptions', () => {
  it('should dispatch fetchSubscriptionsCompleted', async () => {
    OrganizationService.getSubscriptions.mockResolvedValueOnce([{ id: 'test-relationship-id' }]);
    const dispatched = await recordSaga(fetchSubscriptionsSaga, fetchSubscriptions());
    expect(dispatched).toContainEqual(
      fetchSubscriptionsCompleted([{ id: 'test-relationship-id' }])
    );
  });

  it('should dispatch fetchSubscriptionsCompleted on error', async () => {
    OrganizationService.getSubscriptions.mockRejectedValueOnce(new Error('test-error'));
    const dispatched = await recordSaga(fetchSubscriptionsSaga, fetchSubscriptions());
    expect(dispatched).toContainEqual(fetchSubscriptionsCompleted(new Error('test-error')));
  });
});
