import { all, takeEvery, takeLatest } from 'redux-saga/effects';
import amasSaga, { fetchAmaAttendancesSaga, fetchAmasSaga } from './amas';
import {
  fetchAmaAttendances,
  fetchAmaAttendancesCompleted,
  fetchAmas,
  fetchAmasCompleted,
  fetchMentor,
} from 'ducks';

import AmaService from 'services/api/plato/ama/ama';
import { getMentor } from 'selectors';
import { recordSaga } from 'utils/testHelpers';

jest.mock('selectors');
jest.mock('services/api/plato/ama/ama');

it('should setup the sagas', () => {
  const generator = amasSaga();
  expect(generator.next().value).toEqual(
    all([
      takeLatest(fetchAmas, fetchAmasSaga),
      takeEvery(fetchAmaAttendances, fetchAmaAttendancesSaga),
    ])
  );
  expect(generator.next().done).toBe(true);
});

describe('fetchAmas', () => {
  it('should dispatch fetchAmasCompleted', async () => {
    AmaService.getAmasForMentee.mockResolvedValueOnce({
      result: [{ id: 'test-ama-id' }],
    });
    const dispatched = await recordSaga(fetchAmasSaga, fetchAmas());
    expect(dispatched).toContainEqual(fetchAmasCompleted([{ id: 'test-ama-id' }]));
  });

  it('should fetch the list of attendances', async () => {
    AmaService.getAmasForMentee.mockResolvedValueOnce({
      result: [{ id: 'test-ama-id' }],
    });
    const dispatched = await recordSaga(fetchAmasSaga, fetchAmas());
    expect(dispatched).toContainEqual(fetchAmaAttendances({ amaId: 'test-ama-id' }));
  });

  it('should dispatch fetchAmasCompleted on error', async () => {
    AmaService.getAmasForMentee.mockRejectedValueOnce(new Error('test-error'));
    const dispatched = await recordSaga(fetchAmasSaga, fetchAmas());
    expect(dispatched).toContainEqual(fetchAmasCompleted(new Error('test-error')));
  });
});

describe('fetchAmaAttendances', () => {
  it.skip('should dispatch fetchAmaAttendancesCompleted', async () => {
    AmaService.getAttendances.mockResolvedValueOnce([{ id: 'test-ama-attendance-id' }]);
    const dispatched = await recordSaga(
      fetchAmaAttendancesSaga,
      fetchAmaAttendances({ amaId: 'test-ama-id', mentorId: 'test-mentor-id' })
    );
    expect(dispatched).toContainEqual(
      fetchAmaAttendancesCompleted({
        amaId: 'test-ama-id',
        attendances: [{ id: 'test-ama-attendance-id' }],
      })
    );
  });

  it.skip('should fetch the mentor when the mentor is missing', async () => {
    AmaService.getAttendances.mockResolvedValueOnce([{ id: 'test-ama-attendance-id' }]);
    getMentor.mockResolvedValueOnce({ id: 'test-mentor-id' });
    const dispatched = await recordSaga(
      fetchAmaAttendancesSaga,
      fetchAmaAttendances({ amaId: 'test-ama-id', mentorId: 'test-mentor-id' })
    );
    expect(dispatched).toContainEqual(fetchMentor('test-mentor-id'));
  });

  it.skip('should not fetch the mentor when the mentor is present', async () => {
    AmaService.getAttendances.mockResolvedValueOnce([{ id: 'test-ama-attendance-id' }]);
    getMentor.mockResolvedValueOnce(null);
    const dispatched = await recordSaga(
      fetchAmaAttendancesSaga,
      fetchAmaAttendances({ amaId: 'test-ama-id', mentorId: 'test-mentor-id' })
    );
    expect(dispatched).not.toContainEqual(fetchMentor('test-mentor-id'));
  });

  it('should dispatch fetchAmaAttendancesCompleted on error', async () => {
    AmaService.getAttendances.mockRejectedValueOnce(new Error('test-error'));
    const dispatched = await recordSaga(
      fetchAmaAttendancesSaga,
      fetchAmaAttendances({ amaId: 'test-ama-id', mentorId: 'test-mentor-id' })
    );
    expect(dispatched).toContainEqual(fetchAmaAttendancesCompleted(new Error('test-error')));
  });
});
