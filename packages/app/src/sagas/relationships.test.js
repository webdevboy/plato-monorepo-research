import { all, takeLatest } from 'redux-saga/effects';
import { createRelationship, createRelationshipCompleted } from 'ducks';
import saga, { createRelationshipSaga } from './relationships';

import MenteeService from 'services/api/plato/mentee/mentee';
import { recordSaga } from 'utils/testHelpers';

jest.mock('services/api/plato/mentee/mentee');
jest.mock('utils/history');

it('should setup the sagas', () => {
  const generator = saga();
  expect(generator.next().value).toEqual(
    all([takeLatest(createRelationship, createRelationshipSaga)])
  );
  expect(generator.next().done).toBe(true);
});

describe('createRelationship', () => {
  it('should dispatch createRelationshipCompleted', async () => {
    MenteeService.createRelationship.mockResolvedValueOnce({ id: 'test-relationship-id' });
    const dispatched = await recordSaga(createRelationshipSaga, createRelationship({}));
    expect(dispatched).toContainEqual(createRelationshipCompleted());
  });

  it('should dispatch createRelationshipCompleted on error', async () => {
    MenteeService.createRelationship.mockRejectedValueOnce(new Error('test-error'));
    const dispatched = await recordSaga(createRelationshipSaga, createRelationship({}));
    expect(dispatched).toContainEqual(createRelationshipCompleted(new Error('test-error')));
  });
});
