import { all, call, put, takeLatest, takeEvery } from 'redux-saga/effects';
import {
  fetchMentor,
  fetchMentorCompleted,
  fetchMentorRelationshipSlots,
  fetchMentorRelationshipSlotsCompleted,
  fetchMentors,
  fetchMentorsCompleted,
} from 'ducks';

import MentorService from 'services/api/plato/mentor/mentor';

export function* fetchMentorSaga(action) {
  try {
    const result = yield call(MentorService.getMentor, action.payload);
    yield put(fetchMentorCompleted(result));
  } catch (error) {
    yield put(fetchMentorCompleted(error));
  }
}

export function* fetchMentorRelationshipSlotsSaga(action) {
  const mentorId = action.payload;
  try {
    const result = yield call(MentorService.getRelationshipSlots, mentorId);
    yield put(fetchMentorRelationshipSlotsCompleted({ mentorId, slots: result }));
  } catch (error) {
    yield put(fetchMentorRelationshipSlotsCompleted(error));
  }
}

export function* fetchMentorsSaga(action) {
  try {
    const { result } = yield call(MentorService.searchForMentee, action.payload);
    yield put(fetchMentorsCompleted(result));
  } catch (error) {
    yield put(fetchMentorsCompleted(error));
  }
}

export default function* main() {
  yield all([
    takeEvery(fetchMentor, fetchMentorSaga),
    takeLatest(fetchMentorRelationshipSlots, fetchMentorRelationshipSlotsSaga),
    takeLatest(fetchMentors, fetchMentorsSaga),
  ]);
}
