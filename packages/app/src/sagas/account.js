import { all, call, put, takeLatest } from 'redux-saga/effects';
import { signUp, signUpCompleted } from 'ducks';

import AccountService from 'services/api/plato/account/account';
import history from 'utils/history';

/**
 * Get the Stripe token and create an account for the user including a company, a mentee
 * and a subscription.
 * @param {Object} action.payload.mentee The object containing the information about the mentee.
 * @param {Object} action.payload.stripe The Stripe object injected via a module.
 * @param {String} action.payload.email The email of the card holder.
 * @param {String} action.payload.name The name of the card holder.
 * @param {String} action.payload.planId The ID of the selected plan.
 */
export function* signUpSaga(action) {
  const { stripe, name, email, planId, mentee } = action.payload;
  try {
    const { error, token } = yield call(stripe.createToken, {
      name,
      email,
      plan: planId,
    });
    if (error) {
      yield put(signUpCompleted(new Error(error.message)));
      return;
    }

    const { result } = yield call(
      AccountService.createAccount,
      Object.assign(mentee, {
        cardId: token.id,
      })
    );
    yield put(signUpCompleted(result));
    yield call(history.push, '/sign-up/slack');
  } catch (error) {
    yield put(signUpCompleted(error));
  }
}

export default function* main() {
  yield all([takeLatest(signUp, signUpSaga)]);
}
