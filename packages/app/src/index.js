import 'react-app-polyfill/ie11';
import 'normalize.css';
import 'typeface-source-sans-pro';
import 'utils/extractFontAwesomeIcons';
import 'utils/configureMoment';
import 'utils/initializeGoogleTagManager';

import { QueryLoader, ScrollToTop, Snackbar } from 'containers';

import App from 'App';
import { ConnectedRouter } from 'connected-react-router';
import { ConnectedUserProvider } from 'components/context/UserContext';
import ErrorBoundary from 'components/shared/ErrorBoundary';
import { Provider } from 'react-redux';
import { QueryLoaderProvider } from 'components/context/QueryLoaderContext';
import React from 'react';
import ReactDOM from 'react-dom';
import { Router } from 'react-router-dom';
import { SEGMENT_WRITE_KEY } from 'constants/Analytics';
import { SnackbarProvider } from 'components/context/SnackbarContext';
import { ThemeProvider } from 'styled-components';
import analytics from 'react-segment';
import { fetchInitialData } from 'ducks';
import history from 'utils/history';
import { initializeLogRocket } from 'utils/logRocket';
import { initializeSentry } from 'utils/sentry';
import store from 'store';
import theme from 'styles/theme';
import GlobalStyle from 'components/GlobalStyle';

initializeLogRocket();
initializeSentry();

analytics.default.load(SEGMENT_WRITE_KEY);
analytics.default.page();

// Fetch the initial data
store.dispatch(fetchInitialData());

ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      {/* TODO: Need to keep a Router context for non-redux stuff */}
      <Router history={history}>
        <ConnectedUserProvider>
          <ScrollToTop>
            <ThemeProvider theme={theme}>
              <ErrorBoundary>
                <QueryLoader>
                  <Snackbar>
                    <QueryLoaderProvider>
                      <SnackbarProvider>
                        <GlobalStyle />
                        <App />
                      </SnackbarProvider>
                    </QueryLoaderProvider>
                  </Snackbar>
                </QueryLoader>
              </ErrorBoundary>
            </ThemeProvider>
          </ScrollToTop>
        </ConnectedUserProvider>
      </Router>
    </ConnectedRouter>
  </Provider>,
  document.getElementById('root')
);
