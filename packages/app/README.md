[![Build Status](https://travis-ci.com/birdly/plato-app.svg?token=kvpw7PPQswhzT2dojFnJ&branch=master)](https://travis-ci.com/birdly/plato-app)
[![Netlify Status](https://api.netlify.com/api/v1/badges/e9a0bdc1-2d20-499a-81bf-aec6a53e5d71/deploy-status)](https://app.netlify.com/sites/plato-web-master/deploys)
![Redux migration progress](https://img.shields.io/static/v1.svg?label=Redux%20migration%20progress&message=12%&color=red)
![Formik migration progress](https://img.shields.io/static/v1.svg?label=Formik%20migration%20progress&message=46%&color=orange)

# Web app

React app for mentors and mentees.

## Setup

### Editor Configuration

We use Visual Studio Code with a number of recommended extensions. To install
these extensions, follow these steps:

1. Press ⌘⇧P and run `Shell Command: Install 'code' command in PATH`.
2. Run a terminal in the root directory of this project.
3. Run `./install-extensions`.

### Development

Here are the steps to build and run the project locally.

```
npm i
npm start
```

To customize the application you can edit the `.env.local` file.

#### Lint code

```
npm run lint
```

#### Run tests

```
npm test
```

#### Prepare the production build

```
npm run build
```

### Docker

#### Build the image

In order to build the Docker image, you need to pass build arguments to target the right endpoints and compile the application.

Note that we use a multi-stage build to optimise the image size and you need to interpolate values given the environment you are targeting.

```
docker build \
    --build-arg REACT_APP_VERSION=<PACKAGE_VERSION> \
    --build-arg REACT_APP_PLATO_API_URI=<PLATO_API_URI> \
    --build-arg REACT_APP_BACK_OFFICE_API_URI=<BACK_OFFICE_API_URI> \
    --build-arg REACT_APP_POSTGRES_API_URI=<POSTGRES_API_URI> \
    --build-arg REACT_APP_SEGMENT_WRITE_KEY=<SEGMENT_API_KEY> \
    --build-arg REACT_APP_SENTRY_DSN=<SENTRY_DSN> \
    -t plato-web .
```

#### Run the image

Run the application and expose the port 8080.

```
docker run -it -p 8080:80 plato-web
```

## Configuration

### Environment variables

Access environment variables in the app by referencing properties on `process.env`. You must prepend the name of your environment variables with `REACT_APP_` in order to reference them in the code.

| NAME                                           | Type   | Description                                                   |
| ---------------------------------------------- | ------ | ------------------------------------------------------------- |
| NODE_PATH                                      | String | Root path used for absolute imports.                          |
| REACT_APP_BACK_OFFICE_API_URI                  | URI    | Endpoint of the Plato back office.                            |
| REACT_APP_DAYS_BEFORE_ONBOARDING_CHECK_IN_CALL | Number | Time to wait before check-in call in days.                    |
| REACT_APP_PLATO_API_URI                        | URI    | Endpoint of the Plato API.                                    |
| REACT_APP_POSTGRES_API_URI                     | URI    | Endpoint of the Plato PGSQL API.                              |
| REACT_APP_SEGMENT_WRITE_KEY                    | String | Segment API key.                                              |
| REACT_APP_SENTRY_DSN                           | URI    | Endpoint of the Sentry DSN.                                   |
| REACT_APP_SOURCE_APPLICATION                   | String | Source-Application header value.                              |
| REACT_APP_LOGROCKET_ID                         | String | LogRocket ID.                                                 |
| REACT_APP_TAG_MANAGER_ID                       | String | Google Tag Manager ID.                                        |
| REACT_APP_VALIDATIONS_FEATURE_WHITELIST        | CSV    | List of mentee id that have access to the validation feature. |
| REACT_APP_VERSION                              | String | Current version of the app.                                   |
